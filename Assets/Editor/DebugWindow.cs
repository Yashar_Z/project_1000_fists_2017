﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class DebugWindow : EditorWindow
{

    string enemyNumber;

    [MenuItem("Designer/Settings")]
    static void Init()
    {
        DebugWindow window = (DebugWindow)EditorWindow.GetWindow(typeof(DebugWindow));
        window.Show();
    }

    void OnEnable()
    {

    }
    void OnGUI()
    {
        enemyNumber = EditorGUILayout.TextField(new GUIContent("enemy number"), enemyNumber,GUILayout.MaxWidth(300),GUILayout.MinWidth(250));

        StreamReader sr = new StreamReader("Assets/Scripts/Editor Scripts/designervalues.txt");
        EditorGUILayout.LabelField("currect value  "+sr.ReadToEnd());
        sr.Close();
        if (GUILayout.Button("save",GUILayout.MaxWidth(150)))
        {
            SaveToFile();
        }

    }
    void SaveToFile()
    {
        StreamWriter sw = new StreamWriter("Assets/Scripts/Editor Scripts/designervalues.txt");
        sw.Write(enemyNumber);
        sw.Close();
    }
}
