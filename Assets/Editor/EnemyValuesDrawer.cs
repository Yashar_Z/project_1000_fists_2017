﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Text.RegularExpressions;

[CustomPropertyDrawer(typeof(EnemyValues))]
public class EnemyValuesDrawer : PropertyDrawer
{

    Color tempColor;
    private const string defaultRandomPool1 = "lurd";
    private const string defaultRandomPool2 = "/-2486";
    private const string defaultRandomPool3 = "lurd/-2486";

    void OnEnable()
    {

    }

    bool isEnemy=false; //flag baraye enemy boodan ya naboodan

//    bool isExplosive = false;//flag baraye explision damage

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string alarmText = (property.FindPropertyRelative("flagAlarm").boolValue) ? "AKHTOM" : "";

        SetFlags(property);
        SetDefaultRandomPools(property);

        var storeTheRealPosition = position;
        position.height = 16f;

        GUIContent labelCorrection = new GUIContent();
        labelCorrection.text = property.FindPropertyRelative("id").stringValue;
        labelCorrection.text += "-" + Enum.GetName(typeof(Enemy.EnemyType), property.FindPropertyRelative("enemyType").enumValueIndex);
        labelCorrection.text += "      " +  property.FindPropertyRelative("AbsoluteTime").stringValue;
        labelCorrection.text += "      " + alarmText;

        labelCorrection.tooltip = Enum.GetName(typeof(Enemy.LaneTypes), property.FindPropertyRelative("enemyLane").enumValueIndex); 

        if (!hasError(property))
            EditorGUI.PropertyField(position, property, labelCorrection); //onvan e list
        else
        {
            PaintItRed();
            EditorGUI.PropertyField(position, property, labelCorrection);
            RevertTheColor();
        }
        if (property.isExpanded) //namayesh anasor list
        {
            EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyType"), new GUIContent("Enemy Type"));

            if (property.FindPropertyRelative("enemySpawnTime").floatValue > 0)
            {
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemySpawnTime"), new GUIContent("Time of Spawn", "Must be greater than 0"));
            }
            else
            {
                PaintItRed();
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemySpawnTime"), new GUIContent("Time of Spawn", "Must be greater than 0"));
                RevertTheColor();
            }

            if (property.FindPropertyRelative("Hp").intValue > 0)
            {
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 3, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("Hp"), new GUIContent("Health Point", "Must be greater than 0"));
            }
            else
            {
                PaintItRed();
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 3, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("Hp"), new GUIContent("Health Point", "Must be greater than 0"));
                RevertTheColor();
            }
            if (property.FindPropertyRelative("enemyHP").stringValue.Length <= property.FindPropertyRelative("Hp").intValue
                 && Regex.IsMatch(property.FindPropertyRelative("enemyHP").stringValue, @"^[lurd\/\-2486*]*$"))
            {
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 4, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyHP"), new GUIContent("Arrows", "Must be less than  Health point and contains only lurd/-2486*"));
            }
            else
            {
                PaintItRed();
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 4, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyHP"), new GUIContent("Arrows", "Must be less than  Health point and contains only lurd/-2486*"));
                RevertTheColor();
            }

            if (Regex.IsMatch(property.FindPropertyRelative("randomPool").stringValue, @"^[lurd\/\-2486]*$"))
            {
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 5, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("randomPool"), new GUIContent("Random Pool", "Must contains only lurd/-2486"));
            }
            else
            {
                PaintItRed();
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 5, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("randomPool"), new GUIContent("Random Pool", "Must contains only lurd/-2486"));
                RevertTheColor();
            }

            //if (isEnemy && isExplosive)
            //    if (property.FindPropertyRelative("explosionDamage").intValue > 0)
            //    {
            //        EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 6, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("explosionDamage"), new GUIContent("Explosion Damage", "Must be greater than 0"));
            //    }
            //    else
            //    {
            //        PaintItRed();
            //        EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 6, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("explosionDamage"), new GUIContent("Explosion Damage", "Must be greater than 0"));
            //        RevertTheColor();
            //    }
            
			// The if was to show lanes ONLY for enemies
			//if (isEnemy)
                EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 6, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyLane"), new GUIContent("Enemy Lane"));

            if (!isEnemy)
                if (property.FindPropertyRelative("enemyMoveSpeed").floatValue > 0)
                {
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 7, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyMoveSpeed"), new GUIContent("Item Speed", "Must be greater than 0"));
                }
                else
                {
                    PaintItRed();
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 7, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("enemyMoveSpeed"), new GUIContent("Item Speed", "Must be greater than 0"));
                    RevertTheColor();
                }
            if (!isEnemy)
                if (property.FindPropertyRelative("itemValue").intValue > 0)
                {
                    EditorGUI.IntSlider(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 8, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemValue"), 1, 3, new GUIContent("Item Value", "Must be greater than 0"));
                }
                else
                {
                    PaintItRed();
                    EditorGUI.IntSlider(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 8, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemValue"), 1, 3, new GUIContent("Item Value", "Must be greater than 0"));
                    RevertTheColor();
                }


            if (!isEnemy)
                if (property.FindPropertyRelative("itemTimeVariation").floatValue >= 0)
                {
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 9, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemTimeVariation"), new GUIContent("Item Time Variation", "Must be greater than 0"));
                }
                else
                {
                    PaintItRed();
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 9, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemTimeVariation"), new GUIContent("Item Time Variation", "Must be greater than 0"));
                    RevertTheColor();
                }

            if (property.FindPropertyRelative("enemyType").enumValueIndex == 24)
                if (Regex.IsMatch(property.FindPropertyRelative("itemRandomPool").stringValue, @"^[hpgmifa]*$"))
                {
                    //TO DO: az default ha bekhanad
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 10, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemRandomPool"), new GUIContent("Item Random Pool", "Must contains only hpgmifa"));
                }
                else
                {
                    PaintItRed();
                    EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 10, position.width, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("itemRandomPool"), new GUIContent("Item Random Pool", "Must contains only hpgmifa"));
                    RevertTheColor();
                }
            EditorGUI.PrefixLabel(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 11, 100, EditorGUIUtility.singleLineHeight), new GUIContent("Flag"));
            property.FindPropertyRelative("flagAlarm").boolValue = EditorGUI.Toggle(new Rect(position.xMax, position.y + EditorGUIUtility.singleLineHeight * 11, position.width - 100, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("flagAlarm").boolValue);
            if (GUI.Button(new Rect(position.xMin + 50f, storeTheRealPosition.yMax - 25f, position.width - 100f, 20f), "duplicate"))
            {
                (property.serializedObject.targetObject as LevelEnemies).Duplicate(property.FindPropertyRelative("id").stringValue);
            }
        }
    }

    private static void SetDefaultRandomPools(SerializedProperty property)
    {
        switch (property.FindPropertyRelative("enemyType").enumValueIndex)
        {
            case 0:
            case 2:
            case 4:
            case 6:
            case 8:
            case 10:
                if (string.IsNullOrEmpty(property.FindPropertyRelative("randomPool").stringValue))
                    property.FindPropertyRelative("randomPool").stringValue = defaultRandomPool1;
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
            case 11:
                if (string.IsNullOrEmpty(property.FindPropertyRelative("randomPool").stringValue))
                    property.FindPropertyRelative("randomPool").stringValue = defaultRandomPool2;
                break;
            default:
                if (string.IsNullOrEmpty(property.FindPropertyRelative("randomPool").stringValue))
                    property.FindPropertyRelative("randomPool").stringValue = defaultRandomPool3;
                break;
        }
        if (string.IsNullOrEmpty(property.FindPropertyRelative("itemRandomPool").stringValue))
            property.FindPropertyRelative("itemRandomPool").stringValue = "a";
    }
    private void SetFlags(SerializedProperty property)
    {
		// Change the 18 from the end to 40
        if ((property.FindPropertyRelative("enemyType").enumValueIndex) < 40)
            isEnemy = true;
        else
            isEnemy = false;
//        if ((property.FindPropertyRelative("enemyType").enumValueIndex) == 10 || (property.FindPropertyRelative("enemyType").enumValueIndex) == 11
//            || ((property.FindPropertyRelative("enemyType").enumValueIndex) >= 14) && (property.FindPropertyRelative("enemyType").enumValueIndex) <= 17)//10,11,14,15,16,17
//            isExplosive = true;
//        else
//            isExplosive = false;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (property.isExpanded)
            return EditorGUI.GetPropertyHeight(property);// + 20f;
        return EditorGUI.GetPropertyHeight(property);
    }
    bool hasError(SerializedProperty property)
    {
        if (property.FindPropertyRelative("enemySpawnTime").floatValue < 0 
            || property.FindPropertyRelative("Hp").intValue <= 0  //all
            || property.FindPropertyRelative("enemyHP").stringValue.Length > property.FindPropertyRelative("Hp").intValue //all
            || !Regex.IsMatch(property.FindPropertyRelative("enemyHP").stringValue, @"^[lurd\/\-2486*]*$") //all
            //|| (property.FindPropertyRelative("explosionDamage").intValue <= 0 && isExplosive) //explosive enemy
            || (property.FindPropertyRelative("enemyMoveSpeed").floatValue <= 0 && !isEnemy) //item
            || (property.FindPropertyRelative("itemValue").intValue <= 0 && !isEnemy)  //item
            || (property.FindPropertyRelative("itemTimeVariation").floatValue < 0 &&!isEnemy) //item
            || (!Regex.IsMatch(property.FindPropertyRelative("itemRandomPool").stringValue, @"^[hpgmifa]*$") &&!isEnemy) //item
            || !Regex.IsMatch(property.FindPropertyRelative("randomPool").stringValue, @"^[lurd\/\-2486]*$") //all
            )
        {
            return true;
        }
        return false;
    }
    void PaintItRed()
    {
        tempColor = GUI.contentColor;
        GUI.contentColor = Color.red;
    }
    void RevertTheColor()
    {
        GUI.contentColor = tempColor;
    }
}
