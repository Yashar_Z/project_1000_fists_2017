﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
public class ItemDatabaseEditor : EditorWindow
{
    private enum State
    {
        BLANK,
        EDIT,
        ADD
    }

    private State state;
    private int selectedItem;
    private string newItemName;
    private int newItemId;
    private int newOddsOfHappening;
    private int newNeedsToComplete;
    private Item.ItemTag newItemTag;
    private string newFarsiName;
    private string newDescription;
    private string newItemEffectDescription;

    private const string DATABASE_PATH = @"Assets/Resources/itemDB.asset";

    private ItemDatabase items;
    private Vector2 _scrollPos;

    [MenuItem("Designer/Item Database %#w")]
    public static void Init()
    {
        ItemDatabaseEditor window = EditorWindow.GetWindow<ItemDatabaseEditor>();
        window.minSize = new Vector2(800, 400);
        window.Show();
    }

    void OnEnable()
    {
        if (items == null)
            LoadDatabase();

        state = State.BLANK;
    }

    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        DisplayListArea();
        DisplayMainArea();
        EditorGUILayout.EndHorizontal();
    }

    void LoadDatabase()
    {
        items = (ItemDatabase)AssetDatabase.LoadAssetAtPath(DATABASE_PATH, typeof(ItemDatabase));

        if (items == null)
            CreateDatabase();
    }

    void CreateDatabase()
    {
        items = ScriptableObject.CreateInstance<ItemDatabase>();
        AssetDatabase.CreateAsset(items, DATABASE_PATH);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    void DisplayListArea()
    {
        EditorGUILayout.BeginVertical(GUILayout.Width(250));
        EditorGUILayout.Space();

        _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, "box", GUILayout.ExpandHeight(true));

        for (int cnt = 0; cnt < items.COUNT; cnt++)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("-", GUILayout.Width(25)))
            {
                items.RemoveAt(cnt);
                //items.SortAlphabeticallyAtoZ();
                items.SortByItemId();
                EditorUtility.SetDirty(items);
                state = State.BLANK;
                return;
            }

            if (GUILayout.Button(items.ItemAtIndex(cnt).name, "box", GUILayout.ExpandWidth(true)))
            {
                selectedItem = cnt;
                state = State.EDIT;
            }

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        EditorGUILayout.LabelField("Items: " + items.COUNT, GUILayout.Width(100));

        if (GUILayout.Button("New Item"))
            state = State.ADD;

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }

    void DisplayMainArea()
    {
        EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
        EditorGUILayout.Space();

        switch (state)
        {
            case State.ADD:
                DisplayAddMainArea();
                break;
            case State.EDIT:
                DisplayEditMainArea();
                break;
            default:
                DisplayBlankMainArea();
                break;
        }

        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }

    void DisplayBlankMainArea()
    {
        EditorGUILayout.LabelField(
            "There are 3 things that can be displayed here.\n" +
            "1) Item info for editing\n" +
            "2) Black fields for adding a new item\n" +
            "3) Blank Area",
            GUILayout.ExpandHeight(true));
    }

    void DisplayEditMainArea()
    {
        items.ItemAtIndex(selectedItem).itemTag = (Item.ItemTag)EditorGUILayout.EnumPopup(new GUIContent("Item Tag: "), items.ItemAtIndex(selectedItem).itemTag);
        items.ItemAtIndex(selectedItem).itemId = int.Parse(EditorGUILayout.TextField(new GUIContent("Item Id: "), items.ItemAtIndex(selectedItem).itemId.ToString()));
        items.ItemAtIndex(selectedItem).name = EditorGUILayout.TextField(new GUIContent("Name: "), items.ItemAtIndex(selectedItem).name);
        items.ItemAtIndex(selectedItem).needsToComplete = int.Parse(EditorGUILayout.TextField(new GUIContent("Needs To Complete: "), items.ItemAtIndex(selectedItem).needsToComplete.ToString()));
        items.ItemAtIndex(selectedItem).oddsOfHappening = int.Parse(EditorGUILayout.TextField(new GUIContent("Odds of Happening: "), items.ItemAtIndex(selectedItem).oddsOfHappening.ToString()));
        items.ItemAtIndex(selectedItem).farsiName = EditorGUILayout.TextField(new GUIContent("Farsi Name: "), items.ItemAtIndex(selectedItem).farsiName);
		EditorGUILayout.LabelField("Item Effect Description:");
		items.ItemAtIndex(selectedItem).itemEffectDescription = EditorGUILayout.TextArea(items.ItemAtIndex(selectedItem).itemEffectDescription);
        EditorGUILayout.LabelField("Description:");
        items.ItemAtIndex(selectedItem).description = EditorGUILayout.TextArea(items.ItemAtIndex(selectedItem).description);

        EditorGUILayout.Space();

        if (GUILayout.Button("Done", GUILayout.Width(100)))
        {
            //items.SortAlphabeticallyAtoZ();
            items.SortByItemId();
            EditorUtility.SetDirty(items);
            state = State.BLANK;
        }
    }

    void DisplayAddMainArea()
    {
        newItemTag = (Item.ItemTag)EditorGUILayout.EnumPopup(new GUIContent("Item Tag: "), newItemTag);
        newItemId = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item Id: "), newItemId.ToString()));
        newItemName = EditorGUILayout.TextField(new GUIContent("Name: "), newItemName);
        newNeedsToComplete = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Need To Complete: "), newNeedsToComplete.ToString()));
        newOddsOfHappening = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Odds of Happening: "), newOddsOfHappening.ToString()));
        newFarsiName = EditorGUILayout.TextField(new GUIContent("FarsiName: "), newFarsiName);
		EditorGUILayout.LabelField("Item Effect Description:");
		items.ItemAtIndex(selectedItem).itemEffectDescription = EditorGUILayout.TextArea(items.ItemAtIndex(selectedItem).itemEffectDescription);
        EditorGUILayout.LabelField("Description:");
        newDescription = EditorGUILayout.TextArea(newDescription);

        //itemtag

        EditorGUILayout.Space();

        if (GUILayout.Button("Done", GUILayout.Width(100)))
        {
            items.Add(new Item(newItemTag, newItemId, newItemName, newOddsOfHappening, newNeedsToComplete, newFarsiName, newDescription,newItemEffectDescription));
            //items.SortAlphabeticallyAtoZ();
            items.SortByItemId();

            newItemName = string.Empty;
            newItemId = 0;
            newOddsOfHappening = 0;
            newItemTag = Item.ItemTag.Common;
            newNeedsToComplete = 0;
            newFarsiName = string.Empty;
            newDescription = string.Empty;
			newItemEffectDescription = string.Empty;
            EditorUtility.SetDirty(items);
            state = State.BLANK;
        }
    }
}