﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Kit.Editor;
using System;

[CustomEditor(typeof(LevelEnemies))]
public class LevelEnemiesEditor : Editor {
    bool generalPropFoldout = true;
    bool commonPropFoldout = true;
    bool additionalPropFoldout = true;
    bool rewardPropFoldout = true;
//    readonly GUIContent enemyListLabel = new GUIContent("List of Enemies");

    ReorderableListExtend enemyList;

    string removeFrom = "0";
    string removeTo = "1";

    string duplicateFrom = "0";
    string duplicateTo = "1";

    int increaseTimeFrom = 0;
    int increaseTimeTo = 1;
    float increaseTime = 0;
    bool includeZero = false;

    int randomPoolFrom = 0;
    int randomPoolTo = 1;
    string randomPool = "";

    int moveRangeFrom = 0;
    int moveRangeTo = 1;
    int moveTarget = 2;

    int allEnemyMustache=0;

    LevelEnemies spawnedEnemy;

    void OnEnable()
    {
        spawnedEnemy = (LevelEnemies)target;
        enemyList = new ReorderableListExtend(serializedObject, "enemyList");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        UpdateAbsoluteTime();

        EditorGUILayout.BeginVertical();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("Wave"), true);

		EditorGUILayout.PropertyField(serializedObject.FindProperty("Story"), true);
        #region General Properties

        generalPropFoldout = EditorGUILayout.Foldout(generalPropFoldout, "General Properties");

        GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.MaxWidth(90.0f), GUILayout.MinWidth(10.0f) };

        if (generalPropFoldout)
        {
            EditorGUI.indentLevel++;
            commonPropFoldout = EditorGUILayout.Foldout(commonPropFoldout, "Common Properties");
            if (commonPropFoldout)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("", options);
                EditorGUILayout.LabelField("Speed", options);
                EditorGUILayout.LabelField("Damage", options);
                EditorGUILayout.LabelField("Mustache", options);
                EditorGUILayout.LabelField("Health", options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Fat", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("fatSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("fatDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("fatMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("fatHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Thin", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("thinSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("thinDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("thinMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("thinHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Muscle", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("muscleSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("muscleDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("muscleMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("muscleHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //....
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Giant", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("giantSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("giantDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("giantMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("giantHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("HF", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //....
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("HF X", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfxSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfxDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfxMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfxHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //...
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel X", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelxSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelxDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelxMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelxHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //....
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel ?", options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelqSpeed"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelqDamage"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelqMustacheDrop"), GUIContent.none, options);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelqHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                //....
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Health", options))
                {
                    SetHealths(serializedObject);
                }
                EditorGUILayout.EndHorizontal();
            }
            additionalPropFoldout = EditorGUILayout.Foldout(additionalPropFoldout, "Additional Properties");
            if (additionalPropFoldout)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("HF Sticky Health", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfStickyHealth"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("HF_X Expl Damage", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hfxExplosionDamage"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel Vuln Time", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelVulnerableTime"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel_X Expl Damage", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelXExplosionDamage"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel_X Vuln Time", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelXVulnerableTime"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel_Q Expl Damage", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelQExplosionDamage"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel_Q Vuln Time", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelQVulnerableTime"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Barrel_Q Hedg Damage", GUILayout.MaxWidth(160f));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("barrelQHedgehogDamage"), GUIContent.none, options);
                EditorGUILayout.EndHorizontal();
            }
            rewardPropFoldout = EditorGUILayout.Foldout(rewardPropFoldout, "Reward Properties");
            if (rewardPropFoldout)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Mustache"), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("RandomChest"), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Items"), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("FixedChest"), true);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("missesThresholdPercent"), true);
            EditorGUI.indentLevel--;

        }
        #endregion

        enemyList.DoLayoutList();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Mustache in this level:" + allEnemyMustache);
        if (GUILayout.Button("Get Mustache", options))
        {

            foreach (var enemy in spawnedEnemy.enemyList)
            {
                switch (enemy.enemyType)
                {
                    case Enemy.EnemyType.Fat1:
                    case Enemy.EnemyType.Fat2:
                        allEnemyMustache += serializedObject.FindProperty("fatMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Thin1:
                    case Enemy.EnemyType.Thin2:
                        allEnemyMustache += serializedObject.FindProperty("thinMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Muscle1:
                    case Enemy.EnemyType.Muscle2:
                        allEnemyMustache += serializedObject.FindProperty("muscleMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Giant1:
                    case Enemy.EnemyType.Giant2:
                        allEnemyMustache += serializedObject.FindProperty("giantMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.HF1:
                    case Enemy.EnemyType.HF2:
                        allEnemyMustache += serializedObject.FindProperty("hfMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.HF_X1:
                    case Enemy.EnemyType.HF_X2:
                        allEnemyMustache += serializedObject.FindProperty("hfxMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Barrel1:
                    case Enemy.EnemyType.Barrel2:
                        allEnemyMustache += serializedObject.FindProperty("barrelMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Barrel_X1:
                    case Enemy.EnemyType.Barrel_X2:
                        allEnemyMustache += serializedObject.FindProperty("barrelxMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Barrel_Q1:
                    case Enemy.EnemyType.Barrel_Q2:
                        allEnemyMustache += serializedObject.FindProperty("barrelqMustacheDrop").intValue;
                        break;
                    case Enemy.EnemyType.Heart:
                        break;
                    case Enemy.EnemyType.Pipe:
                        break;
                    case Enemy.EnemyType.Hedgehog:
                        break;
                    case Enemy.EnemyType.Mustache:
                        break;
                    case Enemy.EnemyType.Elixir:
                        break;
                    case Enemy.EnemyType.Freeze:
                        break;
                    case Enemy.EnemyType.Random_Item:
                        break;
                    default:
                        break;
                }
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Remove Range:",GUILayout.MinWidth(85f));
        removeFrom = EditorGUILayout.TextField(removeFrom,options);
        removeTo = EditorGUILayout.TextField(removeTo,options);
        if (GUILayout.Button("-",options))
        {
            RemoveRangeEnemy(int.Parse(removeFrom), int.Parse(removeTo));
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Duplicate Range:", GUILayout.MinWidth(85f));
        duplicateFrom = EditorGUILayout.TextField(duplicateFrom, options);
        duplicateTo = EditorGUILayout.TextField(duplicateTo, options);
        if (GUILayout.Button("Dup", options))
        {
            DuplicateRangeEnemy(int.Parse(duplicateFrom), int.Parse(duplicateTo));
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Increase Time Range:", GUILayout.MinWidth(85f));
        increaseTimeFrom = EditorGUILayout.IntField(increaseTimeFrom, options);
        increaseTimeTo = EditorGUILayout.IntField(increaseTimeTo, options);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(20f));
        EditorGUILayout.LabelField("Time:", GUILayout.MaxWidth(40f));
        increaseTime = EditorGUILayout.FloatField(increaseTime, options);
        includeZero = EditorGUILayout.Toggle("Include Zero",includeZero, GUILayout.MinWidth(60f));
        if (GUILayout.Button("T+", options))
        {
            IncreaseTimeRange(increaseTimeFrom, increaseTimeTo, increaseTime, includeZero);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Random pool range:", GUILayout.MinWidth(85f));
        randomPoolFrom = EditorGUILayout.IntField(randomPoolFrom, options);
        randomPoolTo = EditorGUILayout.IntField(randomPoolTo, options);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(20f));
        EditorGUILayout.LabelField("Pool:", GUILayout.MaxWidth(40f));
        randomPool = EditorGUILayout.TextField(randomPool, options);

        if (GUILayout.Button("SetPool", options))
        {
            SetRandomPoolRange(randomPoolFrom, randomPoolTo, randomPool);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Move Range:", GUILayout.MinWidth(85f));
        moveRangeFrom = EditorGUILayout.IntField(moveRangeFrom, options);
        moveRangeTo = EditorGUILayout.IntField(moveRangeTo, options);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(20f));
        EditorGUILayout.LabelField("Move Target:", GUILayout.MaxWidth(80f));
        moveTarget = EditorGUILayout.IntField(moveTarget, options);

        if (GUILayout.Button("Move", options))
        {
            MoveRange(moveRangeFrom, moveRangeTo, moveTarget);
        }
        EditorGUILayout.EndHorizontal();

		// To turn enemy 2s in 1s. TODO: Remove it after this release.
		EditorGUILayout.Space();
		if (GUILayout.Button("Change Enemies 2 to 1", GUILayout.MinWidth(80f)))
		{
			EnemyValues e;
			var so = serializedObject;
			for (int i = 0; i < spawnedEnemy.enemyList.Count; i++)
			{
				e = spawnedEnemy.enemyList[i];
				switch (e.enemyType)
				{
				case Enemy.EnemyType.Fat1:
				case Enemy.EnemyType.Fat2:
					e.enemyType = Enemy.EnemyType.Fat1;
					break;
				case Enemy.EnemyType.Thin1:
				case Enemy.EnemyType.Thin2:
					e.enemyType = Enemy.EnemyType.Thin1;
					break;
				case Enemy.EnemyType.Muscle1:
				case Enemy.EnemyType.Muscle2:
					e.enemyType = Enemy.EnemyType.Muscle1;
					break;
				case Enemy.EnemyType.Giant1:
				case Enemy.EnemyType.Giant2:
					e.enemyType = Enemy.EnemyType.Giant1;
					break;
				default:
					break;
				}
			}
		}

        EditorGUILayout.EndVertical();

        if (GUI.changed)
            serializedObject.ApplyModifiedProperties();
    }

    private void MoveRange(int moveRangeFrom, int moveRangeTo, int moveTarget)
    {
        spawnedEnemy.MoveRange(moveRangeFrom, moveRangeTo, moveTarget);
    }

    private void SetRandomPoolRange(int from, int to, string randomPool)
    {
        for (int i = from; i <= to; i++)
        {
                spawnedEnemy.enemyList[i].randomPool = randomPool;
        }
    }

    private void SetHealths(SerializedObject so)
    {
        EnemyValues e;

        for (int i = 0; i < spawnedEnemy.enemyList.Count; i++)
        {
            e = spawnedEnemy.enemyList[i];
            switch (e.enemyType)
            {
                case Enemy.EnemyType.Fat1:
                case Enemy.EnemyType.Fat2:
                    e.Hp = so.FindProperty("fatHealth").intValue;
                    break;
                case Enemy.EnemyType.Thin1:
                case Enemy.EnemyType.Thin2:
                    e.Hp = so.FindProperty("thinHealth").intValue;
                    break;
                case Enemy.EnemyType.Muscle1:
                case Enemy.EnemyType.Muscle2:
                    e.Hp = so.FindProperty("muscleHealth").intValue;
                    break;
                case Enemy.EnemyType.Giant1:
                case Enemy.EnemyType.Giant2:
                    e.Hp = so.FindProperty("giantHealth").intValue;
                    break;
                case Enemy.EnemyType.HF1:
                case Enemy.EnemyType.HF2:
                    e.Hp = so.FindProperty("hfHealth").intValue;
                    break;
                case Enemy.EnemyType.HF_X1:
                case Enemy.EnemyType.HF_X2:
                    e.Hp = so.FindProperty("hfxHealth").intValue;
                    break;
                case Enemy.EnemyType.Barrel1:
                case Enemy.EnemyType.Barrel2:
                    e.Hp = so.FindProperty("barrelHealth").intValue;
                    break;
                case Enemy.EnemyType.Barrel_X1:
                case Enemy.EnemyType.Barrel_X2:
                    e.Hp = so.FindProperty("barrelxHealth").intValue;
                    break;
                case Enemy.EnemyType.Barrel_Q1:
                case Enemy.EnemyType.Barrel_Q2:
                    e.Hp = so.FindProperty("barrelqHealth").intValue;
                    break;
                case Enemy.EnemyType.Heart:
                    break;
                case Enemy.EnemyType.Pipe:
                    break;
                case Enemy.EnemyType.Hedgehog:
                    break;
                case Enemy.EnemyType.Mustache:
                    break;
                case Enemy.EnemyType.Elixir:
                    break;
                case Enemy.EnemyType.Freeze:
                    break;
                case Enemy.EnemyType.Random_Item:
                    break;
                default:
                    break;
            }          
        }
    }



    public void RemoveRangeEnemy(int from, int to)
    {
        int diff = to - from + 1;
        spawnedEnemy.enemyList.RemoveRange(from, diff);
    }
    public void DuplicateRangeEnemy(int from, int to)
    {
        spawnedEnemy.DuplicateRange(from, to);
    }

    public void IncreaseTimeRange(int from, int to, float time, bool includeZeros)
    {
        for (int i = from; i <= to; i++)
        {
            if (spawnedEnemy.enemyList[i].enemySpawnTime != 0 || includeZeros)
            {
                spawnedEnemy.enemyList[i].enemySpawnTime += time;
            }
        }
    }

    public void UpdateAbsoluteTime()
    {
        EnemyValues e;

        float t = 0;
        for (int i = 0; i < spawnedEnemy.enemyList.Count; i++)
        {
            e = spawnedEnemy.enemyList[i];
            e.id = i.ToString();
            t += e.enemySpawnTime;
            e.AbsoluteTime = string.Format("{0:0.00}", t.ToString());
        }
    }

}
