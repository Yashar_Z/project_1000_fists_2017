﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Aspect_Ratio_Fixer : MonoBehaviour {

	// InGame Holders
	public Transform spritesParentHolder;
	public Transform hudParentHolder;
	public Transform hudTOPparentHolder;

	// Menu Holder
	public Transform menuParentHolder;

	// Menu Canvas Holder
	public Transform transMenuTopCanvasHolder;

	private Vector3 ratioScale;
	private Camera mainCam;
	private float baseAspect;
	private string sceneName;

	// Use this for initialization
	void Awake () {
		//
		sceneName = SceneManager.GetActiveScene ().name;
		baseAspect = 9F / 16F;
		mainCam = Camera.main.GetComponent<Camera> ();
		ratioScale.x = mainCam.aspect * baseAspect;
		//
		ratioScale.y = ratioScale.x;
		ratioScale.z = ratioScale.x;

		// Update scale based on Scene
		switch (sceneName) {
		case "InGame_Scene":
//			spritesParentHolder.localScale = ratioScale;
			hudParentHolder.localScale = ratioScale;
			hudTOPparentHolder.localScale = ratioScale;
//			transform.localScale = ratioScale;
			mainCam.orthographicSize /= ratioScale.x;
			break;
		case "Menu_Scene":
//			menuParentHolder.localScale = ratioScale;

			// Separated for this might (Fix and test YES) but needs to remove cam from canv and for above to be comment
			mainCam.orthographicSize /= ratioScale.x;

			// TODO: Check for problem
//			transMenuTopCanvasHolder.localScale *= ratioScale.x;
			break;
		}

		// Check this stuff!
		Screen.orientation = ScreenOrientation.LandscapeLeft;
//		Screen.orientation = ScreenOrientation.AutoRotation;

	}

	public IEnumerator Orientation_Checker () {
		yield return new WaitForSeconds (5);
		Screen.orientation = ScreenOrientation.AutoRotation;

		// Check for bugs
		if (Screen.orientation == ScreenOrientation.Portrait) {
			Screen.orientation = ScreenOrientation.Landscape;
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
			
		yield return new WaitForSeconds (1);
		StartCoroutine (Orientation_Checker ());
	}
}
