﻿using UnityEngine;
using System.Collections;

public class Balance_Constants_Script {

	// Item_Menu_Script
	public static readonly int EggPrice_Koochool = 900;
	public static readonly int EggPrice_Aghdasi = 2500;
	public static readonly int EggPrice_Moshtan = 5500;

	// Item_Values BUUUUUTTTTTT in public values / inspector
	public static readonly int Item_MustacheValue_Common = 90;
	public static readonly int Item_MustacheValue_Rare = 300;
	public static readonly int Item_MustacheValue_Legendary = 1000;
		   
	// PlayerIngame
	public static readonly int ContinueCost = 500;
		    
	// PowerUps_Menu_Script
	public static readonly int BatteryRecharge_PerSlot = 75;

	// PowerUp_TimeManager_Script
	public static readonly int PowUp1_RechargeTime = 600;
	public static readonly int PowUp2_RechargeTime = 600;
		    
	// PowerUps_Menu_Script
	public static readonly int PowUp1_SlotPrice_2 = 5000;
	public static readonly int PowUp1_SlotPrice_3 = 25000;
	public static readonly int PowUp1_SlotPrice_4 = 100000;
	public static readonly int PowUp1_SlotPrice_5 = 300000;
		
	// PowerUps_Menu_Script
	public static readonly int PowUp2_SlotPrice_2 = 6000;
	public static readonly int PowUp2_SlotPrice_3 = 30000;
	public static readonly int PowUp2_SlotPrice_4 = 120000;
	public static readonly int PowUp2_SlotPrice_5 = 360000;
		    
	// Menu_Main_Script
	public static readonly int WorldUnlock_ByStar_1 = 0;
	public static readonly int WorldUnlock_ByStar_2 = 20;
	public static readonly int WorldUnlock_ByStar_3 = 55;
	public static readonly int WorldUnlock_ByStar_4 = 85;
	public static readonly int WorldUnlock_ByStar_5 = 100;
		
	// Menu_Main_Script
	public static readonly int WorldUnlock_ByMustache_1 = 0;
	public static readonly int WorldUnlock_ByMustache_2 = 1500;
	public static readonly int WorldUnlock_ByMustache_3 = 4000;
	public static readonly int WorldUnlock_ByMustache_4 = 6000;
	public static readonly int WorldUnlock_ByMustache_5 = 8500;

	// Shop_Values
	public static readonly int Shop_PriceNoOffer_Normal1 = 2000;
	public static readonly int Shop_PriceNoOffer_Normal2 = 5000;
	public static readonly int Shop_PriceNoOffer_Normal3 = 10000;
	public static readonly int Shop_PriceNoOffer_Normal4 = 20000;
	public static readonly int Shop_PriceNoOffer_Normal5 = 50000;

	// Shop_Values (For VAS)
	public static readonly int Shop_PriceNoOffer_NormalVAS1 = 100;
	public static readonly int Shop_PriceNoOffer_NormalVAS2 = 200;
	public static readonly int Shop_PriceNoOffer_NormalVAS3 = 500;
	public static readonly int Shop_PriceNoOffer_NormalVAS4 = 1000;
	public static readonly int Shop_PriceNoOffer_NormalVAS5 = 1000;

	// Shop_Values
	public static readonly int Shop_Mustache_Normal1 = 4000;
	public static readonly int Shop_Mustache_Normal2 = 10500;
	public static readonly int Shop_Mustache_Normal3 = 22000;
	public static readonly int Shop_Mustache_Normal4 = 48000;
	public static readonly int Shop_Mustache_Normal5 = 130000;

	// Shop_Values (For VAS)
	public static readonly int Shop_Mustache_NormalVAS1 = 1000;
	public static readonly int Shop_Mustache_NormalVAS2 = 2200;
	public static readonly int Shop_Mustache_NormalVAS3 = 6000;
	public static readonly int Shop_Mustache_NormalVAS4 = 13000;
	public static readonly int Shop_Mustache_NormalVAS5 = 13000;

	// Tutorial Unlock Levels
	public static readonly int TutLevel_BatteryCharge = 2;
	public static readonly int TutLevel_PowUp2Gain = 5;
	public static readonly int TutLevel_ItemProgress = 3;
	public static readonly int TutLevel_ItemRepeatItem = 3;
	public static readonly int TutLevel_EndlessUnlock = 7;

	// Tapsell Zone IDs
	public static readonly string Tapsell_AfterDailyString = "59e7009446846524fb42100f";
	public static readonly string Tapsell_BatteryRechargeString = "59ad1e6b468465033b0c643d";
    public static readonly string Tapsell_VidGift = "5a34e717799e6f0001530347";
    public static readonly string Tapsell_BannerGift = "5a520ebf600e6d0001381a65";

    // Popup_Daily
    public static readonly int DailyReward_NoVid1 = 150;
	public static readonly int DailyReward_NoVid2 = 200;
	public static readonly int DailyReward_NoVid3 = 250;
	public static readonly int DailyReward_NoVid4 = 300;
	public static readonly int DailyReward_NoVid5 = 350;
	public static readonly int DailyReward_NoVid6 = 500;
	public static readonly int DailyReward_YesVid1 = 300;
	public static readonly int DailyReward_YesVid2 = 400;
	public static readonly int DailyReward_YesVid3 = 500;
	public static readonly int DailyReward_YesVid4 = 600;
	public static readonly int DailyReward_YesVid5 = 700;
	public static readonly int DailyReward_YesVid6 = 1000;

	// Vid Gift
	public static readonly int VidGift_MustacheReward_Small = 100;
	public static readonly int VidGift_MustacheReward_Mid = 150;
	public static readonly int VidGift_MustacheReward_Big = 200;
	public static readonly int VidGift_MustacheReward_Biggest = 150;

	// Quest Reward Amount
	public static readonly int QuestReward_1 = 150;
	public static readonly int QuestReward_2 = 300;
	public static readonly int QuestReward_3 = 600;

	// Endless
	#region Endless Potion Values
	// Battery (Doesn't have)
//	public static readonly int Potion_Battery_1_Reward = 150;
	public static readonly int Potion_Battery_1_Cost = 200;
//	public static readonly int Potion_Battery_2_Reward = 150;
	public static readonly int Potion_Battery_2_Cost = 400;
//	public static readonly int Potion_Battery_3_Reward = 150;
	public static readonly int Potion_Battery_3_Cost = 600;
//	public static readonly int Potion_Battery_4_Reward = 150;
	public static readonly int Potion_Battery_4_Cost = 800;
//	public static readonly int Potion_Battery_5_Reward = 150;
	public static readonly int Potion_Battery_5_Cost = 1000;
	public static readonly int Potion_Battery_6_Cost = 1000;

	// Damage
	public static readonly int Potion_Damage_1_Reward = 1;
	public static readonly int Potion_Damage_1_Cost = 150;
	public static readonly int Potion_Damage_2_Reward = 1;
	public static readonly int Potion_Damage_2_Cost = 250;
	public static readonly int Potion_Damage_3_Reward = 1;
	public static readonly int Potion_Damage_3_Cost = 350;
	public static readonly int Potion_Damage_4_Reward = 1;
	public static readonly int Potion_Damage_4_Cost = 450;
	public static readonly int Potion_Damage_5_Reward = 1;
	public static readonly int Potion_Damage_5_Cost = 550;
	public static readonly int Potion_Damage_6_Reward = 1;
	public static readonly int Potion_Damage_6_Cost = 650;

	// HP
	public static readonly int Potion_HP_1_Reward = 5;
	public static readonly int Potion_HP_1_Cost = 150;
	public static readonly int Potion_HP_2_Reward = 10;
	public static readonly int Potion_HP_2_Cost = 250;
	public static readonly int Potion_HP_3_Reward = 15;
	public static readonly int Potion_HP_3_Cost = 350;
	public static readonly int Potion_HP_4_Reward = 20;
	public static readonly int Potion_HP_4_Cost = 450;
	public static readonly int Potion_HP_5_Reward = 25;
	public static readonly int Potion_HP_5_Cost = 550;
	public static readonly int Potion_HP_6_Reward = 25;
	public static readonly int Potion_HP_6_Cost = 650;

	// Crit
	public static readonly int Potion_Crit_1_Reward = 5;
	public static readonly int Potion_Crit_1_Cost = 150;
	public static readonly int Potion_Crit_2_Reward = 4;
	public static readonly int Potion_Crit_2_Cost = 250;
	public static readonly int Potion_Crit_3_Reward = 4;
	public static readonly int Potion_Crit_3_Cost = 350;
	public static readonly int Potion_Crit_4_Reward = 4;
	public static readonly int Potion_Crit_4_Cost = 450;
	public static readonly int Potion_Crit_5_Reward = 3;
	public static readonly int Potion_Crit_5_Cost = 550;
	public static readonly int Potion_Crit_6_Reward = 3;
	public static readonly int Potion_Crit_6_Cost = 650;

	// Last Stand
	public static readonly int Potion_LastStand_1_Reward = 1;
	public static readonly int Potion_LastStand_1_Cost = 150;
	public static readonly int Potion_LastStand_2_Reward = 1;
	public static readonly int Potion_LastStand_2_Cost = 250;
	public static readonly int Potion_LastStand_3_Reward = 1;
	public static readonly int Potion_LastStand_3_Cost = 350;
	public static readonly int Potion_LastStand_4_Reward = 1;
	public static readonly int Potion_LastStand_4_Cost = 450;
	public static readonly int Potion_LastStand_5_Reward = 1;
	public static readonly int Potion_LastStand_5_Cost = 550;
	public static readonly int Potion_LastStand_6_Reward = 1;
	public static readonly int Potion_LastStand_6_Cost = 650;

	// HeadStart
	public static readonly int Potion_WAVE_PER_HEADSTART_POTION = 3;
	public static readonly int Potion_HeadStart_1_Reward = 1;
	public static readonly int Potion_HeadStart_1_Cost = 150;
	public static readonly int Potion_HeadStart_2_Reward = 1;
	public static readonly int Potion_HeadStart_2_Cost = 250;
	public static readonly int Potion_HeadStart_3_Reward = 1;
	public static readonly int Potion_HeadStart_3_Cost = 350;
	public static readonly int Potion_HeadStart_4_Reward = 1;
	public static readonly int Potion_HeadStart_4_Cost = 450;
	public static readonly int Potion_HeadStart_5_Reward = 1;
	public static readonly int Potion_HeadStart_5_Cost = 550;
	public static readonly int Potion_HeadStart_6_Reward = 1;
	public static readonly int Potion_HeadStart_6_Cost = 650;
	public static readonly int Potion_HeadStart_7_Reward = 1;
	public static readonly int Potion_HeadStart_7_Cost = 650;
	public static readonly int Potion_HeadStart_8_Reward = 1;
	public static readonly int Potion_HeadStart_8_Cost = 650;

	// HeadStart Use Rewards
	public static readonly int Potion_HeadStart_1_BonusCombo = 5;
	public static readonly int Potion_HeadStart_1_BonusScore = 2500;
	public static readonly int Potion_HeadStart_2_BonusCombo = 2;
	public static readonly int Potion_HeadStart_2_BonusScore = 5000;
	public static readonly int Potion_HeadStart_3_BonusCombo = 2;
	public static readonly int Potion_HeadStart_3_BonusScore = 8500;
	public static readonly int Potion_HeadStart_4_BonusCombo = 2;
	public static readonly int Potion_HeadStart_4_BonusScore = 12000;
	public static readonly int Potion_HeadStart_5_BonusCombo = 2;
	public static readonly int Potion_HeadStart_5_BonusScore = 16000;
	public static readonly int Potion_HeadStart_6_BonusCombo = 2;
	public static readonly int Potion_HeadStart_6_BonusScore = 21000;
	public static readonly int Potion_HeadStart_7_BonusCombo = 2;
	public static readonly int Potion_HeadStart_7_BonusScore = 26000;
	public static readonly int Potion_HeadStart_8_BonusCombo = 2;
	public static readonly int Potion_HeadStart_8_BonusScore = 30000;

	#endregion

	// Endless XP Requirements
	public static readonly int Endless_XP_Req_Lvl1 = 0;
	public static readonly int Endless_XP_Req_Lvl2 = 2500;
	public static readonly int Endless_XP_Req_Lvl3 = 10600;
	public static readonly int Endless_XP_Req_Lvl4 = 29800;
	public static readonly int Endless_XP_Req_Lvl5 = 65600;
	public static readonly int Endless_XP_Req_Lvl6 = 123700;
	public static readonly int Endless_XP_Req_Lvl7 = 209600;
	public static readonly int Endless_XP_Req_Lvl8 = 328700;
	public static readonly int Endless_XP_Req_Lvl9 = 486800;
	public static readonly int Endless_XP_Req_Lvl10 = 689400;
	public static readonly int Endless_XP_Req_Lvl11 = 941900;
	public static readonly int Endless_XP_Req_Lvl12 = 1250000;
	public static readonly int Endless_XP_Req_Lvl13 = 1619200;
	public static readonly int Endless_XP_Req_Lvl14 = 2050000;
	public static readonly int Endless_XP_Req_Lvl15 = 2563100;
	public static readonly int Endless_XP_Req_LvlMore = 3200000;

	// Endless XP Requirements (Test Numbers)
//	public static readonly int Endless_XP_Req_Lvl1 = 0;
//	public static readonly int Endless_XP_Req_Lvl2 = 500;
//	public static readonly int Endless_XP_Req_Lvl3 = 800;
//	public static readonly int Endless_XP_Req_Lvl4 = 1200;
//	public static readonly int Endless_XP_Req_Lvl5 = 2000;
//	public static readonly int Endless_XP_Req_Lvl6 = 5000;
//	public static readonly int Endless_XP_Req_Lvl7 = 8000;
//	public static readonly int Endless_XP_Req_Lvl8 = 16000;
//	public static readonly int Endless_XP_Req_Lvl9 = 21000;
//	public static readonly int Endless_XP_Req_Lvl10 = 55000;
//	public static readonly int Endless_XP_Req_Lvl11 = 120000;
//	public static readonly int Endless_XP_Req_Lvl12 = 150000;
//	public static readonly int Endless_XP_Req_Lvl13 = 180000;
//	public static readonly int Endless_XP_Req_Lvl14 = 190000;
//	public static readonly int Endless_XP_Req_Lvl15 = 200000;
//	public static readonly int Endless_XP_Req_LvlMore = 250000;

	// Endless Roadmap Reward Texts
	public static readonly string Endless_RewardText_Egg = "!ﻪﯾﺪﻫ ﻍﺮﻣ ﻢﺨﺗ";
	public static readonly string Endless_RewardText_Damage = "ﺖﺸﻣ ﺕﺭﺪﻗ";
	public static readonly string Endless_RewardText_HP = "ﺐﻠﻗ";
	public static readonly string Endless_RewardText_Crit = "ﻦﯿﮕﻨﺳ ﺖﺸﻣ ﺲﻧﺎﺷ";
	public static readonly string Endless_RewardText_LastStand = "ﺎﻫﺩﮊﺍ ﻢﺸﺧ ﮏﯿﻨﮑﺗ";
	public static readonly string Endless_RewardText_HeadStart = "ﺮﺗﻮﻠﺟ ﺝﻮﻣ ﮏﯾ ﺯﺍ ﻉﻭﺮﺷ";

	// Endless Multiplier Hit Requirements
	public static readonly int Endless_IntComboReq_Lvl1 = 45;
	public static readonly int Endless_IntComboReq_Lvl5 = 55;
	public static readonly int Endless_IntComboReq_Lvl10 = 70;
	public static readonly int Endless_IntComboReq_Lvl15 = 85;
	public static readonly int Endless_IntComboReq_Lvl20 = 100;
	public static readonly int Endless_IntComboReq_Lvl25 = 125;
	public static readonly int Endless_IntComboReq_Lvl30 = 155;
	public static readonly int Endless_IntComboReq_Lvl35 = 185;
	public static readonly int Endless_IntComboReq_LvlInfinite = 200;

	// Endless Multiplier Penalty Requirements
	public static readonly int Endless_intMultiPenalty = 2;

	// Endless Combo Increase Effect
	public static readonly int Endless_ComboMultiIncreaser = 2;
	public static readonly int Endless_ComboKillReward = 5;

	// Endless Egg Mustache Reward (Not Used)
	public static readonly int Endless_EggMustacheReward_Lvl1 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl2 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl3 = 400;
	public static readonly int Endless_EggMustacheReward_Lvl4 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl5 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl6 = 250;
	public static readonly int Endless_EggMustacheReward_Lvl7 = 600;
	public static readonly int Endless_EggMustacheReward_Lvl8 = 250;
	public static readonly int Endless_EggMustacheReward_Lvl9 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl10 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl11 = 600;
	public static readonly int Endless_EggMustacheReward_Lvl12 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl13 = 0;
	public static readonly int Endless_EggMustacheReward_Lvl14 = 600;
	public static readonly int Endless_EggMustacheReward_Lvl15 = 0;
	public static readonly int Endless_EggMustacheReward_LvlMore = 0;
//	public static readonly int Endless_EggMustacheReward = 400;
}
