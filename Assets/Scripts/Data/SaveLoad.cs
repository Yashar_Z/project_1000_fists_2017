﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class SaveLoad  {

    private static readonly int NUMBER_OF_ITEMS_IN_GAME = 60;

    private static PersistentData persistentData = new PersistentData();

    public static void Save()
    {

        if (AchievementManager.Instance != null)
        {
            persistentData.AchivementsArr = AchievementManager.Instance.AchievementsArr;
        }

		if (QuestManager.Instance != null) {
			persistentData.Quests = QuestManager.Instance.Quests;
			persistentData.ActiveQuests = QuestManager.Instance.ActiveQuests;
			persistentData.QueueQuests = QuestManager.Instance.QueueQuests;

			persistentData.LastQuestGivenDate = QuestManager.Instance.LastQuestGivenDate;
		}

		if (PlayerData_Main.Instance != null) {
			persistentData.GameVersion = PlayerData_Main.Instance.GameVersion;

			persistentData.player_WorldsUnlocked = PlayerData_Main.Instance.player_WorldsUnlocked;
			persistentData.player_LevelsUnlocked = PlayerData_Main.Instance.player_LevelsUnlocked;

			persistentData.player_ItemsAmount = PlayerData_Main.Instance.player_ItemsAmount;
			persistentData.player_Mustache = PlayerData_Main.Instance.player_Mustache;
			persistentData.player_Stars = PlayerData_Main.Instance.player_Stars;

			persistentData.player_PowUp1_Own = PlayerData_Main.Instance.player_PowUp1_Own;
			persistentData.player_PowUp2_Own = PlayerData_Main.Instance.player_PowUp2_Own;

			persistentData.player_PowUp1_Ready = PlayerData_Main.Instance.player_PowUp1_Ready;
			persistentData.player_PowUp2_Ready = PlayerData_Main.Instance.player_PowUp2_Ready;

			persistentData.player_Sound_ON = PlayerData_Main.Instance.player_Sound_ON;
			persistentData.player_Music_ON = PlayerData_Main.Instance.player_Music_ON;

			persistentData.player_World1_LevelsDataArr = PlayerData_Main.Instance.player_World1_LevelsDataArr;
			persistentData.player_World2_LevelsDataArr = PlayerData_Main.Instance.player_World2_LevelsDataArr;
			persistentData.player_World3_LevelsDataArr = PlayerData_Main.Instance.player_World3_LevelsDataArr;
			persistentData.player_World4_LevelsDataArr = PlayerData_Main.Instance.player_World4_LevelsDataArr;
			persistentData.player_World5_LevelsDataArr = PlayerData_Main.Instance.player_World5_LevelsDataArr;

			persistentData.worldsGoldStarsArr = PlayerData_Main.Instance.worldsGoldStarsArr;

			persistentData.TutSeen_BatteryCharge = PlayerData_Main.Instance.TutSeen_BatteryCharge;
			persistentData.TutSeen_ItemProgress = PlayerData_Main.Instance.TutSeen_ItemProgress;
			persistentData.TutSeen_ItemRepeatItem = PlayerData_Main.Instance.TutSeen_ItemRepeatItem;
			persistentData.TutSeen_BatterySlot = PlayerData_Main.Instance.TutSeen_BatterySlot;
			persistentData.TutSeen_EndlessUnlock = PlayerData_Main.Instance.TutSeen_EndlessUnlock;

			persistentData.TutSeen_2ndCostume_Fat = PlayerData_Main.Instance.TutSeen_2ndCostume_Fat;
			persistentData.TutSeen_2ndCostume_Thin = PlayerData_Main.Instance.TutSeen_2ndCostume_Thin;
			persistentData.TutSeen_2ndCostume_Muscle = PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle;
			persistentData.TutSeen_2ndCostume_Giant = PlayerData_Main.Instance.TutSeen_2ndCostume_Giant;

			persistentData.Event_NewYear_SawPopup = PlayerData_Main.Instance.Event_NewYear_SawPopup;
			persistentData.Event_NewYear_Registered = PlayerData_Main.Instance.Event_NewYear_Registered;

			persistentData.Show_NewLevel_Allow = PlayerData_Main.Instance.Show_NewLevel_Allow;
			persistentData.Show_NewWorld_Allow = PlayerData_Main.Instance.Show_NewWorld_Allow;
			persistentData.Show_NewWorld_Remain = PlayerData_Main.Instance.Show_NewWorld_Remain;

			persistentData.isVibratingPhone = PlayerData_Main.Instance.isVibratingPhone;

            persistentData.PowUp1_StartCharge_Time = PlayerData_Main.Instance.PowUp1_StartCharge_Time;
            persistentData.PowUp2_StartCharge_Time = PlayerData_Main.Instance.PowUp2_StartCharge_Time;

			persistentData.player_dateVidGiftLast = PlayerData_Main.Instance.player_dateVidGiftLast;

			persistentData.player_XP_Curr = PlayerData_Main.Instance.player_XP_Curr;
			persistentData.player_Score_Record = PlayerData_Main.Instance.player_Score_Record;
			persistentData.player_EndlessLevel = PlayerData_Main.Instance.player_EndlessLevel;
			persistentData.player_Google_PreviouslySignedIn = PlayerData_Main.Instance.player_Google_PreviouslySignedIn;

//			persistentData.boolFutureArr1 = PlayerData_Main.Instance.boolFutureArr1;
//			persistentData.boolFutureArr2 = PlayerData_Main.Instance.boolFutureArr2;
//			persistentData.intFutureArr1 = PlayerData_Main.Instance.intFutureArr1;
//			persistentData.intFutureArr2 = PlayerData_Main.Instance.intFutureArr2;

            BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
			bf.Serialize (file, persistentData);
			file.Close ();

            Debug.Log("SAVED!");
        }
    }

    public static void Load()
    {
        Debug.Log(Application.persistentDataPath);

        if (PlayerData_Main.Instance != null) {
			if (File.Exists (Application.persistentDataPath + "/savedGames.gd")) {
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (Application.persistentDataPath + "/savedGames.gd", FileMode.Open);

				try {
					persistentData = (PersistentData)bf.Deserialize (file);
				} catch (System.Exception e) {
					Debug.LogError (e.Message);
				}

				file.Close ();

				PlayerData_Main.Instance.GameVersion = persistentData.GameVersion;

				PlayerData_Main.Instance.player_WorldsUnlocked = persistentData.player_WorldsUnlocked;
				PlayerData_Main.Instance.player_LevelsUnlocked = persistentData.player_LevelsUnlocked;

				if (persistentData.player_ItemsAmount != null) {
					PlayerData_Main.Instance.player_ItemsAmount = persistentData.player_ItemsAmount;
				}

				PlayerData_Main.Instance.player_Mustache = persistentData.player_Mustache;
				PlayerData_Main.Instance.player_Stars = persistentData.player_Stars;
				PlayerData_Main.Instance.player_PowUp1_Own = persistentData.player_PowUp1_Own;
				PlayerData_Main.Instance.player_PowUp2_Own = persistentData.player_PowUp2_Own;
				PlayerData_Main.Instance.player_PowUp1_Ready = persistentData.player_PowUp1_Ready;
				PlayerData_Main.Instance.player_PowUp2_Ready = persistentData.player_PowUp2_Ready;
				PlayerData_Main.Instance.player_Sound_ON = persistentData.player_Sound_ON;
				PlayerData_Main.Instance.player_Music_ON = persistentData.player_Music_ON;

				// Levels data
				PlayerData_Main.Instance.player_World1_LevelsDataArr = persistentData.player_World1_LevelsDataArr;
				PlayerData_Main.Instance.player_World2_LevelsDataArr = persistentData.player_World2_LevelsDataArr;
				PlayerData_Main.Instance.player_World3_LevelsDataArr = persistentData.player_World3_LevelsDataArr;
				PlayerData_Main.Instance.player_World4_LevelsDataArr = persistentData.player_World4_LevelsDataArr;
				PlayerData_Main.Instance.player_World5_LevelsDataArr = persistentData.player_World5_LevelsDataArr;

				// Calculate the stars
				StarsCalculator ();

//				if (persistentData.worldsGoldStarsArr != null) {
//					PlayerData_Main.Instance.worldsGoldStarsArr = persistentData.worldsGoldStarsArr;
//				}
					
				if (AchievementManager.Instance != null) {
					if (persistentData.AchivementsArr != null) {
						// TODO: Should NOT be comment for proper game
						AchievementManager.Instance.AchievementsArr = persistentData.AchivementsArr;

						// Moved this to end of daily check
//					} else {
//						AchievementManager.Instance.Achievements_CheckForPrev ();
					} 
				}

				if (QuestManager.Instance != null) {
					// TODO: Should NOT be comment for proper game
					if (persistentData.Quests != null) {
						QuestManager.Instance.Quests = persistentData.Quests;
					} 
					if (persistentData.ActiveQuests != null) {
						QuestManager.Instance.ActiveQuests = persistentData.ActiveQuests;
					}
					if (persistentData.QueueQuests != null) {
						QuestManager.Instance.QueueQuests = persistentData.QueueQuests;
					}
					if (persistentData.LastQuestGivenDate != null) {
						QuestManager.Instance.LastQuestGivenDate = persistentData.LastQuestGivenDate;
					} 

//					else {
//					}
				}

				// Version check for first times for quest update
				if (IsNewer_VersionCheck ()) {
					Debug.LogError ("Newer version! " + PlayerData_Main.Instance.GameVersion);
				}

				if (PlayerData_Main.Instance.player_WorldsUnlocked > 1) {
					// This means player is in world 2 or later so all tuts must be seen
					PlayerData_Main.Instance.TutSeen_BatteryCharge = true;
					PlayerData_Main.Instance.TutSeen_ItemProgress = true;
					PlayerData_Main.Instance.TutSeen_ItemRepeatItem = true;
					PlayerData_Main.Instance.TutSeen_BatterySlot = true;
				} else {
					// This means player is still in world 1

					if (PlayerData_Main.Instance.player_LevelsUnlocked > (Balance_Constants_Script.TutLevel_BatteryCharge + 1)) {
						PlayerData_Main.Instance.TutSeen_BatteryCharge = true;
					} else {
						PlayerData_Main.Instance.TutSeen_BatteryCharge = persistentData.TutSeen_BatteryCharge;
					}
						
					if (PlayerData_Main.Instance.player_LevelsUnlocked > (Balance_Constants_Script.TutLevel_ItemProgress + 1)) {
						PlayerData_Main.Instance.TutSeen_ItemProgress = true;
					} else {
						PlayerData_Main.Instance.TutSeen_ItemProgress = persistentData.TutSeen_ItemProgress;
					}

					if (PlayerData_Main.Instance.player_LevelsUnlocked > (Balance_Constants_Script.TutLevel_ItemRepeatItem + 1)) {
						PlayerData_Main.Instance.TutSeen_ItemRepeatItem = true;
					} else {
						PlayerData_Main.Instance.TutSeen_ItemRepeatItem = persistentData.TutSeen_ItemRepeatItem;
					}

					// Didn't remove this always-true check because of the else part.
					if (persistentData.TutSeen_BatterySlot != null) {
						PlayerData_Main.Instance.TutSeen_BatterySlot = persistentData.TutSeen_BatterySlot;
					} 

					// This means player has older version
					else {
						if (PlayerData_Main.Instance.player_LevelsUnlocked > (Balance_Constants_Script.TutLevel_PowUp2Gain + 1)) {
							PlayerData_Main.Instance.TutSeen_BatterySlot = true;
						}
						// This is to make sure remove slot tut for players that have already bought tuts
						//					if ((PlayerData_Main.Instance.player_WorldsUnlocked == 1) && (PlayerData_Main.Instance.player_PowUp2_Own < 2) 
						//						&& (PlayerData_Main.Instance.player_PowUp1_Own < 2)) {
						//						PlayerData_Main.Instance.TutSeen_BatteryCharge = false;
						//					} else {
						//						PlayerData_Main.Instance.TutSeen_BatteryCharge = true;
						//					}
					}
				}

				// For endless tutseen, there is no old check (Needs to be outside world1 check / other tutseens)
				PlayerData_Main.Instance.TutSeen_EndlessUnlock = persistentData.TutSeen_EndlessUnlock;

				// For 2nd costumes
				PlayerData_Main.Instance.TutSeen_2ndCostume_Fat = persistentData.TutSeen_2ndCostume_Fat;
				PlayerData_Main.Instance.TutSeen_2ndCostume_Thin = persistentData.TutSeen_2ndCostume_Thin;
				PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle = persistentData.TutSeen_2ndCostume_Muscle;
				PlayerData_Main.Instance.TutSeen_2ndCostume_Giant = persistentData.TutSeen_2ndCostume_Giant;

				// Event stuff
				PlayerData_Main.Instance.Event_NewYear_SawPopup = persistentData.Event_NewYear_SawPopup;
				PlayerData_Main.Instance.Event_NewYear_Registered = persistentData.Event_NewYear_Registered;
					
				// Show new world and level stuff
				PlayerData_Main.Instance.Show_NewLevel_Allow = persistentData.Show_NewLevel_Allow;
				PlayerData_Main.Instance.Show_NewWorld_Allow = persistentData.Show_NewWorld_Allow;
				PlayerData_Main.Instance.Show_NewWorld_Remain = persistentData.Show_NewWorld_Remain;
				PlayerData_Main.Instance.isVibratingPhone = persistentData.isVibratingPhone;

				if (persistentData.PowUp1_StartCharge_Time != null) {
					PlayerData_Main.Instance.PowUp1_StartCharge_Time = persistentData.PowUp1_StartCharge_Time;
				}
				if (persistentData.PowUp2_StartCharge_Time != null) {
					PlayerData_Main.Instance.PowUp2_StartCharge_Time = persistentData.PowUp2_StartCharge_Time;
				}

//				if (persistentData.boolFutureArr1 != null) {
//					PlayerData_Main.Instance.boolFutureArr1 = persistentData.boolFutureArr1;
//					PlayerData_Main.Instance.boolFutureArr2 = persistentData.boolFutureArr2;
//					PlayerData_Main.Instance.intFutureArr1 = persistentData.intFutureArr1;
//					PlayerData_Main.Instance.intFutureArr2 = persistentData.intFutureArr2;
//				}

                //default value
                if (string.IsNullOrEmpty(PlayerData_Main.Instance.PowUp1_StartCharge_Time))
                    PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                if (string.IsNullOrEmpty(PlayerData_Main.Instance.PowUp2_StartCharge_Time))
                    PlayerData_Main.Instance.PowUp2_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

				if (persistentData.player_dateVidGiftLast != null) {
					PlayerData_Main.Instance.player_dateVidGiftLast = persistentData.player_dateVidGiftLast;
				} else {
					PlayerData_Main.Instance.player_dateVidGiftLast = System.DateTime.Now.AddDays (-1);
				}

                if (PlayerData_Main.Instance.player_ItemsAmount.Count == 0)
                {
                    persistentData.player_ItemsAmount = new List<ItemWithAmount>();
                    for (int i = 0; i < NUMBER_OF_ITEMS_IN_GAME; i++)
                    {
                        ItemWithAmount item = new ItemWithAmount() { ItemID = 101 + i };

                        persistentData.player_ItemsAmount.Add(item);
                    }
                }

				// Endless values
				PlayerData_Main.Instance.player_XP_Curr = persistentData.player_XP_Curr;
				PlayerData_Main.Instance.player_Score_Record = persistentData.player_Score_Record;
				PlayerData_Main.Instance.player_EndlessLevel = persistentData.player_EndlessLevel;
				PlayerData_Main.Instance.player_Google_PreviouslySignedIn = persistentData.player_Google_PreviouslySignedIn;

				if (PlayerData_Main.Instance.player_EndlessLevel == 0) {
					PlayerData_Main.Instance.player_EndlessLevel = 1;
				}

				PlayerData_Main.Instance.PlayingALevelForTheFirstTime = false;

				PlayGamesScript.Instance.AutoSignIn ();
            }
            // Default values and create file
            else
            {
				PlayerData_Main.Instance.player_WorldsUnlocked = 1;
				PlayerData_Main.Instance.player_LevelsUnlocked = 1;

				// This was for building item with amount set
//                List<ItemWithAmount> tempList = new List<ItemWithAmount>();
//                for (int i = 0; i < NUMBER_OF_ITEMS_IN_GAME; i++)
//                {
//                    ItemWithAmount item = new ItemWithAmount() { ItemID = 101 + i };
//
//                    tempList.Add(item);
//                }
//
//                PlayerData_Main.Instance.player_ItemsAmount = tempList;

                PlayerData_Main.Instance.player_Mustache = 0;
				PlayerData_Main.Instance.player_Stars = 0;

				PlayerData_Main.Instance.player_PowUp1_Own = 0;
				PlayerData_Main.Instance.player_PowUp2_Own = 0;

				PlayerData_Main.Instance.player_PowUp1_Ready = 0;
				PlayerData_Main.Instance.player_PowUp2_Ready = 0;

				PlayerData_Main.Instance.player_Sound_ON = true;
				PlayerData_Main.Instance.player_Music_ON = true;

//                LevelData_Class[] levelDataTempArr = new LevelData_Class[10];
//                for (int i = 0; i < levelDataTempArr.Length; i++)
//                {
//                    levelDataTempArr[i] = new LevelData_Class();
//                }
//                PlayerData_Main.Instance.player_World1_LevelsDataArr = levelDataTempArr;
//                PlayerData_Main.Instance.player_World2_LevelsDataArr = levelDataTempArr;
//                PlayerData_Main.Instance.player_World3_LevelsDataArr = levelDataTempArr;
//                PlayerData_Main.Instance.player_World4_LevelsDataArr = levelDataTempArr;
//                PlayerData_Main.Instance.player_World5_LevelsDataArr = levelDataTempArr;

                PlayerData_Main.Instance.TutSeen_BatteryCharge = false;
				PlayerData_Main.Instance.TutSeen_ItemProgress = false;
				PlayerData_Main.Instance.TutSeen_ItemRepeatItem = false;
				PlayerData_Main.Instance.TutSeen_BatterySlot = false;
				PlayerData_Main.Instance.TutSeen_EndlessUnlock = false;

				PlayerData_Main.Instance.TutSeen_2ndCostume_Fat = false;
				PlayerData_Main.Instance.TutSeen_2ndCostume_Thin = false;
             	PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle = false;
				PlayerData_Main.Instance.TutSeen_2ndCostume_Giant = false;

				PlayerData_Main.Instance.Event_NewYear_SawPopup = false;
				PlayerData_Main.Instance.Event_NewYear_Registered = false;

				PlayerData_Main.Instance.Show_NewLevel_Allow = false;
				PlayerData_Main.Instance.Show_NewWorld_Allow = false;
				PlayerData_Main.Instance.Show_NewWorld_Remain = false;

				PlayerData_Main.Instance.isVibratingPhone = false;

                PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                PlayerData_Main.Instance.PowUp2_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

				PlayerData_Main.Instance.player_dateVidGiftLast = System.DateTime.Now.AddDays (-1);

				PlayerData_Main.Instance.PlayingALevelForTheFirstTime = true;
            }
		}
        Debug.Log("LOADED!");
    }

	public static void Load_VAS_CheatOnly() {
		persistentData.GameVersion = PlayerData_Main.Instance.GameVersion;

		PlayerData_Main.Instance.player_WorldsUnlocked = 3;
		PlayerData_Main.Instance.player_LevelsUnlocked = 10;
		PlayerData_Main.Instance.player_Mustache = 250000;

		PlayerData_Main.Instance.player_PowUp1_Own = 4;
		PlayerData_Main.Instance.player_PowUp2_Own = 4;

		PlayerData_Main.Instance.player_PowUp1_Ready = 4;
		PlayerData_Main.Instance.player_PowUp2_Ready = 4;

		PlayerData_Main.Instance.TutSeen_BatteryCharge = true;
		PlayerData_Main.Instance.TutSeen_ItemProgress = true;
		PlayerData_Main.Instance.TutSeen_ItemRepeatItem = true;
		PlayerData_Main.Instance.TutSeen_BatterySlot = true;
		PlayerData_Main.Instance.TutSeen_EndlessUnlock = true;
	}

	private static void StarsCalculator () {
		PlayerData_Main.Instance.player_Stars = 0;
		PlayerData_Main.Instance.worldsGoldStarsArr [0] = 0;
		PlayerData_Main.Instance.worldsGoldStarsArr [1] = 0;
		PlayerData_Main.Instance.worldsGoldStarsArr [2] = 0;
		PlayerData_Main.Instance.worldsGoldStarsArr [3] = 0;
		PlayerData_Main.Instance.worldsGoldStarsArr [4] = 0;

		for (int i = 0; i < 10; i++) {
			// World 1
			if (PlayerData_Main.Instance.player_World1_LevelsDataArr [i].star_Unlocked_Level) {
				PlayerData_Main.Instance.worldsGoldStarsArr [0]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World1_LevelsDataArr [i].star_Unlocked_Unmissable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [0]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World1_LevelsDataArr [i].star_Unlocked_Untouchable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [0]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			// World 2
			if (PlayerData_Main.Instance.player_World2_LevelsDataArr [i].star_Unlocked_Level) {
				PlayerData_Main.Instance.worldsGoldStarsArr [1]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World2_LevelsDataArr [i].star_Unlocked_Unmissable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [1]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World2_LevelsDataArr [i].star_Unlocked_Untouchable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [1]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			// World 3
			if (PlayerData_Main.Instance.player_World3_LevelsDataArr [i].star_Unlocked_Level) {
				PlayerData_Main.Instance.worldsGoldStarsArr [2]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World3_LevelsDataArr [i].star_Unlocked_Unmissable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [2]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			if (PlayerData_Main.Instance.player_World3_LevelsDataArr [i].star_Unlocked_Untouchable) {
				PlayerData_Main.Instance.worldsGoldStarsArr [2]++;
				PlayerData_Main.Instance.player_Stars++;
			}

			// World 4
			//			if (PlayerData_Main.Instance.player_World4_LevelsDataArr [i].star_Unlocked_Level) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [3]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}
			//
			//			if (PlayerData_Main.Instance.player_World4_LevelsDataArr [i].star_Unlocked_Unmissable) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [3]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}
			//
			//			if (PlayerData_Main.Instance.player_World4_LevelsDataArr [i].star_Unlocked_Untouchable) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [3]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}

			// World 5
			//			if (PlayerData_Main.Instance.player_World5_LevelsDataArr [i].star_Unlocked_Level) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [4]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}
			//
			//			if (PlayerData_Main.Instance.player_World5_LevelsDataArr [i].star_Unlocked_Unmissable) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [4]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}
			//
			//			if (PlayerData_Main.Instance.player_World5_LevelsDataArr [i].star_Unlocked_Untouchable) {
			//				PlayerData_Main.Instance.worldsGoldStarsArr [4]++;
			//				PlayerData_Main.Instance.player_Stars++;
			//			}

			//			Debug.LogWarning ("stage " + i + " stars = " + PlayerData_Main.Instance.player_Stars);
		}
	}

	private static bool IsNewer_VersionCheck () {
		int versionBufferInt = System.Convert.ToInt32(System.Text.RegularExpressions.Regex.Replace(Application.version, "[^0-9]", "" ));
		if (PlayerData_Main.Instance.GameVersion < versionBufferInt) {
			PlayerData_Main.Instance.GameVersion = versionBufferInt;
			return true;
		} else {
			return false;
		}
	}
}
