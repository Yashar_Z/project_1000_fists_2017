﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;

public class EndlessSpawner : MonoBehaviour
{
	public Worlds_InGame_Script worldsInGameScriptHolder;
	public StoryMain_Script storyMainScriptHolder;
	public Bell_Script bellScriptHolder;

    //attribute haye marboot be blockhayee ke gharar ast az dakhele marahel joda she
    private readonly int MINIMUM_LEVEL_TO_CHOOSE_FROM = 5; // kamtarin marahale ke block az an ebtekhab mishe masalan 1-1 avalin marahale ast
    private readonly int MAXIMUM_LEVEL_TO_CHOOSE_FROM = 24; //akharin marahle ghabele entekhab

    private readonly int MINIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL = 12;
	private readonly int MAXIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL =16;
	private readonly int MID_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL =15;

    private readonly int EXTEND_CURRENT_BRACKET_FOR_GET_HP = 30; // in adad ezafe beshe baze i ke player damage tosh gharar gerefte

    private readonly int GLOBAL_DETEMINER = 50; // manzoor hamon n hast

    private float timeOfNextSpawn;
    int enemyIndex;

    List<Enemy> enemyList;

	[HideInInspector]
	public Dictionary <Enemy.EnemyType, int> dicEnemyScoresTable;

	// For allowing Update to continue in Endless spawner
	[HideInInspector]
	public bool endWave_EnemiesFinished;

	// For Enemies spawn here trans and de-activate enemies source / parent trans
	public Transform transDeactivateEnemyParent;
	public Transform enemysParentTransHolder;

	// Levels (Real) Source
	public Transform levelsHolder;

    // Enemies - Human
    public GameObject EnemyObject_Comm_Fat;
    public GameObject EnemyObject_Comm_Thin;
    public GameObject EnemyObject_Comm_Muscle;
    public GameObject EnemyObject_Comm_Giant;
    public GameObject EnemyObject_HF;
    public GameObject EnemyObject_Barrel;

    // Enemies - Items
    public GameObject EnemyObject_Pipe;
    public GameObject EnemyObject_Elixir;
    public GameObject EnemyObject_Freeze;
    public GameObject EnemyObject_Heart;
    public GameObject EnemyObject_Hedgehog;
    public GameObject EnemyObject_Mustache;

    public GameObject EnemyObject_SuperMustache;
	public GameObject EnemyObject_BigBomb;
	public GameObject EnemyObject_LineBomb;

	public GameObject EnemyObject_Score_Item;
	public GameObject EnemyObject_SuperScore;
	public GameObject EnemyObject_PowerUp_Fist;
	public GameObject EnemyObject_PowerUp_Shield;

//	public UnityEngine.UI.Text textEnemyIndex;

	// Comes from endless_potionscontroller script (The holder in there: endlessSpawnerHolder)
	public int intHeadStart_Count;
	public int intEnemyEndOfWaveIndex; 

	private List<int> intEnemyEndOfWaveArr; 
	private int intTempWaveCounter;

	// This counter is a buffer for the result of headstarts * wave_per_headstart
	private int intHeadStartBuffer;

	private float delaySpawnTime;
	private float additionalIntroWait;

	private int bellCounter;

    private Transform currentLevelTrans;
    float afterEachBlockTime;

    LevelManifestUniversalData universalData;
    private bool isProcessing;

    int baze1;
    int baze2;
    int baze3;
    int baze4;
    int baze5;
    int baze6;
    int baze7;
    int baze8;
    int baze9;

	void Start () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless)
		{
			Start_EndlessPreparation();
		}
		else
		{
			this.enabled = false;
		}
	}

    void Start_EndlessPreparation () {
        int playerDamage = PlayerData_InGame.Instance.p1_Damage_Base;

//        Debug.Log("player damage" + playerDamage);

        int amountToDistributeToOtherBrackets;

        //bar asas e damage player 
        switch (playerDamage)
        {
            case 1:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * EXTEND_CURRENT_BRACKET_FOR_GET_HP) / 100;
                break;
            case 2:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * EXTEND_CURRENT_BRACKET_FOR_GET_HP + 10) / 100;
                break;
            case 3:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 20)) / 100;
                break;
            case 4:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 30)) / 100;
                break;
            case 5:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 40)) / 100;
                break;
            case 6:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 50)) / 100;
                break;
            case 7:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 60)) / 100;
                break;
            case 8:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 70)) / 100;
                break;
            case 9:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 80)) / 100;
                break;
            default:
                amountToDistributeToOtherBrackets = (GLOBAL_DETEMINER * (EXTEND_CURRENT_BRACKET_FOR_GET_HP + 90)) / 100;
                break;
        }

        switch (playerDamage)
        {
            case 1:
                baze1 = GLOBAL_DETEMINER ;
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4));
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4));
                break;
            case 2:
                baze1 = GLOBAL_DETEMINER  - amountToDistributeToOtherBrackets;
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4));
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4));
                break;
            case 3:
                baze1 = GLOBAL_DETEMINER - (2*(amountToDistributeToOtherBrackets / 3));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 3);
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4));
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4));
                break;
            case 4:
//                Debug.Log("extended baze 4 by " + amountToDistributeToOtherBrackets);
                baze1 = GLOBAL_DETEMINER - (3 * (amountToDistributeToOtherBrackets / 6));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (2 * (amountToDistributeToOtherBrackets / 6));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 6);
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4));
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4));
                break;
            case 5:
//                Debug.Log("extended baze 5 by " + amountToDistributeToOtherBrackets);
                baze1 = GLOBAL_DETEMINER - (4*(amountToDistributeToOtherBrackets / 10));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (3*(amountToDistributeToOtherBrackets / 10));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (2*(amountToDistributeToOtherBrackets / 10));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 10);
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4));
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4));
                break;
            case 6:
//                Debug.Log("extended bazev 6 by " + amountToDistributeToOtherBrackets);
                baze1 = GLOBAL_DETEMINER - (5 * (amountToDistributeToOtherBrackets / 15));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (4 * (amountToDistributeToOtherBrackets / 15));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (3 * (amountToDistributeToOtherBrackets / 15));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (2* (amountToDistributeToOtherBrackets / 15));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 15);
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                break;
            case 7:
                baze1 = GLOBAL_DETEMINER - (6 * (amountToDistributeToOtherBrackets / 21));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (5 * (amountToDistributeToOtherBrackets / 21));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (4 * (amountToDistributeToOtherBrackets / 21));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (3 * (amountToDistributeToOtherBrackets / 21));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) - (2 * (amountToDistributeToOtherBrackets / 21));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 21);
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                break;
            case 8:
                baze1 = GLOBAL_DETEMINER - (7 * (amountToDistributeToOtherBrackets / 28));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (6 * (amountToDistributeToOtherBrackets / 28));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (5 * (amountToDistributeToOtherBrackets / 28));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (4 * (amountToDistributeToOtherBrackets / 28));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) - (3 * (amountToDistributeToOtherBrackets / 28));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4)) - (2 * (amountToDistributeToOtherBrackets / 28));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 28);
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                break;
            case 9:
//                Debug.Log("extended baze 9 by " + amountToDistributeToOtherBrackets);
                baze1 = GLOBAL_DETEMINER - (8 * (amountToDistributeToOtherBrackets / 36));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (7 * (amountToDistributeToOtherBrackets / 36));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (6 * (amountToDistributeToOtherBrackets / 36));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (5 * (amountToDistributeToOtherBrackets / 36));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) - (4 * (amountToDistributeToOtherBrackets / 36));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4)) - (3 * (amountToDistributeToOtherBrackets / 36));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4)) - (2 * (amountToDistributeToOtherBrackets / 36));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 36);
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                break;
            default:
//                Debug.Log("extended baze 10+ by " + amountToDistributeToOtherBrackets);
                baze1 = GLOBAL_DETEMINER - (8 * (amountToDistributeToOtherBrackets / 36));
                baze2 = baze1 + (4 * (GLOBAL_DETEMINER / 4)) - (7 * (amountToDistributeToOtherBrackets / 36));
                baze3 = baze2 + (4 * (GLOBAL_DETEMINER / 4)) - (6 * (amountToDistributeToOtherBrackets / 36));
                baze4 = baze3 + (4 * (GLOBAL_DETEMINER / 4)) - (5 * (amountToDistributeToOtherBrackets / 36));
                baze5 = baze4 + (4 * (GLOBAL_DETEMINER / 4)) - (4 * (amountToDistributeToOtherBrackets / 36));
                baze6 = baze5 + (4 * (GLOBAL_DETEMINER / 4)) - (3 * (amountToDistributeToOtherBrackets / 36));
                baze7 = baze6 + (4 * (GLOBAL_DETEMINER / 4)) - (2 * (amountToDistributeToOtherBrackets / 36));
                baze8 = baze7 + (4 * (GLOBAL_DETEMINER / 4)) - (amountToDistributeToOtherBrackets / 36);
                baze9 = baze8 + (4 * (GLOBAL_DETEMINER / 4)) + amountToDistributeToOtherBrackets;
                Debug.Log("default switch");
                break;
        }

//        Debug.LogError("amountToDistributeToOtherBrackets" + amountToDistributeToOtherBrackets);
//        Debug.LogError("baze 1 " + baze1 + " baze 2 " + baze2 + " baze 3 " + baze3 + " baze 4 " + baze4 + " baze 5 " + baze5 + " baze 6 " + baze6 + " baze 7 " + baze7 + " baze 8 " + baze8 + " baze 9 " + baze9);

		Start_Endless();
	}

	public IEnumerator GameplayGesture_Allow (float delay) {
		yield return new WaitForSeconds (delay);
		PlayerData_InGame.Instance.gameplayGestureAllowed = true;
	}

	public IEnumerator GameplayGesture_Prevent () {
		yield return null;
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
	}

	public bool IsEnemyEmpty_Checker () {
		if (enemysParentTransHolder.childCount == 0) {
			return true;
		} 
		else {
			return false;
		}
	}

    void Start_Endless ()
    {
		intTempWaveCounter = 0;

		intEnemyEndOfWaveArr = new List<int> ();

		dicEnemyScoresTable = new Dictionary<Enemy.EnemyType, int> ();
		dicEnemyScoresTable.Add (Enemy.EnemyType.Fat1, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Fat2, 15);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Thin1, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Thin2, 20);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Muscle1, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Muscle2, 25);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Giant1, 75);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Giant2, 100);
		dicEnemyScoresTable.Add (Enemy.EnemyType.HF1, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.HF2, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.HF_X1, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.HF_X2, 10);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel1, 20);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel2, 20);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel_X1, 20);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel_X2, 20);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel_Q1, 25);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Barrel_Q2, 25);

		dicEnemyScoresTable.Add (Enemy.EnemyType.Pipe, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Elixir, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Freeze, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Heart, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Hedgehog, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Mustache, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Random_Item, 0);

		dicEnemyScoresTable.Add (Enemy.EnemyType.SuperMustache, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.BigBomb, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.LineBomb, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.Score_Item, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.SuperScore, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.PowerUp_Fist, 0);
		dicEnemyScoresTable.Add (Enemy.EnemyType.PowerUp_Shield, 0);

		// Hide enemy source trans obj (Previously in enemymaker test)
		transDeactivateEnemyParent.gameObject.SetActive (false);

		bellCounter = 0;

		// Allow gesture (Moved here from InGameController) BUT Instead use the one in endless potions controller

		if (PlayerData_Main.Instance != null) {
			additionalIntroWait = PlayerData_Main.Instance.introAdditionTime;
		}

        universalData = new LevelManifestUniversalData();
        enemyList = new List<Enemy>();
        enemyIndex = 0;
        afterEachBlockTime = 0;

		// To prevent spawning before finishing potions (Moved this to endless potions controller)
//		endWave_EnemiesFinished = true;

        
		// Moved to headstart to prevent 1 additional next block for headstarts
//		GetUniversalData();
//      StartCoroutine(AddNextBlock());

		if (enemyIndex > 0)
			timeOfNextSpawn = enemyList[enemyIndex-1].enemySpawnTime;

		if (PlayerData_Main.Instance != null) {
			// Enable ding
			BellDing_Enabler ();

			// Call from loading!!!
		} else {
			SpawnsSetup ();

			// Enable ding for playing without playermain
			bellScriptHolder.canDoDingOnly = true;
		}

		// Set to activate World 1
		worldsInGameScriptHolder.intWorldActive_Endless = 1;

		if (worldsInGameScriptHolder.intWorldActive_Endless == 4) {
			PlayerData_InGame.Instance.worldsInGameScriptHolder.World_Activate (0);
			worldsInGameScriptHolder.intWorldActive_Endless = 1;
		} else {
			PlayerData_InGame.Instance.worldsInGameScriptHolder.World_Activate (3);
			worldsInGameScriptHolder.intWorldActive_Endless = 4;
		}

		// At the end of endless start, give values to combo kill reward
		StartCoroutine (SetKillReward_BasedOnWave ());
    }

	public void Start_NormalORHeadstart () {
//		Debug.LogError ("BEFORE intHeadStart_Count = " + intHeadStart_Count);

		// In case there was NO headstart (This was moved from the start_endless)
		if (intHeadStart_Count == 0) {
			GetUniversalData ();
			StartCoroutine (AddNextBlock ());
		} 

		// In case there was headstart
		else {
			intHeadStartBuffer = intHeadStart_Count * Balance_Constants_Script.Potion_WAVE_PER_HEADSTART_POTION;

			for (int i = 0; i < (intHeadStartBuffer * MID_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL); i++) {
				// Add fake new enemies to enemyList
				Enemy fakeEnemy = new Enemy ();

				fakeEnemy.enemyType = Enemy.EnemyType.Fat1;
				fakeEnemy.enemyMoveSpeed = 2;
				fakeEnemy.enemyHP = FixEnemyHealth ("", 1, "U");
				fakeEnemy.enemyLane = Enemy.LaneTypes.Lane_1;

				fakeEnemy.enemySpawnTime = 1;

				enemyList.Add (fakeEnemy);
				enemyIndex++;
			}

			afterEachBlockTime = enemyList [enemyIndex - 1].enemySpawnTime;

			// The additional, new block
			GetUniversalData ();
			StartCoroutine (AddNextBlock ());

			// Instead of using divide by 3, use the base count in the next method
			// Divide by 3 is because each time potion effect is implement, the count increases by 3 due to each reward arr number that is 3 (due to info display reasons)
//			bellScriptHolder.intWaveEndlessCounter += (intHeadStartBuffer / 3);

			// Update Wave banner messgage
			bellScriptHolder.intWaveEndlessCounter += (intHeadStart_Count);

			// Decrease is for the start addition (Don't use it yet)
//			bellScriptHolder.intWaveEndlessCounter--;
		}
	}

	public void BellDing_Enabler () {
		if (!PlayerData_Main.Instance.isFirstTimePlaying) {
			bellScriptHolder.canDoDingOnly = true;
		}
	}

	public IEnumerator NewWave_BackAndBanner () {
		timeOfNextSpawn -= 4;

		yield return new WaitForSeconds (12);
		StartCoroutine (Endless_ChangeBackground ());

		yield return new WaitForSeconds (4);
		StartCoroutine (BellAndBanner_Call ());
	}

	public IEnumerator BellAndBanner_Call () {
		yield return null;
		bellScriptHolder.BellAnim_Call_Delayed (0, false);
	}

    public void GetUniversalData()
    {
        // HF Clingy Data
        PlayerData_InGame.Instance.P1_HF_ClingyHP = GetEnemyStickyHealth();

        int expolisonDamage = GetEnemyExplosionDamage();

        // HF Explosive Data
        PlayerData_InGame.Instance.P1_HF_ExplosionDamage = expolisonDamage;

        // To prevent Explode null on events
        if (PlayerData_InGame.Instance.P1_HF_ExplosionDamage == 0)
        {
            PlayerData_InGame.Instance.P1_HF_ExplosionDamage = 5;
        }

        // Barrel data
        PlayerData_InGame.Instance.P1_Barrel_Q_ExplodeDamage = expolisonDamage;
        PlayerData_InGame.Instance.P1_Barrel_Q_HedgehogDamage = 3;
        PlayerData_InGame.Instance.P1_Barrel_X_ExplodeDamage = expolisonDamage;
        PlayerData_InGame.Instance.P1_Barrel_N_HeadUpTime = 1.5f;// universalData.barrelVulnerableTime;
        PlayerData_InGame.Instance.P1_Barrel_Q_HeadUpTime = 1.7f; //universalData.barrelQVulnerableTime;
        PlayerData_InGame.Instance.P1_Barrel_X_HeadUpTime = 1.4f;// universalData.barrelXVulnerableTime;
        PlayerData_InGame.Instance.missesThresholdPercent = 80;
    }
    private List<Enemy> RetrieveABlock(int fullLvl)
    {
        List<Enemy> fullLevellist;
        List<Enemy> subLevelList;

        int selectedWorld;
        int selectedLevel;

        ConvertFullLevelToWorldLevels(fullLvl - 1, out selectedWorld, out selectedLevel);
        //Debug.Log("selectedWorld" + selectedWorld + "  " + "selectedLevel" + selectedLevel);


        currentLevelTrans = levelsHolder.GetChild(selectedWorld).GetChild(selectedLevel);
       
        //build manifest az level trans
        // TODO: build manifest kolan biad to hamin script
        fullLevellist = currentLevelTrans.GetComponent<LevelManifest>().BuildManifest(currentLevelTrans);
        //universalData = currentLevelTrans.GetComponent<LevelManifest>().GetLevelManifestData(currentLevelTrans);

        // har 3 ta block bayad 1 block az last wave ha biad
        if (intTempWaveCounter % 3 == 0 && intTempWaveCounter != 0)
        {
            LevelEnemies levelHolder = currentLevelTrans.GetComponent<LevelEnemies>();
            int lastWaveStartIndex = levelHolder.Wave[levelHolder.Wave.Length - 1];
            int lastWaveCount = levelHolder.enemyList.Count - lastWaveStartIndex;

            if (lastWaveCount >= MAXIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL)
            {
                lastWaveCount = MAXIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL;
            }

            subLevelList = fullLevellist.GetRange(lastWaveStartIndex,lastWaveCount);

            float firstEnemyTime = subLevelList[0].enemySpawnTime;

            foreach (var e in subLevelList)
            {
                e.enemySpawnTime -= firstEnemyTime - fullLevellist[0].enemySpawnTime;
            }

			// Mohammad Endless 
//            Debug.LogError("level number " + (selectedWorld + 1).ToString() + "-" + (selectedLevel + 1).ToString() + " start with enemy " + lastWaveStartIndex);
        }
        else
        {
            //clip az enemy ha ye entekhab shode.. block aval:
            //adad randomi beyne minimum va maximum tedad enemy ha dar biad.
            // check kone ke in adad az kole tedad enemy ha bishtar nabashe
            //adad randomi az 0 ta count - tedad mored nazar entekhab she
            //list jadid enemy ke clip shode az list asli hast dar biad
            int numberOfEnemyInBlock = new System.Random().Next(MINIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL, MAXIMUM_NUMBER_OF_ENMEY_CHOSEN_FROM_LEVEL);
            int firstEnemyIndex = 0;
            if (numberOfEnemyInBlock < fullLevellist.Count)
            {
                firstEnemyIndex = new System.Random().Next(0, fullLevellist.Count - numberOfEnemyInBlock);
                subLevelList = fullLevellist.GetRange(firstEnemyIndex, numberOfEnemyInBlock);
                float firstEnemyTime = subLevelList[0].enemySpawnTime;

                foreach (var e in subLevelList)
                {
                    e.enemySpawnTime -= firstEnemyTime - fullLevellist[0].enemySpawnTime;
                }
//				Debug.LogError ("GOOD Enemy Spawner numberOfEnemyInBlock: " + numberOfEnemyInBlock + "     fullLevellist.Count: " + fullLevellist.Count);
            }
            else
            {
//				Debug.LogError ("BAD Enemy Spawner numberOfEnemyInBlock: " + numberOfEnemyInBlock + "     fullLevellist.Count: " + fullLevellist.Count);
                subLevelList = null;
            }

			// Mohammad Endless
//            Debug.Log("level number " + (selectedWorld + 1).ToString() + "-" + (selectedLevel + 1).ToString() + " start with enemy " + firstEnemyIndex);

        }
        return subLevelList;
    }

    private void ConvertFullLevelToWorldLevels(int fullLevel, out int sWorld, out int sLevel)
    {
        sWorld = fullLevel / 10;
        sLevel = fullLevel % 10;
    }

	// Yashar (Should be the same in Endless)
	public void SpawnsSetup () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			delaySpawnTime = PlayerData_InGame.Instance.introAnimLength + additionalIntroWait + 1;
		} else {
			delaySpawnTime = 1;
		}

		// This should not be here (Instead using endless potions controller)
//		endWave_EnemiesFinished = false;

		timeOfNextSpawn += -1.2F - delaySpawnTime;
	}

    IEnumerator SpawnThis(GameObject newObj, int enemyNumber)
    {
        GameObject newEnemy = Instantiate(newObj);
        Enemy newEnemyBuffer = newEnemy.GetComponent<Enemy>();

        newEnemyBuffer.enemyType = enemyList[enemyIndex].enemyType;
        //spawntime dar update estefade mishavad
        newEnemyBuffer.enemyMustache = enemyList[enemyIndex].enemyMustache;
        newEnemyBuffer.enemyHP = enemyList[enemyIndex].enemyHP;
        newEnemyBuffer.enemyDamage = enemyList[enemyIndex].enemyDamage;
        newEnemyBuffer.enemyLane = enemyList[enemyNumber].enemyLane;
        newEnemyBuffer.enemyMoveSpeed = enemyList[enemyNumber].enemyMoveSpeed;
        newEnemyBuffer.explosionDamage = enemyList[enemyIndex].explosionDamage;
        newEnemyBuffer.itemValue = enemyList[enemyIndex].itemValue;
//		newEnemyBuffer.enemyScore = enemyList[enemyIndex].enemyScore;
		newEnemyBuffer.enemyScore = dicEnemyScoresTable [enemyList[enemyIndex].enemyType];

        newEnemy.transform.SetParent(enemysParentTransHolder);

        switch (enemyList[enemyNumber].enemyType)
        {
            case Enemy.EnemyType.Fat1:
            case Enemy.EnemyType.Fat2:
            case Enemy.EnemyType.Thin1:
            case Enemy.EnemyType.Thin2:
            case Enemy.EnemyType.Muscle1:
            case Enemy.EnemyType.Muscle2:
            case Enemy.EnemyType.Giant1:
            case Enemy.EnemyType.Giant2:
                StartCoroutine(newEnemy.GetComponent<EnemyMover_Common>().ActiveDelay());
                break;
            case Enemy.EnemyType.HF1:
            case Enemy.EnemyType.HF2:
            case Enemy.EnemyType.HF_X1:
            case Enemy.EnemyType.HF_X2:
                StartCoroutine(newEnemy.GetComponent<EnemyMover_HF>().ActiveDelay());
                break;
            case Enemy.EnemyType.Barrel1:
            case Enemy.EnemyType.Barrel2:
            case Enemy.EnemyType.Barrel_X1:
            case Enemy.EnemyType.Barrel_X2:
            case Enemy.EnemyType.Barrel_Q1:
            case Enemy.EnemyType.Barrel_Q2:
                StartCoroutine(newEnemy.GetComponent<EnemyMover_Barrel>().ActiveDelay());
                break;
            case Enemy.EnemyType.Heart:
            case Enemy.EnemyType.Pipe:
            case Enemy.EnemyType.Hedgehog:
            case Enemy.EnemyType.Mustache:
            case Enemy.EnemyType.Elixir:
            case Enemy.EnemyType.Freeze:
            case Enemy.EnemyType.Random_Item:
			
			case Enemy.EnemyType.SuperMustache:
			case Enemy.EnemyType.BigBomb:
			case Enemy.EnemyType.LineBomb:
			case Enemy.EnemyType.Score_Item:
			case Enemy.EnemyType.SuperScore:
			case Enemy.EnemyType.PowerUp_Fist:
			case Enemy.EnemyType.PowerUp_Shield:
                StartCoroutine(newEnemy.GetComponent<EnemyMover_Item>().ActiveDelay());
                break;
            default:
                Debug.LogError("default value");
                break;
        }
        yield return null;
    }

    //tasmim giri baraye inke in block az enemy ha baese overlap shodan ba block ghabli mishe:
    // akharin enemy az har lane cheghad tool mikeshe
    // avalin enemy to har lane key be entehaye khat mirese
    // ekhtelaf in 2 adad dar tamame lane ha bayad az ye adadi bozorgtar bashe


    //IEnumerator AddNextBlock()
    //{
    //    isProcessing = true;
    //    yield return null;
    //    //avalin level i ke gharar ast enemy ha az an entekhab shavand


    //    //Debug.LogWarning("start of process" + DateTime.Now.ToLongTimeString());

    //    var tempList = RetrieveABlock(new System.Random().Next(MINIMUM_LEVEL_TO_CHOOSE_FROM, MAXIMUM_LEVEL_TO_CHOOSE_FROM));

    //    foreach (var enemy in tempList)
    //    {
    //        enemy.enemySpawnTime += afterEachBlockTime;
    //    }
    //    yield return null;
    //    StartCoroutine(isValidNextBlock(enemyList, tempList, () =>
    //     {
    //         foreach (var enemy in tempList)
    //         {
    //             Enemy e = SetEnemyAttributes(enemy);
    //             enemyList.Add(e);
    //         }
    //         afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime;
    //         Moshtan_Utilties_Script.ShowToast(enemyList.Count.ToString());
    //         isProcessing = false;
    //     }
    //    ,
    //    () =>
    //    {
    //        Debug.Log("fail next block");
    //        isProcessing = false;
    //    }
    //    ));
    //    //Debug.LogWarning("end of process" + DateTime.Now.ToLongTimeString());


    //}

    IEnumerator AddNextBlock()
    {
        isProcessing = true;
        yield return null;
        //avalin level i ke gharar ast enemy ha az an entekhab shavand


        //Debug.LogWarning("start of process" + DateTime.Now.ToLongTimeString());
        int minimumLevel =MINIMUM_LEVEL_TO_CHOOSE_FROM ;
        int maximumLevel;
        // shoro az mimumum level koli hast ta 1-7

        maximumLevel = 7;
        float minimumLevelDeterminer = ((float)enemyIndex / GLOBAL_DETEMINER);
        // 0.8n
        if (minimumLevelDeterminer > 0.8)
        {
            minimumLevel = 5;
            maximumLevel = 9;
        }
        // 1.2n 
        if (minimumLevelDeterminer > 1.2)
        {
            minimumLevel = 7;
            maximumLevel = 12;
        }
        // 1.3n 
        if (minimumLevelDeterminer > 1.3)
        {
            minimumLevel = 7;
            maximumLevel = 13;
        }
        //1.5 n
        if (minimumLevelDeterminer > 1.5)
        {
            minimumLevel = 8;
            maximumLevel = 15;
        }
        //1.9n
        if (minimumLevelDeterminer > 1.9)
        {
            minimumLevel = 9;
            maximumLevel = 16;
        }
        //2n
        if (minimumLevelDeterminer > 2)
        {
            minimumLevel = 11;
            maximumLevel = 20;
        }
        //2.5 n 
        if (minimumLevelDeterminer > 2.5)
        {
            minimumLevel = 13;
            maximumLevel = 23;
        }
        //3 n 
        if (minimumLevelDeterminer > 3 )
        {
            minimumLevel = 13;
            maximumLevel = 25;
        }
        //3.6n
        if (minimumLevelDeterminer > 3.6)
        {
            minimumLevel = 14;
            maximumLevel = 30;
        }

		// TODO: Uncomment after releasing world 4
        // 5 n
//        if (minimumLevelDeterminer > 5)
//        {
//            minimumLevel = 16;
//            maximumLevel = 33;
//        }
//        // 6 n
//        if (minimumLevelDeterminer > 6)
//        {
//            minimumLevel = 17;
//            maximumLevel = 36;
//        }
//        // 6 n
//        if (minimumLevelDeterminer > 6)
//        {
//            minimumLevel = 19;
//            maximumLevel = 40;
//        }

        //        Debug.Log("maximum level" + maximumLevel);

		int intRandInt = new System.Random().Next(minimumLevel, maximumLevel);

		// To make sure level 3-5 (25) is not called here for endless
		if (intRandInt == 25) {
			intRandInt = 24;
		}

		var tempList = RetrieveABlock(intRandInt);

        //lane ha ro shift kon adad 1-2-3 random select mishe be tedad on shift mishe jolo
        //masalan agar 2 biad (lane 2 mishe 4) va (lane 3 mishe 1)

        int shiftForwardLanes = UnityEngine.Random.Range(1, 4);

        foreach (var enemy in tempList)
        {
            // swap lanes of enemy
            enemy.enemyLane = SwapEnemyLane(enemy.enemyLane, shiftForwardLanes);
            enemy.enemySpawnTime += afterEachBlockTime;
        }

        foreach (var enemy in tempList)
        {
            Enemy e = SetEnemyAttributes(enemy);
            enemyList.Add(e);
        }

        // item intermission bad az har wave miad
        //mitoone yeki az item haye mamole bazi bashe
        //mitoone super sibil bashe ya chizaye dige mokhtas endless
        var randInt = UnityEngine.Random.Range(0, 100);
        // 3 ta dar mion super sibil bashe (The comment status of the next 2 lines should flip for test or fullbuild)
        if (intTempWaveCounter % 3 == 0 && intTempWaveCounter != 0)
        {
			// az intTempWaveCounter be onvan shomare wave estefade shode
            Enemy intermissionItem;
            intermissionItem = GetIntermissionItem(Enemy.EnemyType.SuperMustache);
            enemyList.Add(intermissionItem);
            afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime + intermissionItem.enemyMoveSpeed ;
        }

        // tebghe darsad item ha va pity system
        // baghie wave ha 45% chance inke item haye mamoli biad
        //be gheir az magnet , freeze , hedgehog , big bomb

        else if (randInt <= 45)
        {
            Enemy intermissionItem;
            Enemy.EnemyType et = Enemy.EnemyType.Heart;

            float currentHealthPercentageOfTotal = PlayerData_InGame.Instance.p1_HP_Curr / (float)PlayerData_InGame.Instance.p1_HP_Start ;
            int currentPowerUp1Used = PlayerData_InGame.Instance.p1_PowUp1_Count_Own - PlayerData_InGame.Instance.p1_PowUp1_Count_Ready;
            int currentPowerUp2Used = PlayerData_InGame.Instance.p1_PowUp2_Count_Own - PlayerData_InGame.Instance.p1_PowUp2_Count_Ready;

            int heartChance = 30;
            int pipeChance = 30;
            int mustacheChance = 30;
            int PU1Chance = 5;
            int PU2Chance = 5;

            if (currentPowerUp1Used ==5 || currentPowerUp2Used ==5)
            {
                if (currentPowerUp1Used ==5)
                {
                    PU1Chance += 24;
                    heartChance -= 8;
                    pipeChance -= 8;
                    mustacheChance -= 8;
                }
                if (currentPowerUp2Used == 5)
                {
                    PU2Chance += 24;
                    heartChance -= 8;
                    pipeChance -= 8;
                    mustacheChance -= 8;
                }
            }
            else if (currentPowerUp1Used == 4 || currentPowerUp2Used == 4)
            {
                if (currentPowerUp1Used == 4)
                {
                    PU1Chance += 18;
                    heartChance -= 6;
                    pipeChance -=6;
                    mustacheChance -= 6;
                }
                if (currentPowerUp2Used == 4)
                {
                    PU2Chance += 18;
                    heartChance -=6;
                    pipeChance -= 6;
                    mustacheChance -= 6;
                }
            }
            else if (currentHealthPercentageOfTotal <= 0.2)
            {
                heartChance += 54;
                pipeChance -= 27;
                mustacheChance -= 27;
            }
            else if (currentHealthPercentageOfTotal <= 0.4)
            {
                heartChance += 30;
                pipeChance -= 15;
                mustacheChance -= 15;
            }
            else if (currentPowerUp1Used == 3 || currentPowerUp2Used == 3)
            {
                if (currentPowerUp1Used == 3)
                {
                    PU1Chance += 12;
                    heartChance -=4;
                    pipeChance -= 4;
                    mustacheChance -= 4;
                }
                if (currentPowerUp2Used == 3)
                {
                    PU2Chance += 12;
                    heartChance -= 4;
                    pipeChance -= 4;
                    mustacheChance -= 4;
                }
            }
            else if (currentPowerUp1Used == 2 || currentPowerUp2Used == 2)
            {
                if (currentPowerUp1Used == 2)
                {
                    PU1Chance += 6;
                    heartChance -= 2;
                    pipeChance -= 2;
                    mustacheChance -= 2;
                }
                if (currentPowerUp2Used == 2)
                {
                    PU2Chance += 6;
                    heartChance -= 2;
                    pipeChance -= 2;
                    mustacheChance -= 2;
                }
            }
            else if (currentHealthPercentageOfTotal <= 0.5)
            {
                heartChance += 14;
                pipeChance -= 7;
                mustacheChance -= 7;
            }

            int randomInt = UnityEngine.Random.Range(0, 100);

			// Mohammad Endless 
//            Debug.Log("random int for intermission" + randomInt);

			// Mohammad Endless 
//            Debug.Log("heartChance" + heartChance);
//            Debug.Log("pipeChance" + pipeChance);
//            Debug.Log("mustacheChance" + mustacheChance);
//            Debug.Log("PU2Chance" + PU2Chance);
//            Debug.Log("PU1Chance" + PU1Chance);

            if (randomInt <= heartChance)
            {
                et = Enemy.EnemyType.Heart;
            }
            else if (randomInt <= heartChance + pipeChance)
            {
                et = Enemy.EnemyType.Pipe;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance)
            {
                et = Enemy.EnemyType.Mustache;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance+PU1Chance)
            {
                et = Enemy.EnemyType.PowerUp_Fist;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance + PU1Chance + PU2Chance)
            {
                et = Enemy.EnemyType.PowerUp_Shield;
            }

			// Mohammad Endless 
//            Debug.Log("random int for intermission" + randomInt);

			// Mohammad Endless 
//            Debug.Log("heartChance" + heartChance);
//            Debug.Log("pipeChance" + pipeChance);
//            Debug.Log("mustacheChance" + mustacheChance);
//            Debug.Log("PU2Chance" + PU2Chance);
//            Debug.Log("PU1Chance" + PU1Chance);

			// Mohammad Endless 
//            Debug.LogError("enemy type intermissionItem" + et);

            intermissionItem = GetIntermissionItem(et);
            enemyList.Add(intermissionItem);
            afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime + intermissionItem.enemyMoveSpeed - 3;
        }
        // 10% chance inke 2 ta item beshe
        else if (randInt > 45 && randInt <=55)
        {
            Enemy intermissionItem;
            Enemy.EnemyType et = Enemy.EnemyType.Heart;

            float currentHealthPercentageOfTotal = PlayerData_InGame.Instance.p1_HP_Curr / (float)PlayerData_InGame.Instance.p1_HP_Start;
            int currentPowerUp1Used = PlayerData_InGame.Instance.p1_PowUp1_Count_Own - PlayerData_InGame.Instance.p1_PowUp1_Count_Ready;
            int currentPowerUp2Used = PlayerData_InGame.Instance.p1_PowUp2_Count_Own - PlayerData_InGame.Instance.p1_PowUp2_Count_Ready;

            int heartChance = 30;
            int pipeChance = 30;
            int mustacheChance = 30;
            int PU1Chance = 5;
            int PU2Chance = 5;

			// Mohammad Endless 
//            Debug.Log("currentHealthPercentageOfTotal" + currentHealthPercentageOfTotal);
//            Debug.Log("currentPowerUp1Used" + currentPowerUp1Used);
//            Debug.Log("currentPowerUp2Used" + currentPowerUp2Used);


            if (currentPowerUp1Used == 5 || currentPowerUp2Used == 5)
            {
                if (currentPowerUp1Used == 5)
                {
                    PU1Chance += 24;
                    heartChance -= 8;
                    pipeChance -= 8;
                    mustacheChance -= 8;
                }
                if (currentPowerUp2Used == 5)
                {
                    PU2Chance += 24;
                    heartChance -= 8;
                    pipeChance -= 8;
                    mustacheChance -= 8;
                }
            }
            else if (currentPowerUp1Used == 4 || currentPowerUp2Used == 4)
            {
                if (currentPowerUp1Used == 4)
                {
                    PU1Chance += 18;
                    heartChance -= 6;
                    pipeChance -= 6;
                    mustacheChance -= 6;
                }
                if (currentPowerUp2Used == 4)
                {
                    PU2Chance += 18;
                    heartChance -= 6;
                    pipeChance -= 6;
                    mustacheChance -= 6;
                }
            }
            else if (currentHealthPercentageOfTotal <= 0.2)
            {
                heartChance += 54;
                pipeChance -= 27;
                mustacheChance -= 27;
            }
            else if (currentHealthPercentageOfTotal <= 0.4)
            {
                heartChance += 30;
                pipeChance -= 15;
                mustacheChance -= 15;
            }
            else if (currentPowerUp1Used == 3 || currentPowerUp2Used == 3)
            {
                if (currentPowerUp1Used == 3)
                {
                    PU1Chance += 12;
                    heartChance -= 4;
                    pipeChance -= 4;
                    mustacheChance -= 4;
                }
                if (currentPowerUp2Used == 3)
                {
                    PU2Chance += 12;
                    heartChance -= 4;
                    pipeChance -= 4;
                    mustacheChance -= 4;
                }
            }
            else if (currentPowerUp1Used == 2 || currentPowerUp2Used == 2)
            {
                if (currentPowerUp1Used == 2)
                {
                    PU1Chance += 6;
                    heartChance -= 2;
                    pipeChance -= 2;
                    mustacheChance -= 2;
                }
                if (currentPowerUp2Used == 2)
                {
                    PU2Chance += 6;
                    heartChance -= 2;
                    pipeChance -= 2;
                    mustacheChance -= 2;
                }
            }
            else if (currentHealthPercentageOfTotal <= 0.5)
            {
                heartChance += 14;
                pipeChance -= 7;
                mustacheChance -= 7;
            }

            int randomInt = UnityEngine.Random.Range(0, 100);

            if (randomInt <= heartChance)
            {
                et = Enemy.EnemyType.Heart;
            }
            else if (randomInt <= heartChance + pipeChance)
            {
                et = Enemy.EnemyType.Pipe;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance)
            {
                et = Enemy.EnemyType.Mustache;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance + PU1Chance)
            {
                et = Enemy.EnemyType.PowerUp_Fist;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance + PU1Chance + PU2Chance)
            {
                et = Enemy.EnemyType.PowerUp_Shield;
            }

			// Mohammad Endless 
//            Debug.Log("random int for intermission" + randomInt);

			// Mohammad Endless 
//            Debug.Log("heartChance" + heartChance);
//            Debug.Log("pipeChance" + pipeChance);
//            Debug.Log("mustacheChance" + mustacheChance);
//            Debug.Log("PU2Chance" + PU2Chance);
//            Debug.Log("PU1Chance" + PU1Chance);

			// Mohammad Endless 
//            Debug.LogError("enemy type intermissionItem" + et);

            intermissionItem = GetIntermissionItem(et);
            enemyList.Add(intermissionItem);
            afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime + intermissionItem.enemyMoveSpeed - 3;

            heartChance = 30;
            pipeChance = 30;
            mustacheChance = 30;
            PU1Chance = 5;
            PU2Chance = 5;

            randomInt = UnityEngine.Random.Range(0, 100);
            
            if (randomInt <= heartChance)
            {
                et = Enemy.EnemyType.Heart;
            }
            else if (randomInt <= heartChance + pipeChance)
            {
                et = Enemy.EnemyType.Pipe;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance)
            {
                et = Enemy.EnemyType.Mustache;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance + PU1Chance)
            {
                et = Enemy.EnemyType.PowerUp_Fist;
            }
            else if (randomInt <= heartChance + pipeChance + mustacheChance + PU1Chance + PU2Chance)
            {
                et = Enemy.EnemyType.PowerUp_Shield;
            }

			// Mohammad Endless 
//            Debug.Log("random int for intermission" + randomInt);

			// Mohammad Endless 
//            Debug.Log("heartChance" + heartChance);
//            Debug.Log("pipeChance" + pipeChance);
//            Debug.Log("mustacheChance" + mustacheChance);
//            Debug.Log("PU2Chance" + PU2Chance);
//            Debug.Log("PU1Chance" + PU1Chance);

			// Mohammad Endless 
//            Debug.LogError("enemy type intermissionItem" + et);

            intermissionItem = GetIntermissionItem(et);
            enemyList.Add(intermissionItem);
            afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime + intermissionItem.enemyMoveSpeed - 3;
        }
        // 45% chnace inke hichi item nayad
        else
        {
			// Mohammad Endless 
//            Debug.LogError("no intermission item");

            afterEachBlockTime = enemyList[enemyList.Count - 1].enemySpawnTime;
        }
        // The minus 2 is for going before mid-wave rewards index (-1 for reward and -1 for before)
        intEnemyEndOfWaveArr.Add (enemyList.Count - 1);

		intTempWaveCounter++;

        //Moshtan_Utilties_Script.ShowToast(enemyList.Count.ToString());
        isProcessing = false;
    }
    private Enemy GetIntermissionItem(Enemy.EnemyType enemyType)
    {
        Enemy intermissionItem = new Enemy();
        intermissionItem.enemyType = enemyType;
        intermissionItem.enemySpawnTime = enemyList[enemyList.Count - 1].enemySpawnTime + 1;
        if (enemyType == Enemy.EnemyType.SuperMustache)
        {
            intermissionItem.enemyMoveSpeed = 13;
            intermissionItem.itemValue = 3;
            intermissionItem.enemyHP = FixEnemyHealth("UUU", 100, GetRandomPool());
            intermissionItem.enemyLane = GetRandomLane();
        }
        else
        {
            intermissionItem.enemyMoveSpeed = 8;
            intermissionItem.itemValue = GetEnemyItemValue();
            intermissionItem.enemyHP = FixEnemyHealth("", GetHp(enemyType), GetRandomPool());
            intermissionItem.enemyLane = GetRandomLane();
        }

        return intermissionItem;
    }
    private Enemy.LaneTypes GetRandomLane()
    {
        int rand = UnityEngine.Random.Range(0, 4);
        switch (rand)
        {
            case 0:
                return Enemy.LaneTypes.Lane_1;
            case 1:
                return Enemy.LaneTypes.Lane_2;
            case 2:
                return Enemy.LaneTypes.Lane_3;
            case 3:
                return Enemy.LaneTypes.Lane_4;
            default:
                return Enemy.LaneTypes.Lane_2;
        }
    }

    private Enemy.LaneTypes SwapEnemyLane(Enemy.LaneTypes originalLane,int shiftForwardLanes)
    {
        Enemy.LaneTypes resultLane;
        switch (originalLane)
        {
            case Enemy.LaneTypes.Lane_1:
                switch (shiftForwardLanes)
                {
                    case 1:
                        resultLane = Enemy.LaneTypes.Lane_2;
                        break;
                    case 2:
                        resultLane = Enemy.LaneTypes.Lane_3;
                        break;
                    case 3:
                        resultLane = Enemy.LaneTypes.Lane_4;
                        break;
                    default:
                        resultLane = Enemy.LaneTypes.Lane_1;
                        break;
                }
                break;
            case Enemy.LaneTypes.Lane_2:
                switch (shiftForwardLanes)
                {
                    case 1:
                        resultLane = Enemy.LaneTypes.Lane_3;
                        break;
                    case 2:
                        resultLane = Enemy.LaneTypes.Lane_4;
                        break;
                    case 3:
                        resultLane = Enemy.LaneTypes.Lane_1;
                        break;
                    default:
                        resultLane = Enemy.LaneTypes.Lane_2;
                        break;
                }
                break;
            case Enemy.LaneTypes.Lane_3:
                switch (shiftForwardLanes)
                {
                    case 1:
                        resultLane = Enemy.LaneTypes.Lane_4;
                        break;
                    case 2:
                        resultLane = Enemy.LaneTypes.Lane_1;
                        break;
                    case 3:
                        resultLane = Enemy.LaneTypes.Lane_2;
                        break;
                    default:
                        resultLane = Enemy.LaneTypes.Lane_3;
                        break;
                }
                break;
            case Enemy.LaneTypes.Lane_4:
                switch (shiftForwardLanes)
                {
                    case 1:
                        resultLane = Enemy.LaneTypes.Lane_1;
                        break;
                    case 2:
                        resultLane = Enemy.LaneTypes.Lane_2;
                        break;
                    case 3:
                        resultLane = Enemy.LaneTypes.Lane_3;
                        break;
                    default:
                        resultLane = Enemy.LaneTypes.Lane_4;
                        break;
                }
                break;
            default:
                resultLane = originalLane;
                break;
        }

        return resultLane;
    }

    private Enemy SetEnemyAttributes(Enemy enemy )
    {
        //HP - Damage - Mustache - Explosion Damage -Sticky health - item value - aorrow(hp)
        //attribute hayee hastan ke bayad bar tebgh tedad enemy attribute ha avaz mishe
        enemy.enemyHP = FixEnemyHealth("", GetHp(enemy.enemyType ), GetRandomPool());
        enemy.enemyDamage = GetEnemyDamage(enemy.enemyType);
        enemy.enemyMustache = GetEnemyMustache(enemy.enemyType);
        //enemy.explosionDamage = GetEnemyExplosionDamage(enemy.enemyType);
        enemy.stickyHealth = GetEnemyStickyHealth();
        enemy.itemValue = GetEnemyItemValue();
        
        return enemy;
    }

    private int GetEnemyItemValue()
    {
        int itemValue = (enemyIndex / (GLOBAL_DETEMINER * 2)) + 1;
        int randInt = UnityEngine.Random.Range(0, 3);
        switch (itemValue)
        {
            case 1:
                itemValue = 1;
                break;
            case 2:
                if (randInt == 0)
                {
                    itemValue = 1;
                }
                else
                {
                    itemValue = 2;
                }
                break;
            case 3:
                if (randInt == 0)
                {
                    if (UnityEngine.Random.Range(0, 2) == 0)
                    {
                        itemValue = 1;
                    }
                    else
                    {
                        itemValue = 2;
                    }
                    //itemValue = (UnityEngine.Random.Range(0, 2) == 0) ?  1 :  2;
                }
                else
                {
                    itemValue = 2;
                }
                break;
            default:
			// Default happens when itemValue is 4 and more
			if (randInt == 0)
			{
				if (UnityEngine.Random.Range(0, 2) == 0)
				{
					itemValue = 1;
				}
				else
				{
					itemValue = 2;
				}
				//itemValue = (UnityEngine.Random.Range(0, 2) == 0) ?  1 :  2;
			}
			else
			{
				itemValue = 2;
			}
                break;
        }
         //        Debug.Log("itemValue" + itemValue);
        return itemValue;
    }

    private int GetEnemyStickyHealth()
    {
        return (((enemyIndex / (GLOBAL_DETEMINER*3)) + 1) < 5) ? ((enemyIndex / (GLOBAL_DETEMINER * 3)) + 1) : 5;
    }

    private int GetEnemyExplosionDamage()
    {
        int x = (int)(GetHp(Enemy.EnemyType.Fat1)*0.5f);
        if (x <=0)
        {
            x = 1;
        }
        return x ;
    }

    private int GetEnemyMustache(Enemy.EnemyType enemyType)
    {
        if (enemyType == Enemy.EnemyType.Giant1 || enemyType == Enemy.EnemyType.Giant2)
        {
            return 5;
        }
        return 1;
    }

    private int GetEnemyDamage(Enemy.EnemyType enemyType)
    {
        int dmg = 1;
        int initialDmg = 1;
        int fatDmg = 1;

        fatDmg = (enemyIndex / (GLOBAL_DETEMINER * 5)) + initialDmg;
        switch (enemyType)
        {
            case Enemy.EnemyType.Fat1:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.Fat2:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.Thin1:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.Thin2:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.Muscle1:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Muscle2:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Giant1:
                dmg = fatDmg*15;
                break;
            case Enemy.EnemyType.Giant2:
                dmg = fatDmg*15;
                break;
            case Enemy.EnemyType.HF1:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.HF2:
                dmg = fatDmg;
                break;
            case Enemy.EnemyType.HF_X1:
                dmg = fatDmg*5;
                break;
            case Enemy.EnemyType.HF_X2:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Barrel1:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Barrel2:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Barrel_X1:
                dmg = fatDmg*5;
                break;
            case Enemy.EnemyType.Barrel_X2:
                dmg = fatDmg*5;
                break;
            case Enemy.EnemyType.Barrel_Q1:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.Barrel_Q2:
                dmg = fatDmg*2;
                break;
            case Enemy.EnemyType.e18:
                break;
            case Enemy.EnemyType.e19:
                break;
            case Enemy.EnemyType.e20:
                break;
            case Enemy.EnemyType.e21:
                break;
            case Enemy.EnemyType.e22:
                break;
            case Enemy.EnemyType.e23:
                break;
            case Enemy.EnemyType.e24:
                break;
            case Enemy.EnemyType.e25:
                break;
            case Enemy.EnemyType.e26:
                break;
            case Enemy.EnemyType.e27:
                break;
            case Enemy.EnemyType.e28:
                break;
            case Enemy.EnemyType.e29:
                break;
            case Enemy.EnemyType.e30:
                break;
            case Enemy.EnemyType.e31:
                break;
            case Enemy.EnemyType.e32:
                break;
            case Enemy.EnemyType.e33:
                break;
            case Enemy.EnemyType.e34:
                break;
            case Enemy.EnemyType.e35:
                break;
            case Enemy.EnemyType.e36:
                break;
            case Enemy.EnemyType.e37:
                break;
            case Enemy.EnemyType.e38:
                break;
            case Enemy.EnemyType.e39:
                break;
            case Enemy.EnemyType.Heart:
                break;
            case Enemy.EnemyType.Pipe:
                break;
            case Enemy.EnemyType.Hedgehog:
                break;
            case Enemy.EnemyType.Mustache:
                break;
            case Enemy.EnemyType.Elixir:
                break;
            case Enemy.EnemyType.Freeze:
                break;
            case Enemy.EnemyType.Random_Item:
                break;
            case Enemy.EnemyType.BigBomb:
                break;
            case Enemy.EnemyType.LineBomb:
                break;
			case Enemy.EnemyType.Score_Item:
			case Enemy.EnemyType.SuperScore:
			case Enemy.EnemyType.PowerUp_Fist:
			case Enemy.EnemyType.PowerUp_Shield:
				break;
            default:
                break;
        }
        return dmg;
    }

    private string GetRandomPool()
    {
        string randomPool = "";
        StringBuilder sb = new StringBuilder();
        sb.Append("lurd");
        // ta n/2 faghat lurd
        // az n/2 be bad 2468 ezafe mishe ba 6% ehtemal
        // har n ta 3% be shanse ona ezafe mishe
        // masalan ba farz n=80 enemy 50 6% enemy 100 9% 
        if (enemyIndex > GLOBAL_DETEMINER/2 )
        {
            if (enemyIndex < GLOBAL_DETEMINER) //beine 40 ta 80
            {
                //baraye inke 6% chance gheire lurd bashe 31 lurd lazeme va 2 ta 2468
                for (int i = 0; i < 23; i++)
                {
                    sb.Append("lurd");
                }
                for (int i = 0; i < 2; i++)
                {
                    sb.Append("2468");
                }
            }
            else // az 80 be bad
            {
                var x = (enemyIndex - GLOBAL_DETEMINER) / GLOBAL_DETEMINER;
                for (int i = 0; i < 23 - x; i++)
                {
                    sb.Append("lurd");
                }
                for (int i = 0; i < 2 + x; i++)
                {
                    sb.Append("2468");
                }
            }
            // TODO: cap 50% baraye in amlamat ha dar nazar begir
        }

        randomPool = sb.ToString();
//        Debug.Log(randomPool);
        return randomPool;
    }


    private int GetHp(Enemy.EnemyType enemyType )
    {
        int hp = 7;
        int initialHp = 7;
        int fatHp = 7;
        // har n/10 om 1 ki be joon e fat ezafe mishe

        // 2 bakhsh afzayesh joon hast
        // 
        // bakhsh aval ba farz n=80 har 6 ta enemy 1 ki ziad she .. az 0 ta 30 - Not impl
        // bakhsh dovom ba farz n=80 har 10 ta 1 ki ziad she .. az 30 ta 80 - Not impl

        // ta avalin n/2 har n/6 1ki hp ziad she
        // az on be bad har n/10 1ki hp ziad she



		if (enemyList.Count < baze1)
		{
            //Debug.Log("baze 111111");
            fatHp = (enemyList.Count / (baze1 / 3)) + initialHp;
		}
		else if (enemyList.Count < baze2)
		{
            //Debug.Log("baze 222222");
            initialHp = 10;
            fatHp = ((enemyList.Count - baze1) / ((baze2 - baze1) / 10)) + initialHp;
		}
		else if (enemyList.Count < baze3)
		{
            //Debug.Log("baze 33333");
            initialHp = 20;
            fatHp = ((enemyList.Count - baze2) / ((baze3 - baze2) / 11)) + initialHp;
		}
		else if (enemyList.Count < baze4)
		{
            //Debug.Log("baze 44444");
            initialHp = 31;
            fatHp = ((enemyList.Count - baze3) / ((baze4 - baze3) / 12)) + initialHp;
		}
        else if (enemyList.Count < baze5)
        {
            //Debug.Log("baze 44444");
            initialHp = 42;
            fatHp = ((enemyList.Count - baze4) / ((baze5 - baze4) / 13)) + initialHp;
        }
        else if (enemyList.Count < baze6)
        {
            //Debug.Log("baze 44444");
            initialHp = 53;
            fatHp = ((enemyList.Count - baze5) / ((baze5 - baze4) / 14)) + initialHp;
        }
        else if (enemyList.Count < baze7)
        {
            //Debug.Log("baze 44444");
            initialHp = 64;
            fatHp = ((enemyList.Count - baze6) / ((baze6 - baze5) / 15)) + initialHp;
        }
        else if (enemyList.Count < baze8)
        {
            //Debug.Log("baze 44444");
            initialHp = 79;
            fatHp = ((enemyList.Count - baze7) / ((baze7 - baze6) / 16)) + initialHp;
        }
        else if (enemyList.Count < baze9)
        {
            //Debug.Log("baze 44444");
            initialHp = 95;
            fatHp = ((enemyList.Count - baze9) / ((baze9 - baze8) / 17)) + initialHp;
        }
        else
		{
            //Debug.Log("baze 55555");
			initialHp = 112;
            fatHp = ((enemyList.Count - baze9) / ((baze9 - baze8) / 18)) + initialHp;
		}

        //Debug.Log("fat hp" + fatHp);

        //50% chance ziad shodan hp
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            float fatHpTemp = fatHp;
            fatHpTemp *= (UnityEngine.Random.Range(0, 10) / 100f);
            fatHp += (int)fatHpTemp;
            fatHp += UnityEngine.Random.Range(0, 3);
        }
        else //50% chance kam shodan hp
        {
            float fatHpTemp = fatHp;
            fatHpTemp *= (UnityEngine.Random.Range(0, 10) / 100f);
            fatHp -= (int)fatHpTemp;
            fatHp -= UnityEngine.Random.Range(0, 3);
        }

        switch (enemyType) {
		case Enemy.EnemyType.Fat1:
			hp = fatHp;
			break;
		case Enemy.EnemyType.Fat2:
			hp = fatHp;
			break;
		case Enemy.EnemyType.Thin1:
			hp = (int)(fatHp * 0.6);
			break;
		case Enemy.EnemyType.Thin2:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Muscle1:
			hp = (int)(fatHp * 0.8);
			break;
		case Enemy.EnemyType.Muscle2:
			hp = (int)(fatHp * 0.7);
			break;
		case Enemy.EnemyType.Giant1:
			hp = (int)(fatHp * 5);
			break;
		case Enemy.EnemyType.Giant2:
			hp = (int)(fatHp * 5);
			break;
		case Enemy.EnemyType.HF1:
			hp = (int)(fatHp * 0.4);
			break;
		case Enemy.EnemyType.HF2:
			hp = (int)(fatHp * 0.4);
			break;
		case Enemy.EnemyType.HF_X1:
			hp = (int)(fatHp * 0.4);
			break;
		case Enemy.EnemyType.HF_X2:
			hp = (int)(fatHp * 0.4);
			break;
		case Enemy.EnemyType.Barrel1:
			hp = (int)(fatHp * 0.7);
			break;
		case Enemy.EnemyType.Barrel2:
			hp = (int)(fatHp * 0.7);
			break;
		case Enemy.EnemyType.Barrel_X1:
			hp = (int)(fatHp * 0.6);
			break;
		case Enemy.EnemyType.Barrel_X2:
			hp = (int)(fatHp * 0.6);
			break;
		case Enemy.EnemyType.Barrel_Q1:
			hp = (int)(fatHp * 0.6);
			break;
		case Enemy.EnemyType.Barrel_Q2:
			hp = (int)(fatHp * 0.6);
			break;
		case Enemy.EnemyType.e18:
			break;
		case Enemy.EnemyType.e19:
			break;
		case Enemy.EnemyType.e20:
			break;
		case Enemy.EnemyType.e21:
			break;
		case Enemy.EnemyType.e22:
			break;
		case Enemy.EnemyType.e23:
			break;
		case Enemy.EnemyType.e24:
			break;
		case Enemy.EnemyType.e25:
			break;
		case Enemy.EnemyType.e26:
			break;
		case Enemy.EnemyType.e27:
			break;
		case Enemy.EnemyType.e28:
			break;
		case Enemy.EnemyType.e29:
			break;
		case Enemy.EnemyType.e30:
			break;
		case Enemy.EnemyType.e31:
			break;
		case Enemy.EnemyType.e32:
			break;
		case Enemy.EnemyType.e33:
			break;
		case Enemy.EnemyType.e34:
			break;
		case Enemy.EnemyType.e35:
			break;
		case Enemy.EnemyType.e36:
			break;
		case Enemy.EnemyType.e37:
			break;
		case Enemy.EnemyType.e38:
			break;
		case Enemy.EnemyType.e39:
			break;
		case Enemy.EnemyType.Heart:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Pipe:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Hedgehog:
			hp = (int)(fatHp * 0.2);
			break;
		case Enemy.EnemyType.Mustache:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Elixir:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Freeze:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Random_Item:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.BigBomb:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.LineBomb:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.Score_Item:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.SuperScore:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.PowerUp_Fist:
			hp = (int)(fatHp * 0.5);
			break;
		case Enemy.EnemyType.PowerUp_Shield:
			hp = (int)(fatHp * 0.5);
			break;
		default:
			Debug.LogError ("default switch");
			break;
		}
        return hp;
    }

    private string FixEnemyHealth(string enemyHP, int hp, string randomPool)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(enemyHP);

        //aval tamame jahaye khali ba * por she

		// Mohammad Endless 
//        if (hp - enemyHP.Length < 0) Debug.LogError("hp az arrow kamtare");

        string starRepeat = new string('*', hp - enemyHP.Length);
        sb.Append(starRepeat);

        //az random pool bekhoone va * haro ba yeki az charachter haye random pool avaz kone
        for (int i = 0; i < sb.Length; i++)
        {
            if (sb[i] == '*')
            {
                sb[i] = GetRandomCharacter(randomPool, new System.Random(Guid.NewGuid().GetHashCode()));
            }
        }

        sb.Replace('l', 'L').Replace('u', 'U').Replace('r', 'R').Replace('d', 'D').Replace('/', '|').Replace('2', 'v')
    .Replace('4', '<').Replace('8', '^').Replace('6', '>');
        return sb.ToString();
    }
    public static char GetRandomCharacter(string randomPool, System.Random rng)
    {
        int index = rng.Next(randomPool.Length);
        return randomPool[index];
    }

    IEnumerator isValidNextBlock(List<Enemy> firstBlock, List<Enemy> secondBlock, Action onSuccess , Action onFail)
    {
        yield return null;
        Enemy firstBlock_Lane1LastEnemy = firstBlock.LastOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_1);
        Enemy firstBlock_Lane2LastEnemy = firstBlock.LastOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_2);
        Enemy firstBlock_Lane3LastEnemy = firstBlock.LastOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_3);
        Enemy firstBlock_Lane4LastEnemy = firstBlock.LastOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_4);

        Enemy secondBlock_Lane1FirstEnemy = secondBlock.FirstOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_1);
        Enemy secondBlock_Lane2FirstEnemy = secondBlock.FirstOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_2);
        Enemy secondBlock_Lane3FirstEnemy = secondBlock.FirstOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_3);
        Enemy secondBlock_Lane4FirstEnemy = secondBlock.FirstOrDefault(l => l.enemyLane == Enemy.LaneTypes.Lane_4);

        float firstBlock_Lane1LastEnemyExitTime;
        float firstBlock_Lane2LastEnemyExitTime;
        float firstBlock_Lane3LastEnemyExitTime;
        float firstBlock_Lane4LastEnemyExitTime;

        //be gheir az barrel faghat speed zaman e residan be khat payan ro moshakhas mikone

        try
        {
            if (firstBlock_Lane1LastEnemy.enemyDamage > 0)
            {
                if ((int)firstBlock_Lane1LastEnemy.enemyType < 12 || (int)firstBlock_Lane1LastEnemy.enemyType > 17)
                {
                    firstBlock_Lane1LastEnemyExitTime = firstBlock_Lane1LastEnemy.enemySpawnTime + firstBlock_Lane1LastEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    firstBlock_Lane1LastEnemyExitTime = firstBlock_Lane1LastEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (firstBlock_Lane1LastEnemy.enemyMoveSpeed + 1.6f);
                }
            }
            else
            {
                firstBlock_Lane1LastEnemyExitTime = 0f;
            }

        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            firstBlock_Lane1LastEnemyExitTime = 0f;
        }
        yield return null;
        try
        {
            if (firstBlock_Lane2LastEnemy.enemyDamage >0)
            {
                if ((int)firstBlock_Lane2LastEnemy.enemyType < 12 || (int)firstBlock_Lane2LastEnemy.enemyType > 17)
                {
                    firstBlock_Lane2LastEnemyExitTime = firstBlock_Lane2LastEnemy.enemySpawnTime + firstBlock_Lane2LastEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    firstBlock_Lane2LastEnemyExitTime = firstBlock_Lane2LastEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (firstBlock_Lane2LastEnemy.enemyMoveSpeed + 1.6f);
                }
            }
            else
            {
                firstBlock_Lane2LastEnemyExitTime = 0f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            firstBlock_Lane2LastEnemyExitTime = 0f;
        }
        yield return null;
        try
        {
            if (firstBlock_Lane3LastEnemy.enemyDamage >0)
            {
                if ((int)firstBlock_Lane3LastEnemy.enemyType < 12 || (int)firstBlock_Lane3LastEnemy.enemyType > 17)
                {
                    firstBlock_Lane3LastEnemyExitTime = firstBlock_Lane3LastEnemy.enemySpawnTime + firstBlock_Lane3LastEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    firstBlock_Lane3LastEnemyExitTime = firstBlock_Lane3LastEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (firstBlock_Lane3LastEnemy.enemyMoveSpeed + 1.6f);
                }
            }
            else
            {
                firstBlock_Lane3LastEnemyExitTime = 0f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            firstBlock_Lane3LastEnemyExitTime = 0f;
        }
        yield return null;
        try
        {
            if (firstBlock_Lane4LastEnemy.enemyDamage > 0)
            {
                if ((int)firstBlock_Lane4LastEnemy.enemyType < 12 || (int)firstBlock_Lane4LastEnemy.enemyType > 17)
                {
                    firstBlock_Lane4LastEnemyExitTime = firstBlock_Lane4LastEnemy.enemySpawnTime + firstBlock_Lane4LastEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    firstBlock_Lane4LastEnemyExitTime = firstBlock_Lane4LastEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (firstBlock_Lane4LastEnemy.enemyMoveSpeed + 1.6f);
                }
            }
            else
            {
                firstBlock_Lane4LastEnemyExitTime = 0f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            firstBlock_Lane4LastEnemyExitTime = 0f;
        }
        yield return null;
        float secondBlock_Lane1FirstEnemyExitTime;
        float secondBlock_Lane2FirstEnemyExitTime;
        float secondBlock_Lane3FirstEnemyExitTime;
        float secondBlock_Lane4FirstEnemyExitTime;

        try
        {
            if (secondBlock_Lane1FirstEnemy.enemyDamage >0)
            {
                if ((int)secondBlock_Lane1FirstEnemy.enemyType < 12 || (int)secondBlock_Lane1FirstEnemy.enemyType > 17)
                {
                    //be gheir az barrel faghat speed zaman e residan be khat payan ro moshakhas mikone
                    secondBlock_Lane1FirstEnemyExitTime = secondBlock_Lane1FirstEnemy.enemySpawnTime + secondBlock_Lane1FirstEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    secondBlock_Lane1FirstEnemyExitTime = secondBlock_Lane1FirstEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (secondBlock_Lane1FirstEnemy.vulnerableTime + 1.6f) ;
                }
            }
            else
            {
                secondBlock_Lane1FirstEnemyExitTime = 999999f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            secondBlock_Lane1FirstEnemyExitTime = 999999f;
        }
        yield return null;
        try
        {
            if (secondBlock_Lane2FirstEnemy.enemyDamage>0)
            {
                if ((int)secondBlock_Lane2FirstEnemy.enemyType < 12 || (int)secondBlock_Lane2FirstEnemy.enemyType > 17)
                {
                    //be gheir az barrel faghat speed zaman e residan be khat payan ro moshakhas mikone
                    secondBlock_Lane2FirstEnemyExitTime = secondBlock_Lane2FirstEnemy.enemySpawnTime + secondBlock_Lane2FirstEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    secondBlock_Lane2FirstEnemyExitTime = secondBlock_Lane1FirstEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (secondBlock_Lane2FirstEnemy.vulnerableTime + 1.6f); 
                }
            }
            else
            {
                secondBlock_Lane2FirstEnemyExitTime = 999999f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            secondBlock_Lane2FirstEnemyExitTime = 999999f;
        }
        yield return null;
        try
        {
            if (secondBlock_Lane3FirstEnemy.enemyDamage >0)
            {
                if ((int)secondBlock_Lane3FirstEnemy.enemyType < 12 || (int)secondBlock_Lane3FirstEnemy.enemyType > 17)
                {
                    //be gheir az barrel faghat speed zaman e residan be khat payan ro moshakhas mikone
                    secondBlock_Lane3FirstEnemyExitTime = secondBlock_Lane3FirstEnemy.enemySpawnTime + secondBlock_Lane3FirstEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    secondBlock_Lane3FirstEnemyExitTime = secondBlock_Lane3FirstEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (secondBlock_Lane3FirstEnemy.vulnerableTime + 1.6f); 
                }
            }
            else
            {
                secondBlock_Lane3FirstEnemyExitTime = 999999f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            secondBlock_Lane3FirstEnemyExitTime = 999999f;
        }
        yield return null;
        try
        {
            if (secondBlock_Lane4FirstEnemy.enemyDamage>0)
            {
                if ((int)secondBlock_Lane4FirstEnemy.enemyType < 12 || (int)secondBlock_Lane4FirstEnemy.enemyType > 17)
                {
                    //be gheir az barrel faghat speed zaman e residan be khat payan ro moshakhas mikone
                    secondBlock_Lane4FirstEnemyExitTime = secondBlock_Lane4FirstEnemy.enemySpawnTime + secondBlock_Lane4FirstEnemy.enemyMoveSpeed;
                }
                else
                {
                    //barrel alabe bar speed .. tedad biron omadan*(zaman biroon mandan + zaman sabet animation)
                    secondBlock_Lane4FirstEnemyExitTime = secondBlock_Lane4FirstEnemy.enemySpawnTime + PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount * (secondBlock_Lane4FirstEnemy.vulnerableTime + 1.6f);
                }
            }
            else
            {
                secondBlock_Lane4FirstEnemyExitTime = 999999f;
            }
        }
        catch (Exception)
        {
            //Debug.LogError("code reached here..exception" + e.Message);
            secondBlock_Lane4FirstEnemyExitTime = 999999f;
        }
        yield return null;
        if ((firstBlock_Lane1LastEnemyExitTime +0.5f < secondBlock_Lane1FirstEnemyExitTime)
            && (firstBlock_Lane2LastEnemyExitTime + 0.5f < secondBlock_Lane2FirstEnemyExitTime)
            && (firstBlock_Lane3LastEnemyExitTime + 0.5f < secondBlock_Lane3FirstEnemyExitTime)
            && (firstBlock_Lane4LastEnemyExitTime + 0.5f < secondBlock_Lane4FirstEnemyExitTime))
        {
            //Debug.LogWarning("wave for conflict returned true");
            onSuccess();
        }
        //Debug.LogWarning("wave for conflict returned false");
        onFail();
    }

	public void ContinueSpawning () {
		endWave_EnemiesFinished = false;
		intEnemyEndOfWaveIndex++;

		// Mohammad Endless 
		Debug.LogError ("Continue spawning with index of " + intEnemyEndOfWaveIndex);

		PlayerData_InGame.Instance.lastEnemyWasKilled = false;
		PlayerData_InGame.Instance.lastEnemyWasSpawned = false;

		StartCoroutine (SetKillReward_BasedOnWave ());
	}

	public IEnumerator SetKillReward_BasedOnWave () {
		yield return null;

		// Increase combo kill reward as well
		PlayerData_InGame.Instance.hudScriptHolder.endlessScoreController.intKillComboReward = intEnemyEndOfWaveIndex;
	}

	public IEnumerator Endless_ChangeBackground () {
		yield return null;
		PlayerData_InGame.Instance.hudScriptHolder.HUD_EndlessChangeBackground ();
	}

	IEnumerator ForceStory (int whatWorld, int whatLevel, int whatSet) {
		yield return null;
		storyMainScriptHolder.StartStory (whatWorld, whatLevel, whatSet, true);
	}

    void Update()
    {
		if (!endWave_EnemiesFinished) {
			if (!EventTrigger.isPaused && !EventTrigger.isFrozen) {
				//Debug.Log(enemyList.Count);

				// For scary times when list doesn't get added to for some reason
				if (enemyList.Count == 0)
				{
					return;
				}

//				Debug.Log(enemyIndex);

				// This is to make sure we haven't reach the end of count
				if (enemyIndex + 3 > enemyList.Count)
				{
					if (!isProcessing) {
						GetUniversalData ();
						StartCoroutine (AddNextBlock ());
					}
				}

				timeOfNextSpawn += Time.deltaTime;

				if (timeOfNextSpawn >= enemyList [enemyIndex].enemySpawnTime) {
					switch (enemyList [enemyIndex].enemyType) {
					case Enemy.EnemyType.Fat1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));
						break;
					case Enemy.EnemyType.Fat2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));

						// Check tutorial when fat 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Fat) {
								StartCoroutine (ForceStory (1, 11, 0));
							}
						}

						break;
					case Enemy.EnemyType.Thin1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Thin, enemyIndex));
						break;
					case Enemy.EnemyType.Thin2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Thin, enemyIndex));

						// Check tutorial when thin 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Thin) {
								StartCoroutine (ForceStory (1, 12, 0));
							}
						}

						break;
					case Enemy.EnemyType.Muscle1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Muscle, enemyIndex));
						break;
					case Enemy.EnemyType.Muscle2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Muscle, enemyIndex));

						// Check tutorial when muscle 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle) {
								StartCoroutine (ForceStory (1, 13, 0));
							}
						}

						break;
					case Enemy.EnemyType.Giant1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Giant, enemyIndex));
						break;
					case Enemy.EnemyType.Giant2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Giant, enemyIndex));

						// Check tutorial when giant 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Giant) {
								StartCoroutine (ForceStory (1, 14, 0));
							}
						}

						break;
					case Enemy.EnemyType.HF1:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF2:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF_X1:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF_X2:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_X1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_X2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_Q1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_Q2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Pipe:
						StartCoroutine (SpawnThis (EnemyObject_Pipe, enemyIndex));
						break;
					case Enemy.EnemyType.Elixir:
						StartCoroutine (SpawnThis (EnemyObject_Elixir, enemyIndex));
						break;
					case Enemy.EnemyType.Freeze:
						StartCoroutine (SpawnThis (EnemyObject_Freeze, enemyIndex));
						break;
					case Enemy.EnemyType.Heart:
						StartCoroutine (SpawnThis (EnemyObject_Heart, enemyIndex));
						break;
					case Enemy.EnemyType.Hedgehog:
						StartCoroutine (SpawnThis (EnemyObject_Hedgehog, enemyIndex));
						break;
					case Enemy.EnemyType.Mustache:
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;
					case Enemy.EnemyType.SuperMustache:
						StartCoroutine (SpawnThis (EnemyObject_SuperMustache, enemyIndex));

						// To change background and show new wave
						StartCoroutine (NewWave_BackAndBanner ());

//						StartCoroutine (Endless_ChangeBackground ());
//						StartCoroutine (BellAndBanner_Call ());
						break;
					case Enemy.EnemyType.BigBomb:
						StartCoroutine (SpawnThis (EnemyObject_BigBomb, enemyIndex));
						break;
					case Enemy.EnemyType.LineBomb:
						// TODO: Should spawn Bomb Enemy
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;
					case Enemy.EnemyType.Score_Item:
						StartCoroutine (SpawnThis (EnemyObject_Score_Item, enemyIndex));
						break;
					case Enemy.EnemyType.SuperScore:
						StartCoroutine (SpawnThis (EnemyObject_SuperScore, enemyIndex));
						break;
					case Enemy.EnemyType.PowerUp_Fist:
						StartCoroutine (SpawnThis (EnemyObject_PowerUp_Fist, enemyIndex));
						break;
					case Enemy.EnemyType.PowerUp_Shield:
						StartCoroutine (SpawnThis (EnemyObject_PowerUp_Shield, enemyIndex));
						break;
					case Enemy.EnemyType.Random_Item:
						break;
					default:
						Debug.LogError ("default switch");
						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));
						break;
					}
//					Debug.Log ("time of next spawn after enemy spawn" + timeOfNextSpawn);

					// For DEL (In main game)
//					textEnemyIndex.text = enemyIndex.ToString ();

					enemyIndex++;

					// Call bell / banner (Moved to after wave end!)
//					if (enemyIndex % 10 == 0) {
//						StartCoroutine (BellAndBanner_Call ());
//					}

					if (enemyIndex >= intEnemyEndOfWaveArr[intEnemyEndOfWaveIndex]) {

						// Stops update and spawn
						endWave_EnemiesFinished = true;

						// To start checking for last enemy by EnemyHealth and other scripts
						PlayerData_InGame.Instance.lastEnemyWasSpawned = true;

						// Mohammad Endless 
                        Debug.LogError ("End!");
					}


				}
			}
		}

    }
}
