﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;

public class EnemySpawner : MonoBehaviour {

    public int worldNumber;
	public int levelNumber;
	public End_Level_Script endLvlScriptHolder;
	public StoryMain_Script storyMainScriptHolder;
	public Worlds_InGame_Script worldsInGameScriptHolder;
	public Bell_Script bellScriptHolder;

	public LevelManifestRewardData levelRewardDataRef;

	// For Enemies spawn here trans and de-activate enemies source / parent trans
	public Transform transDeactivateEnemyParent;
	public Transform enemysParentTransHolder;

	public Transform worldsParentTrans_Real;
	public Transform worldsParentTrans_Test;

	private Transform currentLevelTrans;

	// Enemies - Human
	public GameObject EnemyObject_Comm_Fat;
	public GameObject EnemyObject_Comm_Thin;
	public GameObject EnemyObject_Comm_Muscle;
	public GameObject EnemyObject_Comm_Giant;
	public GameObject EnemyObject_HF;
	public GameObject EnemyObject_Barrel;

	// Enemies - Items
	public GameObject EnemyObject_Pipe;
	public GameObject EnemyObject_Elixir;
	public GameObject EnemyObject_Freeze;
	public GameObject EnemyObject_Heart;
	public GameObject EnemyObject_Hedgehog;
	public GameObject EnemyObject_Mustache;

	public GameObject EnemyObject_SuperMustache;
	public GameObject EnemyObject_BigBomb;
	public GameObject EnemyObject_LineBomb;

	public GameObject EnemyObject_Score_Item;
	public GameObject EnemyObject_SuperScore;
	public GameObject EnemyObject_PowerUp_Fist;
	public GameObject EnemyObject_PowerUp_Shield;

	private LevelManifest levelManifestRef;
	private List<Enemy> enemyManifest;
	private bool end_EnemiesFinished;

	private bool disableSeenTutorials;

//	private bool end_BossFinished;
//	private bool end_AllFinished;

	private float delaySpawnTime;

	private float timeOfNextSpawn;
	private float waitBeforeSpawns;
	private float waitBeforeBell;
	private float additionalIntroWait;
	private int enemyIndex;
	private int enemyTotalNumber;
	private int enemyTypeInt;

	private int bellCounter;
	private int storySetNumber;

	private LevelManifestUniversalData levelUniversalDataRef;

	void Start () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			Start_Campaign ();
		} else {
			this.enabled = false;
		}
	}

    void Start_Campaign()
    {
		// Hide enemy source trans obj (Previously in enemymaker test)
		transDeactivateEnemyParent.gameObject.SetActive (false);

		bellCounter = 0;
		storySetNumber = 0;

		// Allow gesture (Moved here from InGameController)
		StartCoroutine (GameplayGesture_Allow(1));

		if (PlayerData_Main.Instance != null) {
			worldNumber = PlayerData_Main.Instance.WorldNumber;
			levelNumber = PlayerData_Main.Instance.LevelNumber;

			PlayerData_Main.Instance.WorldNumber_Last = worldNumber;
			PlayerData_Main.Instance.LevelNumber_Last = levelNumber;

			additionalIntroWait = PlayerData_Main.Instance.introAdditionTime;
		}

        // Read from ui callback and fill world number and level number
		if (PlayerData_InGame.Instance.isThisTestLevel) {
			currentLevelTrans = worldsParentTrans_Test.GetChild (worldNumber - 1).GetChild (levelNumber - 1);
		} else {
			currentLevelTrans = worldsParentTrans_Real.GetChild (worldNumber - 1).GetChild (levelNumber - 1);
		}

		// Ref system for LevelEnemies just like the Level ManifestRef?

		levelManifestRef = currentLevelTrans.GetComponent<LevelManifest> ();

		enemyManifest = levelManifestRef.BuildManifest(currentLevelTrans);
//		PlayerData_InGame.Instance.P1_HF_ClingyHP = enemyManifest[enemyIndex].

		// Get Universal data as well as reward data
		levelUniversalDataRef = levelManifestRef.GetLevelManifestData (currentLevelTrans);
		levelRewardDataRef = levelManifestRef.GetLevelManifestRewardData (currentLevelTrans);

		// Setup level universal data
		GetUniversalData ();

		enemyTotalNumber = enemyManifest.Where(a=> (int)a.enemyType < 18).Count<Enemy>(); //count enemy ha
		PlayerData_InGame.Instance.stats_EnemiesTotalNumber = enemyTotalNumber;

		// Comment for android
		if (PlayerData_InGame.Instance.isThisNonBuildVersion) {
			StreamReader sr = new StreamReader ("Assets/Scripts/Editor Scripts/designervalues.txt");
			enemyIndex = int.Parse (sr.ReadToEnd ());
			sr.Close ();
		}

        //enemyIndex = 3;
        //Debug.Log(enemyIndex);

        if (enemyIndex > 0)
            timeOfNextSpawn = enemyManifest[enemyIndex-1].enemySpawnTime;
        
		// Yashar initialized
		end_EnemiesFinished = true;

//		end_BossFinished = false;
//		end_AllFinished = false;

		if (PlayerData_InGame.Instance.tuturialRemove) {
			disableSeenTutorials = true;
		}

		if (PlayerData_Main.Instance != null) {
			// Disable old tutorials (Based on first star of level)
			Tutorial_Disabler_NewestLevel ();

			// Enable ding
			BellDing_Enabler ();

			// Call from loading!!!
		} else {
			SpawnsSetup ();

			// Enable ding for playing without playermain
			bellScriptHolder.canDoDingOnly = true;
		}

		// Activate World
		worldsInGameScriptHolder.World_Activate (worldNumber - 1);
		// Yashar

        //araye enemy sort shavad bad az ezafe shodane item bar paye timespawn
    }

	public IEnumerator GameplayGesture_Allow (float delay) {
		yield return new WaitForSeconds (delay);
		PlayerData_InGame.Instance.gameplayGestureAllowed = true;
	}

	public IEnumerator GameplayGesture_Prevent () {
		yield return null;
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
	}

	public void Tutorial_Disabler_NewestLevel () {
//		Debug.LogError ("TUT ============== CHECK NEW");
		disableSeenTutorials = true;

		if (PlayerData_Main.Instance.PlayingALevelForTheFirstTime) {
			disableSeenTutorials = false;

//			Debug.LogError ("TUT ============== DISABLE NEW");
		}
	}

	public void BellDing_Enabler () {
		if (!PlayerData_Main.Instance.isFirstTimePlaying) {
			bellScriptHolder.canDoDingOnly = true;
		}
	}

	public void Tutorial_Disabler_FullCheck () {
		switch (PlayerData_Main.Instance.WorldNumber) {
		case 1:
			Tutorial_LevelData (PlayerData_Main.Instance.player_World1_LevelsDataArr);
			break;
		case 2:
			Tutorial_LevelData (PlayerData_Main.Instance.player_World2_LevelsDataArr);
			break;
		case 3:
			Tutorial_LevelData (PlayerData_Main.Instance.player_World3_LevelsDataArr);
			break;
		case 4:
			Tutorial_LevelData (PlayerData_Main.Instance.player_World4_LevelsDataArr);
			break;
		case 5:
			Tutorial_LevelData (PlayerData_Main.Instance.player_World5_LevelsDataArr);
			break;
		}
	}

	public void Tutorial_LevelData (LevelData_Class[] levelDataArr) {

//		Debug.LogError ("TUT ============== CHECK OLD");

		disableSeenTutorials = false;
		// This means first star of this is ON
		if (levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Level) {
			disableSeenTutorials = true;

//			Debug.LogError ("TUT ============== DISABLE! OLD");
		}
	}

	// Yashar (Should be the same in Endless)
	public void SpawnsSetup () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			delaySpawnTime = PlayerData_InGame.Instance.introAnimLength + additionalIntroWait + 1;
		} else {
			delaySpawnTime = 1;
		}
		end_EnemiesFinished = false;
		timeOfNextSpawn += -1.2F - delaySpawnTime;
	}

	public void GetUniversalData () {
		// HF Clingy Data
		PlayerData_InGame.Instance.P1_HF_ClingyHP = levelUniversalDataRef.hfStickyHealth;

		// HF Explosive Data
		PlayerData_InGame.Instance.P1_HF_ExplosionDamage = levelUniversalDataRef.hfxExplosionDamage;

		// To prevent Explode null on events
		if (PlayerData_InGame.Instance.P1_HF_ExplosionDamage == 0) {
			PlayerData_InGame.Instance.P1_HF_ExplosionDamage = 5;
		}

		// Barrel data
		PlayerData_InGame.Instance.P1_Barrel_Q_ExplodeDamage = levelUniversalDataRef.barrelQExplosionDamage;
		PlayerData_InGame.Instance.P1_Barrel_Q_HedgehogDamage = levelUniversalDataRef.barrelQHedgehogDamage;
		PlayerData_InGame.Instance.P1_Barrel_X_ExplodeDamage = levelUniversalDataRef.barrelXExplosionDamage;
		PlayerData_InGame.Instance.P1_Barrel_N_HeadUpTime = levelUniversalDataRef.barrelVulnerableTime;
		PlayerData_InGame.Instance.P1_Barrel_Q_HeadUpTime = levelUniversalDataRef.barrelQVulnerableTime;
		PlayerData_InGame.Instance.P1_Barrel_X_HeadUpTime = levelUniversalDataRef.barrelXVulnerableTime;
		PlayerData_InGame.Instance.missesThresholdPercent = levelUniversalDataRef.missesThresholdPercent;

//		Debug.LogWarning ("WAVE! " + levelUniversalDataRef.Wave [0]);

	}
	// Yashar

 	IEnumerator SpawnThis (GameObject newObj, int enemyNumber) {
		if (!disableSeenTutorials) {
			StartCoroutine (CheckStory ());
		}
		StartCoroutine (CheckBell ());

		GameObject newEnemy = Instantiate (newObj);

        Enemy newEnemyBuffer = newEnemy.GetComponent<Enemy>();

        newEnemyBuffer.enemyType = enemyManifest[enemyIndex].enemyType;
        //spawntime dar update estefade mishavad
        newEnemyBuffer.enemyMustache = enemyManifest[enemyIndex].enemyMustache;
        newEnemyBuffer.enemyHP = enemyManifest[enemyIndex].enemyHP;
        newEnemyBuffer.enemyDamage = enemyManifest[enemyIndex].enemyDamage;
        newEnemyBuffer.enemyLane = enemyManifest[enemyNumber].enemyLane;
        newEnemyBuffer.enemyMoveSpeed = enemyManifest[enemyNumber].enemyMoveSpeed;
        newEnemyBuffer.explosionDamage = enemyManifest[enemyIndex].explosionDamage;
        newEnemyBuffer.itemValue = enemyManifest[enemyIndex].itemValue;

        newEnemy.transform.SetParent (enemysParentTransHolder);
		enemyTypeInt = (int)enemyManifest [enemyNumber].enemyType;

		switch (enemyTypeInt) {
		case 0: 
		case 1: 
		case 2: 
		case 3: 
		case 4: 
		case 5: 
		case 6: 
		case 7: 
			StartCoroutine (newEnemy.GetComponent<EnemyMover_Common> ().ActiveDelay ());
			break;
		case 8: 
		case 9: 
		case 10: 
		case 11:
			StartCoroutine (newEnemy.GetComponent<EnemyMover_HF> ().ActiveDelay ());
			break;
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
			StartCoroutine (newEnemy.GetComponent<EnemyMover_Barrel> ().ActiveDelay ());
			break;
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
		case 38:
		case 39:
			Debug.LogError ("Enemy place holders");
			break;
		case 40:
		case 41:
		case 42:
		case 43:
		case 44:
		case 45:
			// Normal Mustache

		case 46:
			// Random Item

		case 47:
			// Super MustaCHE

		case 48:
			// Big Bomb

		case 49:
			// Line Bomb
		case 50:
			// Score_Item
		case 51:
			// SuperScore
		case 52:
			// PowerUp_Fist
		case 53:
			// PowerUp_Shield
			StartCoroutine (newEnemy.GetComponent<EnemyMover_Item> ().ActiveDelay ());
			break;
		default:
			Debug.LogError ("Enemy spawner Default");
			break;
		}

		// Old enemy select by non-switch
//		if (enemyTypeInt < 8) {
//			StartCoroutine (newEnemy.GetComponent<EnemyMover_Common> ().ActiveDelay ());
//		} else {
//			if (enemyTypeInt >= 8 && enemyTypeInt < 12) {
//				StartCoroutine (newEnemy.GetComponent<EnemyMover_HF> ().ActiveDelay ());
//			} else {
//				if (enemyTypeInt >= 12 && enemyTypeInt < 40) {
//					StartCoroutine (newEnemy.GetComponent<EnemyMover_Barrel> ().ActiveDelay ());
//				} else {
//					StartCoroutine (newEnemy.GetComponent<EnemyMover_Item> ().ActiveDelay ());
//				}
//			}
//		}

        yield return null;
	}

	IEnumerator ForceStory (int whatWorld, int whatLevel, int whatSet) {
		yield return null;
		storyMainScriptHolder.StartStory (whatWorld, whatLevel, whatSet, true);
	}

	IEnumerator CheckStory () {
		if (storySetNumber < levelUniversalDataRef.Story.Length) {
			if (enemyIndex == levelUniversalDataRef.Story [storySetNumber]) {
				
//				storyMainScriptHolder.StoryNumbers_SetNow (worldNumber, levelNumber, storySetNumber);
//				Debug.LogError ("WORLD = " + (worldNumber - 1) + " LEVEL = " + (levelNumber - 1) + " AND SEQ = " + storySetNumber);

				storyMainScriptHolder.StartStory (worldNumber, levelNumber, storySetNumber, true);

//				Debug.LogWarning ("STORY = " + enemyIndex);
				storySetNumber++;
			}
		}
		yield return null;
	}

	IEnumerator CheckBell () {
		yield return null;
		if (bellCounter < levelUniversalDataRef.Wave.Length) {
			if (enemyIndex + 1 == levelUniversalDataRef.Wave [bellCounter]) {
//				Debug.LogError ("Spawn time: " + (enemyManifest [enemyIndex + 1].enemySpawnTime - enemyManifest [enemyIndex].enemySpawnTime).ToString());

				// For delay before spawning last wave
				waitBeforeBell = enemyManifest [enemyIndex + 1].enemySpawnTime - enemyManifest [enemyIndex].enemySpawnTime;

				// Check for last wave
				if (bellCounter != levelUniversalDataRef.Wave.Length - 1) {
					bellScriptHolder.BellAnim_Call_Delayed (waitBeforeBell - 4, false);
				} else {
					bellScriptHolder.BellAnim_Call_Delayed (waitBeforeBell - 4, true);
				}

//				StartCoroutine ( bellScriptHolder.WaveTitleAnim_Call (true) );
				bellCounter++;

				// Delay after each bell (Use if you want to enforce delay)
				timeOfNextSpawn -= 1;
			}
		}
	}

	void CheckEndOfEnemies () {
		CheckLevelCompleteStar ();
	}

	void CheckLevelCompleteStar () {
		if (PlayerData_Main.Instance != null) {
			// Check world to find world NUMBER for levelDataArray
			switch (PlayerData_Main.Instance.WorldNumber) {
			case 1:
				Check_RewardStar (PlayerData_Main.Instance.player_World1_LevelsDataArr);
				break;
			case 2:
				Check_RewardStar (PlayerData_Main.Instance.player_World2_LevelsDataArr);
				break;
			case 3:
				Check_RewardStar (PlayerData_Main.Instance.player_World3_LevelsDataArr);
				break;
			case 4:
				Check_RewardStar (PlayerData_Main.Instance.player_World4_LevelsDataArr);
				break;
			case 5:
				Check_RewardStar (PlayerData_Main.Instance.player_World5_LevelsDataArr);
				break;
			}
		}
	}

	void Check_RewardStar (LevelData_Class[] levelDataArr) {
//		if (PlayerData_InGame.Instance.stats_Stars_Completed) {
//			if (!levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Level) {
//				CheckReward ();
//			} else {
//				Victory_StartEnd ();
//			}
//		}
	}
		
	void CheckReward () {
		Debug.Log ("REWARD NOW!!!!");
//		rewardMachineScriptHolder.gameObject.SetActive (true);
	}

	void Victory_StartEnd () {
		Debug.Log ("No Reward");
//		endLvlScriptHolder.EndCheck_Start ();
	}

	void BeforeSpawn_Delay () {
		end_EnemiesFinished = false;
	}

    void Update () {
		if (!end_EnemiesFinished) {
			if (!EventTrigger.isPaused && !EventTrigger.isFrozen) {
				timeOfNextSpawn += Time.deltaTime;

				if (timeOfNextSpawn >= enemyManifest [enemyIndex].enemySpawnTime) {
					
//					Debug.LogError ("Enemy Type = " + enemyManifest [enemyIndex].enemyType);

					switch (enemyManifest [enemyIndex].enemyType) {
					case Enemy.EnemyType.Fat1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));
						break;
					case Enemy.EnemyType.Fat2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));

						// Check tutorial when fat 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Fat) {
								StartCoroutine (ForceStory (1, 11, 0));
							}
						}

						break;
					case Enemy.EnemyType.Thin1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Thin, enemyIndex));
						break;
					case Enemy.EnemyType.Thin2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Thin, enemyIndex));

						// Check tutorial when thin 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Thin) {
								StartCoroutine (ForceStory (1, 12, 0));
							}
						}

						break;
					case Enemy.EnemyType.Muscle1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Muscle, enemyIndex));
						break;
					case Enemy.EnemyType.Muscle2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Muscle, enemyIndex));

						// Check tutorial when muscle 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle) {
								StartCoroutine (ForceStory (1, 13, 0));
							}
						}

						break;
					case Enemy.EnemyType.Giant1:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Giant, enemyIndex));
						break;
					case Enemy.EnemyType.Giant2:
						StartCoroutine (SpawnThis (EnemyObject_Comm_Giant, enemyIndex));

						// Check tutorial when giant 2 is first shown (Same for both normal AND endless)
						if (PlayerData_Main.Instance != null) {
							if (!PlayerData_Main.Instance.TutSeen_2ndCostume_Giant) {
								StartCoroutine (ForceStory (1, 14, 0));
							}
						}

						break;
					case Enemy.EnemyType.HF1:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF2:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF_X1:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.HF_X2:
						StartCoroutine (SpawnThis (EnemyObject_HF, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_X1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_X2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_Q1:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Barrel_Q2:
						StartCoroutine (SpawnThis (EnemyObject_Barrel, enemyIndex));
						break;
					case Enemy.EnemyType.Pipe:
						StartCoroutine (SpawnThis (EnemyObject_Pipe, enemyIndex));
						break;
					case Enemy.EnemyType.Elixir:
						StartCoroutine (SpawnThis (EnemyObject_Elixir, enemyIndex));
						break;
					case Enemy.EnemyType.Freeze:
						StartCoroutine (SpawnThis (EnemyObject_Freeze, enemyIndex));
						break;
					case Enemy.EnemyType.Heart:
						StartCoroutine (SpawnThis (EnemyObject_Heart, enemyIndex));
						break;
					case Enemy.EnemyType.Hedgehog:
						StartCoroutine (SpawnThis (EnemyObject_Hedgehog, enemyIndex));
						break;
					case Enemy.EnemyType.Mustache:
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;
					case Enemy.EnemyType.Random_Item:
						break;

					case Enemy.EnemyType.SuperMustache:
						StartCoroutine (SpawnThis (EnemyObject_SuperMustache, enemyIndex));
						break;
					case Enemy.EnemyType.BigBomb:
						StartCoroutine (SpawnThis (EnemyObject_BigBomb, enemyIndex));
						break;
					case Enemy.EnemyType.LineBomb:
						// TODO: Should spawn Bomb Enemy
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;

					case Enemy.EnemyType.Score_Item:
						// TODO: Should spawn Bomb Enemy
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;
					case Enemy.EnemyType.SuperScore:
						// TODO: Should spawn Bomb Enemy
						StartCoroutine (SpawnThis (EnemyObject_Mustache, enemyIndex));
						break;
					case Enemy.EnemyType.PowerUp_Fist:
						StartCoroutine (SpawnThis (EnemyObject_PowerUp_Fist, enemyIndex));
						break;
					case Enemy.EnemyType.PowerUp_Shield:
						StartCoroutine (SpawnThis (EnemyObject_PowerUp_Shield, enemyIndex));
						break;
					default:
//						StartCoroutine (SpawnThis (EnemyObject_Comm_Fat, enemyIndex));
						Debug.LogError ("WTF Default spawn");
						break;
					}

//					Debug.LogError ("The ENEMY number: " + enemyIndex + " out of " + (enemyManifest.Count - 1));

					//akharin enemy ra ta abad tekrar mikonad
					if (enemyIndex < enemyManifest.Count - 1) {
						enemyIndex++;
					} else {
						Debug.LogError ("End!");
						end_EnemiesFinished = true;

						PlayerData_InGame.Instance.lastEnemyWasSpawned = true;

						// No more period checks
//						endLvlScriptHolder.EndCheck_Start ();
				
						// End world background anims / close doors
						StartCoroutine (worldsInGameScriptHolder.World_EndIt ());
//						timeOfNextSpawn -= 5;
						// OLD
//						CheckEndOfEnemies ();
					}
				}
			}
		}
    }
}
