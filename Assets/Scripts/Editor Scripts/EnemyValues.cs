﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemyValues {

    // General Info
    public Enemy.EnemyType enemyType;
    public float enemySpawnTime;
    public int enemyMustache;

    // Health Info
    public string enemyHP; //arrows
    public int enemyDamage;

    // Mover Info
    public Enemy.LaneTypes enemyLane;
    public float enemyMoveSpeed; //also itemspeed

    // Explosion Info
    public int explosionDamage;

    // Item Info
    public int itemValue;

    //addition to enemy
    public float itemTimeVariation;
    public int Hp;
    public string randomPool;
    public string itemRandomPool;

    //needed fields for time and id and alarm
    [HideInInspector]
    public string id;
    [HideInInspector]
    public string AbsoluteTime;
    [HideInInspector]
    public bool flagAlarm;
}
