﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class LevelEnemies : MonoBehaviour {

    public int[] Wave;
	public int[] Story;

    #region Variables
    //general properties
    public float fatSpeed;
    public int fatDamage;
    public int fatMustacheDrop;
    public int fatHealth;
    //....
    public float thinSpeed;
    public int thinDamage;
    public int thinMustacheDrop;
    public int thinHealth;
    //....
    public float muscleSpeed;
    public int muscleDamage;
    public int muscleMustacheDrop;
    public int muscleHealth;
    //....
    public float giantSpeed;
    public int giantDamage;
    public int giantMustacheDrop;
    public int giantHealth;
    //....
    public float hfSpeed;
    public int hfDamage;
    public int hfMustacheDrop;
    public int hfHealth;
    //....
    public float hfxSpeed;
    public int hfxDamage;
    public int hfxMustacheDrop;
    public int hfxHealth;
    //....
    public float barrelSpeed;
    public int barrelDamage;
    public int barrelMustacheDrop;
    public int barrelHealth;
    //....
    public float barrelxSpeed;
    public int barrelxDamage;
    public int barrelxMustacheDrop;
    public int barrelxHealth;
    //....
    public float barrelqSpeed;
    public int barrelqDamage;
    public int barrelqMustacheDrop;
    public int barrelqHealth;
    #endregion

    #region Additional Properties
    public int hfStickyHealth;

    public int hfxExplosionDamage;

    public float barrelVulnerableTime;

    public int barrelXExplosionDamage;
    public float barrelXVulnerableTime;

    public int barrelQExplosionDamage;
    public float barrelQVulnerableTime;
    public int barrelQHedgehogDamage;
    #endregion

    #region Reward Properties
    public int[] Mustache;
    public Chest.ChestType[] RandomChest;
    public int[] Items;
    public FixedChest[] FixedChest;
    #endregion

    public float missesThresholdPercent;

    public List<EnemyValues> enemyList;

    public LevelEnemies()
    {
        enemyList = new List<EnemyValues>() { new EnemyValues { } };
    }
    public void Duplicate(string index)
    {
        EnemyValues ev;
        EnemyValues temp=new EnemyValues();
        temp = enemyList[int.Parse(index)];
        ev = new EnemyValues {
            enemyDamage = temp.enemyDamage,
            enemyLane = temp.enemyLane,
            enemyMoveSpeed = temp.enemyMoveSpeed,
            enemyHP = temp.enemyHP,
            enemyMustache = temp.enemyMustache,
            enemySpawnTime = temp.enemySpawnTime,
            enemyType = temp.enemyType,
            explosionDamage = temp.explosionDamage,
            Hp = temp.Hp,
            itemRandomPool = temp.itemRandomPool,
            itemTimeVariation = temp.itemTimeVariation,
            itemValue = temp.itemValue,
            randomPool = temp.randomPool            
        };

        enemyList.Insert(int.Parse(index) + 1, ev);
    }
    public void DuplicateRange(int from , int to)
    {
        EnemyValues ev;
        for (int i = from; i <= to; i++)
        {
            EnemyValues temp = new EnemyValues();
            temp = enemyList[i];
            ev = new EnemyValues
            {
                enemyDamage = temp.enemyDamage,
                enemyLane = temp.enemyLane,
                enemyMoveSpeed = temp.enemyMoveSpeed,
                enemyHP = temp.enemyHP,
                enemyMustache = temp.enemyMustache,
                enemySpawnTime = temp.enemySpawnTime,
                enemyType = temp.enemyType,
                explosionDamage = temp.explosionDamage,
                Hp = temp.Hp,
                itemRandomPool = temp.itemRandomPool,
                itemTimeVariation = temp.itemTimeVariation,
                itemValue = temp.itemValue,
                randomPool = temp.randomPool
            };
            enemyList.Insert(to-from + i+1, ev);
        }
    }

    public void MoveRange(int moveRangeFrom, int moveRangeTo, int moveTarget)
    {
        //aval duplicate kon on range ro
        //bad remove kon
        //move target be in soorat e ke ba index avalie mohasebe she
        //test:list 20 taee az 3-7 move kon be 16 --> 3-7(5ta ro be 16+5 duplicate kon) bad 3-7 ro remove kon - halat aval
        //test:list 20 taee az 13-15 be 5 --> 13-15( 3 ta ro be 5 dupliacate kon) bad 13+3 - 15+3 ro remove kon - halat dovom
        //test:list 20 taee az 7-11 be 10(nabayad kar kone)
        if ( moveRangeTo< moveTarget)
        {
            EnemyValues ev;
            for (int i = moveRangeFrom; i <= moveRangeTo; i++)
            {
                EnemyValues temp = new EnemyValues();
                temp = enemyList[moveRangeFrom];
                ev = new EnemyValues
                {
                    enemyDamage = temp.enemyDamage,
                    enemyLane = temp.enemyLane,
                    enemyMoveSpeed = temp.enemyMoveSpeed,
                    enemyHP = temp.enemyHP,
                    enemyMustache = temp.enemyMustache,
                    enemySpawnTime = temp.enemySpawnTime,
                    enemyType = temp.enemyType,
                    explosionDamage = temp.explosionDamage,
                    Hp = temp.Hp,
                    itemRandomPool = temp.itemRandomPool,
                    itemTimeVariation = temp.itemTimeVariation,
                    itemValue = temp.itemValue,
                    randomPool = temp.randomPool
                };
                enemyList.Insert(moveTarget+1, ev);
                enemyList.RemoveAt(moveRangeFrom);
            }
        }
        else if(moveRangeFrom > moveTarget)
        {
            int c = 0;
            EnemyValues ev;
            for (int i = moveRangeFrom; i <= moveRangeTo; i++,c++)
            {
                EnemyValues temp = new EnemyValues();
                temp = enemyList[moveRangeFrom+c];
                ev = new EnemyValues
                {
                    enemyDamage = temp.enemyDamage,
                    enemyLane = temp.enemyLane,
                    enemyMoveSpeed = temp.enemyMoveSpeed,
                    enemyHP = temp.enemyHP,
                    enemyMustache = temp.enemyMustache,
                    enemySpawnTime = temp.enemySpawnTime,
                    enemyType = temp.enemyType,
                    explosionDamage = temp.explosionDamage,
                    Hp = temp.Hp,
                    itemRandomPool = temp.itemRandomPool,
                    itemTimeVariation = temp.itemTimeVariation,
                    itemValue = temp.itemValue,
                    randomPool = temp.randomPool
                };
                enemyList.Insert(moveTarget+c , ev);
                enemyList.RemoveAt(moveRangeFrom+1+c);
            }

        }
    }
}

[System.Serializable]
public class FixedChest
{
    public Chest.ChestType type;
    public int[] FixedItems;
}