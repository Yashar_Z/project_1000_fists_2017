﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class LevelManifest : MonoBehaviour {

    //khondan har enemy az component editor
    //sakhtan list az in enemy ha
    LevelEnemies le;

	public List<Enemy> BuildManifest(Transform t){

        le = t.GetComponent<LevelEnemies>();

        List<Enemy> localEnemyList = new List<Enemy>();

        for (int i = 0; i < le.enemyList.Count; i++)
        {
            localEnemyList.Add(new Enemy
            {
                enemyType = le.enemyList[i].enemyType,
                enemySpawnTime = GetEnemySpawnTime(le.enemyList[i].enemyType, le.enemyList[i].itemTimeVariation, float.Parse(le.enemyList[i].AbsoluteTime)),
                enemyMustache = GetEnemyMustache(le.enemyList[i].enemyType, le),
                enemyHP = FixEnemyHealth(le.enemyList[i].enemyHP, le.enemyList[i].Hp, le.enemyList[i].randomPool),
                enemyDamage = GetEnemyDamage(le.enemyList[i].enemyType, le),
                enemyLane = le.enemyList[i].enemyLane,
                enemyMoveSpeed = GetEnemyMoveSpeed(le.enemyList[i].enemyType, le, le.enemyList[i].enemyMoveSpeed),
                explosionDamage = GetExplosionDamage(le.enemyList[i].enemyType, le),
                itemValue = le.enemyList[i].itemValue,
                hedgehogDamage = GetHedgehogDamage(le.enemyList[i].enemyType,le),
                vulnerableTime = GetVulnerableTime(le.enemyList[i].enemyType, le),
                stickyHealth = GetStickyHealth(le.enemyList[i].enemyType, le)

				// Used dictionary in endless
//				enemyScore = GetEnemyScore (le.enemyList[i].enemyType)
            });
        }    
        return localEnemyList;
	}

    public LevelManifestUniversalData GetLevelManifestData(Transform t)
    {
        le = t.GetComponent<LevelEnemies>();
        LevelManifestUniversalData levelUniversalData = new LevelManifestUniversalData();

        levelUniversalData.barrelQExplosionDamage = le.barrelQExplosionDamage;
        levelUniversalData.barrelQHedgehogDamage = le.barrelQHedgehogDamage;
        levelUniversalData.barrelQVulnerableTime = le.barrelQVulnerableTime;
        levelUniversalData.barrelVulnerableTime = le.barrelVulnerableTime;
        levelUniversalData.barrelXExplosionDamage = le.barrelXExplosionDamage;
        levelUniversalData.barrelXVulnerableTime = le.barrelXVulnerableTime;
        levelUniversalData.hfStickyHealth = le.hfStickyHealth;
        levelUniversalData.hfxExplosionDamage = le.hfxExplosionDamage;
		levelUniversalData.Wave = le.Wave;
		levelUniversalData.Story = le.Story;

        levelUniversalData.missesThresholdPercent = le.missesThresholdPercent;

        return levelUniversalData;
    }

    public LevelManifestRewardData GetLevelManifestRewardData(Transform t)
    {
        le = t.GetComponent<LevelEnemies>();
        LevelManifestRewardData RewardData = new LevelManifestRewardData();

        RewardData.FixedChests = le.FixedChest;
        RewardData.Items = le.Items;
        RewardData.Mustache = le.Mustache;
        RewardData.RandomChests = le.RandomChest;

        return RewardData;
    }


    private float GetEnemySpawnTime(Enemy.EnemyType enemyType, float itemTimeVariation, float absoluteTime)
    {
        float spawnTime = 0;
        //
		switch (enemyType) {
		case Enemy.EnemyType.Fat1:
		case Enemy.EnemyType.Fat2:
		case Enemy.EnemyType.Thin1:
		case Enemy.EnemyType.Thin2:
		case Enemy.EnemyType.Muscle1:
		case Enemy.EnemyType.Muscle2:
		case Enemy.EnemyType.Giant1:
		case Enemy.EnemyType.Giant2:
		case Enemy.EnemyType.HF1:
		case Enemy.EnemyType.HF2:
		case Enemy.EnemyType.HF_X1:
		case Enemy.EnemyType.HF_X2:
		case Enemy.EnemyType.Barrel1:
		case Enemy.EnemyType.Barrel2:
		case Enemy.EnemyType.Barrel_X1:
		case Enemy.EnemyType.Barrel_X2:
		case Enemy.EnemyType.Barrel_Q1:
		case Enemy.EnemyType.Barrel_Q2:
			spawnTime = absoluteTime;
			break;
		case Enemy.EnemyType.Heart:
		case Enemy.EnemyType.Pipe:
		case Enemy.EnemyType.Hedgehog:
		case Enemy.EnemyType.Mustache:
		case Enemy.EnemyType.Elixir:
		case Enemy.EnemyType.Freeze:
		case Enemy.EnemyType.Random_Item:
		case Enemy.EnemyType.SuperMustache:
		case Enemy.EnemyType.BigBomb:
		case Enemy.EnemyType.LineBomb:
		case Enemy.EnemyType.Score_Item:
		case Enemy.EnemyType.SuperScore:
		case Enemy.EnemyType.PowerUp_Fist:
		case Enemy.EnemyType.PowerUp_Shield:
            //get a random number between -variation time and +variantiont time
			spawnTime = absoluteTime + GetARandomNumberBetween (itemTimeVariation);
			break;
		default:
			Debug.LogError ("Default Error!");
			break;
		}

        return spawnTime;
    }
    private float GetARandomNumberBetween(float itemTimeVariation)
    {
        System.Random rng = new System.Random();

        return ((float)((rng.NextDouble() * 2.0 - 1.0) * itemTimeVariation)); 
    }
    private string FixEnemyHealth(string enemyHP, int hp, string randomPool)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(enemyHP);

        //aval tamame jahaye khali ba * por she
        if (hp - enemyHP.Length<0) Debug.LogError("hp az arrow kamtare");
        string starRepeat = new string('*', hp - enemyHP.Length);
        sb.Append(starRepeat);

        //az random pool bekhoone va * haro ba yeki az charachter haye random pool avaz kone
        for (int i = 0; i < sb.Length; i++)
        {
            if (sb[i] == '*')
            {
                sb[i] = GetRandomCharacter(randomPool, new System.Random(Guid.NewGuid().GetHashCode()));
            }
        }

        sb.Replace('l', 'L').Replace('u', 'U').Replace('r', 'R').Replace('d', 'D').Replace('/', '|').Replace('2', 'v')
    .Replace('4', '<').Replace('8', '^').Replace('6', '>');
        return sb.ToString();
    }
    public static char GetRandomCharacter(string randomPool, System.Random rng)
    {
        int index = rng.Next(randomPool.Length);
        return randomPool[index];
    }
    private int GetEnemyMustache(Enemy.EnemyType enemyType, LevelEnemies le)
    {
        int mustache=0;
		switch (enemyType) {
		case Enemy.EnemyType.Fat1:
		case Enemy.EnemyType.Fat2:
			mustache = le.fatMustacheDrop;
			break;
		case Enemy.EnemyType.Thin1:
		case Enemy.EnemyType.Thin2:
			mustache = le.thinMustacheDrop;
			break;
		case Enemy.EnemyType.Muscle1:
		case Enemy.EnemyType.Muscle2:
			mustache = le.muscleMustacheDrop;
			break;
		case Enemy.EnemyType.Giant1:
		case Enemy.EnemyType.Giant2:
			mustache = le.giantMustacheDrop;
			break;
		case Enemy.EnemyType.HF1:
		case Enemy.EnemyType.HF2:
			mustache = le.hfMustacheDrop;
			break;
		case Enemy.EnemyType.HF_X1:
		case Enemy.EnemyType.HF_X2:
			mustache = le.hfxMustacheDrop;
			break;
		case Enemy.EnemyType.Barrel1:
		case Enemy.EnemyType.Barrel2:
			mustache = le.barrelMustacheDrop;
			break;
		case Enemy.EnemyType.Barrel_X1:
		case Enemy.EnemyType.Barrel_X2:
			mustache = le.barrelxMustacheDrop;
			break;
		case Enemy.EnemyType.Barrel_Q1:
		case Enemy.EnemyType.Barrel_Q2:
			mustache = le.barrelqMustacheDrop;
			break;
		case Enemy.EnemyType.Heart:
		case Enemy.EnemyType.Pipe:
		case Enemy.EnemyType.Hedgehog:
		case Enemy.EnemyType.Mustache:
		case Enemy.EnemyType.Elixir:
		case Enemy.EnemyType.Freeze:
		case Enemy.EnemyType.Random_Item:
		case Enemy.EnemyType.SuperMustache:
		case Enemy.EnemyType.BigBomb:
		case Enemy.EnemyType.LineBomb:
		case Enemy.EnemyType.Score_Item:
		case Enemy.EnemyType.SuperScore:
		case Enemy.EnemyType.PowerUp_Fist:
		case Enemy.EnemyType.PowerUp_Shield:
			break;
		default:
			Debug.LogError ("Default Error");
			mustache = 0;
			break;
		}
        return mustache;
    }
    private float GetEnemyMoveSpeed(Enemy.EnemyType enemyType, LevelEnemies le, float enemyMoveSpeed)
    {
        float speed = enemyMoveSpeed;
		switch (enemyType) {
		case Enemy.EnemyType.Fat1:
		case Enemy.EnemyType.Fat2:
			speed = le.fatSpeed;
			break;
		case Enemy.EnemyType.Thin1:
		case Enemy.EnemyType.Thin2:
			speed = le.thinSpeed;
			break;
		case Enemy.EnemyType.Muscle1:
		case Enemy.EnemyType.Muscle2:
			speed = le.muscleSpeed;
			break;
		case Enemy.EnemyType.Giant1:
		case Enemy.EnemyType.Giant2:
			speed = le.giantSpeed;
			break;
		case Enemy.EnemyType.HF1:
		case Enemy.EnemyType.HF2:
			speed = le.hfSpeed;
			break;
		case Enemy.EnemyType.HF_X1:
		case Enemy.EnemyType.HF_X2:
			speed = le.hfxSpeed;
			break;
		case Enemy.EnemyType.Barrel1:
		case Enemy.EnemyType.Barrel2:
			speed = le.barrelSpeed;
			break;
		case Enemy.EnemyType.Barrel_X1:
		case Enemy.EnemyType.Barrel_X2:
			speed = le.barrelxSpeed;
			break;
		case Enemy.EnemyType.Barrel_Q1:
		case Enemy.EnemyType.Barrel_Q2:
			speed = le.barrelqSpeed;
			break;
		case Enemy.EnemyType.Heart:
		case Enemy.EnemyType.Pipe:
		case Enemy.EnemyType.Hedgehog:
		case Enemy.EnemyType.Mustache:
		case Enemy.EnemyType.Elixir:
		case Enemy.EnemyType.Freeze:
		case Enemy.EnemyType.Random_Item:
		case Enemy.EnemyType.SuperMustache:
		case Enemy.EnemyType.BigBomb:
		case Enemy.EnemyType.LineBomb:
		case Enemy.EnemyType.Score_Item:
		case Enemy.EnemyType.SuperScore:
		case Enemy.EnemyType.PowerUp_Fist:
		case Enemy.EnemyType.PowerUp_Shield:
//				Debug.LogError ("enemyType = " + enemyType + " is at speed of " + speed);
			break;
		default:
			Debug.LogError ("Default Error");
			speed = 1;
			break;
		}
        return speed;
    }

    private int GetEnemyDamage(Enemy.EnemyType enemyType,LevelEnemies levelEnemies)
    {
        int damage =0;
		switch (enemyType) {
		case Enemy.EnemyType.Fat1:
		case Enemy.EnemyType.Fat2:
			damage = levelEnemies.fatDamage;
			break;
		case Enemy.EnemyType.Thin1:
		case Enemy.EnemyType.Thin2:
			damage = levelEnemies.thinDamage;
			break;
		case Enemy.EnemyType.Muscle1:
		case Enemy.EnemyType.Muscle2:
			damage = levelEnemies.muscleDamage;
			break;
		case Enemy.EnemyType.Giant1:
		case Enemy.EnemyType.Giant2:
			damage = levelEnemies.giantDamage;
			break;
		case Enemy.EnemyType.HF1:
		case Enemy.EnemyType.HF2:
			damage = levelEnemies.hfDamage;
			break;
		case Enemy.EnemyType.HF_X1:
		case Enemy.EnemyType.HF_X2:
			damage = levelEnemies.hfxDamage;
			break;
		case Enemy.EnemyType.Barrel1:
		case Enemy.EnemyType.Barrel2:
			damage = levelEnemies.barrelDamage;
			break;
		case Enemy.EnemyType.Barrel_X1:
		case Enemy.EnemyType.Barrel_X2:
			damage = levelEnemies.barrelxDamage;
			break;
		case Enemy.EnemyType.Barrel_Q1:
		case Enemy.EnemyType.Barrel_Q2:
			damage = levelEnemies.barrelqDamage;
			break;
		case Enemy.EnemyType.Heart:
		case Enemy.EnemyType.Pipe:
		case Enemy.EnemyType.Hedgehog:
		case Enemy.EnemyType.Mustache:
		case Enemy.EnemyType.Elixir:
		case Enemy.EnemyType.Freeze:
		case Enemy.EnemyType.Random_Item:
		case Enemy.EnemyType.SuperMustache:
		case Enemy.EnemyType.BigBomb:
		case Enemy.EnemyType.LineBomb:
		case Enemy.EnemyType.Score_Item:
		case Enemy.EnemyType.SuperScore:
		case Enemy.EnemyType.PowerUp_Fist:
		case Enemy.EnemyType.PowerUp_Shield:
			break;
		default:
			Debug.LogError ("Default Error");
			damage = 0;
			break;
		}
        return damage;
    }

	// Used dictionary in endless
//	private int GetEnemyScore (Enemy.EnemyType enemyType)
//	{
//		int score = 0;
//		switch (enemyType) {
//		case Enemy.EnemyType.Fat1:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Fat2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Thin1:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Thin2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Muscle1:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Muscle2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Giant1:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Giant2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.HF1:
//		case Enemy.EnemyType.HF2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.HF_X1:
//		case Enemy.EnemyType.HF_X2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Barrel1:
//		case Enemy.EnemyType.Barrel2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Barrel_X1:
//		case Enemy.EnemyType.Barrel_X2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Barrel_Q1:
//		case Enemy.EnemyType.Barrel_Q2:
//			score = 10;
//			break;
//		case Enemy.EnemyType.Heart:
//		case Enemy.EnemyType.Pipe:
//		case Enemy.EnemyType.Hedgehog:
//		case Enemy.EnemyType.Mustache:
//		case Enemy.EnemyType.Elixir:
//		case Enemy.EnemyType.Freeze:
//		case Enemy.EnemyType.Random_Item:
//		case Enemy.EnemyType.SuperMustache:
//		case Enemy.EnemyType.BigBomb:
//		case Enemy.EnemyType.LineBomb:
//		case Enemy.EnemyType.Score_Item:
//		case Enemy.EnemyType.SuperScore:
//		case Enemy.EnemyType.PowerUp_Fist:
//		case Enemy.EnemyType.PowerUp_Shield:
//			break;
//		default:
//			Debug.LogError ("Default Error");
//			score = 0;
//			break;
//		}
//		return score;
//	}

    private int GetExplosionDamage(Enemy.EnemyType enemyType, LevelEnemies le)
    {
        int dmg;
        switch (enemyType)
        {
            case Enemy.EnemyType.HF_X1:
            case Enemy.EnemyType.HF_X2:
                dmg = le.hfxExplosionDamage;
                break;
            case Enemy.EnemyType.Barrel_X1:
            case Enemy.EnemyType.Barrel_X2:
                dmg = le.barrelXExplosionDamage;
                break;
            case Enemy.EnemyType.Barrel_Q1:
            case Enemy.EnemyType.Barrel_Q2:
                dmg = le.barrelQExplosionDamage;
                break;
            default:
                dmg = 0;
                break;
        }
        return dmg;
    }
    private int GetHedgehogDamage(Enemy.EnemyType enemyType, LevelEnemies le)
    {
        int dmg;
        switch (enemyType)
        {
            case Enemy.EnemyType.Barrel_Q1:
            case Enemy.EnemyType.Barrel_Q2:
                dmg = le.barrelQHedgehogDamage;
                break;
            default:
                dmg = 0;
                break;
        }
        return dmg;
    }
    private float GetVulnerableTime(Enemy.EnemyType enemyType, LevelEnemies le)
    {
        float time;
        switch (enemyType)
        {
            case Enemy.EnemyType.Barrel1:
            case Enemy.EnemyType.Barrel2:
                time = le.barrelVulnerableTime;
                break;
            case Enemy.EnemyType.Barrel_X1:
            case Enemy.EnemyType.Barrel_X2:
                time = le.barrelXVulnerableTime;
                break;
            case Enemy.EnemyType.Barrel_Q1:
            case Enemy.EnemyType.Barrel_Q2:
                time = le.barrelQVulnerableTime;
                break;
            default:
                time = 0f;
                break;
        }
        return time;
    }
    private int GetStickyHealth(Enemy.EnemyType enemyType, LevelEnemies le)
    {
        int hp;
        switch (enemyType)
        {
            case Enemy.EnemyType.HF1:
            case Enemy.EnemyType.HF2:
                hp = le.hfStickyHealth;
                break;
            default:
                hp = 0;
                break;
        }
        return hp;
    }
}
