﻿using System.Collections.Generic;

public class LevelManifestRewardData
{
    //this two goes to container chest 	ChestArtType General
    public int[] Mustache;
    public int[] Items;
    //this two can be any of the 3 type of chests
    public Chest.ChestType[] RandomChests;
    public FixedChest[] FixedChests;
}