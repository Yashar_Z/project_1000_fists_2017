﻿using UnityEngine;
using System.Collections;

public class LevelManifestUniversalData
{
	public int[] Wave;
	public int[] Story;

    public int hfStickyHealth;

    public int hfxExplosionDamage;

    public float barrelVulnerableTime;

    public int barrelXExplosionDamage;
    public float barrelXVulnerableTime;

    public int barrelQExplosionDamage;
    public float barrelQVulnerableTime;
    public int barrelQHedgehogDamage;

    public float missesThresholdPercent;
}