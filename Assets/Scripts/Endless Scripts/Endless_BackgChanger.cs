﻿using UnityEngine;
using System.Collections;

public class Endless_BackgChanger : MonoBehaviour {

	public UnityStandardAssets.ImageEffects.BlurOptimized blurOptimHolder;
	public Worlds_InGame_Script worldsInGameScript;

	public ParticleSystem particleBackchangeDarkness;

	public bool isBlur_Increase;
	public bool isBlur_Decrease;

	public float floatChangeSpeed;

	private float floatBlurSize_Curr = 0;
	private float floatBlurSize_Target = 0;
	private float floatBlurSize_Distance;

	private bool canChangeSpeed;

	public void BackgroundChanger_Setup () {
		BackgroundChanger_Enabler (true);

		StartCoroutine (Blur_StartProcess ());
	}

	public void BackgroundChanger_Exit () {
		BackgroundChanger_Enabler (false);
	}

	void BackgroundChanger_Enabler (bool isActive) {
		this.enabled = isActive;
		this.gameObject.SetActive (isActive);
	}

    
    private int lastMusicArrayNum  = 8;

	IEnumerator Blur_StartProcess () {
		yield return null;

        // SOUND EFFECTS - Music Change After Blur (done) 
        // 8 music cinema night e
        //1 music cinema rooz e
        //2 music garage
        // 3 music grand hotel
        // 11 music high tempo for endless
        // random beine in 4 ta biad amma ghabli hatman nabayd bashe
        // masalan age 3 bood beyne 1 2 8 random enbtekhab she
        if (MusicManager.Instance != null)
        {
            int[] randomMusic;
            switch (lastMusicArrayNum)
            {
                case 1:
                    randomMusic = new int[] { 2, 3, 8 , 11 };
                    break;
                case 2:
                    randomMusic = new int[] { 1, 3, 8 , 11 };
                    break;
                case 3:
                    randomMusic = new int[] { 1, 2, 8 ,11 };
                    break;
                case 8:
                    randomMusic = new int[] { 1, 2, 3 ,11};
                    break;
                case 11:
                    randomMusic = new int[] { 1, 2, 3, 8 };
                    break;
                default:
                    Debug.LogError("default switch");
                    randomMusic = new int[] { 1, 2, 3, 8 };
                    break;
            }

            var musicToBlendInto = randomMusic[Random.Range(0, randomMusic.Length)];
            lastMusicArrayNum = musicToBlendInto;
            MusicManager.Instance.BlendMusic(musicToBlendInto);
        }

        // Blur Stuff
        //		blurOptimHolder.blurIterations = 1;
        blurOptimHolder.enabled = true;

		canChangeSpeed = true;
		floatChangeSpeed = 8;
		floatBlurSize_Curr = 0;
		floatBlurSize_Target = 10;
		floatBlurSize_Distance = (floatBlurSize_Target - floatBlurSize_Curr) / 3;

		isBlur_Increase = true;
		isBlur_Decrease = false;
	}

	IEnumerator Blur_MidProcess () {
		isBlur_Increase = false;

		// Blur Stuff
//		blurOptimHolder.blurIterations = 1;
//		blurOptimHolder.enabled = true;

		canChangeSpeed = true;
		floatChangeSpeed = 5;
//		floatBlurSize_Curr = 0;
		floatBlurSize_Target = 0;
//		floatBlurSize_Distance = (floatBlurSize_Target - floatBlurSize_Curr) / 3;

		yield return new WaitForSeconds (0.1F);

		// Change of background happens here! 
		worldsInGameScript.World_ChangeForEndless ();

		yield return new WaitForSeconds (0.1F);

		isBlur_Decrease = true;
	}

	IEnumerator Blur_EndProcess () {
		yield return null;

		// Blur Stuff
//		blurOptimHolder.blurIterations = 1;
		blurOptimHolder.enabled = false;

		isBlur_Increase = false;
		isBlur_Decrease = false;

		BackgroundChanger_Exit ();
	}

	public void AnimEvent_HeadStartFeed () {
		PlayerData_InGame.Instance.hudScriptHolder.endlessScoreController.HUD_ScoreAmountUpdate ();
		PlayerData_InGame.Instance.hudScriptHolder.endlessScoreController.HUD_UpdateMultiText ();
	}

	// Update is called once per frame
	void Update () {
		if (!EventTrigger.isPaused) {
			if (isBlur_Increase) {
				floatBlurSize_Curr = Mathf.MoveTowards (floatBlurSize_Curr, floatBlurSize_Target, Time.deltaTime * floatChangeSpeed);
				blurOptimHolder.blurSize = floatBlurSize_Curr;

				if (canChangeSpeed) {
					if ((floatBlurSize_Target - floatBlurSize_Curr) < 2) {
						floatChangeSpeed = floatChangeSpeed * 0.6F;
//					blurOptimHolder.blurIterations = 2;
						canChangeSpeed = false;

						// Play particle before going full dark
						particleBackchangeDarkness.Play ();
					}
				}

				if (floatBlurSize_Curr == floatBlurSize_Target) {
					StartCoroutine (Blur_MidProcess ());
				}
			}

			if (isBlur_Decrease) {
				floatBlurSize_Curr = Mathf.MoveTowards (floatBlurSize_Curr, floatBlurSize_Target, Time.deltaTime * floatChangeSpeed);
				blurOptimHolder.blurSize = floatBlurSize_Curr;

				if (canChangeSpeed) {
					if ((floatBlurSize_Target - floatBlurSize_Curr) < 2) {
						floatChangeSpeed = floatChangeSpeed * 1.5F;
//					blurOptimHolder.blurIterations = 2;
						canChangeSpeed = false;
					}
				}

				if (floatBlurSize_Curr == floatBlurSize_Target) {
					StartCoroutine (Blur_EndProcess ());
				}
			}
		}
	}
}
