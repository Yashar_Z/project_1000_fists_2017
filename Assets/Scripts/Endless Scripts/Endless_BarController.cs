﻿using UnityEngine;
using System.Collections;

public class Endless_BarController : MonoBehaviour {

	static public Endless_BarController Instance;

	public Popup_EndlessRewards_Script popUpEndlessRewardsScript;
	public Endless_PotionsRewardsSource endlessPotionRewardsSource;

	public Animation animEndBarController;
	public Animation animRoadmapPhoneHolder;

	public AnimationClip[] animClipEndBarArr;
	public AnimationClip[] animClipRoadmapArr;

	public Roadmap_PackageScript[] roadmapPackageArr;

	public Endless_BarUpdater endlessBarUpdater;

	public LevelManifestRewardData rewardEndless_LevelManifest;

	public Color[] colorRoadmapCardsArr;
	public Sprite[] spriteRoadmapItemsArr;

	public UnityEngine.UI.ScrollRect scrollRoadmapPhone;

	public Transform transRoadmapTopParentHolder;

	// For canvas and graphics raycaster
	public Canvas canvasPhoneRoadmapHolder;
	public GameObject objPhoneRoadmapHolder;

	public ParticleSystem particleTargetLevelUpSpark;
	public ParticleSystem particleTargetLevelUpCircle;

	void Awake () {
		Instance = this;
	}

	public void Pressed_RoadmapPhone_Enter () {
		if (PlayerData_InGame.Instance != null) {
			transRoadmapTopParentHolder.localPosition = new Vector2 (0, -10);
		} else {
			// Move change to make up for difference in menu phone
			transRoadmapTopParentHolder.localPosition = new Vector2 (0, 69);
		}

		MoveIn_RoadmapPhone ();

		animRoadmapPhoneHolder.clip = animClipRoadmapArr [0];
		animRoadmapPhoneHolder.Play ();

		StartCoroutine (RoadmapPhone_Setup ());

		// SOUNDD EFFECTS - Rewards Roadmap Phone Enter (Done)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.VoroodMobile.Play ();
		}
	}

	public void Pressed_RoadmapPhone_Leave () {
		animRoadmapPhoneHolder.clip = animClipRoadmapArr [1];
		animRoadmapPhoneHolder.Play ();

		StartCoroutine (CanvasDisableRountine ());

		// SOUNDD EFFECTS - Rewards Roadmap Phone Enter (Done)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.VoroodMobile.Play ();
		}
	}

	public IEnumerator CanvasDisableRountine () {
		yield return new WaitForSeconds (0.5F);

		// Disable roadmap phone canvas
		canvasPhoneRoadmapHolder.enabled = false;
		objPhoneRoadmapHolder.SetActive (false);
	}

	public IEnumerator RoadmapPhone_Setup () {
		yield return null;
		roadmapPackageArr = GetComponentsInChildren <Roadmap_PackageScript> ();
		yield return null;
		for (int i = 0; i < roadmapPackageArr.Length; i++) {
			roadmapPackageArr [i].gameObject.SetActive (false);
		}
		for (int i = 0; i < endlessPotionRewardsSource.PotionRewards_SourceArr.Length; i++) {
			roadmapPackageArr [i].gameObject.SetActive (true);
			StartCoroutine (roadmapPackageArr [i].Setup_RoadmapPackage ());
		}
			
		// The old method had was for always TEN roadmap items
//		yield return null;
//		for (int i = 0; i < roadmapPackageArr.Length; i++) {
//			StartCoroutine (roadmapPackageArr [i].Setup_RoadmapPackage ());
//		}
	}

	public void MoveIn_RoadmapPhone () {
		Roadmap_ScrollDisableNOW ();
		scrollRoadmapPhone.horizontalNormalizedPosition = 0;

		// Enable roadmap phone canvas
		canvasPhoneRoadmapHolder.enabled = true;
		objPhoneRoadmapHolder.SetActive (true);

		Roadmap_ScrollEnableDelayed ();
	}

	public void MoveOut_RoadmapPhone () {
		canvasPhoneRoadmapHolder.enabled = false;
		objPhoneRoadmapHolder.SetActive (false);
	}

	public void MoveIn_BarController () {
	}

	public void MoveOut_BarController () {
	}

	public void Roadmap_ScrollDisableNOW () {
		scrollRoadmapPhone.enabled = false;
	}

	public void Roadmap_ScrollEnableDelayed () {
		StartCoroutine (Roadmap_ScrollEnableRoutine ());
	}

	public IEnumerator Roadmap_ScrollEnableRoutine () {
		yield return new WaitForSeconds (0.6F);
		Roadmap_ScrollEnableNOW ();
	}

	public void Roadmap_ScrollEnableNOW () {
		scrollRoadmapPhone.enabled = true;
	}

	public IEnumerator Roadmap_ScrollDisableRoutine () {
		yield return new WaitForSeconds (0.5F);
		Roadmap_ScrollDisableNOW ();
	}

	public void EndlessBar_ScaleZero () {
		this.transform.localScale = Vector3.zero;
	}

	public void EndlessBar_EnterAnim () {
		animEndBarController.clip = animClipEndBarArr [0];
		animEndBarController.Play ();
	}

	public void EndlessBar_LeaveAnim () {
		animEndBarController.clip = animClipEndBarArr [1];
		animEndBarController.Play ();
	}

	public void EndlessBar_ShowQuestion () {
		animEndBarController.clip = animClipEndBarArr [2];
		animEndBarController.Play ();
	}

	public void EndlessBar_PrepareLevelUp () {
		animEndBarController.clip = animClipEndBarArr [3];
		animEndBarController.Play ();

		particleTargetLevelUpSpark.Play ();
		particleTargetLevelUpCircle.Play ();
	}

	public void AnimEvent_LevelUpRewardStart () {
		StartCoroutine (LevelUpReward_FullStart ());
	}

	public IEnumerator LevelUpReward_FullStart () {
		popUpEndlessRewardsScript.Animate_EndlessRewards_Enter ();

		// SOUNDD EFFECTS - Level Up Show Rewards (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessLevelUpPhaseTwo.Play ();	
		}

		yield return null;

		popUpEndlessRewardsScript.MoveIn_EndlessRewards ();
		popUpEndlessRewardsScript.gameObject.SetActive (true);
		StartCoroutine (popUpEndlessRewardsScript.Setup_RewardRoadmapItems ());
	}
}