﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Endless_BarUpdater : MonoBehaviour {

	public Image imageBarProgHolder;

	public Text text_XP_Curr;
	public Text text_XP_ReqForWholeLevel;

	public Text text_LevelDisplay;

	public float floatBar_StartPoint;
	public float floatBar_Curr;
	public float floatBar_Target;

	public float floatBar_TargetBufferMain;
	public float floatBar_TargetBuffer1;
	public float floatBar_TargetBuffer2;
	public float floatBar_TargetBuffer3;
	public float floatBar_TargetBuffer4;
	public float floatBar_Distance;

	public float floatBarImageFull;

	public float floatSpeedModifier;

	public int intXP_Curr;
	public int intXP_Req;

	public bool hasSlowDown;
	public bool isBarEndLevelUp;
	public bool canMoveBar;

	public Vector2 v2_BarBuffer;

	void Start () {
//		Setup_BarUpdater ();
	}

	public void Setup_XPforDisplay (int curXP, int reqXP) {
		intXP_Curr = curXP;
		intXP_Req = reqXP;

		XP_ProgSet_Curr ();
		XP_ProgSet_Req ();
	}

	public void Setup_BarFixedPosit (float floatPosit) {
		floatBar_Curr = floatPosit;

		// Should be based on image's y sizeDelta (Copied from below)
		v2_BarBuffer.y = 28;

		Bar_ProgSet ();
	}

	public void Setup_StartAll (float whatFloat) {
		this.gameObject.SetActive (true);
		this.enabled = true;
		floatBar_Target = whatFloat;

		floatBar_Distance = floatBar_Target - floatBar_Curr;
		floatBar_TargetBuffer1 = floatBar_Curr + floatBar_Distance * 0.65F;
		floatBar_TargetBuffer2 = floatBar_Curr + floatBar_Distance * 0.82F;
		floatBar_TargetBuffer3 = floatBar_Curr + floatBar_Distance * 0.95F;
		floatBar_TargetBuffer4 = floatBar_Curr + floatBar_Distance * 0.99F;

		floatSpeedModifier = 0.6F;

		// Means no slowdown
		if (whatFloat == 1) {
			hasSlowDown = false;
			isBarEndLevelUp = true;

			floatBar_TargetBufferMain = floatBar_Target;
		} else {
			hasSlowDown = true;
			isBarEndLevelUp = false;

			floatBar_TargetBufferMain = floatBar_TargetBuffer1;

			// Check for the lenght of bar progress
			if ((floatBar_Distance) > 0.3F) {
				// Long but not full
			} else {
				// Short but not full
				floatSpeedModifier = 0.4F;
			}
		}

		// SOUNDD EFFECTS - XP Bar Moving START (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessHarkateNavareSabz.Play ();	
		}

		Setup_BarUpdater ();
	}

	public void Setup_BarUpdater () {
//		v2_BarBuffer.x = floatBar_StartPoint;

		// Dont use now. Instead use logic set from popup and fixedpoint
//		floatBar_Curr = floatBar_StartPoint;

		// Should be based on image's y sizeDelta
		v2_BarBuffer.y = 28;

		Bar_ProgSet ();
		canMoveBar = true;
	}

	void Update () {
		if (canMoveBar) {
//			Debug.Log ("Updating!");
			floatBar_Curr = Mathf.MoveTowards (floatBar_Curr, floatBar_TargetBufferMain, Time.deltaTime * floatSpeedModifier);
			Bar_ProgSet ();

			intXP_Curr = (int) (floatBar_Curr * intXP_Req);
			XP_ProgSet_Curr ();

			if (hasSlowDown) {
				if ((floatBar_Curr >= floatBar_TargetBuffer1) && (floatBar_TargetBuffer1 == floatBar_TargetBufferMain)) {
					floatBar_TargetBufferMain = floatBar_TargetBuffer2;
					floatSpeedModifier *= 0.75F;
//					Debug.LogWarning ("hasSlowDown: 1");
				}

				if ((floatBar_Curr >= floatBar_TargetBuffer2) && (floatBar_TargetBuffer2 == floatBar_TargetBufferMain)) {
					floatBar_TargetBufferMain = floatBar_TargetBuffer3;
					floatSpeedModifier *= 0.75F;
//					Debug.LogWarning ("hasSlowDown: 2");
				}

				if ((floatBar_Curr >= floatBar_TargetBuffer3) && (floatBar_TargetBuffer3 == floatBar_TargetBufferMain)) {
					floatBar_TargetBufferMain = floatBar_TargetBuffer4;
					floatSpeedModifier *= 0.75F;
//					Debug.LogWarning ("hasSlowDown: 3");
				}

				if ((floatBar_Curr >= floatBar_TargetBuffer4) && (floatBar_TargetBuffer4 == floatBar_TargetBufferMain)) {
					floatBar_TargetBufferMain = floatBar_Target;
					floatSpeedModifier *= 0.75F;
//					Debug.LogWarning ("hasSlowDown: 4");
				}

			}

			if (floatBar_Curr >= floatBar_Target) {
				canMoveBar = false;

				// SOUNDD EFFECTS - XP Bar Moving END (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.EndlessHarkateNavareSabz.Stop ();	
				}

				if (isBarEndLevelUp) {
//					Debug.LogError ("BAR END: Open Gift!");
					PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.Endless_LevelUp_Start ();

					// SOUNDD EFFECTS - Level Up - Bar reached the end, the gift icon moves towards level (empty endless)
					if (SoundManager.Instance != null) {
						SoundManager.Instance.EndlessLevelUpPhaseOne.Play ();	
					}
						
				} else {
//					Debug.LogError ("BAR END: End in the middle!");
					PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.Endless_LevelUp_EndOfAll ();

					// Update final xp for end and all
					intXP_Curr = PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.intScore_Target;
					PlayerData_InGame.Instance.p1_XP_Curr = intXP_Curr;
					XP_ProgSet_Curr ();
				}
			}
		}
	}

	void Bar_ProgSet () {
		v2_BarBuffer.x = floatBar_Curr * floatBarImageFull;
		imageBarProgHolder.rectTransform.sizeDelta = v2_BarBuffer;
	}

	void XP_ProgSet_Curr () {
		text_XP_Curr.text = intXP_Curr.ToString ();
	}

	void XP_ProgSet_Req () {
		text_XP_ReqForWholeLevel.text = intXP_Req.ToString ();
	}

	public void Level_DisplaySet () {
		if (PlayerData_InGame.Instance != null) {
			text_LevelDisplay.text = PlayerData_InGame.Instance.p1_EndlessLevel.ToString ();
		} else {
			text_LevelDisplay.text = PlayerData_Main.Instance.player_EndlessLevel.ToString ();
		}
	}
}
