﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Endless_ComboCircleScript : MonoBehaviour {

	public Image imageComboCircle;

	public bool isUpdateCircleAllowed;

	[HideInInspector]
	public bool isMultiCombo;

	public Color colorCircle_GreenIncrease;
	public Color colorCircle_Green;
	public Color colorCircle_Red;

	private float floatCircleFill_Curr;
	private float floatCircleFill_Target;
	private float floatCircleFill_Speed;

	private float floatCircleFill_Distance;

	private bool isSpeedDampener;

	public void ComboCircle_Setup (float curr, float target, float speed) {
		floatCircleFill_Curr = curr;
		floatCircleFill_Target = target;
		floatCircleFill_Speed = speed;

		if (curr < target) {
			floatCircleFill_Distance = target - curr;

			// To make sure dampener happens after fixed amount OR at the end
			if (floatCircleFill_Distance < 0.3F) {
				floatCircleFill_Distance = floatCircleFill_Distance / 3;
//				Debug.LogWarning ("Damped  /  3!");
			}

			else {
				floatCircleFill_Distance = 0.1F;
//				Debug.LogWarning ("Damped = 0.1F");
			}
		} else {
			floatCircleFill_Distance = curr - target;
		}

		ComboCircle_Enabler (true);

		if (curr < target) {
			if (isMultiCombo) {
				StartCoroutine (ComboCircle_ColorRoutine (0));
			} else {
				StartCoroutine (ComboCircle_ColorRoutine (1));
			}
			isSpeedDampener = true;
		} else {
			StartCoroutine (ComboCircle_ColorRoutine (2));
			isSpeedDampener = false;
		}
	}

	public void ComboCircle_Reset () {
		floatCircleFill_Curr = 0;
		floatCircleFill_Target = 0;
		floatCircleFill_Speed = 1;

		imageComboCircle.fillAmount = 0;
		ComboCircle_Enabler (false);
	}

	public IEnumerator ComboCircle_ColorRoutine (int intColor) {
		yield return null;
		switch (intColor) {
		case 0:
			imageComboCircle.color = colorCircle_GreenIncrease;
			break;
		case 1:
			imageComboCircle.color = colorCircle_Green;
			break;
		case 2:
			imageComboCircle.color = colorCircle_Red;
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}
	}

	public void ComboCircle_ColorINSTANT (int intColor) {
		switch (intColor) {
		case 0:
			imageComboCircle.color = colorCircle_GreenIncrease;
			break;
		case 1:
			imageComboCircle.color = colorCircle_Green;
			break;
		case 2:
			imageComboCircle.color = colorCircle_Red;
			break;
		default:
			imageComboCircle.color = colorCircle_Green;
			Debug.LogError ("Default Error");
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		if (isUpdateCircleAllowed) {
			floatCircleFill_Curr = Mathf.MoveTowards (
				floatCircleFill_Curr, 
				floatCircleFill_Target, 
				Time.deltaTime * floatCircleFill_Speed);

//			Debug.LogError ("floatCircleFill_Curr = " + floatCircleFill_Curr + "   and floatCircleFill_Target = " + floatCircleFill_Target);

			if (isSpeedDampener) {
				if (floatCircleFill_Distance > (floatCircleFill_Target - floatCircleFill_Curr)) {
//					Debug.LogError ("Damped!");
					floatCircleFill_Speed = floatCircleFill_Speed * 0.4F;
					isSpeedDampener = false;
				}
			}

			imageComboCircle.fillAmount = floatCircleFill_Curr;
			if (floatCircleFill_Curr == floatCircleFill_Target) {
				ComboCircle_ColorINSTANT (1);
				ComboCircle_Enabler (false);
			}
		}
	}

	void ComboCircle_Enabler (bool isActive) {
		isUpdateCircleAllowed = isActive;
		this.enabled = isActive;
		this.gameObject.SetActive (isActive);
	}
}
