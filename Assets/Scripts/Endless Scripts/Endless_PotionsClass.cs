﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Endless_PotionsClass {

	public enum PotionType {
		Battery,
		Damage,
		HP,
		Crit,
		LastStand,
		HeadStart
	}

	public enum PotionRewardType {
		Empty,

		Egg_Koochool,
		Egg_Aghdasi,
		Egg_Mosthan,

		Potion_Damage_Min,
		Potion_Damage_Max,
		Potion_HP_Min,
		Potion_HP_Max,
		Potion_Crit_Min,
		Potion_Crit_Max,
		Potion_LastStand_Min,
		Potion_LastStand_Max,
		Potion_HeadStart_Min,
		Potion_HeadStart_Max,

		Egg_Mustache

//		Potion_Shield_Min,
//		Potion_Shield_Max,
//		Potion_KillBonus_Min,
//		Potion_KillBonus_Max,
//		Potion_LuckyExplode_Min,
//		Potion_LuckyExplode_Max,
//		Potion_ScoreMulti_Min,
//		Potion_ScoreMulti_Max,
//		Potion_GoldIncrease_Min,
//		Potion_GoldIncrease_Max
	}

	public PotionRewardType Potion_Reward1;
	public PotionRewardType Potion_Reward2;
	public PotionRewardType Potion_Reward3;
	public PotionRewardType Potion_Reward4;
}
