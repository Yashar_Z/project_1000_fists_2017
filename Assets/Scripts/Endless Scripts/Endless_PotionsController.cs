﻿using UnityEngine;
using System.Collections;

public class Endless_PotionsController : MonoBehaviour {

//	public Endless_PotionsPackage[] endlessPackageArr;
	public Endless_ScoreController endlessScoreControllerHolder;
	public Popup_EndlessPotions_Script popUpEndlessScriptHolder;
	public Popup_Button_Script popupButtonHolder;
	public EndlessSpawner endlessSpawnerHolder;
	public PowerUps_Script powUpsScriptHolder;

	public Endless_PotionsRewardsSource endlessPotionRewardsSourceHolder;

	public GameObject objPlayEndlessButton;

	public int intEndlessLevel;

	public Sprite[] spritePotionInsideArr;
	public Color[] colorPotionBackArr;
	public string[] stringPotionTooltipArr;

	public ParticleSystem[] particlePotionEffectArr_PowUp;
	public ParticleSystem[] particlePotionEffectArr_Damage;
	public ParticleSystem[] particlePotionEffectArr_HP;
	public ParticleSystem[] particlePotionEffectArr_Crit;
	public ParticleSystem[] particlePotionEffectArr_LastStand;
	public ParticleSystem[] particlePotionEffectArr_HeadStart;

	public ParticleSystem particleHeadStartPressedPlay_No;
	public ParticleSystem particleHeadStartPressedPlay_Yes;

	public Endless_PotionsClass[] endlessPotionRewardsArr;

	public Animation animEndlessHeadStartPotion;

	[HideInInspector]
	public int PowerUpBOTH_EachChargeCost;

	[HideInInspector]
	public int PowerUpBOTH_TotalCost;

	private int intScoreBonusBuffer;
	private int intComboBonusBuffer;

	void Start () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			StartCoroutine (Start_EndlessController ());

			// The potions themselves are enabled via hud_ingame line 449 (In case of needs to be commented)
		} else {
			this.enabled = false;
		}
	}


	public IEnumerator CallPlayButton () {
		yield return new WaitForSeconds (0.75F);
		popupButtonHolder.Button_PlayAnim (0);
	}

	public IEnumerator PotionsButton_Enable () {
		yield return new WaitForSeconds (0.2F);
		objPlayEndlessButton.SetActive (true);

		// Because after reappearing, this becomes size 0
		popupButtonHolder.Button_PlayAnim (0);

		// Old scale method for button reappear
//		objPlayEndlessButton.transform.GetChild(0).localScale = Vector3.one;
	}

	public IEnumerator PotionsButton_Disable () {
		yield return null;
		objPlayEndlessButton.SetActive (false);

		// Old scale method for button reappear
//		objPlayEndlessButton.transform.localScale = Vector3.zero;
	}

	public void PotionsTexts_SetupArr () {
		// Battery uses the in scene already inside the array
//		stringPotionTooltipArr [0] = Balance_Constants_Script.Endless_RewardText_Battery;

		stringPotionTooltipArr [1] = Balance_Constants_Script.Endless_RewardText_Damage;
		stringPotionTooltipArr [2] = Balance_Constants_Script.Endless_RewardText_HP;
		stringPotionTooltipArr [3] = Balance_Constants_Script.Endless_RewardText_Crit;
		stringPotionTooltipArr [4] = Balance_Constants_Script.Endless_RewardText_LastStand;
		stringPotionTooltipArr [5] = Balance_Constants_Script.Endless_RewardText_HeadStart;

		stringPotionTooltipArr [6] = Balance_Constants_Script.Endless_RewardText_Egg;
	}

	IEnumerator Start_EndlessController () {
		yield return null;
		PotionsTexts_SetupArr ();

		yield return null;
		endlessPotionRewardsArr = endlessPotionRewardsSourceHolder.PotionRewards_SourceArr;

		intEndlessLevel = PlayerData_InGame.Instance.p1_EndlessLevel;

		EndlessPlay_Stop ();

		// Enable Shop Button For Start of Endless Potions Segment
		PlayerData_InGame.Instance.hudScriptHolder.HUD_DeathMustache_Button (true);

		PowerUpBOTH_EachChargeCost = Balance_Constants_Script.BatteryRecharge_PerSlot;
		PotionsPowerUp_CostCalculator ();

//		yield return null;
//		yield return new WaitForSeconds (3);

//		SetupPacakges_FirstTimeArt ();
//		SetupPackage_All ();
	}

	public IEnumerator Start_EndlessPotionsEnter () {
		yield return null;

		// RAISE layer of HUD mustache to normal (9)
		PlayerData_InGame.Instance.hudScriptHolder.Mustache_LayerSort_To (9);

		PlayerData_InGame.Instance.isPotionsMenuOpen = true;

		popUpEndlessScriptHolder.MoveIn_Potions ();

		StartCoroutine (CallPlayButton ());
		//		Debug.LogWarning ("gameplayGestureAllowed = " + PlayerData_InGame.Instance.gameplayGestureAllowed);
	}

	public void PotionsPowerUp_CostCalculator () {
		PowerUpBOTH_TotalCost = (((PlayerData_InGame.Instance.p1_PowUp1_Count_Own - PlayerData_InGame.Instance.p1_PowUp1_Count_Ready)
			+ (PlayerData_InGame.Instance.p1_PowUp2_Count_Own - PlayerData_InGame.Instance.p1_PowUp2_Count_Ready)) * PowerUpBOTH_EachChargeCost); 
	}

	public void Pressed_EndlessPlay () {
		// Update the headstart in endlessSpawner
		endlessSpawnerHolder.Start_NormalORHeadstart ();

		// This bool is used by shops button to enable / disable the potions play button that is in front of all in terms of layer
		PlayerData_InGame.Instance.isPotionsMenuOpen = false;

		// DISABLE Shop Button For Start of Endless Potions Segment
		PlayerData_InGame.Instance.hudScriptHolder.HUD_DeathMustache_Button (false);

		popUpEndlessScriptHolder.Endless_PlayAnim ();
		StartCoroutine (EndlessPlay_StartRoutine ());

		// Return layer of HUD mustache to normal (2)
		PlayerData_InGame.Instance.hudScriptHolder.Mustache_LayerSort_To (2);

		// Enter HUD Score
		PlayerData_InGame.Instance.hudScriptHolder.HUD_ScoreDisplay_Now ();

		// Endless world intro / door activate (Doesn't use normal play delayed)
		StartCoroutine (After_PressPlay_WorldIntro ());

		// For HeadStart Visual Effect ()
		StartCoroutine (HeadStart_PressedPlay());

		// Pass the new base damage (p1_Damage_Base) from potions to in_Damage_BaseOrig
		PlayerData_InGame.Instance.SetDamage_OriginalBase ();

		// Enable Pause button after pressing endless play 
		StartCoroutine (PlayerData_InGame.Instance.hudScriptHolder.PauseButton_EnableDisable(true, 0.25F));
	}

	public IEnumerator HeadStart_PressedPlay () {
		yield return new WaitForSeconds (0.3F);
		if (endlessSpawnerHolder.intHeadStart_Count == 0) {
			particleHeadStartPressedPlay_No.Play ();
		} else {
			particleHeadStartPressedPlay_Yes.Play ();

			// Headstart bonus scores
			HeadStart_ScoreComboBonus ();
		}
	}

	public void HeadStart_ScoreComboBonus () {
		switch (endlessSpawnerHolder.intHeadStart_Count) {
		case 1:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			break;
		case 2:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			break;
		case 3:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			break;
		case 4:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusCombo;
			break;
		case 5:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusCombo;
			break;
		case 6:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusCombo;
			break;
		case 7:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_7_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_7_BonusCombo;
			break;
		case 8:
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_1_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_2_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_3_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_4_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_5_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_6_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_7_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_7_BonusCombo;
			intScoreBonusBuffer += Balance_Constants_Script.Potion_HeadStart_8_BonusScore;
			intComboBonusBuffer += Balance_Constants_Script.Potion_HeadStart_8_BonusCombo;
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}

		PlayerData_InGame.Instance.p1_ComboMultiplier += intComboBonusBuffer;
		PlayerData_InGame.Instance.p1_Score_Curr += intScoreBonusBuffer;

		StartCoroutine (HeadStart_ScoreComboBonus_Visual ());
	}

	IEnumerator HeadStart_ScoreComboBonus_Visual () {
		yield return new WaitForSeconds (1.5F);
		animEndlessHeadStartPotion.Play ();

//		endlessScoreControllerHolder.HUD_ScoreAmountUpdate ();
//		endlessScoreControllerHolder.HUD_UpdateMultiText ();
	}

	public IEnumerator After_PressPlay_WorldIntro () {
		yield return new WaitForSeconds (0.75F);
		if (endlessSpawnerHolder.worldsInGameScriptHolder.intWorldActive_Endless == 4) {
			endlessSpawnerHolder.worldsInGameScriptHolder.world4_MoverHolder.W4_IntroAnims_PlayNOW ();
		} else {
			// Do in case we started from world 1
		}
	}

	void Start_UserLevelEffect () {
		for (int i = 0; i < intEndlessLevel; i++) {
			Potion_RewardEffectAssign (endlessPotionRewardsArr [i].Potion_Reward1);
			Potion_RewardEffectAssign (endlessPotionRewardsArr [i].Potion_Reward2);
			Potion_RewardEffectAssign (endlessPotionRewardsArr [i].Potion_Reward3);
			Potion_RewardEffectAssign (endlessPotionRewardsArr [i].Potion_Reward4);
		}
	}

	void Potion_RewardEffectAssign (Endless_PotionsClass.PotionRewardType rewardType) {
		switch (rewardType) {
		case Endless_PotionsClass.PotionRewardType.Empty:
		case Endless_PotionsClass.PotionRewardType.Egg_Koochool:
		case Endless_PotionsClass.PotionRewardType.Egg_Aghdasi:
		case Endless_PotionsClass.PotionRewardType.Egg_Mosthan:
		case Endless_PotionsClass.PotionRewardType.Egg_Mustache:
			break;
		case Endless_PotionsClass.PotionRewardType.Potion_Damage_Min:
			break;
		default:
			break;
		}
	}

	public IEnumerator EndlessPlay_StartRoutine () {
		yield return new WaitForSeconds (0.1F);
		endlessSpawnerHolder.bellScriptHolder.BellAnim_CallShort ();

		yield return new WaitForSeconds (1);
		EndlessPlay_Start ();

//		yield return new WaitForSeconds (0.5F);
//		StartCoroutine (endlessSpawnerHolder.BellAndBanner_Call ());
	}

	public void EndlessPlay_Start () {
		PlayerData_InGame.Instance.gameplayGestureAllowed = true;

		// For update in Endless Spawner
		endlessSpawnerHolder.endWave_EnemiesFinished = false;
	}

	public void EndlessPlay_Stop () {
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;

		// For update in Endless Spawner
		endlessSpawnerHolder.endWave_EnemiesFinished = true;
	}

	public void PotionEffect (Endless_PotionsClass.PotionType potionType, int amount, bool wasPressed) {
		switch (potionType) {
		case Endless_PotionsClass.PotionType.Battery:
			PlayerData_InGame.Instance.p1_PowUp1_Count_Ready = PlayerData_InGame.Instance.p1_PowUp1_Count_Own;
			PlayerData_InGame.Instance.p1_PowUp2_Count_Ready = PlayerData_InGame.Instance.p1_PowUp2_Count_Own;
			powUpsScriptHolder.PowerUpBoth_Update ();
			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_PowUp.Length; i++) {
					particlePotionEffectArr_PowUp [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed BATTERY (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionBattery.Play ();	
				}
			}

			break;
		case Endless_PotionsClass.PotionType.Damage:
			PlayerData_InGame.Instance.p1_Damage_Base += amount;
			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_Damage.Length; i++) {
					particlePotionEffectArr_Damage [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed DAMAGE (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionMosht.Play ();	
				}
			}

			break;
		case Endless_PotionsClass.PotionType.HP:
			PlayerData_InGame.Instance.p1_HP_Curr += amount;
//			PlayerData_InGame.Instance.p1_HP_Start += amount;

			// Normal in-game effect
//			PlayerData_InGame.Instance.hudScriptHolder.HUD_Healed_Normal ();
//			StartCoroutine (PlayerData_InGame.Instance.PlayerHeal_Normal (amount));

			// Only updates the enemy counter display
			StartCoroutine (PlayerData_InGame.Instance.hudScriptHolder.HUD_Update_Health (PlayerData_InGame.Instance.p1_HP_Curr));

			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_HP.Length; i++) {
					particlePotionEffectArr_HP [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed HP (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionGhalb.Play ();	
				}
			}

			break;
		case Endless_PotionsClass.PotionType.Crit:
			PlayerData_InGame.Instance.p1_CritChance += amount;
			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_Crit.Length; i++) {
					particlePotionEffectArr_Crit [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed CRIT (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionMoshtCrit.Play ();	
				}
			}

			break;
		case Endless_PotionsClass.PotionType.LastStand:
			PlayerData_InGame.Instance.p1_LastStandShoutCount += amount;
			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_LastStand.Length; i++) {
					particlePotionEffectArr_LastStand [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed LAST STAND (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionKhashmEzhdeha.Play ();	
				}
			}

			break;
		case Endless_PotionsClass.PotionType.HeadStart:
			endlessSpawnerHolder.intHeadStart_Count += amount;
//			endlessSpawnerHolder.intHeadStart_Count += (amount / 3);

			// Needs to update wave text
			// Need to use "WAVE" as text for this AND the logic of headstart

			if (wasPressed) {
				for (int i = 0; i < particlePotionEffectArr_HeadStart.Length; i++) {
					particlePotionEffectArr_HeadStart [i].Play ();
				}

				// SOUNDD EFFECTS - Potion Card Pressed HEAD START (empty endless)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PotionHeadStart.Play ();	
				}
			}

			break;

		default:
			Debug.LogError ("DEFAULT ERROR");
			break;
		}

	}

//	void SetupPackage_All () {
//		for (int i = 0; i < endlessPackageArr.Length; i++) {
//			SetupPackage_Single (i);
//		}
//	}
//
//	void SetupPackage_Single (int intWhich) {
//		switch (endlessPackageArr [intWhich].package_PotionType) { 
//
//		default:
//			break;
//		}
//		endlessPackageArr [intWhich].Setup_PotionPackage ();
//	}
}
