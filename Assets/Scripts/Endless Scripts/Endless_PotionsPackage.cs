﻿using UnityEngine;
using System.Collections;

public class Endless_PotionsPackage : MonoBehaviour {

	[Header ("Potion Package ID")]
	public int intPotionPackage;

	[Header ("")]
	public Endless_PotionsClass.PotionType package_PotionType;

	public Endless_PotionsController potionControllerScriptHolder;
	public Popup_Tooltips_Script popupTooltipScriptHolder;

	public bool hasOffer;
	public bool isLocked;

	public bool isNoAmountReward;

	public int intPotionAmount_Curr;
	public int intPotionAmount_Max;

	public int intPotionAmount_FinalLimit;

	public int[] intPotionCostArr;
	public int[] intPotionRewardArr;

	public UnityEngine.UI.Text text_PotionAmount;
	public UnityEngine.UI.Text text_PotionCost_BeforeOffer;
	public UnityEngine.UI.Text text_PotionCost_Final;
	public UnityEngine.UI.Text text_PotionOfferPercent;

	public UnityEngine.UI.Image imagePotionInsideArt;
	public UnityEngine.UI.Image imagePotionInsideBackColor;

	public string string_PotionRewardText;
	public string string_PotionRewardAmount;

	public Animation animOffPercentHolder;
	public Animation animPackageParentHolder;

	public GameObject objPotionCost_BeforeOffer;
	public GameObject objPotionLockedHolder;
	public GameObject objPotionCompletedHolder;
	public GameObject objTooltipPotionLocked;

	public GameObject objPotionButtonHolder;
	public GameObject objPotionShadowHolder;

	public ParticleSystem particlePressedOnly;
	public ParticleSystem particlePressedBuySuccess;
//	public ParticleSystem particleIdle;

	private bool isCompleted;
	private int intCostFinalized;

	void Start () {
//		switch (transform.GetSiblingIndex ()) {
//		Debug.Log (this.name + " is Sibling Index " + transform.GetSiblingIndex ());

		switch (intPotionPackage) {
		case 0:
			package_PotionType = Endless_PotionsClass.PotionType.Battery;
			break;
		case 1:
			package_PotionType = Endless_PotionsClass.PotionType.Damage;
			// Chance of offer
			Setup_OfferRandom ();
			break;
		case 2:
			package_PotionType = Endless_PotionsClass.PotionType.HP;
			// Chance of offer
			Setup_OfferRandom ();
			break;
		case 3:
			package_PotionType = Endless_PotionsClass.PotionType.Crit;
			// Chance of offer
			Setup_OfferRandom ();
			break;
		case 4:
			package_PotionType = Endless_PotionsClass.PotionType.LastStand;
			// Chance of offer
			Setup_OfferRandom ();
			break;
		case 5:
			package_PotionType = Endless_PotionsClass.PotionType.HeadStart;
			// Chance of offer
			Setup_OfferRandom ();
			break;

		default:
			break;
		}
			
		Setup_PotionTypeStuff ();
		Setup_PotionPackage ();

//		StartCoroutine (Setup_PotionIdle ());
	}

	public void Setup_OfferRandom () {
		if (Random.Range (0, 100) > 80) {
			hasOffer = true;
		} else {
			hasOffer = false;
		}
	}

	public void Setup_OfferPriceUpdate () {
		// To use a separate cost var for display and actual logic
		if (!hasOffer) {
			intCostFinalized = intPotionCostArr [intPotionAmount_Curr];
		} else {
			intCostFinalized = (int)(intPotionCostArr [intPotionAmount_Curr] / 2F);
		}
	}

	public void Setup_OfferDisplay () {
		if (hasOffer) {
			animOffPercentHolder.gameObject.SetActive (true);
			animOffPercentHolder.Play ();

			objPotionCost_BeforeOffer.SetActive (true);

//			int intOfferPercent = 100 - Mathf.RoundToInt (((float)intPotionCostArr [intPotionAmount_Curr] / (float)intPotionCostArr [intPotionAmount_Curr]) * 100);
//			text_PotionOfferPercent.text = intOfferPercent.ToString () + "%";

			text_PotionOfferPercent.text = "-50%";
		} else {
			animOffPercentHolder.gameObject.SetActive (false);
			objPotionCost_BeforeOffer.SetActive (false);
		}
	}

	public IEnumerator Setup_PotionIdle () {
		yield return new WaitForSeconds (transform.GetSiblingIndex() * 0.2F);
//		particleIdle.Play ();
	}

	public void Setup_PackageTexts () {
		text_PotionCost_BeforeOffer.text = intPotionCostArr [intPotionAmount_Curr].ToString ();
		text_PotionCost_Final.text = intCostFinalized.ToString ();

		// Tooltip
		string_PotionRewardAmount = intPotionRewardArr [intPotionAmount_Curr].ToString ();
	}

	public void Setup_PotionPackage () {
		text_PotionAmount.text = intPotionAmount_Curr.ToString () + "/" + intPotionAmount_Max.ToString ();
//		text_PotionAmount.text = intPotionAmount_Curr.ToString () + " / " + "13";
	
//		Debug.LogError (this.name + " has curr of " + intPotionAmount_Curr + " and max of " + intPotionAmount_Max);

		// Update CostFinalized for actual calculations
		Setup_OfferPriceUpdate ();

		if ((intPotionAmount_Curr <= (intPotionAmount_FinalLimit + 1))) {
//			intPotionAmount_Curr--;

			Setup_PackageTexts ();
		}

		if (intPotionAmount_FinalLimit == -1) {
			Setup_PackageTexts ();
		}

		// Show or not show (The offer)
		Setup_OfferDisplay ();

		if (!isNoAmountReward) {
			popupTooltipScriptHolder.stringToolTipsText = "(ﯼﺯﺎﺑ ﺖﺳﺩ ﻦﯾﺍ ﺭﺩ) +" + string_PotionRewardAmount + "  " + string_PotionRewardText;
		} else {
			popupTooltipScriptHolder.stringToolTipsText = string_PotionRewardText;
		}

		// Two lines system (Not used)
//		popupTooltipScriptHolder.stringToolTipsText = string_PotionRewardText + "\n" + string_PotionRewardAmount + "            " + "            ";
	}

	public void Pressed_PotionPackage () {
		if (!isLocked) {
			if (PlayerData_InGame.Instance.p1_Moustache > intCostFinalized) {
				StartCoroutine (PlayerData_InGame.Instance.PlayerMustache_Gain (-intCostFinalized));

				// Instead of straight increase like below, use above method
//				PlayerData_InGame.Instance.p1_Moustache -= intPotionCostArr [intPotionAmount_Curr];

				Potion_PurchaseSucces ();
			} else {

				StartCoroutine (PlayerData_InGame.Instance.hudScriptHolder.Player_TellGoShopRoutine ());

				Debug.LogError ("GO TO SHOP!");
			}
		} else {
			if (intPotionAmount_Curr <= intPotionAmount_FinalLimit) {
				objTooltipPotionLocked.SetActive (true);
				particlePressedOnly.Play ();
			}
		}
	}

	public void Potion_PurchaseSucces () {
//		Debug.Log ("BOUGHT PACKAGE!");

		potionControllerScriptHolder.PotionEffect (package_PotionType, intPotionRewardArr [intPotionAmount_Curr], true);

		// To avoid out of range index
		if (intPotionAmount_Curr <= intPotionAmount_FinalLimit) {
			intPotionAmount_Curr++;
		}

		// Moved to separate event / method
		//			PotionLockedCheck ();
		//			Setup_PotionPackage ();

		particlePressedBuySuccess.Play ();
		animPackageParentHolder.Play ();
	}

	public void AnimEvent_UpdatedPackage () {
		PotionLockedCheck ();
		Setup_PotionPackage ();
	}

	#region PotionTypeSetupStuff
	public void Setup_PotionTypeStuff () {
		switch (package_PotionType) {
		case Endless_PotionsClass.PotionType.Battery:
			intPotionAmount_FinalLimit = -1;

			intPotionCostArr [0] = potionControllerScriptHolder.PowerUpBOTH_TotalCost;
			intPotionCostArr [1] = potionControllerScriptHolder.PowerUpBOTH_TotalCost;
			intPotionCostArr [2] = potionControllerScriptHolder.PowerUpBOTH_TotalCost;
			intPotionCostArr [3] = potionControllerScriptHolder.PowerUpBOTH_TotalCost;
			intPotionCostArr [4] = potionControllerScriptHolder.PowerUpBOTH_TotalCost;
					
//			intPotionCostArr [0] = Balance_Constants_Script.Potion_Battery_1_Cost;
//			intPotionCostArr [1] = Balance_Constants_Script.Potion_Battery_2_Cost;
//			intPotionCostArr [2] = Balance_Constants_Script.Potion_Battery_3_Cost;
//			intPotionCostArr [3] = Balance_Constants_Script.Potion_Battery_4_Cost;
//			intPotionCostArr [4] = Balance_Constants_Script.Potion_Battery_5_Cost;

			// Only tooltip with 1 line
//			popupTooltipScriptHolder.NumberOfLines = 0;
			isNoAmountReward = true;

			// Display display amount
			PotionInside_ColorizeAndText (0);

			// Disable amount display
			text_PotionAmount.transform.parent.gameObject.SetActive (false);

			// Only for battery, we have locked check for setup
			PotionLockedCheck ();

//			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_Damage_Min, Endless_PotionsClass.PotionRewardType.Potion_Damage_Max);
			break;

		case Endless_PotionsClass.PotionType.Damage:
			intPotionAmount_FinalLimit = 4;

			intPotionCostArr [0] = Balance_Constants_Script.Potion_Damage_1_Cost;
			intPotionCostArr [1] = Balance_Constants_Script.Potion_Damage_2_Cost;
			intPotionCostArr [2] = Balance_Constants_Script.Potion_Damage_3_Cost;
			intPotionCostArr [3] = Balance_Constants_Script.Potion_Damage_4_Cost;
			intPotionCostArr [4] = Balance_Constants_Script.Potion_Damage_5_Cost;

			intPotionRewardArr [0] = Balance_Constants_Script.Potion_Damage_1_Reward;
			intPotionRewardArr [1] = Balance_Constants_Script.Potion_Damage_2_Reward;
			intPotionRewardArr [2] = Balance_Constants_Script.Potion_Damage_3_Reward;
			intPotionRewardArr [3] = Balance_Constants_Script.Potion_Damage_4_Reward;
			intPotionRewardArr [4] = Balance_Constants_Script.Potion_Damage_5_Reward;

			PotionInside_ColorizeAndText (1);
			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_Damage_Min, Endless_PotionsClass.PotionRewardType.Potion_Damage_Max);
			break;

		case Endless_PotionsClass.PotionType.HP:
			intPotionAmount_FinalLimit = 4;

			intPotionCostArr [0] = Balance_Constants_Script.Potion_HP_1_Cost;
			intPotionCostArr [1] = Balance_Constants_Script.Potion_HP_2_Cost;
			intPotionCostArr [2] = Balance_Constants_Script.Potion_HP_3_Cost;
			intPotionCostArr [3] = Balance_Constants_Script.Potion_HP_4_Cost;
			intPotionCostArr [4] = Balance_Constants_Script.Potion_HP_5_Cost;

			intPotionRewardArr [0] = Balance_Constants_Script.Potion_HP_1_Reward;
			intPotionRewardArr [1] = Balance_Constants_Script.Potion_HP_2_Reward;
			intPotionRewardArr [2] = Balance_Constants_Script.Potion_HP_3_Reward;
			intPotionRewardArr [3] = Balance_Constants_Script.Potion_HP_4_Reward;
			intPotionRewardArr [4] = Balance_Constants_Script.Potion_HP_5_Reward;

			PotionInside_ColorizeAndText (2);
			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_HP_Min, Endless_PotionsClass.PotionRewardType.Potion_HP_Max);
			break;

		case Endless_PotionsClass.PotionType.Crit:
			intPotionAmount_FinalLimit = 4;

			intPotionCostArr [0] = Balance_Constants_Script.Potion_Crit_1_Cost;
			intPotionCostArr [1] = Balance_Constants_Script.Potion_Crit_2_Cost;
			intPotionCostArr [2] = Balance_Constants_Script.Potion_Crit_3_Cost;
			intPotionCostArr [3] = Balance_Constants_Script.Potion_Crit_4_Cost;
			intPotionCostArr [4] = Balance_Constants_Script.Potion_Crit_5_Cost;

			intPotionRewardArr [0] = Balance_Constants_Script.Potion_Crit_1_Reward;
			intPotionRewardArr [1] = Balance_Constants_Script.Potion_Crit_2_Reward;
			intPotionRewardArr [2] = Balance_Constants_Script.Potion_Crit_3_Reward;
			intPotionRewardArr [3] = Balance_Constants_Script.Potion_Crit_4_Reward;
			intPotionRewardArr [4] = Balance_Constants_Script.Potion_Crit_5_Reward;

			PotionInside_ColorizeAndText (3);
			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_Crit_Min, Endless_PotionsClass.PotionRewardType.Potion_Crit_Max);
			break;

		case Endless_PotionsClass.PotionType.LastStand:
			intPotionAmount_FinalLimit = 2;

			intPotionCostArr [0] = Balance_Constants_Script.Potion_LastStand_1_Cost;
			intPotionCostArr [1] = Balance_Constants_Script.Potion_LastStand_2_Cost;
			intPotionCostArr [2] = Balance_Constants_Script.Potion_LastStand_3_Cost;
			intPotionCostArr [3] = Balance_Constants_Script.Potion_LastStand_4_Cost;
			intPotionCostArr [4] = Balance_Constants_Script.Potion_LastStand_5_Cost;

			intPotionRewardArr [0] = Balance_Constants_Script.Potion_LastStand_1_Reward;
			intPotionRewardArr [1] = Balance_Constants_Script.Potion_LastStand_2_Reward;
			intPotionRewardArr [2] = Balance_Constants_Script.Potion_LastStand_3_Reward;
			intPotionRewardArr [3] = Balance_Constants_Script.Potion_LastStand_4_Reward;
			intPotionRewardArr [4] = Balance_Constants_Script.Potion_LastStand_5_Reward;

			PotionInside_ColorizeAndText (4);
			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_LastStand_Min, Endless_PotionsClass.PotionRewardType.Potion_LastStand_Max);
			break;

		case Endless_PotionsClass.PotionType.HeadStart:
			// REMEMBER! When increasing potions, update final limit and each of new int[]s
			intPotionAmount_FinalLimit = 7;

			intPotionCostArr = new int[8];
			intPotionRewardArr = new int[8];

			intPotionCostArr [0] = Balance_Constants_Script.Potion_HeadStart_1_Cost;
			intPotionCostArr [1] = Balance_Constants_Script.Potion_HeadStart_2_Cost;
			intPotionCostArr [2] = Balance_Constants_Script.Potion_HeadStart_3_Cost;
			intPotionCostArr [3] = Balance_Constants_Script.Potion_HeadStart_4_Cost;
			intPotionCostArr [4] = Balance_Constants_Script.Potion_HeadStart_5_Cost;
			intPotionCostArr [5] = Balance_Constants_Script.Potion_HeadStart_6_Cost;
			intPotionCostArr [6] = Balance_Constants_Script.Potion_HeadStart_7_Cost;
			intPotionCostArr [7] = Balance_Constants_Script.Potion_HeadStart_8_Cost;

			intPotionRewardArr [0] = Balance_Constants_Script.Potion_HeadStart_1_Reward;
			intPotionRewardArr [1] = Balance_Constants_Script.Potion_HeadStart_2_Reward;
			intPotionRewardArr [2] = Balance_Constants_Script.Potion_HeadStart_3_Reward;
			intPotionRewardArr [3] = Balance_Constants_Script.Potion_HeadStart_4_Reward;
			intPotionRewardArr [4] = Balance_Constants_Script.Potion_HeadStart_5_Reward;
			intPotionRewardArr [5] = Balance_Constants_Script.Potion_HeadStart_6_Reward;
			intPotionRewardArr [6] = Balance_Constants_Script.Potion_HeadStart_7_Reward;
			intPotionRewardArr [7] = Balance_Constants_Script.Potion_HeadStart_8_Reward;

			PotionInside_ColorizeAndText (5);
			PotionInside_MinMax (Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Min, Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Max);
			break;

		default:
			Debug.LogError ("Default Error!");
			break;
		}
	}
	#endregion

	void PotionInside_ColorizeAndText (int intWhich) {
		imagePotionInsideArt.sprite = potionControllerScriptHolder.spritePotionInsideArr[intWhich];
		imagePotionInsideBackColor.color = potionControllerScriptHolder.colorPotionBackArr[intWhich];

		string_PotionRewardText = potionControllerScriptHolder.stringPotionTooltipArr [intWhich];
	}

	void PotionInside_MinMax (Endless_PotionsClass.PotionRewardType rewardType1, Endless_PotionsClass.PotionRewardType rewardType2) {
//		Debug.LogWarning ("BEFORE " + this.name + " Level Reward check. intPotionAmount_Curr = " + intPotionAmount_Curr + "  intPotionAmount_Max = " + intPotionAmount_Max);
//		Debug.LogError ("COUNT = " + potionControllerScriptHolder.endlessPotionRewardsArr.Length);

		for (int i = 0; i < (potionControllerScriptHolder.intEndlessLevel - 1); i++) {
			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward1 == rewardType1) {
				intPotionAmount_Curr++;
				potionControllerScriptHolder.PotionEffect (package_PotionType, intPotionRewardArr [intPotionAmount_Curr], false);
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward1 == rewardType2) {
				intPotionAmount_Max++;
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward2 == rewardType1) {
				intPotionAmount_Curr++;
				potionControllerScriptHolder.PotionEffect (package_PotionType, intPotionRewardArr [intPotionAmount_Curr], false);
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward2 == rewardType2) {
				intPotionAmount_Max++;
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward3 == rewardType1) {
				intPotionAmount_Curr++;
				potionControllerScriptHolder.PotionEffect (package_PotionType, intPotionRewardArr [intPotionAmount_Curr], false);
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward3 == rewardType2) {
				intPotionAmount_Max++;
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward4 == rewardType1) {
				intPotionAmount_Curr++;
				potionControllerScriptHolder.PotionEffect (package_PotionType, intPotionRewardArr [intPotionAmount_Curr], false);
			}

			if (potionControllerScriptHolder.endlessPotionRewardsArr [i].Potion_Reward4 == rewardType2) {
				intPotionAmount_Max++;
			}

//			Debug.LogWarning ("DUR " + this.name + " Level Reward check. intPotionAmount_Curr = " + intPotionAmount_Curr + "  intPotionAmount_Max = " + intPotionAmount_Max);
		}

		PotionLockedCheck ();
	}

	void PotionLockedCheck () {
		if (intPotionAmount_Curr < intPotionAmount_Max) {
			isLocked = false;
			objPotionLockedHolder.SetActive (false);
		} else {
			isLocked = true;
			objPotionLockedHolder.SetActive (true);

			// Complete check (Don't show Complete in unless 5 out of 5 full)
			if (intPotionAmount_Curr > intPotionAmount_FinalLimit) {
				objPotionCompletedHolder.SetActive (true);
//				objPotionButtonHolder.SetActive (false);
				objPotionShadowHolder.SetActive (false);

//				objPotionLockedHolder.transform.GetChild (2).gameObject.SetActive (true);
//				objPotionLockedHolder.transform.GetChild (1).gameObject.SetActive (false);	
			}

			// lock check (Don't show lock ONLY in case it was 0 out of 0 Empty)
			if (intPotionAmount_Curr == 0) {
				objPotionLockedHolder.transform.GetChild (1).gameObject.SetActive (true);	
			}

			// Remove offer display in case of lock
			hasOffer = false;
			Setup_OfferDisplay ();
		}

		// Special locked for pow ups
		if (package_PotionType == Endless_PotionsClass.PotionType.Battery) {
			potionControllerScriptHolder.PotionsPowerUp_CostCalculator ();
			if (potionControllerScriptHolder.PowerUpBOTH_TotalCost < 1) {
				isLocked = true;
				objPotionLockedHolder.SetActive (true);
				objPotionLockedHolder.transform.GetChild (1).gameObject.SetActive (false);	
			}
		}
	}
}
