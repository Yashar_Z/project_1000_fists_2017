﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Endless_ScoreController : MonoBehaviour {

	public Endless_ComboCircleScript endlessComboCircleScript;
	public Endless_BackgChanger endlessBackgroundChanger;

	public Text text_ScoreMultiplier;
	public Text text_ScoreAmount;

	public GameObject objEndlessControllerHolder;
	public GameObject[] objMultiComboTextsArr;
	public Transform transEndlessCrontroller;
	public Transform transScoreMultiplierText;

	public Image imageComboFillAmount;

	public AnimationClip[]	animclipEndlessScoreArr;
	public Animation animEndlessScoreMoverHolder;
	public Animation animEndlessScoreHolder;
	public Animation animMultiComboHolder;

	public ParticleSystem particleEndless_MiniGainCircle;
	public ParticleSystem particleEndless_MiniResetCircle;
	public ParticleSystem particleEndless_MultiGainCircle;
	public ParticleSystem particleEndless_MultiLossCircle;
	public ParticleSystem particleEndless_ScoreAmount;

	[HideInInspector]
	public int intCombo_Mini_IncreaseAmount;
	[HideInInspector]
	public int intCombo_Mini_Target;
	[HideInInspector]
	public int intCombo_MultiAmount_Mini;
	[HideInInspector]
	public int intKillComboReward;

	[HideInInspector]
	public int intCombo_MultiMaxRecord;

	// Now using the variable in playdata ingame
//	[HideInInspector]
//	public int intCombo_MultiAmount_Main;

	public Vector2 v2_HUD_ScoreMultiText_Default;
	public Vector2 v2_HUD_ScoreMultiTexT_ToLeft;

	private Vector2 v2_HUD_ScorePositVect2;

	private int intMultiPenalty = Balance_Constants_Script.Endless_intMultiPenalty;

	private int intComboReq_Lvl1 = Balance_Constants_Script.Endless_IntComboReq_Lvl1;
	private int intComboReq_Lvl5 = Balance_Constants_Script.Endless_IntComboReq_Lvl5;
	private int intComboReq_Lvl10 = Balance_Constants_Script.Endless_IntComboReq_Lvl10;
	private int intComboReq_Lvl15 = Balance_Constants_Script.Endless_IntComboReq_Lvl15;
	private int intComboReq_Lvl20 = Balance_Constants_Script.Endless_IntComboReq_Lvl20;
	private int intComboReq_Lvl25 = Balance_Constants_Script.Endless_IntComboReq_Lvl25;
	private int intComboReq_Lvl30 = Balance_Constants_Script.Endless_IntComboReq_Lvl30;
	private int intComboReq_Lvl35 = Balance_Constants_Script.Endless_IntComboReq_Lvl35;
	private int intComboReq_LvlInfinite = Balance_Constants_Script.Endless_IntComboReq_LvlInfinite;

	public void Start_EndlessController () {
		text_ScoreAmount.text = "0";

		// Instead of intCombo_MultiAmount_Main
		PlayerData_InGame.Instance.p1_ComboMultiplier = 1;

		// Resize to zero
//		this.transform.localScale = Vector3.zero;

		HUD_UpdateMultiText ();

		// Now use the above line
//		text_ScoreMultiplier.text = "1";

		imageComboFillAmount.fillAmount = 0;

		this.gameObject.SetActive (true);

		// Set size to zero
		HUD_EndlessScore_Reset ();

		// Get base value of kill reward
//		intKillComboReward = 0;
	}

	public void HUD_ScorePosition (int intNewMustacheLength) {
		v2_HUD_ScorePositVect2.x = -(intNewMustacheLength - 12);
		transEndlessCrontroller.localPosition = v2_HUD_ScorePositVect2;
	}

	public void HUD_ComboUpdate_KillReward () {
		endlessComboCircleScript.isMultiCombo = true;

		// Add base kill reward to the wave number
		intCombo_MultiAmount_Mini += (intKillComboReward + Balance_Constants_Script.Endless_ComboKillReward);

		// To combo calculate
		Combo_IncreaseCalculate ();
	}

	// Player successfully hit
	public void HUD_ComboUpdate_AnyGain () {
		intCombo_Mini_IncreaseAmount = PlayerData_InGame.Instance.intOneComboCount;

		// To avoid else for the below "if"
		endlessComboCircleScript.isMultiCombo = false;

		if (intCombo_Mini_IncreaseAmount > 1) {
			// Display multi-combo message should be here
			StartCoroutine (HUD_MultiComboMessage (intCombo_Mini_IncreaseAmount));

			// mini-increase for amount in case of combo
			intCombo_Mini_IncreaseAmount *= Balance_Constants_Script.Endless_ComboMultiIncreaser;

			// To change color of green ONLY for combo
			endlessComboCircleScript.isMultiCombo = true;
		} 

		intCombo_MultiAmount_Mini += intCombo_Mini_IncreaseAmount;

		// To combo calculate
		Combo_IncreaseCalculate ();
	}

	public void Combo_IncreaseCalculate () {
//		Debug.LogError ("intOneComboCount = " + PlayerData_InGame.Instance.intOneComboCount);

		switch (PlayerData_InGame.Instance.p1_ComboMultiplier) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			intCombo_Mini_Target = intComboReq_Lvl1;
			break;
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			intCombo_Mini_Target = intComboReq_Lvl5;
			break;
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
			intCombo_Mini_Target = intComboReq_Lvl10;
			break;
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
			intCombo_Mini_Target = intComboReq_Lvl15;
			break;
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
			intCombo_Mini_Target = intComboReq_Lvl20;
			break;
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
			intCombo_Mini_Target = intComboReq_Lvl25;
			break;
		case 31:
		case 32:
		case 33:
		case 34:
		case 35:
			intCombo_Mini_Target = intComboReq_Lvl30;
			break;
		case 36:
		case 37:
		case 38:
		case 39:
		case 40:
			intCombo_Mini_Target = intComboReq_Lvl35;
			break;
		case 41:
			intCombo_Mini_Target = intComboReq_LvlInfinite;
			break;
		default:
			Debug.LogError ("Default Error");
			intCombo_Mini_Target = intComboReq_LvlInfinite;
			break;
		}

		if (intCombo_MultiAmount_Mini >= intCombo_Mini_Target) {
			// Main-increase
			intCombo_MultiAmount_Mini = 0;
			PlayerData_InGame.Instance.p1_ComboMultiplier++;
			HUD_UpdateMultiText ();

			HUD_ComboAnimUpdate_MainGain ();
			particleEndless_MiniGainCircle.Play ();

			endlessComboCircleScript.ComboCircle_Reset ();
		} 

		// No main increase
		else {
			HUD_ComboAnimUpdate_MiniGain ();

			endlessComboCircleScript.ComboCircle_Setup (imageComboFillAmount.fillAmount, (float)intCombo_MultiAmount_Mini / intCombo_Mini_Target, 0.7F);
		}

		// Update multiplier max
		StartCoroutine (MaxMulti_Check ());

		// Instant method (Old)
		//		imageComboFillAmount.fillAmount = (float)intCombo_MultiAmount_Mini / intCombo_Mini_Target;
	}

	public IEnumerator MaxMulti_Check () {
		yield return null;
		if (intCombo_MultiMaxRecord < PlayerData_InGame.Instance.p1_ComboMultiplier) {
			intCombo_MultiMaxRecord = PlayerData_InGame.Instance.p1_ComboMultiplier;
		}
	}

	// Player missed
	public void HUD_ComboUpdate_MiniLoss () {
		HUD_ComboAnimUpdate_MiniReset ();

		// Now use color change
//		particleEndless_MiniResetCircle.Play ();

		intCombo_MultiAmount_Mini = 0;

		endlessComboCircleScript.ComboCircle_Setup (imageComboFillAmount.fillAmount, 0, 1.5F);

		// Instant method (Old)
//		imageComboFillAmount.fillAmount = 0;
	}

	// Player was hurt
	public void HUD_ComboUpdate_MainLoss (bool isDeath) {
		HUD_ComboAnimUpdate_MiniReset ();

		// Main-decrease
		intCombo_MultiAmount_Mini = 0;

		if (!isDeath) {
			PlayerData_InGame.Instance.p1_ComboMultiplier -= intMultiPenalty;

			// Prevent combo multiplier from reaching zero
			if (PlayerData_InGame.Instance.p1_ComboMultiplier < 1) {
				PlayerData_InGame.Instance.p1_ComboMultiplier = 1;
			}

		} else {
			PlayerData_InGame.Instance.p1_ComboMultiplier = 1;
		}

		if (PlayerData_InGame.Instance.p1_ComboMultiplier < 1) {
			PlayerData_InGame.Instance.p1_ComboMultiplier = 1;
		}

		HUD_UpdateMultiText ();

		HUD_ComboAnimUpdate_MainLoss ();
		particleEndless_MultiLossCircle.Play ();
		particleEndless_MiniResetCircle.Play ();

		imageComboFillAmount.fillAmount = 0;
	}

	public IEnumerator HUD_MultiComboMessage (int intNumber) {
		animMultiComboHolder.Play ();

		objMultiComboTextsArr [0].SetActive (false);
		objMultiComboTextsArr [1].SetActive (false);
		objMultiComboTextsArr [2].SetActive (false);
		objMultiComboTextsArr [3].SetActive (false);

		yield return null;

		if (intNumber > 4) {
			objMultiComboTextsArr [3].SetActive (true);
		} else {
			objMultiComboTextsArr [intNumber - 2].SetActive (true);
		}
	}

	public void HUD_UpdateMultiText () {
		if (PlayerData_InGame.Instance.p1_ComboMultiplier != 1) {
			transScoreMultiplierText.localPosition = v2_HUD_ScoreMultiText_Default;
		} else {
			if ((PlayerData_InGame.Instance.p1_ComboMultiplier > 9) && (Mathf.FloorToInt (PlayerData_InGame.Instance.p1_ComboMultiplier / 10) != 1)) {
				transScoreMultiplierText.localPosition = v2_HUD_ScoreMultiText_Default;
			} else {
				transScoreMultiplierText.localPosition = v2_HUD_ScoreMultiTexT_ToLeft;
			}
		}

		text_ScoreMultiplier.text = PlayerData_InGame.Instance.p1_ComboMultiplier.ToString ();
	}

	public void HUD_ComboAnimUpdate_MiniGain () {
		animEndlessScoreHolder.clip = animclipEndlessScoreArr [1];
		animEndlessScoreHolder.Play ();
	}

	public void HUD_ComboAnimUpdate_MiniReset () {
//		animEndlessScoreHolder.clip = animclipEndlessScoreArr [3];
//		animEndlessScoreHolder.Play ();
	}

	public void HUD_ComboAnimUpdate_MainGain () {
		// Don't use normal endless anim
		animEndlessScoreMoverHolder.Play ();

		// SOUNDD EFFECTS - Increase Combo Multiplier Number (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessComboMultiplierIncrease.Play ();	
		}

//		animEndlessScoreHolder.clip = animclipEndlessScoreArr [2];
//		animEndlessScoreHolder.Play ();

		// Moved to event
//		particleEndless_MultiGainCircle.Emit (1);
	}

	public void HUD_ComboAnimUpdate_MainLoss () {
		animEndlessScoreHolder.clip = animclipEndlessScoreArr [4];
		animEndlessScoreHolder.Play ();

		// SOUNDD EFFECTS - Decrease Combo Multiplier Number (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessComboMultiplierDecrease.Play ();	
		}
	}

	public void HUD_EndlessScore_Enter () {
		animEndlessScoreHolder.clip = animclipEndlessScoreArr [0];
		animEndlessScoreHolder.Play ();
	}

	public void HUD_EndlessScore_Leave () {
		animEndlessScoreHolder.clip = animclipEndlessScoreArr [5];
		animEndlessScoreHolder.Play ();
	}

	public void HUD_EndlessScore_Reset () {
		animEndlessScoreHolder.clip = animclipEndlessScoreArr [6];
		animEndlessScoreHolder.Play ();
	}

	public void AnimEvent_MultiGainParticle () {
		particleEndless_MultiGainCircle.Emit (1);
	}

	public void HUD_ScoreAmountUpdate () {
		particleEndless_ScoreAmount.Emit (1);
		text_ScoreAmount.text = PlayerData_InGame.Instance.p1_Score_Curr.ToString ();
	}
	
	// Destroy the whole thing NOW
	public void Destroy_EndlessScoreNOW (){
		StartCoroutine (Destroy_EndlessScoreRoutine ());
	}

	// Destroy the whole thing
	public IEnumerator Destroy_EndlessScoreRoutine () {
		yield return null;
		Destroy (objEndlessControllerHolder);
	}
}
