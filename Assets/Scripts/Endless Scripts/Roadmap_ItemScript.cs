﻿using UnityEngine;
using System.Collections;

public class Roadmap_ItemScript : MonoBehaviour {

	public Sprite[] spriteRoadmapItemsArr;

	public UnityEngine.UI.Image imageRoadmapItems_Egg;
	public UnityEngine.UI.Image imageRoadmapItems_Potion;

	public UnityEngine.UI.Text textRoadmapReward;

	public GameObject objRoadampEggBase;
	public GameObject objRoadampPotionBase;
	public GameObject objRewardText;

	public Popup_Tooltips_Script popUpTooltipScriptHolder;

	private string message_EggReward = "!ﻪﯾﺪﻫ";
	private string message_PotionMin = "+ ﻪﯿﻟﻭﺍ ﺭﺍﺪﻘﻣ";
	private string message_PotionMax = "+ ﺖﯿﻓﺮﻇ";

	public void Setup_RoadmapItem (Endless_PotionsClass.PotionRewardType potionRewardType) {
		spriteRoadmapItemsArr = Endless_BarController.Instance.spriteRoadmapItemsArr;
		switch (potionRewardType) {

		// Empty
		case Endless_PotionsClass.PotionRewardType.Empty:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (false);

			objRewardText.SetActive (false);
//			popUpTooltipScriptHolder.stringToolTipsText = "";

			break;

		// Eggs
		case Endless_PotionsClass.PotionRewardType.Egg_Koochool:
			objRoadampEggBase.SetActive (true);
			objRoadampPotionBase.SetActive (false);

			objRewardText.SetActive (true);

			imageRoadmapItems_Egg.sprite = spriteRoadmapItemsArr [0];
			textRoadmapReward.text = message_EggReward;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Egg;

			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Aghdasi:
			objRoadampEggBase.SetActive (true);
			objRoadampPotionBase.SetActive (false);

			objRewardText.SetActive (true);

			imageRoadmapItems_Egg.sprite = spriteRoadmapItemsArr [1];
			textRoadmapReward.text = message_EggReward;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Egg;

			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Mosthan:
			objRoadampEggBase.SetActive (true);
			objRoadampPotionBase.SetActive (false);

			objRewardText.SetActive (true);

			imageRoadmapItems_Egg.sprite = spriteRoadmapItemsArr [2];
			textRoadmapReward.text = message_EggReward;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Egg;

			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Mustache:
			objRoadampEggBase.SetActive (true);
			objRoadampPotionBase.SetActive (false);

			objRewardText.SetActive (true);

			imageRoadmapItems_Egg.sprite = spriteRoadmapItemsArr [8];
			textRoadmapReward.text = message_EggReward;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Egg;

			break;
		
		// Damage
		case Endless_PotionsClass.PotionRewardType.Potion_Damage_Min:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [3];
			textRoadmapReward.text = message_PotionMin;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Damage;

			break;
		case Endless_PotionsClass.PotionRewardType.Potion_Damage_Max:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [3];
			textRoadmapReward.text = message_PotionMax;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Damage;

			break;
		
		// HP
		case Endless_PotionsClass.PotionRewardType.Potion_HP_Min:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [4];
			textRoadmapReward.text = message_PotionMin;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_HP;

			break;
		case Endless_PotionsClass.PotionRewardType.Potion_HP_Max:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [4];
			textRoadmapReward.text = message_PotionMax;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_HP;

			break;

		// Crit
		case Endless_PotionsClass.PotionRewardType.Potion_Crit_Min:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [5];
			textRoadmapReward.text = message_PotionMin;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Crit;

			break;
		case Endless_PotionsClass.PotionRewardType.Potion_Crit_Max:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [5];
			textRoadmapReward.text = message_PotionMax;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_Crit;

			break;

		// Last Stand
		case Endless_PotionsClass.PotionRewardType.Potion_LastStand_Min:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [6];
			textRoadmapReward.text = message_PotionMin;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_LastStand;

			break;
		case Endless_PotionsClass.PotionRewardType.Potion_LastStand_Max:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [6];
			textRoadmapReward.text = message_PotionMax;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_LastStand;

			break;

		// Head Start
		case Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Min:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [7];
			textRoadmapReward.text = message_PotionMin;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_HeadStart;

			break;
		case Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Max:
			objRoadampEggBase.SetActive (false);
			objRoadampPotionBase.SetActive (true);

			objRewardText.SetActive (true);

			imageRoadmapItems_Potion.sprite = spriteRoadmapItemsArr [7];
			textRoadmapReward.text = message_PotionMax;

			popUpTooltipScriptHolder.stringToolTipsText = Balance_Constants_Script.Endless_RewardText_HeadStart;

			break;

		default:
			Debug.LogError ("Default Error");
			break;
		}
	}
}
