﻿using UnityEngine;
using System.Collections;

public class Roadmap_PackageScript : MonoBehaviour {

	public int intRewardLevel;
	public Roadmap_ItemScript[] roadItemScriptArr;

	public UnityEngine.UI.Text text_levelName;

	public UnityEngine.UI.Image image_levelBackInside;

	public GameObject objLockedRoadmapHolder;

	private int intLevelNumber;
	private string stringLevelName;

	public IEnumerator Setup_RoadmapPackage () {
		intRewardLevel = transform.GetSiblingIndex ();

		roadItemScriptArr[0].Setup_RoadmapItem (Endless_BarController.Instance
			.endlessPotionRewardsSource.PotionRewards_SourceArr[intRewardLevel].Potion_Reward1);
		roadItemScriptArr[1].Setup_RoadmapItem (Endless_BarController.Instance
			.endlessPotionRewardsSource.PotionRewards_SourceArr[intRewardLevel].Potion_Reward2);

		// Old assign method that relied on ingame
//		roadItemScriptArr[0].Setup_RoadmapItem (PlayerData_InGame.Instance.hudScriptHolder
//			.endlessPotionsController.endlessPotionRewardsArr[intRewardLevel].Potion_Reward1);
//		roadItemScriptArr[1].Setup_RoadmapItem (PlayerData_InGame.Instance.hudScriptHolder
//			.endlessPotionsController.endlessPotionRewardsArr[intRewardLevel].Potion_Reward2);

		yield return null;

		roadItemScriptArr[2].Setup_RoadmapItem (Endless_BarController.Instance
			.endlessPotionRewardsSource.PotionRewards_SourceArr[intRewardLevel].Potion_Reward3);
		roadItemScriptArr[3].Setup_RoadmapItem (Endless_BarController.Instance
			.endlessPotionRewardsSource.PotionRewards_SourceArr[intRewardLevel].Potion_Reward4);

		// Old assign method that relied on ingame
//		roadItemScriptArr[2].Setup_RoadmapItem (PlayerData_InGame.Instance.hudScriptHolder
//			.endlessPotionsController.endlessPotionRewardsArr[intRewardLevel].Potion_Reward3);	
//		roadItemScriptArr[3].Setup_RoadmapItem (PlayerData_InGame.Instance.hudScriptHolder
//			.endlessPotionsController.endlessPotionRewardsArr[intRewardLevel].Potion_Reward4);

		yield return null;

		Setup_PackageLevelName ();

		Setup_PackageLevelColor ();
	}

	public void Setup_PackageLevelName () {
		switch (intRewardLevel) {
		case 0:
			stringLevelName = "ﻝﻭﺍ ﺢﻄﺳ";
			break;
		case 1:
			stringLevelName = "ﻡﻭﺩ ﺢﻄﺳ";
			break;
		case 2:
			stringLevelName = "ﻡﻮﺳ ﺢﻄﺳ";
			break;
		case 3:
			stringLevelName = "ﻡﺭﺎﻬﭼ ﺢﻄﺳ";
			break;
		case 4:
			stringLevelName = "ﻢﺠﻨﭘ ﺢﻄﺳ";
			break;
		case 5:
			stringLevelName = "ﻢﺸﺷ ﺢﻄﺳ";
			break;
		case 6:
			stringLevelName = "ﻢﺘﻔﻫ ﺢﻄﺳ";
			break;
		case 7:
			stringLevelName = "ﻢﺘﺸﻫ ﺢﻄﺳ";
			break;
		case 8:
			stringLevelName = "ﻢﻬﻧ ﺢﻄﺳ";
			break;
		case 9:
			stringLevelName = "ﻢﻫﺩ ﺢﻄﺳ";
			break;
		case 10:
			stringLevelName = "ﻢﻫﺩﺯﺎﯾ ﺢﻄﺳ";
			break;
		case 11:
			stringLevelName = "ﻢﻫﺩﺯﺍﻭﺩ ﺢﻄﺳ";
			break;
		case 12:
			stringLevelName = "ﻢﻫﺩﺰﯿﺳ ﺢﻄﺳ";
			break;
		case 13:
			stringLevelName = "ﻢﻫﺩﺭﺎﻬﭼ ﺢﻄﺳ";
			break;
		case 14:
			stringLevelName = "ﻢﻫﺩﺰﻧﺎﭘ ﺢﻄﺳ";
			break;
		case 15:
			stringLevelName = "ﻢﻫﺩﺰﻧﺎﺷ ﺢﻄﺳ";
			break;
		case 16:
			stringLevelName = "ﻢﻫﺪﻔﻫ ﺢﻄﺳ";
			break;
		case 17:
			stringLevelName = "ﻢﻫﺪﺠﻫ ﺢﻄﺳ";
			break;
		case 18:
			stringLevelName = "OP!";
			break;

		default:
			Debug.LogError ("Default Error");
			break;
		}
		text_levelName.text = stringLevelName;
	}

	public void Setup_PackageLevelColor () {
		if (PlayerData_InGame.Instance != null) {
			intLevelNumber = PlayerData_InGame.Instance.p1_EndlessLevel;
		} else {
			intLevelNumber = PlayerData_Main.Instance.player_EndlessLevel;
		}

		if (intRewardLevel < intLevelNumber) {
			// Green card back (Unlocked);
			image_levelBackInside.color = Endless_BarController.Instance.colorRoadmapCardsArr [0];
			objLockedRoadmapHolder.SetActive (false);
		} else {
			// Red card back (Locked);
			image_levelBackInside.color = Endless_BarController.Instance.colorRoadmapCardsArr [1];
			objLockedRoadmapHolder.SetActive (true);
		}
	}

}
