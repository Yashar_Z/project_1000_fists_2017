﻿using UnityEngine;
using System.Collections;

public class ArrowDisplay3_Script : MonoBehaviour {

	[Header ("Display 3 Arrows System")]
	public GameObject objEnemyArrowPrev;
	public GameObject[] objEnemyArrowSourceArr;

	public void ArrowState_Display (char whatChar) {

		objEnemyArrowPrev.SetActive (false);

		switch (whatChar) {
		case 'U':
			objEnemyArrowPrev = objEnemyArrowSourceArr [0];
			break;
		case 'D':
			objEnemyArrowPrev = objEnemyArrowSourceArr [1];
			break;
		case 'R':
			objEnemyArrowPrev = objEnemyArrowSourceArr [2];
			break;
		case 'L':
			objEnemyArrowPrev = objEnemyArrowSourceArr [3];
			break;
		case '-':
			objEnemyArrowPrev = objEnemyArrowSourceArr [4];
			break;
		case '|':
			objEnemyArrowPrev = objEnemyArrowSourceArr [5];
			break;
		case '^':
			objEnemyArrowPrev = objEnemyArrowSourceArr [6];
			break;
		case 'v':
			objEnemyArrowPrev = objEnemyArrowSourceArr [7];
			break;
		case '>':
			objEnemyArrowPrev = objEnemyArrowSourceArr [8];
			break;
		case '<':
			objEnemyArrowPrev = objEnemyArrowSourceArr [9];
			break;
		}

		objEnemyArrowPrev.SetActive (true);
	}

	public void ArrowState_TurnOff() {
		objEnemyArrowPrev.SetActive (false);
	}
}
