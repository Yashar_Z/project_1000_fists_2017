﻿using UnityEngine;
using System.Collections;

public class Arrow_Wiggling_Script : MonoBehaviour {

	private Animation myArrowsAnimRef;

	// Use this for initialization
	void Start () {
		myArrowsAnimRef = GetComponent<Animation> ();
		myArrowsAnimRef [myArrowsAnimRef.clip.name].time = Random.Range (0F, 1.8F);
		myArrowsAnimRef.Play ();
	}
}
