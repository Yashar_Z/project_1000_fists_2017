﻿using UnityEngine;
using System.Collections;

public class Barrel_BodyScript : MonoBehaviour {

	public EnemyMover_Barrel myMoverBarrelHolder;

	public void myMover_BarrelHead_Up() {
		myMoverBarrelHolder.MoveHead_HeadGoes_Up ();
//		Debug.Log ("PART 1");
	}

	public void myMover_BarrelHead_Wait() {
		myMoverBarrelHolder.MoveHead_HeadGoes_Wait ();
//		Debug.Log ("PART 2: " + GetComponent<Animation>().clip.name);
	}

	public void myMover_BarrelHead_Down() {
		myMoverBarrelHolder.MoveHead_HeadGoes_Down ();
//		Debug.Log ("PART 3 " + GetComponent<Animation>().clip.name);
	}
		
	public void myMover_BarrelBody_Continue () {
		myMoverBarrelHolder.MoveHead_HeadGone_Continue ();
//		Debug.Log ("PART 4 " + GetComponent<Animation>().clip.name);
	}

	public void myMover_BarrelHead_Invinciblity_True () {
		myMoverBarrelHolder.MoveHead_HeadGoes_InvinciblityBool (true);
	}

	public void myMover_BarrelHead_Invinciblity_False () {
		myMoverBarrelHolder.MoveHead_HeadGoes_InvinciblityBool (false);
	}
}
