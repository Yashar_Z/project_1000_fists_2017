﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public enum EnemyType
	{ 	
		// 0
		Fat1,
		Fat2,
		Thin1,
		Thin2,
		Muscle1,
		Muscle2,
		Giant1,
		Giant2,
		HF1,
		HF2,

		// 10
		HF_X1,
		HF_X2,
		Barrel1,
		Barrel2,
		Barrel_X1,
		Barrel_X2,
		Barrel_Q1,
		Barrel_Q2,
		e18,
		e19,

		// 20
		e20,
		e21,
		e22,
		e23,
		e24,
		e25,
		e26,
		e27,
		e28,
		e29,

		// 30
		e30,
		e31,
		e32,
		e33,
		e34,
		e35,
		e36,
		e37,
		e38,
		e39,

		// 40
		Heart,
		Pipe,
		Hedgehog,
		Mustache,
		Elixir,
		Freeze,
		Random_Item,

		// 47
		SuperMustache,
		BigBomb,
		LineBomb,
		Score_Item,
		SuperScore,
		PowerUp_Fist,
		PowerUp_Shield

		// TODO: New enemies AND items stuff needs to be added at:
		/* 
		 - Here
		 - EnemySpawner script
		 - LevelManifest script (MANY PLACES)
		 */
	}
	public enum LaneTypes {Lane_1, Lane_2, Lane_3, Lane_4};

	// General Info
	public EnemyType enemyType;
	public float enemySpawnTime;
	public int enemyMustache;

	// Health Info
	public string enemyHP;
	public int enemyDamage;

	// Health Modifier
//	public bool isPosioned;
	private bool _isExplosive;
	public bool isExplosive{
		get {
			if (this.enemyType == EnemyType.Barrel_X1 || this.enemyType == EnemyType.Barrel_X2 ||
			    this.enemyType == EnemyType.HF_X1 || this.enemyType == EnemyType.HF_X2) {
				_isExplosive = true;
			} else {
				_isExplosive = false;
			}
			return _isExplosive;}
		set { _isExplosive = value;}
	}
//	public bool isFreezing;
//	public bool isHealing;

	// Mover Info
	public LaneTypes enemyLane;
	public float enemyMoveSpeed;

	// Explosion Info
	public int explosionDamage;

	// Item Info
	public int itemValue;

    //additional
    public int stickyHealth;
    public float vulnerableTime;
    public int hedgehogDamage;

	// Endless into
	public int enemyScore;
}
