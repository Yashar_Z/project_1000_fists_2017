﻿using UnityEngine;
using System.Collections;

public class EnemyDestroyer : MonoBehaviour {

	public void DestroyMe () {
		// New method
		PlayerData_InGame.Instance.Ref_PlayerHurtAnim ();

		// Old method
//		FindObjectOfType <Camera_InGame_Script> ().PlayAnim_PlayerHurt ();

		Destroy (this.transform.parent.gameObject);
	}
}
