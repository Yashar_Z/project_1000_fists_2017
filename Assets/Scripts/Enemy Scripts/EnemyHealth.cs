﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour {

	public Enemy myValuesHolder;

	public string EnemyHP;
	public int EnemyDamage;
	public int EnemyMustache;
	public int EnemyScore;

	public Mustache_Script mustacheSriptHolder;
	public Transform arrowWaitLineTransHolder;
	public Transform mustacheTransHolder;
	public Transform decoyParent;

	public Image healthImage_FillHolder;
	public AnimationClip[] myHitHead_AnimClipsArr;
	public Animation myHPBack_AnimHolder;
	public Animation myHitHead_AnimHolder;
	public Animation myHPBack_WaitLineHolder;
	public bool amInvincible;

	// Enemy Unique Types
	public bool amItemPickup;
	public bool amPosioned;
	public bool amExplosive;
	public bool amFreezing;
	public bool amHealing;
	public bool amAlbinoReverse;
	public bool amThorns;
	public bool am2ndCostume;
	// For super mustache and any other
	public bool amImmortal;

	// Explosive Info
	public bool wasHurtByExplosion;

	public bool amDead;
	public Enemy.EnemyType myEnemyType;
	public Color mustacheColorFinal;

	[Header ("FaceHit Particles")]
	public ParticleSystem particleHitSparkRef;
	public ParticleSystem particleHitPipe_BloodHolder;
	public ParticleSystem particleHitPipe_WaterHolder;
	public ParticleSystem particleHitExtra_ToothHolder;
	public ParticleSystem particleHitExtra_WaterHolder;

	[HideInInspector]
	public ParticleSystem particleHitPipe_CurrRef;
	[HideInInspector]
	public ParticleSystem particleHitExtra_CurrRef;

	public ParticleSystem particleHitFist_NormRef;
	public ParticleSystem particleHitFist_CritRef;
	public ParticleSystem particleLuckyDeathExplodeHolder;
	public ParticleSystem[] particleBodyArrRef;
	public ParticleSystem[] particleHPBackRef;
	public GameObject HPBackObjParentHolder;
	public Text myHPtextHolder;
	public Sprite[] spriteHpFillARR;

//	private ParticleSystem particleHPcritRef;

	[Header ("Movers For Death and more")]
	public EnemyMover_Common enemyMoverScript_Common;
	public EnemyMover_HF enemyMoverScript_HF;
	public EnemyMover_Barrel enemyMoverScript_Barrel;
	public EnemyMover_Item enemyMoverScript_Item;

	[Header ("HP Anim Hurt Clip")]
	public AnimationClip animClipHPbackHurtHolder;

	[Header ("Display 3 Arrows System")]
	public Image[] imageArrow_3Arr;
//	public Transform[] transHappyArrowArr;

//	public ArrowDisplay3_Script[] arrowDispScriptArr;
//	public Sprite[] spriteArrow_TypeArr;

//	public GameObject objEnemyArrowPrev_1;
//	public GameObject objEnemyArrowPrev_2;
//	public GameObject objEnemyArrowPrev_3;
//
//	public GameObject[] objEnemyArrowSourceArr_1;
//	public GameObject[] objEnemyArrowSourceArr_2;
//	public GameObject[] objEnemyArrowSourceArr_3;

//	public EnemyMover_Common enemyMoverScript_Common;

	private int myEnemyTypeInt;
	private int myHurt;
	private int myChance;
	private int myHPlength;
	private int punchCount;

	private int deathNumber;

	[HideInInspector]
	public float myHpLength_Max;

	private float myHP_CurrNormalized;

	// Endless Score
	public Text text_EnemyScore;
	public Transform trans_EnemyScoreSource;
	private bool haveScore;

//	[SerializeField]
//	private Transform[] refArrowsArr;

	// For reference of body mover trans (For super mustache at least)
	private Transform transBodyMoverRef;
	private Vector3 v3_BodyMoverBuffer;

	private Transform tempTransformChild;
	// Moved to publics
//	private Transform transHappyArrowArr;

	private Vector3 tempVec3Rotation;
	// This is for rotation of normal arrows
	private Vector3 tempVec3RotationNew;
	private float tempFloatRotation;

//	[SerializeField]
//	private List<Transform> enemyArrowsList;

	void OnEnable () {
		EventManager.StartListening ("ArrowHappyTime", ArrowHappyTime_Now);
//		Debug.Log ("Enabled!");
	}

	void OnDisable () {
		EventManager.StopListening ("ArrowHappyTime", ArrowHappyTime_Now);
	}
		
	void Awake () {

		// Only for Initialize Random Test. DELETE later
		//RandomizeEnemyHP_Test();

		// Basic Personal Initialize
		amDead = false;

//		enemyArrowsList = new List<Transform>();
//		enemyArrowsList.Clear ();

		// Get values from EnemyValues
		// For non-spawner, should be here (For EnemyMaker ONLY);
//		GetMyValues_Health();

		// Health Start Report
//		Debug.LogError ("Call HEALTH! enemyArrowsList size = " + enemyArrowsList.Count + " And my length = " + myHPlength);

		// Setup pipe particle
//		particleHitPipe_CurrRef = particleHitPipe_BloodHolder;
		particleHitPipe_CurrRef = particleHitPipe_WaterHolder;
	}

	void WaitLine_FillNow () {
		
		for (int i = 0; i < 3; i++) {
			Transform newArrow = Instantiate (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr[0]);
			newArrow.SetParent (arrowWaitLineTransHolder);
			newArrow.localScale = Vector3.one;
			imageArrow_3Arr [i] = newArrow.GetChild (0).GetComponent <Image> ();
			imageArrow_3Arr [i].transform.parent.localPosition = Enemy_ArrowSource_Script.Instance.v3_ArrowNewWaitPositionArr[i];

			// Colors for arrows in case of 2nd costume of albino / reverse
			if (amAlbinoReverse) {
				// Blue Arrows
				newArrow.GetChild(0).GetComponent<Image> ().color = new Color (0, 0.45F, 0.9F, 1);
//				newArrow.GetChild(0).GetComponent<Image> ().color = Color.black;

				// For happy hour arrows of 2nd costume albino / reverse
				newArrow.GetChild(1).GetComponentInChildren<Image> ().color = new Color (0, 0.45F, 0.9F, 1);
//				newArrow.GetChild(1).GetComponentInChildren<Image> ().color = Color.black;
			}
		}

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			StartCoroutine (MyScore_Initiate ());
		}

		// Full old
		/*
//			if (i > 2) {
//				newArrow.localScale = Vector3.zero;
//			} else {
//				newArrow.localScale = Vector3.one;
//			}

			//				Debug.Log ("Tom");
			//				newArrow.localScale = Vector3.one;

//			newArrow.localPosition = new Vector3 (newArrow.localPosition.x, newArrow.localPosition.y, 0);
		

		// For OLD arrow system
		//		for (int i = 0; i < myHPlength; i++) {
//			Transform newArrow = Instantiate (enemyArrowsList [i]);
//			newArrow.SetParent (arrowWaitLineTransHolder);
//			if (i > 2) {
//				newArrow.localScale = Vector3.zero;
//			} else {
//				newArrow.localScale = Vector3.one;
//			}
//			//				Debug.Log ("Tom");
//			//				newArrow.localScale = Vector3.one;
//			newArrow.localPosition = new Vector3 (newArrow.localPosition.x, newArrow.localPosition.y, 0);
//		}
		*/

		// Initialize My Visual Health
		myHpLength_Max = myHPlength;
		StartCoroutine (UpdateMyVisualHealth ());
	}

	public IEnumerator ArrowsColorize_Gold () {
		yield return null;
		for (int i = 0; i < 3; i++) {
			imageArrow_3Arr [i].color = new Color32 (255, 255, 113, 255);
		}
	}

	public void GetMyValues_Health () {

        // Needs to be comment for HP randomizer to work.
        EnemyHP = myValuesHolder.enemyHP;
        EnemyDamage = myValuesHolder.enemyDamage;

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			EnemyScore = myValuesHolder.enemyScore;
		}

		myEnemyType = myValuesHolder.enemyType;
		EnemyMustache = myValuesHolder.enemyMustache;

		SetUpMyHP ();

		// Colorize mustache 
		mustacheSriptHolder.Mustache_Colorize (mustacheColorFinal);

		// TODO: Delete if not used here for 2nd cost
		// Second costume stuff
//		switch (myEnemyTypeInt) {
//		case 0:
//			break;
//		case 1:
//			break;
//		case 2:
//			break;
//		case 3:
//			break;
//		case 4:
//			break;
//		case 5:
//			break;
//		case 6:
//			break;
//		case 7:
//			break;
//			
//		default:
//			break;
//		}
	}

	public void SetUpMyHP () {
        // Read enemy HP string
        myHPlength = EnemyHP.Length;

		// For OLD arrow system
//        for (int i = 0; i < myHPlength; i++) {
//			switch (EnemyHP [i]) {
//			case 'U':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [0]);
//				break;
//			case 'D':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [1]);
//				break;
//			case 'R':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [2]);
//				break;
//			case 'L':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [3]);
//				break;
//			case '-':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [4]);
//				break;
//			case '|':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [5]);
//				break;
//			case '^':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [6]);
//				break;
//			case 'v':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [7]);
//				break;
//			case '>':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [8]);
//				break;
//			case '<':
//				enemyArrowsList.Add (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [9]);
//				break;
//			}
//		}

		WaitLine_FillNow ();

		// Check for long health instantiate slowdowns (Needs more work);
//		if (myHPlength < 150) {
//			WaitLine_FillNow ();
//		} else {
//			StartCoroutine (WaitLine_FillNow_LongOnes ());
//			Debug.LogError ("LOOOOOOOONG HP!");
//		}

		// Get HP Crit Particle
//		particleHPcritRef = myHPtextHolder.GetComponentInChildren<ParticleSystem>();

		// For spawn after happy time
		if (PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {
			ArrowHappyTime_Now ();
		}
	}

	public IEnumerator MyScore_Initiate () {
		yield return null;
		if (!amItemPickup) {
			Transform newScore = Instantiate (Enemy_ArrowSource_Script.Instance.transArrowsSourceArr [1]);
			newScore.SetParent (arrowWaitLineTransHolder.parent);
			//			newScore.localScale = Vector3.zero;
			newScore.position = Vector3.zero;

			yield return null;
			text_EnemyScore = newScore.GetComponentInChildren <Text> ();

			// Moved the score text from here to death of enemy
//			text_EnemyScore.text = (EnemyScore * PlayerData_InGame.Instance.p1_ComboMultiplier).ToString ();

			trans_EnemyScoreSource = newScore;

			newScore.gameObject.SetActive (false);
			haveScore = true;
		}
	}

	public IEnumerator MyScore_Show () {
		yield return null;
		if (!amItemPickup) {
			text_EnemyScore.text = (EnemyScore).ToString ();
			trans_EnemyScoreSource.gameObject.SetActive (true);
			trans_EnemyScoreSource.transform.SetParent (arrowWaitLineTransHolder.parent.parent.parent);
			Destroy (trans_EnemyScoreSource.gameObject, 1);
		}
	}

	// Should be same as BELOW CompareMyArrow_AmImmortal (TODO: Remember)
	public void CompareMyArrow () {
        if (!amInvincible && !amDead) {
			if (!PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {
				//Debug.LogError ("The Key Shape 1 = " + EventTrigger.theKey_Shape1[0] +
				//    "  The Key Shape 2 = " + EventTrigger.theKey_Shape2[0] + " And the HP = " +
				//    EnemyHP[0]
				//    );

				if (EventTrigger.theKey_Shape1 [0] == EnemyHP [0] || 
					EventTrigger.theKey_Shape2 [0] == EnemyHP [0]) {
					CorrectHit ();
				}
			} else {
				
				if (EventTrigger.theKey_Shape1 [0] == EventTrigger.theKey_HappyTimeDirection [0] || 
					EventTrigger.theKey_Shape2 [0] == EventTrigger.theKey_HappyTimeDirection [0]) {
					CorrectHit ();
				}
			}
		}
	}

	public void CorrectHit () {
		// With each correct hit, increase the intOneComboCount so we can find out about the combo
		PlayerData_InGame.Instance.intOneComboCount++;

		// Update Correct hit for Camera Move
		EventTrigger.isCorrectHit = true;

		// Find Damage Amount for Critical
		DamageCalculator ();

		if (amThorns) {
			enemyMoverScript_Common.HurtByThorns ();
		}
	}

	// Should be same as ABOVE CompareMyArrow (TODO: Remember)
	public void CompareMyArrow_AmImmortal () {
		if (!amInvincible && !amDead) {
			
			// Update Correct hit for Camera Move
			EventTrigger.isCorrectHit = true;

			if (!PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {

				if (EventTrigger.theKey_Shape1 [0] == EnemyHP [0] || 
					EventTrigger.theKey_Shape2 [0] == EnemyHP [0]) {
					// Update Correct hit for Camera Move
					EventTrigger.isCorrectHit = true;

					// Find Damage Amount for Critical
					HurtMe_AmImmortal ();

					// With each correct hit, increase the intOneComboCount so we can find out about the combo
					PlayerData_InGame.Instance.intOneComboCount++;
				}
			} else {

				if (EventTrigger.theKey_Shape1 [0] == EventTrigger.theKey_HappyTimeDirection [0] || 
					EventTrigger.theKey_Shape2 [0] == EventTrigger.theKey_HappyTimeDirection [0]) {
					// Update Correct hit for Camera Move
					EventTrigger.isCorrectHit = true;

					// Find Damage Amount for Critical
					HurtMe_AmImmortal ();

					// With each correct hit, increase the intOneComboCount so we can find out about the combo
					PlayerData_InGame.Instance.intOneComboCount++;
				}
			}
		}
	}

	IEnumerator ChangeMyFaceNParticle () {
		yield return null;

		switch (EventTrigger.theKey_Direction) {
		case "U":
			myHitHead_AnimHolder.clip = myHitHead_AnimClipsArr [0];
			myHitHead_AnimHolder.Play ();
			particleHitSparkRef.startRotation = -1.57F;

			if (!amItemPickup && PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
				ChangeMyFace_PipeParticle (Random.Range (75, 90));
			} 

//			else {
//				ChangeMyFace_ExtraParticle (Random.Range (75, 90));
//			}

//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, 90);
			break;
		case "D":
			myHitHead_AnimHolder.clip = myHitHead_AnimClipsArr [1];
			myHitHead_AnimHolder.Play ();
			particleHitSparkRef.startRotation = 1.57F;

			if (!amItemPickup && PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
				ChangeMyFace_PipeParticle (Random.Range (-110, -95));
			} 

//			else {
//				ChangeMyFace_ExtraParticle (Random.Range (-110, -95));
//			}

//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, -90);
			break;
		case "R":
			myHitHead_AnimHolder.clip = myHitHead_AnimClipsArr [2];
			myHitHead_AnimHolder.Play ();
			particleHitSparkRef.startRotation = 0F;

			if (!amItemPickup && PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
				ChangeMyFace_PipeParticle (Random.Range (-5, 10));
			} 

//			else {
//				ChangeMyFace_ExtraParticle (Random.Range (-5, 10));
//			}

//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, 0);
			break;
		case "L":
			myHitHead_AnimHolder.clip = myHitHead_AnimClipsArr [3];
			myHitHead_AnimHolder.Play ();
			particleHitSparkRef.startRotation = 3.14F;

			if (!amItemPickup && PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
				ChangeMyFace_PipeParticle (Random.Range (145, 160));
			}

//			else {
//				ChangeMyFace_ExtraParticle (Random.Range (145, 160));
//			}

//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, 180);
			break;
		}
		// Optimize Later. Also use string or char for both. OLD
//		switch (EventTrigger.theKey_Direction) {
//		case "U":
//			particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 90);
//			break;
//		case "D":
//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, -90);
//			particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, -90);
//			break;
//		case "R":
//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, 0);
//			particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 0);
//			break;
//		case "L":
//			particleHitSparkRef.transform.parent.eulerAngles = new Vector3 (0, 0, 180);
//			particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 180);
//			break;
//		}

		particleHitSparkRef.Play ();
	}

	public void ChangeMyFace_PipeParticle (int degree) {
//		if (PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
			particleHitPipe_CurrRef.transform.parent.eulerAngles = new Vector3 (0, 0, degree);
			particleHitPipe_CurrRef.Play ();
//		}
	}

	public void ChangeMyFace_ExtraParticle (int degree) {
		if (!amItemPickup) {
			if (Random.Range (0, 2) == 0) {
				particleHitExtra_CurrRef = particleHitExtra_ToothHolder;
			} else {
				particleHitExtra_CurrRef = particleHitExtra_WaterHolder;
			}

			particleHitExtra_CurrRef.transform.parent.eulerAngles = new Vector3 (0, 0, degree);
			particleHitExtra_CurrRef.Play ();
		}
	}

	public IEnumerator UpdateMyVisualHealth () {
		
		myHP_CurrNormalized = myHPlength / myHpLength_Max;
		if (myHPlength < 100) {
			myHPtextHolder.text = myHPlength.ToString ();
		} else {
			myHPtextHolder.text = "99";
		}

		if (myHP_CurrNormalized > 0.8F) {
			healthImage_FillHolder.sprite = spriteHpFillARR [0];
		} else if (myHP_CurrNormalized > 0.6F) {
			healthImage_FillHolder.sprite = spriteHpFillARR [1];
		} else if (myHP_CurrNormalized > 0.4F) {
			healthImage_FillHolder.sprite = spriteHpFillARR [2];
		} else if (myHP_CurrNormalized > 0.2F) {
			healthImage_FillHolder.sprite = spriteHpFillARR [3];
		} else {
			healthImage_FillHolder.sprite = spriteHpFillARR [4];
		}

		// For 3 arrows display
		StartCoroutine (EnemyArrowDisplay ());

		// For hp and punch log
//		if (myEnemyType == Enemy.EnemyType.Giant1) {
//			
//			Debug.LogWarning ("Giant HP = " + myHPlength + " after " + punchCount + " punches");
//			punchCount++;
//		}

		yield return null;
		// Original method. Not compatible with Android
//		myHPtextHolder.text = myHPlength.ToString();
	}

	IEnumerator HP_ChangeColor () {
//		myHPtextHolder.fontSize = 80;
//		myHPtextHolder.GetComponentInParent<Image>().color = Color.red;
//		myHPtextHolder.color = new Color (0.7F, 0.3F, 0.3F);
//		particleHPcritRef.Play();
//		myHPtextHolder.transform.parent.GetComponent <Image>().color = Color.green;
		myHPtextHolder.color = Color.white;

		yield return new WaitForSeconds (0.12F);

//		myHPtextHolder.fontSize = 47;
//		myHPtextHolder.GetComponentInParent<Image>().color = Color.white;
//		myHPtextHolder.color = new Color (1, 1, 1);
//		myHPtextHolder.transform.parent.GetComponent <Image>().color = Color.white;
		myHPtextHolder.color = Color.black;
	}

	void DamageCalculator () {
		if (PlayerData_InGame.Instance.critHappened) {
			myHurt = PlayerData_InGame.Instance.p1_Damage_Base * 2;

			// Change Color & Particle
//			StartCoroutine (HP_ChangeColor ());

//			particleHPBackRef[5].Play ();

			// particleHPBackRef [5] = Burst white area crit For HP particle
		} else {
			myHurt = PlayerData_InGame.Instance.p1_Damage_Base;
		}

		HurtMe (myHurt, false);

		// Use listener of item for this
//		if (!amImmortal) {
//			HurtMe (myHurt, false);
//		} else {
//			// Means AM immortal
//			HurtMe_AmImmortal ();
//		}
	}

	IEnumerator HurtHP_Animate () {
		//Don't give the name because it is default
		myHPBack_AnimHolder.clip = animClipHPbackHurtHolder;

//		yield return new WaitForSeconds (Random.Range (0, 0.2F));

		yield return null;
		myHPBack_AnimHolder.Stop ();
//		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = 0;
		myHPBack_AnimHolder.Play ();

		// Old one with title
//		myHPBack_AnimHolder.Play ("Enemy HP Back Hurt Anim 1 (Legacy)");
	}

//	public void HurtMe (int damage, bool powUpDead) {
//		StartCoroutine (HurtMe_Routine (damage, powUpDead));
//	}

	public IEnumerator EnemyArrowDisplay () {
		//		Debug.LogWarning ("NEW New HP Set");
//		imageHPArrow3Arr[0].sprite = 

		for (int i = 0; i < 3; i++) {
			if (i < EnemyHP.Length) {
				Enemy_SingleArrow (imageArrow_3Arr [i], EnemyHP [i]);
			} else {
				imageArrow_3Arr [i].enabled = false;

				if (PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {
					arrowWaitLineTransHolder.GetChild (i).GetChild (1).gameObject.SetActive (false);
				}
			}
			yield return null;
		}
	}

	void Enemy_SingleArrow (Image imageEnemy, char charEnemy) {
		switch (charEnemy) {

		// Arrows
		case 'U':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [0];
			tempVec3RotationNew = Vector3.zero;
			break;
		case 'D':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [0];
			tempVec3RotationNew = new Vector3 (0, 0, 180);
			break;
		case 'R':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [0];
			tempVec3RotationNew = new Vector3 (0, 0, 270);
			break;
		case 'L':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [0];
			tempVec3RotationNew = new Vector3 (0, 0, 90);
			break;

		// '|'s
		case '-':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [1];
			tempVec3RotationNew = new Vector3 (0, 0, 90);
			break;
		case '|':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [1];
			tempVec3RotationNew = Vector3.zero;
			break;

		// '^'s
		case '^':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [2];
			tempVec3RotationNew = Vector3.zero;
			break;
		case 'v':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [2];
			tempVec3RotationNew = new Vector3 (0, 0, 180);
			break;
		case '>':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [2];
			tempVec3RotationNew = new Vector3 (0, 0, 270);
			break;
		case '<':
			imageEnemy.sprite = Enemy_ArrowSource_Script.Instance.spriteArrowSourceArr [2];
			tempVec3RotationNew = new Vector3 (0, 0, 90);
			break;
		}

		if (amAlbinoReverse) {
			tempVec3RotationNew.z += 180;
		}
		imageEnemy.transform.eulerAngles = tempVec3RotationNew;
	}

	public void EnemyArrowDisplay_OLD () {
//		Debug.LogWarning ("New HP Set");
		for (int i = 0; i < 3; i++) {
			if (i < EnemyHP.Length) {
//				Debug.LogError ("EnemyHP [i] = " + EnemyHP [i] + " At i of " + i + " with EnemyLength of " + EnemyHP.Length);
//				arrowDispScriptArr [i].ArrowState_Display (EnemyHP [i]);

			} else {
//				Debug.LogError ("YO");
//				arrowDispScriptArr [i].ArrowState_TurnOff ();
			}
		}

//		arrowDispScriptArr[0].ArrowState_Display (EnemyHP [0]);
//		arrowDispScriptArr[1].ArrowState_Display (EnemyHP [1]);
//		arrowDispScriptArr[2].ArrowState_Display (EnemyHP [2]);

//		objEnemyArrowPrev_1.SetActive (false);
//		objEnemyArrowPrev_2.SetActive (false);
//		objEnemyArrowPrev_3.SetActive (false);
//
//		switch (EnemyHP [0]) {
//		case 'U':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [0];
//			break;
//		case 'D':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [1];
//			break;
//		case 'R':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [2];
//			break;
//		case 'L':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [3];
//			break;
//		case '-':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [4];
//			break;
//		case '|':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [5];
//			break;
//		case '^':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [6];
//			break;
//		case 'v':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [7];
//			break;
//		case '>':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [8];
//			break;
//		case '<':
//			objEnemyArrowPrev_1 = objEnemyArrowSourceArr_1 [9];
//			break;
//		}
//
//		switch (EnemyHP [0]) {
//		case 'U':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [0];
//			break;
//		case 'D':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [1];
//			break;
//		case 'R':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [2];
//			break;
//		case 'L':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [3];
//			break;
//		case '-':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [4];
//			break;
//		case '|':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [5];
//			break;
//		case '^':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [6];
//			break;
//		case 'v':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [7];
//			break;
//		case '>':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [8];
//			break;
//		case '<':
//			objEnemyArrowPrev_2 = objEnemyArrowSourceArr_2 [9];
//			break;
//		}
//
//		switch (EnemyHP [0]) {
//		case 'U':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [0];
//			break;
//		case 'D':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [1];
//			break;
//		case 'R':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [2];
//			break;
//		case 'L':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [3];
//			break;
//		case '-':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [4];
//			break;
//		case '|':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [5];
//			break;
//		case '^':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [6];
//			break;
//		case 'v':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [7];
//			break;
//		case '>':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [8];
//			break;
//		case '<':
//			objEnemyArrowPrev_3 = objEnemyArrowSourceArr_3 [9];
//			break;
//		}
//
//		objEnemyArrowPrev_1.SetActive (true);
//		objEnemyArrowPrev_2.SetActive (true);
//		objEnemyArrowPrev_3.SetActive (true);

	}
		
	public IEnumerator ShootSeparateMustache () {
		Mustache_Script newMustache = Instantiate (mustacheSriptHolder);
//		newMustache.gameObject.name = "BOO!";
//		Debug.LogWarning ("Super Mustache Position = " + enemyMoverScript_Item.myRemover_AnimHolder.transform.position);

		v3_BodyMoverBuffer = transBodyMoverRef.position;
		v3_BodyMoverBuffer.x += 0.4F;
		v3_BodyMoverBuffer.y += 2.1F;

		newMustache.transform.position = v3_BodyMoverBuffer;
		newMustache.ShootMustache (1);
		yield return null;
	}

	public void ImmortalSetup () {
		amImmortal = true;
//		mustacheSriptHolder.Mustache_LayerSet ();
		transBodyMoverRef = enemyMoverScript_Item.myRemover_AnimHolder.transform;
		StartCoroutine (ArrowsColorize_Gold ());
	}

	// Should be same as BELOW HurtMe (TODO: Remember)
	public void HurtMe_AmImmortal () {
		// Update And Animate Enemy ITEM Face (New Place)
		StartCoroutine (ChangeMyFaceNParticle ());

		// Am NOT dead
		// Turns was hurt by explosion to false in case enemy did NOT die by the explosion
		wasHurtByExplosion = false;

		// New animate
		myHPBack_WaitLineHolder.Play ();

		// Update HP String
		EnemyHP = EnemyHP.Substring (1, EnemyHP.Length - 1);

		// For more than one damage to the end of string
//		if (damage > 0) {
//			EnemyHP = EnemyHP.Substring (0, EnemyHP.Length - (damage - 1));
//		}
//		myHPlength = EnemyHP.Length;

		// Animate Back HP Holder / Animate by being hit
		StartCoroutine (HurtHP_Animate());

		// Update my visual HP (Fill Bar)
		StartCoroutine (UpdateMyVisualHealth ());

		// Shoot separate mustache for super mustache
		StartCoroutine (ShootSeparateMustache ());
	}

	// Should be same as ABOVE HurtMe_AmImmortal (TODO: Remember)
	public void HurtMe (int damage, bool powUpDead) {
		// Update And Animate Enemy Face (New Place)
		StartCoroutine (ChangeMyFaceNParticle ());

		// Check that the damage does not go overboard (I die!)
		if (powUpDead && amItemPickup) {
			// Using the sibling thing
			transform.SetAsLastSibling ();

			// Like return for the old system
//			yield return null;
//			yield break;

			// For non coroutine method
			return;
		}

		if (damage >= myHPlength) {
			damage = myHPlength;
			amDead = true;
//			Debug.Log (this.gameObject.name + " AM DEAD! 11111");

			// Increase number of dead enemies
			PlayerData_InGame.Instance.p1_TotalKills_All++;
		}
			
		if (!amDead) {
			// Turns was hurt by explosion to false in case enemy did NOT die by the explosion
			wasHurtByExplosion = false;

			// Previously, ChangeMyfaceparticle was here
//			ChangeMyFaceNParticle ();

			// For OLD arrow system
//			// Remove Arrows from Wait Line
			/*
////			arrowWaitLineTransHolder.GetChild (0).GetComponent<Animation> ().Play ("Enemy HP Remove Anim 1 (Legacy)");
//			arrowWaitLineTransHolder.GetChild (0).SetParent (decoyParent);
//			arrowWaitLineTransHolder.GetChild (0).transform.localScale = Vector2.zero;
////			arrowWaitLineTransHolder.GetChild (0).gameObject.SetActive (false);
////			StartCoroutine (HP_ChangeColor ());
//
//			if (damage > 1) {
//				for (int i = 1; i < (damage); i++) {
//
//					// For old arrow remove anim
////					if ((myHPlength - i - 1) < 2) {
////						arrowWaitLineTransHolder.GetChild (myHPlength - i - 1).GetComponent<Animation> ().Play ("Enemy HP Remove Anim 1 (Legacy)");
////					}
//
//					// Health Report
////					Debug.Log ("That Out of bounds problem. myHPlength = " + myHPlength
////					+ " And (myHPlength - 1) = " + (myHPlength - i - 1) + " And decoy parent position = " + decoyParent.position);
//
//					arrowWaitLineTransHolder.GetChild (myHPlength - i - 1).SetParent (decoyParent);
//				}
//			}
			*/

			// New animate
			myHPBack_WaitLineHolder.Play ();
//			Debug.LogError ("New HP Wait Line Animate");


			// For OLD arrow system
			/*
//			if (arrowWaitLineTransHolder.childCount > 2) {

////				arrowWaitLineTransHolder.GetChild (0).GetComponent<Animation> ().Play ("Enemy HP Strafe Anim 1 (Legacy)");
//				arrowWaitLineTransHolder.GetChild (0).localScale = Vector3.one;
////				arrowWaitLineTransHolder.GetChild (1).GetComponent<Animation> ().Play ("Enemy HP Strafe Anim 1 (Legacy)");
//				arrowWaitLineTransHolder.GetChild (1).localScale = Vector3.one;
////				arrowWaitLineTransHolder.GetChild (2).GetComponent<Animation> ().Play ("Enemy HP Add Anim 1 (Legacy)");
//				arrowWaitLineTransHolder.GetChild (2).localScale = Vector3.one;
//			} else {
//				switch (arrowWaitLineTransHolder.childCount) {
//				case 2:
////					arrowWaitLineTransHolder.GetChild (0).GetComponent<Animation> ().Play ("Enemy HP Strafe Anim 1 (Legacy)");
//					arrowWaitLineTransHolder.GetChild (0).localScale = Vector3.one;
////					arrowWaitLineTransHolder.GetChild (1).GetComponent<Animation> ().Play ("Enemy HP Strafe Anim 1 (Legacy)");
//					arrowWaitLineTransHolder.GetChild (1).localScale = Vector3.one;
//					break;
//				case 1:
////					arrowWaitLineTransHolder.GetChild (0).GetComponent<Animation> ().Play ("Enemy HP Strafe Anim 1 (Legacy)");
//					arrowWaitLineTransHolder.GetChild (0).localScale = Vector3.one;
//					break;
//				case 0:
////					Debug.LogError ("DEATH!");
//					break;
//				}
//			}
			*/

			// Update HP String
			EnemyHP = EnemyHP.Substring (1, EnemyHP.Length - 1);
			if (damage > 0) {
				EnemyHP = EnemyHP.Substring (0, EnemyHP.Length - (damage - 1));
			}
			myHPlength = EnemyHP.Length;

//			myHPBack_AnimHolder.Stop ();	

			// Animate Back HP Holder / Animate by being hit
			StartCoroutine (HurtHP_Animate());

			// Update my visual HP (Fill Bar)
			StartCoroutine (UpdateMyVisualHealth ());

//			yield return null;
		} 

		// I am dead!
		else {
			// Disable hit head at the moment of death (To avoid stupid face shown in front of mustache on death bug) (Now use sprite disable in mover)
//			myHitHead_AnimHolder.Stop ();
//			myHitHead_AnimHolder[myHitHead_AnimHolder.clip.name].speed = 0;

			if (haveScore) {
				EnemyScore *= PlayerData_InGame.Instance.p1_ComboMultiplier;

				// Show enemy score effect
				StartCoroutine (MyScore_Show ());

				// Give score to the player
				StartCoroutine (PlayerData_InGame.Instance.PlayerScore_Gain (EnemyScore));

				// Kill reward for non-itempickups
				if (!amItemPickup) {
					StartCoroutine (PlayerData_InGame.Instance.PlayerCombo_KillReward ());
				}
			}

//			Debug.LogError ("Score = " + EnemyScore);

			myEnemyTypeInt = (int)myEnemyType;
			// For feedback test only. FOR DELETE
//			FindObjectOfType<EnemyMaker_Test>().UpdateNumber_Kills();

			// Activate Mustache!
			if (!amItemPickup) {
				mustacheTransHolder.SetParent (transform.root);
				mustacheSriptHolder.ShootMustache (EnemyMustache);

				// New Update stats for enemies killed
				PlayerData_InGame.Instance.Player_EnemyKill_Increase ();

				// Old Update stats for enemies killed
//				PlayerData_InGame.Instance.stats_EnemiesKilled++;
			}

			// Change parent in hierarchy at the moment of death
			transform.SetParent (transform.root);

			// Last check before the end?
//			EnemyHealth_LastEnemyCheck ();

			// Check to see which mover script needs ref
			if (!powUpDead) {

				// If it's not pow up death, then check for last enemy (End Check) (This check is for enemy KILLED by player)
				EnemyHealth_LastEnemyCheck ();

				// Copied from EnemySpawner Switch line
				switch (myEnemyTypeInt) {
				case 0: 
				case 1: 
				case 2: 
				case 3: 
				case 4: 
				case 5: 
				case 6: 
				case 7: 
					enemyMoverScript_Common.DestroyMe_RemoveMe ();
					break;
				case 8: 
				case 9: 
				case 10: 
				case 11:
					enemyMoverScript_HF.DestroyMe_RemoveMe ();
					break;
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
					enemyMoverScript_Barrel.DestroyMe_RemoveMe ();
					break;
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 30:
				case 31:
				case 32:
				case 33:
				case 34:
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
					// TODO: New enemies after barrel (YES Pow Up Dead PART)
					Debug.LogError ("Enemy place holders");
					break;
				case 40:
				case 41:
				case 42:
				case 43:
				case 44:
				case 45:
				case 46:

					// Super mustache and the rest
				case 47:
				case 48:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
					enemyMoverScript_Item.DestroyMe_RemoveMe ();

					// SOUNDD EFFECTS - Item Bubble Pop (done)
					if (SoundManager.Instance != null) {
						SoundManager.Instance.TerekidaneHobab.Play ();
					}

					break;
				default:
					Debug.LogError ("Enemy spawner Default");
					break;
				}

				// Old "IF" method of killing enemies WITHOUT pow up
//				if (myEnemyTypeInt < 8) {
//					enemyMoverScript_Common.DestroyMe_RemoveMe ();
//				} else {
//					if (myEnemyTypeInt >= 8 && myEnemyTypeInt < 12) {
//						enemyMoverScript_HF.DestroyMe_RemoveMe ();
//					} else {
//						if (myEnemyTypeInt >= 12 && myEnemyTypeInt < 18) {
//							enemyMoverScript_Barrel.DestroyMe_RemoveMe ();
//						} else {
//							if (myEnemyTypeInt >= 18 && myEnemyTypeInt < 40) {
//								// TODO: New enemies after barrel (Non Pow Up Dead PART)
//
//							} else {
//								enemyMoverScript_Item.DestroyMe_RemoveMe ();
//
//								// SOUNDD EFFECTS - Item Bubble Pop (done)
//								if (SoundManager.Instance != null) {
//									SoundManager.Instance.TerekidaneHobab.Play ();
//								}
//							}
//
//						}
//					}
//				}

				switch (EventTrigger.theKey_Direction [0]) {
				case 'U':
					deathNumber = 0;
//					particleHPBackRef [0].Play ();
//				particleHPBackRef[0].transform.SetParent (transform.root);
//				particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 90);
					break;
				case 'D':
					deathNumber = 1;
//					particleHPBackRef [1].Play ();
//				particleHPBackRef[1].transform.SetParent (transform.root);
//				particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, -90);
					break;
				case 'R':
					deathNumber = 2;
//					particleHPBackRef [2].Play ();
//				particleHPBackRef[2].transform.SetParent (transform.root);
//				particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 0);
				// For Follow
//				particleHPBackRef[2].transform.GetChild(0).GetComponent<ParticleSystem>().Play ();
					break;
				case 'L':
					deathNumber = 3;
//					particleHPBackRef [3].Play ();
//				particleHPBackRef[3].transform.SetParent (transform.root);
//				particleHitFist_NormRef.transform.parent.eulerAngles = new Vector3 (0, 0, 180);
					break;
				}

				// To make sure the death hp of albino is white
				if (amAlbinoReverse) {
					particleHPBackRef [deathNumber].startColor = Color.white;
				}

				particleHPBackRef [deathNumber].Play ();
			} 

			// Death by Power Up 1
			else {
				// If it's pow up death, then check for last enemy a bit LATER (End Check)
				PlayerData_InGame.Instance.Player_ForceLastEnemyCheck_LATE ();

				deathNumber = Random.Range (0, 4);

				// Copied from EnemySpawner Switch line
				switch (myEnemyTypeInt) {
				case 0: 
				case 1: 
				case 2: 
				case 3: 
				case 4: 
				case 5: 
				case 6: 
				case 7: 
					enemyMoverScript_Common.DestroyMe_RemoveMe_PowUp (deathNumber);
					break;
				case 8: 
				case 9: 
				case 10: 
				case 11:
					enemyMoverScript_HF.DestroyMe_RemoveMe_PowUp (deathNumber);
					break;
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
					enemyMoverScript_Barrel.DestroyMe_RemoveMe_PowUp (deathNumber);
					break;
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 30:
				case 31:
				case 32:
				case 33:
				case 34:
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
					// TODO: New enemies after barrel (YES Pow Up Dead PART)
					Debug.LogError ("Enemy place holders");
					break;
				case 40:
				case 41:
				case 42:
				case 43:
				case 44:
				case 45:

				// Random Item
				case 46:

				// Super mustache and the rest
				case 47:
				case 48:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
					enemyMoverScript_Item.DestroyMe_RemoveMe ();
					break;
				default:
					Debug.LogError ("Enemy spawner Default");
					break;
				}

				// Old "IF" method of killing enemies by pow up
//				if (myEnemyTypeInt < 8) {
//					enemyMoverScript_Common.DestroyMe_RemoveMe_PowUp (deathNumber);
//				} else {
//					if (myEnemyTypeInt >= 8 && myEnemyTypeInt < 12) {
//						enemyMoverScript_HF.DestroyMe_RemoveMe_PowUp (deathNumber);
//					} else {
//						if (myEnemyTypeInt >= 12 && myEnemyTypeInt < 18) {
//							enemyMoverScript_Barrel.DestroyMe_RemoveMe_PowUp (deathNumber);
//						} else {
//							if (myEnemyTypeInt >= 18 && myEnemyTypeInt < 40) {
//								// TODO: New enemies after barrel (YES Pow Up Dead PART)
//
//							} else {
//								enemyMoverScript_Item.DestroyMe_RemoveMe ();
//							}
//						}
//					}
//				}

				particleHPBackRef [deathNumber].Play();
			}

			// Play Poof particle & fist & stop running particle
//			particleHPBackRef [4].Play ();

			// particleHPBackRef [4] = Burst Black For HP particle

//			particleHPBackRef[4].transform.SetParent (transform.root);
			particleBodyArrRef [0].Stop ();
//			particleBodyArrRef[1].Play ();
//			particleHitFist_NormRef.startRotation = -particleHitFist_NormRef.transform.parent.eulerAngles.z * Mathf.Deg2Rad;
//			particleHitFist_NormRef.Play ();

			// Disable actual HP back & wait line
			arrowWaitLineTransHolder.parent.gameObject.SetActive (false);
			HPBackObjParentHolder.SetActive (false);
		}
	
		// Many Reports
//		Debug.LogError ("BEFORE Damage: " + damage + " enemylenght: " + EnemyHP.Length);
//		Debug.Log ("The string is: " + EnemyHP);
	}

	public void LuckyExplode () {
		if (PlayerData_InGame.Instance.p1_LuckyExplodeChance > 0) {
//			Debug.LogWarning ("Has lucky explode chance");

			// Check the lucky chance
			if (Random.Range (0, 100) < PlayerData_InGame.Instance.p1_LuckyExplodeChance) {
				// Increase total of lucky explodes
				PlayerData_InGame.Instance.p1_TotalKills_LuckyExplode++;

				PlayerData_InGame.Instance.MiniExplosion ();

//				Debug.LogWarning ("LUCKY DEATH!");

				EventManager.TriggerEvent ("Explode N Stuff");

				particleLuckyDeathExplodeHolder.Play ();

				// Item active for lucky explode
				PlayerData_InGame.Instance.ItemActiveFeed_Show ("!ﯽﺴﻧﺎﺷ ﺭﺎﺠﻔﻧﺍ");
			}
		}
	}

	// Make sure this is the same as normal hurt player (TODO: Remember!)
	public void HurtPlayer_Thorns () {
		if (EnemyDamage > 40) {
			StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (Mathf.RoundToInt ((EnemyDamage / 8F)), false));
		} else {
			StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (1, false));

			// Old numbers
//			if (EnemyDamage > 10) {
//				StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (Mathf.RoundToInt ((EnemyDamage / 4F)), false));
//			} else {
//				StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (Mathf.RoundToInt ((EnemyDamage / 2F)), false));
//			}
		}
	}

	public void HurtPlayer () {
		StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (EnemyDamage, false));
	}

	public void HurtExplodePlayer () {
		StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (EnemyDamage, true));
	}
		
	public void Shielded_HurtPlayer () {
		StartCoroutine (PlayerData_InGame.Instance.PlayerHurt_Shielded ());
	}

	public void HurtLastCheck () {
		EnemyHealth_LastEnemyCheck ();
	}

	public void EndChangeRoot () {
		this.transform.SetParent (transform.root);
	}

	public void ArrowHappyTime_Now () {
		//		Debug.LogError ("H TIME! 1  ---   p1_ArrowHappyTime = " + PlayerData_InGame.Instance.p1_Item_ArrowHappyTime);
		if (PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {
			switch (EventTrigger.theKey_HappyTimeDirection) {
			case "U":
				tempVec3Rotation = new Vector3 (0, 0, 0);
				tempFloatRotation = 0F;
				break;
			case "D":
				tempVec3Rotation = new Vector3 (0, 0, 180);
				tempFloatRotation = 3.14F;
				break;
			case "R":
				tempVec3Rotation = new Vector3 (0, 0, 270);
				tempFloatRotation = 1.57F;
				break;
			case "L":
				tempVec3Rotation = new Vector3 (0, 0, 90);
				tempFloatRotation = 4.71F;
				break;
			default:
				tempVec3Rotation = new Vector3 (0, 0, 0);
				tempFloatRotation = 0F;
				break;
			}

			for (int i = 0; i < 3; i++) {
				imageArrow_3Arr [i].gameObject.SetActive(false);

				if (imageArrow_3Arr [i].enabled) {
					tempTransformChild = arrowWaitLineTransHolder.GetChild (i);
					tempTransformChild.GetChild (1).gameObject.SetActive (true);

					tempTransformChild.GetChild (1).eulerAngles = tempVec3Rotation;
					tempTransformChild.GetChild (1).GetChild (2).GetComponent<ParticleSystem> ().startRotation = tempFloatRotation + Random.Range (-0.2F, 0.2F);

//					Debug.LogError ("tempTransformChild.GetChild (1). Name = " + tempTransformChild.GetChild (1).name);

//					if (!amAlbinoReverse) {
//						imageArrow_3Arr [i].color = Color.black;
//					} else {
//						imageArrow_3Arr [i].color = Color.white;
//					}
				} 

				// Disable less than 3 arrows
				else {
					tempTransformChild = arrowWaitLineTransHolder.GetChild (i);
					tempTransformChild.GetChild (1).GetChild(1).gameObject.SetActive (false);
				}
			}
		} else {
			for (int i = 0; i < 3; i++) {
				tempTransformChild = arrowWaitLineTransHolder.GetChild (i);
				if (tempTransformChild.GetChild (1).gameObject.activeInHierarchy) {
					tempTransformChild.GetChild (2).GetComponent<ParticleSystem> ().Emit (1);
				}
				tempTransformChild.GetChild (1).gameObject.SetActive (false);

				imageArrow_3Arr [i].gameObject.SetActive(true);
//				if (!amAlbinoReverse) {
//					imageArrow_3Arr [i].color = Color.white;
//				} else {
//					imageArrow_3Arr [i].color = Color.black;
//				}
			}
		}
	}

	public void ArrowHappyTime_Now_OLD () {
//		Debug.LogError ("H TIME! 1  ---   p1_ArrowHappyTime = " + PlayerData_InGame.Instance.p1_Item_ArrowHappyTime);
		if (PlayerData_InGame.Instance.p1_Item_ArrowHappyTime) {
			switch (EventTrigger.theKey_HappyTimeDirection) {
			case "U":
				tempVec3Rotation = new Vector3 (0, 0, 0);
				tempFloatRotation = 0F;
				break;
			case "D":
				tempVec3Rotation = new Vector3 (0, 0, 180);
				tempFloatRotation = 3.14F;
				break;
			case "R":
				tempVec3Rotation = new Vector3 (0, 0, 270);
				tempFloatRotation = 1.57F;
				break;
			case "L":
				tempVec3Rotation = new Vector3 (0, 0, 90);
				tempFloatRotation = 4.71F;
				break;
			default:
				tempVec3Rotation = new Vector3 (0, 0, 0);
				tempFloatRotation = 0F;
				break;
			}

			for (int i = 0; i < arrowWaitLineTransHolder.childCount; i++) {
				tempTransformChild = arrowWaitLineTransHolder.GetChild (i);
				tempTransformChild.GetChild (1).gameObject.SetActive (true);

				tempTransformChild.GetChild (1).eulerAngles = tempVec3Rotation;
				tempTransformChild.GetChild (1).GetChild (2).GetComponent<ParticleSystem> ().startRotation = tempFloatRotation + Random.Range (-0.2F, 0.2F);
				}
		} else {
			for (int i = 0; i < arrowWaitLineTransHolder.childCount; i++) {
				tempTransformChild = arrowWaitLineTransHolder.GetChild (i);
				if (i < 4) {
					tempTransformChild.GetChild (2).GetComponent<ParticleSystem> ().Emit (1);
				}
				tempTransformChild.GetChild (1).gameObject.SetActive (false);
			}
		}
	}

	void EnemyHealth_LastEnemyCheck () {
//		Debug.LogError ("Last health check!");

		if (PlayerData_InGame.Instance.lastEnemyWasSpawned) {
			StartCoroutine (PlayerData_InGame.Instance.Player_CheckLastEnemy ());
		}
	}
}
