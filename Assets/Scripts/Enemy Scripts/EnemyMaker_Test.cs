﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyMaker_Test : MonoBehaviour {

	public static int enemyNumber;
	public static int minLength = 3;
	public static int maxLength = 5;

	public Text textHurts;
	public Text textKills;
	public Text textMoves;
	public Text textTime;
	public Text textAvg;

	public Text textMoves100;
	public Text textAvg100;
	public Text textTime100;
	public GameObject hud100to110ParentHolder;
	public GameObject hud100to110FrontBaclHolder;

	public float WaitForMin;
	public float WaitForMid;
	public float WaitForMax;
	public GameObject enemyObjHolder;
	public GameObject enemyDiactivatHolder;
	public Transform enemySpawnHereTransHolder;

	private float waitFor;
	private int hurts = 0;
	private int kills = 0;
	private int moves = 0;
	private float time = 0;
	private double avg = 0;
//	private EnemyMover_Common[] moversArr;

	private bool the100to110;
	private float moves100 = 0;
	private float time100 = 0;
	private double avg100 = 0;

	void OnEnable () {
	}

	void OnDisable () {
	}

	void SetupNumbers () {
		textHurts.text = "0";
		textKills.text = "0";
		textMoves.text = "0";
		textTime.text = "0";
		textAvg.text = "0";

		textMoves100.text = "0";
		textTime100.text = "0";
		textAvg100.text = "0";
	}

	// Use this for initialization
	void Start () {
		// 100 to 110
		hud100to110ParentHolder.SetActive (false);
		hud100to110FrontBaclHolder.SetActive (false);
		the100to110 = false;

		enemyNumber = 0;
//		moversArr = GetComponentsInChildren<EnemyMover_Common> ();
//		Invoke ("StartSpawning", 1.5F);
		SetupNumbers ();
		enemyDiactivatHolder.SetActive (false);
	}

	void StartSpawning () {
		StartCoroutine (SpawnCoroutine ());
	}

	IEnumerator SpawnCoroutine () {
		while (EventTrigger.isPaused || EventTrigger.isFrozen) {
			yield return null;
		}
		GameObject newEnemy = Instantiate (enemyObjHolder);
		newEnemy.transform.SetParent (enemySpawnHereTransHolder);
		newEnemy.GetComponent<EnemyMover_Common> ().myLineOverride = (Enemy.LaneTypes)Random.Range (0, 4);
		StartCoroutine (newEnemy.GetComponent<EnemyMover_Common> ().ActiveDelay ());
		enemyNumber++;

		if (enemyNumber < 5) {
		} else {
			enemyNumber = 0;
			minLength += Random.Range (0, 2);
			maxLength += Random.Range (1, 2);
		}
		if (1 < Random.Range (0F, 2F)) {
			waitFor = Random.Range (WaitForMid, WaitForMax);
		} else {
			waitFor = Random.Range (WaitForMin, WaitForMax);
		}

		yield return new WaitForSeconds (waitFor);
		StartCoroutine (SpawnCoroutine ());
	}

	public void UpdateNumber_Hurts () {
		hurts++;
		textHurts.text = hurts.ToString ();
	}

	public void UpdateNumber_Kills () {
		kills++;
		textKills.text = kills.ToString ();
	}

	public void UpdateNumber_Moves () {
		moves++;
		textMoves.text = moves.ToString ();

		if (the100to110) {
			moves100++;
			textMoves100.text = moves100.ToString ();
		}
	}
		
	void Update () {
		// Update Time
		time = Time.realtimeSinceStartup;
		time = (int)time;
		textTime.text = time.ToString ();

		// Update Avg
		avg = moves / time;
		avg = System.Math.Round (avg, 2);
		textAvg.text = avg.ToString ();

		// 100 to 110 sec
		if (time > 99) {
			if (time < 109) {
				if (!hud100to110ParentHolder.activeInHierarchy) {
					hud100to110ParentHolder.SetActive (true);
				}
				the100to110 = true;
				time100 = time - 99;
				textTime100.text = time100.ToString ();
			
				avg100 = moves100 / time100;
				avg100 = System.Math.Round (avg100, 2);
				textAvg100.text = avg100.ToString ();
			} else {
				the100to110 = false;
				hud100to110FrontBaclHolder.SetActive (true);
			}
		}
	}
}
