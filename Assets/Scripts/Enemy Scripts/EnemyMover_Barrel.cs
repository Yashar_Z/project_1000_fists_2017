﻿using UnityEngine;
using System.Collections;

public class EnemyMover_Barrel : MonoBehaviour {

	public Enemy myValuesHolder;

//	public Sprite[] mySpritesArr_Body1;
//	public Sprite[] mySpritesArr_Body2;
//	public Sprite[] mySpritesArr_Body3;
//	public Sprite[] mySpritesArr_Head1;
//	public Sprite[] mySpritesArr_Head2;
//	public Sprite[] mySpritesArr_Head3;
	public GameObject myObjBarrel_NormalHolder;
	public GameObject myObjBarrel_BomberHolder;
	public GameObject myObjBarrel_QuestionHolder;
	public Sprite[] mySpritesArr_HitHead1;
	public Sprite[] mySpritesArr_HitHead2;
	public Sprite[] mySpritesArr_HitHead3;
	public Sprite[] mySpritesAttack;
	public SpriteRenderer spriteRend_Body;
//	public SpriteRenderer spriteRend_Head;
	public SpriteRenderer spriteRend_HitHead_UP;
	public SpriteRenderer spriteRend_HitHead_Down;
	public SpriteRenderer spriteRend_HitHead_Right;
	public SpriteRenderer spriteRend_HitHead_Left;
	public SpriteRenderer spriteRend_Attack_Body;
	public SpriteRenderer spriteRend_Attack_Head;

	public Color mustacheColor1;
	public Color mustacheColor2;
	public Color mustacheColor3;

	public Canvas canvasHP_Holder;

	public float framesPerSec;
	public float RunTimeLength;
	public AnimationClip[] myAnimClipsArr;
	public AnimationClip[] myBarrelAnimClipsArr;
	public Transform myRemoved_BodyTransHolder;
	public Animation myBarrelEnemyBodyAnimHolder;
	public Animation myRemover_AnimHolder;
	public Animation myHPBack_AnimHolder;
	public Animation myHPBackUnPausable_AnimHolder;
//	public Animation myHPBackRedArrows_AnimHolder;

	public Animation myHedgehogItem_AnimHolder;
	public Animator myAnimatorHolder;
	public bool iHaveMyAnimator;
	public Transform[] linesTransArr;
	public ParticleSystem particleDeadBodyTrailHolder;
	public ParticleSystem particleHPAttackBurstHolder;
	public ParticleSystem particleRunDustHolder;
	public ParticleSystem particleDeathPoofHolder;
	public ParticleSystem particleBarrelBarrelPoofHolder;

	public ParticleSystem particleBarrelDeathAllWoodHolder;
	public ParticleSystem particleExplode_AttackHolder;

	public Transform transform_HeadHolder;
	public Transform transform_HeadHitHolder;

	// Line & Body Types
	public enum BodyTypes {B_normal, B_bomber, B_question};
	public Enemy.LaneTypes myLineOverride;
	public Item_Remover_Script itemRemoverScriptHolder;

	// Barrel Q Stuff
	public Transform redArrowWaitLineTransHolder;
	public GameObject enNorm_HPback_ObjHolder;
	public GameObject enNorm_Arrows_ObjHolder;
	public GameObject enRED_HPback_ObjHolder;
	public GameObject enRED_Arrows_ObjHolder;

	// Barrel B Stuff
	public ParticleSystem particleBarrelFromHFDeathPoofHolder;
	public ParticleSystem particleBarrelExplodePoofHolder;
	public ParticleSystem particleBarrelExplodeGroundHolder;
	public ParticleSystem particleBarrelExplodeRingHolder;
	public Transform explosionBarrelParentTransHolder;

	[Header ("Barrel Anim Clips")]
	public AnimationClip[] animClipsBarrelArr;

	// For front paper
	public Sprite[] spriteBarrelTypePaperArr;
	public SpriteRenderer spriteBarrelTypePaperHolder;

	[Header ("Mover Publics")]
	public EnemyHealth enemyHealth_Script_Holder;
	public Animation myAnimLegacyRef;
	public Transform myHPBack_TransRef;

//	private Sprite[] activeSprites_Body;
	private Sprite[] activeSprites_Head;
	private Sprite[] activeSprites_HitHead;
	private Color activeMustacheColor;
	private int currFrame;
	private float pausedAnimTime;
	private float freezeAnimTime;
	private float pausedAnimTime_BarrelEnemyBody;
	private float freezeAnimTime_BarrelEnemyBody;
	private float mySpeed;
	private bool attackBegun;
	private bool iHaveExploded;
	private Enemy.EnemyType myEnemyType;
	[SerializeField]
	private BodyTypes MyType;
	[SerializeField]
	private BodyTypes MyType_Dynamic;
	[SerializeField]
	private Enemy.LaneTypes MyLine;

	// Barrel Stuff
	private int barrelQ_PatternRandInt;
	private char barrelQ_ArrowRedChar;
	private Transform barrelQ_ArrowRedSelectedTransRef;
	private bool barrelHedgehogHasAlreadyCame;

	private bool barrelHasStopped;

	[SerializeField]
	private bool barrelInsideInvincible;

	// Barrel Walk Anim Legacy Times
	private float barrelWalkAnimTime;
	private float barrelHPAnimTime;

	// Use this for initialization
	void Awake () {
//		enemyHealth_Script_Holder = GetComponent<EnemyHealth> ();

		// Start HP Back size and anim stuff
		myHPBack_TransRef.localScale = Vector3.zero;

		transform.position = Vector3.zero;
//		HideEnemySprite_Normal ();
		HideEnemySprite_Attack ();
		enemyHealth_Script_Holder.amInvincible = true;
		barrelInsideInvincible = true;
		particleRunDustHolder.Stop ();
		iHaveExploded = false;

		// For non-spawner, should be here (For EnemyMaker ONLY);
//		GetMyValues_Mover ();
	}

	void GetMyValues_Mover() {
//        myValuesHolder = GetComponent<Enemy>();

        RunTimeLength = myValuesHolder.enemyMoveSpeed;
		myEnemyType = myValuesHolder.enemyType;
		myLineOverride = myValuesHolder.enemyLane;

        SetUpMyMover ();

		// Get Values for Health
		enemyHealth_Script_Holder.GetMyValues_Health();
	}

	void SetUpMyMover () {
		// Initialize Type of Common
		switch (myEnemyType) {
		case Enemy.EnemyType.Barrel1:
			MyType = BodyTypes.B_normal;
			spriteBarrelTypePaperHolder.gameObject.SetActive (false);
			break;
		case Enemy.EnemyType.Barrel2:
			MyType = BodyTypes.B_normal;
			spriteBarrelTypePaperHolder.gameObject.SetActive (false);
			break;
		case Enemy.EnemyType.Barrel_X1:
			MyType = BodyTypes.B_bomber;
			spriteBarrelTypePaperHolder.gameObject.SetActive (true);
			spriteBarrelTypePaperHolder.sprite = spriteBarrelTypePaperArr [0];
			break;
		case Enemy.EnemyType.Barrel_X2:
			MyType = BodyTypes.B_bomber;
			spriteBarrelTypePaperHolder.gameObject.SetActive (true);
			spriteBarrelTypePaperHolder.sprite = spriteBarrelTypePaperArr [0];
			break;
		case Enemy.EnemyType.Barrel_Q1:
			MyType = BodyTypes.B_question;
			spriteBarrelTypePaperHolder.gameObject.SetActive (true);
			spriteBarrelTypePaperHolder.sprite = spriteBarrelTypePaperArr [1];
			break;
		case Enemy.EnemyType.Barrel_Q2:
			MyType = BodyTypes.B_question;
			spriteBarrelTypePaperHolder.gameObject.SetActive (true);
			spriteBarrelTypePaperHolder.sprite = spriteBarrelTypePaperArr [1];
			break;
		}

		// Initialize Body & Head (And set mustache color)
		myObjBarrel_NormalHolder.SetActive (false);
		myObjBarrel_BomberHolder.SetActive (false);
		myObjBarrel_QuestionHolder.SetActive (false);

		switch (MyType) {
		case BodyTypes.B_normal:
			spriteRend_Attack_Body.sprite = mySpritesAttack [0];
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		case BodyTypes.B_bomber:
			spriteRend_Attack_Body.sprite = mySpritesAttack [1];
			activeSprites_HitHead = mySpritesArr_HitHead2;
			activeMustacheColor = mustacheColor2;
			break;
		case BodyTypes.B_question:
			spriteRend_Attack_Body.sprite = mySpritesAttack [0];
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		}

		// Initialize Attack
//		spriteRend_Attack_Body.sprite = activeSprites_Body [4];

		// Initialize Hit Head
		spriteRend_HitHead_UP.sprite = activeSprites_HitHead [0];
		spriteRend_HitHead_Down.sprite = activeSprites_HitHead [1];
		spriteRend_HitHead_Right.sprite = activeSprites_HitHead [2];
		spriteRend_HitHead_Left.sprite = activeSprites_HitHead [3];

		// Removed Head For Attack
		//		spriteRend_Attack_Head.sprite = activeSprites_Head [7];

		// Initialize Anim Legacy (Speed & Component)
//		myAnimLegacyRef = GetComponent<Animation> ();
		mySpeed = 1F / RunTimeLength;

		// Initialize Mustache Color
		enemyHealth_Script_Holder.mustacheColorFinal = activeMustacheColor;

		// Initialize full frames & HP back
		attackBegun = false;
		currFrame = Random.Range (0, 7);

		if (RunTimeLength > 13) {
			// Change clip for slow HP anims to prevent slow hp display
			myHPBack_AnimHolder.clip = myHPBack_AnimHolder ["Enemy Comm HP Line Anim 2 Slow (Legacy)"].clip;

			//			Debug.LogError ("Slow Enemy");
		}

		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].speed = mySpeed;
		//Test Anim
		//		Invoke ("ActivateEnemy", SpawnWait);
		//		Invoke ("ActivateEnemy", 1);

		// Barrel Initialize
		barrelHasStopped = false;
		barrelHedgehogHasAlreadyCame = false;
	}

	void OnDisable () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);
	}

	void InitializeMyLine () {
		// Initialize Line Number
		MyLine = myLineOverride;
		transform.GetChild(0).position = linesTransArr [(int)MyLine].position;

		// Initialize Line Anim Legacy
		myAnimLegacyRef.clip = myAnimClipsArr [(int)MyLine];
		myAnimLegacyRef[myAnimLegacyRef.clip.name].speed = mySpeed;
	}

	public IEnumerator ActiveDelay () {
		GetMyValues_Mover ();
		ActivateEnemy ();

		// Start Listeners
		EventManager.StartListening ("Pause", PauseMe);
		EventManager.StartListening ("Freeze", FreezeMe);
		EventManager.StartListening ("Hurt Enemies", HurtCheck);
		EventManager.StartListening ("Explode N Stuff", ExplodeNStuff);
        yield return null;
    }

	public void ActivateEnemy () {
		if (EventTrigger.isPaused || EventTrigger.isFrozen) {
			Invoke ("ActivateEnemy", 0.2F);
		} else {
			InitializeMyLine ();
//			ShowEnemySprite_Normal ();
			enemyHealth_Script_Holder.amInvincible = false;
			barrelInsideInvincible = true;

			// Say if it is enemy (human) or item (pickup)
			enemyHealth_Script_Holder.amItemPickup = false;

			// Start Full Frame Sprites Anim
//			StartCoroutine (PlayNextFrame ());

			// Start Animation Legacy
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();
			// Activate object AND canvas of HP
			ActivateHPcanvas ();

			// Get World Number for anim. NEEDS DELETE and FIX with new world record rather than FIND
//			worldNumForMe = FindObjectOfType<EnemySpawner>().worldNumber;

			// Start Animator OLD
//			myAnimatorHolder.SetInteger ("World Number", worldNumForMe);
//			myAnimatorHolder.SetFloat ("My Run Speed", mySpeed);
//			myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
//			myAnimatorHolder.SetTrigger ("Run Trigger");
//			myAnimatorHolder.Play ("Start Run Fade");

			// Start Barrel Idle Loop Legacy Anim
			myBarrelEnemyBodyAnimHolder.clip = myBarrelAnimClipsArr[0];
			myBarrelEnemyBodyAnimHolder.Play ();

			// Start Animator New
			if (iHaveMyAnimator) {
				myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
				myAnimatorHolder.Play ("Enemy Fade-In Anim 1");
			}

			// Start run smoke particle
			particleRunDustHolder.Play();

			// HP Black holder Anim Resize
//			myHPBackUnPausable_AnimHolder.clip = myAnimClipsArr [3];
//			myHPBackUnPausable_AnimHolder.Play ();

//			myHPBackUnPausable_AnimHolder.Stop ();

			// HP Black holder Anim Resize (Don't need because of slow enemies)
//			myHPBackUnPausable_AnimHolder.Play ("Enemy HP Barrel Disappear Anim 1 (Legacy)");

			// HP Black Size Controller
			myHPBackUnPausable_AnimHolder.transform.localScale = Vector3.zero;

			// Barrel Hedgehog Prepare & Disable 
			itemRemoverScriptHolder.gameObject.SetActive (false);

			// The One letter for hedgehogs
			if (MyType == BodyTypes.B_question) {
				BarrelQ_Hedgehog_ShowRedLetter ();
			}

			// Select Head Type
			Barrel_SelectHeadType ();
		}
	}

	void ActivateHPcanvas () {
		canvasHP_Holder.gameObject.SetActive (true);
		canvasHP_Holder.enabled = true;

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			canvasHP_Holder.sortingOrder = 7;
		}
	}

	public void BarrelQ_PatternSelect () {
		if (barrelHedgehogHasAlreadyCame) {
			barrelQ_PatternRandInt = Random.Range (0, 2);
			switch (barrelQ_PatternRandInt) {
			case 0:
				MyType_Dynamic = BodyTypes.B_normal;
				break;
			case 1:
				MyType_Dynamic = BodyTypes.B_bomber;
				break;
			default:
				MyType_Dynamic = BodyTypes.B_bomber;
				break;
			}
		} else {
			barrelQ_PatternRandInt = Random.Range (0, 3);
			switch (barrelQ_PatternRandInt) {
			case 0:
				MyType_Dynamic = BodyTypes.B_normal;
				break;
			case 1:
				MyType_Dynamic = BodyTypes.B_bomber;
				break;
			case 2:
				MyType_Dynamic = BodyTypes.B_question;
				barrelHedgehogHasAlreadyCame = true;
				break;
			default:
				MyType_Dynamic = BodyTypes.B_normal;
				break;
			}
		}
		Barrel_SelectHeadType_OnlyQ ();
	}

	public void Barrel_SelectHeadType () {
		switch (MyType) {
		case BodyTypes.B_normal:
			myObjBarrel_NormalHolder.SetActive (true);
			myObjBarrel_BomberHolder.SetActive (false);
			myObjBarrel_QuestionHolder.SetActive (false);

			break;
		case BodyTypes.B_bomber:
			myObjBarrel_NormalHolder.SetActive (false);
			myObjBarrel_BomberHolder.SetActive (true);
			myObjBarrel_QuestionHolder.SetActive (false);

			break;
		case BodyTypes.B_question:

			myObjBarrel_NormalHolder.SetActive (false);
			myObjBarrel_BomberHolder.SetActive (false);
			myObjBarrel_QuestionHolder.SetActive (false);

			break;
		}
	}

	public void Barrel_SelectHeadType_OnlyQ () {
		switch (MyType_Dynamic) {
		case BodyTypes.B_normal:
			myObjBarrel_NormalHolder.SetActive (true);
			myObjBarrel_BomberHolder.SetActive (false);
			myObjBarrel_QuestionHolder.SetActive (false);

			break;
		case BodyTypes.B_bomber:
			myObjBarrel_NormalHolder.SetActive (false);
			myObjBarrel_BomberHolder.SetActive (true);
			myObjBarrel_QuestionHolder.SetActive (false);

			break;
		case BodyTypes.B_question:

			myObjBarrel_NormalHolder.SetActive (false);
			myObjBarrel_BomberHolder.SetActive (false);
			myObjBarrel_QuestionHolder.SetActive (true);

			break;
		}
	}

	public void BarrelQ_Hedgehog_ShowRedLetter () {
		if (Random.Range (0, 2) == 0) {
			barrelQ_ArrowRedSelectedTransRef = redArrowWaitLineTransHolder.GetChild (Random.Range (0, 4)).GetComponent<Transform> ();
		} else {
			barrelQ_ArrowRedSelectedTransRef = redArrowWaitLineTransHolder.GetChild (Random.Range (6, 10)).GetComponent<Transform> ();
		}

		barrelQ_ArrowRedSelectedTransRef.gameObject.SetActive (true);
		barrelQ_ArrowRedChar = barrelQ_ArrowRedSelectedTransRef.name[7];
//		Debug.LogError ("YO: " + barrelQ_ArrowRedChar);
	}

	public void MoveHead_Check (int whatTime) {
		for (int i = 0; i < whatTime.ToString().Length; i++) {
			if (PlayerData_InGame.Instance.P1_Barrel_HeadIsUpCount.ToString ()[0] == whatTime.ToString () [i]) {
				if (MyType == BodyTypes.B_question) {
					BarrelQ_PatternSelect ();
				}
				MoveHead_HeadGoes_Up ();
			}
		}
	}

	public void MoveHead_HeadGoes_Up () {
		barrelHasStopped = true;

		// Legacy Anims Pause
		barrelHPAnimTime = myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time;
		barrelWalkAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
		myHPBack_AnimHolder.Stop ();
		myAnimLegacyRef.Stop ();

		// Barrel Head goes up anim
		myBarrelEnemyBodyAnimHolder.Stop ();
		myBarrelEnemyBodyAnimHolder.clip = myBarrelAnimClipsArr [1];
		myBarrelEnemyBodyAnimHolder.Play ();

		myHPBackUnPausable_AnimHolder.Stop ();
		if (MyType == BodyTypes.B_question) {
			
//			Debug.LogError ("Barrel Dynamic Type: " + MyType_Dynamic);

			if (MyType_Dynamic == BodyTypes.B_question) {
				enNorm_HPback_ObjHolder.SetActive (false);
				enNorm_Arrows_ObjHolder.SetActive (false);
				enRED_HPback_ObjHolder.SetActive (true);
				enRED_Arrows_ObjHolder.SetActive (true);
			} 
			else {
				enNorm_HPback_ObjHolder.SetActive (true);
				enNorm_Arrows_ObjHolder.SetActive (true);
				enRED_HPback_ObjHolder.SetActive (false);
				enRED_Arrows_ObjHolder.SetActive (false);
			}
		} 			
		else {
			enNorm_HPback_ObjHolder.SetActive (true);
			enNorm_Arrows_ObjHolder.SetActive (true);
			enRED_HPback_ObjHolder.SetActive (false);
			enRED_Arrows_ObjHolder.SetActive (false);
		}

//		myHPBackUnPausable_AnimHolder.clip = myAnimClipsArr [2];
//		myHPBackUnPausable_AnimHolder.Play ();
		myHPBackUnPausable_AnimHolder.Play ("Enemy HP Barrel ReAppear Anim 1 (Legacy)");
	}

	public void MoveHead_HeadGoes_InvinciblityBool (bool whatBool) {
		// No longer invincible, can be hit
		barrelInsideInvincible = whatBool;
	}

	public void MoveHead_HeadGoes_Wait () {
		myBarrelEnemyBodyAnimHolder.Stop ();
		myBarrelEnemyBodyAnimHolder.clip = myBarrelAnimClipsArr [2];

		// Make him NOT invincible
		MoveHead_HeadGoes_InvinciblityBool (false);

		switch (MyType) {
		case BodyTypes.B_normal:
			myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].speed = 1 / PlayerData_InGame.Instance.P1_Barrel_N_HeadUpTime;
			break;
		case BodyTypes.B_bomber:
			myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].speed = 1 / PlayerData_InGame.Instance.P1_Barrel_X_HeadUpTime;
			break;
		case BodyTypes.B_question:
			myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].speed = 1 / PlayerData_InGame.Instance.P1_Barrel_Q_HeadUpTime;
			break;
		}

//		Debug.LogWarning ("Barrel name: " + myBarrelEnemyBodyAnimHolder.clip.name + " And barrel speed: " 
//			+ myBarrelEnemyBodyAnimHolder[myBarrelEnemyBodyAnimHolder.clip.name].speed);
		myBarrelEnemyBodyAnimHolder.Play ();
	}

	public void MoveHead_HeadGoes_Down () {
		// Barrel Head goes down anim
		myBarrelEnemyBodyAnimHolder.clip = myBarrelAnimClipsArr [3];
		myBarrelEnemyBodyAnimHolder.Play ();

		// Back to invincible
		MoveHead_HeadGoes_InvinciblityBool (true);
//		enemyHealth_Script_Holder.amInvincible = true;

//		myHPBackUnPausable_AnimHolder.clip = myAnimClipsArr [3];
//		myHPBackUnPausable_AnimHolder.Play ();
		myHPBackUnPausable_AnimHolder.Play ("Enemy HP Barrel Disappear Anim 1 (Legacy)");

//		myHPBackUnPausable_AnimHolder.Stop ();

	}

	public void MoveHead_HeadGone_Continue () {
		myBarrelEnemyBodyAnimHolder.Stop ();
		barrelHasStopped = false;

		// Legacy Anims Unpause
		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = barrelHPAnimTime;
		myAnimLegacyRef [myAnimLegacyRef.clip.name].time = barrelWalkAnimTime;
		myHPBack_AnimHolder.Play ();
		myAnimLegacyRef.Play ();

		myBarrelEnemyBodyAnimHolder.clip = myBarrelAnimClipsArr [0];
//		Debug.LogError ("Arr Anim: " + myBarrelAnimClipsArr [0].name + " BARREL Anim: " + myBarrelEnemyBodyAnimHolder.clip.name);
		myBarrelEnemyBodyAnimHolder.Play ();
	}

	public void Barrel_Hedgehog_Shoot () {
		itemRemoverScriptHolder.gameObject.SetActive (true);
		itemRemoverScriptHolder.transform.SetParent (this.transform.root);
		myHedgehogItem_AnimHolder.Play ("Enemy Item Removed FLOAT STRAIGHT Barrel Anim 1 (Legacy)");
		itemRemoverScriptHolder.ShootItemPickUp_ForBarrelQ ();
	}

	IEnumerator PlayNextFrame () {
		if (!attackBegun) {
//			spriteRend_Body.sprite = activeSprites_Body [currFrame];
//			spriteRend_Head.sprite = activeSprites_Head [currFrame];
		}
		yield return new WaitForSeconds (framesPerSec);
		currFrame++;

		while (EventTrigger.isPaused || EventTrigger.isFrozen) {
			yield return null;
		}
		if (!attackBegun) {
			StartCoroutine (PlayNextFrame ());
		}
	}

	public void EnemyAttack () {
//		Debug.LogError ("Attack!");

		if (!enemyHealth_Script_Holder.amDead) {
			// The barrel wood explode particle stuff
			BarrelWoodPoofParticle ();

			// Make Enemy Invincible
			enemyHealth_Script_Holder.amInvincible = true;
			barrelInsideInvincible = true;

			// Remove Listener
//			EventManager.StopListening ("Pause", PauseMe);
			EventManager.StopListening ("Freeze", FreezeMe);

			EventManager.StopListening ("Hurt Enemies", HurtCheck);
			EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

			// Barrel All: Wood pieces particle
			particleBarrelDeathAllWoodHolder.Play ();

			// Attack Anims
			myAnimLegacyRef.clip = myAnimClipsArr [4];
			myAnimLegacyRef.Play ();

			if (iHaveMyAnimator) {
				myAnimatorHolder.Play ("Enemy Comm Turn-Dark Anim 1");
			}

			attackBegun = true;
			HideEnemySprite_Normal ();
			ShowEnemySprite_Attack ();

			// Play the HP Back Anim Legacy
//			myHPBackUnPausable_AnimHolder.clip = myAnimClipsArr [1];
//			myHPBackUnPausable_AnimHolder.Play ();
			myHPBackUnPausable_AnimHolder.Play ("Enemy HP Back Attack Anim 1 (Legacy)");

//			particleHPAttackBurstHolder.transform.SetParent (particleHPAttackBurstHolder.transform.parent.parent);
//			myHPBackUnPausable_AnimHolder.gameObject.SetActive (false);
//			particleHPAttackBurstHolder.Play ();
		}

//		Trigger Attack (Don't want to use event)
//		EventManager.TriggerEvent ("Hurt Player");

	}
		
	public void BarrelWoodPoofParticle () {
		particleBarrelBarrelPoofHolder.Play ();
		particleBarrelBarrelPoofHolder.transform.SetParent (this.transform.root);
		Destroy (particleBarrelBarrelPoofHolder.gameObject, 1.5F);
	}
		
	public void DestroyMe_AfterAttack () {
		this.transform.SetParent (transform.root);

		// (This check is for player HURT by enemy)
		enemyHealth_Script_Holder.HurtLastCheck ();

		// Check player "Shileded" chance
		// Player's shield chance was higher
		if (PlayerData_InGame.Instance.p1_Invincible
			|| (PlayerData_InGame.Instance.p1_ShieldChanceTotal >= Random.Range (1, 100))) {

			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.Shielded_HurtPlayer ();
		}
		// Player's shield chance was lower
		else {
			// Use enemy health script to hurt the player
			if (MyType == BodyTypes.B_bomber) {
				enemyHealth_Script_Holder.HurtExplodePlayer ();
			} else {
				if (MyType == BodyTypes.B_question) {
					if (MyType_Dynamic == BodyTypes.B_question) {
					} else {
						enemyHealth_Script_Holder.HurtExplodePlayer ();
					}
				} else {
					enemyHealth_Script_Holder.HurtPlayer ();
				}
			}

			// Player was hit AND hurt
//			if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
//				PlayerData_InGame.Instance.stats_Stars_Untouchable = false;
//			}
		}

		if (MyType == BodyTypes.B_bomber) {
			// To add explosion effect to the front of camera
			particleExplode_AttackHolder.Play ();
			particleExplode_AttackHolder.transform.SetParent (transform.root);
			Destroy (particleExplode_AttackHolder.gameObject, 2);
		} else {
			if (MyType == BodyTypes.B_question) {
				if (MyType_Dynamic == BodyTypes.B_question) {

					// If explosion from hitting player clears out the clingy HFs
					if (PlayerData_InGame.Instance.P1_Help_HFX_CanKillClingy && PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool) {
						PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_KillAll ();
						Debug.LogWarning ("KILL ALL!");
					}

					// To add explosion effect to the front of camera
					particleExplode_AttackHolder.Play ();
					particleExplode_AttackHolder.transform.SetParent (transform.root);
					Destroy (particleExplode_AttackHolder.gameObject, 2);
				}
			}
		}

		DestroyMe_Now ();
	}

	public void DestroyMe_RemoveMe () {
		// Barrel All: Wood pieces particle
		particleBarrelDeathAllWoodHolder.Play ();

		switch (EventTrigger.theKey_Direction) {
		case "U":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-7, 7));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case "D":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (173, 187));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case "R":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case "L":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		}

		switch (MyType) {
		case BodyTypes.B_normal:
			// The barrel wood explode particle stuff
			BarrelWoodPoofParticle ();
			particleDeathPoofHolder.Play ();
			break;
		case BodyTypes.B_bomber:
			BarrelBomber_Particles ();
			ExplodeMe_Now ();
			break;
		case BodyTypes.B_question:
			// The barrel wood explode particle stuff
			if (MyType_Dynamic == BodyTypes.B_bomber) {
				BarrelBomber_Particles ();
				ExplodeMe_Now ();
			} else {
				BarrelWoodPoofParticle ();
				particleDeathPoofHolder.Play ();
			}
			break;
		default:
			break;
		}

		// Play Death Particle
//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.Play();

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.2F);

		// Quest stuff for: barrel POW UP death 
		StartCoroutine (Quest_KillBarrel_Normal());
	}

	public void ExplodeMe_Now () {
		iHaveExploded = true;
		EventManager.TriggerEvent ("Explode N Stuff");
		PlayerData_InGame.Instance.OverallExplosion ();
	}

	// Death by Power Up 1 Direction for Body
	public void DestroyMe_RemoveMe_PowUp (int aNumber) {
		switch (aNumber) {
		case 0:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case 1:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case 2:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		case 3:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Barrel Removed UpSide Anim 1 (Legacy)");
			break;
		}
		// Play Death Particle
		//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
		//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.Play();

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.2F);

		// Quest stuff for: barrel POW UP death 
		StartCoroutine (Quest_KillBarrel_PowUp());
	}

	public IEnumerator DestroyMe_Routine () {
		// Avoid head sudden appear bug / frame (Now via bodyremover Anim)
//		spriteRend_Head.gameObject.SetActive (false);

		yield return new WaitForSeconds (1.2F);
		DestroyMe_Now ();
	}
		
	IEnumerator Quest_KillBarrel_Normal () {
		yield return null;
		if (QuestManager.Instance != null) {
			
			StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Any ());
			Quest_KillByExplosion ();

			switch (myEnemyType) {
			case Enemy.EnemyType.Barrel1:
			case Enemy.EnemyType.Barrel2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Barrel ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Barrel ());

				break;
			case Enemy.EnemyType.Barrel_X1:
			case Enemy.EnemyType.Barrel_X2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_BarrelX ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_BarrelX ());

				break;
			case Enemy.EnemyType.Barrel_Q1:
			case Enemy.EnemyType.Barrel_Q2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_BarrelQ ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_BarrelQ ());

				break;
			default:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Barrel ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Barrel ());

				break;
			}
		}
	}

	IEnumerator Quest_KillBarrel_PowUp () {
		yield return null;
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_PowUp1 ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_PowUp1 ());
		}
		StartCoroutine (Quest_KillBarrel_Normal());
	}

	void Quest_KillByExplosion () {
		if (enemyHealth_Script_Holder.wasHurtByExplosion) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_Explosion ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_Explosion ());
		}
	}

	public void DestroyMe_Now () {
		this.enabled = false;
//		GetComponent<EnemyMover_Common> ().enabled = false;
		Destroy (this.gameObject);
	}

	public void BarrelBomber_Particles () {
		particleBarrelFromHFDeathPoofHolder.Play ();
		particleBarrelExplodePoofHolder.Play ();
		particleBarrelExplodeGroundHolder.Play ();
		particleBarrelExplodeRingHolder.Play ();
		explosionBarrelParentTransHolder.SetParent (transform.root);
		Destroy (explosionBarrelParentTransHolder.gameObject, 3);
	}

	void HurtCheck () {
		// Is barrel invincible
		if (barrelInsideInvincible) {

			// Is barrel invincible BUT power up 1 overrides it
			if (PlayerData_InGame.Instance.p1_PowUp1_Active) {
				enemyHealth_Script_Holder.CompareMyArrow ();
			}
		} 

		// Is NOT barrel invincible
		else {
			
//			Debug.Log ("CHECK HURT! 1");
			if (MyType == BodyTypes.B_question) {
				
//				Debug.Log ("CHECK HURT! 2");
				if (MyType_Dynamic == BodyTypes.B_question) {
					if (EventTrigger.theKey_Shape1 [0] == barrelQ_ArrowRedChar ||
					    EventTrigger.theKey_Shape2 [0] == barrelQ_ArrowRedChar) {

						// Moment that player hits hedgehog barrel
						Barrel_Hedgehog_Shoot ();
						enemyHealth_Script_Holder.HurtMe (500, false);

						// Animate HP Back
						myHPBackUnPausable_AnimHolder.Play ("Enemy HP Back Attack Anim 1 (Legacy)");

						// Disable HP Later to avoid reappearing
						StartCoroutine (BarrelHPDisable());
					}
				} else {
					enemyHealth_Script_Holder.CompareMyArrow ();
				}
			} else {
				enemyHealth_Script_Holder.CompareMyArrow ();
			}
		}
	}

	IEnumerator BarrelHPDisable () {
		yield return new WaitForSeconds (0.6F);
		canvasHP_Holder.gameObject.SetActive (false);
	}

	void ExplodeNStuff () {
//		Debug.LogError ("My Type = " + MyType);

		if (!iHaveExploded) {
			if (!barrelInsideInvincible) {
				enemyHealth_Script_Holder.HurtMe (PlayerData_InGame.Instance.P1_HF_ExplosionDamage, false);
				enemyHealth_Script_Holder.wasHurtByExplosion = true;
			}
		}
	}

	void PauseMe () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {

				// Different times for based on wether or not barrel head is up 
				if (barrelHasStopped) {
					pausedAnimTime = barrelWalkAnimTime;
				} else {
					pausedAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
				}

				pausedAnimTime_BarrelEnemyBody = myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].time;

			}
			// Stop Legacy Anims & Animator
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myBarrelEnemyBodyAnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
//			particleRunDustHolder.Pause();
		}
		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {

			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = pausedAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = pausedAnimTime;
			myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].time = pausedAnimTime_BarrelEnemyBody;

			// Resume Play of Legacy Anims & Animator

			if (!barrelHasStopped) {
				myAnimLegacyRef.Play ();
				myHPBack_AnimHolder.Play ();
			}

			myBarrelEnemyBodyAnimHolder.Play ();
			myAnimatorHolder.speed = 1;
		}
	}

	void FreezeMe () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {

				// Different times for based on wether or not barrel head is up 
				if (barrelHasStopped) {
					freezeAnimTime = barrelWalkAnimTime;
				} else {
					freezeAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
				}
					
				freezeAnimTime_BarrelEnemyBody = myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].time;
			}
			// Stop Legacy Anims
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myBarrelEnemyBodyAnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
//			particleRunDustHolder.Pause();
		}
		if (EventTrigger.isFrozen && !EventTrigger.isPaused) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = freezeAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = freezeAnimTime;
			myBarrelEnemyBodyAnimHolder [myBarrelEnemyBodyAnimHolder.clip.name].time = freezeAnimTime_BarrelEnemyBody;

			// Resume Play of Legacy Anims

			if (!barrelHasStopped) {
				myAnimLegacyRef.Play ();
				myHPBack_AnimHolder.Play ();
			}
			myBarrelEnemyBodyAnimHolder.Play ();
			myAnimatorHolder.speed = 1;
		}
	}

	void HideEnemySprite_Normal () {
		spriteRend_Body.enabled = false;
//		spriteRend_Head.enabled = false;
	}

	void ShowEnemySprite_Normal () {
		spriteRend_Body.enabled = true;
//		spriteRend_Head.enabled = true;
	}

	void ShowEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = true;
		spriteRend_Attack_Head.enabled = true;
	}

	void HideEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = false;
		spriteRend_Attack_Head.enabled = false;
	}

	public void EnemyMover_Die () {
	}
}
