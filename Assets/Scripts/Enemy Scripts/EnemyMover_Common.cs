﻿using UnityEngine;
using System.Collections;

public class EnemyMover_Common : MonoBehaviour {

	public Enemy myValuesHolder;

	public Sprite[] mySpritesArr_Body1;
	public Sprite[] mySpritesArr_Body2;
	public Sprite[] mySpritesArr_Head1;
	public Sprite[] mySpritesArr_Head2;
	public Sprite[] mySpritesArr_HitHead1;
	public Sprite[] mySpritesArr_HitHead2;
	public SpriteRenderer spriteRend_Body;
	public SpriteRenderer spriteRend_Head;
	public SpriteRenderer spriteRend_HitHead_UP;
	public SpriteRenderer spriteRend_HitHead_Down;
	public SpriteRenderer spriteRend_HitHead_Right;
	public SpriteRenderer spriteRend_HitHead_Left;
	public SpriteRenderer spriteRend_Attack_Body;
	public SpriteRenderer spriteRend_Attack_Head;

	public Color mustacheColor1;
	public Color mustacheColor2;

	public Canvas canvasHP_Holder;

	public float framesPerSec;
	public float RunTimeLength;
	public AnimationClip[] myAnimClipsArr;
	public Transform myRemoved_BodyTransHolder;
	public Animation myRemover_AnimHolder;
	public Animation myHPBack_AnimHolder;
	public Animation myHPBackUnPausable_AnimHolder;
	public Animator myAnimatorHolder;
	public Transform[] linesTransArr;
	public ParticleSystem particleDeadBodyTrailHolder;
//	public ParticleSystem particleHPAttackBurstHolder;
	public ParticleSystem particleRunDustHolder;
	public ParticleSystem particleDeathPoofHolder;

	public Transform transform_HeadHolder;
	public Transform transform_HeadHitHolder;

	// Line & Body Types
	public enum BodyTypes {B1_H1, B1_H2, B2_H1, B2_H2};
	public Enemy.LaneTypes myLineOverride;

	[Header ("Mover Publics")]
	public EnemyHealth enemyHealth_Script_Holder;
	public Animation myAnimLegacyRef;
	public Transform myHPBack_TransRef;

	[Header ("Second Costumes")]
	public GameObject obj2ndCostumeObj;
	public ParticleSystem particle2ndCostPoof;
	public ParticleSystem particle2ndCostHurt;
	public int int2ndCostumeCounter;

	public bool isFats2ndCostumeAlbino;
	public bool isGiant2ndCostumeGinger;
	public bool isThornsOn;

	private delegate IEnumerator CoroutineDelegate();
	private CoroutineDelegate coroutineDeleg2ndCostume;

	[Header ("")]
	private Sprite[] activeSprites_Body;
	private Sprite[] activeSprites_Head;
	private Sprite[] activeSprites_HitHead;
	private Color activeMustacheColor;
	private int currFrame;
	private int whichClothes;
	private float pausedAnimTime;
	private float freezeAnimTime;
	private float mySpeed;
	private bool attackBegun;
	private Enemy.EnemyType myEnemyType;
	[SerializeField]
	private BodyTypes MyType;
	[SerializeField]
	private Enemy.LaneTypes MyLine;

	// Headspinner stuff
	private bool isHeadSpinner;
	private	Vector3 transformPois_HeadOrigV3;
	private Vector3 transformPois_HeadHitOrigV3;
	private Vector3 newV3Buffer_Head;
	private Vector3 newV3Buffer_HeadHit;

	// Use this for initialization
	void Awake () {
//		enemyHealth_Script_Holder = GetComponent<EnemyHealth> ();

		// Start HP Back size and anim stuff
//		myHPBack_TransRef = myHPBack_AnimHolder.GetComponent<Transform> ();
		myHPBack_TransRef.localScale = Vector3.zero;

		transform.position = Vector3.zero;
		HideEnemySprite_Normal ();
		HideEnemySprite_Attack ();
		enemyHealth_Script_Holder.amInvincible = true;
		particleRunDustHolder.Stop ();

		// For non-spawner, should be here (For EnemyMaker ONLY);
//		GetMyValues_Mover ();
	}

	void GetMyValues_Mover() {
//        myValuesHolder = GetComponent<Enemy>();

        RunTimeLength = myValuesHolder.enemyMoveSpeed;
		myEnemyType = myValuesHolder.enemyType;
		myLineOverride = myValuesHolder.enemyLane;

        SetUpMyMover ();

		// Get Values for Health
		enemyHealth_Script_Holder.GetMyValues_Health();
	}

	void SetUpMyMover () {
		// Initialize Type of Common
		switch (myEnemyType) {
		case Enemy.EnemyType.Fat1:
			MyType = BodyTypes.B1_H1;
			break;
		case Enemy.EnemyType.Thin1:
			MyType = BodyTypes.B1_H1;
			break;
		case Enemy.EnemyType.Muscle1:
			MyType = BodyTypes.B1_H1;
			break;
		case Enemy.EnemyType.Giant1:
			MyType = BodyTypes.B1_H1;
			break;
		case Enemy.EnemyType.Fat2:
			MyType = BodyTypes.B2_H2;

			// Shound NOT be reverse because it repeates it EVERY frame
//			coroutineDeleg2ndCostume = Arrows2ndCostume_Reverse;

			enemyHealth_Script_Holder.am2ndCostume = true;
			coroutineDeleg2ndCostume = Arrows2ndCostume_Nothing;

			enemyHealth_Script_Holder.amAlbinoReverse = true;

			StartCoroutine (Arrows2ndCostume_Reverse ());
			break;
		case Enemy.EnemyType.Thin2:
			MyType = BodyTypes.B2_H2;

			enemyHealth_Script_Holder.am2ndCostume = true;
			coroutineDeleg2ndCostume = Arrows2ndCostume_Bandage;
//			StartCoroutine (Arrows2ndCostume_Bandage ());
			break;
		case Enemy.EnemyType.Muscle2:
			MyType = BodyTypes.B2_H2;

			enemyHealth_Script_Holder.am2ndCostume = true;
			coroutineDeleg2ndCostume = Arrows2ndCostume_Question;
//			StartCoroutine (Arrows2ndCostume_Question ());
			break;
		case Enemy.EnemyType.Giant2:
			MyType = BodyTypes.B2_H2;

			enemyHealth_Script_Holder.am2ndCostume = true;
			enemyHealth_Script_Holder.amThorns = true;

			coroutineDeleg2ndCostume = Arrows2ndCostume_Thorns;
			StartCoroutine (Arrows2ndCostume_Thorns ());

			// Unlike Albino, doesn't use the bool in EnemyHealth
			isGiant2ndCostumeGinger = true;
			isThornsOn = false;
			break;
		}

		// Initialize Body & Head (And set mustache color)
		switch (MyType) {
		case BodyTypes.B1_H1:
			activeSprites_Body = mySpritesArr_Body1;
			activeSprites_Head = mySpritesArr_Head1;
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		case BodyTypes.B1_H2:
			activeSprites_Body = mySpritesArr_Body1;
			activeSprites_Head = mySpritesArr_Head2;
			activeSprites_HitHead = mySpritesArr_HitHead2;
			activeMustacheColor = mustacheColor2;
			break;
		case BodyTypes.B2_H1:
			activeSprites_Body = mySpritesArr_Body2;
			activeSprites_Head = mySpritesArr_Head1;
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		case BodyTypes.B2_H2:
			activeSprites_Body = mySpritesArr_Body2;
			activeSprites_Head = mySpritesArr_Head2;
			activeSprites_HitHead = mySpritesArr_HitHead2;
			activeMustacheColor = mustacheColor2;
			break;
		}

		// Initialize Attack
		spriteRend_Attack_Body.sprite = activeSprites_Body [7];

		// Initialize Hit Head
		spriteRend_HitHead_UP.sprite = activeSprites_HitHead [0];
		spriteRend_HitHead_Down.sprite = activeSprites_HitHead [1];
		spriteRend_HitHead_Right.sprite = activeSprites_HitHead [2];
		spriteRend_HitHead_Left.sprite = activeSprites_HitHead [3];

		// Removed Head For Attack
		//		spriteRend_Attack_Head.sprite = activeSprites_Head [7];

		// Initialize Anim Legacy (Speed & Component)
//		myAnimLegacyRef = GetComponent<Animation> ();
		mySpeed = 1F / RunTimeLength;

		// Initialize Mustache Color
		enemyHealth_Script_Holder.mustacheColorFinal = activeMustacheColor;

		// Initialize full frames & HP back
		attackBegun = false;
		currFrame = Random.Range (0, 7);

		if (RunTimeLength > 13) {
			// Change clip for slow HP anims to prevent slow hp display
			myHPBack_AnimHolder.clip = myHPBack_AnimHolder ["Enemy Comm HP Line Anim 2 Slow (Legacy)"].clip;

//			Debug.LogError ("Slow Enemy");
		}

		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].speed = mySpeed;
//		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = 0;

//		myHPBack_AnimHolder ["Enemy Comm HP Line Anim 2 Slow (Legacy)"].speed = mySpeed;
//		myHPBack_AnimHolder ["Enemy Comm HP Line Anim 2 Slow (Legacy)"].time = 0;

		//Test Anim
		//		Invoke ("ActivateEnemy", SpawnWait);
		//		Invoke ("ActivateEnemy", 1);

		// Check for head spinner
		if (myEnemyType == Enemy.EnemyType.Thin1 
			|| myEnemyType == Enemy.EnemyType.Thin2
			|| myEnemyType == Enemy.EnemyType.Muscle1
			|| myEnemyType == Enemy.EnemyType.Muscle2
			|| myEnemyType == Enemy.EnemyType.Giant1
			|| myEnemyType == Enemy.EnemyType.Giant2
		) {
			isHeadSpinner = true;
		} else {
			isHeadSpinner = false;
		}

		// Headspinner head position store
		if (isHeadSpinner) {
			transformPois_HeadOrigV3 = transform_HeadHolder.localPosition;
			transformPois_HeadHitOrigV3 = transform_HeadHitHolder.localPosition;
//		transformPois_HeadOrigV3.y += 0.0F;
//		transformPois_HeadHitOrigV3.y += 0.0F;
			transform_HeadHolder.localPosition = transformPois_HeadOrigV3;
			transform_HeadHitHolder.localPosition = transformPois_HeadHitOrigV3;
		}
	}

	void OnDisable () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);
	}

	void InitializeMyLine () {
		// Initialize Line Number
		MyLine = myLineOverride;
		transform.GetChild(0).position = linesTransArr [(int)MyLine].position;

		// Initialize Line Anim Legacy
		myAnimLegacyRef.clip = myAnimClipsArr [(int)MyLine];
		myAnimLegacyRef[myAnimLegacyRef.clip.name].speed = mySpeed;
		myAnimLegacyRef[myAnimLegacyRef.clip.name].time = 0;
	}

	public IEnumerator ActiveDelay () {
		GetMyValues_Mover ();
		ActivateEnemy ();

		// Start Listeners
		EventManager.StartListening ("Pause", PauseMe);
		EventManager.StartListening ("Freeze", FreezeMe);
		EventManager.StartListening ("Hurt Enemies", HurtCheck);
		EventManager.StartListening ("Explode N Stuff", ExplodeNStuff);

        yield return null;
    }
		
	public void ActivateEnemy () {
		if (EventTrigger.isPaused || EventTrigger.isFrozen) {
			Invoke ("ActivateEnemy", 0.2F);
		} else {
			InitializeMyLine ();
			ShowEnemySprite_Normal ();
			enemyHealth_Script_Holder.amInvincible = false;

			// Say if it is enemy (human) or item (pickup)
			enemyHealth_Script_Holder.amItemPickup = false;

			// Start Full Frame Sprites Anim
			StartCoroutine (PlayNextFrame ());

			// Start Animation Legacy
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();

			// Activate object AND canvas of HP
			ActivateHPcanvas ();

			// Get World Number for anim. NEEDS DELETE and FIX with new world record rather than FIND
//			worldNumForMe = FindObjectOfType<EnemySpawner>().worldNumber;

			// Start Animator OLD
//			myAnimatorHolder.SetInteger ("World Number", worldNumForMe);
//			myAnimatorHolder.SetFloat ("My Run Speed", mySpeed);
//			myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
//			myAnimatorHolder.SetTrigger ("Run Trigger");
//			myAnimatorHolder.Play ("Start Run Fade");

			// Start Animator New
			myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
			myAnimatorHolder.Play ("Enemy Fade-In Anim 1");

			// Start run smoke particle
			particleRunDustHolder.Play();

			// Toast for spawn
//			Moshtan_Utilties_Script.ShowToast ("WWW");
		}
	}

	void ActivateHPcanvas () {
		canvasHP_Holder.gameObject.SetActive (true);
		canvasHP_Holder.enabled = true;

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			canvasHP_Holder.sortingOrder = 7;
		}
	}

	IEnumerator PlayNextFrame () {
		if (!attackBegun) {
			spriteRend_Body.sprite = activeSprites_Body [currFrame];
			spriteRend_Head.sprite = activeSprites_Head [currFrame];
		}
		yield return new WaitForSeconds (framesPerSec);
		currFrame++;
		if (currFrame > 6) {
			currFrame = 0;
			spriteRend_Body.flipX = !spriteRend_Body.flipX;

			// No flip for fat head and some others
//			if (myEnemyType == Enemy.EnemyType.Thin1 || myEnemyType == Enemy.EnemyType.Thin2) {
//				Debug.LogError ("YO");
//				spriteRend_Body.transform.localScale = new Vector3 (-1,1,1);
//				spriteRend_HitHead_UP.flipX = !spriteRend_HitHead_UP.flipX;
//				spriteRend_HitHead_Down.flipX = !spriteRend_HitHead_Down.flipX;
//				spriteRend_HitHead_Right.flipX = !spriteRend_HitHead_Right.flipX;
//				spriteRend_HitHead_Left.flipX = !spriteRend_HitHead_Left.flipX;
			if (isHeadSpinner) {
				spriteRend_Head.flipX = !spriteRend_Head.flipX;
			}
//				spriteRend_Head.flipX = !spriteRend_Head.flipX;
//				spriteRend_Head.transform.localScale = new Vector3 (-spriteRend_Head.transform.localScale.x,1,1);
//			}
		}

//		if (false) {
//			Debug.LogError ("Head spinning!");
//			newV3Buffer_Head = transformPois_HeadOrigV3;
//			newV3Buffer_HeadHit = transformPois_HeadHitOrigV3;
//			if (spriteRend_Body.flipX) {
//				switch (currFrame) {
//				case 2:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.35F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.35F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("2 new head: " + newV3Buffer_Head);
//					break;
//				case 3:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.5F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.5F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("3 new head: " + newV3Buffer_Head);
//					break;
//				case 4:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.4F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.4F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("4 new head: " + newV3Buffer_Head);
//					break;
//				default:
//					transform_HeadHolder.localPosition = transformPois_HeadOrigV3;
//					transform_HeadHitHolder.localPosition = transformPois_HeadHitOrigV3;
//					break;
//				}
//			}
//		}
//

		while (EventTrigger.isPaused || EventTrigger.isFrozen) {
			yield return null;
		}

		// This check is new. It makes sure the frames are not continued after death
		if (!enemyHealth_Script_Holder.amDead) {
			if (!attackBegun) {
				StartCoroutine (PlayNextFrame ());
			}

			// Check in case its second costume
			if (MyType == BodyTypes.B2_H2) {
				StartCoroutine (coroutineDeleg2ndCostume ());
			}

		}
	}

//	IEnumerator coroutine2ndCostume () {
//		yield return null;
//	}

	public IEnumerator Arrows2ndCostume_Nothing () {
		yield return null;
	}

	public IEnumerator Arrows2ndCostume_Question () {
		yield return null;
		int2ndCostumeCounter++;
		if (int2ndCostumeCounter == 42) {
			particle2ndCostPoof.Play ();
			obj2ndCostumeObj.SetActive (true);
		}

		if (int2ndCostumeCounter == 65) {
			particle2ndCostPoof.Play ();
			obj2ndCostumeObj.SetActive (false);
			int2ndCostumeCounter = 0;
		}
	}

	public IEnumerator Arrows2ndCostume_Reverse () {
		yield return null;
		obj2ndCostumeObj.SetActive (true);
	}

	public IEnumerator Arrows2ndCostume_Thorns () {
		yield return null;
		int2ndCostumeCounter++;
		if (int2ndCostumeCounter == 65) {
			particle2ndCostPoof.Play ();
		}

		if (int2ndCostumeCounter == 80) {
			obj2ndCostumeObj.SetActive (true);
			isThornsOn = true;
		}

		if (int2ndCostumeCounter == 100) {
			obj2ndCostumeObj.SetActive (false);
			isThornsOn = false;
			particle2ndCostPoof.Emit (3);
			//
			int2ndCostumeCounter = 0;
		}
	}

	public IEnumerator Arrows2ndCostume_Bandage () {
		yield return null;
		int2ndCostumeCounter++;
		if (int2ndCostumeCounter == 20) {
			obj2ndCostumeObj.SetActive (true);

			// So after turning the 2nd cost obj on, it does nothing
			coroutineDeleg2ndCostume = Arrows2ndCostume_Nothing;
		}
	}

	public void EnemyAttack () {
//		Debug.LogError ("Attack!");

		if (!enemyHealth_Script_Holder.amDead) {
			// Make Enemy Invincible
			enemyHealth_Script_Holder.amInvincible = true;

			// Remove Listener
//			EventManager.StopListening ("Pause", PauseMe);
			EventManager.StopListening ("Freeze", FreezeMe);

			EventManager.StopListening ("Hurt Enemies", HurtCheck);
			EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

			// Attack Anims
			myAnimLegacyRef.clip = myAnimClipsArr [4];
			myAnimLegacyRef.Play ();
			myAnimatorHolder.Play ("Enemy Comm Turn-Dark Anim 1");
			attackBegun = true;
			HideEnemySprite_Normal ();
			ShowEnemySprite_Attack ();

			// Play the HP Back Anim Legacy
			myHPBackUnPausable_AnimHolder.Play ("Enemy HP Back Attack Anim 1 (Legacy)");

			// Remove second costume addition (Start of attack)
			if (enemyHealth_Script_Holder.am2ndCostume) {
				obj2ndCostumeObj.SetActive (false);
			}

			// Removed particleHPAttackBurstHolder. Use black burst from remainder to replace

//			particleHPAttackBurstHolder.transform.SetParent (particleHPAttackBurstHolder.transform.parent.parent);
//			myHPBackUnPausable_AnimHolder.gameObject.SetActive (false);
//			particleHPAttackBurstHolder.Play ();
		}

//		Trigger Attack (Don't want to use event)
//		EventManager.TriggerEvent ("Hurt Player");

	}
		
	public void DestroyMe_AfterAttack () {
		this.transform.SetParent (transform.root);

		// (This check is for player HURT by enemy)
		enemyHealth_Script_Holder.HurtLastCheck ();

		// (TODO: Copied TO Thorns. Needs to be the same)

		// Check player "Shileded" chance
		// Player's shield chance was higher
		if (PlayerData_InGame.Instance.p1_Invincible
			|| (PlayerData_InGame.Instance.p1_ShieldChanceTotal >= Random.Range (1, 100))) {

			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.Shielded_HurtPlayer ();
		}
		// Player's shield chance was lower
		else {
			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.HurtPlayer ();

			// Player was hit AND hurt
//			if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
//				PlayerData_InGame.Instance.stats_Stars_Untouchable = false;
//			}
		}
		DestroyMe_Now ();
	}

	public void DestroyMe_RemoveMe () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

		// Lucky Explode Check
		enemyHealth_Script_Holder.LuckyExplode ();

		particleDeathPoofHolder.Play ();
		switch (EventTrigger.theKey_Direction) {
		case "U":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-7, 7));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case "D":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (173, 187));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case "R":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case "L":
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		}
		// Play Death Particle
//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.Play();

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.3F);

		// Quest stuff for: common NORMAL death 
		StartCoroutine (Quest_KillCommon_Normal());
	}
		
	// Death by Power Up 1 Direction for Body
	public void DestroyMe_RemoveMe_PowUp (int aNumber) {
		switch (aNumber) {
		case 0:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 1:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 2:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 3:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		}

		// Play Death Particle
		//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
		//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.Play();

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.3F);

		// Quest stuff for: common POW UP death 
		StartCoroutine (Quest_KillCommon_PowUp());
	}

	public IEnumerator DestroyMe_Routine () {
		// Avoid head sudden appear bug / frame (Now via bodyremover Anim)
//		spriteRend_Head.gameObject.SetActive (false);

		// Remove second costume addition (death of enemy)
		if (enemyHealth_Script_Holder.am2ndCostume) {
			obj2ndCostumeObj.SetActive (false);

			// To turn of Giant 2's particles after death (They are not inside obj2ndCostume)
			if (isGiant2ndCostumeGinger) {
				obj2ndCostumeObj.transform.parent.GetChild (1).gameObject.SetActive (false);
				obj2ndCostumeObj.transform.parent.GetChild (2).gameObject.SetActive (false);
			}
		}

		myAnimLegacyRef.Stop ();
		yield return new WaitForSeconds (1.3F);
		DestroyMe_Now ();
	}

	IEnumerator Quest_KillCommon_Normal () {
		yield return null;
		if (QuestManager.Instance != null) {
			
			StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Any ());
			Quest_KillByExplosion ();

			switch (myEnemyType) {
			case Enemy.EnemyType.Fat1:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Fat1 ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Fat1 ());
				break;
			case Enemy.EnemyType.Fat2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Fat2 ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Fat2 ());
				break;
			case Enemy.EnemyType.Thin1:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Thin1 ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Thin1 ());
				break;
			case Enemy.EnemyType.Thin2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Thin2 ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Thin2 ());
				break;
			case Enemy.EnemyType.Muscle1:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Muscle1 ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Muscle1 ());
				break;
			case Enemy.EnemyType.Muscle2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Muscle2 ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Muscle2 ());
				break;
			case Enemy.EnemyType.Giant1:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Giant1 ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Giant1 ());
				break;
			case Enemy.EnemyType.Giant2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Giant2 ());
//				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Giant2 ());
				break;
			default:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Fat1 ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_Fat1 ());
				break;
			}
		}
	}

	IEnumerator Quest_KillCommon_PowUp () {
		yield return null;
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_PowUp1 ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_PowUp1 ());
		}
		StartCoroutine (Quest_KillCommon_Normal());
	}
		
	void Quest_KillByExplosion () {
		if (enemyHealth_Script_Holder.wasHurtByExplosion) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_Explosion ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_Explosion ());
		}
	}

	public void DestroyMe_Now () {
		this.enabled = false;
//		GetComponent<EnemyMover_Common> ().enabled = false;
		Destroy (this.gameObject);
	}

	void HurtCheck () {
		enemyHealth_Script_Holder.CompareMyArrow ();
	}

	public void HurtByThorns () {
		if (isThornsOn) {
			// Copied from Destroy me after attack (TODO: Needs to be the same)
			if (PlayerData_InGame.Instance.p1_Invincible
				|| (PlayerData_InGame.Instance.p1_ShieldChanceTotal >= Random.Range (1, 100))) {

				// Use enemy health script to hurt the player
				enemyHealth_Script_Holder.Shielded_HurtPlayer ();
			}
			// Player's shield chance was lower
			else {
				// Use enemy health script to hurt the player
				enemyHealth_Script_Holder.HurtPlayer_Thorns ();
				particle2ndCostHurt.Play ();
			}
		}
	}

	void ExplodeNStuff () {
		enemyHealth_Script_Holder.HurtMe (PlayerData_InGame.Instance.P1_HF_ExplosionDamage , false);
		enemyHealth_Script_Holder.wasHurtByExplosion = true;
	}

	void PauseMe () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {
				pausedAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
			}
			// Stop Legacy Anims & Animator
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
			particleRunDustHolder.Stop();
		}
		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = pausedAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = pausedAnimTime;

			// Resume Play of Legacy Anims & Animator
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();
			myAnimatorHolder.speed = 1;
			particleRunDustHolder.Play();
		}
	}

	void FreezeMe () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {
				freezeAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
			}
			// Stop Legacy Anims
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
			particleRunDustHolder.Stop();
		}
		if (EventTrigger.isFrozen && !EventTrigger.isPaused) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = freezeAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = freezeAnimTime;

			// Resume Play of Legacy Anims
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();
			myAnimatorHolder.speed = 1;
			particleRunDustHolder.Play();
		}
	}

	void HideEnemySprite_Normal () {
		spriteRend_Body.enabled = false;
		spriteRend_Head.enabled = false;
	}

	void ShowEnemySprite_Normal () {
		spriteRend_Body.enabled = true;
		spriteRend_Head.enabled = true;
	}

	void ShowEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = true;
		spriteRend_Attack_Head.enabled = true;
	}

	void HideEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = false;
		spriteRend_Attack_Head.enabled = false;
	}
}
