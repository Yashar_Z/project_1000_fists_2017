﻿using UnityEngine;
using System.Collections;

public class EnemyMover_HF : MonoBehaviour {

	public Enemy myValuesHolder;
	public bool myExplosionEffectIs_New;

	public Sprite[] mySpritesArr_Body1;
	public Sprite[] mySpritesArr_Body2;
	public Sprite[] mySpritesArr_Head1;
	public Sprite[] mySpritesArr_Head2;
	public Sprite[] mySpritesArr_HitHead1;
	public Sprite[] mySpritesArr_HitHead2;
	public SpriteRenderer spriteRend_Body;
	public SpriteRenderer spriteRend_Head;
	public SpriteRenderer spriteRend_HitHead_UP;
	public SpriteRenderer spriteRend_HitHead_Down;
	public SpriteRenderer spriteRend_HitHead_Right;
	public SpriteRenderer spriteRend_HitHead_Left;
	public SpriteRenderer spriteRend_Attack_Body;
	public SpriteRenderer spriteRend_Attack_Head;

	public Color mustacheColor1;
	public Color mustacheColor2;

	public Canvas canvasHP_Holder;

	public float framesPerSec;
	public float RunTimeLength;
	public AnimationClip[] myAnimClipsArr;
	public Transform myAttackBodyTransHolder;
	public Transform myRemoved_BodyTransHolder;
	public Animation myRemover_AnimHolder;
	public Animation myHPBack_AnimHolder;
	public Animation myHPBackUnPausable_AnimHolder;
	public Animator myAnimatorHolder;
	public Transform[] linesTransArr;
	public ParticleSystem particleDeadBodyTrailHolder;
	public ParticleSystem particleHPAttackBurstHolder;
	public ParticleSystem particleRunDustHolder;
	public ParticleSystem particleDeathPoofHolder;
	public ParticleSystem particleHFDeathPoofHolder;
	public ParticleSystem particleExplodePoofHolder;
	public ParticleSystem particleExplodeGroundHolder;
	public ParticleSystem particleExplodeRingHolder;
	public ParticleSystem particleExplode_AttackHolder;

	public Transform explosionParentTransHolder;
	public Transform transform_HeadHolder;
	public Transform transform_HeadHitHolder;

	// Line & Body Types
	public enum BodyTypes {B1_H1, B1_H2, B2_H1, B2_H2};
	public Enemy.LaneTypes myLineOverride;

	[Header ("Mover Publics")]
	public EnemyHealth enemyHealth_Script_Holder;
	public Animation myAnimLegacyRef;
	public Transform myHPBack_TransRef;

	private Sprite[] activeSprites_Body;
	private Sprite[] activeSprites_Head;
	private Sprite[] activeSprites_HitHead;
	private Color activeMustacheColor;
	private int currFrame;
	private int myExplosionDamage;
	private float pausedAnimTime;
	private float freezeAnimTime;
	private float mySpeed;
	private bool attackBegun;
	private bool iHaveExploded;
	private Enemy.EnemyType myEnemyType;
	[SerializeField]
	private BodyTypes MyType;
	[SerializeField]
	private Enemy.LaneTypes MyLine;

	// Headspinner stuff
	private bool isHeadSpinner;
//	private	Vector3 transformPois_HeadOrigV3;
//	private Vector3 transformPois_HeadHitOrigV3;
//	private Vector3 newV3Buffer_Head;
//	private Vector3 newV3Buffer_HeadHit;

	// Use this for initialization
	void Awake () {
//		enemyHealth_Script_Holder = GetComponent<EnemyHealth> ();

		// Start HP Back size and anim stuff
		myHPBack_TransRef.localScale = Vector3.zero;

		transform.position = Vector3.zero;
		HideEnemySprite_Normal ();
		HideEnemySprite_Attack ();
		enemyHealth_Script_Holder.amInvincible = true;
		particleRunDustHolder.Stop ();
		iHaveExploded = false;

		// For non-spawner, should be here (For EnemyMaker ONLY);
//		GetMyValues_Mover ();
	}

	void GetMyValues_Mover() {
//        myValuesHolder = GetComponent<Enemy>();

        RunTimeLength = myValuesHolder.enemyMoveSpeed;
		myEnemyType = myValuesHolder.enemyType;
		myLineOverride = myValuesHolder.enemyLane;
//		myExplosionDamage = myValuesHolder.explosionDamage;

        SetUpMyMover ();

		// Get Values for Health
		enemyHealth_Script_Holder.GetMyValues_Health();
	}

	void SetUpMyMover () {
		// Initialize Type of Common
		switch (myEnemyType) {
		case Enemy.EnemyType.HF1:
			MyType = BodyTypes.B1_H1;
			enemyHealth_Script_Holder.amExplosive = false;
			break;
		case Enemy.EnemyType.HF2:
			MyType = BodyTypes.B1_H1;
			enemyHealth_Script_Holder.amExplosive = false;
			break;
		case Enemy.EnemyType.HF_X1:
			MyType = BodyTypes.B2_H2;
			enemyHealth_Script_Holder.amExplosive = true;
			break;
		case Enemy.EnemyType.HF_X2:
			MyType = BodyTypes.B2_H2;
			enemyHealth_Script_Holder.amExplosive = true;
			break;
		}

		// Initialize Body & Head (And set mustache color)
		switch (MyType) {
		case BodyTypes.B1_H1:
			activeSprites_Body = mySpritesArr_Body1;
			activeSprites_Head = mySpritesArr_Head1;
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		case BodyTypes.B1_H2:
			activeSprites_Body = mySpritesArr_Body1;
			activeSprites_Head = mySpritesArr_Head2;
			activeSprites_HitHead = mySpritesArr_HitHead2;
			activeMustacheColor = mustacheColor2;
			break;
		case BodyTypes.B2_H1:
			activeSprites_Body = mySpritesArr_Body2;
			activeSprites_Head = mySpritesArr_Head1;
			activeSprites_HitHead = mySpritesArr_HitHead1;
			activeMustacheColor = mustacheColor1;
			break;
		case BodyTypes.B2_H2:
			activeSprites_Body = mySpritesArr_Body2;
			activeSprites_Head = mySpritesArr_Head2;
			activeSprites_HitHead = mySpritesArr_HitHead2;
			activeMustacheColor = mustacheColor2;
			break;
		}

		// Initialize Attack
		spriteRend_Attack_Body.sprite = activeSprites_Body [7];

		// Initialize Hit Head
		spriteRend_HitHead_UP.sprite = activeSprites_HitHead [0];
		spriteRend_HitHead_Down.sprite = activeSprites_HitHead [1];
		spriteRend_HitHead_Right.sprite = activeSprites_HitHead [2];
		spriteRend_HitHead_Left.sprite = activeSprites_HitHead [3];

		// Removed Head For Attack
		//		spriteRend_Attack_Head.sprite = activeSprites_Head [7];

		// Initialize Anim Legacy (Speed & Component)
//		myAnimLegacyRef = GetComponent<Animation> ();
		mySpeed = 1F / RunTimeLength;

		// Initialize Mustache Color
		enemyHealth_Script_Holder.mustacheColorFinal = activeMustacheColor;

		// Initialize full frames & HP back
		attackBegun = false;
		currFrame = Random.Range (0, 7);

		// Slow HP Anim isn't used for HF
//		if (RunTimeLength > 13) {
//			// Change clip for slow HP anims to prevent slow hp display
//			myHPBack_AnimHolder.clip = myHPBack_AnimHolder ["Enemy Comm HP Line Anim 2 Slow (Legacy)"].clip;
//
//			//			Debug.LogError ("Slow Enemy");
//		}

		myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].speed = mySpeed;
		//Test Anim
		//		Invoke ("ActivateEnemy", SpawnWait);
		//		Invoke ("ActivateEnemy", 1);

		// Check for head spinner
//		if (myEnemyType == Enemy.EnemyType.Thin1 
//			|| myEnemyType == Enemy.EnemyType.Thin2
//			|| myEnemyType == Enemy.EnemyType.Muscle1
//			|| myEnemyType == Enemy.EnemyType.Muscle2
//			|| myEnemyType == Enemy.EnemyType.Giant1
//			|| myEnemyType == Enemy.EnemyType.Giant2
//		) {
		isHeadSpinner = true;
//		} else {
//			isHeadSpinner = false;
//		}

		// Headspinner head position store
//		if (isHeadSpinner) {
//			transformPois_HeadOrigV3 = transform_HeadHolder.localPosition;
//			transformPois_HeadHitOrigV3 = transform_HeadHitHolder.localPosition;

//		transformPois_HeadOrigV3.y += 0.0F;
//		transformPois_HeadHitOrigV3.y += 0.0F;

//			transform_HeadHolder.localPosition = transformPois_HeadOrigV3;
//			transform_HeadHitHolder.localPosition = transformPois_HeadHitOrigV3;
//		}
	}

	void OnDisable () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);
	}

	void InitializeMyLine () {
		// Initialize Line Number
		MyLine = myLineOverride;
		transform.GetChild(0).position = linesTransArr [(int)MyLine].position;

		// Initialize Line Anim Legacy
		myAnimLegacyRef.clip = myAnimClipsArr [(int)MyLine];
		myAnimLegacyRef[myAnimLegacyRef.clip.name].speed = mySpeed;
	}

	public IEnumerator ActiveDelay () {
		GetMyValues_Mover ();
		ActivateEnemy ();

		// Start Listeners
		EventManager.StartListening ("Pause", PauseMe);
		EventManager.StartListening ("Freeze", FreezeMe);
		EventManager.StartListening ("Hurt Enemies", HurtCheck);
		EventManager.StartListening ("Explode N Stuff", ExplodeNStuff);

        yield return null;
    }
		
	public void ActivateEnemy () {
		if (EventTrigger.isPaused || EventTrigger.isFrozen) {
			Invoke ("ActivateEnemy", 0.2F);
		} else {
			InitializeMyLine ();
			ShowEnemySprite_Normal ();
			enemyHealth_Script_Holder.amInvincible = false;

			// Say if it is enemy (human) or item (pickup)
			enemyHealth_Script_Holder.amItemPickup = false;

			// Start Full Frame Sprites Anim
			StartCoroutine (PlayNextFrame ());

			// Start Animation Legacy
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();

			// Activate object AND canvas of HP
			ActivateHPcanvas ();

			// Get World Number for anim. NEEDS DELETE and FIX with new world record rather than FIND
//			worldNumForMe = FindObjectOfType<EnemySpawner>().worldNumber;

			// Start Animator OLD
//			myAnimatorHolder.SetInteger ("World Number", worldNumForMe);
//			myAnimatorHolder.SetFloat ("My Run Speed", mySpeed);
//			myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
//			myAnimatorHolder.SetTrigger ("Run Trigger");
//			myAnimatorHolder.Play ("Start Run Fade");

			// Start Animator New
			myAnimatorHolder.SetFloat ("My Fade Speed", (13F / RunTimeLength));
			myAnimatorHolder.Play ("Enemy Fade-In Anim 1");

			// Start run smoke particle
			particleRunDustHolder.Play();

			// Size for attack (Clingy vs Explosive)
//			if (enemyHealth_Script_Holder.amExplosive) {
//				myAttackBodyTransHolder.localScale = Vector3.one;
//			} else {
//			}
		}
	}

	void ActivateHPcanvas () {
		canvasHP_Holder.gameObject.SetActive (true);
		canvasHP_Holder.enabled = true;

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			canvasHP_Holder.sortingOrder = 7;
		}
	}

	IEnumerator PlayNextFrame () {
		if (!attackBegun) {
			spriteRend_Body.sprite = activeSprites_Body [currFrame];
			spriteRend_Head.sprite = activeSprites_Head [currFrame];
		}
		yield return new WaitForSeconds (framesPerSec);
		currFrame++;
		if (currFrame > 6) {
			currFrame = 0;
			spriteRend_Body.flipX = !spriteRend_Body.flipX;

			// No flip for fat head and some others
//			if (myEnemyType == Enemy.EnemyType.Thin1 || myEnemyType == Enemy.EnemyType.Thin2) {
//				Debug.LogError ("YO");
//				spriteRend_Body.transform.localScale = new Vector3 (-1,1,1);
//				spriteRend_HitHead_UP.flipX = !spriteRend_HitHead_UP.flipX;
//				spriteRend_HitHead_Down.flipX = !spriteRend_HitHead_Down.flipX;
//				spriteRend_HitHead_Right.flipX = !spriteRend_HitHead_Right.flipX;
//				spriteRend_HitHead_Left.flipX = !spriteRend_HitHead_Left.flipX;
			if (isHeadSpinner) {
				spriteRend_Head.flipX = !spriteRend_Head.flipX;
			}
//				spriteRend_Head.flipX = !spriteRend_Head.flipX;
//				spriteRend_Head.transform.localScale = new Vector3 (-spriteRend_Head.transform.localScale.x,1,1);
//			}


		}

//		if (false) {
//			Debug.LogError ("Head spinning!");
//			newV3Buffer_Head = transformPois_HeadOrigV3;
//			newV3Buffer_HeadHit = transformPois_HeadHitOrigV3;
//			if (spriteRend_Body.flipX) {
//				switch (currFrame) {
//				case 2:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.35F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.35F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("2 new head: " + newV3Buffer_Head);
//					break;
//				case 3:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.5F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.5F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("3 new head: " + newV3Buffer_Head);
//					break;
//				case 4:
//					newV3Buffer_Head.x = newV3Buffer_Head.x - 0.4F;
//					newV3Buffer_HeadHit.x = newV3Buffer_HeadHit.x - 0.4F;
//					transform_HeadHolder.localPosition = newV3Buffer_Head;
//					transform_HeadHitHolder.localPosition = newV3Buffer_HeadHit;
//					Debug.LogError ("4 new head: " + newV3Buffer_Head);
//					break;
//				default:
//					transform_HeadHolder.localPosition = transformPois_HeadOrigV3;
//					transform_HeadHitHolder.localPosition = transformPois_HeadHitOrigV3;
//					break;
//				}
//			}
//		}

		while (EventTrigger.isPaused || EventTrigger.isFrozen) {
			yield return null;
		}
		if (!attackBegun) {
			StartCoroutine (PlayNextFrame ());
		}
	}

	public void EnemyAttack () {
//		Debug.LogError ("Attack!");

		if (!enemyHealth_Script_Holder.amDead) {
			// Make Enemy Invincible
			enemyHealth_Script_Holder.amInvincible = true;

			// Remove Listener
//			EventManager.StopListening ("Pause", PauseMe);
			EventManager.StopListening ("Freeze", FreezeMe);

			EventManager.StopListening ("Hurt Enemies", HurtCheck);
			EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

			// Attack Anims
			myAnimLegacyRef.clip = myAnimClipsArr [4];
			myAnimLegacyRef.Play ();
			myAnimatorHolder.Play ("Enemy Comm Turn-Dark Anim 1");
			attackBegun = true;
			HideEnemySprite_Normal ();
			ShowEnemySprite_Attack ();

			// Play the HP Back Anim Legacy
			myHPBackUnPausable_AnimHolder.Play ("Enemy HP Back Attack Anim 1 (Legacy)");
//			particleHPAttackBurstHolder.transform.SetParent (particleHPAttackBurstHolder.transform.parent.parent);
//			myHPBackUnPausable_AnimHolder.gameObject.SetActive (false);
//			particleHPAttackBurstHolder.Play ();
		}

//		Trigger Attack (Don't want to use event)
//		EventManager.TriggerEvent ("Hurt Player");

	}
		
	public void DestroyMe_AfterAttack () {
		this.transform.SetParent (transform.root);

		// (This check is for player HURT by enemy)
		enemyHealth_Script_Holder.HurtLastCheck ();

		// "Shielded" chance: Player's shield chance was higher
		if (PlayerData_InGame.Instance.p1_Invincible
			|| (PlayerData_InGame.Instance.p1_ShieldChanceTotal >= Random.Range (1, 100))) {

			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.Shielded_HurtPlayer ();
		}

		// "Shielded" chance: Player's shield chance was lower
		else {
			// Check for explosive or clingy HF (Previously ouside the if/else and above start of "Shielded"
			Determine_ClingyOrExplode ();

			// Use enemy health script to hurt the player
			if (enemyHealth_Script_Holder.amExplosive) {
				enemyHealth_Script_Holder.HurtExplodePlayer ();
			} else {
				enemyHealth_Script_Holder.HurtPlayer ();
			}

			// Player was hit AND hurt
//			if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
//				PlayerData_InGame.Instance.stats_Stars_Untouchable = false;
//			}
		}

		DestroyMe_Now ();
	}

	public void Determine_ClingyOrExplode () {
		// THIS: Explosive HF
		if (enemyHealth_Script_Holder.amExplosive) {

			// If explosion from hitting player clears out the clingy HFs
			if (PlayerData_InGame.Instance.P1_Help_HFX_CanKillClingy && PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool) {
				PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_KillAll ();

//				Debug.LogWarning ("KILL ALL!");
			}

			// To add explosion effect to the front of camera
			particleExplode_AttackHolder.Play ();
			particleExplode_AttackHolder.transform.SetParent (transform.root);
			Destroy (particleExplode_AttackHolder.gameObject, 2);

		} 

		// THIS: Clingy HF
		else {
			if (!PlayerData_InGame.Instance.p1_Invincible) {

				// For anti HF item
				if (Random.Range (1, 100) > PlayerData_InGame.Instance.p1_isAntiHF_Sometimes) {

					if (PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool) {
						PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_Start ((int)MyLine + 1);
					} else {
						PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_Start ((int)MyLine + 1);
					}

				} else {
					// TODO: Anti HF Effect or particle
					Debug.LogError ("Sometimes!!");

					// Achievement for deflecting clingy
					if (AchievementManager.Instance != null) {
						StartCoroutine (AchievementManager.Instance.AchieveProg_HF_ClingySwatter ());
					}

					PlayerData_InGame.Instance.ItemActiveFeed_Show ("!ﺐﺴﭽﻧ");
				}
			}
		}
	}

	public void DestroyMe_RemoveMe () {
		HideEnemySprite_Normal ();
		// Poof effect
//		particleDeathPoofHolder.Play ();
//		particleDeathPoofHolder.transform.SetParent (transform.root);
//		Destroy (particleDeathPoofHolder.gameObject, 1);

		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

		// ALSO Check for explosive or clingy HF

		// THIS: Explosive HF
		if (enemyHealth_Script_Holder.amExplosive) {
			ExplodeMe_Now ();

			// Explosion chance
			if (myExplosionEffectIs_New) {
				particleExplode_AttackHolder.Play ();
				particleExplode_AttackHolder.transform.SetParent (transform.root);
				Destroy (particleExplode_AttackHolder.gameObject, 2);
			} else {
				particleHFDeathPoofHolder.Play ();
				particleExplodePoofHolder.Play ();
				particleExplodeGroundHolder.Play ();
				particleExplodeRingHolder.Play ();
				explosionParentTransHolder.SetParent (transform.root);
				Destroy (explosionParentTransHolder.gameObject, 3);
			}

			DestroyMe_Now ();
//			Invoke ("DestroyMe_Now", 0.1F);
		}

		// THIS: Clingy HF
		else {
			// Lucky Explode Check (ONLY FOR CLINGY)
			enemyHealth_Script_Holder.LuckyExplode ();

			particleDeathPoofHolder.Play ();
			switch (EventTrigger.theKey_Direction) {
			case "U":
				myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-7, 7));
				myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
				break;
			case "D":
				myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (173, 187));
				myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
				break;
			case "R":
				myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
				myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
				break;
			case "L":
				myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
				myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
				break;
			}

			// Call Destroy
			StartCoroutine (DestroyMe_Routine());
//			Invoke ("DestroyMe_Now", 1.3F);
		}

		// Quest stuff for: hf NORMAL death 
		StartCoroutine (Quest_KillHF_Normal());

		// Play Death Particle
//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
//		particleDeadBodyTrailHolder.Play();
	}

	public void ExplodeMe_Now () {
		iHaveExploded = true;
//		PlayerData_InGame.Instance.ExplosionDamage_Set (myExplosionDamage);

		// Copied this to bigbomb pickup items gain/effect in playdata-ingame
		EventManager.TriggerEvent ("Explode N Stuff");
		PlayerData_InGame.Instance.OverallExplosion ();
//		PlayerData_InGame.Instance.PlayerSpeed_SloMo_Start (0.1F);

//		Invoke ("ExplodeMe_End", 0.1F);
	}

	public void ExplodeMe_End () {
		Debug.LogError ("NORMAL SPEED!");
		iHaveExploded = true;
//		PlayerData_InGame.Instance.ExplosionDamage_Reset ();
	}

	// Death by Power Up 1 Direction for Body
	public void DestroyMe_RemoveMe_PowUp (int aNumber) {
		switch (aNumber) {
		case 0:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 1:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 2:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		case 3:
			myRemoved_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
			myRemover_AnimHolder.Play ("Enemy Comm Removed UpSide Anim 1 (Legacy)");
			break;
		}
		// Play Death Particle
		//		Debug.LogError ("Euler = " + myRemoved_BodyTransHolder.eulerAngles.z + " which in rad is " + myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad); 
		//		particleDeadBodyTrailHolder.startRotation = -myRemoved_BodyTransHolder.eulerAngles.z * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.startRotation = 90 * Mathf.Deg2Rad;
		//		particleDeadBodyTrailHolder.Play();

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.3F);

		// Quest stuff for: hf POW UP death 
		StartCoroutine (Quest_KillHF_PowUp());
	}

	public IEnumerator DestroyMe_Routine () {
		// Avoid head sudden appear bug / frame (Now via bodyremover Anim)
//		spriteRend_Head.gameObject.SetActive (false);

		yield return new WaitForSeconds (1.3F);
		DestroyMe_Now ();
	}

	IEnumerator Quest_KillHF_Normal () {
		yield return null;
		if (QuestManager.Instance != null) {
			
			StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_Any ());
			Quest_KillByExplosion ();

			switch (myEnemyType) {
			case Enemy.EnemyType.HF1:
			case Enemy.EnemyType.HF2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_HF ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_HF1 ());
				break;
			case Enemy.EnemyType.HF_X1:
			case Enemy.EnemyType.HF_X2:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_HFX ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_HFX ());
				break;
			default:
				StartCoroutine (QuestManager.Instance.QuestProg_KillEnemy_HF ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_KillEnemy_HF1 ());
				break;
			}
		}
	}

	IEnumerator Quest_KillHF_PowUp () {
		yield return null;
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_PowUp1 ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_PowUp1 ());
		}
		StartCoroutine (Quest_KillHF_Normal());
	}

	void Quest_KillByExplosion () {
		if (enemyHealth_Script_Holder.wasHurtByExplosion) {
			StartCoroutine (QuestManager.Instance.QuestProg_KillBy_Explosion ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_KillBy_Explosion ());
		}
	}

	public void DestroyMe_Now () {
		this.enabled = false;
//		GetComponent<EnemyMover_HF> ().enabled = false;
		StartCoroutine (DestroyMe_NowRoutineHF());
	}

	public IEnumerator DestroyMe_NowRoutineHF () {
		yield return null;
		Destroy (this.gameObject);
	}

	void HurtCheck () {
		enemyHealth_Script_Holder.CompareMyArrow ();
	}

	void ExplodeNStuff () {
		if (!iHaveExploded) {
			enemyHealth_Script_Holder.HurtMe (PlayerData_InGame.Instance.P1_HF_ExplosionDamage , false);
			enemyHealth_Script_Holder.wasHurtByExplosion = true;
		}
	}

	void PauseMe () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {
				pausedAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
				Debug.LogWarning ("PRE_PAUSE, " + gameObject.name + " is playing ANIM: " + myAnimLegacyRef.clip.name);
			}
			// Stop Legacy Anims & Animator
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
			particleRunDustHolder.Stop();
		}
		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = pausedAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = pausedAnimTime;

			// Resume Play of Legacy Anims & Animator
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();
			Debug.LogWarning ("POST_PAUSE, " + gameObject.name + " is playing ANIM: " + myAnimLegacyRef.clip.name);
			myAnimatorHolder.speed = 1;
			particleRunDustHolder.Play();
		}
	}

	void FreezeMe () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {
				freezeAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
			}
			// Stop Legacy Anims
			myAnimLegacyRef.Stop ();
			myHPBack_AnimHolder.Stop ();
			myAnimatorHolder.speed = 0;

			// Stop Run Particle
			particleRunDustHolder.Stop();
		}
		if (EventTrigger.isFrozen && !EventTrigger.isPaused) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = freezeAnimTime;
			myHPBack_AnimHolder [myHPBack_AnimHolder.clip.name].time = freezeAnimTime;

			// Resume Play of Legacy Anims
			myAnimLegacyRef.Play ();
			myHPBack_AnimHolder.Play ();
			myAnimatorHolder.speed = 1;
			particleRunDustHolder.Play();
		}
	}

	void HideEnemySprite_Normal () {
		spriteRend_Body.enabled = false;
		spriteRend_Head.enabled = false;
	}

	void ShowEnemySprite_Normal () {
		spriteRend_Body.enabled = true;
		spriteRend_Head.enabled = true;
	}

	void ShowEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = true;
		spriteRend_Attack_Head.enabled = true;
	}

	void HideEnemySprite_Attack () {
		spriteRend_Attack_Body.enabled = false;
		spriteRend_Attack_Head.enabled = false;
	}
}
