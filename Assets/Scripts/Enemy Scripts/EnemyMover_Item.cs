﻿using UnityEngine;
using System.Collections;

public class EnemyMover_Item : MonoBehaviour {

	public Enemy myValuesHolder;

	public GameObject[] itemObjGroupArr;
	public Sprite[] mySpritesArr;

	public Color mustacheColor1;
	public Color mustacheColor2;
	public Color mustacheColor3;

	public Canvas canvasHP_Holder;

	public float framesPerSec;
	public float RunTimeLength;
	public int myEnemyItemInt;

	// Move Lines (Left & Right)
	public AnimationClip[] myAnimClipsArr;
	public GameObject myBodyBuffer;
	public Transform myGroup_BodyTransHolder;
	public Animation myRemover_AnimHolder;
	public Animation myIdle_AnimHolder;
	public Animation myIdle_InnerBubbleHolder;

	// For Death
//	public Animation myHPBackUnPausable_AnimHolder;
//	public Animator myAnimatorHolder;

	public Transform[] linesTransArr;
	public Transform[] elixirTransRotationsHolder; 
	public Transform itemDeathParticlesTransHolder;

	public ParticleSystem particleHPAttackBurstHolder;
	public ParticleSystem[] particleDeathPoofHolder;

	public Enemy.LaneTypes myLineOverride;

	[Header ("Item Stuff like Remover")]
	public Item_Remover_Script itemRemoverScriptHolder;

	[Header ("Mover Publics")]
	public EnemyHealth enemyHealth_Script_Holder;
	public Animation myAnimLegacyRef;

//	public Transform myHPBack_TransRef;

	private Color activeMustacheColor;
	private bool amMoverImmortal;
	private float pausedAnimTime;
	private float freezeAnimTime;
	private float pausedInnterItemAnimTime;
	private float freezeInnterItemAnimTime;
	private float mySpeed;
	private Enemy.EnemyType myEnemyType;
	private int MyValue;
	private int MyValuePLUS;
	private int myElixirDirRandomInt;
	[SerializeField]
	private Enemy.LaneTypes MyLine;

	private string myElixirDirection;
	private Vector3 myVec3ElixirRotationsBuffer;

	// Headspinner stuff
//	private bool isHeadSpinner;
//	private	Vector3 transformPois_HeadOrigV3;
//	private Vector3 transformPois_HeadHitOrigV3;
//	private Vector3 newV3Buffer_Head;
//	private Vector3 newV3Buffer_HeadHit;

	// Use this for initialization
	void Awake () {
//		enemyHealth_Script_Holder = GetComponent<EnemyHealth> ();

//		transform.position = Vector3.zero;
		HideItemGroupBody ();
		enemyHealth_Script_Holder.amInvincible = true;

		// For non-spawner, should be here (For EnemyMaker ONLY);
		//		GetMyValues_Mover ();
	}

	void GetMyValues_Mover() {
//		myValuesHolder = GetComponent<Enemy>();

//		Debug.LogError ("Super mustache myValuesHolder.enemyMoveSpeed = " + myValuesHolder.enemyMoveSpeed);

		RunTimeLength = myValuesHolder.enemyMoveSpeed;
		myEnemyType = myValuesHolder.enemyType;
		myLineOverride = myValuesHolder.enemyLane;
		MyValue = myValuesHolder.itemValue;

		SetUpMyMover ();

		// Get Values for Health
		enemyHealth_Script_Holder.GetMyValues_Health();
	}

	void SetUpMyMover () {
		// Initialize Value
		itemObjGroupArr [0].SetActive (false);
		itemObjGroupArr [1].SetActive (false);
		itemObjGroupArr [2].SetActive (false);

		// Initialize Body & Head (And set mustache color)
		switch (MyValue) {
		case 1:
			activeMustacheColor = mustacheColor1;
			itemObjGroupArr [0].SetActive (true);
			break;
		case 2:
			activeMustacheColor = mustacheColor2;
			itemObjGroupArr [1].SetActive (true);
			break;
		case 3:
			activeMustacheColor = mustacheColor3;
			itemObjGroupArr [2].SetActive (true);
			break;
		}
			
		// Initialize Anim Legacy (Speed & Component)
//		myAnimLegacyRef = GetComponent<Animation> ();

		mySpeed = 1F / RunTimeLength;

		// Initialize Mustache Color
		enemyHealth_Script_Holder.mustacheColorFinal = activeMustacheColor;

		// Initialize idle speed
//		myIdle_AnimHolder [myIdle_AnimHolder.clip.name].speed = mySpeed;
	}

	void OnDisable () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
		EventManager.StopListening ("Hurt Enemies", HurtCheck);
//		EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);
	}

	void InitializeMyLine () {
//		Debug.LogError ("Initialized!?");
		// Initialize Line Number
		MyLine = myLineOverride;
		transform.GetChild(0).position = linesTransArr [(int)MyLine].position;

		// Initialize Line Anim Legacy
		myAnimLegacyRef.clip = myAnimClipsArr [(int)MyLine];
		myAnimLegacyRef[myAnimLegacyRef.clip.name].speed = mySpeed;
	}

	public IEnumerator ActiveDelay () {
		GetMyValues_Mover ();
		ActivateEnemy ();

		// Start Listeners
		EventManager.StartListening ("Pause", PauseMe);
		EventManager.StartListening ("Freeze", FreezeMe);
		EventManager.StartListening ("Hurt Enemies", HurtCheck);
//		EventManager.StartListening ("Explode N Stuff", ExplodeNStuff);
		yield return null;
	}

	public void ActivateEnemy () {
		if (EventTrigger.isPaused || EventTrigger.isFrozen) {
			Invoke ("ActivateEnemy", 0.2F);
		} else {

			InitializeMyLine ();
			ShowItemGroupBody ();

			// Setup Number for Item
			ItemEffect_SetupMyNumber ();

			// NEED to activate
			enemyHealth_Script_Holder.amInvincible = false;

			// Say if it is enemy (human) or item (pickup)
			enemyHealth_Script_Holder.amItemPickup = true;

			// Start Animation Legacy
			myAnimLegacyRef.Play ();
			myIdle_AnimHolder.Play ();

			// Activate object AND canvas of HP
			ActivateHPcanvas ();

			// Start Inner Bubble Anim
//			myIdle_InnerBubbleHolder[myIdle_InnerBubbleHolder.clip.name].speed = 0.75F;
		}
	}

	void ActivateHPcanvas () {
		canvasHP_Holder.gameObject.SetActive (true);
		canvasHP_Holder.enabled = true;

		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			canvasHP_Holder.sortingOrder = 7;
		}
	}
		
	public void EnemyInvincibleDelay () {
		enemyHealth_Script_Holder.amInvincible = false;
	}

	public void EnemyAttack () {
		//		Debug.LogError ("Attack!");

		if (!enemyHealth_Script_Holder.amDead) {
			// Make Enemy Invincible
			enemyHealth_Script_Holder.amInvincible = true;

			// Remove Listener
//			EventManager.StopListening ("Pause", PauseMe);
			EventManager.StopListening ("Freeze", FreezeMe);

			EventManager.StopListening ("Hurt Enemies", HurtCheck);
//			EventManager.StopListening ("Explode N Stuff", ExplodeNStuff);

			// Attack Anims
			myAnimLegacyRef.clip = myAnimClipsArr [4];
			myAnimLegacyRef.Play ();
//			myAnimatorHolder.Play ("Enemy Comm Turn-Dark Anim 1");
//			attackBegun = true;
			HideItemGroupBody ();
		}
	}

	public void DestroyMe_AfterAttack () {
		this.transform.SetParent (transform.root);

		// (This check is for player HURT by enemy)
		enemyHealth_Script_Holder.HurtLastCheck ();

		// Check player "Shileded" chance
		// Player's shield chance was higher
		if (PlayerData_InGame.Instance.p1_Invincible
			|| (PlayerData_InGame.Instance.p1_ShieldChanceTotal >= Random.Range (1, 100))) {

			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.Shielded_HurtPlayer ();
		}
		// Player's shield chance was lower
		else {
			// Use enemy health script to hurt the player
			enemyHealth_Script_Holder.HurtPlayer ();

			// Player was hit AND hurt
//			if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
//				PlayerData_InGame.Instance.stats_Stars_Untouchable = false;
//			}
		}
		DestroyMe_Now ();
	}

	public void DestroyMe_RemoveMe () {
		if (enemyHealth_Script_Holder.amItemPickup) {
//			myRemover_AnimHolder.Play ("Enemy Item Removed FLOAT STRAIGHT Anim 1 (Legacy)");
//			Debug.LogError ("myEnemyItemInt = " + myEnemyItemInt);

			// To make sure the item is NOT big bomb so it can be shot (ALSO ANOTHER COPY OF IF IS AT THE END FOR SHOOT)
			if (myEnemyItemInt != 7) {
				itemRemoverScriptHolder.ShootItemPickup ();
			} 

			// Meaning the item IS big bomb
			else {
				itemRemoverScriptHolder.ShootItemPickup_QuickEffectCall ();
			}

			// Check for anti-hedgehog sometimes
			if (myEnemyItemInt == 4) {

				// For anti HF item
				if (Random.Range (1, 100) > PlayerData_InGame.Instance.p1_isAntiHedgehog_Sometimes) {
					// Anti-hedgehog failed
					Debug.LogError ("Anti-Hedgehog FAILED!!");
				} else {
					// Particle for anti hedgehog is within the item group 4 hierarchy (play on active)

					Debug.LogError ("Anti-Hedgehog Sometimes!!");
					itemObjGroupArr [0].SetActive (false);
					itemObjGroupArr [1].SetActive (false);
					itemObjGroupArr [2].SetActive (false);

					// Turn on heart icon
					itemObjGroupArr [3].SetActive (true);

					// TODO: Achievement for changing hedgehog to heart
//					if (AchievementManager.Instance != null) {
//						StartCoroutine (AchievementManager.Instance.AchieveProg_HF_ClingySwatter ());
//					}

					PlayerData_InGame.Instance.ItemActiveFeed_Show ("!ﺏﻭﺭ ﻦﯿﻣ");
				}
			}

			if (myEnemyItemInt != 7) {
				StartCoroutine (DestroyMe_ItemAnimStraight ());
			} 

			particleDeathPoofHolder [0].Play ();
		} 

//		else {
//
//			switch (EventTrigger.theKey_Direction) {
//			case "U":
//			myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-7, 7));
//				myRemover_AnimHolder.Play ("Enemy Item Removed UP Anim 1 (Legacy)");
//				particleDeathPoofHolder [0].Play ();
//				break;
//			case "D":
//			myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (173, 187));
//				myRemover_AnimHolder.Play ("Enemy Item Removed DOWN Anim 1 (Legacy)");
//				particleDeathPoofHolder [1].Play ();
//				break;
//			case "R":
//      	myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
//				myRemover_AnimHolder.Play ("Enemy Item Removed RIGHT Anim 1 (Legacy)");
//				particleDeathPoofHolder [2].Play ();
//				break;
//			case "L":
//			myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
//				myRemover_AnimHolder.Play ("Enemy Item Removed LEFT Anim 1 (Legacy)");
//				particleDeathPoofHolder [3].Play ();
//				break;
//			}
//		}

		particleDeathPoofHolder[4].Play ();
		// Call Destroy (Not here, for item, it happens at end of anim leg parent)
//		Invoke ("DestroyMe_Now", myAnimLegacyRef[myAnimLegacyRef.clip.name].length);

		// Item killed normall (Bubble burst)
		StartCoroutine (Quest_KillItem_Normal ());
	}

	public IEnumerator DestroyMe_ItemAnimStraight () {
		myRemover_AnimHolder.Play ();
		yield return null;
	}

	public void DestroyMe_RemoveMe_TheSequel () {
//		myRemover_AnimHolder.Play ("Enemy Item Removed FLOAT END Anim 1 (Legacy)");
	}

//	public IEnumerator DestroyMe_RemoveMe_TheSequel (float waitForDestroyMe_Sequel) {
//		yield return new WaitForSeconds (waitForDestroyMe_Sequel);
//		myRemover_AnimHolder.Play ("Enemy Item Removed FLOAT END Anim 1 (Legacy)");
//	}

	// Death by Power Up 1 Direction for Body
	public void DestroyMe_RemoveMe_PowUp (int aNumber) {
		// Stop horiz movement (Anim Legacy Parent)
		myAnimLegacyRef.Stop();

		// Check to see if item is not mustache so I can use normal move out of bubble anim
		if (myEnemyType != Enemy.EnemyType.Mustache) {

			switch (aNumber) {
			case 0:
				myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-7, 7));
				myRemover_AnimHolder.Play ("Enemy Item Removed UpSide Anim 1 (Legacy)");
				break;
			case 1:
				myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (173, 187));
				myRemover_AnimHolder.Play ("Enemy Item Removed UpSide Anim 1 (Legacy)");
				break;
			case 2:
				myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (-80, -66));
				myRemover_AnimHolder.Play ("Enemy Item Removed UpSide Anim 1 (Legacy)");
				break;
			case 3:
				myGroup_BodyTransHolder.eulerAngles = new Vector3 (0, 0, Random.Range (80, 66));
				myRemover_AnimHolder.Play ("Enemy Item Removed UpSide Anim 1 (Legacy)");
				break;
			}
		}

		// Call Destroy
		StartCoroutine (DestroyMe_Routine());
//		Invoke ("DestroyMe_Now", 1.3F);
	}

	public void ItemEffect_ElixirDirection_All () {
		PlayerData_InGame.Instance.p1_FreezeDirectionInt = myElixirDirRandomInt;
		EventTrigger.theKey_HappyTimeDirection = myElixirDirection;
//		Debug.LogError ("Elixir Setup = " + EventTrigger.theKey_HappyTimeDirection); 
	}

	public void ItemEffect_ElixirDirection_MeOnly () {
		myElixirDirRandomInt = Random.Range (0, 4);
		switch (myElixirDirRandomInt) {
//		switch (2) {
		case 0:
			myElixirDirection = "U";
			myVec3ElixirRotationsBuffer = new Vector3 (0, 0, 180);
//			Debug.LogError (myElixirDirection + " which means:" + myVec3ElixirRotationsBuffer.z);
			break;
		case 1:
			myElixirDirection = "D";
			myVec3ElixirRotationsBuffer = new Vector3 (0, 0, 0);
//			Debug.LogError (myElixirDirection + " which means:" + myVec3ElixirRotationsBuffer.z);
			break;
		case 2:
			myElixirDirection = "R";
			myVec3ElixirRotationsBuffer = new Vector3 (0, 0, 90);
//			Debug.LogError (myElixirDirection + " which means:" + myVec3ElixirRotationsBuffer.z);
			break;
		case 3:
			myElixirDirection = "L";
			myVec3ElixirRotationsBuffer = new Vector3 (0, 0, 270);
//			Debug.LogError (myElixirDirection + " which means:" + myVec3ElixirRotationsBuffer.z);
			break;
		default:
			myElixirDirection = "U";
			myVec3ElixirRotationsBuffer = Vector3.zero;
			break;
		} 
		for (int i = 0; i < 3; i++) {
			elixirTransRotationsHolder [i].eulerAngles = myVec3ElixirRotationsBuffer;
		}
	}

	public void ItemEffect_StopAnimsOnly () {
		// Stop horiz movement (Anim Legacy Parent)
		myAnimLegacyRef.Stop();
		myIdle_AnimHolder.Stop ();
	}

	public void ItemEffect_SetupMyNumber () {
		// Increase pickup spawn count for Achievement
		PlayerData_InGame.Instance.PickupItemCounter_Spawned++;

//		Debug.Log ("MY Type is: " + myEnemyType);
		MyValuePLUS = MyValue + PlayerData_InGame.Instance.p1_ItemValueIncrease;
		switch (myEnemyType) {

		// Item Effect: Pipe = 0
		case Enemy.EnemyType.Pipe:
//			Debug.LogError ("PIPE!");
			myEnemyItemInt = 0;
			break;

		// Item Effect: Elixir = 1
		case Enemy.EnemyType.Elixir:
//			Debug.LogError ("ELIXIR!");
			myEnemyItemInt = 1;
			ItemEffect_ElixirDirection_MeOnly ();
			break;

		// Item Effect: Freeze = 2
		case Enemy.EnemyType.Freeze:
//			Debug.LogError ("FREEZE!");
			myEnemyItemInt = 2;
			break;

		// Item Effect: Heart = 3
		case Enemy.EnemyType.Heart:
//			Debug.LogError ("HEAL!");
			myEnemyItemInt = 3;
			break;

		// Item Effect: Hedgehog  = 4
		case Enemy.EnemyType.Hedgehog:
//			Debug.LogError ("HEDGEHOG!");
			myEnemyItemInt = 4;

			// Increase hedgehog spawn count for Achievement
			PlayerData_InGame.Instance.HedgehogItemCounter_Spawned++;
			break;

		// Item Effect: Mustache  = 5
		case Enemy.EnemyType.Mustache:
//			Debug.LogError ("MUSTACHE!");
			myEnemyItemInt = 5;
			break;

		// Item Effect: Super Mustache  = 6
		case Enemy.EnemyType.SuperMustache:
//			Debug.LogError ("SUPER MUSTACHE!");
			myEnemyItemInt = 6;

			enemyHealth_Script_Holder.ImmortalSetup ();
			itemObjGroupArr [0].SetActive (false);
			itemObjGroupArr [1].SetActive (false);
			itemObjGroupArr [2].SetActive (false);

			itemObjGroupArr [2].SetActive (true);
			amMoverImmortal = true;

//			enemyHealth_Script_Holder.amImmortal = true;
			break;

		// Item Effect: Big Bomb  = 7
		case Enemy.EnemyType.BigBomb:
//			Debug.LogError ("BIG BOMB!");
			myEnemyItemInt = 7;
			break;

		// Item Effect: Big Bomb  = 8
		case Enemy.EnemyType.LineBomb:
//			Debug.LogError ("LINE BOMB!");
			myEnemyItemInt = 8;
			break;

		// Item Effect: Pow Up Fist  = 9
		case Enemy.EnemyType.PowerUp_Fist:
			myEnemyItemInt = 9;

			// Initialize Value
			itemObjGroupArr [0].SetActive (false);
			itemObjGroupArr [1].SetActive (false);
			itemObjGroupArr [2].SetActive (false);

			// Initialize Pow Up Fist icon from item groups
			itemObjGroupArr [0].SetActive (true);

			break;

		// Item Effect: Pow Up Shield  = 10
		case Enemy.EnemyType.PowerUp_Shield:
			myEnemyItemInt = 10;

			// Initialize Value
			itemObjGroupArr [0].SetActive (false);
			itemObjGroupArr [1].SetActive (false);
			itemObjGroupArr [2].SetActive (false);

			// Initialize Pow Up Shield icon from item groups
			itemObjGroupArr [1].SetActive (true);

			break;

		// Item Effect: None / Default
		default:
			break;
		}
	}

	public void ItemEffect_Actual () {
		// Stop horiz movement (Anim Legacy Parent)
		myAnimLegacyRef.Stop();
		myIdle_AnimHolder.Stop ();

//		Debug.Log ("MY Type is: " + myEnemyType);
//		int MyValuePLUS = MyValue + PlayerData_InGame.Instance.p1_ItemValueIncrease;
		switch (myEnemyItemInt) {

		// Item Effect: Pipe = 0
		case 0:
//			Debug.LogError ("PIPE!");
//			myEnemyItemInt = 0;
			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Pipe_1);
				break;
			case 2:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Pipe_2);
				break;
			case 3:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Pipe_3);
				break;
			case 4:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Pipe_4);
				break;
			}
			break;

		// Item Effect: Elixir = 1
		case 1:
//			Debug.LogError ("ELIXIR!");
//			myEnemyItemInt = 1;
			ItemEffect_ElixirDirection_All ();
			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Elixir_1);
				break;
			case 2:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Elixir_2);
				break;
			case 3:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Elixir_3);
				break;
			case 4:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Elixir_4);
				break;
			}
			break;
		
		// Item Effect: Freeze = 2
		case 2:
//			Debug.LogError ("FREEZE!");
//			myEnemyItemInt = 2;
			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Freeze_1);
				break;
			case 2:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Freeze_2);
				break;
			case 3:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Freeze_3);
				break;
			case 4:
				PlayerData_InGame.Instance.PlayerItem_ActivateGeneral (myEnemyItemInt, Item_Values.Instance.ItemValue_Freeze_4);
				break;
			}
			break;

		// Item Effect: Heart = 3
		case 3:
//			Debug.LogError ("HEAL!");
//			myEnemyItemInt = 3;
			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_1);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_1);
				break;
			case 2:
				PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_2);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_2);
				break;
			case 3:
				PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_3);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_3);
				break;
			case 4:
				PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_4);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_4);
				break;
			}
			break;

		// Item Effect: Hedgehog  = 4
		case 4:
//			Debug.LogError ("HEDGEHOG!");
//			myEnemyItemInt = 4;

			// Meaning that the hedgehog has NOT turned to heart
			if (!itemObjGroupArr [3].activeInHierarchy) {

				Debug.LogError ("Yo NO heart");

				switch (MyValue) {
				case 1:
					PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Hedgehog_1);
//				enemyHealth_Script_Holder.EnemyDamage = 1;
					break;
				case 2:
					PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Hedgehog_2);
//				enemyHealth_Script_Holder.EnemyDamage = 3;
					break;
				case 3:
					PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Hedgehog_3);
//				enemyHealth_Script_Holder.EnemyDamage = 5;
					break;
				case 4:
					PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Hedgehog_4);
//				enemyHealth_Script_Holder.EnemyDamage = 5;
					break;
				}
			} 

			// Meaning that the hedgehog HAS turned to heart
			else {
				Debug.LogError ("Yo YESSSS heart");

				// Copy pasted from heart effect (TODO: Remember that it should be the same as normal case of effect for heart)
				switch (MyValuePLUS) {
				case 1:
					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_1);

					// Old method of of mustache gain that used Item Gain obj / scripts
					//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_1);
					break;
				case 2:
					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_2);

					// Old method of of mustache gain that used Item Gain obj / scripts
					//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_2);
					break;
				case 3:
					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_3);

					// Old method of of mustache gain that used Item Gain obj / scripts
					//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_3);
					break;
				case 4:
					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (Item_Values.Instance.ItemValue_Health_4);

					// Old method of of mustache gain that used Item Gain obj / scripts
					//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Health_4);
					break;
				}
			}
			break;

		// Item Effect: Mustache  = 5
		case 5:
//			Debug.LogError ("MUSTACHE! 2222222222222222222");
//			myEnemyItemInt = 5;

			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.itemGainScriptArr [myEnemyItemInt].mustacheCollScriptHolder.MustacheCollector_GiveMe (Item_Values.Instance.ItemValue_Mustache_1);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_1);
				break;
			case 2:
				PlayerData_InGame.Instance.itemGainScriptArr [myEnemyItemInt].mustacheCollScriptHolder.MustacheCollector_GiveMe (Item_Values.Instance.ItemValue_Mustache_2);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_2);
				break;
			case 3:
				PlayerData_InGame.Instance.itemGainScriptArr [myEnemyItemInt].mustacheCollScriptHolder.MustacheCollector_GiveMe (Item_Values.Instance.ItemValue_Mustache_3);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_3);
				break;
			case 4:
				PlayerData_InGame.Instance.itemGainScriptArr [myEnemyItemInt].mustacheCollScriptHolder.MustacheCollector_GiveMe (Item_Values.Instance.ItemValue_Mustache_4);

				// Old method of of mustache gain that used Item Gain obj / scripts
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_4);
				break;
			}
			break;

		// Item Effect: SUPER Mustache  = 6
		case 6:
//			Debug.LogError ("Super Mustache! 2222222222222222222");
			myEnemyItemInt = 6;

			// No item value 
//			switch (MyValuePLUS) {
//			case 1:
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_1);
//				break;
//			case 2:
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_2);
//				break;
//			case 3:
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_3);
//				break;
//			case 4:
//				PlayerData_InGame.Instance.PlayerItem_ActivateConsume (myEnemyItemInt, Item_Values.Instance.ItemValue_Mustache_4);
//				break;
//			}

			break;

		// Item Effect: BigBomb  = 7
		case 7:
			switch (MyValuePLUS) {
			case 1:
				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (1);
				break;
			case 2:
				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (2);
				break;
			case 3:
				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (3);
				break;
			case 4:
				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (4);
				break;
			}
			break;
		
		// Item Effect: LineBomb  = 8
		case 8:
//			switch (MyValuePLUS) {
//			case 1:
//				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (1);
//				break;
//			case 2:
//				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (2);
//				break;
//			case 3:
//				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (3);
//				break;
//			case 4:
//				PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (4);
//				break;
//			}
			break;

		// Item Effect: PowUp Fist  = 9
		case 9:
			PlayerData_InGame.Instance.PlayerItem_PowUpGain_Fist ();
			break;

		// Item Effect: PowUp Shield  = 10
		case 10:
			PlayerData_InGame.Instance.PlayerItem_PowUpGain_Shield ();
			break;

		// Item Effect: None / Default
		default:
			Debug.LogError ("Default Error Yo!");
			break;
		}
	}

	public IEnumerator DestroyMe_Routine () {
		// Item doesn't need this
//		spriteRend_Head.gameObject.SetActive (false);

		yield return new WaitForSeconds (1.3F);
		DestroyMe_Now ();
	}

	public void DestroyMe_Now () {
		this.transform.SetParent (transform.root);
		enemyHealth_Script_Holder.HurtLastCheck ();

		this.enabled = false;
//		GetComponent<EnemyMover_Item> ().enabled = false;

//		Debug.LogError ("DESTROY THIS!");
		Destroy (this.gameObject);
	}

	IEnumerator Quest_KillItem_Normal () {
		yield return null;
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_BubbleExplode ());
		}

		// For items PICKED achievement
		PlayerData_InGame.Instance.PickupItemCounter_Picked++;
	}

	void HurtCheck () {
		if (!amMoverImmortal) {
			enemyHealth_Script_Holder.CompareMyArrow ();
		} else {
			enemyHealth_Script_Holder.CompareMyArrow_AmImmortal ();
//			enemyHealth_Script_Holder.HurtMe_AmImmortal ();
		}
	}

	void ExplodeNStuff () {
		enemyHealth_Script_Holder.HurtMe (PlayerData_InGame.Instance.P1_HF_ExplosionDamage , false);
	}

	void PauseMe () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {
				pausedAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
				pausedInnterItemAnimTime = myIdle_InnerBubbleHolder [myIdle_InnerBubbleHolder.clip.name].time;
//				Debug.LogError (this.name + "THE PAUSE TIME? " + pausedAnimTime);
			}
			// Stop Legacy Anims & Animator & Idle
			myAnimLegacyRef.Stop ();
			myIdle_AnimHolder.Stop ();
			myIdle_InnerBubbleHolder.Stop ();
		}
		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = pausedAnimTime;
			myIdle_AnimHolder [myIdle_AnimHolder.clip.name].time = pausedAnimTime;
			myIdle_InnerBubbleHolder [myIdle_InnerBubbleHolder.clip.name].time = pausedInnterItemAnimTime;

			// Resume Play of Legacy Anims & Animator
			myAnimLegacyRef.Play ();
			myIdle_AnimHolder.Play ();
			myIdle_InnerBubbleHolder.Play ();
		}
	}

	void FreezeMe () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {
				pausedAnimTime = myAnimLegacyRef [myAnimLegacyRef.clip.name].time;
				pausedInnterItemAnimTime = myIdle_InnerBubbleHolder [myIdle_InnerBubbleHolder.clip.name].time;
				//				Debug.LogError (this.name + "THE PAUSE TIME? " + pausedAnimTime);
			}
			// Stop Legacy Anims
			myAnimLegacyRef.Stop ();
			myIdle_AnimHolder.Stop ();
			myIdle_InnerBubbleHolder.Stop ();
		}
		if (EventTrigger.isFrozen && !EventTrigger.isPaused) {
			// Restore Pre-pause Times
			myAnimLegacyRef [myAnimLegacyRef.clip.name].time = pausedAnimTime;
			myIdle_AnimHolder [myIdle_AnimHolder.clip.name].time = pausedAnimTime;
			myIdle_InnerBubbleHolder [myIdle_InnerBubbleHolder.clip.name].time = pausedInnterItemAnimTime;

			// Resume Play of Legacy Anims
			myAnimLegacyRef.Play ();
			myIdle_AnimHolder.Play ();
			myIdle_InnerBubbleHolder.Play ();
		}
	}

	public void ItemInvinciblity_On () {
		enemyHealth_Script_Holder.amInvincible = true;
	}

	public void ItemInvinciblity_Off () {
		enemyHealth_Script_Holder.amInvincible = false;
	}

	void HideItemGroupBody () {
		myBodyBuffer.SetActive (false);
	}

	void ShowItemGroupBody () {
		myBodyBuffer.SetActive (true);
	}
}
