﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy_2ndCostReverseScript : MonoBehaviour {

	public Image imageHP_CircleHolder;
	public Text textHP_CircleHolder;

	public Image imageHP_AllBackHolderL;
	public Image imageHP_AllBackHolderR;
	public Image imageHP_AllBackHolderGrads1;
	public Image imageHP_AllBackHolderGrads2;

	void OnEnable () {
		StartCoroutine (ArrowsReverseColor ());
	}

	IEnumerator ArrowsReverseColor () {
		yield return new WaitForSeconds (0.1F);

		// Blue Arrow
		imageHP_CircleHolder.color = new Color (0, 0.45F, 0.9F, 1);
//		imageHP_CircleHolder.color = Color.black;

		// White HP Text / color
//		textHP_CircleHolder.color = new Color (1, 0.75F, 0.8F, 1);
		textHP_CircleHolder.color = Color.white;

		// Pink Everything Else
		imageHP_AllBackHolderL.color = new Color (1, 0.75F, 0.8F, 1);
		imageHP_AllBackHolderR.color = new Color (1, 0.75F, 0.8F, 1);
//		imageHP_AllBackHolderL.color = Color.white;
//		imageHP_AllBackHolderR.color = Color.white;
		imageHP_AllBackHolderGrads1.color = new Color (1, 0.75F, 0.8F, 1);
		imageHP_AllBackHolderGrads2.color = new Color (1, 0.75F, 0.8F, 1);
//		imageHP_AllBackHolderGrads1.color = Color.white;
//		imageHP_AllBackHolderGrads2.color = Color.white;
	}
}
