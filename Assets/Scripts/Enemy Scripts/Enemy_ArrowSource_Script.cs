﻿using UnityEngine;
using System.Collections;

public class Enemy_ArrowSource_Script : MonoBehaviour {

	public static Enemy_ArrowSource_Script Instance;

	public Vector3[] v3_ArrowNewWaitPositionArr;
	public Sprite[] spriteArrowSourceArr;

	public Transform[] transArrowsSourceArr;

	// Use this for initialization
	void Awake () {
		Instance = this;
	}
}
