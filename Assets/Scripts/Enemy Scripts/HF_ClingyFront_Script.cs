﻿using UnityEngine;
using System.Collections;

public class HF_ClingyFront_Script : MonoBehaviour {

	public Animation hf_AnimHolder;
	public int hf_ClingerHP;
	public int hf_ClingerNumber;
	public bool amAvailable;

	private bool amDead;

	void Start () {
//		amEmpty = true;
	}

	public void HF_ClingyFront_Add () {
		hf_ClingerNumber = PlayerData_InGame.Instance.P1_HF_CountTotal;
		amAvailable = false;
		amDead = false;
		this.gameObject.SetActive (true);
		transform.SetAsFirstSibling ();

		hf_ClingerHP = PlayerData_InGame.Instance.P1_HF_ClingyHP;
		hf_AnimHolder.Play ("HUD HF Front RESET Anim 1 (Legacy)");
	}

	void HF_LastEnemyCheck () {
		if (PlayerData_InGame.Instance.lastEnemyWasSpawned) {
			StartCoroutine (PlayerData_InGame.Instance.Player_CheckLastEnemy ());
		}
	}

	public void HF_ClingyFront_Hurt (int hitAmount) {

		hf_ClingerHP = hf_ClingerHP - hitAmount;
//		Debug.LogError ("My HP: " + hf_ClingerHP);
		// HF Clingy was killed
		if (hf_ClingerHP < 1) {
			amDead = true;
			Debug.LogError ("HF DIES!");
			Debug.LogError ("HF LIVES!");
			switch (EventTrigger.theKey_Direction) {
			case "U":
				hf_AnimHolder.Play ("HUD HF Front END UP Anim 1 (Legacy)");
				break;
			case "D":
				hf_AnimHolder.Play ("HUD HF Front END DOWN Anim 1 (Legacy)");
				break;
			case "R":
				hf_AnimHolder.Play ("HUD HF Front END RIGHT Anim 1 (Legacy)");
				break;
			case "L":
				hf_AnimHolder.Play ("HUD HF Front END LEFT Anim 1 (Legacy)");
				break;
			default:
				hf_AnimHolder.Play ("HUD HF Front END UP Anim 1 (Legacy)");
				break;
			}

			// The moment enemy dies, send them to the front of hierarchy
//				transform.SetAsFirstSibling ();
		} 

	// HF Clingy was hurt but not killed
	else {
			amDead = false;
			Debug.LogError ("HF LIVES!");
			switch (EventTrigger.theKey_Direction) {
			case "U":
				hf_AnimHolder.Play ("HUD HF Front HIT UP Anim 1 (Legacy)");
				break;
			case "D":
				hf_AnimHolder.Play ("HUD HF Front HIT DOWN Anim 1 (Legacy)");
				break;
			case "R":
				hf_AnimHolder.Play ("HUD HF Front HIT RIGHT Anim 1 (Legacy)");
				break;
			case "L":
				hf_AnimHolder.Play ("HUD HF Front HIT LEFT Anim 1 (Legacy)");
				break;
			default:
				hf_AnimHolder.Play ("HUD HF Front HIT UP Anim 1 (Legacy)");
				break;
			}
		}

	}

	public void HF_Clingy_CheckLife () {
		if (!amDead) {
			hf_AnimHolder.Stop ();
		}
	}

	public void HF_ClingyFront_Remove () {
		// Update Clingy count and check to turn clingy bool to "false"
		PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontCount--;
		if (PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontCount == 0) {
			PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool = false;
			PlayerData_InGame.Instance.Player_CameraIdle ();
		}
		hf_ClingerNumber = 600;
		amAvailable = true;
		this.gameObject.SetActive (false);
	}
}
