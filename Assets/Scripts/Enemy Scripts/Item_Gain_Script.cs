﻿using UnityEngine;
using System.Collections;

public class Item_Gain_Script : MonoBehaviour {

	public ParticleSystem[] particleItemAllArr;
	public ParticleSystem particleFlare;
	public ParticleSystem particleHeartSlash;
//	public ParticleSystem[] particleHedgehogArr;
//	public ParticleSystem[] particleMustacheArr;

	public Mustache_Collector mustacheCollScriptHolder;

	public Animation animItemAllMoveHolder;
	public bool AllowOldAnims;

	public int howMuchItem_GainScript;
	public int whichItem_GainScript;

	public void ItemGain_StartMove (int whichItem, int howMuchItem) {
		howMuchItem_GainScript = howMuchItem;
		whichItem_GainScript = whichItem;

//		Debug.LogWarning ("whichItem = " + whichItem_GainScript);

		switch (whichItem_GainScript) {

		// Need to update values from mover!!!!!! NEED!
		// Index 3 = Health
		case 3:
			PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);

			// Old method that checked for top of screen anims allowence
//			if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Health_2) {
//				if (AllowOldAnims) {
//					StartCoroutine (ParticleEmitter_Delayed (0, 0));
//					animItemAllMoveHolder.Play ();
//				} else {
//					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);
//				}
//			} else if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Health_3) {
//				if (AllowOldAnims) {
//					StartCoroutine (ParticleEmitter_Delayed (0, 0));
//					StartCoroutine (ParticleEmitter_Delayed (1, 0.1F));
//					animItemAllMoveHolder.Play ();
//				} else {
//					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);
//				}
//			} else {
//				if (AllowOldAnims) {
//					StartCoroutine (ParticleEmitter_Delayed (0, 0.1F));
//					StartCoroutine (ParticleEmitter_Delayed (1, 0.2F));
//					StartCoroutine (ParticleEmitter_Delayed (2, 0));
//					animItemAllMoveHolder.Play ();
//				} else {
//					PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);
//				}
//			}
			break;

		// Index 4 = Hedgehog
		case 4:
			if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Health_2) {
//				particleItemAllArr [0].Emit (1);
				animItemAllMoveHolder.Play ();
			} else if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Health_3) {
//				particleItemAllArr [1].Emit (1);
				animItemAllMoveHolder.Play ();
			} else {
//				particleItemAllArr [2].Emit (1);
				animItemAllMoveHolder.Play ();
			}
			break;

		// Index 5 = Mustache
		case 5:
			mustacheCollScriptHolder.MustacheCollector_GiveMe (howMuchItem_GainScript);

			// Old method that checked for top of screen anims allowence

			#region OldMustacheItemGain
//			if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Mustache_2) {
////				StartCoroutine (ParticleEmitter_Delayed (0, 0));
//
//				// Old Straight Give
////				PlayerData_InGame.Instance.PlayerItem_Mustache_GainNow (howMuchItem_GainScript);
//				mustacheCollScriptHolder.MustacheCollector_GiveMe (howMuchItem_GainScript);
////				particleItemAllArr [0].Emit (1);
////				animItemAllMoveHolder.Play ();
//			} else if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Mustache_3) {
////				StartCoroutine (ParticleEmitter_Delayed (0, 0));
////				StartCoroutine (ParticleEmitter_Delayed (1, 0.1F));
//
//				// Old Straight Give
////				PlayerData_InGame.Instance.PlayerItem_Mustache_GainNow (howMuchItem_GainScript);
//				mustacheCollScriptHolder.MustacheCollector_GiveMe (howMuchItem_GainScript);
////				particleItemAllArr [0].Emit (1);
////				particleItemAllArr [1].Emit (1);
////				animItemAllMoveHolder.Play ();
//			} else {
////				StartCoroutine (ParticleEmitter_Delayed (0, 0.1F));
////				StartCoroutine (ParticleEmitter_Delayed (1, 0.2F));
////				StartCoroutine (ParticleEmitter_Delayed (2, 0));
//
//				// Old Straight Give
////				PlayerData_InGame.Instance.PlayerItem_Mustache_GainNow (howMuchItem_GainScript);
//				mustacheCollScriptHolder.MustacheCollector_GiveMe (howMuchItem_GainScript);
////				particleItemAllArr [0].Emit (1);
////				particleItemAllArr [1].Emit (1);
////				particleItemAllArr [2].Emit (1);
////				animItemAllMoveHolder.Play ();
//			}
			#endregion
			break;

		// Index 6 = Super Mustache
		case 6:
			if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Mustache_2) {
				if (AllowOldAnims) {
					StartCoroutine (ParticleEmitter_Delayed (0, 0));
					animItemAllMoveHolder.Play ();
				}
			} else if (howMuchItem_GainScript < Item_Values.Instance.ItemValue_Mustache_3) {
				if (AllowOldAnims) {
					StartCoroutine (ParticleEmitter_Delayed (0, 0));
					StartCoroutine (ParticleEmitter_Delayed (1, 0.1F));
					animItemAllMoveHolder.Play ();
				}
			} else {
				if (AllowOldAnims) {
					StartCoroutine (ParticleEmitter_Delayed (0, 0.1F));
					StartCoroutine (ParticleEmitter_Delayed (1, 0.2F));
					StartCoroutine (ParticleEmitter_Delayed (2, 0));
					animItemAllMoveHolder.Play ();
				}
			}
			break;

			// Don't use this. instead go for big bomb gain now!
		case 7:
			PlayerData_InGame.Instance.PlayerItem_BigBomb_GainNow (howMuchItem_GainScript);

			break;
			// Flare particle
//			particleFlare.Play ();
//			break;
		default:
			break;
		}
	}

	public IEnumerator ParticleEmitter_Delayed (int particleIndex, float particleDelay) {
		yield return new WaitForSeconds (particleDelay);
		particleItemAllArr [particleIndex].gameObject.SetActive (true);
		particleItemAllArr [particleIndex].Emit (1);
	}

	public void ItemEffect_GainLATE () {
//		Debug.LogWarning ("Bonus Kill Heal: " + howMuchItem_GainScript);

		PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);
	}

	public void ItemEffect_GainNow () {
		switch (whichItem_GainScript) {

		// Index 3 = Health
		case 3:
			PlayerData_InGame.Instance.PlayerItem_Heart_GainNow (howMuchItem_GainScript);
			break;

		// Index 4 = Hedgehog
		case 4:
			PlayerData_InGame.Instance.PlayerItem_Hedgehog_GainNow (howMuchItem_GainScript);
			break;

		// Index 5 = Mustache
		case 5:
			PlayerData_InGame.Instance.PlayerItem_Mustache_GainNow (howMuchItem_GainScript);
			break;

		default:
			break;
		}
	}
		
	public void SlashHeartOLD () {
	}

	public void SlashHeart () {
		// Slash heart effect
		particleHeartSlash.Emit (1);
	}
}
