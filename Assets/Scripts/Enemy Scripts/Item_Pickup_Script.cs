﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Item_Pickup_Script : MonoBehaviour {

	public bool isFreeze;
	public Image itemImageFillerBack;
//	public Image itemImageFillerFront;
	public float itemCountdownIncreaseAmount;
	public float itemCountdownIncreaseTime;
	public float itemDurability;
	public bool thisItemIsActive;

	public ParticleSystem particleItemUseHolder;
	public Animation itemAnimRef;

	public Camera_InGame_Script cameraIngameScriptHolder;

	private int itemIndex;
	private float itemLength;
	private bool allowFillIncrease;

	void Start () {
		allowFillIncrease = false;
//		thisItemIsActive = false;
	}

	public void ItemEffect_Activate () {
		thisItemIsActive = true;
		switch (itemIndex) {
		case 0:
			PlayerData_InGame.Instance.PlayerItem_Pipe_Activate ();
			break;
		
		case 1:
			PlayerData_InGame.Instance.PlayerItem_Elixir_Activate ();
			break;
		
		case 2:
			PlayerData_InGame.Instance.PlayerItem_Freeze_Activate ();
			break;
		// Health
		case 3:
			break;
		// Hedgehog
		case 4:
			break;
		// Mustache
		case 5:
			break;
		default:
			break;
		}
	}

	public void ItemEffect_Deactivate () {
		switch (itemIndex) {
		case 0:
			PlayerData_InGame.Instance.PlayerItem_Pipe_Deactivate ();
			break;

		case 1:
			PlayerData_InGame.Instance.PlayerItem_Elixir_Deactivate ();
			break;

		case 2:
			PlayerData_InGame.Instance.PlayerItem_Freeze_Deactivate ();
			break;
			// Health
		case 3:
			break;
			// Hedgehog
		case 4:
			break;
			// Mustache
		case 5:
			break;
		default:
			break;
		}
	}

	public void ItemHierarchy_MoveUp () {
		transform.SetSiblingIndex (0);
//		Debug.LogError ("FIRST : " + this.name);
	}

	public void ItemCountdown_Start (int whichItem, float length) {
		particleItemUseHolder.Stop ();
		particleItemUseHolder.Play ();

		ItemHierarchy_MoveUp ();
		itemIndex = whichItem;
		itemLength = length;
		itemCountdownIncreaseTime = 0.5F / (itemLength);
		itemCountdownIncreaseAmount = 0.5F / (itemLength * itemLength);
		itemAnimRef.Stop ();
		itemAnimRef.Play ("Enemy Item Countdown Start Anim 2 (Legacy)");
		allowFillIncrease = false;
		allowFillIncrease = true;
		itemImageFillerBack.fillAmount = 1;
//		itemImageFillerFront.fillAmount = 1;

		// Start fill amount
		if (itemIndex == 1) {
			StartCoroutine (ItemCountDown_Progress ());
		} else {
			itemDurability = length;
		}

		// Activate Item 
		ItemEffect_Activate ();
	}
		
	public void ItemCountDown_DurabilityDecrease () {
		itemDurability--;
		if (itemDurability > -1) {
			particleItemUseHolder.Emit (1);

			if (itemIndex == 2) {
				PlayerData_InGame.Instance.PlayerHUD_ShutterAnim_2 ();
				//Debug.LogWarning ("Name of this item: " + this.name);
                // SOUNDD EFFECTS - Camera Shutter (done)
                if (SoundManager.Instance != null)
                {
                    SoundManager.Instance.CameraClick.Play();
                }

				if (AchievementManager.Instance != null) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_DoPhotograph_100 ());
				}

				// TODO: End camera early (When no more enemies on screen)
				if (PlayerData_InGame.Instance.Player_CamCheckLastEnemy ()) {
					itemDurability = 0;
				}
			}

			if (itemDurability == 0) {
				allowFillIncrease = false;
				ItemCountdown_End ();
			} else {
				itemImageFillerBack.fillAmount = itemDurability / itemLength;
				if (isFreeze) {
					PlayerData_InGame.Instance.PlayerItem_Freeze_FilmDecrease ();
				}
			}
		}
	}

	public IEnumerator ItemCountDown_DurabilityEND () {
		yield return null;
		if (itemDurability > -1) {
			ItemCountDown_DurabilityDecrease ();
			StartCoroutine (ItemCountDown_DurabilityEND());
		}
	}

	public void ItemEndItNow() {
		itemImageFillerBack.fillAmount = 0;
		itemDurability = 0;
		ItemCountdown_End ();
	}
		
	public void ItemCountdown_End () {
		particleItemUseHolder.Stop ();

		itemAnimRef.Play ("Enemy Item Countdown End Anim 1 (Legacy)");

//		Debug.LogError ("Count END!");

		// Deactivate Item 
		thisItemIsActive = false;
		ItemEffect_Deactivate ();
	}

	public void Item_Deactivate () {
		this.gameObject.SetActive (false);
	}

	IEnumerator ItemCountDown_Progress () {
		if (allowFillIncrease) {
			itemImageFillerBack.fillAmount -= itemCountdownIncreaseAmount;
//			itemImageFillerFront.fillAmount -= itemCountdownIncreaseAmount;
			yield return new WaitForSeconds (itemCountdownIncreaseTime);

			while (EventTrigger.isPaused) {
				yield return null;
			}

			if (itemImageFillerBack.fillAmount > 0) {
				if (!isFreeze) {
					while (EventTrigger.isFrozen || EventTrigger.isPaused) {
						yield return null;
					}
					allowFillIncrease = true;
					StartCoroutine (ItemCountDown_Progress ());
				} else {
					while (EventTrigger.isPaused) {
						yield return null;
					}
					allowFillIncrease = true;
					StartCoroutine (ItemCountDown_Progress ());
				}
			} else {
				allowFillIncrease = false;
				ItemCountdown_End ();
			}
		}
	}
}
