﻿using UnityEngine;
using System.Collections;

public class Item_Remover_Script : MonoBehaviour {

	public Vector3 v3_ItemPickupTarget_LeftFar;
	public Vector3 v3_ItemPickupTarget_LeftFar_Barrel;
	public Vector3 v3_ItemPickupTarget_LeftMid;
	public Vector3 v3_ItemPickupTarget_Mid;
	public Vector3 v3_ItemPickupTarget_RightMid;
	public Vector3 v3_ItemPickupTarget_RightFar;
	public Vector3 v3_ItemPickupAngle;

	// V3s for HUD pow up positions
	public Vector3 v3_ItemPickupTarget_BottomLeft;
	public Vector3 v3_ItemPickupTarget_BottomRight;

	public float itemFinalHeight;
	public float itemFinalDepth;
	public Transform itemGroupTransRef;
	public Transform transQuickEffectParticlesHolder;
	public Animation animItemRemover;
	public EnemyMover_Item enemyMoverItemHolder;
	public bool isThisFromBarrel;

	private bool isGoingUp;
	private bool isFinalFlying;
	private float itemShootEndTime;
	private float moveLength;
	private Vector3 v3_ItemPickup_TargetFinal;

	void Awake () {
		isFinalFlying = false;
		v3_ItemPickupTarget_LeftFar = new Vector3 (-8F, 4F, 0);
		v3_ItemPickupTarget_LeftFar_Barrel = new Vector3 (-8.9F, 4F, 0);
		v3_ItemPickupTarget_LeftMid = new Vector3 (-5.2F, 3.8F, 0);
		v3_ItemPickupTarget_Mid = new Vector3 (0, 4F, 0);
		v3_ItemPickupTarget_RightMid = new Vector3 (5.2F, 3.8F, 0);
		v3_ItemPickupTarget_RightFar = new Vector3 (8F, 4F, 0);

		v3_ItemPickupTarget_BottomRight = new Vector3 (7.5F, -4, 0);
		v3_ItemPickupTarget_BottomLeft = new Vector3 (-7.5F, -4, 0);
	}

	public void ItemRemoverSetup (int whichItem) {
		switch (whichItem) {
		case 0:
			// Item Effect: Pipe = 0
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_LeftMid;
			isGoingUp = true;
			break;

		case 1:
			// Item Effect: Elixir = 1
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_LeftMid;
			isGoingUp = true;
			break;

		case 2:
			// Item Effect: Freeze = 2
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_LeftMid;
			isGoingUp = true;
			break;

		case 3:
			// Item Effect: Heart = 3
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_LeftFar;
			isGoingUp = true;
			break;	

		case 4:
			// Item Effect: Hedgehog = 4
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_LeftFar;
			isGoingUp = true;
			break;

		case 5:
			// Item Effect: Mustache = 5
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_RightFar;
			isGoingUp = true;
			break;

		case 6: // Super Mustache
		case 7: // Big Bomb
		case 8: // Line Bomb
			break;

		case 9:
			// Item Effect: Pow Up Fist = 9
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_BottomRight;
			isGoingUp = false;
			break;

		case 10:
			// Item Effect: Pow Up Shield = 10
			v3_ItemPickup_TargetFinal = v3_ItemPickupTarget_BottomLeft;
			isGoingUp = false;
			break;

		default:
			Debug.LogError ("Default Case");
			break;
		}
		v3_ItemPickupAngle = Vector3.zero;
	}

	public void ShootItemPickup () {
//		Debug.LogError ("YO 1");

		enemyMoverItemHolder.ItemEffect_StopAnimsOnly ();
		ItemRemoverSetup (enemyMoverItemHolder.myEnemyItemInt);
//		Debug.LogWarning ("Target Posit: " + v3_ItemPickup_TargetFinal + " while this posit " + transform.position);

		v3_ItemPickupAngle.z = Mathf.Atan2 ((v3_ItemPickup_TargetFinal.y - transform.position.y)
			, (v3_ItemPickup_TargetFinal.x - transform.position.x)) * Mathf.Rad2Deg;

//		Debug.LogWarning ("Angle = " + v3_ItemPickupAngle.z);

		// Rotate Remover
		transform.eulerAngles = v3_ItemPickupAngle;
		itemGroupTransRef.localEulerAngles = -v3_ItemPickupAngle;
		isFinalFlying = true;

//		StartCoroutine (ItemHeight_Check ());
//		moveLength = Vector2.Distance (transform.position, v3_ItemPickup_TargetFinal);
//		itemShootEndTime = 1.36F * (moveLength / 16.5F );
//		StartCoroutine (enemyMoverItemHolder.DestroyMe_RemoveMe_TheSequel (itemShootEndTime));
//		animItemRemover [animItemRemover.clip.name].speed = (v3_ItemPickupAngle.z / 40) - 0.75F;
	}

	public void ShootItemPickUp_ForBarrelQ () {
		ItemRemoverSetup (4);

		v3_ItemPickupAngle.z = Mathf.Atan2 ((v3_ItemPickupTarget_LeftFar_Barrel.y - transform.position.y)
			, (v3_ItemPickupTarget_LeftFar_Barrel.x - transform.position.x)) * Mathf.Rad2Deg;

		// Rotate Remover
		transform.eulerAngles = v3_ItemPickupAngle;
		itemGroupTransRef.localEulerAngles = -v3_ItemPickupAngle;
		isFinalFlying = true;
	}

	public void ShootItemPickup_QuickEffectCall () {
		enemyMoverItemHolder.ItemEffect_StopAnimsOnly ();
		Anim_PrepareToDestroy_EffectQuick ();

		// Delayed Item Effect is in the anim
//		StartCoroutine (ShootItemPickup_QuickEffectCallRoutine ());
	}

	public IEnumerator ShootItemPickup_QuickEffectCallRoutine () {
		yield return new WaitForSeconds (0.3F);
		ToMover_ItemEffect ();
	}

//	public IEnumerator ItemHeight_Check () {
//		Debug.LogError ("Y Posit = " + itemGroupTransRef.position.y);
//		yield return new WaitForSeconds (0.1F);
//		if (itemGroupTransRef.position.y > 4) {
//			enemyMoverItemHolder.DestroyMe_RemoveMe_TheSequel ();
//			enemyMoverItemHolder.ItemEffect_Actual ();
//			Debug.LogError ("NOW!");
//		} else {
//			StartCoroutine (ItemHeight_Check ());
//		}
//	}

	public void ToMover_ParticleParentChange () {
		transQuickEffectParticlesHolder.SetParent (transform.root);
		transQuickEffectParticlesHolder.localScale = Vector3.one;
		Destroy (transQuickEffectParticlesHolder.gameObject, 1);
		ToMover_ItemEffect ();
	}

	public void ToMover_ItemEffect () {
		enemyMoverItemHolder.ItemEffect_Actual ();
	}

	public void ToMover_DestroyMe () {
//		Debug.LogError ("YES!?");
		enemyMoverItemHolder.DestroyMe_Now ();
	}

	public void Anim_PrepareToDestroy () {
		animItemRemover.Play ("Enemy Item Removed FLOAT END Anim 1 (Legacy)");
	}

	public void Anim_PrepareToDestroy_BarrelsHedgehog () {
		PlayerData_InGame.Instance.PlayerItem_ActivateConsume (4, PlayerData_InGame.Instance.P1_Barrel_Q_HedgehogDamage);
		animItemRemover.Play ("Enemy Item Removed FLOAT END Barrel Anim 1 (Legacy)");
	}

	public void Anim_PrepareToDestroy_EffectQuick () {
		animItemRemover.Play ("Enemy Item Removed EFFECT QUICK Anim 1 (Legacy) 1");
	}

	public void Destroy_ThisItemNow_BarrelsHedgehog () {
		Destroy (this.gameObject);
	}

	public bool ItemPosition_Check () {
		if (isGoingUp) {
			return (itemGroupTransRef.position.y > itemFinalHeight);
		} 

		else {
			return (itemGroupTransRef.position.y <= itemFinalDepth);
		}
	}

	void FixedUpdate() {
		if (isFinalFlying) {
			if (ItemPosition_Check()) {
//				enemyMoverItemHolder.DestroyMe_RemoveMe_TheSequel ();
				isFinalFlying = false;
				if (!PlayerData_InGame.Instance.p1_isDead) {
					if (isThisFromBarrel) {
						Anim_PrepareToDestroy_BarrelsHedgehog ();
					} else {
						ToMover_ItemEffect ();
						Anim_PrepareToDestroy ();
					}
				} 

				// Player is dead before flying item reaches item
				else {
					Destroy (enemyMoverItemHolder.gameObject);
				}
			}
		}
	}
}
