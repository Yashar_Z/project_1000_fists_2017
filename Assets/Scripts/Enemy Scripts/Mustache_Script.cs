﻿using UnityEngine;
using System.Collections;

public class Mustache_Script : MonoBehaviour {

	public int mustacheAmount;
	public Transform mustacheTargetTransHolder;
	public ParticleSystem mustacheParticleHolder;
	public ParticleSystem mustacheParticleExplodeHolder;
	public Animation mustacheAnimHolder;
	public Vector3 v3_MustacheTarget;
	public Vector3 mustacheAngle;

	// Use this for initialization
	void Start () {
//		v3_MustacheTarget = mustacheTargetTransHolder.position;
		v3_MustacheTarget = new Vector3 (9.8F, 4.5F, 0);
		mustacheAngle = Vector3.zero;
	}

	void MustacheMoveStart () {
		mustacheAnimHolder.Play ();
//		Debug.LogError ("MUSTACHE!2");
	}

//	public void Mustache_LayerSet () {
//		mustacheParticleHolder.GetComponent <ParticleSystemRenderer> ().sortingLayerName = "UI";
//		mustacheParticleExplodeHolder.GetComponent <ParticleSystemRenderer> ().sortingLayerName = "UI";
//	}

	public void Mustache_Colorize (Color newColor) {
		mustacheParticleHolder.startColor = newColor;
		mustacheParticleExplodeHolder.startColor = newColor;
	}

	public void ShootMustache (int newMustache) {
		mustacheAmount = newMustache;
		mustacheAngle.z = Mathf.Atan2 ((v3_MustacheTarget.y - transform.position.y)
			, (v3_MustacheTarget.x - transform.position.x)) * Mathf.Rad2Deg;
		transform.eulerAngles = mustacheAngle;
		mustacheParticleHolder.Play ();
		Invoke ("MustacheMoveStart", 0.01F);

		// Update stats for mustache (Old Place)
//		PlayerData_InGame.Instance.stats_MustacheGained += newMustache;
	}

	public void ShootMustache_Awards (int newMustache) {
		mustacheAmount = newMustache;
		transform.eulerAngles = new Vector3 (0, 0, 13);
		mustacheParticleHolder.Play ();
		Invoke ("MustacheMoveStart", 0.01F);

		// Update stats for mustache
//		PlayerData_InGame.Instance.stats_MustacheGained += newMustache;
	}

	public void EndOfMustacheAnim () {
		if (PlayerData_InGame.Instance != null) {
			PlayerData_InGame.Instance.PlayerMustache_Gain (mustacheAmount);
		} else {
			PlayerData_Main.Instance.Player_Mustache_Add (mustacheAmount);
		}

		Destroy (this.gameObject);
	}
}
