﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System.Text;
using UnityEngine;
using System;
using System.Collections;

public class PlayGamesScript : MonoBehaviour
{

    public static PlayGamesScript Instance { get; private set; }


    void Start()
    {
        Instance = this;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

		// Auto sign in in case it was previously successful (Removed from here, moved to end of loading)
    }

	public void AutoSignIn () {
		if (PlayerData_Main.Instance.player_Google_PreviouslySignedIn) {
			SignIn ();
		}
	}

    public void testUnlockAchivement()
    {
        UnlockAchievement(GPGSIds.achievement);
    }

	public void GooglePrevAndIcon (bool isActive) {
		StartCoroutine (GooglePrevAndIconROUTINE (isActive));
	}

	public IEnumerator GooglePrevAndIconROUTINE (bool isActive) {
		yield return new WaitForSeconds (0.2F);
		PlayerData_Main.Instance.player_Google_PreviouslySignedIn = isActive;

		// To enable button for google play
		if (PlayerData_InGame.Instance == null) {
			while (Phone_Stats_Script.Instance == null) {
				yield return new WaitForSeconds (0.3F);
			}
			Phone_Stats_Script.Instance.menuBackAnimsScriptHolder.popUpSettingsScriptHolder.objGooglePlayObjHolder_ON.SetActive (isActive);
		}
	}

	public void SignOut () {
		PlayGamesPlatform.Instance.SignOut ();

		GooglePrevAndIcon (false);
	}

    public void SignIn()
    {
        //when authentication process is done (successfuly or not), we load cloud data
		Social.localUser.Authenticate ((success) => {
			if (success) {
//				Debug.Log ("sign in successful");
//                Moshtan_Utilties_Script.ShowToast("sign in successful");
				StartCoroutine (CheckAndUnlockAllAchievements ());
				AddScoreToLeaderboard (GPGSIds.leaderboard, PlayerData_Main.Instance.player_Score_Record);

				GooglePrevAndIcon (true);
			} 

			else {
//				Debug.Log ("sign in fail");
//                Moshtan_Utilties_Script.ShowToast("sign in fail");

				GooglePrevAndIcon (false);
			}

		});
    }


    #region Achievements
    public void UnlockAchievement(string id)
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGameService.Instance.UnlockAchievement(GetArioId(id));
        }
        else
        {
            Social.ReportProgress(id, 100, success =>
            {
                //			Moshtan_Utilties_Script.ShowToast ("achievement unlocked");
            });
        }
    }

    private string GetArioId(string id)
    {
        switch (id)
        {
            case GPGSIds.achievement:
                return "84";
            case GPGSIds.achievement_2:
                return "85";
            default:
                return "-1";
        }
    }

    public void IncrementAchievement(string id, int stepsToIncrement)
    {
		PlayGamesPlatform.Instance.IncrementAchievement (id, stepsToIncrement, success => {
//			Moshtan_Utilties_Script.ShowToast ("achievement incremented");
		});
    }

    public void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }


    IEnumerator CheckAndUnlockAllAchievements()
    {
        foreach (Achievement achievement in AchievementManager.Instance.AchievementsArr)
        {
            yield return null;
            if (achievement.completed)
            {
                switch (achievement.AchievementID)
                {
                    case 100:
                        UnlockAchievement(GPGSIds.achievement);
                        break;
                    case 101:
                        UnlockAchievement(GPGSIds.achievement_2);
                        break;
                    case 102:
                        UnlockAchievement(GPGSIds.achievement_3);
                        break;
                    case 103:
                        UnlockAchievement(GPGSIds.achievement_4);
                        break;
                    case 104:
                        UnlockAchievement(GPGSIds.achievement_5);
                        break;
                    case 105:
                        UnlockAchievement(GPGSIds.achievement_6);
                        break;
                    case 106:
                        UnlockAchievement(GPGSIds.achievement_7);
                        break;
                    case 107:
                        UnlockAchievement(GPGSIds.achievement_8);
                        break;
                    case 108:
                        UnlockAchievement(GPGSIds.achievement_9);
                        break;
                    case 109:
                        UnlockAchievement(GPGSIds.achievement_10);
                        break;
                    case 110:
                        UnlockAchievement(GPGSIds.achievement_11);
                        break;
                    case 111:
                        UnlockAchievement(GPGSIds.achievement_12);
                        break;
                    case 112:
                        UnlockAchievement(GPGSIds.achievement_13);
                        break;
                    case 113:
                        UnlockAchievement(GPGSIds.achievement_14);
                        break;
                    case 114:
                        UnlockAchievement(GPGSIds.achievement_15);
                        break;
                    case 115:
                        UnlockAchievement(GPGSIds.achievement_16);
                        break;
                    case 116:
                        UnlockAchievement(GPGSIds.achievement_17);
                        break;
                    case 117:
                        UnlockAchievement(GPGSIds.achievement_18);
                        break;
                    case 118:
                        UnlockAchievement(GPGSIds.achievement_19);
                        break;
                    case 119:
                        UnlockAchievement(GPGSIds.achievement_20);
                        break;
                    case 120:
                        UnlockAchievement(GPGSIds.achievement_21);
                        break;
                    case 121:
                        UnlockAchievement(GPGSIds.achievement_22);
                        break;
                    case 122:
                        UnlockAchievement(GPGSIds.achievement_23);
                        break;
                    case 123:
                        UnlockAchievement(GPGSIds.achievement_24);
                        break;
                    case 124:
                        UnlockAchievement(GPGSIds.achievement_25);
                        break;
                    case 125:
                        UnlockAchievement(GPGSIds.achievement_26);
                        break;
                    case 126:
                        UnlockAchievement(GPGSIds.achievement_27);
                        break;
                    case 127:
                        UnlockAchievement(GPGSIds.achievement_28);
                        break;
                    case 128:
                        UnlockAchievement(GPGSIds.achievement_29);
                        break;
                    case 129:
                        UnlockAchievement(GPGSIds.achievement_30);
                        break;
                    case 130:
                        UnlockAchievement(GPGSIds.achievement_31);
                        break;
                    case 131:
                        UnlockAchievement(GPGSIds.achievement_32);
                        break;
                    case 132:
                        UnlockAchievement(GPGSIds.achievement_33);
                        break;
                    case 133:
                        UnlockAchievement(GPGSIds.achievement_34);
                        break;
                    case 134:
                        UnlockAchievement(GPGSIds.achievement_35);
                        break;
                    case 135:
                        UnlockAchievement(GPGSIds.achievement_36);
                        break;
                    case 136:
                        UnlockAchievement(GPGSIds.achievement_37);
                        break;
                    case 137:
                        UnlockAchievement(GPGSIds.achievement_38);
                        break;
                    case 138:
                        UnlockAchievement(GPGSIds.achievement_39);
                        break;
                    case 139:
                        UnlockAchievement(GPGSIds.achievement_40);
                        break;
                    case 140:
                        UnlockAchievement(GPGSIds.achievement_41);
                        break;
                    case 141:
                        UnlockAchievement(GPGSIds.achievement_42);
                        break;
                    case 142:
                        UnlockAchievement(GPGSIds.achievement_43);
                        break;
                    case 143:
                        UnlockAchievement(GPGSIds.achievement_44);
                        break;
                    case 144:
                        UnlockAchievement(GPGSIds.achievement_45);
                        break;
                    case 145:
                        UnlockAchievement(GPGSIds.achievement_46);
                        break;
                    case 146:
                        UnlockAchievement(GPGSIds.achievement_47);
                        break;
                    case 147:
                        UnlockAchievement(GPGSIds.achievement_48);
                        break;
                    case 148:
                        UnlockAchievement(GPGSIds.achievement_49);
                        break;
                    case 149:
                        UnlockAchievement(GPGSIds.achievement_50);
                        break;
                    case 150:
                        UnlockAchievement(GPGSIds.achievement_51);
                        break;
                    case 151:
                        UnlockAchievement(GPGSIds.achievement_52);
                        break;
                    case 152:
                        UnlockAchievement(GPGSIds.achievement_53);
                        break;
                    case 153:
                        UnlockAchievement(GPGSIds.achievement_54);
                        break;
                    case 154:
                        UnlockAchievement(GPGSIds.achievement_55);
                        break;
                    case 155:
                        UnlockAchievement(GPGSIds.achievement_56);
                        break;
                    case 156:
                        UnlockAchievement(GPGSIds.achievement_57);
                        break;
                    case 157:
                        UnlockAchievement(GPGSIds.achievement_58);
                        break;
                    case 158:
                        UnlockAchievement(GPGSIds.achievement_59);
                        break;
                    case 159:
                        UnlockAchievement(GPGSIds.achievement_60);
                        break;
                    case 160:
                        UnlockAchievement(GPGSIds.achievement_61);
                        break;
                    case 161:
                        UnlockAchievement(GPGSIds.achievement_62);
                        break;
                    case 162:
                        UnlockAchievement(GPGSIds.achievement_63);
                        break;
                    case 163:
                        UnlockAchievement(GPGSIds.achievement_64);
                        break;
                    case 164:
                        UnlockAchievement(GPGSIds.achievement_65);
                        break;
                    case 165:
                        UnlockAchievement(GPGSIds.achievement_66);
                        break;
                    case 166:
                        UnlockAchievement(GPGSIds.achievement_67);
                        break;
                    case 167:
                        UnlockAchievement(GPGSIds.achievement_68);
                        break;
                    case 168:
                        UnlockAchievement(GPGSIds.achievement_69);
                        break;
                    case 169:
                        UnlockAchievement(GPGSIds.achievement_70);
                        break;
                    case 170:
                        UnlockAchievement(GPGSIds.achievement_71);
                        break;
                    case 171:
                        UnlockAchievement(GPGSIds.achievement_72);
                        break;
                    case 172:
                        UnlockAchievement(GPGSIds.achievement_73);
                        break;
                    default:
                        Debug.LogError("default switch");
                        break;
                }
            }
        }
    }
    #endregion /Achievements

    #region Leaderboards
    public void AddScoreToLeaderboard(string leaderboardId, long score)
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGameService.Instance.SubmitScoreToLeaderboard("35", score);
        }
        else
        {
            Social.ReportScore(score, leaderboardId, (bool success) =>
                {
                    if (success)
                    {
                        Debug.Log("score posted to leader board " + score);
                    }
                });
        }
    }

    public void ShowLeaderboardsUI()
    {
        //Social.ShowLeaderboardUI();

        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard);

    }
    #endregion /Leaderboards

}