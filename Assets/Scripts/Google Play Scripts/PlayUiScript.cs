﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class PlayUiScript : MonoBehaviour {

	public void Pressed_SignIn()
    {
		if (PlayGamesPlatform.Instance.IsAuthenticated ()) {
			PlayGamesScript.Instance.SignOut ();
		}
        else
            PlayGamesScript.Instance.SignIn();

    }

    public void Pressed_ShowAchievementsUI()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGameService.Instance.ShowAllAchievements();
        }
        else
        {
            if (PlayGamesPlatform.Instance.IsAuthenticated())
                PlayGamesScript.Instance.ShowAchievementsUI();
            else
                PlayGamesScript.Instance.SignIn();
        }
//            Social.localUser.Authenticate((bool success) => {
//                PlayGamesScript.Instance.ShowAchievementsUI();
//            });
        
    }

    public void Pressed_ShowEndlessLeaderboard()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGameService.Instance.ShowLeaderboard("35");
        }
        else
        {
            if (PlayGamesPlatform.Instance.IsAuthenticated())
                PlayGamesScript.Instance.ShowLeaderboardsUI();
            else
                PlayGamesScript.Instance.SignIn();
        }

        //			Social.localUser.Authenticate((bool success) => {
        //				PlayGamesScript.Instance.ShowLeaderboardsUI();
        //			});
    }
}
