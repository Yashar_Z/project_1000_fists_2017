﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

//
// Sample scene which uses Ario in-app purchase sdk
// please note that it's important to implement callbacks 
// and register them before calling ArioGameService.Instance methods
public class ArioGamesIapManager : MonoBehaviour
{
    public static System.Action<string> OnArioPurchaseSuccessful;
    public static System.Action OnArioPurchaseCancelled;

    private void OnEnable()
    {
        // Registering callback methods
        ArioInAppPurchase.Instance.OnGetProductsInfo += OnGetProductInfo;
        ArioInAppPurchase.Instance.OnConsumeFailed += OnConsumeFailed;
        ArioInAppPurchase.Instance.OnConsumeSucceed += OnConsumeSucceed;
        ArioInAppPurchase.Instance.OnPurchaseFailed += OnPurchaseFailed;
        ArioInAppPurchase.Instance.OnPurchseSucceed += OnPurchasedSucceed;
    }
    private void OnDisable()
    {
        ArioInAppPurchase.Instance.OnGetProductsInfo -= OnGetProductInfo;
        ArioInAppPurchase.Instance.OnConsumeFailed -= OnConsumeFailed;
        ArioInAppPurchase.Instance.OnConsumeSucceed -= OnConsumeSucceed;
        ArioInAppPurchase.Instance.OnPurchaseFailed -= OnPurchaseFailed;
        ArioInAppPurchase.Instance.OnPurchseSucceed -= OnPurchasedSucceed;
    }
    void Start()
    {

        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGameService.Instance.init(
            /*ENTER_YOUR_ARIO_APP_ID=*/ "1377",
            /*ENTER_YOUR_ARIO_APP_SECRET_KEY=*/ "01d2e8e1-4a27-4236-bacc-bdfd05ab0cf2"
        );

		ArioInAppPurchase.Instance.init(
            /* ENTER_YOUR_ARIO_APP_RSA_KEY */
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0La3AC8CuggCi4XPwQz9Kk7zDggxAmS9NjNKaZwY6j02Pj8kOvI4dfopyJ2LYH+gxLJEx16P2WvHCHnYFNgW2COKe3uqDetAyRhql5iKo+bujtwWUmxj918PG5ctE4HwxXXRXmY6bKwYwie8dsjUbQN1fkNNzm8oMqTM7Nui3gQIDAQAB");

            ArioInAppPurchase.Instance.SetSKUList(new List<string>() { "sibil_0_pack", "sibil_1_pack", "sibil_2_pack", "sibil_3_pack", "sibil_4_pack" });

            if (ArioGameService.Instance.IsStorePackageInstalled())
            {
                Debug.Log("ario store store package is installed");
            }
        }
    }


    public void OnPurchaseFailed(string error)
    {
        if (OnArioPurchaseCancelled != null)
        {
            OnArioPurchaseCancelled();
        }
        Debug.Log("ARIO/MARKETS Purachse failed \n" + error);
    }

    public void OnPurchasedSucceed(string sku, string token)
    {
        Debug.Log("ARIO/MARKETS Purchasing item with sku " + sku + " succeed with token " + token);
        ArioInAppPurchase.Instance.Consume();
        Debug.Log("ARIO/MARKETS Consumed called");
    }

    public void OnConsumeFailed(string error)
    {
        Debug.Log("ARIO/MARKETS Consume failed \n" + error);
    }

    public void OnConsumeSucceed(string sku, string token)
    {
        if (OnArioPurchaseSuccessful!= null)
        {
            OnArioPurchaseSuccessful(sku);
        }
        Debug.Log("ARIO/MARKETS Consuming item with SKU " + sku + " succeeded with token " + token);
    }

    public static void PurchaseSIBIL_0_PACK()
    {
        ArioInAppPurchase.Instance.Purchase(CafeBazaarIapManager.SIBIL_0_PACK);
    }
    public static void PurchaseSIBIL_1_PACK()
    {
        ArioInAppPurchase.Instance.Purchase(CafeBazaarIapManager.SIBIL_1_PACK);
    }
    public static void PurchaseSIBIL_2_PACK()
    {
        ArioInAppPurchase.Instance.Purchase(CafeBazaarIapManager.SIBIL_2_PACK);
    }
    public static void PurchaseSIBIL_3_PACK()
    {
        ArioInAppPurchase.Instance.Purchase(CafeBazaarIapManager.SIBIL_3_PACK);
    }
    public static void PurchaseSIBIL_4_PACK()
    {
        ArioInAppPurchase.Instance.Purchase(CafeBazaarIapManager.SIBIL_4_PACK);
    }

    public void OnGetProductInfo(string info)
    {
        Debug.Log("Unity Ario -> The returned info is: " + info);
    }

    public void OnInitializeArio()
    {
        ArioInAppPurchase.Instance.QueryInventory();
    }

    public void GetPackageName()
    {
        Debug.Log("ARIO/MARKETS PackageName" + ArioInAppPurchase.Instance.GetGamePackage());
    }

    public void IsStoreInstalled()
    {
        Debug.Log("ARIO/MARKETS");
        if (ArioGameService.Instance.IsStorePackageInstalled())
            Debug.Log("ARIO/MARKETS PackageName The Store is Installed ");
        else
            Debug.Log("ARIO/MARKETS PackageNameThe Store is Not Installed");
    }
}
