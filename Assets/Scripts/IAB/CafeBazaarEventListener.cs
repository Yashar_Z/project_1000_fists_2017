﻿using UnityEngine;
using System.Collections.Generic;
using BazaarPlugin;


public class CafeBazaarEventListener : MonoBehaviour
{
    public static System.Action<string> OnPurchaseSuccessful;
    public static System.Action OnPurchaseCancelled;

#if UNITY_ANDROID

    void OnEnable()
    {
        // Listen to all events for illustration purposes
        IABEventManager.billingSupportedEvent += billingSupportedEvent;
        IABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
        IABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
        IABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
        IABEventManager.querySkuDetailsSucceededEvent += querySkuDetailsSucceededEvent;
        IABEventManager.querySkuDetailsFailedEvent += querySkuDetailsFailedEvent;
        IABEventManager.queryPurchasesSucceededEvent += queryPurchasesSucceededEvent;
        IABEventManager.queryPurchasesFailedEvent += queryPurchasesFailedEvent;
        IABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
        IABEventManager.purchaseFailedEvent += purchaseFailedEvent;
        IABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
        IABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
    }

    void OnDisable()
    {
        // Remove all event handlers
        IABEventManager.billingSupportedEvent -= billingSupportedEvent;
        IABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
        IABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
        IABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
        IABEventManager.querySkuDetailsSucceededEvent -= querySkuDetailsSucceededEvent;
        IABEventManager.querySkuDetailsFailedEvent -= querySkuDetailsFailedEvent;
        IABEventManager.queryPurchasesSucceededEvent -= queryPurchasesSucceededEvent;
        IABEventManager.queryPurchasesFailedEvent -= queryPurchasesFailedEvent;
        IABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
        IABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
        IABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
        IABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
    }


    void billingSupportedEvent()
    {
        Debug.Log("billingSupportedEvent");
        // check for purchase
        BazaarIAB.queryPurchases();
    }

    void billingNotSupportedEvent(string error)
    {
        Debug.Log("billingNotSupportedEvent: " + error);
    }

    void queryInventorySucceededEvent(List<BazaarPurchase> purchases, List<BazaarSkuInfo> skus)
    {
        Debug.Log(string.Format("queryInventorySucceededEvent. total purchases: {0}, total skus: {1}", purchases.Count, skus.Count));

        for (int i = 0; i < purchases.Count; ++i)
        {
            Debug.Log(purchases[i].ToString());
        }

        Debug.Log("-----------------------------");

        for (int i = 0; i < skus.Count; ++i)
        {
            Debug.Log(skus[i].ToString());
        }
    }

    void queryInventoryFailedEvent(string error)
    {
        Debug.Log("queryInventoryFailedEvent: " + error);
    }

    private void querySkuDetailsSucceededEvent(List<BazaarSkuInfo> skus)
    {
        Debug.Log(string.Format("querySkuDetailsSucceededEvent. total skus: {0}", skus.Count));

        for (int i = 0; i < skus.Count; ++i)
        {
            Debug.Log(skus[i].ToString());
        }
    }

    private void querySkuDetailsFailedEvent(string error)
    {
        Debug.Log("querySkuDetailsFailedEvent: " + error);
    }

    private void queryPurchasesSucceededEvent(List<BazaarPurchase> purchases)
    {
        Debug.Log(string.Format("queryPurchasesSucceededEvent. total purchases: {0}", purchases.Count));
    }

    private void queryPurchasesFailedEvent(string error)
    {
        Debug.Log("queryPurchasesFailedEvent: " + error);
    }

    void purchaseSucceededEvent(BazaarPurchase purchase)
    {

        Debug.Log("purchaseSucceededEvent: " + purchase);
        if (purchase.DeveloperPayload == "hezarmoshtan")
        {
            switch (purchase.ProductId)
            {
                case CafeBazaarIapManager.SIBIL_0_PACK:
                    BazaarIAB.consumeProduct(CafeBazaarIapManager.SIBIL_0_PACK);
                    break;
                case CafeBazaarIapManager.SIBIL_1_PACK:
                    BazaarIAB.consumeProduct(CafeBazaarIapManager.SIBIL_1_PACK);
                    break;
                case CafeBazaarIapManager.SIBIL_2_PACK:
                    BazaarIAB.consumeProduct(CafeBazaarIapManager.SIBIL_2_PACK);
                    break;
                case CafeBazaarIapManager.SIBIL_3_PACK:
                    BazaarIAB.consumeProduct(CafeBazaarIapManager.SIBIL_3_PACK);
                    break;
                case CafeBazaarIapManager.SIBIL_4_PACK:
                    BazaarIAB.consumeProduct(CafeBazaarIapManager.SIBIL_4_PACK);
                    break;
                default:
                    Debug.LogError("Default Error");
                    break;
            }
        }
    }

    void purchaseFailedEvent(string error)
    {
        if (OnPurchaseCancelled != null)
        {
            OnPurchaseCancelled();
        }
        Debug.Log("purchaseFailedEvent: " + error);
    }

    void consumePurchaseSucceededEvent(BazaarPurchase purchase)
    {
        if (purchase.DeveloperPayload == "hezarmoshtan")
        {
            if (OnPurchaseSuccessful != null)
            {
                OnPurchaseSuccessful(purchase.ProductId);
            }
    }
    Debug.Log("consumePurchaseSucceededEvent: " + purchase);
    }

    void consumePurchaseFailedEvent(string error)
    {
        if (OnPurchaseCancelled != null)
        {
            OnPurchaseCancelled();
        }
        Debug.Log("consumePurchaseFailedEvent: " + error);
    }

#endif

}