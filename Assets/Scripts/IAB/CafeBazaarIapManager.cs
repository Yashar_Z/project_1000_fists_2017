﻿using BazaarPlugin;
using UnityEngine;

public class CafeBazaarIapManager : MonoBehaviour
{

    void OnEnable()
    {
        var key = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCZyrV4fyODu2NWmsASTnhjG3jwipp/r1fCfseKQXOYGdXw+tU8oVqQCm9wRtggn69i5nDYhhPrlC9pUV8mJDy2naoDOCTdyynpiW4aAOj7whiSpGYcLyISkGVrPCDEBWisCERlTgo5iAPwXRM1tkk4JjcTAw54TZHtQ0gHVIsUpxjG73QFDzJkBwc6eOYMvL9qjXyI3m1f58+URTzrM/oLZJ6iFTemmoFXbFDyfOECAwEAAQ==";
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            BazaarIAB.init(key);
            BazaarIAB.enableLogging(true);
        }     
    }

    public const string SIBIL_0_PACK = "sibil_0_pack";
    public const string SIBIL_1_PACK = "sibil_1_pack";
    public const string SIBIL_2_PACK = "sibil_2_pack";
    public const string SIBIL_3_PACK = "sibil_3_pack";
    public const string SIBIL_4_PACK = "sibil_4_pack";

    string[] skus = { SIBIL_0_PACK, SIBIL_1_PACK,SIBIL_2_PACK, SIBIL_3_PACK, SIBIL_4_PACK };

    public static void PurchaseSIBIL_0_PACK()
    {
        BazaarIAB.purchaseProduct(SIBIL_0_PACK, "hezarmoshtan");
    }
    public static void PurchaseSIBIL_1_PACK()
    {
        BazaarIAB.purchaseProduct(SIBIL_1_PACK, "hezarmoshtan");
    }
    public static void PurchaseSIBIL_2_PACK()
    {
        BazaarIAB.purchaseProduct(SIBIL_2_PACK, "hezarmoshtan");
    }
    public static void PurchaseSIBIL_3_PACK()
    {
        BazaarIAB.purchaseProduct(SIBIL_3_PACK, "hezarmoshtan");
    }
    public static void PurchaseSIBIL_4_PACK()
    {
        BazaarIAB.purchaseProduct(SIBIL_4_PACK, "hezarmoshtan");
    }
}
