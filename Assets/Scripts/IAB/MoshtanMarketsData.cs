﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class MoshtanMarketsData : MonoBehaviour {

    bool isDone = false;
    public RestUtil rest;

    public List< PackageResult> marketResult;

    private void Start()
    {
        //rest = GetComponent<RestUtil>();
		WWW w = rest.GET("http://apis.1000moshtan.com/api/HezarMoshtanMarket", () => { isDone = true; });
//      WWW w = rest.GET("http://hezarmoshtan.appete.mobi/api/HezarMoshtanMarket", () => { isDone = true; });

        //StartCoroutine(Amaliat());
    }
    public IEnumerator Amaliat(System.Action OnComplete)
    {
        while (!isDone)
            yield return null;

        Debug.Log(rest.Results);

        var jsonData = JSON.Parse(rest.Results);

        //string LevelId = jsonData.AsObject["LevelId"].Value;
        //string LevelName = jsonData.AsObject["LevelName"].Value;
        //string LevelData = jsonData.AsObject["LevelData"].Value;

        marketResult = new List<PackageResult>();

        for (int i = 0; i < jsonData.Count; i++)
        {
            var packageResult = new PackageResult()
            {
                id = int.Parse(jsonData[i].AsObject["id"].Value),
                cafebazaar_reward = int.Parse(jsonData[i].AsObject["cafebazaar_reward"].Value),
                cafebazaar_package_name = jsonData[i].AsObject["cafebazaar_package_name"].Value,
                cafebazaar_normal_price = int.Parse(jsonData[i].AsObject["cafebazaar_normal_price"].Value),
                cafebazaar_has_discount = bool.Parse(jsonData[i].AsObject["cafebazaar_has_discount"].Value),
                cafebazaar_discount_price = int.Parse(jsonData[i].AsObject["cafebazaar_discount_price"].Value),

                myket_reward = int.Parse(jsonData[i].AsObject["myket_reward"].Value),
                myket_package_name = jsonData[i].AsObject["myket_package_name"].Value,
                myket_normal_price = int.Parse(jsonData[i].AsObject["myket_normal_price"].Value),
                myket_has_discount = bool.Parse(jsonData[i].AsObject["myket_has_discount"].Value),
                myket_discount_price = int.Parse(jsonData[i].AsObject["myket_discount_price"].Value),

//				vas_reward = int.Parse(jsonData[i].AsObject["vas_reward"].Value),
//				vas_package_name = jsonData[i].AsObject["vas_package_name"].Value,
//				vas_normal_price = int.Parse(jsonData[i].AsObject["vas_normal_price"].Value),
//				vas_has_discount = bool.Parse(jsonData[i].AsObject["vas_has_discount"].Value),
//				vas_discount_price = int.Parse(jsonData[i].AsObject["vas_discount_price"].Value),
            };
            marketResult.Add(packageResult);
        }
        if (OnComplete!=null)
        {
            OnComplete();
        }
    }
}

public class PackageResult
{
    public int id;
    public int cafebazaar_reward;
    public string cafebazaar_package_name;
    public int cafebazaar_normal_price;
    public bool cafebazaar_has_discount;
    public int cafebazaar_discount_price;

    public int myket_reward;
    public string myket_package_name;
    public int myket_normal_price;
    public bool myket_has_discount;
    public int myket_discount_price;

//	public int vas_reward;
//	public string vas_package_name;
//	public int vas_normal_price;
//	public bool vas_has_discount;
//	public int vas_discount_price;

}