﻿//using UnityEngine;
//using System.Collections;
//using Soomla.Store;

//public class MoshtanStoreAssets : IStoreAssets
//{

//    public int GetVersion()
//    {
//        return 2;
//    }
//    public VirtualCurrency[] GetCurrencies()
//    {
//        return new VirtualCurrency[] { SIBIL_CURRENCY };
//    }
//    public VirtualGood[] GetGoods()
//    {
//        return new VirtualGood[] {
//                POWER_UP_1_UNLOCK_LTVG ,
//                POWER_UP_1_UNLOCK_UPGRADE_1,
//                POWER_UP_1_UNLOCK_UPGRADE_2,
//                POWER_UP_1_UNLOCK_UPGRADE_3,
//                POWER_UP_1_UNLOCK_UPGRADE_4,
//                POWER_UP_2_UNLOCK_LTVG,
//                POWER_UP_2_UNLOCK_UPGRADE_1,
//                POWER_UP_2_UNLOCK_UPGRADE_2,
//                POWER_UP_2_UNLOCK_UPGRADE_3,
//                POWER_UP_2_UNLOCK_UPGRADE_4,
//                CHAPTER_2_UNLOCK_LTVG,
//                CHAPTER_3_UNLOCK_LTVG,
//                CHAPTER_4_UNLOCK_LTVG,
//                SILVER_CHEST_GOOD,
//                GOLD_CHEST_GOOD,
//                UNIQUE_CHEST_GOOD,
//                LEVEL_CONTINUE_1_GOOD,
//                LEVEL_CONTINUE_2_GOOD,
//                LEVEL_CONTINUE_3_GOOD,
//                POWER_UP_RECHARGE_5_GOOD,
//                POWER_UP_RECHARGE_4_GOOD,
//                POWER_UP_RECHARGE_3_GOOD,
//                POWER_UP_RECHARGE_2_GOOD,
//                POWER_UP_RECHARGE_1_GOOD
//            };
//    }
//    public VirtualCurrencyPack[] GetCurrencyPacks()
//    {
//        return new VirtualCurrencyPack[] {
//                SIBIL_0_PACK,
//                SIBIL_1_PACK,
//                SIBIL_2_PACK,
//                SIBIL_3_PACK,
//                SIBIL_4_PACK,
//                SIBIL_5_PACK
//            };
//    }
//    public VirtualCategory[] GetCategories()
//    {
//        return new VirtualCategory[] { };
//    }

//    #region constant fields declaration
//    public const string SIBIL_CURRENCY_ITEM_ID = "sibil_currency";
//    public const string POWER_UP_1_UNLOCK_ITEM_ID = "power_up_1_unlock";
//    public const string POWER_UP_1_UNLOCK_UPGRADE_1_ITEM_ID = "power_up_1_unlock_upgrade_1";
//    public const string POWER_UP_1_UNLOCK_UPGRADE_2_ITEM_ID = "power_up_1_unlock_upgrade_2";
//    public const string POWER_UP_1_UNLOCK_UPGRADE_3_ITEM_ID = "power_up_1_unlock_upgrade_3";
//    public const string POWER_UP_1_UNLOCK_UPGRADE_4_ITEM_ID = "power_up_1_unlock_upgrade_4";
//    public const string POWER_UP_2_UNLOCK_ITEM_ID = "power_up_2_unlock";
//    public const string POWER_UP_2_UNLOCK_UPGRADE_1_ITEM_ID = "power_up_2_unlock_upgrade_1";
//    public const string POWER_UP_2_UNLOCK_UPGRADE_2_ITEM_ID = "power_up_2_unlock_upgrade_2";
//    public const string POWER_UP_2_UNLOCK_UPGRADE_3_ITEM_ID = "power_up_2_unlock_upgrade_3";
//    public const string POWER_UP_2_UNLOCK_UPGRADE_4_ITEM_ID = "power_up_2_unlock_upgrade_4";
//    public const string CHAPTER_2_UNLOCK_ITEM_ID = "chapter_2_unlock";
//    public const string CHAPTER_3_UNLOCK_ITEM_ID = "chapter_3_unlock";
//    public const string CHAPTER_4_UNLOCK_ITEM_ID = "chapter_4_unlock";
//    public const string SILVER_CHEST_ITEM_ID = "silver_chest";
//    public const string GOLD_CHEST_ITEM_ID = "gold_chest";
//    public const string UNIQUE_CHEST_ITEM_ID = "unique_chest";
//    public const string LEVEL_CONTINUE_1_ITEM_ID = "first_continue_of_level";
//    public const string LEVEL_CONTINUE_2_ITEM_ID = "second_continue_of_level";
//    public const string LEVEL_CONTINUE_3_ITEM_ID = "third_continue_of_level";
//    public const string POWER_UP_RECHARGE_5_ITEM_ID = "5_power_up_recharge";
//    public const string POWER_UP_RECHARGE_4_ITEM_ID = "4_power_up_recharge";
//    public const string POWER_UP_RECHARGE_3_ITEM_ID = "3_power_up_recharge";
//    public const string POWER_UP_RECHARGE_2_ITEM_ID = "2_power_up_recharge";
//    public const string POWER_UP_RECHARGE_1_ITEM_ID = "1_power_up_recharge";

//    public const string SIBIL_0_PACK_ITEM_ID = "sibil_0_pack";
//    public const string SIBIL_1_PACK_ITEM_ID = "sibil_1_pack";
//    public const string SIBIL_2_PACK_ITEM_ID = "sibil_2_pack";
//    public const string SIBIL_3_PACK_ITEM_ID = "sibil_3_pack";
//    public const string SIBIL_4_PACK_ITEM_ID = "sibil_4_pack";
//    public const string SIBIL_5_PACK_ITEM_ID = "sibil_5_pack";
//    #endregion

//    #region currency packs declaration
//    public static VirtualCurrency SIBIL_CURRENCY = new VirtualCurrency(
//        "Sibil",
//        "Mard baaas dashte bashe"
//        , SIBIL_CURRENCY_ITEM_ID);

//    public static VirtualCurrencyPack SIBIL_0_PACK = new VirtualCurrencyPack(
//        "sibil_200_pack",                                   // name
//        "200 sibil",                       // description
//        SIBIL_0_PACK_ITEM_ID,                                   // item id
//        200,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_0_PACK_ITEM_ID, 1000)
//        );


//    public static VirtualCurrencyPack SIBIL_1_PACK = new VirtualCurrencyPack(
//        "sibil_500_pack",                                   // name
//        "500 sibil",                       // description
//        SIBIL_1_PACK_ITEM_ID,                                   // item id
//        500,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_1_PACK_ITEM_ID, 2500)
//        );

//    public static VirtualCurrencyPack SIBIL_2_PACK = new VirtualCurrencyPack(
//        "sibil_1000_pack",                                   // name
//        "1000 sibil",                       // description
//        SIBIL_2_PACK_ITEM_ID,                                   // item id
//        1000,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_2_PACK_ITEM_ID, 5000)
//        );

//    public static VirtualCurrencyPack SIBIL_3_PACK = new VirtualCurrencyPack(
//        "sibil_2000_pack",                                   // name
//        "2000 sibil",                       // description
//        SIBIL_3_PACK_ITEM_ID,                                   // item id
//        2000,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_3_PACK_ITEM_ID, 10000)
//        );

//    public static VirtualCurrencyPack SIBIL_4_PACK = new VirtualCurrencyPack(
//        "sibil_5000_pack",                                   // name
//        "5000 sibil",                       // description
//        SIBIL_4_PACK_ITEM_ID,                                   // item id
//        5000,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_4_PACK_ITEM_ID, 25000)
//        );

//    public static VirtualCurrencyPack SIBIL_5_PACK = new VirtualCurrencyPack(
//        "sibil_10000_pack",                                   // name
//        "10000 sibil",                       // description
//        SIBIL_5_PACK_ITEM_ID,                                   // item id
//        100000,                                             // number of currencies in the pack
//        SIBIL_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket(SIBIL_5_PACK_ITEM_ID, 50000)
//        );
//    #endregion

//    #region chests declaration

//    public static VirtualGood SILVER_CHEST_GOOD = new SingleUseVG(
//                "Silver Chest",                                                 // name
//                "chest khak bar sari ke ashghal faghat mide hehe",              // description
//                SILVER_CHEST_ITEM_ID,                                                 // item id
//                new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 50));       // the way this virtual good is purchased

//    public static VirtualGood GOLD_CHEST_GOOD = new SingleUseVG(
//            "Gold Chest",                                                 // name
//            "chest i ke momkene chiz khob dar biad azashs",              // description
//            GOLD_CHEST_ITEM_ID,                                                 // item id
//            new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 150));       // the way this virtual good is purchased

//    public static VirtualGood UNIQUE_CHEST_GOOD = new SingleUseVG(
//            "Unique Chest",                                                 // name
//            "chest i ke chiz be dard bokhor belakhare azash miad",              // description
//            UNIQUE_CHEST_ITEM_ID,                                                 // item id
//            new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 350));       // the way this virtual good is purchased
//    #endregion

//    #region level continue declaration
//    public static VirtualGood LEVEL_CONTINUE_1_GOOD = new SingleUseVG(
//        "First Level Continue ",                                                 // name
//        "avalin continue yek marhale",              // description
//        LEVEL_CONTINUE_1_ITEM_ID,                                                 // item id
//        new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 5));       // the way this virtual good is purchased

//    public static VirtualGood LEVEL_CONTINUE_2_GOOD = new SingleUseVG(
//    "Second Level Continue ",                                                 // name
//    "dovomin continue yek marhale",              // description
//    LEVEL_CONTINUE_2_ITEM_ID,                                                 // item id
//    new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 7));       // the way this virtual good is purchased

//    public static VirtualGood LEVEL_CONTINUE_3_GOOD = new SingleUseVG(
//    "Third Level Continue ",                                                 // name
//    "sevomin continue yek marhale",              // description
//    LEVEL_CONTINUE_3_ITEM_ID,                                                 // item id
//    new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 10));       // the way this virtual good is purchased
//    #endregion

//    #region chaptar unlock declarations
//    public static VirtualGood CHAPTER_2_UNLOCK_LTVG = new LifetimeVG(
//    "Chapter 2 Unlock",                                                       // name
//    "gate 2 ro baz kon ",                                                 // description
//    CHAPTER_2_UNLOCK_ITEM_ID,                                                       // item id
//    new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 200));  // the way this virtual good is purchased

//    public static VirtualGood CHAPTER_3_UNLOCK_LTVG = new LifetimeVG(
//"Chapter 3 Unlock",                                                       // name
//"gate 3 ro baz kon ",                                                 // description
//CHAPTER_3_UNLOCK_ITEM_ID,                                                       // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500));  // the way this virtual good is purchased

//    public static VirtualGood CHAPTER_4_UNLOCK_LTVG = new LifetimeVG(
//"Chapter 4 Unlock",                                                       // name
//"gate 4 ro baz kon ",                                                 // description
//CHAPTER_4_UNLOCK_ITEM_ID,                                                       // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 1000));  // the way this virtual good is purchased

//    #endregion

//    #region power up unlock declarations


//    public static VirtualGood POWER_UP_1_UNLOCK_LTVG = new LifetimeVG(
//"Power Up 1 Unlock",                                                            // Name
//"item upgrade", // Description
//POWER_UP_1_UNLOCK_ITEM_ID,                                                            // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500)               // Purchase type
//);

//    public static VirtualGood POWER_UP_1_UNLOCK_UPGRADE_1 = new UpgradeVG(
//    POWER_UP_1_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//    POWER_UP_1_UNLOCK_UPGRADE_2_ITEM_ID,         // Item ID of the next upgrade good
//    null,                         // Item ID of the previous upgrade good
//    "Power Up 1 Unlock 1",                                 // Name
//    "avalin upgrade power up 1",       // Description
//    POWER_UP_1_UNLOCK_UPGRADE_1_ITEM_ID,                                       // Item ID
//    new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500)    // Purchase type
//);

//    public static VirtualGood POWER_UP_1_UNLOCK_UPGRADE_2 = new UpgradeVG(
//POWER_UP_1_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//POWER_UP_1_UNLOCK_UPGRADE_3_ITEM_ID,         // Item ID of the next upgrade good
//POWER_UP_1_UNLOCK_UPGRADE_1_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 1 Unlock 2",                                 // Name
//"dovomin upgrade power up 1",       // Description
//POWER_UP_1_UNLOCK_UPGRADE_2_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 5000)    // Purchase type
//);
//    public static VirtualGood POWER_UP_1_UNLOCK_UPGRADE_3 = new UpgradeVG(
//POWER_UP_1_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//POWER_UP_1_UNLOCK_UPGRADE_4_ITEM_ID,         // Item ID of the next upgrade good
//POWER_UP_1_UNLOCK_UPGRADE_2_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 1 Unlock 3",                                 // Name
//"sevomin upgrade power up 1",       // Description
//POWER_UP_1_UNLOCK_UPGRADE_3_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 50000)    // Purchase type
//);
//    public static VirtualGood POWER_UP_1_UNLOCK_UPGRADE_4 = new UpgradeVG(
//POWER_UP_1_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//null,         // Item ID of the next upgrade good
//POWER_UP_1_UNLOCK_UPGRADE_3_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 1 Unlock 4",                                 // Name
//"charomin upgrade power up 1",       // Description
//POWER_UP_1_UNLOCK_UPGRADE_4_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500000)    // Purchase type
//);

//    public static VirtualGood POWER_UP_2_UNLOCK_LTVG = new LifetimeVG(
//"Power Up 2 Unlock",                                                            // Name
//"avalin upgrade power up 2", // Description
//POWER_UP_2_UNLOCK_ITEM_ID,                                                            // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500)               // Purchase type
//);

//    public static VirtualGood POWER_UP_2_UNLOCK_UPGRADE_1 = new UpgradeVG(
//    POWER_UP_2_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//    POWER_UP_2_UNLOCK_UPGRADE_2_ITEM_ID,         // Item ID of the next upgrade good
//    null,                         // Item ID of the previous upgrade good
//    "Power Up 2 Unlock 2",                                 // Name
//    "dovomin upgrade power up 2",       // Description
//    POWER_UP_2_UNLOCK_UPGRADE_1_ITEM_ID,                                       // Item ID
//    new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500)    // Purchase type
//);

//    public static VirtualGood POWER_UP_2_UNLOCK_UPGRADE_2 = new UpgradeVG(
//POWER_UP_2_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//POWER_UP_2_UNLOCK_UPGRADE_3_ITEM_ID,         // Item ID of the next upgrade good
//POWER_UP_2_UNLOCK_UPGRADE_1_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 2 Unlock 3",                                 // Name
//"dovomin upgrade power up 2",       // Description
//POWER_UP_2_UNLOCK_UPGRADE_2_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 5000)    // Purchase type
//);
//    public static VirtualGood POWER_UP_2_UNLOCK_UPGRADE_3 = new UpgradeVG(
//POWER_UP_2_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//POWER_UP_2_UNLOCK_UPGRADE_4_ITEM_ID,         // Item ID of the next upgrade good
//POWER_UP_2_UNLOCK_UPGRADE_2_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 1 Unlock 3",                                 // Name
//"sevomin upgrade power up 1",       // Description
//POWER_UP_2_UNLOCK_UPGRADE_3_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 50000)    // Purchase type
//);
//    public static VirtualGood POWER_UP_2_UNLOCK_UPGRADE_4 = new UpgradeVG(
//POWER_UP_2_UNLOCK_ITEM_ID,                   // Item ID of the associated good that is being upgraded
//null,         // Item ID of the next upgrade good
//POWER_UP_2_UNLOCK_UPGRADE_3_ITEM_ID,                         // Item ID of the previous upgrade good
//"Power Up 1 Unlock 4",                                 // Name
//"charomin upgrade power up 1",       // Description
//POWER_UP_2_UNLOCK_UPGRADE_4_ITEM_ID,                                       // Item ID
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500000)    // Purchase type
//);

//    #endregion

//    #region power up recharge declarations

//    public static VirtualGood POWER_UP_RECHARGE_5_GOOD = new SingleUseVG(
//"5 recharge of power up ",                                                 // name
//"recharge 5 ta power up yani vaghti hame unlock shode o hame masraft shode",              // description
//POWER_UP_RECHARGE_5_ITEM_ID,                                                 // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 500));       // the way this virtual good is purchased

//    public static VirtualGood POWER_UP_RECHARGE_4_GOOD = new SingleUseVG(
//"4 recharge of power up ",                                                 // name
//"recharge 4 ta power up ",              // description
//POWER_UP_RECHARGE_4_ITEM_ID,                                                 // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 400));       // the way this virtual good is purchased

//    public static VirtualGood POWER_UP_RECHARGE_3_GOOD = new SingleUseVG(
//"3 recharge of power up ",                                                 // name
//"recharge 3 ta power up ",              // description
//POWER_UP_RECHARGE_3_ITEM_ID,                                                 // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 300));       // the way this virtual good is purchased

//    public static VirtualGood POWER_UP_RECHARGE_2_GOOD = new SingleUseVG(
//"2 recharge of power up ",                                                 // name
//"recharge 2 ta power up ",              // description
//POWER_UP_RECHARGE_2_ITEM_ID,                                                 // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 200));       // the way this virtual good is purchased

//    public static VirtualGood POWER_UP_RECHARGE_1_GOOD = new SingleUseVG(
//"1 recharge of power up ",                                                 // name
//"recharge 1 ta power up",              // description
//POWER_UP_RECHARGE_1_ITEM_ID,                                                 // item id
//new PurchaseWithVirtualItem(SIBIL_CURRENCY_ITEM_ID, 100));       // the way this virtual good is purchased

//    #endregion
//}
