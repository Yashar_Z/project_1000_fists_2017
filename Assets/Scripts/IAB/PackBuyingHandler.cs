﻿using UnityEngine;
using BazaarPlugin;

public class PackBuyingHandler
{

    public static void _Handler_FirstPackBuy()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            try
            {
                CafeBazaarIapManager.PurchaseSIBIL_0_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("BAZZAR PLUGIN " + e.Message);
            }
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            try
            {
                ArioGamesIapManager.PurchaseSIBIL_0_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("ARIO PLUGIN " + e.Message);
            }
        }
    }
    public static void _Handler_SecondPackBuy()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            try
            {
                CafeBazaarIapManager.PurchaseSIBIL_1_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("BAZZAR PLUGIN " + e.Message);
            }
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            try
            {
                ArioGamesIapManager.PurchaseSIBIL_1_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("ARIO PLUGIN " + e.Message);
            }
        }
    }
    public static void _Handler_ThirdPackBuy()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            try
            {
                CafeBazaarIapManager.PurchaseSIBIL_2_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("BAZZAR PLUGIN " + e.Message);
            }
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            try
            {
                ArioGamesIapManager.PurchaseSIBIL_2_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("ARIO PLUGIN " + e.Message);
            }
        }
    }
    public static void _Handler_FourthPackBuy()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            try
            {
                CafeBazaarIapManager.PurchaseSIBIL_3_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("BAZZAR PLUGIN " + e.Message);
            }
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            try
            {
                ArioGamesIapManager.PurchaseSIBIL_3_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("ARIO PLUGIN " + e.Message);
            }
        }
    }
    public static void _Handler_FifthPackBuy()
    {
        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            try
            {
                CafeBazaarIapManager.PurchaseSIBIL_4_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("BAZZAR PLUGIN " + e.Message);
            }
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            try
            {
                ArioGamesIapManager.PurchaseSIBIL_4_PACK();
            }
            catch (System.Exception e)
            {
                Debug.LogError("ARIO PLUGIN " + e.Message);
            }
        }
    }
}
