﻿using UnityEngine;
using System.Collections;

public class Bell_DingCaller : MonoBehaviour {
	
	private World3_Mover w3MoverScript;
	private World4_Mover w4MoverScript;
	public Bell_Script bellScriptHolder;


	public void Bell_DingCallNow () {
//		Debug.LogError ("Bell_DingCallNow parent = " + this.transform.parent.name);

		// This the second bell anim 3 / bell sound repeat that caused problem in endless
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			bellScriptHolder.BellAnim_CallDingOnly ();
		}
	}

	public void BellTo_Wolrd1 () {
		PlayerData_InGame.Instance.worldsInGameScriptHolder.world1_MoverHolder.W1_ParticlesPlay ();
	}

	public void BellTo_Wolrd2 () {
		PlayerData_InGame.Instance.worldsInGameScriptHolder.world2_MoverHolder.W2_ParticlesPlay ();
	}

	public void BellTo_Wolrd3_Shake () {
		w3MoverScript = PlayerData_InGame.Instance.worldsInGameScriptHolder.world3_MoverHolder;
//		w3MoverScript = GetComponentInParent<World3_Mover> ();
		w3MoverScript.W3_Shake_Now ();
		w3MoverScript.W3_ParticlesPlay ();
	}

	public void BellTo_Wolrd4 () {
		w4MoverScript = PlayerData_InGame.Instance.worldsInGameScriptHolder.world4_MoverHolder;
		w4MoverScript.W4_ParticlesPlay ();

//		PlayerData_InGame.Instance.worldsInGameScriptHolder.world4_MoverHolder.W4_ParticlesPlay ();
	}

	public void AnimEvent_W4_IntroToBackFlicker () {
		StartCoroutine (w4MoverScript.W4_Flicker_FirstTimeOnly ());
	}

	public void AnimEvent_W4_FlickerToFlickerLoop () {
		StartCoroutine (w4MoverScript.W4_Flicker_Delayer ());
	}
}
