﻿using UnityEngine;
using System.Collections;

public class Bell_Script : MonoBehaviour {

	public AnimationClip[] animClipsArr;

	public AnimationClip[] animClipsWaveBannerArr;

	public Animation animBellHolder;
	public Animation animWaveBannerHolder;
	public UnityEngine.UI.Text textWaveBannerHolder;
	public int intWaveEndlessCounter;

	public ParticleSystem particleBellRingHolder;
	public bool canDoDingOnly;
	public bool canEndlessWaveCount;

	public GameObject objBellChainsHolder;

	private bool isIdleMode;
	private bool wasFirstBellrung = false;

	public IEnumerator BellAnim_Play (int whichAnim, float delay, bool isLastWave, bool hasText) {
		yield return new WaitForSeconds (delay);
		animBellHolder.clip = animClipsArr [whichAnim];
		animBellHolder.Play ();

//		Debug.LogError ("YO! whichAnim = " + whichAnim);

        // SOUNDD EFFECTS - Bell call (done)
		if (whichAnim == 3) {
//			Debug.LogError ("YO! BELL!");
			if (SoundManager.Instance != null) {
				SoundManager.Instance.Zang.Play ();
			}
		}

        // Check for bell ring particle time
        if (whichAnim > 1) {
			particleBellRingHolder.Play ();
		}

		// Check between campaign and endless
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			// Last Wave
			if (isLastWave) {
				WaveTitleAnim_Play_Last ();
			} else {
				if (hasText) {
					if (wasFirstBellrung) {
						// 
						WaveTitleAnim_Play_Mids ();
					} else {
						// Only now the first bell can be rung
						wasFirstBellrung = true;

						WaveTitleAnim_Play_First ();
					}
				}
			}
		}
	}

	public IEnumerator DingOnly_TimedDisableEnable () {
//		Debug.LogError ("DING ONLY FALSE? = " + canDoDingOnly);
		canDoDingOnly = false;
		yield return new WaitForSeconds (4);
		canDoDingOnly = true;
	}

	public void BellAnim_IdleOnly () {
//		Debug.LogError ("Call Bell - IDLE ONLY");

		isIdleMode = true;
		StartCoroutine (BellAnim_Play (1, 0, false, false));

		// To make sure bell doesn't stay down
		StartCoroutine (BellAnim_CallDingDelayed ());
	}

	public void BellAnim_CallShort () {
//		Debug.LogError ("Call Bell - SHORT");
		StartCoroutine (BellAnim_Play (0, 0, false, false));
//		StartCoroutine (BellAnim_Play (3, 2.5F));
	}

	public void BellAnim_CallDingOnly () {
//		Debug.LogError ("Call Bell - DING ONLY = " + canDoDingOnly);
		if (canDoDingOnly) {
			StartCoroutine (BellAnim_Play (3, 0, false, true));
		}

		// Copied from end of BellAnim_Call_Delayed
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			WaveTitleAnim_Play_EndlessWave ();
		}
	}

	public IEnumerator BellAnim_CallDingDelayed () {
		yield return new WaitForSeconds (1.5F);
//		Debug.LogError ("Bell delayed, FIRST");

		// This means it is still down
		if (this.transform.localPosition.y == 5.45F) {
//			Debug.LogError ("Bell delayed, SECOND");
			BellAnim_CallDingOnly ();
		}

		// Only show this at the start of endless' delayed part (Now called directly after potions menu is closed)
//		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
//			WaveTitleAnim_Play_EndlessWave ();
//		}
	}

	// This is used for middle of gameplay (Rather than start of level where there is intro)
	public void BellAnim_Call_Delayed (float delay, bool isLastWave) {
//		Debug.LogError ("Call Bell - DELAYED for this test");
		StartCoroutine (BellAnim_Play (0, delay + 0, isLastWave, true));
		StartCoroutine (BellAnim_Play (3, delay + 1, false, false));

		// Copied TO BellAnim_CallDingOnly
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			WaveTitleAnim_Play_EndlessWave ();

			// Old method
//			if (canEndlessWaveCount) {
//				WaveTitleAnim_Play_EndlessWave ();
//			} else {
//				WaveTitleAnim_Play_EndlessStart ();
//			}
		}
	}

	public IEnumerator WaveTitleAnim_Call (bool isLastWave) {
		if (!isLastWave) {
			yield return new WaitForSeconds (2.6F);
			animWaveBannerHolder.Play ("HUD Wave Title - First Anim 1 (Legacy)");
		} else {
			yield return new WaitForSeconds (0.1F);
			animWaveBannerHolder.Play ("HUD Wave Title - Last Anim 1 (Legacy)");
		}
	}

	public IEnumerator WaveBanner_AnimPlay (int whichInt) {
		animWaveBannerHolder.clip = animClipsWaveBannerArr [whichInt];
		animWaveBannerHolder.Play ();
		yield return null;
	}

	public void WaveTitleAnim_Play_First () {
		StartCoroutine (WaveBanner_AnimPlay (0));
//		animWaveTitleHolder.Play ("HUD Wave Title - First Anim 1 (Legacy)");
	}

	public void WaveTitleAnim_Play_Mids () {
		if (Random.Range (1, 3) == 1) {
			StartCoroutine (WaveBanner_AnimPlay (2));
//			animWaveTitleHolder.Play ("HUD Wave Title - Mid 1 Anim 1 (Legacy)");
		} else {
			StartCoroutine (WaveBanner_AnimPlay (3));
//			animWaveTitleHolder.Play ("HUD Wave Title - Mid 2 Anim 1 (Legacy)");
		}
	}

	public void WaveTitleAnim_Play_Last () {
		StartCoroutine (WaveBanner_AnimPlay (1));
//		animWaveTitleHolder.Play ("HUD Wave Title - Last Anim 1 (Legacy)");
	}

	public void WaveTitleAnim_Play_EndlessStart () {
		StartCoroutine (WaveBanner_AnimPlay (4));
		canEndlessWaveCount = true;
	}

	public void WaveTitleAnim_Play_EndlessWave () {
		StartCoroutine (WaveBanner_AnimPlay (5));
//		Debug.LogError ("intWaveEndlessCounter = " + intWaveEndlessCounter);

		intWaveEndlessCounter++;
		textWaveBannerHolder.text = intWaveEndlessCounter.ToString () + " ﺝﻮـــﻣ";
	}

	public void BellEnsabler () {
		objBellChainsHolder.SetActive (true);
	}

	public void BellDisabler () {
		objBellChainsHolder.SetActive (false);
	}

//	void Update () {
//		Debug.LogWarning ("DING status = " + canDoDingOnly);
//
//		if (Input.GetKeyDown ("i")) {
//			StartCoroutine (BellAnim_Play (0, 0, false));
//		}
//
//		if (Input.GetKeyDown ("j")) {
//			StartCoroutine (BellAnim_Play (2, 0, false));
//		}
//	}
}
