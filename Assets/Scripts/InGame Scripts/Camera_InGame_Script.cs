﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Camera_InGame_Script : MonoBehaviour {

	public Animation camParentAnimHolder;
	public Animation camIdleAnimHolder;
	public Animation camMainAnimHolder;
//	public Animation pauseMenuAnimHolder;

	public Animation eyesAnimHolder;
	public Animation endGradBordersAnimHolder;
	public Animation HF_FrontClingyAnimHolder;
	public Camera camRef;
//	public ParticleSystem[] particleFistWindArr;

	public enum CamZoom {ZoomOut, ZoomIn_Fast, ZoomIn_Slow, ZoomIn_Done} 
	public NoiseAndScratches camNoiseRef;
//	public BlurOptimized camBlurRef;
	public SepiaTone sepiaToneRef;
	public ColorCorrectionCurves colorCorSatRef;

	public ParticleSystem particleFullScreenCritWhiteQuickHolder;
	public ParticleSystem particleFullScreenRedHolder;
	public ParticleSystem particleFullScreenRedExplodeHolder;
	public ParticleSystem particleFullScreenBlueHolder;
	public ParticleSystem particleFullScreenShieldHolder;
	public ParticleSystem particleFullScreenDeathHolder;

	public ParticleSystem particleFullScreenFreezeStartHolder;
	public ParticleSystem particleFullScreenFreezeEndHolder;

	public ParticleSystem particleFullScreenPipeHitHolder;

	private AudioSource audioSourceBuffer;

	private GameObject pauseMenuParentObjRef;
	private bool camMoveBackAllowed;
	private bool camMoveForwardAllowed;
	private int camExplodeShakeNumberInt;
//	private float cam_BlurCurr;
	private float cam_zoomOutAmount;
	private float camSize_Start;
	private float camSize_Curr;
	private float camSize_Target;
	private float pausedTimeCamIdleAnim;
	private float pausedTimeHF_FrontIdleAnim;
	private string pausedTimeCamIdleClipName;
	private bool camPhase_ZoomOut;
	private bool camPhase_ZoomIn;
	private bool option_OrigEffectsActive;
	private bool camAnim_CritIsPlaying;
	private bool allowNormalAttackAnim;

	private string cam_CurrHitDir;
	private float critAnim_CurrTime;

	private CamZoom camZoom;

	void Start () {
		camAnim_CritIsPlaying = false;
		camMoveBackAllowed = false;
		camMoveForwardAllowed = false;
		Invoke ("Camera_FirstSize", 0.1F);
		cam_zoomOutAmount = 0.7F;

//		camNoiseRef = camRef.GetComponent<NoiseAndScratches> ();
//		camBlurRef = camRef.GetComponent<BlurOptimized> ();
		sepiaToneRef = camRef.GetComponent<SepiaTone> ();
		colorCorSatRef = camRef.GetComponent<ColorCorrectionCurves> ();

//		camBlurRef.enabled = false;
		option_OrigEffectsActive = PlayerData_InGame.Instance.option_OrigEffects_Active;

//		pauseMenuParentObjRef = pauseMenuAnimHolder.gameObject;

		camExplodeShakeNumberInt = Random.Range (1, 3);

		// Full attack anim or not
//		if (PlayerData_InGame.Instance.fullAttackAnim_Allowed) {
//			allowNormalAttackAnim = true;
//		} else {
//			allowNormalAttackAnim = false;
//		}
	}

    // TODO: mosht ba jahat

	public void PlayAnim_PlayerAttack (string hitDir) {
		if (!camAnim_CritIsPlaying) {
			cam_CurrHitDir = hitDir;
			switch (cam_CurrHitDir) {
			case "U":
				camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");

				if (SoundManager.Instance != null) {
					audioSourceBuffer = SoundManager.Instance.Mosht1;
					audioSourceBuffer.pitch = Random.Range (1, 1.1F);
				}

//				if (allowNormalAttackAnim) {
//					camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");
//				} else {
//					camParentAnimHolder.Play ("Cam Attack Up Anim 1 (Legacy)");
//				}

//				particleFistWindArr [0].Emit (4);
				break;
			case "D":
				camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");

				if (SoundManager.Instance != null) {
					audioSourceBuffer = SoundManager.Instance.Mosht1;
					audioSourceBuffer.pitch = Random.Range (1, 1.1F);
				}

//				if (allowNormalAttackAnim) {
//					camParentAnimHolder.Play ("Cam Attack Down Anim 2 (Legacy)");
//				} else {
//					camParentAnimHolder.Play ("Cam Attack Down Anim 1 (Legacy)");
//				}

//				particleFistWindArr [1].Emit (4);
				break;
			case "R":
				camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");

				if (SoundManager.Instance != null) {
					audioSourceBuffer = SoundManager.Instance.Mosht2;
					audioSourceBuffer.pitch = Random.Range (0.9F, 1);
				}

//				if (allowNormalAttackAnim) {
//					camParentAnimHolder.Play ("Cam Attack Right Anim 2 (Legacy)");
//				} else {
//					camParentAnimHolder.Play ("Cam Attack Right Anim 1 (Legacy)");
//				}

//				particleFistWindArr [2].Emit (6);
				break;
			case "L":
				camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");

				if (SoundManager.Instance != null) {
					audioSourceBuffer = SoundManager.Instance.Mosht2;
					audioSourceBuffer.pitch = Random.Range (0.9F, 1);
				}

//				if (allowNormalAttackAnim) {
//					camParentAnimHolder.Play ("Cam Attack Left Anim 2 (Legacy)");
//				} else {
//					camParentAnimHolder.Play ("Cam Attack Left Anim 1 (Legacy)");
//				}

//				particleFistWindArr [3].Emit (6);
				break;
			default:
				camParentAnimHolder.Play ("Cam Attack Up Anim 2 (Legacy)");

				if (SoundManager.Instance != null) {
					audioSourceBuffer = SoundManager.Instance.Mosht2;
					audioSourceBuffer.pitch = Random.Range (0.9F, 1);
				}
				break;
			}

			StartCoroutine (StopAnim_PlayerMiss ());

			// Old, non coroutine method
//			Invoke ("StopAnim_PlayerMiss", 0.002F);
		}
	}
		
	public void Camera_Noise_Toggle () {
		if (!camNoiseRef.enabled) {
			Invoke ("Camera_Noise_ColorToggle", 1);
//			camMainAnimHolder.Stop ();
			camParentAnimHolder.Stop();
			camParentAnimHolder.Play ("Cam Death Noise Start Anim 1 (Legacy)");
//			camMainAnimHolder.Play ("Cam Player Grain Start Anim 1 (Legacy)");
		} else {
			camParentAnimHolder.Play ("Cam Death Noise End Anim 1 (Legacy)");
//			camMainAnimHolder.Play ("Cam Player Grain End Anim 1 (Legacy)");
		}
	}

	public void Camera_Sepia_LowSat (bool isActive, float whatSat) {
		colorCorSatRef.saturation = whatSat;
		colorCorSatRef.enabled = isActive;
	}

	public void PlayAnim_PlayerExplosionShake () {
		camMainAnimHolder.Play ("Cam Player Explosion Shake Anim " + camExplodeShakeNumberInt.ToString () +" (Legacy)");
		camExplodeShakeNumberInt++;
		if (camExplodeShakeNumberInt > 2) {
			camExplodeShakeNumberInt = 1;
		}
	}

	public void PlayAnim_PlayerLastStandShoutShake () {
		camMainAnimHolder.Play ("Cam Player Last Stand Shout Shake Anim 1 (Legacy)");
	}

	public void PlayAnim_PlayerHeal () {
		CamerA_ZoomInOut ();
	}

	public void PlayAnim_RemoveChromatic () {
		camMainAnimHolder.Play ("Cam Player Remove Chromatic Anim 1 (Legacy)");
	}

	public void PlayAnim_PlayerHurt () {
		particleFullScreenRedHolder.Play ();
//		Debug.LogWarning ("EXPLODE: Cam Player Hurt Anim 1 (Legacy)");
		camMainAnimHolder.Play ("Cam Player Hurt Anim 1 (Legacy)");
//		fullScreenAnimHolder.Play ("FullScreen Player Hurt Anim 1 (Legacy)");

		// Set Up the camMoveBack process
		Camera_ZoomOutIn ();
//		Camera_Blur ();
	}

	public void PlayAnim_PlayerHurtExplode () {
		particleFullScreenRedExplodeHolder.Play ();
		Debug.LogWarning ("EXPLODE: Cam Player Hurt Explode Anim 1 (Legacy)");
		camMainAnimHolder.Play ("Cam Player Hurt Explode Anim 1 (Legacy)");
//		fullScreenAnimHolder.Play ("FullScreen Player Hurt Anim 1 (Legacy)");

		// Set Up the camMoveBack process
		Camera_ZoomOutIn ();
//		Camera_Blur ();
	}

	public void PlayAnim_PlayerDiedHurt () {
		particleFullScreenDeathHolder.Play ();
		camMainAnimHolder.Play ("Cam Player Died-Hurt Anim 1 (Legacy)");
		//		fullScreenAnimHolder.Play ("FullScreen Player Shielded-Hurt Anim 1 (Legacy)");

		// Set Up the camMoveBack process
		Camera_ZoomOutIn ();
		//		Camera_Blur ();
	}

	public void PlayAnim_PlayerShieldedHurt () {
		particleFullScreenShieldHolder.Play ();
		camMainAnimHolder.Play ("Cam Player Shielded-Hurt Anim 1 (Legacy)");
        //		fullScreenAnimHolder.Play ("FullScreen Player Shielded-Hurt Anim 1 (Legacy)");

        // SOUNDD EFFECTS - Shielded hit sound (done)
        if (SoundManager.Instance!= null)
        {
            switch (Random.Range(1,5))
            {
                case 1:
                    SoundManager.Instance.MoshtBeSepar1.Play();
                    break;
                case 2:
                    SoundManager.Instance.MoshtBeSepar2.Play();
                    break;
                case 3:
                    SoundManager.Instance.MoshtBeSepar3.Play();
                    break;
                case 4:
                    SoundManager.Instance.MoshtBeSepar4.Play();
                    break;
                default:
                    break;
            }
        }

		// Set Up the camMoveBack process
		Camera_ZoomOutIn_Shielded ();
		//		Camera_Blur ();
	}

	public void PlayAnim_PowUP1_RippleStart () {
		camParentAnimHolder.Play ("Cam Ripple Pow1 Start Anim 1 (Legacy)");
	}

	public void PlayAnim_PowUP2_RippleStart () {
		camParentAnimHolder.Play ("Cam Ripple Pow2 Start Anim 1 (Legacy)");
	}

	public void PlayAnim_PowUP1_RippleFull () {
		camParentAnimHolder.Play ("Cam Ripple Pow1 Full Anim 1 (Legacy)");
	}

	public void PlayAnim_PowUP2_RippleFull () {
		camParentAnimHolder.Play ("Cam Ripple Pow2 Full Anim 1 (Legacy)");
	}
		
	public void PlayAnim_EndGradBorders () {
		endGradBordersAnimHolder.Play ("HUD (End) Grads Enter Anim 1 (Legacy)");
//		Debug.LogError ("YYYYY: " + endGradBordersAnimHolder.IsPlaying("HUD (End) Grads Enter Anim 1 (Legacy)"));
//		endGradBordersAnimHolder.gameObject.SetActive(false);
	}

	public void PauseTheGame () {
		eyesAnimHolder.Play ("HUD (Top) Eyes Pause Closed Anim 1 (Legacy)");
//		pauseMenuAnimHolder.Play ("Menu Pause Enter Anim 1 (Legacy)");
		PauseFunction ();

		// Pause Cam Idles
		CamFrontElements_FullFreeze_Start ();

		// Blur for pause
//		DarkenPauseToggle ();
	}

	public void ResumeTheGame () {
		eyesAnimHolder.Play ("HUD (Top) Eyes Pause Opened Anim 1 (Legacy)");
//		pauseMenuAnimHolder.Play ("Menu Pause Leave Anim 1 (Legacy)");
		Invoke ("UnPauseFunction", 0.8F);

		// Pause Cam Idles
		CamFrontElements_FullFreeze_End ();

		// Blur for pause
//		Invoke ("DarkenPauseToggle", 0.75F);
	}

	public void CamFrontElements_FullFreeze_Start () {
//		pausedTimeCamIdleAnim = camIdleAnimHolder[camIdleAnimHolder.clip.name].time;
//		pausedTimeHF_FrontIdleAnim = HF_FrontClingyAnimHolder [HF_FrontClingyAnimHolder.clip.name].time;
//		if (camIdleAnimHolder.IsPlaying ("Cam HF Clingy Idle Anim 1 (Legacy)")) {
//			pausedTimeCamIdleClipName = "Cam HF Clingy Idle Anim 1 (Legacy)";
//		}

		camIdleAnimHolder.Stop ();
		HF_FrontClingyAnimHolder.Stop ();
	}

	public void CamFrontElements_FullFreeze_End () {
//		camIdleAnimHolder[camIdleAnimHolder.clip.name].time = pausedTimeCamIdleAnim;
//		HF_FrontClingyAnimHolder [HF_FrontClingyAnimHolder.clip.name].time = pausedTimeHF_FrontIdleAnim;

		if (PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool) {
			camIdleAnimHolder.Play ("Cam HF Clingy Idle Anim 1 (Legacy)");
		} else {
			camIdleAnimHolder.Play ("Cam Idle Anim 1 (Legacy)");
		}

		HF_FrontClingyAnimHolder.Play ();
	}

	public void ContinueTheGame () {
		if (!EventTrigger.isPaused) {
			PauseFunction ();
		} else {
			Invoke ("UnPauseFunction", 0.6F);
		}
	}

	public void CameraEffects_Reset () {
		camParentAnimHolder.Stop ();
		camParentAnimHolder.Play ("Cam Attack STOP All Anim 1 (Legacy)");
	}

	public void Cam_IdleAnim_Start () {
		camIdleAnimHolder.Stop ();
		camIdleAnimHolder.Play ("Cam Idle Anim 1 (Legacy)");
	}

	public void Cam_HF_ClingyIdleAnim_Start () {
		camIdleAnimHolder.Stop ();
		camIdleAnimHolder.Play ("Cam HF Clingy Idle Anim 1 (Legacy)");
	}

	public void Cam_IdleAnim_Stop () {
		camIdleAnimHolder.Stop ();
	}

	void PauseFunction () {
		EventTrigger.PauseTheGame ();
	}

	void UnPauseFunction () {
		EventTrigger.UnPauseTheGame ();
	}
		
	void DarkenPauseToggle () {
		if (pauseMenuParentObjRef.activeInHierarchy) {
			pauseMenuParentObjRef.SetActive (false);
//			camBlurRef.enabled = false;
//			Debug.LogError ("Blur Toggled OFF");
		} else {
			pauseMenuParentObjRef.SetActive (true);
//			camBlurRef.enabled = true;
//			Debug.LogError ("Blur Toggled ON");
		}
	}

	// Hurt ZoomOut
	void Camera_ZoomOutIn() {
		camMoveBackAllowed = true;
		camSize_Curr = camSize_Start;
		camSize_Target = camSize_Start + cam_zoomOutAmount;
		camZoom = CamZoom.ZoomOut;
	}

	// Hurt ZoomOut
	void Camera_ZoomOutIn_Shielded() {
		camMoveBackAllowed = true;
		camSize_Curr = camSize_Start;
		camSize_Target = camSize_Start + (cam_zoomOutAmount * 0.6F);
		camZoom = CamZoom.ZoomOut;
	}

	// Heal ZoomIn
	void CamerA_ZoomInOut () {
		camMoveForwardAllowed = true;
		camSize_Curr = camSize_Start;
		camSize_Target = camSize_Start - (cam_zoomOutAmount * 0.1F);
		camZoom = CamZoom.ZoomIn_Slow;
	}

//	void Camera_Blur() {
//		camBlurRef.enabled = true;
//		camBlurRef.blurIterations = 1;
//		camBlurRef.blurSize = 4;
////		cam_BlurCurr = camBlurRef.blurSize;
//	}

	void Camera_Noise_ColorToggle () {
		camNoiseRef.monochrome = !camNoiseRef.monochrome;
		if (camNoiseRef.enabled) {
			if (camNoiseRef.monochrome) {
				Invoke ("Camera_Noise_ColorToggle", Random.Range (1.5F, 2.5F));
			} else {
				Invoke ("Camera_Noise_ColorToggle", Random.Range (0.8F, 1.2F));
			}
		}
	}

	IEnumerator StopAnim_PlayerMiss () {
		yield return new WaitForSeconds (0.002F);

		critAnim_CurrTime = camParentAnimHolder [camParentAnimHolder.clip.name].normalizedTime;
		if (!EventTrigger.isCorrectHit) {

			// Punishment for multiplier
			if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
				StartCoroutine (PlayerData_InGame.Instance.PlayerCombo_Reset ());
			}

			CameraEffects_Reset ();

            // SOUNDD EFFECTS - PLAYER MISS (done)
            if (SoundManager.Instance !=null)
            {
                SoundManager.Instance.PlayerMiss.Play();
            }


			// Increase Player misses
			PlayerData_InGame.Instance.p1_TotalHits_Misses++;

		// Was NOT missed

		} else {
			// Reward for multiplier
			if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
				StartCoroutine (PlayerData_InGame.Instance.PlayerCombo_Gain ());
			}
			
			if (PlayerData_InGame.Instance.critHappened) {
				particleFullScreenCritWhiteQuickHolder.Emit (1);
				camAnim_CritIsPlaying = true;
				Invoke ("StopCritPlaying", 0.4F);

				// Slow motion
//				SlowMo_FullProcess();

				if (PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
                    // SOUNDD EFFECTS - Pipe Critical (done)
                    if (SoundManager.Instance!=null)
                    {
                        SoundManager.Instance.PipeCrit.Play();
                    }

//					Debug.LogError ("Pipe Crit!");
				} else {

					// SOUNDD EFFECTS - Fist Critical (Done) (Doesn't need buffer)
					if (SoundManager.Instance != null) {
						switch (Random.Range (1, 3)) {
						case 1:
							SoundManager.Instance.MoshtCrit1.pitch = 0.6f;
							SoundManager.Instance.MoshtCrit1.Play ();
							break;
						case 2:
							SoundManager.Instance.MoshtCrit1.pitch = 0.7f;
							SoundManager.Instance.MoshtCrit1.Play ();
							break;
						default:
							break;
						}
					}
				}

				// This was previously inside an if to choose between old crit and new crit (Now its only new crit)
				switch (cam_CurrHitDir) {
				case "U":
					camParentAnimHolder.Play ("Cam Attack CRIT Up Anim 2 (Legacy)");
					//						particleFistWindArr [0].Emit (44);
					break;
				case "D":
					camParentAnimHolder.Play ("Cam Attack CRIT Down Anim 2 (Legacy)");
					//						particleFistWindArr [1].Emit (44);
					break;
				case "R":
					camParentAnimHolder.Play ("Cam Attack CRIT Right Anim 2 (Legacy)");
					//						particleFistWindArr [2].Emit (44);
					break;
				case "L":
					camParentAnimHolder.Play ("Cam Attack CRIT Left Anim 2 (Legacy)");
					//						particleFistWindArr [3].Emit (44);
					break;
				}
				camParentAnimHolder [camParentAnimHolder.clip.name].normalizedTime = critAnim_CurrTime;

//				if (option_OrigEffectsActive) {
//					switch (cam_CurrHitDir) {
//					case "U":
//						camParentAnimHolder.Play ("Cam Attack CRIT Up Anim 1 (Legacy)");
//						break;
//					case "D":
//						camParentAnimHolder.Play ("Cam Attack CRIT Down Anim 1 (Legacy)");
//						break;
//					case "R":
//						camParentAnimHolder.Play ("Cam Attack CRIT Right Anim 1 (Legacy)");
//						break;
//					case "L":
//						camParentAnimHolder.Play ("Cam Attack CRIT Left Anim 1 (Legacy)");
//						break;
//					}
//					camParentAnimHolder [camParentAnimHolder.clip.name].normalizedTime = critAnim_CurrTime;
//				} else {
//					switch (cam_CurrHitDir) {
//					case "U":
//						camParentAnimHolder.Play ("Cam Attack CRIT Up Anim 2 (Legacy)");
////						particleFistWindArr [0].Emit (44);
//						break;
//					case "D":
//						camParentAnimHolder.Play ("Cam Attack CRIT Down Anim 2 (Legacy)");
////						particleFistWindArr [1].Emit (44);
//						break;
//					case "R":
//						camParentAnimHolder.Play ("Cam Attack CRIT Right Anim 2 (Legacy)");
////						particleFistWindArr [2].Emit (44);
//						break;
//					case "L":
//						camParentAnimHolder.Play ("Cam Attack CRIT Left Anim 2 (Legacy)");
////						particleFistWindArr [3].Emit (44);
//						break;
//					}
//					camParentAnimHolder [camParentAnimHolder.clip.name].normalizedTime = critAnim_CurrTime;
//				}

			} 

			// Different Logic For Pipe Attacks
			else {
				if (PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
					particleFullScreenPipeHitHolder.Play ();

					// Decrease the pipe amount in case of NOT miss
					PlayerData_InGame.Instance.PlayerItem_ItemDurabilityDecrease (0);

					// Pipe count increase
					PlayerData_InGame.Instance.PipeItemUsed_Resetter++;

					// Achievement for pipe hit increase
					if (AchievementManager.Instance != null) {
						StartCoroutine (AchievementManager.Instance.AchieveProg_DoPipe_1000 ());
					}

					//// SOUND EFFECT - Chomagh Hit (Needs change)
					if (SoundManager.Instance != null) {
						SoundManager.Instance.PipeHit.Play ();
					}

					// Pipe attack Slo-mo
//					StartCoroutine (SlowMo_PipeSlow(0.1F, 0.5F));

//					switch (cam_CurrHitDir) {
//					case "U":
//						camParentAnimHolder.Play ("Cam Attack CRIT Up Anim 1 (Legacy)");
//						break;
//					case "D":
//						camParentAnimHolder.Play ("Cam Attack CRIT Down Anim 1 (Legacy)");
//						break;
//					case "R":
//						camParentAnimHolder.Play ("Cam Attack CRIT Right Anim 1 (Legacy)");
//						break;
//					case "L":
//						camParentAnimHolder.Play ("Cam Attack CRIT Left Anim 1 (Legacy)");
//						break;
//					}
//					camParentAnimHolder [camParentAnimHolder.clip.name].normalizedTime = critAnim_CurrTime;

				} else {
					// Place for case that crit NOR pipe have not happened BUT its not a miss

					// SOUNDD EFFECTS - Fist Normal (done)
					if (SoundManager.Instance != null) {
						audioSourceBuffer.Play ();
					}
				}
			}
		}
	}

	IEnumerator SlowMo_PipeSlow (float delay, float speed) {
//		yield return new WaitForSeconds (0.05F);
		Time.timeScale = speed;
		yield return new WaitForSeconds (delay);
		Time.timeScale = 1;
	}

	void SlowMo_FullProcess () {
		TimeScale_SlowMo ();
		Invoke ("TimeScale_Normal", 0.04F);
	}

	void StopCritPlaying() {
		camAnim_CritIsPlaying = false;
	}

	void TimeScale_SlowMo () {
		if (Time.timeScale > 0.2F) {
			Time.timeScale = 0.2F;
		}
	}

	void TimeScale_Normal () {
		Time.timeScale = 1;
	}

	// Update is called once per frame
	void Update () {

		if (camMoveBackAllowed) {
//			cam_BlurCurr = Mathf.MoveTowards (cam_BlurCurr, 0, Time.deltaTime * 10);
//			camBlurRef.blurSize = cam_BlurCurr;
			switch (camZoom) {
			case CamZoom.ZoomOut:
				camSize_Curr = Mathf.MoveTowards (camSize_Curr, camSize_Target, Time.deltaTime * 9);
//				Debug.Log ("Happen? OUT");
				if (camSize_Target == camSize_Curr) {
					camZoom = CamZoom.ZoomIn_Fast;
					camSize_Target = camSize_Start;
				}
				break;
			case CamZoom.ZoomIn_Fast:
				camSize_Curr = Mathf.MoveTowards (camSize_Curr, camSize_Target, Time.deltaTime * 4);
//				Debug.Log ("Happen? FAST");
				if ((camSize_Target + (cam_zoomOutAmount / 2)) > camSize_Curr) {
					camZoom = CamZoom.ZoomIn_Slow;
				}
				break;
			case CamZoom.ZoomIn_Slow:
				camSize_Curr = Mathf.MoveTowards (camSize_Curr, camSize_Target, Time.deltaTime * 1);
//				Debug.Log ("Happen? SLOW");
				if (camSize_Target == camSize_Curr) {
					camZoom = CamZoom.ZoomIn_Done;
				}
				break;
			case CamZoom.ZoomIn_Done:
				camMoveBackAllowed = false;
//				camBlurRef.enabled = false;
				break;
			}
			camRef.orthographicSize = camSize_Curr;
		}
		// Camera hit: Go back anim

		// Heal zoom
		if (camMoveForwardAllowed) {
			if (!camMoveBackAllowed) {
				switch (camZoom) {
				case CamZoom.ZoomIn_Slow:
					camSize_Curr = Mathf.MoveTowards (camSize_Curr, camSize_Target, Time.deltaTime * 0.7F);
					if (camSize_Target == camSize_Curr) {
						camZoom = CamZoom.ZoomOut;
						camSize_Target = camSize_Start;
					}
					break;
				case CamZoom.ZoomOut:
					camSize_Curr = Mathf.MoveTowards (camSize_Curr, camSize_Target, Time.deltaTime * 0.5F);
					if (camSize_Target == camSize_Curr) {
						camZoom = CamZoom.ZoomIn_Done;
					}
					break;
				case CamZoom.ZoomIn_Done:
					camMoveForwardAllowed = false;
					break;
				}
				camRef.orthographicSize = camSize_Curr;
			}
		}
	
	}

	void Camera_FirstSize () {
//		Debug.Log ("Start size = " + camRef.orthographicSize);
		camSize_Start = camRef.orthographicSize;
	}
}
