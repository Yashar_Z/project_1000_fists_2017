﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Death_Continue_Script : MonoBehaviour {

	public Popup_All_Script popupAllScriptHolder;
	public Popup_DeathContine_Script popUpDeathConScriptHolder;

	public GameObject deathContinActivatorHolder;

	public Animation endMenuAnimHolder;
	public Animation animAghdasiChangeHolder;
	public Animation animAghdasiIdleHolder;
	public Animation animAghdasiEyesHolder;

	public Text statNumber_enemiesKilled;
	public Text statNumber_enemiesTotal;    
	public Text statNumber_mustacheGained;
	public Text statNumber_percentCorrect;
	public Text statNumber_gameoverTitle;  

	public Text text_ContinuePrice;

//	public Text text_VictoryTitle;
//	public Text text_VictoryDialogue;

	public Animation endGradBordersAnimHolder;

	public ParticleSystem[] particleEyesCryArr;
	public ParticleSystem particleEyesHappyHolder;
	public ParticleSystem particleHeartsHappyHolder;
	public ParticleSystem particleTextFadeHolder;

	public GameObject[] aghdasiVicFacesObjArr;
	public GameObject[] aghdasiFlagObjArr;

	//	public LevelData_Class[] world_LevelDataArr;

	private float PlayerTotalHits;
	private float PlayerTotalMisses;
	private float PlayerMissesThreshold;
	[SerializeField]
	private float percentOfCorrectHits;

	[SerializeField]
	private int percentOfCorrectHits_INT;
	private int allLevelsPerWorld;
//	private int numberOfStage;
	private int continuePriceInt;

//	private string nameOfStage;

	void OnEnable () {
		animAghdasiEyesHolder.Play ("End Popup - Death Aghdasi Eye Circles Anim 1 (Legacy)");
	}

	void Awake () {
//		animAghdasiEyesHolder.Play ("End Popup - Death Aghdasi Eye Circles Anim 1 (Legacy)");
//		animAghdasiEyesHolder.Stop ();
		popUpDeathConScriptHolder.DeathContinue_CanvasDisable ();
		deathContinActivatorHolder.SetActive (false);
		this.gameObject.SetActive (false);
	}

	void Start () {
//		animAghdasiEyesHolder.Play ("End Popup - Death Aghdasi Eye Circles Anim 1 (Legacy)");
		if (PlayerData_Main.Instance != null) {
//			numberOfStage = PlayerData_Main.Instance.LevelNumber;
			PlayerData_Main.Instance.GetFullName_Level ();
//			nameOfStage = PlayerData_Main.Instance.LevelNumber_FullName;

//			statNumber_LevelNumber.text = nameOfStage;
		}
	}

//	void SetupEnd_RemoveScreenEffects () {
//		if (EventTrigger.isFrozen) {
//			Debug.LogError ("UNFREEZE!");
//			PlayerData_InGame.Instance.PlayerItem_Freeze_END_Deactivate ();
//		}
//	}

	public void SetupDeathContinue_Start () {
		// Enable gameobject
		deathContinActivatorHolder.SetActive (true);

		Popup_Title_NameChange (1);

		SetupDeath_All ();

		animAghdasiIdleHolder.Play ("End Popup - Death Aghdasi Idle Normal Anim 1 (Legacy)");

//		animAghdasiEyesHolder.Stop ();
		animAghdasiEyesHolder.Play ("End Popup - Death Aghdasi Eye Circles Anim 1 (Legacy)");
	}

	public void SetupGameOver_Start () {
		// Text Fade
		particleTextFadeHolder.Play ();
		Invoke ("Popup_Title_ChangeToGameOver", 0.35F);

//		animAghdasiEyesHolder.Stop ();
		animAghdasiChangeHolder.Play ("End Popup - Death Aghdasi Change Anim 1 (Legacy)");

		animAghdasiIdleHolder.Play ("End Popup - Death Aghdasi Idle CRY Anim 1 (Legacy)");

		// Cry / tears particles
		AghdasiEyesParticle_Cry ();

		// Game Popup - ALL Enter GAMEOVER Message Anim 1 (Legacy)
	}

	void SetupDeath_All () {
		// Get Continue Cost
		continuePriceInt = PlayerData_InGame.Instance.continueCost;

		// Show Stats & End Menu
		deathContinActivatorHolder.SetActive (true);
		endMenuAnimHolder.Play ("HUD (Continue) Enter Anim 1 (Legacy)");
		animAghdasiChangeHolder.Play ("End Popup - Death Aghdasi Enter Anim 1 (Legacy)");

		// Victory Dialogue
//		AghdasiDialogueRandom ();

		// Calculate the status of misses star
		SetupEnd_UnmissableCalculate ();

		// Give stats
		SetupGameOver_Stats ();

		// Aghdasi starts moving now
		animAghdasiIdleHolder.Play ("End Popup - Death Aghdasi Idle Normal Anim 1 (Legacy)");
	}

	void SetupEnd_UnmissableCalculate () {
		PlayerTotalHits = (float) PlayerData_InGame.Instance.p1_TotalHits_All;
		PlayerTotalMisses = (float) PlayerData_InGame.Instance.p1_TotalHits_Misses;
		PlayerMissesThreshold = PlayerData_InGame.Instance.missesThresholdPercent;
		PlayerMissesThreshold = 1 - (PlayerMissesThreshold / 100);
		percentOfCorrectHits = (PlayerTotalMisses / PlayerTotalHits);
		percentOfCorrectHits_INT = (int) (100 - percentOfCorrectHits * 100);
		if (percentOfCorrectHits_INT < 0) {
			percentOfCorrectHits_INT = 0;
		}

		//		Debug.Log ("The numbers in order: " + PlayerTotalHits + " & " + PlayerTotalMisses + " & " + PlayerMissesTreshold + " -> " + (PlayerTotalMisses / PlayerTotalHits));

//		if (percentOfCorrectHits > PlayerMissesThreshold) {
//			// Player missed too many times
//			if (PlayerData_InGame.Instance.stats_Stars_Unmissable) {
//				PlayerData_InGame.Instance.stats_Stars_Unmissable = false;
//			}
//		}
	}

	public void AghdasiEyesParticle_Cry () {
		particleEyesCryArr[0].Play ();
		particleEyesCryArr[1].Play ();
		particleEyesCryArr[2].Play ();
		particleEyesCryArr[3].Play ();
		particleEyesCryArr[4].Play ();
	}

	public void AghdasiEyesParticle_Happy () {
		particleEyesHappyHolder.Play ();
		particleHeartsHappyHolder.Play ();
	}

	public void AghdasiIdleCryAnimate () {
		
		// NEEDS UPDATE
		animAghdasiIdleHolder.Play ("End Popup - Death Aghdasi Idle CRY Anim 1 (Legacy)");
	}

//	public void AghdasiDialogueRandom () {
//		switch (Random.Range (1, 5)) {
//		case 1:
//			text_VictoryDialogue.text = "!ﻼﮑﯾﺭﺎﺑ";
//			break;
//		case 2:
//			text_VictoryDialogue.text = "!ﻦﯾﺮﻓﺁ";
//			break;
//		case 3:
//			text_VictoryDialogue.text = "!ﻦﯿﺘﺷﺎﮐ ﻞﮔ";
//			break;
//		default:
//			text_VictoryDialogue.text = "!ﻡﺮﮔ ﻥﻮﺘﻣﺩ";
//			break;
//		}
//	}

	void SetupGameOver_Price () {
//		text_ContinuePrice.text = 
	}

	void SetupGameOver_Stats () {
		statNumber_enemiesKilled.text = PlayerData_InGame.Instance.stats_EnemiesKilled.ToString();
		statNumber_enemiesTotal.text = PlayerData_InGame.Instance.stats_EnemiesTotalNumber.ToString();
		statNumber_mustacheGained.text = PlayerData_InGame.Instance.stats_MustacheGained.ToString();
		statNumber_percentCorrect.text = percentOfCorrectHits_INT.ToString ();
		text_ContinuePrice.text = continuePriceInt.ToString ();
	}

	public void Pressed_Continue () {
		// Moved this from the button
		popupAllScriptHolder.PopUp_AnimPlay (1);
		popUpDeathConScriptHolder.AghdasiAnim_Leave ();
		AghdasiEyesParticle_Happy ();
	}

	public void Pressed_GiveUp () {

		// Check for campaign game over or endless end
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			SetupGameOver_Start ();

			// Moved this from the button
			popupAllScriptHolder.PopUp_AnimPlay (2);

			// Moved this from the button
			popUpDeathConScriptHolder.ButtonsGameOverEnter ();
		} else {
			// Same as pressed continue
			popupAllScriptHolder.PopUp_AnimPlay (1);
			popUpDeathConScriptHolder.AghdasiAnim_EndlessEnd ();

			// Move in endless end
			StartCoroutine (Call_EndlessEnd_Routine());

			// Remove mustache
			PlayerData_InGame.Instance.Player_EndlessEnd_MustacheLateRemove ();

			// Remove background
			PlayerData_InGame.Instance.endLevelScriptHolder.End_DisableBackANDenemies ();
		}
	}

	public void Pressed_QuitToMenu () {
		Invoke ("LoadMenu_Now", 1);
		endMenuAnimHolder.Play ("HUD (End) Leave Anim 1 (Legacy)");
		endGradBordersAnimHolder.Play ("HUD (End) Grads Enter Anim 1 (Legacy)");
	}

	public IEnumerator Call_EndlessEnd_Routine () {
		yield return new WaitForSeconds (0.5F);

		// Show the end message
		PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.Endless_TheEnd ();

		yield return new WaitForSeconds (2F);

		// Move in endless end
		PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.MoveIn_EndlessEnd ();
	}

	public void Popup_Title_ChangeToGameOver () {
		Popup_Title_NameChange (2);
	}

	public void Popup_Title_NameChange (int whichTitle) {
		if (whichTitle == 1) {
			switch (Random.Range (0,3)) {
			case 0:
				statNumber_gameoverTitle.text = "?ﯽﻟﺎﻤﺷﻮﮔ ﻪﻣﺍﺩﺍ";
				break;
			case 1:
				statNumber_gameoverTitle.text = "!ﻮﺸﻧ ﻢﯿﻠﺴﺗ";
				break;
			case 2:
				statNumber_gameoverTitle.text = "!ﺖﺴﯿﻧ ﺰﯾﺎﺟ ﻞﻠﻌﺗ";
				break;
			default:
				statNumber_gameoverTitle.text = "!ﺖﺴﯿﻧ ﺰﯾﺎﺟ ﻞﻠﻌﺗ";
				break;
			}
		} else {
			switch (Random.Range (0,4)) {
			case 0:
				statNumber_gameoverTitle.text = "...ﺖﻓﺭ ﻢﯾﺪﺷ ﻩﺭﺎﭽﯿﺑ";
				break;
			case 1:
				statNumber_gameoverTitle.text = "...ﺩﺍﺩ ﯽﺑ ﺩﺍﺩ ﯼﺍ";
				break;
			case 2:
				statNumber_gameoverTitle.text = "...ﺵﺎﺑﻭﺍ ﻦﯾﺍ ﺮﺑ ﺖﻨﻌﻟ";
				break;
			case 3:
				statNumber_gameoverTitle.text = "...ﻢﺣﺭ ﯽﺑ ﺭﺎﮔﺯﻭﺭ ﯼﺍ";
				break;
			default:
				statNumber_gameoverTitle.text = "...ﺖﻓﺭ ﻢﯾﺪﺷ ﻩﺭﺎﭽﯿﺑ";
				break;
			}
		}
	}

	public void LoadMenu_Now () {
		StartCoroutine ( PlayerData_Main.Instance.LoadScene_Async_Slow ("Menu_Scene"));

//		SceneManager.LoadScene ("Menu_Scene");
	}
}
