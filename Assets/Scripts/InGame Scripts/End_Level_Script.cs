﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;
using System.Collections;
using Fabric.Answers;

public class End_Level_Script : MonoBehaviour {

	public Reward_Machine_Script rewardMachineScriptHolder;
	public EnemySpawner enemySpawnerScriptHolder;
	public EndlessSpawner endlessSpawnerScriptHolder;

	public Transform enemiesSpawnHereTransHolder;
	public Transform transMustacheParentHUD_Holder;
	public Transform[] transAghdasiDialogueParentArr;

	public GameObject objEyesParentHolder;
	public GameObject objWorldBackgroundsAllHolder;
	public GameObject endingActivatorHolder;
	public GameObject starObj_1;
	public GameObject starObj_2;
	public GameObject starObj_3;
	public Animation endMenuAnimHolder;
	public Animation animAghdasiChangeHolder;
	public Animation animAghdasiIdleHolder;
	public Animation animPopUpMoverHitUpHolder;

	public Canvas canvasEndMenuHolder;

	public Text statNumber_enemiesKilled;
	public Text statNumber_enemiesTotal;    
	public Text statNumber_mustacheGained;
	public Text statNumber_mustacheMultiplier;
	public Text statNumber_percentCorrect;
	public Text statNumber_LevelNumber;     

	public Text text_VictoryTitle;
	public Text text_VictoryDialogue;

	public Popup_Tooltips_Script toolTipPopUpScript_UnmissableHolder;

	public Animation endGradBordersAnimHolder;

	public GameObject[] aghdasiVicFacesObjArr;
	public GameObject[] aghdasiFlagObjArr;

	public Transform transFirstEnemyRef;

	public ParticleSystem[] particleStarFullFlareArr;

	public ParticleSystem starParticle_1;
	public ParticleSystem starParticle_2;
	public ParticleSystem starParticle_3;

	public int levelNumber_End;
	public int worldNumber_End;

	// For endless to get the private number
	public int intPercentCorretHit_Endless;

//	public LevelData_Class[] world_LevelDataArr;

	[Header ("For Finishing Final Available Levels / End of Version")]
	public GameObject objEndOfVersionHolder;

	private float PlayerTotalHits;
	private float PlayerTotalMisses;
	private float PlayerMissesThreshold;
	[SerializeField]
	private float percentOfCorrectHits;

	[SerializeField]
	private int percentOfCorrectHits_INT;
	private int allLevelsPerWorld;
//	private int numberOfStage;
	private int starCounterForTitleINT;

	// Sorry for repeat
	private int starCounterForAghdasiINT;

	private string nameOfStage;
	private string multiplierBufferString;
	private Animation starAnim_1;
	private Animation starAnim_2;
	private Animation starAnim_3;

	private int mustacheGained_BeforeMultipler;
	private int mustacheGained_AfterMultipler;

	void Awake () {
		endingActivatorHolder.SetActive (false);
		starCounterForTitleINT = 0;
	}
	void Start () {
		starAnim_1 = starObj_1.GetComponent<Animation> ();
		starAnim_2 = starObj_2.GetComponent<Animation> ();
		starAnim_3 = starObj_3.GetComponent<Animation> ();
		//
//		starParticle_1 = starObj_1.GetComponentInChildren<ParticleSystem> ();
//		starParticle_2 = starObj_2.GetComponentInChildren<ParticleSystem> ();
//		starParticle_3 = starObj_3.GetComponentInChildren<ParticleSystem> ();

		starObj_1.SetActive (false);
		starObj_2.SetActive (false);
		starObj_3.SetActive (false);

		if (PlayerData_Main.Instance != null) {
//			numberOfStage = PlayerData_Main.Instance.LevelNumber;
			PlayerData_Main.Instance.GetFullName_Level ();
			nameOfStage = PlayerData_Main.Instance.LevelNumber_FullName;
//			statNumber_LevelNumber.text = nameOfStage;
		}

		// Achievement resets

		// Achievement for 8 power ups in one level
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_PowerUpUse_8in1_Reset ());

			PlayerData_InGame.Instance.p1_1Health4Times_Counter = 0;
//			StartCoroutine (AchievementManager.Instance.AchieveProg_1Health4Times_Reset ());
		}

		// Disable canvas at start
		StartCoroutine (EndMenu_CanvasController(false));
	}

	public void EndCheck_Start () {
		Invoke ("EndCheck_Checker", 0.5F);
	}

	public bool IsEnemyEmpty_Checker () {
		if (enemiesSpawnHereTransHolder.childCount == 0) {
			return true;
		} 
		else {
			return false;
		}
	}

	void EndCheck_Checker () {
		if (enemiesSpawnHereTransHolder.childCount == 0) {
			if (PlayerData_InGame.Instance.p1_HP_Curr > 0) {
				Invoke ("End_RewardCheck", 0.2F);
				PlayerData_InGame.Instance.gameplayGestureAllowed = false;
			}
		} else {
			Invoke ("EndCheck_Checker", 0.2F);
		}
	}

	public void FirstKillCheck_Start () {
		transFirstEnemyRef = enemiesSpawnHereTransHolder.GetChild (0).GetChild(0).GetChild(0).GetComponent<Transform> ();
	}
		
	public bool CameraEndCheck_EachKill () {
		if (enemiesSpawnHereTransHolder.childCount == 0) {
			return true;
		} else {
			return false;
		}
	}


	public void EndlessCheck_EachKill () {
//		Debug.LogWarning ("EndlessCheck_EachKill. ChildCount = " + enemiesSpawnHereTransHolder.childCount);

		if (enemiesSpawnHereTransHolder.childCount == 0) {
			if (PlayerData_InGame.Instance.p1_HP_Curr > 0) {
				if (!PlayerData_InGame.Instance.lastEnemyWasKilled) {
					PlayerData_InGame.Instance.lastEnemyWasKilled = true;

					// To allow next enemy be spawned
					endlessSpawnerScriptHolder.ContinueSpawning ();
				}
			}
		}
	}

	public void EndCheck_EachKill () {
		if (enemiesSpawnHereTransHolder.childCount == 0) {
			if (PlayerData_InGame.Instance.p1_HP_Curr > 0) {
				if (!PlayerData_InGame.Instance.lastEnemyWasKilled) {

					// Untouchable / no damage star
					if (PlayerData_InGame.Instance.p1_HP_Curr < PlayerData_InGame.Instance.p1_HP_Start) {
						PlayerData_InGame.Instance.stats_Stars_Untouchable = false;
					}

					// Kill all clingy
					PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_KillAll ();
					
//					Debug.LogWarning ("END Player Check BEFORE 444");

					// To make sure player doesn't die the moment they kill last enemy
					PlayerData_InGame.Instance.p1_Invincible = true;
					PlayerData_InGame.Instance.p1_ShieldChanceTotal = 100;

					PlayerData_InGame.Instance.lastEnemyWasKilled = true;
					PlayerData_InGame.Instance.gameplayGestureAllowed = false;
					PlayerData_InGame.Instance.Player_EndGoSlowMo ();

					Invoke ("End_RewardCheck", 1F);

					// Remove Screen Effects
					SetupEnd_RemoveScreenEffects ();

//					Debug.LogWarning ("Vic 1");

					// Remove Victory 2
					End_VictoryHUD_Now2 ();

					// Enable Victory canvas
					StartCoroutine (EndMenu_CanvasController(true));

					// Miss number
					End_GetMissNumberFrom ();

					// Level & World number
					End_GetNumbersFromSpawner ();

					// Quest for completing level
					if (QuestManager.Instance != null) {
						StartCoroutine (QuestManager.Instance.QuestProg_LevelComplete ());
					}

					// Achievements for end of level
					if (AchievementManager.Instance != null) {
						if (PlayerData_InGame.Instance.stats_ContinuesUsed > 0) {
							StartCoroutine (AchievementManager.Instance.AchieveProg_LevelComplete_ContinueUsed ());
						}

						End_LevelSpecificAchievement ();
						StartCoroutine (End_LateCheckAchieves ());

//						if (AchievementManager.Instance.AchieveProg_1Health4Times_Progress () > 3) {
//							StartCoroutine (AchievementManager.Instance.AchieveProg_1Health4Times_Increase ());
//						}
					}

//					Debug.LogWarning ("END Player Check SUCCESS!");
				}
			}
		} 
	}

	IEnumerator End_LateCheckAchieves () {
		yield return new WaitForSeconds (1.5F);
//		Debug.LogError ("Pipe hits NORMAL = " + PlayerData_InGame.Instance.PipeItemUsed_Normal);
//		Debug.LogError ("Pipe hits RESETTER = " + PlayerData_InGame.Instance.PipeItemUsed_Resetter);

		// Pipe No Hit / Not used
//		Debug.LogWarning ("Pipe No Hit Achieve = " + PlayerData_InGame.Instance.PipeItemGained_Bool);
//		Debug.LogWarning ("Pipe PipeItemUsed_Resetter = " + PlayerData_InGame.Instance.PipeItemUsed_Resetter);

		if (PlayerData_InGame.Instance.PipeItemGained_Bool
			&& (PlayerData_InGame.Instance.PipeItemUsed_Resetter == 0)) 
		{
			StartCoroutine (AchievementManager.Instance.AchieveProg_PipeNotHit ());
		}

		if ((PlayerData_InGame.Instance.p1_TotalHits_All == 0) && (PlayerData_InGame.Instance.p1_PowUp1_OnlyUsed == 0)) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_NoPunches ());
		}

		if (PlayerData_InGame.Instance.p1_1Health4Times_Counter > 3) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_1Health4Times_Complete ());
		}

//		if (AchievementManager.Instance != null) {
//			StartCoroutine (AchievementManager.Instance.AchieveProg_1Health4Times_Increase ());
//		}
	}

	void End_LevelSpecificAchievement () {
		switch (worldNumber_End) {
		case 1:
			switch (levelNumber_End) {
			case 3:
				if (PlayerData_InGame.Instance.PickupItemCounter_Picked == PlayerData_InGame.Instance.PickupItemCounter_Spawned) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_1_3 ());
				}
				break;
			case 8:
				if (PlayerData_InGame.Instance.p1_TotalHits_Misses == 0) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_1_8 ());
				}
				break;
			case 10:
				if (PlayerData_InGame.Instance.p1_PowUps_Used == 0 && PlayerData_InGame.Instance.stats_ContinuesUsed == 0) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_1_10 ());
				}
				break;
			default:
				break;
			}
			break;
		case 2:
			switch (levelNumber_End) {
			case 4:
				if (PlayerData_InGame.Instance.PickupItemCounter_Picked == PlayerData_InGame.Instance.PickupItemCounter_Spawned) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_2_4 ());
				}
				break;
			case 7:
				if (PlayerData_InGame.Instance.HedgehogItemCounter_Picked == PlayerData_InGame.Instance.HedgehogItemCounter_Spawned) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_2_7 ());
				}
				break;
			case 9:
				if (PlayerData_InGame.Instance.PickupItemCounter_Picked == 0) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_2_9 ());
				}
				break;
			case 10:
				if (PlayerData_InGame.Instance.p1_PowUps_Used == 0 && PlayerData_InGame.Instance.stats_ContinuesUsed == 0) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_2_10 ());
				}
				break;
			default:
				break;
			}
			break;
		case 3:
			switch (levelNumber_End) {
			case 5:
				if (PlayerData_InGame.Instance.p1_TotalHits_All < 80) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_3_5 ());
				}
				break;
			case 8:
				PlayerTotalHits = (float)PlayerData_InGame.Instance.p1_TotalHits_All;
				PlayerTotalMisses = (float)PlayerData_InGame.Instance.p1_TotalHits_Misses;
				if ((PlayerTotalMisses / PlayerTotalHits) > 0.35F) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_3_8 ());
				}

				break;
			case 10:
				if (PlayerData_InGame.Instance.p1_PowUps_Used == 0 && PlayerData_InGame.Instance.stats_ContinuesUsed == 0) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_UniqueLevel_3_10 ());
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	void End_GetMissNumberFrom () {
		toolTipPopUpScript_UnmissableHolder.stringToolTipsText = "% " + PlayerData_InGame.Instance.missesThresholdPercent.ToString () + " ﯼﻻﺎﺑ ﺖﻗﺩ";
	}

	void End_GetNumbersFromSpawner () {
		worldNumber_End = enemySpawnerScriptHolder.worldNumber;
		levelNumber_End = enemySpawnerScriptHolder.levelNumber;
	}

	void End_RewardCheck () {
//		Debug.LogError ("PlayerData_Main.Instance.PlayingNewestLevel = " + PlayerData_Main.Instance.PlayingNewestLevel);
//		Debug.LogError ("PlayerData_Main.Instance.PlayingALevelForTheFirstTime = " + PlayerData_Main.Instance.PlayingALevelForTheFirstTime);
//		Debug.LogError ("PlayerData_InGame.Instance.isThisNoReward = " + PlayerData_InGame.Instance.isThisNoReward);

		if (PlayerData_Main.Instance != null) {
			if (PlayerData_Main.Instance.PlayingNewestLevel && PlayerData_Main.Instance.PlayingALevelForTheFirstTime) {
				
				if (!PlayerData_InGame.Instance.isThisNoReward) {
					End_GetReward_Yes ();
				} else {
					End_GetReward_No ();
				}

			} else {
				End_GetReward_No ();
			}
		}

		// Meaning that testing from start in In-game rather than startup
		else {
			if (PlayerData_InGame.Instance.rewardTheLevel) {
				End_GetReward_Yes ();
			} else {
				End_GetReward_No ();
			}
		}
	}

	void End_GetReward_Yes () {
		Debug.LogWarning ("REWARD TIME!!!!!!!!");
//		Invoke ("End_RewardTime", 0.3F);
		Invoke ("End_RewardTime", 1.3F);
//		Invoke ("End_VictoryHUD_MustacheLate", 0.1F);
		Invoke ("End_VictoryHUD_MustacheLate", 1.2F);

//		Invoke ("End_VictoryHUD_Now2", 0.3F);
	}

	void End_GetReward_No () {
		Debug.LogWarning ("No Reward");

		Invoke ("End_BeginVictory_PauseNRest", 0.8F);
		Invoke ("End_VictoryHUD_MustacheLate", 1.2F);

//		Invoke ("End_VictoryHUD_Now2", 0.1F);

		// Old delays
//		Invoke ("End_BeginVictory_PauseNRest", 1F);
//		Invoke ("End_VictoryHUD_Now1", 1.2F);
	}

	void End_VictoryHUD_Now1 () {
		// Play HUD Vic Anims
		PlayerData_InGame.Instance.Player_Victory_HUD (1);
	}

	void End_VictoryHUD_Now2 () {
		// Play HUD Vic Anims
		PlayerData_InGame.Instance.Player_Victory_HUD (2);
	}

	void End_VictoryHUD_MustacheLate () {
		// Play HUD Vic Anims
		PlayerData_InGame.Instance.Player_Victory_MustacheLateRemove ();
	}

	void End_RewardTime () {
		rewardMachineScriptHolder.GetReward_InGame (enemySpawnerScriptHolder.levelRewardDataRef);
		rewardMachineScriptHolder.gameObject.SetActive (true);
		rewardMachineScriptHolder.CallFirstChest ();
	}

	public void End_BeginVictory_PauseNRest () {
		// If there were no awards, we need to pause here
		if (!EventTrigger.isPaused) {
			EventTrigger.PauseTheGame ();
		}

        // SOUNDD EFFECTS - Victory (done)
        if (MusicManager.Instance!=null)
        {
            MusicManager.Instance.PlayMusic_Victory();
        }

        Invoke ("End_BeginVictory_Rest", 0.2F);
	}

	void End_BeginVictory_Rest () {
		// Update player status
		PlayerData_InGame.Instance.stats_Stars_Completed = true;
		PlayerData_InGame.Instance.wonTheLevel = true;

		// Update playing for first time in case of restarts and reward stealing
		if (PlayerData_Main.Instance != null) {
			if (PlayerData_Main.Instance.PlayingALevelForTheFirstTime == true) {
				PlayerData_Main.Instance.PlayingALevelForTheFirstTime = false;
			}
		}

		// Setup End
		SetupEnd_All ();
	}

	void SetupEnd_RemoveScreenEffects () {
		if (EventTrigger.isFrozen) {
//			Debug.LogError ("UNFREEZE!");
			PlayerData_InGame.Instance.PlayerItem_Freeze_END_Deactivate ();
		}

		if (PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
			// Decrease the pipe amount until it reaches zero;
			PlayerData_InGame.Instance.PlayerItem_ItemDurabilityEnd (0);
		}

		if (PlayerData_InGame.Instance.p1_PowUp2_Active) {
			// End Pow Up 2 for victory
			PlayerData_InGame.Instance.Player_Victory_PowUp2End ();
		}
	}

	void SetupEnd_All () {
		// Show Stats & End Menu
		endingActivatorHolder.SetActive (true);
		endMenuAnimHolder.Play ("HUD (End) Enter Anim 1 (Legacy)");
		animAghdasiChangeHolder.Play ("End Popup - Vic Aghdasi Enter Anim 1 (Legacy)");

		// Moved this here so it is checked earlier and avoids the Aghdasi bug
		SetupEnd_UnmissableCalculate ();

		// Count Stars for Aghdasi Dialogue
		AghdasiDialogue_StarCount ();

		// Victory Dialogue
		AghdasiDialogueRandom_ViaTrans ();

		// Setup Stars (BUT in the new version, this is called by End Pop-up anim)
//		Debug.LogWarning ("Previous position of Setup Stars");
//		SetupEnd_Stars ();

		// Aghdasi starts moving now
		animAghdasiIdleHolder.Play ("End Popup - Vic Aghdasi Idle Normal Anim 1 (Legacy)");

		// Check newest level & world
		if (PlayerData_Main.Instance != null) {
			Check_IfNewLevel ();
		}

		// Also remove actual backgrounds
		StartCoroutine (End_DisableBackgrounds());
	}

	IEnumerator End_DisableBackgrounds () {
		yield return new WaitForSeconds (1.2F);

		objWorldBackgroundsAllHolder.SetActive (false);
	}

	IEnumerator End_DisableBackgroundsNOW () {
		yield return null;

		objWorldBackgroundsAllHolder.SetActive (false);
	}

	IEnumerator End_DisableEnemiesNOW () {
		yield return null;

		objWorldBackgroundsAllHolder.transform.parent.GetChild(0).gameObject.SetActive (false);
	}

	IEnumerator End_DisableEyesNOW () {
		yield return null;

		objEyesParentHolder.SetActive (false);
	}

	public void End_DisableBackANDenemies () {
		StartCoroutine (End_DisableBackANDenemiesRoutine ());
	}

	public IEnumerator End_DisableBackANDenemiesRoutine () {
		StartCoroutine (PlayerData_InGame.Instance.hudScriptHolder.DeathFullscreen_DisableRoutine ());

		yield return null;
		StartCoroutine (End_DisableBackgroundsNOW ());

		yield return null;
		StartCoroutine (End_DisableEnemiesNOW ());

		yield return null;
		StartCoroutine (End_DisableEyesNOW ());
	}

	void SetupEnd_UnmissableLastCheck () {
		// Achievement for 100 percent accuracy
		if (percentOfCorrectHits_INT > 99) {
			if (AchievementManager.Instance != null) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_LevelStar_Unmissable ());
			}
		}

		//		Debug.Log ("The numbers in order: " + PlayerTotalHits + " & " + PlayerTotalMisses + " & " + PlayerMissesTreshold + " -> " + (PlayerTotalMisses / PlayerTotalHits));
	}

	public void SetupEnd_UnmissableCalculate () {
		PlayerTotalHits = (float) PlayerData_InGame.Instance.p1_TotalHits_All;
		PlayerTotalMisses = (float) PlayerData_InGame.Instance.p1_TotalHits_Misses;
		PlayerMissesThreshold = PlayerData_InGame.Instance.missesThresholdPercent;
		PlayerMissesThreshold = 1 - (PlayerMissesThreshold / 100);
		percentOfCorrectHits = (PlayerTotalMisses / PlayerTotalHits);
		percentOfCorrectHits_INT = (int) (100 - percentOfCorrectHits * 100);
		if (percentOfCorrectHits_INT < 0) {
			percentOfCorrectHits_INT = 0;
		}

		if (percentOfCorrectHits > PlayerMissesThreshold) {
			// Player missed too many times
			if (PlayerData_InGame.Instance.stats_Stars_Unmissable) {
				PlayerData_InGame.Instance.stats_Stars_Unmissable = false;
			}
		}

		intPercentCorretHit_Endless = percentOfCorrectHits_INT;
	}

	public void SetupEnd_Stars () {
		// Calculate the status of misses star

		// Moved this earlier to make sure Aghdasi dialogue works properly
//		SetupEnd_UnmissableCalculate ();
		SetupEnd_UnmissableLastCheck ();

		// Give stats
		SetupEnd_Stats ();

		if (PlayerData_Main.Instance != null) {
			// Check world to find world NUMBER for levelDataArray
			switch (PlayerData_Main.Instance.WorldNumber) {
			case 1:
				Set_LevelData (PlayerData_Main.Instance.player_World1_LevelsDataArr);
				break;
			case 2:
				Set_LevelData (PlayerData_Main.Instance.player_World2_LevelsDataArr);
				break;
			case 3:
				Set_LevelData (PlayerData_Main.Instance.player_World3_LevelsDataArr);
				break;
			case 4:
				Set_LevelData (PlayerData_Main.Instance.player_World4_LevelsDataArr);
				break;
			case 5:
				Set_LevelData (PlayerData_Main.Instance.player_World5_LevelsDataArr);
				break;
			}
		} else {
			// Old Setup
			if (true) {
				StartCoroutine (StarEffects_New (starObj_1, starAnim_1, starParticle_1, 0.5F, 1));
				Debug.Log ("CHEAT Completion Star just unlocked!");
//						starObj_1.SetActive (true);

			}
	
			if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
				StartCoroutine (StarEffects_New (starObj_2, starAnim_2, starParticle_2, 1F, 2));
				Debug.Log ("CHEAT Untouchable Star just unlocked!");
//						starObj_2.SetActive (true);

			}
			if (PlayerData_InGame.Instance.stats_Stars_Unmissable) {
				StartCoroutine (StarEffects_New (starObj_3, starAnim_3, starParticle_3, 1.5F, 3));
				Debug.Log ("CHEAT Unmissable Star just unlocked!");
//						starObj_3.SetActive (true);

			}
		}
	}

	void StarCount_Increase () {
		if (PlayerData_Main.Instance != null) {
			// Increase player total stars
			PlayerData_Main.Instance.player_Stars++;

//			Debug.LogError ("World number index (w - 1) = " + (worldNumber_End - 1));

			PlayerData_Main.Instance.worldsGoldStarsArr [worldNumber_End - 1]++;

			// Achievement for gaining star
			if (AchievementManager.Instance != null) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_StarGain ());
			}

			// Achievement for 3 starring ALL levels in a world
			if (AchievementManager.Instance != null) {
				if (PlayerData_Main.Instance.worldsGoldStarsArr [worldNumber_End - 1] > 29) {
					switch (worldNumber_End - 1) {
					case 0:
						// World 1
						StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_1 ());
						break;
					case 1:
						// World 2
						StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_2 ());
						break;
					case 2:
						// World 3
						StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_3 ());
						break;
					case 3:
						// World 4
//						StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_4 ());
						break;
					case 4:
						// World 5
//						StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_5 ());
						break;
					default:
						break;
					}
				}
			}
		}
	}

	void Set_LevelData (LevelData_Class[] levelDataArr) {
		// Stars Setup
		if (PlayerData_InGame.Instance.stats_Stars_Completed) {
			if (!levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Level) {
				levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Level = true;

				StartCoroutine (StarEffects_New (starObj_1, starAnim_1, starParticle_1, 0.5F, 1));
				Debug.Log ("Completion Star just unlocked!");
				StarCount_Increase ();
			} else {
				StartCoroutine (StarEffects_Old (starObj_1, starAnim_1, 0.3F));
			}
		}

		if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
			if (!levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Untouchable) {
				levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Untouchable = true;

				StartCoroutine (StarEffects_New (starObj_2, starAnim_2, starParticle_2, 1F, 2));
				Debug.Log ("Untouchable Star just unlocked!");
				StarCount_Increase ();
			} else {
				StartCoroutine (StarEffects_Old (starObj_2, starAnim_2, 0.6F));
			}
		}

		if (PlayerData_InGame.Instance.stats_Stars_Unmissable) {
			if (!levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Unmissable) {
				levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Unmissable = true;

				StartCoroutine (StarEffects_New (starObj_3, starAnim_3, starParticle_3, 1.5F, 3));
				Debug.Log ("Unmissable Star just unlocked!");
				StarCount_Increase ();
			} else {
				StartCoroutine (StarEffects_Old (starObj_3, starAnim_3, 0.9F));
			}
		}

		// Save after getting stars
		SaveLoad.Save ();
	}

	public void Star_VisualUpdates (int howManyStars) {
		switch (howManyStars) {
		case 1:
			aghdasiVicFacesObjArr [0].SetActive (true);
			text_VictoryTitle.text = "!ﻥﺎﻣﺮﻬﻗ";
			break;
		case 2:
			aghdasiVicFacesObjArr [1].SetActive (true);
			text_VictoryTitle.text = "!!ﻥﺍﻮﻠﻬﭘ";
			break;
		case 3:
			Invoke ("AghdasiIdleSuperbAnimate", 0.5F);
//			animAghdasiIdleHolder.Play ("End Popup - Vic Aghdasi Idle Superb Anim 1 (Legacy)");
			animAghdasiChangeHolder.Play ("End Popup - Vic Aghdasi Change Anim 1 (Legacy)");

//			aghdasiVicFacesObjArr [2].SetActive (true);
			text_VictoryTitle.text = "!!!ﻥﺍﻮﻠﻬﭘ ﻥﺎﻬﺟ";
			break;
		default:
			break;
		}
	}

	public void AghdasiIdleSuperbAnimate () {
		AghdasiFlagRandom ();
		animAghdasiIdleHolder.Play ("End Popup - Vic Aghdasi Idle Superb Anim 1 (Legacy)");
	}

	public void AghdasiFlagRandom () {
		switch (Random.Range (1,4)) {
		case 1:
			aghdasiFlagObjArr [0].SetActive (true);
			break;
		case 2:
			aghdasiFlagObjArr [1].SetActive (true);
			break;
		case 3:
			aghdasiFlagObjArr [2].SetActive (true);
			break;
		default:
			aghdasiFlagObjArr [0].SetActive (true);
			break;
		}
	}
		
	public void AghdasiDialogue_StarCount () {
		starCounterForAghdasiINT = 0; 
		if (PlayerData_InGame.Instance.stats_Stars_Completed) {
			starCounterForAghdasiINT++;
		}
		if (PlayerData_InGame.Instance.stats_Stars_Untouchable) {
			starCounterForAghdasiINT++;
		}
		if (PlayerData_InGame.Instance.stats_Stars_Unmissable) {
			starCounterForAghdasiINT++;
		}
	}

	public void AghdasiDialogueRandom_ViaTrans () {
		switch (starCounterForAghdasiINT) {
		case 1:
			transAghdasiDialogueParentArr[0].GetChild (Random.Range (0, 7)).gameObject.SetActive (true);
			break;
		case 2:
			transAghdasiDialogueParentArr[1].GetChild (Random.Range (0, 7)).gameObject.SetActive (true);
			break;
		case 3:
			transAghdasiDialogueParentArr[2].GetChild (Random.Range (0, 7)).gameObject.SetActive (true);
			break;
		default:
			break;
		}
	}

	// OLD
	public void AghdasiDialogueRandom_ViaText_OLD () {
		switch (Random.Range (1, 5)) {
		case 1:
			text_VictoryDialogue.text = "!ﻼﮑﯾﺭﺎﺑ";
			break;
		case 2:
			text_VictoryDialogue.text = "!ﻦﯾﺮﻓﺁ";
			break;
		case 3:
			text_VictoryDialogue.text = "!ﻦﯿﺘﺷﺎﮐ ﻞﮔ";
			break;
		default:
			text_VictoryDialogue.text = "!ﻡﺮﮔ ﻥﻮﺘﻣﺩ";
			break;
		}
	}

	IEnumerator StarEffects_New (GameObject starObj, Animation starAnim, ParticleSystem starParticle, float delayTime, int whichStarFullParticle) {
		yield return new WaitForSeconds (delayTime);
		starObj.SetActive (true);
		starAnim.Play ("Menu - One Star NEW Anim 1 (Legacy)");
		yield return new WaitForSeconds (0.18F);
		animPopUpMoverHitUpHolder.Play ();
		yield return new WaitForSeconds (0.02F);
		starParticle.Play ();
		starCounterForTitleINT++;
		Star_VisualUpdates (starCounterForTitleINT);
		particleStarFullFlareArr [whichStarFullParticle - 1].Play ();

//        Debug.Log("Star Hit... NEW");

        // SOUNDD EFFECTS - Victory Star Hit But NEEDS DELAY (done) 3 instance seda niaze
        if (SoundManager.Instance!= null)
        {
            StartCoroutine (SoundManager.Instance.CoPlayDelayedClip(SoundManager.Instance.StarHit, 0.01f));
        }
	}

	IEnumerator StarEffects_Old (GameObject starObj, Animation starAnim, float delayTime) {
		yield return new WaitForSeconds (delayTime);
		starObj.SetActive (true);
//		starAnim.Play ("Menu - One Star OLD Anim 1 (Legacy)");
		starCounterForTitleINT++;
		Star_VisualUpdates (starCounterForTitleINT);

//		Debug.Log("Star Hit... OLD");

		// SOUNDD EFFECTS - Victory Star Hit But NEEDS DELAY (done) 3 instance seda niaze
		if (SoundManager.Instance!= null)
		{
			StartCoroutine(SoundManager.Instance.CoPlayDelayedClip(SoundManager.Instance.StarHit, 0.01f));
		}
	}

//	void Update () {
//		Debug.LogError ("Mustache gained stat = " + PlayerData_InGame.Instance.stats_MustacheGained);
//	}

	public void SetupEnd_MustacheMultiCalculate () {
		if (PlayerData_InGame.Instance.p1_MustacheMultiplier_Cur > 1) {
			//			Debug.LogError ("Mustache BEFORE multiplier = " + PlayerData_InGame.Instance.stats_MustacheGained);

			mustacheGained_BeforeMultipler = PlayerData_InGame.Instance.stats_MustacheGained;
			PlayerData_InGame.Instance.stats_MustacheGained = Mathf.RoundToInt ((float)PlayerData_InGame.Instance.stats_MustacheGained * PlayerData_InGame.Instance.p1_MustacheMultiplier_Cur);
			mustacheGained_AfterMultipler = PlayerData_InGame.Instance.stats_MustacheGained;

			//			Debug.LogError ("Mustache AFTER multiplier = " + PlayerData_InGame.Instance.stats_MustacheGained);
			//			Debug.LogError ("mustacheGained_AfterMultipler = " + mustacheGained_AfterMultipler);
			//			Debug.LogError ("Mustache Difference = " + (mustacheGained_AfterMultipler - mustacheGained_BeforeMultipler).ToString ());

			StartCoroutine (PlayerData_InGame.Instance.PlayerMustache_Gain (mustacheGained_AfterMultipler - mustacheGained_BeforeMultipler));

			// Moved to outside this function (Into setupend stats)
//			multiplierBufferString = "<size=42>+</size>" + "<size=33>%</size> " + (Mathf.Round(PlayerData_InGame.Instance.p1_MustacheMultiplier_Cur * 100) - 100).ToString();
//			transMustacheParentHUD_Holder.localPosition = new Vector3 (0, 500, 0);
//			statNumber_mustacheMultiplier.gameObject.SetActive (true);
		} else {
			
			// Moved to outside this function (Into setupend stats)
//			multiplierBufferString = "";
//			statNumber_mustacheMultiplier.gameObject.SetActive (false);
		}
	}

	IEnumerator SetupEnd_Texts () {
		yield return null;
		statNumber_enemiesKilled.text = PlayerData_InGame.Instance.stats_EnemiesKilled.ToString();
		statNumber_enemiesTotal.text = PlayerData_InGame.Instance.stats_EnemiesTotalNumber.ToString();
		statNumber_mustacheGained.text = PlayerData_InGame.Instance.stats_MustacheGained.ToString();
		statNumber_mustacheMultiplier.text = multiplierBufferString;
		statNumber_percentCorrect.text = percentOfCorrectHits_INT.ToString ();
	}

	void SetupEnd_Stats () {
		// Special for Mustache Multiplier
		SetupEnd_MustacheMultiCalculate ();

		if (PlayerData_InGame.Instance.p1_MustacheMultiplier_Cur > 1) {
			// Should rounded amount text
			multiplierBufferString = "<size=42>+</size>" + "<size=33>%</size> " + (Mathf.Round(PlayerData_InGame.Instance.p1_MustacheMultiplier_Cur * 100) - 100).ToString();

			transMustacheParentHUD_Holder.localPosition = new Vector3 (0, 500, 0);
			statNumber_mustacheMultiplier.gameObject.SetActive (true);
		} else {
			multiplierBufferString = "";
			statNumber_mustacheMultiplier.gameObject.SetActive (false);
		}

		// End numbers used to be here but were moved to separate coroutine
		StartCoroutine (SetupEnd_Texts ());

		// End level reports for design
		if (PlayerData_Main.Instance != null) {
			if (!PlayerData_Main.Instance.isFullBuild) {
				Debugs_ResultsForNonBuild ();
			}
		} else {
			Debugs_ResultsForNonBuild ();
		}

		// Analytics Event
		End_Analytics_Event ();
	}

	public void Debugs_ResultsForNonBuild () {
		Debug.LogError ("Results: ");

		Debug.Log ("Player Total Hits = " + PlayerData_InGame.Instance.p1_TotalHits_All);
		Debug.Log ("Player Critical Hits = " + PlayerData_InGame.Instance.p1_TotalHits_Crits
			+ "    Percent: %" + ((float)PlayerData_InGame.Instance.p1_TotalHits_Crits/(float)PlayerData_InGame.Instance.p1_TotalHits_All) * 100 );
		Debug.Log ("Player Missed Hits = " + PlayerData_InGame.Instance.p1_TotalHits_Misses
			+ "    Percent: %" + ((float)PlayerData_InGame.Instance.p1_TotalHits_Misses/(float)PlayerData_InGame.Instance.p1_TotalHits_All ) * 100);
		
		Debug.Log ("Player Hurt Times = " + PlayerData_InGame.Instance.p1_TotalHurts);
		Debug.Log ("Player Shielded = " + PlayerData_InGame.Instance.p1_TotalShields
			+ "    Percent: %" + ((float)PlayerData_InGame.Instance.p1_TotalShields/(float)PlayerData_InGame.Instance.p1_TotalHurts) * 100 );
		
		Debug.Log ("Enemies Killed = " + PlayerData_InGame.Instance.p1_TotalKills_All);
		Debug.Log ("Enemies LuckyExploded = " + PlayerData_InGame.Instance.p1_TotalKills_LuckyExplode
			+ "    Percent: %" + ((float)PlayerData_InGame.Instance.p1_TotalKills_LuckyExplode/(float)PlayerData_InGame.Instance.p1_TotalKills_All) * 100 );
		
		Debug.LogError ("END Of Results! ");
	}

	void End_Analytics_Event () {
		if (PlayerData_Main.Instance != null) {
			Answers.LogLevelEnd (PlayerData_Main.Instance.WorldNumber.ToString () + "-" + PlayerData_Main.Instance.LevelNumber.ToString (),
				customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
				{"Continues Used", PlayerData_InGame.Instance.stats_ContinuesUsed }, 
				{"Power Up 1 Used", PlayerData_InGame.Instance.p1_PowUp1_Count_Own - PlayerData_InGame.Instance.p1_PowUp1_Count_Ready},
				{"Power Up 2 Used", PlayerData_InGame.Instance.p1_PowUp2_Count_Own - PlayerData_InGame.Instance.p1_PowUp2_Count_Ready},
				{"Correct Gesture Percent", percentOfCorrectHits_INT},
				{"Player Damage Taken", PlayerData_InGame.Instance.p1_TotalHurts},
				{"Player HP", PlayerData_InGame.Instance.p1_HP_Start}, 
				{"Player Damage", PlayerData_InGame.Instance.p1_Damage_Base}
			});
		}
	}

	public void Pressed_EndMenuFinish () {
		Invoke ("LoadMenu_Now", 1.2F);

		// Call loading anim
		Invoke ("Loading_AnimNow", 0.5F);

		endMenuAnimHolder.Play ("HUD (End) Leave Anim 1 (Legacy)");

		// End grads for end of end-screen (Going to menu / reload)
		endGradBordersAnimHolder.Play ("HUD (End) Grads Enter Anim 1 (Legacy)");
	}

	public void Loading_AnimNow () {
		if (PlayerData_Main.Instance != null) {
			StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_Start (0.4F));
		}
	}

	public void LoadMenu_Now () {
		StartCoroutine ( PlayerData_Main.Instance.LoadScene_Async_Slow ("Menu_Scene"));

//		SceneManager.LoadScene ("Menu_Scene");
	}

	// Check if new should USE PlayerDataMain Newest Level

	public IEnumerator EndMenu_CanvasController (bool isActive) {
		canvasEndMenuHolder.enabled = isActive;
		yield return null;
	}

	void Check_IfNewLevel () {
		allLevelsPerWorld = PlayerData_Main.Instance.LevelsPerWorld;

//		Debug.LogError ("Level: " + PlayerData_Main.Instance.LevelNumber);
//		Debug.LogError ("World: " + PlayerData_Main.Instance.WorldNumber);

		// Check if playing in latest world
		if (PlayerData_Main.Instance.WorldNumber == PlayerData_Main.Instance.player_WorldsUnlocked) {
//			Debug.Log ("WORLD Checked!");

			// Check if playig latest level of that world
			if (PlayerData_Main.Instance.LevelNumber == PlayerData_Main.Instance.player_LevelsUnlocked) {
//				Debug.Log ("LEVEL Checked!");

				// Check if still in new world available state (Still remain)
				if (!PlayerData_Main.Instance.Show_NewWorld_Remain) {

					// Check if next world should be unlocked
					if (PlayerData_Main.Instance.LevelNumber == allLevelsPerWorld) {
//						Debug.Log ("Make Available: NEW WORLD BUTTON");

						// Unlock new world button
						if (!PlayerData_Main.Instance.Show_NewWorld_Remain) {

							// For final game / when post world-1 worlds will be unlocked
							if (PlayerData_Main.Instance.WorldNumber != PlayerData_Main.Instance.lastPossibleWorld_ForThisVer) {
								Debug.LogError ("PlayerData_Main.Instance.WorldNumber = " + PlayerData_Main.Instance.WorldNumber);
								Debug.LogError ("PlayerData_Main.Instance.lastPossibleWorld_ForThisVer = " + PlayerData_Main.Instance.lastPossibleWorld_ForThisVer);
								PlayerData_Main.Instance.Show_NewWorld_Allow = true;
							} 

							// Meaning that player has finished the last level of current version
							else {
								objEndOfVersionHolder.SetActive (true);
							}

							// Achievement for completing chapters / worlds
							if (AchievementManager.Instance != null) {
								switch (PlayerData_Main.Instance.WorldNumber) {
								case 1:
									StartCoroutine (AchievementManager.Instance.AchieveProg_WorldComplete_1 ());
									break;
								case 2:
									StartCoroutine (AchievementManager.Instance.AchieveProg_WorldComplete_2 ());
									break;
								case 3:
									StartCoroutine (AchievementManager.Instance.AchieveProg_WorldComplete_3 ());
									break;
								case 4:
//									StartCoroutine (AchievementManager.Instance.AchieveProg_WorldComplete_4 ());
									break;
								case 5:
//									StartCoroutine (AchievementManager.Instance.AchieveProg_WorldComplete_5 ());
									break;
								default:
									break;
								}
							}

//							PlayerData_Main.Instance.player_WorldsUnlocked++;
//							PlayerData_Main.Instance.player_LevelsUnlocked = 1;
						}

					} else {

						// Unlock new level
						Debug.Log ("Unlock NEW LEVEL");
						Debug.Log ("Unlock NEW LEVEL + " + PlayerData_Main.Instance.isFirstTimePlaying);
						PlayerData_Main.Instance.Show_NewLevel_Allow = true;
						PlayerData_Main.Instance.player_LevelsUnlocked++;

						// Falsify first time playing if it's true
						if (PlayerData_Main.Instance.isFirstTimePlaying) {
							PlayerData_Main.Instance.isFirstTimePlaying = false;

							// Achievement for completing FIRST level
							if (AchievementManager.Instance != null) {
								StartCoroutine (AchievementManager.Instance.AchieveProg_LevelComplete_First ());
							}

							// Set last scene bool from player main
							PlayerData_Main.Instance.LastScene_WasInGame_ChangeTo (false);

							// To make sure we get full intro for the first time
							PlayerData_Main.Instance.isFirstToMenu = true;
						}

					}
				}
			}
		}
	}
}
