﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {

	private Dictionary <string, UnityEvent> eventDictionary;
	private static EventManager eventManager;
	public static EventManager Instance {
		get
		{ 
			if (!eventManager) {
				eventManager = FindObjectOfType (typeof (EventManager)) as EventManager;
				if (!eventManager) {
					Debug.LogError ("Fucked! No Manager.");
				} else {
					eventManager.Initialize ();
				}
			}

			return eventManager;
		}
	}

	void Initialize() {
		if (eventDictionary == null) {
			eventDictionary = new Dictionary <string, UnityEvent> ();
		}
	}

	public static void StartListening (string eventName, UnityAction listener) {
		UnityEvent anEvent = null;
		if (Instance.eventDictionary.TryGetValue (eventName, out anEvent)) {
			anEvent.AddListener (listener);
		} else {
			anEvent = new UnityEvent ();
			anEvent.AddListener (listener);
			Instance.eventDictionary.Add (eventName, anEvent);
		}
	}

	public static void StopListening (string eventName, UnityAction listener) {
		if (eventManager == null)
			return;
		UnityEvent anEvent = null;
		if (Instance.eventDictionary.TryGetValue (eventName, out anEvent)) {
			anEvent.RemoveListener (listener);
		}
	}

	public static void TriggerEvent (string eventName) {
		UnityEvent anEvent = null;
		if (Instance.eventDictionary.TryGetValue (eventName, out anEvent)) {
			anEvent.Invoke ();
		}
	}
}
