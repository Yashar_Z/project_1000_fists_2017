﻿using UnityEngine;
using System.Collections;

public class EventTrigger : MonoBehaviour {

	public static bool isCorrectHit;
	public static bool isPaused;
	public static bool isFrozen;
	public static string theKey_Direction;
	public static string theKey_HappyTimeDirection;
	public static string theKey_Shape1 = "U";
	public static string theKey_Shape2 = "U";
	private bool isSlowed;

	// Update is called once per frame (Added Not to remove)
//	void Update() {

	void Update_Old () {
		
//		Debug.LogWarning (theKey_Shape1 [0] + " - " + " - " +
//		theKey_Shape2 [0] + " - ");

		if (Input.GetKeyDown ("k")) {
			Debug.LogError ("K!");
			EventManager.TriggerEvent ("KillAll");
		}
		if (Input.GetKeyDown ("w")) {
//			Debug.Log ("w!!");
//			theKey = "U";
			EventManager.TriggerEvent ("KillSingle");
		}
		if (Input.GetKeyDown ("s")) {
//			Debug.Log ("w!!");
//			theKey = "D";
			EventManager.TriggerEvent ("KillSingle");
		}
		if (Input.GetKeyDown ("a")) {
//			Debug.Log ("w!!");
//			theKey = "L";
			EventManager.TriggerEvent ("KillSingle");
		}
		if (Input.GetKeyDown ("d")) {
//			Debug.Log ("w!!");
//			theKey = "R";
			EventManager.TriggerEvent ("KillSingle");
		}
		if (Input.GetKeyDown ("t")) {
//			EventManager_Script.TriggerEvent ("Paused");
			if (!isSlowed) {
				Time.timeScale = 0.3F;
			} else {
				Time.timeScale = 1F;
			}
			isSlowed = !isSlowed;
		}
		if (Input.GetKeyDown ("p")) {
			if (!isPaused) {
				PauseTheGame ();
			} else {
				UnPauseTheGame ();
			}
//			if (!isPaused) {
//				Time.timeScale = 0.1F;
//			} else {
//				Time.timeScale = 1F;
//			}
		}
		if (Input.GetKeyDown ("f")) {
			if (!isFrozen) {
				FreezeTheGame ();
			} else {
				UnFreezeTheGame ();
			}
		}
		if (Input.GetKeyDown ("h")) {
			PlayerData_InGame.Instance.p1_Item_ArrowHappyTime = !PlayerData_InGame.Instance.p1_Item_ArrowHappyTime;
			EventManager.TriggerEvent ("ArrowHappyTime");
		}
	}

	public static void HappyTime_Do () {
		PlayerData_InGame.Instance.p1_Item_ArrowHappyTime = true;
		EventManager.TriggerEvent ("ArrowHappyTime");
	}

	public static void HappyTime_Undo () {
		PlayerData_InGame.Instance.p1_Item_ArrowHappyTime = false;
		EventManager.TriggerEvent ("ArrowHappyTime");
	}

	public static void PauseTheGame () {
		EventManager.TriggerEvent ("Pause");
		isPaused = true;
	}

	public static void UnPauseTheGame () {
		EventManager.TriggerEvent ("Pause");
		isPaused = false;
	}

	public static void FreezeTheGame () {
		EventManager.TriggerEvent ("Freeze");
		isFrozen = true;
	}

	public static void UnFreezeTheGame () {
		EventManager.TriggerEvent ("Freeze");
		isFrozen = false;
	}

	void Awake () {
		isSlowed = false;
		isPaused = false;
		isFrozen = false;
	}
}
