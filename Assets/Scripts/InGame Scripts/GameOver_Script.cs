﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver_Script : MonoBehaviour {

	public Text text_overStats_KilledCurrHolder;
	public Text text_overStats_EnemiesTotalHolder;
	public Text text_overStats_MustacheGainedHolder;

	private int overStats_KilledCurr;
	private int overStats_KilledTotal;
	private int overStats_EnemiesTotal;
	private int overStats_MustacheCurr;
	private int overStats_MustacheTotal;

	// Use this for initialization
	void Start () {
		Setup_Numbers ();
	}

	void Setup_Numbers () {
		overStats_KilledCurr = 0;
		overStats_KilledTotal = 0;

		overStats_MustacheCurr = 0;
		overStats_MustacheTotal = 0;

		overStats_KilledTotal = PlayerData_InGame.Instance.stats_EnemiesKilled;
		overStats_EnemiesTotal = PlayerData_InGame.Instance.stats_EnemiesTotalNumber;
		overStats_MustacheTotal = PlayerData_InGame.Instance.stats_MustacheGained;

		text_overStats_KilledCurrHolder.text = overStats_KilledCurr.ToString();
		text_overStats_EnemiesTotalHolder.text = overStats_EnemiesTotal.ToString();
		text_overStats_MustacheGainedHolder.text = overStats_MustacheCurr.ToString();

		Invoke ("StartCounting", 0.2F);
	}

	public void StartCounting () {
		StartCoroutine (KilledNumber_IncreaseByOne());
		StartCoroutine (MustacheNumber_IncreaseByOne());
	}

	public IEnumerator KilledNumber_IncreaseByOne () {
		if (overStats_KilledCurr != overStats_KilledTotal) {
			if (overStats_KilledCurr + 4 < overStats_KilledTotal) {
				overStats_KilledCurr = overStats_KilledCurr + 4;
			} else {
				overStats_KilledCurr = overStats_KilledCurr + 1;
			}

			yield return new WaitForSeconds (0.1F);
			StartCoroutine (KilledNumber_IncreaseByOne());
		}
		text_overStats_KilledCurrHolder.text = overStats_KilledCurr.ToString();
	}

	public IEnumerator MustacheNumber_IncreaseByOne () {
		if (overStats_MustacheCurr != overStats_MustacheTotal) {
			if ((overStats_MustacheCurr + 444) <= overStats_MustacheTotal) {
				overStats_MustacheCurr = overStats_MustacheCurr + 444;
				yield return new WaitForSeconds (0.04F);
			} else {
				if ((overStats_MustacheCurr + 44) <= overStats_MustacheTotal) {
					overStats_MustacheCurr = overStats_MustacheCurr + 44;
					yield return new WaitForSeconds (0.07F);
				} else {
					if ((overStats_MustacheCurr + 8) <= overStats_MustacheTotal) {
						overStats_MustacheCurr = overStats_MustacheCurr + 8;
						yield return new WaitForSeconds (0.09F);
					} else {
						overStats_MustacheCurr = overStats_MustacheCurr + 1;
						yield return new WaitForSeconds (0.11F);
					}
				}
			}
			StartCoroutine (MustacheNumber_IncreaseByOne());
		}
		text_overStats_MustacheGainedHolder.text = overStats_MustacheCurr.ToString();
	}
}
