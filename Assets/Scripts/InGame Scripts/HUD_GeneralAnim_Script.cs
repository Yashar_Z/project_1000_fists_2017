﻿using UnityEngine;
using System.Collections;

public class HUD_GeneralAnim_Script : MonoBehaviour {

	public Bell_Script bellScriptHolder;

	public void SetHUDlayerOrder () {
		PlayerData_InGame.Instance.HP_SortingOrderTo5 ();
	}

	public void CallBell_Short () {
		if ((PlayerData_Main.Instance != null) && !PlayerData_Main.Instance.isFirstTimePlaying) {
			if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
				bellScriptHolder.BellAnim_CallShort ();
			}
		} else {
			StartCoroutine (bellScriptHolder.DingOnly_TimedDisableEnable ());
		}
//		StartCoroutine ( bellScriptHolder.WaveTitleAnim_Call (false) );
	}

	public void CallBell_DingNow () {
		bellScriptHolder.BellAnim_CallDingOnly ();
	}

	// Don't use it
	public void CallEndlessScore () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			PlayerData_InGame.Instance.hudScriptHolder.HUD_ScoreDisplay_Now	();
		}
	}
}
