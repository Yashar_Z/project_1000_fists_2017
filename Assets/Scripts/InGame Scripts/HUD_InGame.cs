﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class HUD_InGame : MonoBehaviour {

	public Text hud_Health_Show;
	public Text hud_Moustache_Show;

	public Text hud_Pause_WorldName;
	public Text hud_Pause_LevelName;

	public Camera_InGame_Script camScriptHolder;
	public Mustache_Display_Script mustacheDisplayScriptHolder;
	public Shop_Popup_Script shopPopScriptHolder;
	public PowerUps_Script powUpsScriptHolder;
	public Endless_PotionsController endlessPotionsController;
	public Popup_EndlessEnd_Script popUpEndlessEndScript;

	// Endless Stuff
	public Endless_ScoreController endlessScoreController;

	public Canvas canvasHPholder;
	public Canvas canvasMustacheHolder;
	public Canvas canvasEndGradsHolder;
	public Canvas canvasDeathBlockersHolder;
	public GraphicRaycaster graphRayCastHolder_DeathBlocker;

	// Stuff to disable during victory
	public Canvas canvasAllHUD_Holder;
	public Canvas canvasAllHUDTop_Holder;

	public GameObject objMustacheParentHolder;

	public GameObject objPowRechargeParticle_1;
	public GameObject objPowRechargeParticle_2;

	public GameObject objEyesFront_Holder;
	public GameObject objPowUpsHuD_Holder;

	public GameObject objFirstTimeAllHolder;
	public GameObject objPauseMenuItselfHolder;

	public GameObject objButtonPauseToMenu;
	public GameObject objButtonPauseToMenuShadow;
	public GameObject objButtonGOverToMenu;
	public GameObject objButtonGOverToMenuShadow;

	public GameObject objHealthNonMask_LastOne;
	public GameObject[] objHealthNonMasksArr;

	public Transform transButtonRestartParentx2;

	public Button continueButtonHolder;
	public Button pauseButtonHolder;
//	public Button shopButtonHolder;

	public BoxCollider2D boxColl2dMustacheHolder;

	public Animation eyesAnimHolder;
	public Animation hudTopAllAnimHolder;
	public Animation hud_NormalAllAnimHolder;
	public Animation hud_HP_HeartBeatAnimHolder;
	public Animation hud_Mustache_AnimHolder;
	public Animation hud_DeathMenu_AnimHolder;
	public Animation hud_Bell_AnimHolder;
	public Animation hud_IngameMiniCutsenesHolder;
	public Animation hud_HF_ClingyFront_AnimHolder;
//	public Animation hud_HP_LifeAndDeathAnimHolder;

	public Animation hud_FirstTimeTutPicBackHolder;
	public Animation hud_FirstTimeTutPicBigtoHolder;
	public Animation hud_FirstTimeTutPicSmalltoHolder;
	public Animation hud_FirstTimeTutPicHandHolder;

	public Animation anim_WorldsAllHolder;

	public ParticleSystem hud_Mustache_ParticleHolder;
	public ParticleSystem hud_EyesParticle_IntroHolder;
	public ParticleSystem hud_EyesParticle_ResurrectHolder;
	public ParticleSystem hud_EyesParticle_Heal;
	public ParticleSystem hud_EyesParticle_Hearts;
	public ParticleSystem hud_HPGainParticle_HeartCircle;
	public ParticleSystem hud_EyesParticle_ExplosionWhite;
	public ParticleSystem hud_EyesParticle_ExplosionMiniWhite;
	public ParticleSystem hud_EyesParticle_LastStandShoutWave;

	public ParticleSystem hud_EndParticle_MustacheHolder;
	public ParticleSystem hud_EndParticle_CircleHolder;

	public ParticleSystem particlePowRecharge_1;
	public ParticleSystem particlePowRecharge_2;

	public ParticleSystem[] hud_EyesParticle_PipeIsActiveArr;
	public ParticleSystem[] hud_ElixirMagnetPartclesArr;

//	public ParticleSystem hud_EyesParticle_HeartSlash;

//	public Image PowUpFillerImageHolder1;
//	public Image PowUpFillerImageHolder2;

	public Text FreezeFilmTextCount;

	public RectTransform hud_ItemElixirRectTransHolder;
	public RectTransform hud_MustacheHolderRecTransHolder;
//	public RectTransform hud_HealthFill_RecTransHolder;

	public Transform hud_ItemsAllThreeParentTransHolder;

	public Transform transDeathFullscreenEffectsHolder;
	public Transform transDeathContinueHolder;
	public Transform transShopParentTransHolder;

	public GameObject objShopInGameParentHolder;

	public GameObject objFPScounterHolder;
	public GameObject gameOverClickBlockerHolder;
	public GameObject gameOverDelayedButtonHolder;
	public GameObject deathFullScreenHolder;
	public GameObject deathMenuObjHolder;
	public GameObject freezeFilmParentObjHolder;
	public GameObject freezeFilmsSampleObjHolder;
	public GameObject objMustacheInGameHolder;

	public GameObject[] HF_ClingyFront_ObjArr;
	public HF_ClingyFront_Script[] HF_ClingyFront_ScriptsArr;
	public Transform HF_ClingyFront_TransHolder;
	public Transform mustacheParentTrans_ORIG;
	public Transform mustacheParentTrans_TEMP;

	public Transform freezeFilmsGridTransHolder;
	public Transform camParentTransHolder;
	public RectTransform hudParentRectTransHolder;

//	public Transform mustacheParentTransRef;

	public Item_Pickup_Script[] itemPickUpScriptArr;

	[SerializeField]
	private Animation[] hud_ElixirMagnetAnimsArr;
	private Vector2 hud_MustachePositVect2;
	private Vector2 hud_HPfillPositVect2;
	private float hud_HPstartFull;
	private float hud_HPpercent;
	private float intro_LengthFromPlayer;
	private float additionalIntroWait;

	private int intMustacheLength;
	private int FreezeFilmTextCountInt;
//	private List<int> HF_Front_AvailablityIntList;
	private int whichLevelIntroAnim;
	private int HFcounter_MinChecker;
	[SerializeField]
	private int HFcounter_BestIndex;

	private bool isEndlessHUD;

	void Start () {
		// Check to see if fps is allowed
		if (PlayerData_Main.Instance != null) {

			if (!PlayerData_Main.Instance.isFirstTimePlaying) {
				Destroy (objFirstTimeAllHolder);
			}

			// Reset HUD Anim (Which uses pre-intro anim of HUD and moves pow ups to side);
			additionalIntroWait = PlayerData_Main.Instance.introAdditionTime;

			// Intro Anim & PowUP Position based on first level or not-first level
			if (additionalIntroWait > 1) {
				HUD_PreIntro_Reset ();
				whichLevelIntroAnim = 1;
			} else {
				whichLevelIntroAnim = 2;
			}
		}

		// HP Canvas Order In Layer Set
		HP_LayerSort_To2 ();

		intro_LengthFromPlayer = PlayerData_InGame.Instance.introAnimLength + additionalIntroWait;
		hud_ElixirMagnetAnimsArr = new Animation[4];
		for (int i = 0; i < 4; i++) {
			hud_ElixirMagnetAnimsArr [i] = hud_ElixirMagnetPartclesArr [i].GetComponent<Animation> ();
		}

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			IntroAnim_Delayer ();
		}

		// Deactivate Camera
		freezeFilmParentObjHolder.SetActive (false);

		// Hide Clingy HFs
		HF_ClingyFront_TransHolder.gameObject.SetActive (true);
		HF_ClingyFront_ScriptsArr = new HF_ClingyFront_Script[5];
		for (int i = 0; i < 5; i++) {
			HF_ClingyFront_ScriptsArr [i] = HF_ClingyFront_ObjArr [i].GetComponent<HF_ClingyFront_Script> ();
			HF_ClingyFront_ObjArr [i].SetActive (false);

//			HF_Front_AvailablityIntList.Add (i);
		}

		// Give pause the numbers for level
		if (PlayerData_Main.Instance != null) {
			// Texts!
			hud_Pause_WorldName.text = PlayerData_Main.Instance.WorldNumber_FullName;
			hud_Pause_LevelName.text = PlayerData_Main.Instance.LevelNumber_FullName;

			// Disable Menu Buttons For the first time play
			FirstTimePlay_ButtonDisabler ();
		}

		// Disable Shop At the Start
		Shop_DeActivateNOW ();

		// Check for endless
		Endless_ModeCheck ();

		// Play HUD Idle Anim
//		hud_HP_LifeAndDeathAnimHolder.Play ("Menu HP Idle Anim 1 (Legacy)");
	}

	void Awake () {
		// Disable Click Blockers
		gameOverClickBlockerHolder.SetActive (false);

		// Disable Death Blocker Canvas
		canvasDeathBlockersHolder.enabled = false;
		graphRayCastHolder_DeathBlocker.enabled = false;

//		gameOverDelayedButtonHolder.SetActive (false);

		// Make Eyes Look Closed By Default
		eyesAnimHolder.transform.GetChild (0).localPosition = Vector3.zero;
		eyesAnimHolder.transform.GetChild (1).localPosition = Vector3.zero;
	}

	void Endless_ModeCheck () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			isEndlessHUD = false;
			endlessScoreController.Destroy_EndlessScoreNOW ();
		} else {
			isEndlessHUD = true;
			endlessScoreController.Start_EndlessController ();

			// Disable pause for start of endless until pressed_play on potioncontroller activates it
			StartCoroutine (PauseButton_EnableDisable(false, 0.1F));
		}

		// Should be comment for full build
//		isEndlessHUD = false;
//		endlessScoreController.Destroy_EndlessScoreNOW ();
	}

	void FirstTimePlay_ButtonDisabler () {
		if (PlayerData_Main.Instance.isFirstTimePlaying) {
			objButtonPauseToMenu.SetActive (false);
			objButtonPauseToMenuShadow.SetActive (false);
			objButtonGOverToMenu.SetActive (false);
			objButtonGOverToMenuShadow.SetActive (false);
			transButtonRestartParentx2.localPosition = new Vector2 (0, transButtonRestartParentx2.localPosition.y);
		}
	}

	public IEnumerator PauseButton_EnableDisable (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		pauseButtonHolder.enabled = isActive;
	}

	public void IntroAnim_Delayer () {
		camScriptHolder.Cam_IdleAnim_Start ();
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("OpenEyes_Now", (intro_LengthFromPlayer - PlayerData_Main.Instance.introAdditionTime - 1.5F));
				if (!PlayerData_Main.Instance.isFirstTimePlaying) {
					Invoke ("HUD_Intro_Now", (intro_LengthFromPlayer - 1.1F));

					if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
						// Earilier than without menu
						Invoke ("HUD_PotionsEnter_Now", (intro_LengthFromPlayer + 1.3F));

						// Disable pause for start of endless before AND after potions (MOVED endlessmodecheck above)
//						StartCoroutine (PauseButton_EnableDisable(false, 0.1F));
					}
				} 

//				else {
					// Do nothing, which means this is FIRST time playing
//				}
			} else {
				Invoke ("OpenEyes_Now", (intro_LengthFromPlayer - 1.5F));
				Invoke ("HUD_Intro_Now", (intro_LengthFromPlayer - 1.1F));

				if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
					Invoke ("HUD_PotionsEnter_Now", (intro_LengthFromPlayer + 2.5F));
				}
			}

			// Old place of hud intro anims
//			Invoke ("HUD_Intro_Now", (intro_LengthFromPlayer - 1.1F));

//			Invoke ("LevelMusic_Intro_PlayNow", (intro_LengthFromPlayer));
		} else {
			Invoke ("OpenEyes_Now", 0.1F);
			Invoke ("HUD_Intro_Now", 0.8F);
//			Invoke ("LevelMusic_Intro_PlayNow", 0);
		}
	}

	public IEnumerator HUD_FirstTimePlay_TutPartsAnimate_1 () {
		// Show mini-cutscenes
		HUD_Minicutscene_EnterNow ();

		// Show first time tutorial
		HUD_FirstTimeTutArrow_Play (1);

		// Hand part of HUDs only (1)
		HUD_FirstTimeTut_HandPart (1);

		// Eneter delayed HUD (OLD) (Now after last tut)
//		HUD_Intro_Now ();

		yield return null;
	}

	public IEnumerator HUD_FirstTimePlay_TutPartsAnimate_2and3 (int whichAnim) {
		// Show mini-cutscenes (No mini-cutscene for second tut)
//		HUD_Minicutscene_EnterNow ();

		// Show first time tutorial
		HUD_FirstTimeTutArrow_Play (whichAnim);

		// Hand part of HUDs only (2)
		HUD_FirstTimeTut_HandPart (whichAnim);

		// Eneter delayed HUD (OLD) (Now after last tut)
		//		HUD_Intro_Now ();

		yield return null;
	}

	// Work on removing first time tut parts
	public IEnumerator HUD_FirstTimePlay_StopTutPart_NoLeave () {
		// Stop first time tutorial
		HUD_FirstTimeTutArrow_Stop (false);

		yield return null;
	}

	// Work on removing first time tut parts
	public IEnumerator HUD_FirstTimePlay_StopTutPart_YesLeave () {
		// Stop first time tutorial
		HUD_FirstTimeTutArrow_Stop (true);

		yield return null;
	}

	public IEnumerator HUD_FirstTimePlay_EndAll () {
		// Leave mini-cutscenes
		HUD_Minicutscene_LeaveNow ();

		// Enter delayed HUD 
		HUD_Intro_Now ();

		yield return null;
	}

	void OpenEyes_Now () {
		// Play Intro Eyes Anim
		OpenEyes_AnimsParticles ();

		// First Levels And other level intros
		GoWorldsLevelIntro_Animate ();
	}

	void OpenEyes_AnimsParticles () {
		eyesAnimHolder.Play ("HUD (Top) Eyes Intro Anim 1 (Legacy)");
		hud_EyesParticle_IntroHolder.Emit (1);
	}

	void LevelMusic_Intro_PlayNow () {
		//PlayerData_InGame.Instance.PlayMusic_World ();
	}

	void OpenEyes_Resurrect () {
		// Play Intro Eyes Anim
		eyesAnimHolder.Play ("HUD (Top) Eyes Intro Anim 1 (Legacy)");
		hud_EyesParticle_ResurrectHolder.Emit (1);
	}

	void HUD_FirstTimeTutArrow_Play (int whichAnim) {
		if (whichAnim != 4) {
			hud_FirstTimeTutPicBackHolder.Play ("Tutorial 1-0 Interactive Anim 3 Enter (Back) (Legacy)");
		}

		hud_FirstTimeTutPicSmalltoHolder.Play ();
		hud_FirstTimeTutPicSmalltoHolder.gameObject.SetActive (true);
		hud_FirstTimeTutPicBigtoHolder.Play ();
		hud_FirstTimeTutPicBigtoHolder.gameObject.SetActive (true);

		// Now need separate animator
//		hud_FirstTimeTutPicHandHolder.Play ();
//		hud_FirstTimeTutPicHandHolder.gameObject.SetActive (true);
	}

	void HUD_FirstTimeTut_HandPart (int whichAnim) {
		switch (whichAnim) {
		case 1:
			hud_FirstTimeTutPicHandHolder.Play ("Tutorial 1-0 Interactive Anim 4-1 HandPicto (Legacy)");
			break;
		case 2:
			hud_FirstTimeTutPicHandHolder.Play ("Tutorial 1-0 Interactive Anim 4-2 HandPicto (Legacy)");
			break;
		case 3:
			Debug.LogError ("3!!!!!!!!");
			hud_FirstTimeTutPicHandHolder.Play ("Tutorial 1-0 Interactive Anim 4-3 HandPicto (Legacy)");
			break;
		case 4:
			Debug.LogError ("4!!!!!!!!!!!!!!!!!");
			hud_FirstTimeTutPicHandHolder.Play ("Tutorial 1-0 Interactive Anim 4-4 HandPicto (Legacy)");
			break;
		default:
			hud_FirstTimeTutPicHandHolder.Play ("Tutorial 1-0 Interactive Anim 4-1 HandPicto (Legacy)");
			break;
		}

		hud_FirstTimeTutPicHandHolder.gameObject.SetActive (true);
	}

	void HUD_FirstTimeTutArrow_Stop (bool canBigBackLeave) {
		if (canBigBackLeave) {
			hud_FirstTimeTutPicBackHolder.Play ("Tutorial 1-0 Interactive Anim 3 Leave (Back) (Legacy)");
		}

		hud_FirstTimeTutPicSmalltoHolder.Stop ();
		hud_FirstTimeTutPicSmalltoHolder.gameObject.SetActive (false);
		hud_FirstTimeTutPicBigtoHolder.Stop ();
		hud_FirstTimeTutPicBigtoHolder.gameObject.SetActive (false);
		hud_FirstTimeTutPicHandHolder.Stop ();
		hud_FirstTimeTutPicHandHolder.gameObject.SetActive (false);
	}

	void HUD_Minicutscene_EnterNow () {
		hud_IngameMiniCutsenesHolder.Play ("HUD FirstTime MiniCut Enter Anim 1 (Legacy)");
	}

	void HUD_Minicutscene_LeaveNow () {
		hud_IngameMiniCutsenesHolder.Play ("HUD FirstTime MiniCut Leave Anim 1 (Legacy)");
	}

	void HUD_Intro_Now () {
		hud_NormalAllAnimHolder.Play ("HUD All Intro Anim 1 (Legacy)");
		// Invoke ("Bell_DingNow", 3.7F);
	}

	public void HUD_ScoreDisplay_Now () {
		StartCoroutine (HUD_ScoreDisplay_Routine ());
	}

	public IEnumerator HUD_ScoreDisplay_Routine () {
		yield return new WaitForSeconds (0.5F);

		endlessScoreController.HUD_EndlessScore_Enter ();
	}

	void HUD_PotionsEnter_Now () {
		StartCoroutine (endlessPotionsController.Start_EndlessPotionsEnter ());
	}

	void HUD_PreIntro_Reset() {
		hud_NormalAllAnimHolder.Play ("HUD All Pre-Intro Reset Anim 1 (Legacy)");
	}

	public void HUD_Healed_Normal () {
		camScriptHolder.PlayAnim_PlayerHeal ();
		hud_EyesParticle_Heal.Emit (1);
		hud_EyesParticle_Hearts.Play ();
		hud_HPGainParticle_HeartCircle.Emit (1);
	}

	public void HUD_ExplosionWhite () {
		hud_EyesParticle_ExplosionWhite.Play ();
		camScriptHolder.PlayAnim_PlayerExplosionShake ();
	}

	public void HUD_ExplosionMiniWhite () {
		hud_EyesParticle_ExplosionMiniWhite.Play ();
		camScriptHolder.PlayAnim_PlayerExplosionShake ();
	}

	public void HUD_LastStandShout () {
		hud_EyesParticle_LastStandShoutWave.Play ();
		camScriptHolder.PlayAnim_PlayerLastStandShoutShake ();

		// Actual damage / effect of Last Stand Shout
		powUpsScriptHolder.PowerUpSHOUT_KillAll ();
	}

	public void HUD_PowUpSlotsUpdate () {
		powUpsScriptHolder.PowerUpBoth_Update ();
	}

	public IEnumerator HUD_PowUpRecharge_1 () {
		objPowRechargeParticle_1.SetActive (true);
		particlePowRecharge_1.Play ();

		// HUD Update Visuals
		powUpsScriptHolder.PowerUpBoth_Update ();

		// Animate HUD
		StartCoroutine (powUpsScriptHolder.PowerUp_HudAnimate_1 ());

		yield return new WaitForSeconds (1.5F);
		objPowRechargeParticle_1.SetActive (false);
	}

	public IEnumerator HUD_PowUpRecharge_2 () {
		objPowRechargeParticle_2.SetActive (true);
		particlePowRecharge_2.Play ();

		// HUD Update Visuals
		powUpsScriptHolder.PowerUpBoth_Update ();

		// Animate HUD
		StartCoroutine (powUpsScriptHolder.PowerUp_HudAnimate_2 ());

		yield return new WaitForSeconds (1.5F);
		objPowRechargeParticle_2.SetActive (false);
	}

	public void HUD_PipeParticles_Activate () {
		hud_EyesParticle_PipeIsActiveArr[0].Play ();
		hud_EyesParticle_PipeIsActiveArr[1].Play ();
	}

	public void HUD_PipeParticles_Deactivate () {
		hud_EyesParticle_PipeIsActiveArr[0].Stop ();
		hud_EyesParticle_PipeIsActiveArr[1].Stop ();
	}

	public void PauseFromCam () {
		camScriptHolder.PauseTheGame ();
		HP_LayerSort_To2 ();
	}

	public void Pressed_Resume () {
		camScriptHolder.ResumeTheGame ();
		Invoke ("HP_LayerSort_To5", 0.5F);
	}

	public void HP_LayerSort_To2 () {
		canvasHPholder.sortingOrder = 2;
	}

	public void HP_LayerSort_To5 () {
		canvasHPholder.sortingOrder = 5;
	}

	public void Mustache_LayerSort_To (int whichLayer) {
		canvasMustacheHolder.sortingOrder = whichLayer;
	}

	public void FreezeFromGame_Start () {
		camScriptHolder.CamFrontElements_FullFreeze_Start ();
	}

	public void FreezeFromGame_End () {
		camScriptHolder.CamFrontElements_FullFreeze_End ();
	}

	public void Pressed_Reload () {
		// Enable End Grads Canvas

		// TODO: Fix and test
		// TODO: Also in-game
//		canvasEndGradsHolder.enabled = true;

		// Calling Loading circle & mustache
//		Loading_Start ();
		Invoke ("Loading_Start", 0.4F);
			
		// Camera circle OR border closer
		CamAnim_CircleOrBorder_Close ();
//		Invoke ("CamAnim_CircleOrBorder_Close", 0.5F);

		// Actual Reload
		Invoke ("ReloadLevel_Now", 0.8F);
	}
		
	public void Pressed_QuitToMenu () {
		camScriptHolder.PlayAnim_EndGradBorders ();
//		Loading_Start ();
		Invoke ("Loading_Start", 0.4F);
		Invoke ("LoadMenu_Now", 1.2F);
	}

	public void PressedFOR_ContinueButton () {
		continueButtonHolder.onClick.Invoke ();
	}

	public void Continue_Resume () {
		camScriptHolder.ContinueTheGame ();
	}

	public void CamAnim_CircleOrBorder_Close () {
		camScriptHolder.PlayAnim_EndGradBorders ();
	}

	public IEnumerator HUD_Update_Combo_AnyGain () {
		yield return null;
		endlessScoreController.HUD_ComboUpdate_AnyGain ();
	}

	public IEnumerator HUD_Update_Combo_KillReward () {
		yield return null;
		endlessScoreController.HUD_ComboUpdate_KillReward ();
	}

	public IEnumerator HUD_Update_Combo_MiniLoss () {
		yield return null;
		endlessScoreController.HUD_ComboUpdate_MiniLoss ();
	}

	public IEnumerator HUD_Update_Combo_MainLoss (bool isDeath) {
		yield return null;
		endlessScoreController.HUD_ComboUpdate_MainLoss (isDeath);
	}

	public IEnumerator HUD_Update_Score () {
		yield return null;
		endlessScoreController.HUD_ScoreAmountUpdate ();
	}

	public IEnumerator HUD_Update_Mustache () {
		// Log method (This is to get the number of digits Mustache)
		intMustacheLength = Mathf.FloorToInt (Mathf.Log10(PlayerData_InGame.Instance.p1_Moustache) + 1);

		// String method
//		intMustacheLength = PlayerData_InGame.Instance.p1_Moustache.ToString ().Length;

		intMustacheLength *= 13;
		hud_MustachePositVect2.x = 50 + intMustacheLength;
		hud_MustachePositVect2.y = 41;
		hud_MustacheHolderRecTransHolder.sizeDelta = hud_MustachePositVect2;
		hud_Moustache_Show.text = PlayerData_InGame.Instance.p1_Moustache.ToString();
		yield return null;

//		 Used for when score is in front of mustache
		if (isEndlessHUD) {
			endlessScoreController.HUD_ScorePosition (intMustacheLength);
		}
	}

	public IEnumerator HUD_Update_Health (int newHealth) {
		// Avoid divide by zero
		if (hud_HPstartFull == 0) {
			hud_HPstartFull = newHealth;
		}

		// Compute HP Position
		hud_HPpercent = (float)newHealth / hud_HPstartFull;

		if (hud_HPpercent > 1) {
			hud_HPpercent = 1;
		}

		// For Masked version of Health Display
//		HUD_HealthUpdate_Mask ();

		// For NON-Masked version of Health Display
		HUD_HealthUpdate_NonMask ();

		// Heartbeat Anim Speed
		hud_HP_HeartBeatAnimHolder[hud_HP_HeartBeatAnimHolder.clip.name].normalizedSpeed = 5.5F - hud_HPpercent * 5.2F;

		// Update amount
		hud_Health_Show.text = newHealth.ToString();

		// For achievement
		if (newHealth == 1) {
//			Debug.LogWarning ("HP = 1");
			PlayerData_InGame.Instance.p1_1Health4Times_Counter++;
		}
		yield return null;
	}

	public void HUD_HealthUpdate_Mask () {
		hud_HPfillPositVect2.y = -80 + hud_HPpercent * 70;

//		hud_HealthFill_RecTransHolder.anchoredPosition = hud_HPfillPositVect2;
	}

	public void HUD_HealthUpdate_NonMask () {
		objHealthNonMask_LastOne.SetActive (false);

		if (hud_HPpercent > 0.8F) {
			objHealthNonMasksArr [0].SetActive (true);
			objHealthNonMask_LastOne = objHealthNonMasksArr [0];
		} else {
			if (hud_HPpercent > 0.6F) {
				objHealthNonMasksArr [1].SetActive (true);
				objHealthNonMask_LastOne = objHealthNonMasksArr [1];
			} else {
				if (hud_HPpercent > 0.4F) {
					objHealthNonMasksArr [2].SetActive (true);
					objHealthNonMask_LastOne = objHealthNonMasksArr [2];
				} else {
					if (hud_HPpercent > 0.2F) {
						objHealthNonMasksArr [3].SetActive (true);
						objHealthNonMask_LastOne = objHealthNonMasksArr [3];
					} else {
						if (hud_HPpercent > 0) {
							objHealthNonMasksArr [4].SetActive (true);
							objHealthNonMask_LastOne = objHealthNonMasksArr [4];
						} else {
							objHealthNonMasksArr [5].SetActive (true);
							objHealthNonMask_LastOne = objHealthNonMasksArr [5];
						}
					}
				}
			}
		} 
	}

	public void HUD_AnimateMustache_Gain () {
		hud_Mustache_AnimHolder.Stop ();
		hud_Mustache_AnimHolder.Play ("HUD Mustache Gain Anim 1 (Legacy)");
		hud_Mustache_ParticleHolder.Emit (1);
	}

	public void HUD_EndlessChangeBackground () {
		endlessScoreController.endlessBackgroundChanger.BackgroundChanger_Setup ();
	}

	#region Background Change Old / Eyes Closed
	public IEnumerator HUD_EndlessChangeBackground_RoutineOLD () {
		// Copied from PlayerDied

		// Reset camera parent anims & effects
//		camScriptHolder.CameraEffects_Reset();

		// Clear the pickup Durability HUD elements
//		for (int i = 0; i < 3; i++) {
//			if (itemPickUpScriptArr[i].thisItemIsActive) {
//				itemPickUpScriptArr[i].ItemEndItNow ();
//			}
//		}

		// Close Eyes
		eyesAnimHolder.Play("HUD (Top) Eyes Die Close Anim 1 (Legacy)");

		yield return new WaitForSeconds (5);

		// Open Eyes
		OpenEyes_AnimsParticles ();
	}
	#endregion

	public IEnumerator DeathFullscreen_DisableRoutine () {
		yield return null;
		deathFullScreenHolder.SetActive (false);
	}

	public void PlayerDied () {
		// Reset camera parent anims & effects
		camScriptHolder.CameraEffects_Reset();

		// Clear the pickup Durability HUD elements
		for (int i = 0; i < 3; i++) {
			if (itemPickUpScriptArr[i].thisItemIsActive) {
				itemPickUpScriptArr[i].ItemEndItNow ();
			}
		}

		// Play HP Death Anim
		if (PlayerData_InGame.Instance.p1_PowUp2_Count_Own > 0) {
			hudTopAllAnimHolder.Play ("HUD All DEATH Start Anim 1 (Legacy)");
		} else {
			hudTopAllAnimHolder.Play ("HUD All DEATH Start Anim 2 (Before Pow Up 2) (Legacy)");
		}

		// Animated mustache separately for death
		hud_Mustache_AnimHolder.Play ("HUD Mustache DEATH Start Anim 1 (Legacy)");

		// Remove score for death-continue in endless
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			StartCoroutine (HUD_Score_EnableDisable (false, 1.3F));
		}

//		hud_HP_LifeAndDeathAnimHolder.Play ("Menu HP Death Anim 1 (Legacy)");

		// Add Cam Noise
//		camScriptHolder.Camera_Noise_Toggle ();

		// Change Mustache's parent (TEMP)
//		Debug.LogWarning ("Mustache Trans Parent To Temp was turned into comment");
//		mustacheParentTransRef.SetParent(mustacheParentTrans_TEMP);

		deathFullScreenHolder.SetActive (true);
		gameOverClickBlockerHolder.SetActive (true);

		// Enable Death Blocker Canvas
		canvasDeathBlockersHolder.enabled = true;
		graphRayCastHolder_DeathBlocker.enabled = true;

		if (PlayerData_InGame.Instance.GameOverRequiresTouch) {
			Invoke ("GameOverButtonStart_Delayed", 0.75F);
		} else {
			Invoke ("GameOverButtonStart_Automatic", 1.5F);
		}
	}

	public void PlayerResurrected () {
		// Invinciblity is from playerdata at the momenty of PLAYER DIED!, before last stand check and all other things.

		// Remove Cam Noise & Reset HP Anim
		Invoke ("HUD_HP_Play_Reset", 0.2F);

		// Eyes & Other Stuff
		Invoke ("OpenEyes_Resurrect", 0.8F);
//		OpenEyes_Resurrect ();

		deathFullScreenHolder.SetActive (false);
		gameOverClickBlockerHolder.SetActive (false);

		// Disable Death Blocker Canvas
		canvasDeathBlockersHolder.enabled = false;
		graphRayCastHolder_DeathBlocker.enabled = false;

		Invoke ("Continue_Resume", 2.2F);

		// Death Menu Anim & Disable
//		hud_DeathMenu_AnimHolder.Play ("Menu Death Close Anim 1 (Legacy)");
		Invoke ("HUD_DeathMenu_DeActivate", 1.5F);

		// Remove score for death-continue in endless
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Endless) {
			StartCoroutine (HUD_Score_EnableDisable (true, 2.7F));
		}
	}

	public IEnumerator HUD_Score_EnableDisable (bool isActive,float delay) {
		yield return new WaitForSeconds (delay);
		if (isActive) {
			endlessScoreController.HUD_EndlessScore_Enter ();
		} else {
			endlessScoreController.HUD_EndlessScore_Leave ();
		}
	}

	public void HUD_DeathMenu_DeActivate () {
		deathMenuObjHolder.SetActive (false);
	}

	public void HUD_HP_Play_Reset () {
		hudTopAllAnimHolder.Play ("HUD All DEATH Reset Anim 1 (Legacy)");
//		hud_HP_LifeAndDeathAnimHolder.Play ("Menu HP Reset Anim 1 (Legacy)");
		Invoke ("HUD_HP_Play_Refill", 1);
	}

	public void HUD_HP_Play_Refill () {
		hudTopAllAnimHolder.Stop ();
		hudTopAllAnimHolder.Play ("HUD All DEATH Refill Anim 1 (Legacy)");
//		hud_HP_LifeAndDeathAnimHolder.Play ("Menu HP Refill Anim 1 (Legacy)");
//		Invoke ("HUD_HP_Play_Idle", 1.5F);

		// Change Mustache's parent (ORIG) (OLD)
//		mustacheParentTransRef.SetParent(mustacheParentTrans_ORIG);
	}

//	public void HUD_HP_Play_Idle () {
//		hud_HP_LifeAndDeathAnimHolder.Play ("Menu HP Idle Anim 1 (Legacy)");
//	}

	public void GameOverButtonStart_Delayed () {
		// If press only
		gameOverDelayedButtonHolder.SetActive (true);
	}

	public void GameOverButtonStart_Automatic () {
		// If no press
		Pressed_GameOverButtonStart ();
	}

	public void FreezeShutter_EyesAnimate_1 () {
		eyesAnimHolder.Play ("HUD (Top) Eyes Freeze Shutter Anim 1 (Legacy)");
	}

	public void FreezeShutter_EyesAnimate_2 () {
		eyesAnimHolder.Play ("HUD (Top) Eyes Freeze Shutter Anim 2 (Legacy)");
	}

	public void FreezeShutter_StartHUD () {
//		camParentTransHolder.eulerAngles = new Vector3 (0, 0, -5);
//		hudParentRectTransHolder.localEulerAngles = new Vector3 (0, 0, -5);

		PlayerData_InGame.Instance.HP_SortingOrderTo2 ();
//		PlayerData_InGame.Instance.Mustache_SortingOrderTo (2);


		hudTopAllAnimHolder.Play ("HUD All Freeze Shutter START Anim 2 (Legacy)");
		hudTopAllAnimHolder.Play ("HUD All Freeze Shutter IDLE Anim 1 (Legacy)");
	}

	public void FreezeShutter_EndHUD_Delayer () {
//		camParentTransHolder.eulerAngles = Vector3.zero;
//		hudParentRectTransHolder.localEulerAngles = Vector3.zero;

		Invoke ("FreezeShutter_EndHUD_DelayedHPLayer", 0.4F);
//		PlayerData_InGame.Instance.Mustache_SortingOrderTo (5);

//		Invoke ("FreezeShutter_EndHUD_PlayNow", 0.1F);
		hudTopAllAnimHolder.Play ("HUD All Freeze Shutter END Anim 2 (Legacy)");
	}

	public void FreezeShutter_EndHUD_DelayedHPLayer () {
		PlayerData_InGame.Instance.HP_SortingOrderTo5 ();
//		hudTopAllAnimHolder.Play ("HUD All Freeze Shutter END Anim 2 (Legacy)");
	}


	public void FreezeFilm_Start (int howManyFilms) {
//		Debug.LogError ("YAY!?");
		FreezeFilmTextCountInt = howManyFilms;
		FreezeFilmTextCount.text = FreezeFilmTextCountInt.ToString();
		freezeFilmParentObjHolder.SetActive (true);

//		freezeFilmParentObjHolder.transform.localPosition = new Vector3 (422, -214, 0);
//		freezeFilmParentObjHolder.transform.localScale = new Vector3 (1.3F, 1.3F, 1.3F);

		for (int i = 0; i < howManyFilms; i++) {
			GameObject newFilmExtra = Instantiate (freezeFilmsSampleObjHolder);
			newFilmExtra.transform.SetParent (freezeFilmsGridTransHolder);
			newFilmExtra.transform.localPosition= Vector3.one;
			newFilmExtra.transform.localScale = Vector3.one;
			if (i == (howManyFilms - 1)) {
				newFilmExtra.transform.GetChild (1).gameObject.SetActive (true);
				newFilmExtra.transform.GetChild (0).gameObject.SetActive (false);
			}
		}
	}

	public void FreezeFilm_Decrease () {
		FreezeFilmTextCountInt--;
		FreezeFilmTextCount.text = FreezeFilmTextCountInt.ToString();
		if (freezeFilmsGridTransHolder.GetChild (0) != null) {
			Destroy (freezeFilmsGridTransHolder.GetChild (0).gameObject);
		}
	}

	public void FreezeFilm_End () {
		freezeFilmParentObjHolder.SetActive (false);
	}

	public void ElixirParticle_Start (int whichParticle) {
		for (int i = 0; i < 4; i++) {
			hud_ElixirMagnetAnimsArr [i].Stop ();
			hud_ElixirMagnetPartclesArr [i].Stop ();
		}

		// 
		hud_ElixirMagnetPartclesArr [whichParticle].Play ();
		ElixirArtRotator (whichParticle);

		// Side Magnets Animation
		hud_ElixirMagnetAnimsArr [whichParticle].Play ("HUD ItemPickup Magnets START Anim 1 (Legacy)");
	}

	public void ElixirParticle_End (int whichParticle) {
		hud_ElixirMagnetPartclesArr [whichParticle].Stop ();

		// Side Magnets Animation
		hud_ElixirMagnetAnimsArr [whichParticle].Play ("HUD ItemPickup Magnets END Anim 1 (Legacy)");
	}

	public void ElixirArtRotator (int whatNumber) {
		switch (whatNumber) {
		case 0:
			hud_ItemElixirRectTransHolder.localEulerAngles = new Vector3 (0, 0, 0);
			break;
		case 1:
			hud_ItemElixirRectTransHolder.localEulerAngles = new Vector3 (0, 0, 180);
			break;
		case 2:
			hud_ItemElixirRectTransHolder.localEulerAngles = new Vector3 (0, 0, 270);
			break;
		case 3:
			hud_ItemElixirRectTransHolder.localEulerAngles = new Vector3 (0, 0, 90);
			break;
		default:
			break;
		}
	}

	public void Pressed_GameOverButtonStart () {
		eyesAnimHolder.Play("HUD (Top) Eyes Die Close Anim 1 (Legacy)");
		hud_Mustache_AnimHolder.Play ("HUD Mustache DEATH Return Anim 1 (Legacy)");
//		camScriptHolder.Camera_Noise_Toggle ();

		Invoke ("DeathMenu_Activate", 0.75F);
	}

	public void Pressed_DeathMustache_Return () {
		hud_Mustache_AnimHolder.Play ("HUD Mustache DEATH Return Anim 1 (Legacy)");

		// Send shop out
		Pressed_CallInGameShop_Out ();

		// Update mustache after closing shop in game over
		StartCoroutine (HUD_Update_Mustache ());

		// Check to see if the return in shop was pressed during Endless Potions or during continue screen
		if (PlayerData_InGame.Instance.isPotionsMenuOpen) {
			StartCoroutine (endlessPotionsController.PotionsButton_Enable ());
		}
	}

	public void HUD_DeathMustache_Button (bool whichBool) {
		StartCoroutine (mustacheDisplayScriptHolder.Mustache_ButtonAndShadow_Activator (whichBool));
	}

	public void PlayerCantResurrected () {
		StartCoroutine (Player_TellGoShopRoutine ());
	}

	public IEnumerator Player_TellGoShopRoutine () {
		yield return null;
		mustacheDisplayScriptHolder.Mustache_TellGoShop ();
		Debug.LogError ("Show Shop");
	}

	public void PlayerCam_HurtDiedShield (int camNumber) {
		switch (camNumber) {
		case 1:
			camScriptHolder.PlayAnim_PlayerHurt ();
			break;
		case 2:
			camScriptHolder.PlayAnim_PlayerDiedHurt ();
			break;
		case 3:
			camScriptHolder.PlayAnim_PlayerShieldedHurt ();
			break;
		case 4:
			camScriptHolder.PlayAnim_PlayerHurtExplode ();
			break;
		}
	}

	public void HUD_Victory_PowUp2EndDur () {
		powUpsScriptHolder.PowerUp2_ShieldDurationEnd ();
	}

	public void HUD_Victory_Play (int whichVictoryHUD) {
		if (whichVictoryHUD == 1) {
			hudTopAllAnimHolder.Play ("HUD All VICTORY Anim 1 (Legacy)");
		} else {
			hudTopAllAnimHolder.Play ("HUD All VICTORY Anim 2 (Legacy)");
		}
	}

	public void HUD_Victory_MustacheLateRemove () {
		hud_Mustache_AnimHolder.Play ("HUD Mustache VicSolo Leave Anim 1 (Legacy)");
		boxColl2dMustacheHolder.enabled = false;

		Invoke ("HUD_Victory_MustacheLateDisable", 1);

		Invoke ("HUD_Victory_CanvasDisable", 1);
	}

	public void HUD_EndlessEnd_MustacheLateRemove () {
		hud_Mustache_AnimHolder.Play ("HUD Mustache VicSolo Leave Anim 1 (Legacy)");
		boxColl2dMustacheHolder.enabled = false;

		// Added this because of endless rewards / mustache
		StartCoroutine (HUD_EndlessEnd_MustacheDisableRoutine());
	}

	public IEnumerator HUD_EndlessEnd_MustacheDisableRoutine () {
		yield return new WaitForSeconds (1);
		objMustacheParentHolder.SetActive (false);
	}

	public void HUD_Victory_CanvasDisable () {
		canvasAllHUD_Holder.enabled = false;
//		canvasAllHUD_Holder.transform.GetChild (0).gameObject.SetActive (false);

		canvasAllHUDTop_Holder.enabled = false;
		canvasHPholder.enabled = false;
		canvasMustacheHolder.enabled = false;

		canvasHPholder.gameObject.SetActive (false);
		canvasMustacheHolder.gameObject.SetActive (false);

		objPowUpsHuD_Holder.SetActive (false);
		objEyesFront_Holder.SetActive (false);
	}

	public void HUD_Victory_MustacheLateDisable () {
		objMustacheInGameHolder.SetActive (false);
	}

	public void HUD_CleanScreen_Start () {
		hudTopAllAnimHolder.Play ("HUD All CleanScreen START Anim 1 (Legacy)");
	}

	public void HUD_CleanScreen_End (int whichEnd) {
		if (whichEnd == 1) {
			hudTopAllAnimHolder.Play ("HUD All CleanScreen END Anim 1 (Mustache First) (Legacy)");
		} else {
			hudTopAllAnimHolder.Play ("HUD All CleanScreen END Anim 2 (Mustache Later) (Legacy)");
		}
	}
		
	public void Cam_SepiaTone_Do () {
		camScriptHolder.particleFullScreenFreezeStartHolder.Emit (1);
		camScriptHolder.sepiaToneRef.enabled = true;
	}

	public void Cam_SepiaTone_UnDo () {
		camScriptHolder.particleFullScreenFreezeEndHolder.Emit (1);
		camScriptHolder.sepiaToneRef.enabled = false;
	}

	public void Bell_DingNow () {
		hud_Bell_AnimHolder.Play ();
	}

	public void HF_Front_CallRandom (int randomHF) {
//		Debug.LogError ("Yay!?  " + randomHF + "  === " + HF_ClingyFront_ScriptsArr [randomHF].amAvailable);
		if (HF_ClingyFront_ScriptsArr [randomHF].amAvailable) {
			HF_ClingyFront_ScriptsArr [randomHF].HF_ClingyFront_Add ();
		} else {
			HF_Front_CallRandom (Random.Range (1, 5));
		}
	}

	public void HF_Front_Hit (int hitAmount) {
		HFcounter_MinChecker = 600;
		for (int i = 0; i < 5; i++) {
			if (HF_ClingyFront_ScriptsArr [i].hf_ClingerNumber < HFcounter_MinChecker) {
				HFcounter_BestIndex = i;
				HFcounter_MinChecker = HF_ClingyFront_ScriptsArr [i].hf_ClingerNumber;
			}
		}
		HF_ClingyFront_ScriptsArr [HFcounter_BestIndex].HF_ClingyFront_Hurt (hitAmount);
	}

	public void HF_Front_KillAll () {
		camScriptHolder.Cam_IdleAnim_Start ();
		for (int i = 0; i < 5; i++) {
			if (!HF_ClingyFront_ScriptsArr [i].amAvailable) {
				HF_ClingyFront_ScriptsArr [i].HF_ClingyFront_Hurt (200);
			}
		}
	}

	public void HF_Front_PlayThis (string playWhatAnim, bool camClingyAllow) {
		hud_HF_ClingyFront_AnimHolder.Play (playWhatAnim);

		// HF Clingy Camera Shake Idle
		if (camClingyAllow) {
			camScriptHolder.Cam_HF_ClingyIdleAnim_Start ();
		} else {
			camScriptHolder.Cam_IdleAnim_Start ();
		}
	}

	public void Cam_PlayIdleAnim () {
		camScriptHolder.Cam_IdleAnim_Start ();
	}

//	public void HF_Front_Hit () {
//		hud_HF_ClingyFront_AnimHolder.Play ();
//	}
//
//	public void HF_Front_End () {
//		hud_HF_ClingyFront_AnimHolder.Play ();
//	}
		
	public void DeathMenu_Activate () {
        // SOUNDD EFFECTS - Death Continue Pop-up (done)
        if (MusicManager.Instance!=null)
        {
            MusicManager.Instance.PlayMusic_GameOver();
        }

		deathMenuObjHolder.SetActive (true);
		deathMenuObjHolder.GetComponent<Death_Continue_Script> ().SetupDeathContinue_Start ();
//		hud_DeathMenu_AnimHolder.Play ("Menu Death Enter Anim 1 (Legacy)");
	}

	public void ReloadLevel_Now () {
		StartCoroutine (PlayerData_Main.Instance.LoadScene_Async_Slow (SceneManager.GetActiveScene ().name));
//		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void LoadMenu_Now () {
		StartCoroutine ( PlayerData_Main.Instance.LoadScene_Async_Slow ("Menu_Scene"));

//		SceneManager.LoadScene ("Menu_Scene");
	}

	public void GoWorldsZoomBack_Animate () {
		anim_WorldsAllHolder.Play ("World All - End ZoomIn Anim 1 (Legacy)");
	}

	public void GoWorldsLevelIntro_Animate () {
		if (whichLevelIntroAnim == 1) {
			anim_WorldsAllHolder.Play ("World All - Levels Intro LONG Anim 1 (Legacy)");
		} else {
			anim_WorldsAllHolder.Play ("World All - Levels Intro SHORT Anim 1 (Legacy)");
		}
	}

	public void GoSloMo_Start () {
        // SOUNDD EFFECTS: Last Hit Slo-mo (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.MoshtBeAkharinEnemy.Play();
        }

		GoWorldsZoomBack_Animate ();
		GoEndParticle_Circle ();
		Time.timeScale = 0.09F;
		camScriptHolder.Camera_Sepia_LowSat (true, 0F);
		Invoke ("GoSloMo_Mid1", 0.08F);
	}

	public void GoSloMo_Mid1 () {
		camScriptHolder.Camera_Sepia_LowSat (true, 0.32F);
		Time.timeScale = 0.36F;
		Invoke ("GoSloMo_Mid2", 0.12F);
	}

	public void GoSloMo_Mid2 () {
		GoEndParticle_Mustache ();
		camScriptHolder.Camera_Sepia_LowSat (true, 0.72F);
		Time.timeScale = 0.6F;
		Invoke ("GoSloMo_End", 0.12F);
	}

	public void GoSloMo_End () {
//		Invoke ("GoEndParticle_Mustache", 0.2F);
		camScriptHolder.Camera_Sepia_LowSat (false, 1);
		Time.timeScale = 1;
	}

	public void GoEndParticle_Circle () {
		hud_EndParticle_CircleHolder.Play ();
	}

	public void GoEndParticle_Mustache () {
		hud_EndParticle_MustacheHolder.Play ();
	}

	public void Pressed_SloMo_Test_Start () {
		Time.timeScale = 0.3F;
		camScriptHolder.Camera_Sepia_LowSat (true, 0.2F);
		Invoke ("Pressed_SloMo_Test_End", 1F);
	}

	public void Pressed_SloMo_Test_End () {
		camScriptHolder.Camera_Sepia_LowSat (false, 1);
		Time.timeScale = 1;
	}

	void Loading_Start () {
		if (Loading_Anim_Script.Instance != null) {
//			Debug.LogError ("LOADING!");
			StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_Start (0.4F));
			StartCoroutine (Loading_Anim_Script.Instance.Loading_TextsUpdate ());
		}
	}

	public IEnumerator Shop_MakeOrDestroyer (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		if (isActive) {
			GameObject NewShop = Instantiate (Resources.Load("Prefab Resources/Shop In-Game Parent")) as GameObject;
			NewShop.transform.SetParent (transShopParentTransHolder);
			NewShop.transform.localScale = Vector3.one;
			NewShop.transform.localPosition = Vector3.zero;

			objShopInGameParentHolder = NewShop;

		} else {
			Destroy (objShopInGameParentHolder);
		}
	}

	public void Shop_DeActivateNOW () {
		shopPopScriptHolder.MoveOut_ShopAll ();
	}

	public IEnumerator Shop_Activator (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		Debug.Log ("Move shop NOW!!!");

		shopPopScriptHolder.MoveOut_ShopAll ();

//		objShopInGameParentHolder.SetActive (isActive);
//		objShopInGameParentHolder.transform.localPosition = new Vector2 (6400, 0);
	}

	public void Pressed_CallInGameShop_In () {
		transDeathFullscreenEffectsHolder.localPosition = new Vector2 (-10000, 0);
		transDeathContinueHolder.localPosition = new Vector2 (-10000, 0);
		objShopInGameParentHolder.SetActive (true);

		// In case the shop button was pressed in potions menu
		if (PlayerData_InGame.Instance.isPotionsMenuOpen) {
			StartCoroutine (endlessPotionsController.PotionsButton_Disable ());
		}
	}

	public void Pressed_CallInGameShop_Out () {
		transDeathFullscreenEffectsHolder.localPosition = Vector2.zero;
		transDeathContinueHolder.localPosition = Vector2.zero;
//		Shop_Activator (true, 0);
	}
}
