﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class InGame_Controller : MonoBehaviour {

	public TrailRenderer trailRendHolder1;
	public TrailRenderer trailRendHolder2;
	public Transform trailTransHolder;

	public Transform trailFistParticleTransHolder1;
	public Transform trailFistParticleTransHolder2;

	public GameObject objPauseDisableHolder;

	private Vector2 v2MouseStart;
	private Vector2 v2MouseCurr;
	private Vector2 v2MouseEnd;
	private Vector2 v2MouseEndsAvg;
	private Vector2 v2MouseTotsAvg;
	private Vector2 v2Pos_MIN;
	private Vector2 v2Pos_MAX;
	private Vector2 v2PrevPoint;
	private Vector3 v3TrailPosBuffer;
	private bool NewTouchPoint_Allowed;
	private float xMin;
	private float yMin;
	private float xMax;
	private float yMax;
	private int touchPointCount;
	private int trailFistParticleRotationZ;

	private Pattern_Reader patterReaderRef;
	private TrailRenderer trailRendRef;
	private Camera camRef;
//	private ParticleSystem[] trailParticRef;
	[SerializeField]
	private ParticleSystem[] trailFistParticRef1;
	[SerializeField]
	private ParticleSystem[] trailFistParticRef2;
	private bool gameplayGestureAllowed;
	private bool mouseDownFirst;

	// Use this for initialization
	void Awake () {
		mouseDownFirst = false;

		// Start (Removed allow gesture from InGame Controller)
//		Invoke ("InGameCont_GameplayGesture_Allow", 1);

//		Invoke ("AllowGamePlay_Toggle", 2);

		// Old
//		trailParticRef = trailTransHolder.GetComponentsInChildren<ParticleSystem> ();

		camRef = Camera.main.GetComponent<Camera> ();
		patterReaderRef = GetComponent<Pattern_Reader> ();
//		trailRendRef = trailTransHolder.GetComponentInChildren<TrailRenderer> ();

		trailFistParticRef1 = trailFistParticleTransHolder1.GetComponentsInChildren<ParticleSystem> ();
//		trailFistParticRef2 = trailFistParticleTransHolder2.GetComponentsInChildren<ParticleSystem> ();
		trailRendHolder1.sortingLayerName = "UI Layer";
		trailRendHolder2.sortingLayerName = "UI Layer";

		trailRendHolder1.sortingOrder = 11;
		trailRendHolder2.sortingOrder = 11;

		if (Loading_Anim_Script.Instance != null) {

			// Initiate Loading End Anim
			StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_End_ToLevel ());
		}
			
		trailRendRef = trailRendHolder1;
		trailRendHolder2.enabled = false;
		trailRendHolder1.enabled = false;
//		ResetValues ();
	}

	public void Trails_Clear () {
		trailRendHolder1.Clear ();
		trailRendHolder2.Clear ();
	}
//	void OnEnable () {
//		EventManager.StopListening ("KillAll", KillAll);
//		EventManager.StopListening ("KillSingle", KillSingle);
//		EventManager.StopListening ("Paused", PauseStuff);
//	}
//
//	void OnDisable () {
//	}

	void ResetValues () {
//		trailRendRef.enabled = true;
		trailRendRef.Clear ();
		NewTouchPoint_Allowed = true;
		v2MouseEndsAvg = Vector2.zero;
		v2MouseTotsAvg = Vector2.zero;
		v2Pos_MIN = new Vector2 (10, 13) ;
		v2Pos_MAX = new Vector2 (-10, -13);
		touchPointCount = 0;
	}
		
	Vector2 GetMousePosition2D (Vector2 startPos) {
		startPos = camRef.ScreenToWorldPoint (Input.mousePosition);
		return startPos;
	}

	Vector2 GetUIPosition2D (Vector2 startPos) {
		startPos = camRef.ScreenToWorldPoint (startPos);
		return startPos;
	}

	public void Trail_TurnOff () {
		trailRendHolder2.enabled = false;
		trailRendHolder1.enabled = false;
	}

	void MouseStart () {
		mouseDownFirst = true;
		PlayerData_InGame.Instance.CalculateCritical ();
		if (!PlayerData_InGame.Instance.critHappened) {
			trailRendHolder2.enabled = false;
			trailRendHolder1.enabled = true;
			trailRendRef = trailRendHolder1;
		} else {
			trailRendHolder2.enabled = true;
			trailRendHolder1.enabled = false;
			trailRendRef = trailRendHolder2;

			// Increase crits number
			PlayerData_InGame.Instance.p1_TotalHits_Crits++;

			// Achievement for crit
			if (AchievementManager.Instance != null) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_DoCrit_50000 ());
			}
		}

		ResetValues ();

		trailRendRef.time = 1.3F;
//		trailRendRef.startWidth = 0.5F;
		trailRendRef.endWidth = 0.1F;
//		trailParticRef[0].Play ();
		touchPointCount = 0;
		v2MouseStart = GetMousePosition2D (v2MouseStart);
		v2MouseEndsAvg = UpdateAnAverage (v2MouseEndsAvg, v2MouseStart);
		AllowTouchPoint_Add (v2MouseStart, true);
	}

	void MouseDuring () {
		v2MouseCurr = GetMousePosition2D (v2MouseCurr);
		AllowTouchPoint_Add (v2MouseCurr, false);
		trailTransHolder.position = v2MouseCurr;
		v3TrailPosBuffer = trailTransHolder.localPosition;
		v3TrailPosBuffer.z = 0;
		trailTransHolder.localPosition = v3TrailPosBuffer;
	}

	void MouseEnd () {
		if (mouseDownFirst) {
			Invoke ("DisableTrailRenderer", 0.25F);
			trailRendRef.time = 0.25F;
//			trailRendRef.startWidth = 0.35F;
			trailRendRef.endWidth = 0.13F;
//			trailParticRef[0].Stop ();
//			trailParticRef[1].Play ();

			v2MouseEnd = GetMousePosition2D (v2MouseEnd);
			AllowTouchPoint_Add (v2MouseEnd, true);
			v2MouseEndsAvg = UpdateAnAverage (v2MouseEndsAvg, v2MouseEnd);

			v2MouseTotsAvg = v2MouseTotsAvg / touchPointCount;
			v2MouseEndsAvg = v2MouseEndsAvg / 2;

			// Report Values
//			Debug.Log ("v2Pos_MIN : " + v2Pos_MIN + " v2Pos_MAX: " + v2Pos_MAX + " v2MouseStart: " + v2MouseStart);
//			Debug.Log (" v2MouseEnd: " + v2MouseEnd + " v2MouseEndsAvg: " + v2MouseEndsAvg + " v2MouseTotsAvg: " + v2MouseTotsAvg);

			// Call Pattern Reader
			patterReaderRef.ReadPattern (v2Pos_MIN, v2Pos_MAX, v2MouseStart, v2MouseEnd, v2MouseEndsAvg, v2MouseTotsAvg);

			// For end of gesture fist particle
			if (patterReaderRef.wasDrawn) {
				MouseEnd_FistParticle ();
//				Invoke ("MouseEnd_FistParticle", 0.01F);
			}
			mouseDownFirst = false;
		}
	}

	public void MouseEnd_FistParticle () {
		trailFistParticleTransHolder1.position = v2MouseEnd;
		switch (EventTrigger.theKey_Direction) {
		case "U":
			trailFistParticleRotationZ = 90;
			break;
		case "D":
			trailFistParticleRotationZ = 270;
			break;
		case "R":
			trailFistParticleRotationZ = 0;
			break;
		case "L":
			trailFistParticleRotationZ = 180;
			break;
		default:
			trailFistParticleRotationZ = 0;
			break;
		}

		trailFistParticleTransHolder1.eulerAngles = new Vector3 (0, 0, trailFistParticleRotationZ);
		if (!PlayerData_InGame.Instance.critHappened) {
            trailFistParticRef1 [0].Play ();
		} else {
            trailFistParticRef1 [2].Play ();
		}
	}

	IEnumerator AllowTouchPoint_Bool () {
		NewTouchPoint_Allowed = false;
		yield return new WaitForSeconds (0.01F);
		NewTouchPoint_Allowed = true;
	}

	void AllowTouchPoint_Add (Vector2 addedV2, bool instantAdd) {
		if (NewTouchPoint_Allowed || instantAdd) {
			StartCoroutine (AllowTouchPoint_Bool ());
			touchPointCount++;
//			Debug.LogError ("touchPointCount: " + touchPointCount);
			v2MouseTotsAvg = UpdateAnAverage (v2MouseTotsAvg, addedV2);
			UpdateMinMax (addedV2.x, addedV2.y);
		}
	}
		
	Vector2 UpdateAnAverage (Vector2 pointAverage, Vector2 pointNew) {
		pointAverage += pointNew;
		return pointAverage;
	}

	void UpdateMinMax (float x, float y) {
		xMin = v2Pos_MIN.x;
		xMax = v2Pos_MAX.x;
		yMin = v2Pos_MIN.y;
		yMax = v2Pos_MAX.y;
		v2Pos_MIN.x = xMin > x ? x : xMin;
		v2Pos_MIN.y = yMin > y ? y : yMin;
		v2Pos_MAX.x = xMax < x ? x : xMax;
		v2Pos_MAX.y = yMax < y ? y : yMax;
	}

	public void GamePad_Button (string input) {
		EventTrigger.theKey_Direction = input;
		EventTrigger.theKey_Shape1 = input;
		EventTrigger.theKey_Shape2 = input;
		EventManager.TriggerEvent ("Hurt Enemies");
	}

	public void InGameCont_GameplayGesture_Allow () {
		PlayerData_InGame.Instance.gameplayGestureAllowed = true;
	}

	public void InGameCont_GameplayGesture_Prevent () {
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
	}

	void DisableTrailRenderer () {
		trailRendRef.enabled = false;
	}

//	public void OnMouseDown () {
//		if (!EventTrigger.isPaused && PlayerData_InGame.Instance.gameplayGestureAllowed) {
//			MouseStart ();
//		}
//	}
//
//	public void OnMouseDrag () {
//		if (!EventTrigger.isPaused && PlayerData_InGame.Instance.gameplayGestureAllowed) {
//			MouseDuring ();
//		}
//	}
//
//	public void OnMouseUp () {
//		if (!EventTrigger.isPaused && PlayerData_InGame.Instance.gameplayGestureAllowed) {
//			MouseEnd ();
//		}
//	}
		
//		 Main Input. DONT COMMENT!

	void Update () {
		if (!EventTrigger.isPaused && PlayerData_InGame.Instance.gameplayGestureAllowed) {
			if (Input.GetMouseButtonDown (0)) {
				MouseStart ();
			}
			if (Input.GetMouseButton (0)) {
				MouseDuring ();
			}
			if (Input.GetMouseButtonUp (0)) {
				MouseEnd ();
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (!objPauseDisableHolder.activeInHierarchy) {
					PlayerData_InGame.Instance.hudScriptHolder.objPauseMenuItselfHolder.gameObject.SetActive (true);
					PlayerData_InGame.Instance.hudScriptHolder.PauseFromCam ();
				}
			}

//			if (Input.GetKeyDown ("s")) {
//				if (Time.timeScale == 1) {
//					Time.timeScale = 5;
//				} else {
//					Time.timeScale = 1;
//				}
//			}

		}

		if (Input.GetKeyDown ("s")) {
			if (Time.timeScale == 1) {
				Time.timeScale = 5;
			} else {
				Time.timeScale = 1;
			}
		}
	}

}
