﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Item_Activation_Feed_Script : MonoBehaviour {

	public Canvas canvasItemActHolder;
	public Text text_ItemActiveEffect;

	public Animation animItemActFeedHolder;
	
	public void ItemActivateFeed_RemoteNOW (string nameOfEffect) {
		StartCoroutine (ItemActivateFeed_Routine (nameOfEffect));

	}

	public IEnumerator ItemActivateFeed_Routine (string nameOfEffect) {
		ItemActivate_MoveIn ();
		text_ItemActiveEffect.text = nameOfEffect;
		animItemActFeedHolder.Rewind ();
		animItemActFeedHolder.Play ();
		yield return null;
	}

	public void ItemActivate_MoveIn () {
		canvasItemActHolder.enabled = true;
		this.transform.localPosition = Vector2.zero;
	}

	public void ItemActivate_MoveOut () {
		canvasItemActHolder.enabled = false;
		this.transform.localPosition = new Vector2 (3200, 0);
	}
}
