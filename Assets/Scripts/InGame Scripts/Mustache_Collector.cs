﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mustache_Collector : MonoBehaviour {

	public Text text_MustacheGainedAmount;
	public Animation animMustacheGainHolder;
	public Animation animMustacheMoverHolder;
	public BoxCollider2D boxColl2DHolder;

	[Header ("Many Scripts")]
	public Menu_Main_Script mainMenuScriptHolder;
	public Reward_Machine_Script rewardMachineScriptHolder;
	public Shop_Popup_Script shopPopupScriptHolder;

	public enum MustacheCollideType
	{
		mustacheFor_Shop,
		mustacheFor_InGame,
		mustacheFor_Reward,
		mustacheFor_QuestAchievement
	}

	[Header ("My Mustache Collect Type")]
	public MustacheCollideType myMustacheCollideType;

	private int mustacheGained_Int_Actual;
	private int mustacheGained_Int_ForDisplay;

	public void Collector_Controller (bool isCollActive) {
		boxColl2DHolder.enabled = isCollActive;
	}

	void Awake () {
//		if (myMustacheCollideType == MustacheCollideType.mustacheFor_InGame) {
//			Collector_Controller (true);
//		} else {
//			Collector_Controller (false);
//		}

//		switch (myMustacheCollideType) {
//		case MustacheCollideType.mustacheFor_InGame:
//			Collector_Controller (true);
//			break;
//		case MustacheCollideType.mustacheFor_Reward:
//			Collector_Controller (false);
//			break;
//		case MustacheCollideType.mustacheFor_Shop:
//			Collector_Controller (false);
//			break;
//		default:
//			Collector_Controller (false);
//			break;
//		}
	}

	void OnTriggerEnter2D (Collider2D otherObj) {
//		Debug.Log ("Collision is: " + otherObj.tag + " and name: " + otherObj.name + " and its parent: " + otherObj.transform.parent.name);

//		if (myMustacheCollideType == MustacheCollideType.mustacheFor_InGame || myMustacheCollideType == MustacheCollideType.mustacheFor_Reward) {
//			mustacheGained_Int_Actual = otherObj.GetComponentInParent<Mustache_Script> ().mustacheAmount;
//		} else {
//			mustacheGained_Int_Actual = otherObj.GetComponentInParent<Mustache_Shop_Script> ().mustacheAmount;
//		}

		switch (myMustacheCollideType) {
		case MustacheCollideType.mustacheFor_InGame:
			mustacheGained_Int_Actual = otherObj.GetComponentInParent<Mustache_Script> ().mustacheAmount;
			MustacheCollector_GiveMe (mustacheGained_Int_Actual);
			break;
		case MustacheCollideType.mustacheFor_Reward:
			mustacheGained_Int_Actual = otherObj.GetComponentInParent<Mustache_Script> ().mustacheAmount;
			MustacheCollector_GiveMeFromReward (mustacheGained_Int_Actual);
			break;
		case MustacheCollideType.mustacheFor_Shop:
			mustacheGained_Int_Actual = otherObj.GetComponentInParent<Mustache_Shop_Script> ().mustacheAmount;
			MustacheCollector_GiveMeFromShop (mustacheGained_Int_Actual);
			break;
		default:
			MustacheCollector_GiveMeFromReward (mustacheGained_Int_Actual);
			break;
		}

        // SOUNDD EFFECTS - Barkhord Sibil be Jar (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.SibilToGholak.Play();
        }

		Destroy (otherObj.transform.parent.gameObject);
	}

	// REMEMBER: There are to GIVE ME Scripts. This is for in-game. The other one is for shop
	public void MustacheCollector_GiveMe (int howMuch) {
		StartCoroutine (PlayerData_InGame.Instance.PlayerMustache_Gain (howMuch));

		// Update stats for mustache (New Place) (Previous in mustache script of each enemy type)
		PlayerData_InGame.Instance.stats_MustacheGained += howMuch;

		// Check to reset mustache gain amount
		if (!animMustacheGainHolder.isPlaying) {
			//			Debug.LogError ("RESET!");
			MustacheGained_Reset ();
		}
		// Mustache Gain Anims & Logic
		MustacheGained_Update (howMuch);
		animMustacheGainHolder.Play ();
//		Debug.LogWarning ("Anim Name: " + animMustacheGainHolder.clip.name);

		// Quest Stuff: Gain Mustache
		Quest_MustacheGain (howMuch);
	}

	public void MustacheCollector_GiveMeFromShop (int howMuch) {
		PlayerData_Main.Instance.Player_Mustache_Add (howMuch);

		// Sync both mustaches
		Mustache_Sync_Script.Mustache_SyncNow (PlayerData_Main.Instance.player_Mustache);

		// Update normal in-game HUD mustache amount
		if (PlayerData_InGame.Instance != null) {
			PlayerData_InGame.Instance.PlayerMustache_Gain (0);
		} else {
			// Update values of menu after purchase outside in-game
			mainMenuScriptHolder.Menu_HUD_Update ();
		}

		// Check to reset mustache gain amount
		if (!animMustacheGainHolder.isPlaying) {
			//			Debug.LogError ("RESET!");
			MustacheGained_Reset ();
		}

		// Mustache Gain Anims & Logic
		MustacheGained_Update (howMuch);
		animMustacheGainHolder.Play ();

		// Update Shop Display
		shopPopupScriptHolder.Shop_HUD_Update ();

		// Quest Stuff: Gain Mustache
		Quest_MustacheGain (howMuch);
	}

	public void MustacheCollector_GiveMeFromReward (int howMuch) {
		if (PlayerData_InGame.Instance != null) {
			MustacheCollector_GiveMe (howMuch);
		} else {
			MustacheCollector_GiveMeFromShop (howMuch);
		}


		// Update Reward Mustache Display
		if (PlayerData_InGame.Instance != null) {
			rewardMachineScriptHolder.Rewards_Update_Mustache (PlayerData_InGame.Instance.p1_Moustache);
		} else {
			rewardMachineScriptHolder.Rewards_Update_Mustache (PlayerData_Main.Instance.player_Mustache);
		}

		if (PlayerData_InGame.Instance == null) {
			mainMenuScriptHolder.Menu_HUD_Update ();
		}

		// Gain anim for mover (not the text)
		animMustacheMoverHolder.Play ("HUD Mustache Gain Anim 1 (Legacy)");

		// Quest Stuff: Gain Mustache
		Quest_MustacheGain (howMuch);
	}

	public void MustacheCollector_GiveMeFromQuestAchieve (int howMuch) {
		PlayerData_Main.Instance.Player_Mustache_Add (howMuch);

		// Update nmenu HUD mustache amount
		mainMenuScriptHolder.Menu_HUD_Update ();

		// Gain anim for mover (not the text)
		animMustacheMoverHolder.Play ("HUD Mustache Gain Anim 2 ForQuestAchieve (Legacy)");

		// Check to reset mustache gain amount
		if (!animMustacheGainHolder.isPlaying) {
			MustacheGained_Reset ();
		}

		// Mustache Gain Anims & Logic
		MustacheGained_Update (howMuch);
		animMustacheGainHolder.Play ("HUD Mustache Text Gain Anim 2 QuestAchieve (Legacy)");

		// Quest Stuff: Gain Mustache
		Quest_MustacheGain (howMuch);
	}
		
	void MustacheGained_Reset () {
		mustacheGained_Int_ForDisplay = 0;
	}
		
	void MustacheGained_Update (int howMuch) {
		animMustacheGainHolder.Stop ();
		animMustacheGainHolder.Play ();

		mustacheGained_Int_ForDisplay += howMuch;
		text_MustacheGainedAmount.text = "+ " + mustacheGained_Int_ForDisplay.ToString ();
	}

	void Quest_MustacheGain (int howMuchMustache) {
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_MustacheGain_Amount (howMuchMustache));
		}
	}
}
