﻿using UnityEngine;
using System.Collections;

public class Mustache_Sync_Script {

	public static void Mustache_SyncNow (int totalAmount) {
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.player_Mustache = totalAmount;
		} 

		if (PlayerData_InGame.Instance != null) {
			PlayerData_InGame.Instance.p1_Moustache = totalAmount;
		}
	}
}
