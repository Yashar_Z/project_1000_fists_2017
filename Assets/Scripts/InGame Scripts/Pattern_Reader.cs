﻿using UnityEngine;
using System.Collections;
// DELETE Later?
using UnityEngine.UI;

public class Pattern_Reader : MonoBehaviour {

	public Text textTest_Direction;
	public Text textTest_Shape;
	public Camera_InGame_Script camIngameScriptHolder;
	public bool wasDrawn;

	private bool line_Y;
	private bool line_X;
	private string pattern_Result_Shape1;
	private string pattern_Result_Shape2;
	private string pattern_Result_Direction;

//	private bool bigShape;
//	private int countMaxDiffTooBig;
//	private Vector2 endsAvg_Abs;
//	private Vector2 averagesDiff;

	private Vector2 endsDiff;
//	private Vector2 endsDiff_Abs;

	private Vector2 endsMINUSminMaxAvgs;
	private Vector2 minMaxDiff;
	private Vector2 minMaxAvg;

	private Vector2 maxMINUSstart;
	private Vector2 minMINUSstart;
	private Vector2 maxMINUSend;
	private Vector2 minMINUSend;

	public void ReadPattern (Vector2 min, Vector2 max, Vector2 start, Vector2 end, Vector2 endsAvg, Vector2 totsAvg) {

		EventTrigger.isCorrectHit = false;
		// Show Call Reader 
//		Debug.Log ("Reader was called");

		// Reader Initialize
		pattern_Result_Shape1 = "";
		pattern_Result_Shape2 = "";
		pattern_Result_Direction = "";
		wasDrawn = false;
		endsDiff = end - start;
//		endsDiff_Abs = MakeThisAbsolute (endsDiff);
		//
		maxMINUSstart = max - start;
		maxMINUSend = max - end;
		minMINUSstart = min - start;
		minMINUSend = min - end;

		// Result: Arc Patterns
		// Report
//		Debug.LogError ("11 maxMINUSstart.x = " + maxMINUSstart.x + " And maxMINUSend.x = " + maxMINUSend.x);
//		Debug.LogError ("22 minMINUSstart.x = " + minMINUSstart.x + " And minMINUSend.x = " + minMINUSend.x);
		//

		// Old Number
//		if (maxMINUSstart.x > 0.35F && maxMINUSend.x > 0.35F ) {

//		if (maxMINUSstart.x > 0.5F && maxMINUSend.x > 0.5F ) {
		if (maxMINUSstart.x > 1.1F && maxMINUSend.x > 1.1F) {
//			Debug.LogError ("Right triangle");
			pattern_Result_Shape1 = ">";
			pattern_Result_Shape2 = ">";
			pattern_Result_Direction = "L";

			// For old triangle directions
//			if (endsDiff.y > 0) {
//				pattern_Result_Direction = "U";
//			} else {
//				pattern_Result_Direction = "D";
//			}

			wasDrawn = true;
		} else { 
			// Old Number
//			if (minMINUSstart.x < -0.35F  && minMINUSend.x < -0.35F ) {
				
//			if (minMINUSstart.x < -0.5F  && minMINUSend.x < -0.5F ) {
			if (minMINUSstart.x < -1.1F  && minMINUSend.x < -1.1F ) {
//				Debug.LogError ("Left triangle");
				pattern_Result_Shape1 = "<";
				pattern_Result_Shape2 = "<";
				pattern_Result_Direction = "R";

				// For old triangle directions
//				if (endsDiff.y > 0) {
//					pattern_Result_Direction = "U";
//				} else {
//					pattern_Result_Direction = "D";
//				}

				wasDrawn = true;
			} else {
				// Report
//				Debug.LogError ("333 maxMINUSstart.y = " + maxMINUSstart.y + " And maxMINUSend.y = " + maxMINUSend.y);
//				Debug.LogError ("444 minMINUSstart.y = " + minMINUSstart.y + " And minMINUSend.y = " + minMINUSend.y);
				//

				// Old Number
//				if (maxMINUSstart.y > 0.3F  && maxMINUSend.y > 0.3F ) {
					
//				if (maxMINUSstart.y > 0.45F  && maxMINUSend.y > 0.45F ) {
				if (maxMINUSstart.y > 0.9F  && maxMINUSend.y > 0.9F ) {
//					Debug.LogError ("Up triangle");
					pattern_Result_Shape1 = "^";
					pattern_Result_Shape2 = "^";
					pattern_Result_Direction = "D";

					// For old triangle directions
//					if (endsDiff.x > 0) {
//						pattern_Result_Direction = "R";
//					} else {
//						pattern_Result_Direction = "L";
//					}

					wasDrawn = true;
				} else { 
					// Old Number
//					if (maxMINUSstart.y > 0.3F  && maxMINUSend.y > 0.3F ) {

//					if (minMINUSstart.y < -0.45F  && minMINUSend.y < -0.45F ) {
					if (minMINUSstart.y < -0.9F  && minMINUSend.y < -0.9F ) {
//						Debug.LogError ("Down triangle");
						pattern_Result_Shape1 = "v";
						pattern_Result_Shape2 = "v";
						pattern_Result_Direction = "U";

						// For old triangle directions
//						if (endsDiff.x > 0) {
//							pattern_Result_Direction = "R";
//						} else {
//							pattern_Result_Direction = "L";
//						}

						wasDrawn = true;
					}
				}
			}
		}

//		Debug.LogError ("So far");

		// Result: Arrows
		if (!wasDrawn) {
			minMaxDiff = min - max;
			minMaxDiff = MakeThisAbsolute (minMaxDiff);
//			Debug.Log ("AFTER ABS min max diff Vec2: " + minMaxDiff);
			//
			if (minMaxDiff.x > 1.1F && minMaxDiff.x > minMaxDiff.y) {
//								Debug.Log ("X diff is BIIIIIIIIGGGGG");
				line_X = true;
			} else {
//								Debug.Log ("X diff is small");
				line_X = false;
			}

			if (minMaxDiff.y > 1.1F && minMaxDiff.y > minMaxDiff.x) {
//								Debug.Log ("Y diff is BIIIIIIIIGGGGG\"");
				line_Y = true;
			} else {
//								Debug.Log ("Y diff is small");
				line_Y = false;
			}

			if (line_X && !line_Y) {
				if (endsDiff.x > 0) {
					pattern_Result_Direction = "R";
					pattern_Result_Shape1 = "-";
					pattern_Result_Shape2 = "R";
					wasDrawn = true;
//					patternEndName = "Right Arrow Parent";
				} else {
					pattern_Result_Direction = "L";
					pattern_Result_Shape1 = "-";
					pattern_Result_Shape2 = "L";
					wasDrawn = true;
//					patternEndName = "Left Arrow Parent";
				}
			}

			if (!line_X & line_Y) {
				if (endsDiff.y > 0) {
					pattern_Result_Direction = "U";
					pattern_Result_Shape1 = "|";
					pattern_Result_Shape2 = "U";
					wasDrawn = true;
//					patternEndName = "Up Arrow Parent";
				} else {
					pattern_Result_Direction = "D";
					pattern_Result_Shape1 = "|";
					pattern_Result_Shape2 = "D";
					wasDrawn = true;
//					patternEndName = "Down Arrow Parent";
				}
			}

			// For Diagonal
//			if (line_X && line_Y) {
//				if (endsDiff.x > 0) {
//					if (endsDiff.y > 0) {
//						PlayResultParticle2 (45);
//						patternEndName = "Diag Up Right Arrow Parent";
//					} else {
//						PlayResultParticle2 (-45);
//						patternEndName = "Diag Down Right Arrow Parent";
//					}
//				} else {
//					if (endsDiff.y > 0) {
//						PlayResultParticle2 (135);
//						patternEndName = "Diag Up Left Arrow Parent";
//					} else {
//						PlayResultParticle2 (-135);
//						patternEndName = "Diag Down Left Arrow Parent";
//					}
//				}
//			}

		}

		// Report Results
		if (wasDrawn) {
//			Debug.Log ("Player Attack!");
			EventTrigger.theKey_Direction = pattern_Result_Direction;
			EventTrigger.theKey_Shape1 = pattern_Result_Shape1;
			EventTrigger.theKey_Shape2 = pattern_Result_Shape2;

			// Old place
//			EventManager.TriggerEvent ("Hurt Enemies");

			// Call Camera Effect
			camIngameScriptHolder.PlayAnim_PlayerAttack(pattern_Result_Direction);

			// Increase total hits
			if (!EventTrigger.isFrozen || !EventTrigger.isPaused) {
				PlayerData_InGame.Instance.p1_TotalHits_All++;
			}

			// HF Clingy AND Normal Enemy Hurt
			if (!PlayerData_InGame.Instance.P1_HF_ClingyInTheFrontBool) {
				// Before sending out the event, set intOneComboCount to 0
				PlayerData_InGame.Instance.intOneComboCount = 0;

				EventManager.TriggerEvent ("Hurt Enemies");
			} else {
				PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_Hit (1);
			}

			// Item Durablity
			PlayerData_InGame.Instance.PlayerItem_ItemDurabilityDecrease (2);
			// Pipe probably used to be here but now is in camera, in the place where there is no miss and yes pipe

			// For early text tests
			//			textTest_Direction.text = pattern_Result_Direction;
			//			textTest_Shape.text = pattern_Result_Shape1;
		}
	}

	public Vector2 MakeThisAbsolute (Vector2 v2ForAbs) {
		v2ForAbs.x = Mathf.Abs (v2ForAbs.x);
		v2ForAbs.y = Mathf.Abs (v2ForAbs.y);
		return v2ForAbs;
	}

}
