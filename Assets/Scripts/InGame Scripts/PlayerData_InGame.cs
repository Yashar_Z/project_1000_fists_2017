﻿using UnityEngine;
using System.Collections;

public class PlayerData_InGame : MonoBehaviour {

	static public PlayerData_InGame Instance;

	[Header ("Player Base Elements")]
	public int p1_Moustache;
	public int p1_HP_Curr;
	public int p1_HP_Start;

	[Range (1, 50)]
	public int p1_Damage_Base; 
	[Range (0, 99)]
	public int p1_CritChance;
	[Range (0, 99)]
	public int p1_ShieldChanceTotal;

	[Header ("Level Elements")]
//	[Header ("")]
	public int continueCost;
	public float missesThresholdPercent;

//	[Header ("")]
	[Header ("Level Testing Elements: Reward, Tutorials & More")]
	public bool rewardTheLevel;
	public bool tuturialRemove;
	public bool isThisTestLevel;
	public bool isThisNoReward;
	public bool isThisNonBuildVersion;

	[Header ("For Endless")]
	public GameSpawnType gameSpawnerType;

	[Header ("Power Up Stuff")]
	// Player power ups stuff
//	[Header ("")]
	public int p1_PowUp1_Count_Own;
	public int p1_PowUp2_Count_Own;
	public int p1_PowUp1_Count_Ready;
	public int p1_PowUp2_Count_Ready;

	public float p1_PowUp2Duration;

	[Header ("Endless Stuff")]
	public int p1_Score_Curr;
	public int p1_Score_Record;

	public int p1_ComboMultiplier;
	public int p1_EndlessLevel;

	public int p1_XP_Curr;

	[Header ("Other Stuff")]
	public HUD_InGame hudScriptHolder;
	public End_Level_Script endLevelScriptHolder;
	public Item_Pickup_Script[] itemPickupScriptArr;
	public Item_Gain_Script[] itemGainScriptArr;
	public ItemEffects_Script itemEffectsScriptHolder;
	public Worlds_InGame_Script worldsInGameScriptHolder;
	public Item_Activation_Feed_Script itemActivFeedScriptHolder;

	public float p1_MustacheMultiplier_Base;
	public float p1_MustacheMultiplier_Cur;

//	public float p1_PowUp1_FillFull;
//	public float p1_PowUp2_FillFull;
//	public float p1_PowUp1_FillCurr;
//	public float p1_PowUp2_FillCurr;

	public bool p1_PowUp1_Active;
	public bool p1_PowUp2_Active;

	public int p1_TotalHits_All;
	public int p1_TotalHits_Crits;
	public int p1_TotalHits_Misses;

	// For shield percent
	public int p1_TotalHurts;
	public int p1_TotalShields;

	// For Bonus kills percent
	public int p1_TotalKills_All;
	public int p1_TotalKills_LuckyExplode;

	// Bonus Health for Killing Enemies
	[Header ("Kill Bonus Stuff")]
	public int p1_KIllBonus_EnemyCount;
	public int p1_KIllBonus_EnemyRequirement;
	public int p1_KIllBonus_HealAmount;
	private bool p1_HasKillBonus;

	// Lucky Explode Happen Chance & Last Stand Shout Count
	[Header("")]
	public float p1_LuckyExplodeChance;
	public int p1_LastStandShoutCount;

	public int p1_ItemValueIncrease;
	public int p1_FreezeDirectionInt;
	public int p1_isAntiHF_Sometimes;
	public int p1_isAntiHedgehog_Sometimes;
	public bool p1_Invincible;
	public bool p1_InvincibleBuffer;
	public bool p1_isDead;

	// Timed Item Effects
	public bool p1_Item_ArrowHappyTime;
	public bool p1_Item_PipeIsActive;
//	public bool p1_Item_PipeActiveTime;
//	public bool p1_Item_FreezedTime;

	// HF Clingy Front Effect
	public bool P1_Help_HFX_CanKillClingy;
	public bool P1_HF_ClingyInTheFrontBool;
	public int P1_HF_ClingyInTheFrontCount;

	// Barrel Elements
	public int P1_Barrel_HeadIsUpCount;

	public bool LoadFromMenu;
	public bool critHappened;
	public bool critDuring;
	public bool option_OrigEffects_Active;
	public bool GameOverRequiresTouch;
	public bool fullIntro_Allowed;

//	public bool fullAttackAnim_Allowed;

	public float introAnimLength;
	public float introFirstLevelAddition;

	public bool wonTheLevel;

	// Level Stats & End Paremeters
	[Header ("Main Stats Numbers")]
	public int stats_EnemiesKilled;
	public int stats_MustacheGained;
	public int stats_StreakAchieved;

	[Header ("")]
	public int stats_EnemiesTotalNumber;
	public int stats_ContinuesUsed;

	[Header ("Stars")]
	public bool stats_Stars_Completed;
	public bool stats_Stars_Untouchable;
	public bool stats_Stars_Unmissable;

	[HideInInspector]
	public bool isFirstTimePlaying;

	[HideInInspector]
	public bool isFirstToMenu;

	// HF Clingy health / length
	public int P1_HF_ClingyHP;
	public int P1_HF_CountTotal;

	// HF Explosive Data
	public int P1_HF_ExplosionDamage;

	// Barrel data
	public int P1_Barrel_Q_ExplodeDamage;
	public int P1_Barrel_Q_HedgehogDamage;
	public int P1_Barrel_X_ExplodeDamage;
	public float P1_Barrel_N_HeadUpTime;
	public float P1_Barrel_Q_HeadUpTime;
	public float P1_Barrel_X_HeadUpTime;

	public bool gameplayGestureAllowed;

	[HideInInspector]
	public bool lastEnemyWasSpawned;

	[HideInInspector]
	public bool lastEnemyWasKilled;

	[Header ("For Achievements")]
	public int p1_PowUps_Used;
	public int p1_PowUp1_OnlyUsed;
	public int p1_1Health4Times_Counter;
	public System.DateTime timeFreezeStart;

	public int PickupItemCounter_Spawned;
	public int PickupItemCounter_Picked;

	public int HedgehogItemCounter_Spawned;
	public int HedgehogItemCounter_Picked;

	public bool PipeItemGained_Bool = false;
	public int PipeItemUsed_Resetter;

	// For Endless
	public enum GameSpawnType
	{
		Campaign,
		Endless
	}

	public bool isEndlessEggsRewarding;

	public bool isPotionsMenuOpen;
	public int intOneComboCount;

	// Enemies spawn here (ref)
	private Transform transEnemiesSpawnParentRef;

	private int intShieldChanceTotalBuffer;
	private float floatShieldTimeCounter;

	private int in_Damage_BaseOrig;

	void OnDisable () {
		if (PlayerData_Main.Instance != null) {
			PassValues_InGame_To_Main ();
		}
		SaveLoad.Save ();
		//		EventManager.StopListening ("Hurt Player", Bob);

		// SOUNDD EFFECTS - Shield Circling The Screen Loop End (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.ShieldPowerupTime.Stop();
		}
	}
		
	public void PassValues_InGame_To_Main () {
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.lastLevelWasWon = wonTheLevel;

//			PlayerData_Main.Instance.Player_Mustache_Add (p1_Moustache - PlayerData_Main.Instance.player_Mustache);
			PlayerData_Main.Instance.player_Mustache = p1_Moustache;

			PlayerData_Main.Instance.player_PowUp1_Own = p1_PowUp1_Count_Own;
			PlayerData_Main.Instance.player_PowUp2_Own = p1_PowUp2_Count_Own;

			PlayerData_Main.Instance.player_PowUp1_Ready = p1_PowUp1_Count_Ready;
			PlayerData_Main.Instance.player_PowUp2_Ready = p1_PowUp2_Count_Ready;

			// Pass values to player data main for endless
			if (gameSpawnerType == GameSpawnType.Endless) {
				PassValues_Endless_InGameToMain ();
			}
		}
	}

	public void PassValues_Endless_InGameToMain () {
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.player_XP_Curr = p1_XP_Curr;
			PlayerData_Main.Instance.player_Score_Record = p1_Score_Record;
			PlayerData_Main.Instance.player_EndlessLevel = p1_EndlessLevel;
		}
	}

	void Awake () {
		Instance = this;

		// Turn allow new level in main to false AND Get the Endless or Normal spawner
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.Show_NewLevel_Allow = false;

			// Set spawn type of PlayerData_Ingame based on Mains
			gameSpawnerType = PlayerData_Main.Instance.gameMainSpawnType;
		}

		// For check to see if is BUILD or NOT
		if (PlayerData_Main.Instance != null) {
			if (PlayerData_Main.Instance.isFullBuild) {
				rewardTheLevel = false;
				tuturialRemove = false;
				isThisTestLevel = false;
				isThisNoReward = false;
				isThisNonBuildVersion = false;
			} else {
				switch (PlayerData_Main.Instance.whatPlayType) {
				case PlayerData_Main.PlayerMainType.Play_TestLevels_NoTut:
					tuturialRemove = false;
					isThisTestLevel = true;
					break;
				case PlayerData_Main.PlayerMainType.Play_TestLevels_YesTut:
					tuturialRemove = true;
					isThisTestLevel = true;
					break;
				case PlayerData_Main.PlayerMainType.Play_RealLevels_NoTut:
					tuturialRemove = true;
					isThisTestLevel = false;
					break;
				case PlayerData_Main.PlayerMainType.Play_RealLevels_YesTut:
					tuturialRemove = false;
					isThisTestLevel = false;
					break;
				default:
					tuturialRemove = false;
					isThisTestLevel = false;
					break;
				}
			}
		}

		// Get Continue Cost (From Balance Constants)
		continueCost = Balance_Constants_Script.ContinueCost;

		// Player Gesture Prevent
		gameplayGestureAllowed = false;

		// Last enemy is ALIVE!
		lastEnemyWasKilled = false;
		lastEnemyWasSpawned = false;

		// Check if from menu
		if (PlayerData_Main.Instance != null) {
			LoadFromMenu = true;
		}
			
		critDuring = false;
		p1_Item_ArrowHappyTime = false;

		if (PlayerData_Main.Instance == null) {
			Application.targetFrameRate = 60;
		}

		// Setup Level Stats & End
		stats_EnemiesKilled = 0;
		stats_StreakAchieved = 0;
		stats_ContinuesUsed = 0;
		stats_MustacheGained = 0;
		stats_Stars_Untouchable = true;
		stats_Stars_Unmissable = true;

		// Enemy Count Stuff
		p1_HasKillBonus = false;
		p1_KIllBonus_EnemyCount = 0;
		p1_KIllBonus_HealAmount = 0;

		if (LoadFromMenu) {
			Load_From_PlayerData_Main ();

			// Extend intro
			introAnimLength += 1.5F;

		}
			
		// Implement Item Effects AFTER LoadFromMenu
		if (PlayerData_Main.Instance != null) {
			Player_ItemsEffects_Implement ();
		}

		// Was previously in Player_ItemsEffects_Implement but was moved here
		SetDamage_OriginalBase ();

		// New Setup HUD
		SetupHUD ();

		// Setup hits
		p1_TotalHits_All = 0;
		p1_TotalHits_Misses = 0;

		// For shields and other number
		p1_TotalHurts = 0;
		p1_TotalShields = 0;
		p1_TotalKills_All = 0;
		p1_TotalKills_LuckyExplode = 0;

		// Disable PickupIt
		itemPickupScriptArr [0].gameObject.SetActive (false);
		itemPickupScriptArr [1].gameObject.SetActive (false);
		itemPickupScriptArr [2].gameObject.SetActive (false);

		// HF Clingy front
		P1_HF_ClingyInTheFrontBool = false;
		P1_HF_ClingyInTheFrontCount = 0;
		P1_HF_CountTotal = 0;
//		P1_HF_ClingyHP = 0;

		// Reset explosion damage
//		ExplosionDamage_Reset ();
		p1_PowUp1_Active = false;
		p1_PowUp2_Active = false;

		// Not dead!
		p1_isDead = false;
	}

	public void SetDamage_OriginalBase () {
		// This is to fix the potions problem in case player gets pipe during endless
		in_Damage_BaseOrig = p1_Damage_Base;
	}

	void Load_From_PlayerData_Main () {
		wonTheLevel = false;

		// Needs to read from main
		p1_Moustache = PlayerData_Main.Instance.player_Mustache;

		p1_PowUp1_Count_Own = PlayerData_Main.Instance.player_PowUp1_Own;
		p1_PowUp2_Count_Own = PlayerData_Main.Instance.player_PowUp2_Own;

		p1_PowUp1_Count_Ready = PlayerData_Main.Instance.player_PowUp1_Ready;
		p1_PowUp2_Count_Ready = PlayerData_Main.Instance.player_PowUp2_Ready;

		// (Old) Previously read from player main
//		p1_HP_Curr = PlayerData_Main.Instance.player_HP;
//		p1_HP_Start = PlayerData_Main.Instance.player_HP;
//		p1_Damage_Base = PlayerData_Main.Instance.player_Damage; 
//		p1_CritChance = PlayerData_Main.Instance.player_CritChance;
//		p1_ShieldChanceTotal = PlayerData_Main.Instance.player_ShieldChance;
//		p1_PowUp2Duration = PlayerData_Main.Instance.player_PowUp2Duration;
//		p1_MustacheMultiplier_Cur = p1_MustacheMultiplier_Base;
//		p1_LuckyExplodeChance = 0;

		// Set last scene bool from player main
		PlayerData_Main.Instance.LastScene_WasInGame_ChangeTo (true);

		// Get endless values from playerdata main:
		if (gameSpawnerType == GameSpawnType.Endless) {
			p1_XP_Curr = PlayerData_Main.Instance.player_XP_Curr;
			p1_Score_Record = PlayerData_Main.Instance.player_Score_Record;
			p1_EndlessLevel = PlayerData_Main.Instance.player_EndlessLevel;

			if (p1_EndlessLevel == 0) {
				p1_EndlessLevel = 1;
			}

			p1_Score_Curr = 0;
			p1_ComboMultiplier = 1;
		}

		// Base / Zero value for last stand shout count
//		p1_LastStandShoutCount = 0;

		// Setup HUD (Old place)
//		SetupHUD ();
	}

	void SetupHUD () {
		if (LoadFromMenu) {
			p1_HP_Curr = p1_HP_Start;
		}
		StartCoroutine (hudScriptHolder.HUD_Update_Health (p1_HP_Curr));
		StartCoroutine (hudScriptHolder.HUD_Update_Mustache ());
	}

	void Player_ItemsEffects_Implement () {
//		Debug.LogError ("Pre Pre Effects");

		itemEffectsScriptHolder.ItemEffecst_PreLoad ();
		itemEffectsScriptHolder.ItemsEffects_Check ();

//		Debug.LogError ("Pre Effects");
		p1_LuckyExplodeChance = itemEffectsScriptHolder.itemEffect_LuckyExplodeChance;
		p1_ItemValueIncrease = itemEffectsScriptHolder.itemEffect_ItemValueIncrease;
		p1_isAntiHF_Sometimes = itemEffectsScriptHolder.itemEffect_isAntiHF_Sometimes;
		p1_isAntiHedgehog_Sometimes = itemEffectsScriptHolder.itemEffect_isAntiHedgehog_Sometimes;
		p1_HP_Start = itemEffectsScriptHolder.itemEffect_HP_Start;
		p1_HP_Curr = itemEffectsScriptHolder.itemEffect_HP_Curr;
		p1_CritChance = itemEffectsScriptHolder.itemEffect_CritChance;
		p1_ShieldChanceTotal = itemEffectsScriptHolder.itemEffect_ShieldChanceTotal;
		p1_KIllBonus_HealAmount = itemEffectsScriptHolder.itemEffect_KIllBonus_HealAmount;
		p1_MustacheMultiplier_Cur = itemEffectsScriptHolder.itemEffect_MustacheMultiplier_Cur;
		p1_Damage_Base = itemEffectsScriptHolder.itemEffect_Damage_Base;
		p1_LastStandShoutCount = itemEffectsScriptHolder.itemEffect_LastStandShoutCount;
		p1_PowUp2Duration = itemEffectsScriptHolder.itemEffect_PowUp2Duration;

//		Debug.LogError ("Post Effects!");
		AfterCount_ItemsEffects ();
	}

//	void ImpEffefct () {
//		itemEffectsScriptHolder.ItemsEffects_Check ();
//
//		AfterCount_ItemsEffects ();
//	}

	public void AfterCount_ItemsEffects () {
		if (p1_KIllBonus_HealAmount > 0) {
			p1_HasKillBonus = true;
		}
	}

	public void HP_SortingOrderTo5 () {
		hudScriptHolder.HP_LayerSort_To5 ();
	}

	public void HP_SortingOrderTo2 () {
		hudScriptHolder.HP_LayerSort_To2 ();
	}

	public void Mustache_SortingOrderTo (int whichOrder) {
		hudScriptHolder.Mustache_LayerSort_To (whichOrder);
	}

	public void FirstTimePlay_IntroAnimateTutparts_1 () {
		StartCoroutine (hudScriptHolder.HUD_FirstTimePlay_TutPartsAnimate_1 ());
	}

	public void FirstTimePlay_IntroAnimateTutparts_2and3 (int whichAnim) {
		StartCoroutine (hudScriptHolder.HUD_FirstTimePlay_TutPartsAnimate_2and3 (whichAnim));
	}

	public void FirstTimePlay_StopTutparts_NoLeave () {
		StartCoroutine (hudScriptHolder.HUD_FirstTimePlay_StopTutPart_NoLeave ());
	}

	public void FirstTimePlay_StopTutparts_YesLeave () {
		StartCoroutine (hudScriptHolder.HUD_FirstTimePlay_StopTutPart_YesLeave ());
	}

	public void FirstTimePlay_StopTutparts_EndAll () {
		StartCoroutine (hudScriptHolder.HUD_FirstTimePlay_EndAll ());
	}

	// For start of game over during endless
	public IEnumerator HUD_EnemyParent_MoveIn () {
		yield return null;
		transEnemiesSpawnParentRef = endLevelScriptHolder.endlessSpawnerScriptHolder.enemysParentTransHolder;

		yield return new WaitForSeconds (1);
		transEnemiesSpawnParentRef.localPosition = Vector2.zero;
	}

	// For game over to continue during endless
	public IEnumerator HUD_EnemyParent_MoveOut () {
		yield return null;
		transEnemiesSpawnParentRef = endLevelScriptHolder.endlessSpawnerScriptHolder.enemysParentTransHolder;

		yield return new WaitForSeconds (2.5F);
		transEnemiesSpawnParentRef.localPosition = new Vector2 (140, 0);
	}

	// REF: Intro Anim Delayer
	public void Red_HUDIntroDelay () {
		hudScriptHolder.IntroAnim_Delayer ();
	}

	// REF: Spawn Delay
	public void Ref_SpawnerSetup () {
		if (PlayerData_Main.Instance.gameMainSpawnType == GameSpawnType.Campaign) {
			endLevelScriptHolder.enemySpawnerScriptHolder.SpawnsSetup ();
		} else {
			p1_Score_Curr = 0;
			endLevelScriptHolder.endlessSpawnerScriptHolder.SpawnsSetup ();
		}
	}

	// REF: Anim Only
	public void Ref_PlayerHurtAnim () {
		hudScriptHolder.camScriptHolder.PlayAnim_PlayerHurt ();
	}


	public IEnumerator PlayerHurt (int hurtAmount, bool isExplodeHurt) {
		// Increase shields counter
		p1_TotalHurts++;

        // SOUNDD EFFECT - player take hit (done)

        if (SoundManager.Instance != null)
        {
            switch (Random.Range(1, 6))
            {
                case 1:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 0.8f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
                case 2:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 0.9f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
                case 3:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 1f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
                case 4:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 1.1f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
                case 5:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 1.2f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
                default:
                    SoundManager.Instance.MoshtMikhorim1.pitch = 0.9f;
                    SoundManager.Instance.MoshtMikhorim1.Play();
                    break;
            }
        }

		// Didn't die
		if (p1_HP_Curr > hurtAmount) {

			p1_HP_Curr = p1_HP_Curr - hurtAmount;

			// Play Camera Shake (Hurt)
			if (!isExplodeHurt) {
				hudScriptHolder.PlayerCam_HurtDiedShield (1);
			} else {
				hudScriptHolder.PlayerCam_HurtDiedShield (4);
			}
		    // Disregard
//			camPlayerAnimHolder.Play ();
//			fullScreenAnimHolder.Play ();

			// Check for last health
			if (p1_HP_Curr == 1) {
				Player_LastHealth ();
			}

			// Combo multiplier / main decrease by hurt
			if (gameSpawnerType == GameSpawnType.Endless) {
				StartCoroutine (PlayerCombo_Loss (false));
			}
		} 

		// Died!
		else {
			// Falsify untouchable / no damage for game over
			stats_Stars_Untouchable = false;

			// Activate invinciblity RIGHT HERE (In case it was last stand);
			PlayerResurrectInvinciblity_Start ();

			// Check for last stand
			if (p1_LastStandShoutCount == 0) {
				p1_isDead = true;
				hudScriptHolder.PlayerCam_HurtDiedShield (2);
				Player_Died ();

				// SOUNDD EFFECTS - Player Die Animation Sounds (Breaking and such) (done)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.GameOverSound.Play ();
				}

				// Combo multiplier / main decrease by death
				if (gameSpawnerType == GameSpawnType.Endless) {
					StartCoroutine (PlayerCombo_Loss (true));
				}
			}

			// Has at least one last stand
			else {
				p1_HP_Curr = 1;
				p1_LastStandShoutCount--;
				hudScriptHolder.HUD_LastStandShout ();

                // SOUNDD EFFECTS - Player Last Stand Shout (done)
                if (SoundManager.Instance!=null)
                {
                    SoundManager.Instance.LastStandBruceLee.Play();
                }

				// Item active for last stand
				ItemActiveFeed_Show ("!ﺎﻫﺩﮊﺍ ﻢﺸﺧ");
			}

		}
		StartCoroutine (hudScriptHolder.HUD_Update_Health (p1_HP_Curr));

		// For feedback test only. FOR DELETE
//		FindObjectOfType<EnemyMaker_Test>().UpdateNumber_Hurts();

		yield return null;
	}

	public void ItemActiveFeed_Show (string itemActName) {
		StartCoroutine (itemActivFeedScriptHolder.ItemActivateFeed_Routine (itemActName));
	}

	public IEnumerator PlayerHurt_Shielded () {
		// Play Camera Shake (Shielded)
		hudScriptHolder.PlayerCam_HurtDiedShield(3);

		// Increase shields counter
		p1_TotalShields++;

		// Quest for shield
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_MissShiled ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_DoShieldMiss_100 ());
		}

		yield return null;
	}

	public IEnumerator PlayerHeal_Normal (int healAmount) {
//		Debug.LogError ("Heal! 11");

		// Didn't reach max health
//		if (p1_HP_Start > (p1_HP_Curr + healAmount)) {
//			p1_HP_Curr = p1_HP_Curr + healAmount;
//			Debug.LogError ("Heal! 22");
//		} 
//
//		// Above max health
//		else {
//			p1_HP_Curr = p1_HP_Start;
//			Debug.LogError ("Heal! 33");
//		}
		p1_HP_Curr = p1_HP_Curr + healAmount;

		StartCoroutine (hudScriptHolder.HUD_Update_Health (p1_HP_Curr));

		// Heal particles
		hudScriptHolder.HUD_Healed_Normal ();

		yield return null;
	}

	public IEnumerator PlayerHeal_Full (int healAmount) {
//		Debug.LogError ("Heal! 1");

		p1_HP_Curr = p1_HP_Curr + healAmount;

		// Didn't reach max health
//		if (p1_HP_Start > (p1_HP_Curr + healAmount)) {
//			p1_HP_Curr = p1_HP_Curr + healAmount;
//			Debug.LogError ("Heal! 2");
//		} 
//
//		// Above max health
//		else {
//			p1_HP_Curr = p1_HP_Start;
//			Debug.LogError ("Heal! 3");
//		}

		StartCoroutine (hudScriptHolder.HUD_Update_Health (p1_HP_Curr));

		// Heal particles

		yield return null;
	}

	public void Pressed_PlayerHeal_BonusKill (int whichHealBonus) {
//		StartCoroutine (PlayerHeal_BonusKill (whichHealBonus));

		// For bonus heal based on item values
		switch (whichHealBonus) {
		case 1:
			StartCoroutine (PlayerHeal_BonusKill (Item_Values.Instance.ItemValue_Health_1));
			break;
		case 2:
			StartCoroutine (PlayerHeal_BonusKill (Item_Values.Instance.ItemValue_Health_2));
			break;
		case 3:
			StartCoroutine (PlayerHeal_BonusKill (Item_Values.Instance.ItemValue_Health_3));
			break;
		case 4:
			StartCoroutine (PlayerHeal_BonusKill (Item_Values.Instance.ItemValue_Health_4));
			break;
		default:
			StartCoroutine (PlayerHeal_BonusKill (Item_Values.Instance.ItemValue_Health_1));
			break;
		}

		ItemActiveFeed_Show ("!ﺩﺯﺩ ﺐﻠﻗ");
	}

	public IEnumerator PlayerHeal_BonusKill (int whichHealAmount) {
		itemGainScriptArr [6].ItemGain_StartMove (6, whichHealAmount);
		yield return null;
	}

	public void Player_LastHealth () {
//		Debug.LogError ("Danger!!");
	}

	public void Player_LastStandShout () {
//		Debug.LogError ("Last Stand Shout!!");
	}

	public void Player_Died () {
		// Set layer of mustache
		Mustache_SortingOrderTo (5);

		p1_HP_Curr = 0;
		StartCoroutine (hudScriptHolder.HUD_Update_Health (p1_HP_Curr));

		// Pause for game over
		EventTrigger.PauseTheGame ();

		// Player Died HUD
		hudScriptHolder.PlayerDied ();

		// Player Died Cleanup!
		PlayerHUD_HF_ClingyFront_KillAll ();
//		PlayerItem_Elixir_Deactivate ();
//		PlayerItem_Pipe_Deactivate ();

		// In case player had pipe during this
		if (PlayerData_InGame.Instance.p1_Item_PipeIsActive) {
			PlayerData_InGame.Instance.PlayerItem_ItemDurabilityEnd (0);
		}

		// Enable Shop Button For When Player Died
		hudScriptHolder.HUD_DeathMustache_Button (true);

		// Sync both mustaches
		Mustache_Sync_Script.Mustache_SyncNow (PlayerData_InGame.Instance.p1_Moustache);

		// Save for death
		SaveLoad.Save ();

		// If player died in endless, we need to move enemytransparent
		if (gameSpawnerType == GameSpawnType.Endless) {
			StartCoroutine (HUD_EnemyParent_MoveOut ());
		}

		// Pass the values from play-ingame to play-main
//		PassValues_InGame_To_Main ();
	}

	public void Player_Resurrected () {
		p1_isDead = false;
		StartCoroutine(PlayerMustache_Gain (-continueCost));

		// Sync failed here but other places (During "Death moment" and "Shop Collector Gain Success" still uses sync)

		// Update Playdata_main Mustache as well
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.Player_Mustache_Add (-continueCost);
		}

		// Disable Shop Button For When Player Returns
		hudScriptHolder.HUD_DeathMustache_Button (false);

		// Recharge Pow Ups after Continue Resurrection
		ContinuePowUp_FreeRecahrge ();

		// Save for resurrected
		if (PlayerData_Main.Instance != null) {
			SaveLoad.Save ();
		}

		StartCoroutine (PlayerHeal_Full (p1_HP_Start));
		hudScriptHolder.PlayerResurrected ();

		// Increase price of continue
		ContinueCost_Increase ();

		// Increase continue counter
		stats_ContinuesUsed++;

		// If player died in endless, we need to move enemytransparent
		if (gameSpawnerType == GameSpawnType.Endless) {
			StartCoroutine (HUD_EnemyParent_MoveIn ());
		}
	}

	public void PlayerResurrectInvinciblity_Start () {
		intShieldChanceTotalBuffer = p1_ShieldChanceTotal;
		floatShieldTimeCounter = 0;

		p1_ShieldChanceTotal = 100;

		StartCoroutine (PlayerResurrectInvinciblity_OnGoing ());
	}

	public IEnumerator PlayerResurrectInvinciblity_OnGoing () {
		while (EventTrigger.isFrozen || EventTrigger.isPaused) {
			yield return null;
		}

		yield return new WaitForSeconds (0.1F);
		floatShieldTimeCounter += 0.1F;

		if (floatShieldTimeCounter < 3) {
//			Debug.LogError ("Resurrect shield ONGOING");
			StartCoroutine (PlayerResurrectInvinciblity_OnGoing ());
		} else {
//			Debug.LogError ("Resurrect shield END");
			PlayerResurrectInvinciblity_End ();
		}
	}

	public void PlayerResurrectInvinciblity_End () {
		p1_ShieldChanceTotal = intShieldChanceTotalBuffer;
	}

	public void ContinuePowUp_FreeRecahrge () {
		// Increase PowUp1 Count
		if (p1_PowUp1_Count_Ready < p1_PowUp1_Count_Own) {
			p1_PowUp1_Count_Ready++;
		}

		// Increase PowUp1 Count
		if (p1_PowUp2_Count_Ready < p1_PowUp2_Count_Own) {
			p1_PowUp2_Count_Ready++;
		}
			
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.player_PowUp1_Ready = p1_PowUp1_Count_Ready;
			PlayerData_Main.Instance.player_PowUp2_Ready = p1_PowUp2_Count_Ready;
		}

		// Setup Visual Pow Ups Again
		hudScriptHolder.HUD_PowUpSlotsUpdate ();
	}

	public void ContinueCost_Increase () {
		continueCost = continueCost * 2;
	}

	public IEnumerator PlayerMustache_Gain (int mustacheAmount) {
		yield return null;
		p1_Moustache += mustacheAmount;

		StartCoroutine (hudScriptHolder.HUD_Update_Mustache ());

		// Check if mustache gain is positive
		if (mustacheAmount > 0) {
			hudScriptHolder.HUD_AnimateMustache_Gain ();
//			Debug.LogWarning ("Gain");
		} 

		// Achievment for holding onto too much money
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_Mustache_Hoarder_Set (p1_Moustache));

			if (mustacheAmount < 0) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_Mustache_Spender_AddAmount (mustacheAmount));
			}
		}
	}

	public IEnumerator PlayerCombo_Gain () {
		yield return null;
		StartCoroutine (hudScriptHolder.HUD_Update_Combo_AnyGain ());
	}

	public IEnumerator PlayerCombo_KillReward () {
		yield return null;
		StartCoroutine (hudScriptHolder.HUD_Update_Combo_KillReward ());
	}

	public IEnumerator PlayerCombo_Reset () {
		yield return null;
		StartCoroutine (hudScriptHolder.HUD_Update_Combo_MiniLoss ());
	}

	public IEnumerator PlayerCombo_Loss (bool isDeath) {
		yield return null;
		StartCoroutine (hudScriptHolder.HUD_Update_Combo_MainLoss (isDeath));
	}

	public IEnumerator PlayerScore_Gain (int scoreAmount) {
		yield return null;
		p1_Score_Curr += scoreAmount;

		StartCoroutine (hudScriptHolder.HUD_Update_Score ());
	}
		
	public void Player_Victory_HUD (int whichVictoryHUD) {
		hudScriptHolder.HUD_Victory_Play (whichVictoryHUD);
	}

	public void Player_Victory_PowUp2End () {
		hudScriptHolder.HUD_Victory_PowUp2EndDur ();
	}

	public void Player_Victory_MustacheLateRemove () {
		hudScriptHolder.HUD_Victory_MustacheLateRemove ();
	}

	public void Player_EndlessEnd_MustacheLateRemove () {
		hudScriptHolder.HUD_EndlessEnd_MustacheLateRemove ();
	}

	public void Player_CleanScreen_Start () {
		hudScriptHolder.HUD_CleanScreen_Start ();
	}

	public void Player_CleanScreen_End (int whichEnd) {
		hudScriptHolder.HUD_CleanScreen_End (whichEnd);
	}

	public void PlayMusic_World () {
		// Find music source
		if (MusicManager.Instance != null) {
			MusicManager.Instance.PlayMusic_WorldCinema ();
//            MusicManager.Instance.PlayMusic_MoonlightSonata();
		}
	}

	public void CalculateCritical () {
		// Critical Attack Happened
		if (p1_CritChance > Random.Range (0, 100)) {
			critHappened = true;
		}

		// Normal Attack Happened
		else {
			critHappened = false;
		}
	}

	public void Pressed_Continue () {
		if (p1_Moustache > continueCost) {
			Player_Resurrected ();
			hudScriptHolder.PressedFOR_ContinueButton ();

            // SOUNDD EFFECTS - Continue Successfully Pressed / Resurrection (done)
			if (PlayerData_Main.Instance != null) {
				switch (PlayerData_Main.Instance.WorldNumber) {
				case 1:
					if (MusicManager.Instance != null) {
						MusicManager.Instance.PlayMusic_WorldCinema ();
					}
					break;
				case 2:
					if (MusicManager.Instance != null) {
						MusicManager.Instance.PlayMusic_WorldGarage ();
					}
					break;
				case 3:
					if (MusicManager.Instance != null) {
						MusicManager.Instance.PlayMusic_WorldGrandHotel ();
					}
					break;
				default:
					if (MusicManager.Instance != null) {
						MusicManager.Instance.PlayMusic_WorldGarage ();
					}
					break;
				}
			}


		} else {
			hudScriptHolder.PlayerCantResurrected ();
		}
	}

	public void CriticalScreenEffect () {
		if (!critDuring) {
//			hudScriptHolder.cri
		}
	}

	public void ExplosionDamage_Set (int newExplodeDamage) {
//		Debug.LogWarning ("Explosion damage BEFORE = " + explosionDamage);
//		explosionDamage = newExplodeDamage;
//		Debug.LogWarning ("Explosion damage AFTER = " + explosionDamage);
	}

	public void ExplosionDamage_Reset () {
//		explosionDamage = 0;
	}

	public void PlayerItem_ItemDurabilityDecrease (int whichItem) {
		// Previously, there was no which item and we had decrease 0 and 2
		itemPickupScriptArr [whichItem].ItemCountDown_DurabilityDecrease ();
	}

	public void PlayerItem_ItemDurabilityEnd (int whichItem) {
		StartCoroutine (itemPickupScriptArr [whichItem].ItemCountDown_DurabilityEND ());
	}

	public void PlayerItem_ActivateGeneral (int whichItem, float length) {
		itemPickupScriptArr [whichItem].gameObject.SetActive (true);
		itemPickupScriptArr [whichItem].ItemCountdown_Start (whichItem, length);
	}

	public void PlayerItem_ActivateConsume (int whichItem, int howMuchItem) {
		itemGainScriptArr [whichItem].ItemGain_StartMove (whichItem, howMuchItem);
	}

	public void PlayerItem_Pipe_Activate () {
		p1_Item_PipeIsActive = true;
		hudScriptHolder.HUD_PipeParticles_Activate ();
		p1_Damage_Base = in_Damage_BaseOrig * 2;

		PipeItemGained_Bool = true;
		PipeItemUsed_Resetter = 0;

		// SOUND EFFECTS - Pipe Start (Empty)
	}

	public void PlayerItem_Pipe_Deactivate () {
		p1_Item_PipeIsActive = false;
		hudScriptHolder.HUD_PipeParticles_Deactivate ();
		p1_Damage_Base = in_Damage_BaseOrig;
	}

	public void PlayerItem_Elixir_Activate () {
		EventTrigger.HappyTime_Do ();
		if (!p1_Item_ArrowHappyTime) {
			EventTrigger.HappyTime_Do ();
			PlayerHUD_ElixirParticles_Start (p1_FreezeDirectionInt);
		} else {
			EventTrigger.HappyTime_Undo ();
			EventTrigger.HappyTime_Do ();
			PlayerHUD_ElixirParticles_Start (p1_FreezeDirectionInt);
		}

		// SOUNDD EFFECTS - Elixir / Magnet Start (done)
        if (SoundManager.Instance != null)
        { 
            SoundManager.Instance.AhanrobaContinous.Play();
        }
    }

	public void PlayerItem_Elixir_Deactivate () {
		EventTrigger.HappyTime_Undo ();
		PlayerHUD_ElixirParticles_End (p1_FreezeDirectionInt);
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.AhanrobaContinous.Stop();
        }
    }

	public void PlayerItem_Freeze_Activate () {
		timeFreezeStart = System.DateTime.Now;

		// HUD Freeze stuff start
		hudScriptHolder.FreezeShutter_StartHUD ();
		PlayerHUD_ShutterAnim_1 ();
		hudScriptHolder.Cam_SepiaTone_Do ();

		// Call Film Stuff
		hudScriptHolder.FreezeFilm_Start ((int)itemPickupScriptArr[2].itemDurability);
//		PlayerItem_Freeze_FilmStart ();

		EventTrigger.FreezeTheGame ();

//		Debug.LogWarning ("1 Was INVINCIBLE here?");

		// To start shielding The Player
		p1_InvincibleBuffer = p1_Invincible;
		p1_Invincible = true;

//		Debug.LogWarning ("2 Was INVINCIBLE here?");

		// SOUND EFFECTS - Freeze Start (Empty)
	}
		
	// Is not used for the main freeze by items
	public void PlayerItem_Freeze_FilmStart () {
		hudScriptHolder.FreezeFilm_Start ((int)itemPickupScriptArr[2].itemDurability);
		hudScriptHolder.FreezeFromGame_Start ();
	}

	public void PlayerItem_Freeze_FilmDecrease () {
		hudScriptHolder.FreezeFilm_Decrease ();
	}

	// Is USED for the end of freeze by item
	public void PlayerItem_Freeze_FilmEnd () {
		hudScriptHolder.FreezeFilm_End ();
		hudScriptHolder.FreezeFromGame_End ();

		System.TimeSpan timeDiff = System.DateTime.Now - timeFreezeStart;
//		Debug.LogError ("TIME SPENT = " + timeDiff.Seconds);

		if ((timeDiff.Seconds >= 20) && true) {
			if (AchievementManager.Instance != null) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_PhotographStare ());
			}
		}
	}

	public void PlayerItem_Freeze_Deactivate () {
//		Debug.LogWarning ("Want this!?");

		// HUD freeze stuff end
		hudScriptHolder.FreezeShutter_EndHUD_Delayer ();
		hudScriptHolder.Cam_SepiaTone_UnDo ();

//		Debug.LogWarning ("3 Was INVINCIBLE here?");

		PlayerItem_Freeze_FilmEnd ();
		EventTrigger.UnFreezeTheGame ();

//		Debug.LogWarning ("4 Was INVINCIBLE here?");

		// To stop shielding The Player
		p1_Invincible = p1_InvincibleBuffer;
		p1_Invincible = false;
	}

	public void PlayerItem_Freeze_END_Deactivate () {
		if (itemPickupScriptArr [2].itemDurability > 1) {
			itemPickupScriptArr [2].itemDurability = 1;
		}
		itemPickupScriptArr [2].ItemCountDown_DurabilityDecrease ();

		hudScriptHolder.Cam_SepiaTone_UnDo ();

		EventTrigger.UnFreezeTheGame ();
	}

	public void PlayerHUD_ElixirParticles_Start (int whatNumber) {
		hudScriptHolder.ElixirParticle_Start (whatNumber);
	}

	public void PlayerHUD_ElixirParticles_End (int whatNumber) {
		hudScriptHolder.ElixirParticle_End (whatNumber);
	}

	public void PlayerHUD_ShutterAnim_1 () {
		hudScriptHolder.FreezeShutter_EyesAnimate_1 ();
	}

	public void PlayerHUD_ShutterAnim_2 () {
		hudScriptHolder.FreezeShutter_EyesAnimate_2 ();
	}

	public void PlayerHUD_HF_ClingyFront_Start (int whichLine) {
		P1_HF_ClingyInTheFrontBool = true;
		if (P1_HF_ClingyInTheFrontCount < 5) {
			P1_HF_CountTotal++;
			P1_HF_ClingyInTheFrontCount++;
//			Debug.LogError ("HF Clingy Count = " + P1_HF_ClingyInTheFrontCount);
			hudScriptHolder.HF_Front_CallRandom (0);
			hudScriptHolder.HF_Front_PlayThis ("HUD (Top) HF Front START Anim " + whichLine.ToString () + " (Legacy)", true);
		}
	}

	public void PlayerHUD_HF_ClingyFront_Hit (int hitAmount) {
		hudScriptHolder.HF_Front_Hit (hitAmount);
	}

	public void PlayerHUD_HF_ClingyFront_KillAll () {
		hudScriptHolder.HF_Front_KillAll ();
	}

	public void PlayerItem_Heart_GainNow (int howMuchHealth) {
		StartCoroutine (PlayerHeal_Normal (howMuchHealth));

        // SOUNDD EFFECTS - Heart Gain Start (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.Ghalb.Play();
        }

	}

	public void PlayerItem_Hedgehog_GainNow (int howMuchHurt) {
		// For hedgehog PICKED achievement
		PlayerData_InGame.Instance.HedgehogItemCounter_Picked++;

//		StartCoroutine (PlayerData_InGame.Instance.PlayerHurt (howMuchHurt));
		if (p1_Invincible
		    || (p1_ShieldChanceTotal >= Random.Range (0, 100))) {

			StartCoroutine (PlayerHurt (0, false));
		}
		// Player's shield chance was lower
		else {
			StartCoroutine (PlayerHurt (howMuchHurt, false));

			// Player was hit AND hurt
//			if (stats_Stars_Untouchable) {
//				stats_Stars_Untouchable = false;
//			}

            // SOUNDD EFFECTS - Hedgehog Hurt Start (done)
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Tigh.Play();
            }
        }
	}

	public void PlayerItem_Mustache_GainNow (int howMuchMustache) {
		StartCoroutine (PlayerMustache_Gain (howMuchMustache));
	}

	public void PlayerItem_BigBomb_GainNow (int howManyBombs) {
		StartCoroutine (BigBombLevels (howManyBombs));
	}

	public void PlayerItem_PowUpGain_Fist () {
		if (p1_PowUp1_Count_Ready < p1_PowUp1_Count_Own) {
			p1_PowUp1_Count_Ready++;
		}

		// Update HUD display for both pow ups
		StartCoroutine (hudScriptHolder.HUD_PowUpRecharge_1 ());
	}

	public void PlayerItem_PowUpGain_Shield () {
		if (p1_PowUp2_Count_Ready < p1_PowUp2_Count_Own) {
			p1_PowUp2_Count_Ready++;
		}

		// Update HUD display for both pow ups
		StartCoroutine (hudScriptHolder.HUD_PowUpRecharge_2 ());
	}

	IEnumerator BigBombLevels (int howManyBombs) {
		MiniExplosion ();
		while (howManyBombs > 0) {
			EventManager.TriggerEvent ("Explode N Stuff");
			howManyBombs--;
			yield return null;
		}
	}

	public void Player_EnemyKill_Increase () {
		stats_EnemiesKilled++;

		// Check to see if bonus kill bonus is even available
		if (p1_HasKillBonus) {
			p1_KIllBonus_EnemyCount++;

			// Check to see if bonus kill number was reached or not
			if (p1_KIllBonus_EnemyCount == p1_KIllBonus_EnemyRequirement) {
				p1_KIllBonus_EnemyCount = 0;
				Pressed_PlayerHeal_BonusKill (p1_KIllBonus_HealAmount);

				// Heart stealer / Kill Bonus
				if (AchievementManager.Instance != null) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_DoHeartSteal_100 ());
				}
			}
		}
	}

	public void OverallExplosion () {
		hudScriptHolder.HUD_ExplosionWhite ();
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.Enfejar1.Play();
        }
    }

	public void MiniExplosion () {
		hudScriptHolder.HUD_ExplosionMiniWhite ();
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.Enfejar2.Play();
        }
    }

	public void PlayerSpeed_SloMo_Start (float slowSpeed) {
		Time.timeScale = slowSpeed;
		Invoke ("PlayerSpeed_SloMo_End", slowSpeed / 1.5F);
	}

	public void PlayerSpeed_SloMo_End () {
		Time.timeScale = 1;
	}

	public void Player_CameraIdle () {
		hudScriptHolder.Cam_PlayIdleAnim ();
	}

	public void Player_ForceLastEnemyCheck_LATE () {
		Invoke ("Player_ForceLastEnemyCheck_NOW", 2F);
	}

	public void Player_ForceLastEnemyCheck_NOW () {
		if (lastEnemyWasSpawned) {
			StartCoroutine (Player_CheckLastEnemy ());
		}
	}

	public IEnumerator Player_CheckLastEnemy () {
//		Debug.LogWarning ("END Player Check");
		if (gameSpawnerType == GameSpawnType.Campaign) {
			endLevelScriptHolder.EndCheck_EachKill ();
		} else {
			endLevelScriptHolder.EndlessCheck_EachKill ();
		}
		yield return null;
	}

	public bool Player_CamCheckLastEnemy () {
		//		Debug.LogWarning ("END Player Check");
		return (endLevelScriptHolder.CameraEndCheck_EachKill ());
	}

	public void Player_EndGoSlowMo () {
		hudScriptHolder.GoSloMo_Start ();
//		hudScriptHolder.GoMoshtanLogo_End ();
	}

//	private int screenshotCounter = 0;
//
//	void Update () {
//		if (Input.GetKeyDown ("m")) {
//			Application.CaptureScreenshot("Screenshot " + screenshotCounter.ToString() + ".png");
//		}
//	}



}
