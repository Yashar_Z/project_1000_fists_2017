﻿using UnityEngine;
using System.Collections;

public class PowerUp1_HUD_Script : MonoBehaviour {

	public PowerUps_Script powUpsMainScriptHolder;

	public void PowUp1_Pressed () {
        // SOUNDD EFFECTS - start of Punch Power Up Pressed (done)
        if (SoundManager.Instance!= null)
        {
            if (MusicManager.Instance!=null)
            {
                StartCoroutine( MusicManager.Instance.MusicVolume_Lower());
                //MusicManager.Instance.PauseMusic();
            }
            SoundManager.Instance.FistPowerupClicked.Play();
        }

		PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_KillAll ();

		// Increase count for achievement
		PlayerData_InGame.Instance.p1_PowUp1_OnlyUsed++;

		PowUpBOTH_Logic_Screen_Start ();
		PowUpBOTH_Anim_GradsParticlesRipples ();
		PowUp1_Logic_ParticlesCount_Decrease ();
		PowUpBOTH_Logic_ParticlesStopAll ();
		PowUp1_Logic_Invincible_Start ();
		PowUp1_Anim_FistEnter ();

		StartCoroutine (PowUpBOTH_Logic_Update ());
//		Invoke ("PowUpBOTH_Logic_Update", 0.1F);

		// Quest stuff
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_PowerUpUse_Any ());
			StartCoroutine (QuestManager.Instance.QuestProg_PowerUpUse_PowUp1 ());
		}

		// To avoid the trail bug
		powUpsMainScriptHolder.ingameControllerHolder.Trail_TurnOff ();
	}

	public void PowUp1_Pressed_FrontOverButton () {
		// In case player is out of pow ups
		if (PlayerData_InGame.Instance.p1_PowUp1_Count_Ready == 0) {
			PlayerData_InGame.Instance.p1_PowUp1_Count_Ready = 1;
		}

		EventTrigger.UnPauseTheGame ();
		PowUp1_Pressed ();
	}

	public void PowUpBOTH_Anim_GradsParticlesRipples () {
		powUpsMainScriptHolder.PowerUpBOTH_GradsNParticlesNRipples (1);
	}

	public IEnumerator PowUpBOTH_Logic_Update () {
		yield return new WaitForSeconds (0.1F);
		powUpsMainScriptHolder.PowerUpBoth_Update ();
	}

	public void PowUpBOTH_Logic_Screen_Start () {
		powUpsMainScriptHolder.PowerUpBoth_Screen_Start ();
	}

	public void PowUpBOTH_Logic_Screen_End () {
        // SOUNDD EFFECTS - end of Punch Power Up Pressed (done)
        if (SoundManager.Instance != null)
        {
            if (MusicManager.Instance != null)
            {
                StartCoroutine( MusicManager.Instance.MusicVolume_Higher());
                //MusicManager.Instance.UnPauseMusic();
            }
            SoundManager.Instance.FistPowerupClicked.Stop();
        }
        powUpsMainScriptHolder.PowerUpBoth_Screen_End ();
	}

	public void PowUpBOTH_Logic_ParticlesStopAll () {
		powUpsMainScriptHolder.PowerUpBOTH_ParticleStopAll ();
	}

	public void PowUpBOTH_Logic_ReturnHUD (int whichEnd) {
		powUpsMainScriptHolder.PowerUpBoth_ReturnHUD (whichEnd);
	}

	public void PowUp1_Logic_ParticlesCount_Decrease () {
		powUpsMainScriptHolder.PowerUp1_ParticlesStop_Decrease ();
	}

	public void PowUp1_Anim_FistEnter () {
		powUpsMainScriptHolder.PowerUp1_FistEnter ();
	}

	public void PowUp1_Anim_DamageNow () {
		powUpsMainScriptHolder.PowerUp1_DamageNow ();
	}

	public void PowUp1_Logic_Invincible_Start () {
		powUpsMainScriptHolder.PowerUp1_Invincible_Start ();
	}

	public void PowUp1_Logic_Invincible_End () {
		powUpsMainScriptHolder.PowerUp1_Invincible_End ();
	}

}
