﻿using UnityEngine;
using System.Collections;

public class PowerUp2_HUD_Script : MonoBehaviour {

	public PowerUps_Script powUpsMainScriptHolder;
	public ParticleSystem particlePowUp2_MainShieldHolder;
//	public ParticleSystem[] particlePowUp2_MovingShieldsArr;

	public void PowUp2_Pressed () {
		if (!PlayerData_InGame.Instance.p1_PowUp2_Active) {
			// Stuff about Pow Up 2 Being Active
			PlayerData_InGame.Instance.p1_PowUp2_Active = true;
			powUpsMainScriptHolder.powUp2UsedDisableArtObjHolder.SetActive (true);

			// Check to see if player will have zero pow up 2s after activating / using to move the red used circle accordingly
			if (PlayerData_InGame.Instance.p1_PowUp2_Count_Ready == 1) {
				powUpsMainScriptHolder.powUp2UsedDisableArtObjHolder.transform.localPosition = new Vector2 (0, -10.5F);
			} else {
				powUpsMainScriptHolder.powUp2UsedDisableArtObjHolder.transform.localPosition = Vector2.zero;
			}

			// SOUNDD EFFECTS - Shield Power Up CLICKED (done)
			if (SoundManager.Instance != null) {
				SoundManager.Instance.ShieldPowerupClicked.Play ();
				SoundManager.Instance.ShieldPowerupTime.PlayDelayed (1f);
			}

			PlayerData_InGame.Instance.PlayerHUD_HF_ClingyFront_KillAll ();

			PowUpBOTH_Logic_Screen_Start ();
			PowUpBOTH_Anim_GradsParticlesRipples ();
			PowUp2_Logic_ParticlesCount_Decrease ();
			PowUpBOTH_Logic_ParticlesStopAll ();
			PowUp2_Logic_Invincible_Start ();
			PowUp2_Anim_ShieldEnter ();

			StartCoroutine (PowUpBOTH_Logic_Update ());
//			Invoke ("PowUpBOTH_Logic_Update", 0.1F);

			// Quest stuff
			if (QuestManager.Instance != null) {
				StartCoroutine (QuestManager.Instance.QuestProg_PowerUpUse_Any ());
				StartCoroutine (QuestManager.Instance.QuestProg_PowerUpUse_PowUp2 ());
			}

			// To avoid the trail bug
			powUpsMainScriptHolder.ingameControllerHolder.Trail_TurnOff ();
		}

		// Stuff for already
		else {
		   Debug.LogWarning ("Already active!");
		}
	}

	public void PowUp2_Pressed_FrontOverButton () {
		// In case player is out of pow ups
		if (PlayerData_InGame.Instance.p1_PowUp2_Count_Ready == 0) {
			PlayerData_InGame.Instance.p1_PowUp2_Count_Ready = 1;
		}

		EventTrigger.UnPauseTheGame ();
		PowUp2_Pressed ();
	}

	public void PowUpBOTH_Anim_GradsParticlesRipples () {
		powUpsMainScriptHolder.PowerUpBOTH_GradsNParticlesNRipples (2);
	}

	public IEnumerator PowUpBOTH_Logic_Update () {
		yield return new WaitForSeconds (0.1F);
		powUpsMainScriptHolder.PowerUpBoth_Update ();
	}

	public void PowUpBOTH_Logic_Screen_Start () {
		powUpsMainScriptHolder.PowerUpBoth_Screen_Start ();
	}

	public void PowUpBOTH_Logic_Screen_End () {
		powUpsMainScriptHolder.PowerUpBoth_Screen_End ();
	}

	public void PowUpBOTH_Logic_ParticlesStopAll () {
		powUpsMainScriptHolder.PowerUpBOTH_ParticleStopAll ();
	}

	public void PowUpBOTH_Logic_ReturnHUD (int whichEnd) {
		powUpsMainScriptHolder.PowerUpBoth_ReturnHUD (whichEnd);
	}

	public void PowUp2_Logic_ParticlesCount_Decrease () {
		powUpsMainScriptHolder.PowerUp2_ParticlesStop_Decrease ();
	}

	public void PowUp2_Anim_ShieldEnter () {
		powUpsMainScriptHolder.PowerUp2_ShieldEnter ();
		PlayerData_InGame.Instance.p1_PowUp2_Active = true;
	}

	public void PowUp2_Anim_ShieldLoop () {
		powUpsMainScriptHolder.PowerUp2_ShieldLoop ();
	}

	public void PowUp2_Anim_PreMainShield_Particle_Play () {
//		particlePowUp2_MovingShieldsArr [2].Play ();
	}

	public void PowUp2_Anim_PreMainShield_Particle_Stop () {
//		particlePowUp2_MovingShieldsArr [2].Stop ();
	}

	public void PowUp2_Anim_MainShieldNow () {
		particlePowUp2_MainShieldHolder.Stop ();
		particlePowUp2_MainShieldHolder.Play ();
//		PowUp2_Anim_RippleNow ();
	}

	public void PowUp2_Anim_ShieldNow () {
		powUpsMainScriptHolder.PowerUp2_ShieldNow ();
	}

	public void PowUp2_Anim_ShieldEnd () {
		PowUp2_ShieldEndNow ();
		PowUp2_Logic_Invincible_End ();
		PowUp2_Anim_ParticleShield_Stop ();

		// For final version (This is for one time use only)
//		PlayerData_InGame.Instance.p1_PowUp2_Active = false;
//		powUpsMainScriptHolder.powUp2UsedDisableArtObjHolder.SetActive (false);
	}

	public void PowUp2_ShieldEndNow () {
		powUpsMainScriptHolder.PowerUp2_ShieldDurationEnd ();
	}

	public void PowUp2_Anim_RippleNow () {
		powUpsMainScriptHolder.PowerUp2_RippleFullEffect ();
	}

	public void PowUp2_Logic_Invincible_Start () {
		powUpsMainScriptHolder.PowerUp2_Invincible_Start ();
	}

	public void PowUp2_Logic_Invincible_End () {
		powUpsMainScriptHolder.PowerUp2_Invincible_End ();
	}

	public void PowUp2_Anim_ParticleShield_Play () {
		powUpsMainScriptHolder.PowerUp2_MovingParticleShield_Play ();
//		particlePowUp2_MovingShieldsArr [0].Play ();
//		particlePowUp2_MovingShieldsArr [1].Play ();
	}

	public void PowUp2_Anim_ParticleShield_Stop () {
		powUpsMainScriptHolder.PowerUp2_MovingParticleShield_Stop ();
//		particlePowUp2_MovingShieldsArr [0].Stop ();
//		particlePowUp2_MovingShieldsArr [1].Stop ();
	}

}
