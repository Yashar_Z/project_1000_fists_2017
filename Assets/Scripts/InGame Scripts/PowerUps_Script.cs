﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerUps_Script : MonoBehaviour {

	public InGame_Controller ingameControllerHolder;
	public Camera_InGame_Script camScriptHolder;
	public Image FullscreenClickBlockerHolder;
	public Image PowUpFillerImageHolder1;
	public Image PowUpFillerImageHolder2;
	public Button PowUp1_ButtonHolder;
	public Button PowUp2_ButtonHolder;

	public Sprite[] PowUp1_SpritesArr;
	public Sprite[] PowUp2_SpritesArr;

	public Animation worldGradsAnimHolder;
	public Animation worldGradsFRONTAnimHolder;

//	public Animation PowUpAnimHolder1;
//	public Animation PowUpAnimHolder2;

	public Animation PowUpHUDFistAnimHolder;
	public Animation PowUpHUDShieldAnimHolder;
	public Animation PowUpHUDShieldLOOPAnimHolder;
	public Animation powUpFirstAnimsHolder;

	public Animation animPU1Parent;
	public Animation animPU2Parent;

	public GameObject fromGameOverClickBlockerHolder;
	public GameObject powUp2UsedDisableArtObjHolder;

	public Transform EnemiesSpawnHereTransHolder;
	public Transform PowUpStacksParHolder1;
	public Transform PowUpStacksParHolder2;
	public Transform PowUpStacksFrontHolder1;
	public Transform PowUpStacksFrontHolder2;
	public Transform PowUpStacksHairHolder1;
	public Transform PowUpStacksHairHolder2;
//	public Transform PowUp1_PartsParentTransHolder;
//	public Transform PowUp2_PartsParentTransHolder;
	public Transform PowUp2_ButtonFront_ArtDisableOnlyHolder;
	public ParticleSystem PowUp1_MainReadyParticle;
	public ParticleSystem PowUp2_MainReadyParticle;
	public ParticleSystem[] PowUp1_ParticlesArr;
	public ParticleSystem[] particlePowUp2_MovingShieldsArr;

//	private ParticleSystem[] PowUp2_ParticlesArr;
	private bool powUpsCanRecharge;
//	private bool powUp1_Ready;
//	private bool powUp2_Ready;
	private bool powUp2_InvincibleActive;

	private float powerUp1_Full;
	private float powerUp2_Full;

//	private float powerUp1_AmountCurr;
//	private float powerUp2_AmountCurr;

	private float powerUp2_AnimDurationPauseTime;
	private float powerUp2_AnimLoopPauseTime;
	private float powerUp2_AnimDurationFreezeTime;
	private float powerUp2_AnimLoopFreezeTime;

	private int powerUp1_Count_Own;
	private int powerUp2_Count_Own;
	private int powerUp1_Count_Ready;
	private int powerUp2_Count_Ready;

	private int powerUp1_EnemyCountToKill;

	private bool canAddToAchieve;

	private EnemyHealth enemyHealthHolder;

	[SerializeField]
	private Image[] powerUp1_StacksArr;
	[SerializeField]
	private Image[] powerUp2_StacksArr;
//	[SerializeField]
//	private ParticleSystem[] PowUp1_StacksReadyParticlesArr;
//	[SerializeField]
//	private ParticleSystem[] PowUp2_StacksReadyParticlesArr;

	void OnDisable () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
	}

	// Use this for initialization
	void Start () {

		// Power Up InGame availablity Setup
//		powerUp1_StacksArr = new Image[5];
//		powerUp2_StacksArr = new Image[5];
//		for (int i = 0; i < PowUpStacksParHolder1.childCount; i++) {
//			powerUp1_StacksArr [i] = PowUpStacksParHolder1.GetChild (i).GetComponent<Image> ();
//		}
//
//		for (int i = 0; i < PowUpStacksParHolder2.childCount; i++) {
//			powerUp2_StacksArr [i] = PowUpStacksParHolder2.GetChild (i).GetComponent<Image> ();
//		}

//		powerUp1_StacksArr = PowUpStacksParHolder1.GetComponentsInChildren<Image>();
//		powerUp2_StacksArr = PowUpStacksParHolder2.GetComponentsInChildren<Image>();

//		PowUp1_StacksReadyParticlesArr = PowUpStacksParHolder1.GetComponentsInChildren<ParticleSystem>();
//		PowUp2_StacksReadyParticlesArr = PowUpStacksParHolder2.GetComponentsInChildren<ParticleSystem>();

		// Particles for the moment of impact (By 1000 punches)
//		PowUp1_ParticlesArr = PowUp1_PartsParentTransHolder.GetComponentsInChildren<ParticleSystem> ();

		// Power Up Recharge Setup
//		powUp1_Ready = false;
//		powUp2_Ready = false;
		powUp2_InvincibleActive = false;

		Invoke ("PowerUpsALL_FirstTime", 0.5F);

//		powUpsCanRecharge = true;
//		powerUp1_Full = PlayerData_InGame.Instance.p1_PowUp1_FillFull;
//		powerUp2_Full = PlayerData_InGame.Instance.p1_PowUp2_FillFull;
//		powerUp1_AmountCurr = PlayerData_InGame.Instance.p1_PowUp1_FillCurr;
//		powerUp2_AmountCurr = PlayerData_InGame.Instance.p1_PowUp2_FillCurr;
	}

	public void FirstTimePowerUp_ForTutorial (int whatPU) {
		powUp2_InvincibleActive = false;
		PowerUpsALL_FirstTime ();
		powUpFirstAnimsHolder.Play ("PowerUp " + whatPU.ToString () + " First Time Anim 1 (Legacy)");
	}

	void PowerUpsALL_FirstTime() {
		PowerUps_Reset_FirstTime ();
		PowerUp1_Setup_FirstTime ();
		PowerUp2_Setup_FirstTime ();
	}
		
	void PowerUps_Reset_OLD () {
		powerUp1_Count_Ready = PlayerData_InGame.Instance.p1_PowUp1_Count_Ready;
		powerUp2_Count_Ready = PlayerData_InGame.Instance.p1_PowUp2_Count_Ready;

		for (int i = 0; i < 5; i++) {
			powerUp1_StacksArr [i].enabled = false;
			powerUp2_StacksArr [i].enabled = false;
//			PowUp1_StacksReadyParticlesArr [i].Stop ();
//			PowUp2_StacksReadyParticlesArr [i].Stop ();
		}

		// Stacks Setup (Is ready or not)
		for (int i = 0; i < powerUp1_Count_Ready; i++) {
			PowUpStacksParHolder1.GetChild (i).GetChild (1).gameObject.SetActive (true);
			//			powerUp1_StacksArr [i].enabled = true;
			//			PowUp1_StacksReadyParticlesArr [i].Play ();
		}
	}

	void PowerUps_Reset_FirstTime () {
		powerUp1_Count_Own = PlayerData_InGame.Instance.p1_PowUp1_Count_Own;
		powerUp2_Count_Own = PlayerData_InGame.Instance.p1_PowUp2_Count_Own;

		PowUpStacksFrontHolder1.gameObject.SetActive (false);
		PowUpStacksFrontHolder2.gameObject.SetActive (false);
		for (int i = 0; i < 5; i++) {
			PowUpStacksParHolder1.GetChild (i).gameObject.SetActive (false);
			PowUpStacksParHolder2.GetChild (i).gameObject.SetActive (false);
		}

		PowerUps_Reset ();
	}

	void PowerUps_Reset () {
		for (int i = 0; i < 5; i++) {
			PowUpStacksParHolder1.GetChild (i).GetChild (1).gameObject.SetActive (false);
			PowUpStacksParHolder2.GetChild (i).GetChild (1).gameObject.SetActive (false);
		}
	}
		
	void PowerUp1_Setup_FirstTime () {
		if (powerUp1_Count_Own > 0) {
			PowUpStacksFrontHolder1.gameObject.SetActive (true);
			for (int i = 0; i < powerUp1_Count_Own; i++) {
				PowUpStacksParHolder1.GetChild (i).gameObject.SetActive (true);
			}
			PowerUp1_Setup ();
		} else {
			PowUpStacksHairHolder1.gameObject.SetActive (false);
		}
	}

	void PowerUp2_Setup_FirstTime () {
		if (powerUp2_Count_Own > 0) {
			PowUpStacksFrontHolder2.gameObject.SetActive (true);
			for (int i = 0; i < powerUp2_Count_Own; i++) {
				PowUpStacksParHolder2.GetChild (i).gameObject.SetActive (true);
			}
			PowerUp2_Setup ();
		} else {
			PowUpStacksHairHolder2.gameObject.SetActive (false);
		}
	}

	void PowerUp1_Setup () {
		powerUp1_Count_Ready = PlayerData_InGame.Instance.p1_PowUp1_Count_Ready;
		if (powerUp1_Count_Ready > 0) {
			// Activate Front Button & Art
			PowUpStacksHairHolder1.gameObject.SetActive (true);
			PowUpStacksFrontHolder1.GetChild (0).gameObject.SetActive (false);
			PowUpStacksFrontHolder1.GetChild (1).gameObject.SetActive (true);

			// Main Particle Play
			PowUp1_MainReadyParticle.Play ();

			// Stacks Setup (Is ready or not)
			for (int i = 0; i < powerUp1_Count_Ready; i++) {
				PowUpStacksParHolder1.GetChild (i).GetChild (1).gameObject.SetActive (true);
//				PowUp1_StacksReadyParticlesArr [i].Play ();
//				powerUp1_StacksArr [i].enabled = true;
			}

			// Activate Button if ready
			//		if (powerUp1_Count_Own > 0) {
			//			PowUp1_ButtonHolder.enabled = true;
			//			PowUpFillerImageHolder1.enabled = true;
			//			PowUp1_MainReadyParticle.Play ();
			//		} else {
			//			PowUp1_ButtonHolder.enabled = false;
			//			PowUpFillerImageHolder1.enabled = false;
			//		}
		} else {
			PowUpStacksHairHolder1.gameObject.SetActive (false);
			PowUpStacksFrontHolder1.GetChild (0).gameObject.SetActive (true);
			PowUpStacksFrontHolder1.GetChild (1).gameObject.SetActive (false);
		}
	}

	void PowerUp2_Setup () {
		powerUp2_Count_Ready = PlayerData_InGame.Instance.p1_PowUp2_Count_Ready;
		if (powerUp2_Count_Ready > 0) {
			// Activate Front Button & Art
			PowUpStacksHairHolder2.gameObject.SetActive (true);
			PowUpStacksFrontHolder2.GetChild (0).gameObject.SetActive (false);
			PowUpStacksFrontHolder2.GetChild (1).gameObject.SetActive (true);

			// Main Particle Play
			PowUp2_MainReadyParticle.Play ();

			// Stacks Setup (Is ready or not)
			for (int i = 0; i < powerUp2_Count_Ready; i++) {
				PowUpStacksParHolder2.GetChild (i).GetChild (1).gameObject.SetActive (true);
//				PowUp2_StacksReadyParticlesArr [i].Play ();
				//				powerUp1_StacksArr [i].enabled = true;
			}

			// Activate Button if ready
			//		if (powerUp1_Count_Own > 0) {
			//			PowUp1_ButtonHolder.enabled = true;
			//			PowUpFillerImageHolder1.enabled = true;
			//			PowUp1_MainReadyParticle.Play ();
			//		} else {
			//			PowUp1_ButtonHolder.enabled = false;
			//			PowUpFillerImageHolder1.enabled = false;
			//		}
		} else {
			PowUpStacksHairHolder2.gameObject.SetActive (false);
			PowUpStacksFrontHolder2.GetChild (0).gameObject.SetActive (true);
			PowUpStacksFrontHolder2.GetChild (1).gameObject.SetActive (false);
		}
	}

	void PowerUp2_Setup_OLD () {
		// Stacks Setup
		for (int i = 0; i < powerUp2_Count_Own; i++) {
			powerUp2_StacksArr [i].enabled = true;
//			PowUp2_StacksReadyParticlesArr [i].Play ();

		}

		// Activate Button if ready
		if (powerUp2_Count_Own > 0) {
			PowUp2_ButtonHolder.enabled = true;
//			PowUpFillerImageHolder2.enabled = true;
			PowUp2_MainReadyParticle.Play ();
		} else {
			PowUp2_ButtonHolder.enabled = false;
//			PowUpFillerImageHolder2.enabled = false;
		}
	}

	public void PowUpBOTH_StartListening () {
		//Stop Listeners
		EventManager.StartListening ("Pause", PauseMe);
		EventManager.StartListening ("Freeze", FreezeMe);
	}

	public void PowUpBOTH_StopListening () {
		//Stop Listeners
		EventManager.StopListening ("Pause", PauseMe);
		EventManager.StopListening ("Freeze", FreezeMe);
	}

	public void PowerUp1_Pressed_OLD () {
		PowerUpBoth_Screen_Start ();

		// Make Player Invincible for start & during ONLY
		PowerUp1_Invincible_Start ();

		// Calling particles, grads and ripples
		PowerUpBOTH_GradsNParticlesNRipples (1);

//		PowUpAnimHolder1.Stop ();
//		PowUpAnimHolder1.Play ("PowerUp Default Anim 1 (Legacy)");
//		PowUpAnimHolder1.Play ("PowerUp Grads Anim 1 (Legacy)");
//		Debug.LogError ("PRESSED!");
//		powUp1_Ready = false;

		Invoke ("PowerUpBoth_Update", 0.1F);
		Invoke ("PowerUp1_FistEnter", 2F);
		Invoke ("PowerUp1_DamageNow", 4);
		Invoke ("PowerUpBoth_Screen_End", 6);
		Invoke ("PowerUp1_Invincible_End", 7);
	}

	public void PowerUpBOTH_GradsNParticlesNRipples (int gradNumber) {
		// Play start ripple
		if (gradNumber == 1) {
			camScriptHolder.PlayAnim_PowUP1_RippleStart ();
		} else {
			camScriptHolder.PlayAnim_PowUP2_RippleStart ();
		}

		// Play Grads Anim
		WorldGradsAnim_Start (gradNumber);
	}

	public void PowerUp1_ParticlesStop_Decrease () {
		// Decrease PowUp1 Count
		PlayerData_InGame.Instance.p1_PowUp1_Count_Ready--;

		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
		}

		PowUp1_MainReadyParticle.Stop ();
	}

	public void PowerUp2_ParticlesStop_Decrease () {
		// Decrease PowUp1 Count
		PlayerData_InGame.Instance.p1_PowUp2_Count_Ready--;

		// Check for the red overlay (While in use) in case the ready count is zero and overlay needs to go lower
		if (PlayerData_InGame.Instance.p1_PowUp2_Count_Ready == 0) {
			PowUp2_ButtonFront_ArtDisableOnlyHolder.localPosition = new Vector3 (0, -9.5F, 0);
		} else {
			PowUp2_ButtonFront_ArtDisableOnlyHolder.localPosition = Vector3.zero;
		}

		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.PowUp2_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
		}

		PowUp2_MainReadyParticle.Stop ();
	}

	public void PowerUpBOTH_ParticleStopAll () {
		// Disable Particles ()
		for (int i = 0; i < 5; i++) {
//			PowUp1_StacksReadyParticlesArr [i].Stop ();
//			PowUp2_StacksReadyParticlesArr [i].Stop ();
		}
	}

	public void PowerUp1_FistEnter () {
		PowUpHUDFistAnimHolder.Stop ();
		PowUpHUDFistAnimHolder.clip = PowUpHUDFistAnimHolder.GetClip ("PowerUp 1 Fist Launch Anim 1 (Legacy)");
		PowUpHUDFistAnimHolder.Play ();
	}

	public void PowerUp2_ShieldEnter () {
		PowUpHUDShieldAnimHolder.Stop ();
		PowUpHUDShieldAnimHolder.clip = PowUpHUDShieldAnimHolder.GetClip ("PowerUp 2 Shield Launch Anim 1 (Legacy)");
		PowUpHUDShieldAnimHolder.Play ();
	}

	public void PowerUp2_ShieldDurationEnd () {
		// Listener End!
        PowUpBOTH_StopListening ();
        PlayerData_InGame.Instance.p1_PowUp2_Active = false;
        powUp2UsedDisableArtObjHolder.SetActive (false);

		// SOUNDD EFFECTS - Shield Circling The Screen Loop End (done) (Also has a copy in playerdata_ingame)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.ShieldPowerupTime.Stop();
        }

		PowUpHUDShieldAnimHolder[PowUpHUDShieldAnimHolder.clip.name].speed = 1;
		PowUpHUDShieldLOOPAnimHolder.Stop ();
//		PowUpHUDShieldAnimHolder.Stop ();
	}

	public void PowerUp2_ShieldLoop () {
		PowUpHUDShieldLOOPAnimHolder.Stop ();
		PowUpHUDShieldLOOPAnimHolder.Play ("PowerUp 2 Shield Loop Anim 1 (Legacy)");

		// Test for dur
		PowerUp2_ShieldDuration_Start ();

		// Listener Start!
		PowUpBOTH_StartListening ();
	}

	public void PowerUp2_ShieldDuration_Start () {
		PowUpHUDShieldAnimHolder.Stop ();
		PowUpHUDShieldAnimHolder.clip = PowUpHUDShieldAnimHolder.GetClip ("PowerUp 2 Shield Duration Anim 1 (Legacy)");
		PowUpHUDShieldAnimHolder[PowUpHUDShieldAnimHolder.clip.name].speed = 1F / PlayerData_InGame.Instance.p1_PowUp2Duration;
		PowUpHUDShieldAnimHolder.Play ();
	}

	public IEnumerator PowerUp_HudAnimate_1 () {
		yield return null;
		animPU1Parent.Play ();

		PowerUpBoth_Update ();
	}

	public IEnumerator PowerUp_HudAnimate_2 () {
		yield return null;
		animPU2Parent.Play ();

		PowerUpBoth_Update ();
	}

	public void PowerUpBoth_Update () {
		PowerUps_Reset ();
		PowerUp1_Setup ();
		PowerUp2_Setup ();
	}

	public void PowerUpBoth_Screen_Start () {
		EventTrigger.FreezeTheGame();

		// Remove gestures allowance
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;

		// Clean the HUD
		PlayerData_InGame.Instance. Player_CleanScreen_Start ();

		// ReActivate click blocker
		fromGameOverClickBlockerHolder.SetActive (true);

		// Achievement for using any power up
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_PowerUpUse_Any ());
			StartCoroutine (AchievementManager.Instance.AchieveProg_PowerUpUse_8in1_Increase ());
			PlayerData_InGame.Instance.p1_PowUps_Used++;
		}
	}

	public void PowerUpBoth_ReturnHUD (int whichReturn) {
		// Return the HUD
		PlayerData_InGame.Instance. Player_CleanScreen_End (whichReturn);
	}

	public void PowerUpBoth_Screen_End () {
		EventTrigger.UnFreezeTheGame();

		// Re-allow gestures allowance
		PlayerData_InGame.Instance.gameplayGestureAllowed = true;

		// ReActivate click blocker
		fromGameOverClickBlockerHolder.SetActive (false);
	}

	public void PowerUp1_Invincible_Start () {
		// Make Player Invincible for start & during ONLY
		if (!powUp2_InvincibleActive) {
//			Debug.LogWarning ("5 Was INVINCIBLE here?");

			PlayerData_InGame.Instance.p1_InvincibleBuffer = PlayerData_InGame.Instance.p1_Invincible;
			PlayerData_InGame.Instance.p1_Invincible = true;
		}
	}

	public void PowerUp1_Invincible_End () {
		// Stop Player Invincible for start & during ONLY
		if (!powUp2_InvincibleActive) {
//			Debug.LogWarning ("6 Was INVINCIBLE here?");

			PlayerData_InGame.Instance.p1_Invincible = PlayerData_InGame.Instance.p1_InvincibleBuffer;
//			PlayerData_InGame.Instance.p1_Invincible = false;
		}
	}

//	void Update () {
//		Debug.LogError ("Invincible: " + PlayerData_InGame.Instance.p1_Invincible);
//		Debug.LogError ("InvincibleBuffer: " + PlayerData_InGame.Instance.p1_InvincibleBuffer);
//	}

	public void PowerUp2_Invincible_Start () {
//		Debug.LogWarning ("7 Was INVINCIBLE here?");

		powUp2_InvincibleActive = true;
		PlayerData_InGame.Instance.p1_InvincibleBuffer = PlayerData_InGame.Instance.p1_Invincible;
		PlayerData_InGame.Instance.p1_Invincible = true;
	}

	public void PowerUp2_Invincible_End () {
//		Debug.LogWarning ("8 Was INVINCIBLE here?");

		powUp2_InvincibleActive = false;
		PlayerData_InGame.Instance.p1_Invincible = PlayerData_InGame.Instance.p1_InvincibleBuffer;
//		PlayerData_InGame.Instance.p1_Invincible = false;
	}

	public void PowerUp1_RippleFullEffect () {
		camScriptHolder.PlayAnim_PowUP1_RippleFull();
	}

	public void PowerUp2_RippleFullEffect () {
		camScriptHolder.PlayAnim_PowUP2_RippleFull();
	}

	public void PowerUp1_DamageNow () {
		WorldGradsAnim_Finish (1);
		PowerUp1_RippleFullEffect ();

		// Spark and hit and more
		for (int i = 0; i < PowUp1_ParticlesArr.Length; i++) {
			PowUp1_ParticlesArr [i].Play ();
		}

		PowerUp1_KillALL ();
		StartCoroutine (PowUp1_SlowMo ());

		// SOUND EFFECTS - Spark
		if (SoundManager.Instance != null) {
			SoundManager.Instance.FistPowerupSpark.PlayDelayed (0.9F);
		}
	}

	public void PowerUp2_ShieldNow () {
		WorldGradsAnim_Finish (2);
//		PowerUp2_Invincible_Start ();
	}

	public void PowerUp1_KillALL () {
		powerUp1_EnemyCountToKill = EnemiesSpawnHereTransHolder.childCount;
//		Debug.LogWarning ("We = " + powerUp1_EnemyCountToKill);

		for (int i = 0; i < powerUp1_EnemyCountToKill; i++) {
			enemyHealthHolder = EnemiesSpawnHereTransHolder.GetChild (0).GetComponent<EnemyHealth> ();

			if (enemyHealthHolder.myEnemyType == Enemy.EnemyType.Giant1 || enemyHealthHolder.myEnemyType == Enemy.EnemyType.Giant2) {
				enemyHealthHolder.HurtMe ((int)Mathf.Round(enemyHealthHolder.myHpLength_Max * 0.45F), true);
			} else {
				enemyHealthHolder.HurtMe (800, true);
			}
//			Debug.LogWarning ("Each Kill"); 
		}

	}

	// This is a copy of ABOVE function
	public void PowerUpSHOUT_KillAll () {
		powerUp1_EnemyCountToKill = EnemiesSpawnHereTransHolder.childCount;

		if (AchievementManager.Instance != null) {
			canAddToAchieve = true;
		}

		for (int i = 0; i < powerUp1_EnemyCountToKill; i++) {
			enemyHealthHolder = EnemiesSpawnHereTransHolder.GetChild (0).GetComponent<EnemyHealth> ();

			if (enemyHealthHolder.myEnemyType == Enemy.EnemyType.Giant1 || enemyHealthHolder.myEnemyType == Enemy.EnemyType.Giant2) {
				enemyHealthHolder.HurtMe ((int)Mathf.Round(enemyHealthHolder.myHpLength_Max * 0.45F), true);
			} else {
				enemyHealthHolder.HurtMe (800, true);
			}

			if (canAddToAchieve) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_DoLastStand_100 ());
			}
		}
	}

	public void PowerUp2_MovingParticleShield_Play () {
		particlePowUp2_MovingShieldsArr [0].Play ();
		particlePowUp2_MovingShieldsArr [1].Play ();
	}

	public void PowerUp2_MovingParticleShield_Stop () {
		particlePowUp2_MovingShieldsArr [0].Stop ();
		particlePowUp2_MovingShieldsArr [1].Stop ();
	}

	public void WorldGradsAnim_Start (int whichGrad) {
		if (whichGrad == 1) {
			worldGradsAnimHolder.Play ("PowerUp WorldGrads Start Anim 1 (Legacy)");
		} else {
			worldGradsFRONTAnimHolder.Play ("PowerUp WorldGrads Start Anim 1 (Legacy)");
		}
	}

	public void WorldGradsAnim_Finish (int whichGrad) {
		if (whichGrad == 1) {
			worldGradsAnimHolder.Play ("PowerUp WorldGrads Finish Anim 1 (Legacy)");
		} else {
			worldGradsFRONTAnimHolder.Play ("PowerUp WorldGrads Finish Anim 1 (Legacy)");
		}
	}

	IEnumerator PowUp1_SlowMo () {
		Time.timeScale = 0.16F;
		yield return new WaitForSeconds (0.05F);
		Time.timeScale = 1;
	}

	public void PowerUp2_Pressed_OLD () {
//		powerUp2_AmountCurr = 0;
//		PowUpAnimHolder2.Stop ();
//		PowUpAnimHolder2.Play ("PowerUp Default Anim 1 (Legacy)");
//		powUp2_Ready = false;
	}

	public void PauseMe () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {
				powerUp2_AnimDurationPauseTime = PowUpHUDShieldAnimHolder [PowUpHUDShieldAnimHolder.clip.name].time;
				powerUp2_AnimLoopPauseTime = PowUpHUDShieldLOOPAnimHolder [PowUpHUDShieldLOOPAnimHolder.clip.name].time;
			}
			// Stop Legacy Anims
			PowUpHUDShieldAnimHolder.Stop ();
			PowUpHUDShieldLOOPAnimHolder.Stop ();
			particlePowUp2_MovingShieldsArr [0].Pause ();
			particlePowUp2_MovingShieldsArr [1].Pause ();
		}
		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Restore Pre-pause Times
			PowUpHUDShieldAnimHolder [PowUpHUDShieldAnimHolder.clip.name].time = powerUp2_AnimDurationPauseTime;
			PowUpHUDShieldLOOPAnimHolder [PowUpHUDShieldLOOPAnimHolder.clip.name].time = powerUp2_AnimLoopPauseTime;

			// Resume Play of Legacy Anims
			PowUpHUDShieldAnimHolder.Play ();
			PowUpHUDShieldLOOPAnimHolder.Play ();
			particlePowUp2_MovingShieldsArr [0].Play ();
			particlePowUp2_MovingShieldsArr [1].Play ();
		}
	}

	public void FreezeMe () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {
				powerUp2_AnimDurationFreezeTime = PowUpHUDShieldAnimHolder [PowUpHUDShieldAnimHolder.clip.name].time;
				powerUp2_AnimLoopFreezeTime = PowUpHUDShieldLOOPAnimHolder [PowUpHUDShieldLOOPAnimHolder.clip.name].time;
			}
			// Stop Legacy Anims
			PowUpHUDShieldAnimHolder.Stop ();
			PowUpHUDShieldLOOPAnimHolder.Stop ();
			particlePowUp2_MovingShieldsArr [0].Pause ();
			particlePowUp2_MovingShieldsArr [1].Pause ();
		}
		if (EventTrigger.isFrozen && !EventTrigger.isPaused) {
			// Restore Pre-pause Times
			PowUpHUDShieldAnimHolder [PowUpHUDShieldAnimHolder.clip.name].time = powerUp2_AnimDurationFreezeTime;
			PowUpHUDShieldLOOPAnimHolder [PowUpHUDShieldLOOPAnimHolder.clip.name].time = powerUp2_AnimLoopFreezeTime;

			// Resume Play of Legacy Anims
			PowUpHUDShieldAnimHolder.Play ();
			PowUpHUDShieldLOOPAnimHolder.Play ();
			particlePowUp2_MovingShieldsArr [0].Play ();
			particlePowUp2_MovingShieldsArr [1].Play ();
		}
	}

	// Update is called once per frame
//	void Update () {
//		if (!EventTrigger.isPaused && !EventTrigger.isFrozen && powUpsCanRecharge && -1 > 1) {
//
//			// For Power Up 1
//			if (powerUp1_AmountCurr < powerUp1_Full) {
//				powerUp1_AmountCurr += Time.deltaTime;
//				PowUpFillerImageHolder1.fillAmount = powerUp1_AmountCurr / powerUp1_Full;
//			} else {
//				if (!powUp1_Ready) {
//					PowUp1_ButtonHolder.enabled = true;
//					PowUpAnimHolder1.Play ("PowerUp Ready Anim 1 (Legacy)");
//					Debug.LogError ("DDD");
//					powUp1_Ready = true;
//				}
//			}
//
//			// For Power Up 2
//			if (powerUp2_AmountCurr < powerUp2_Full) {
//				powerUp2_AmountCurr += Time.deltaTime;
//				PowUpFillerImageHolder2.fillAmount = powerUp2_AmountCurr / powerUp2_Full;
//			} else {
//				if (!powUp2_Ready) {
//					PowUp2_ButtonHolder.enabled = true;
//					PowUpAnimHolder2.Play ("PowerUp Ready Anim 1 (Legacy)");
//					Debug.LogError ("DDD");
//					powUp2_Ready = true;
//				}
//			}
//
//		}
//	}
}
