﻿using UnityEngine;
using System.Collections;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using TapsellSDK;

public class AfterLoadDailyReward_Script : MonoBehaviour {

	//Tapsell ids
	string DailyRewardZoneId = Balance_Constants_Script.Tapsell_AfterDailyString;
	public string DailyRewardAdId = null;
    public TapsellAd DailyAd = null;

    DateTime today;
    DateTime lastSaved;

	public bool videoAvailable;
	public bool acceptVidButtonPress;

	public int intMessageDailyRewardAd;

    void Start () {
		acceptVidButtonPress = false;

        RequestDailyRewardAd();

        // Should be uncommented for time machine
        //AfterDaily_TimeMachine();

    }

	public void AfterDaily_TimeMachine () {
		// Time Machine
		PlayerPrefs.SetString("lastDateReward", DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy"));

//		DateTime whatDate = DateTime.ParseExact(PlayerPrefs.GetString("lastDateReward", "01/01/1991"), "MM/dd/yyyy", null);
//		Debug.LogError ("LAAAAAST SAVE DATE: " + whatDate.Date);
	}

	// If true open Daily reward pop Up
	public bool IsEligibleForDailyReward()
	{
		today = DateTime.Now;
		lastSaved = DateTime.ParseExact(PlayerPrefs.GetString("lastDateReward", "01/01/1991"), "MM/dd/yyyy", null);
		Debug.Log("LAST SAVE DATE: " + lastSaved.Date);
		Debug.Log("TODAY DATE: " + today.Date);
		if (lastSaved.Date < today.Date)
		{
			Debug.Log("mitoone jayeze begire");
			return true;
		} else {
			return false;
		}
	}

	public void SetDateAfterVid () {
		PlayerPrefs.SetString("lastDateReward",today.ToString("MM/dd/yyyy"));
	}

	private void RequestDailyRewardAd()
    {
		intMessageDailyRewardAd = 0;
        Tapsell.requestAd(DailyRewardZoneId, false,
    (TapsellAd result) =>
    {
                // onAdAvailable
                Debug.Log("Action: onAdAvailable");

				intMessageDailyRewardAd = 1;
				videoAvailable = true;
                DailyAd = result;
                DailyRewardAdId = result.adId; // store this to show the ad later
            },
    (string zoneId) =>
    {
                // onNoAdAvailable
				intMessageDailyRewardAd = 2;
				videoAvailable = false;
                Debug.Log("No Ad Available");
    },
    (TapsellError error) =>
    {
                // onError / network error somewhere
				intMessageDailyRewardAd = 3;
				videoAvailable = false;
                Debug.Log(error.error);
    },
    (string zoneId) =>
    {
//				
                // onNoNetwork / no internet access
				intMessageDailyRewardAd = 4;
				videoAvailable = false;
                Debug.Log("No Network");
    },
    (TapsellAd result) =>
    {
                // onExpiring
                Debug.Log("Expiring");
				videoAvailable = false;
        // this ad is expired, you must download a new ad for this zone
        DailyRewardAdId= null;
        RequestDailyRewardAd();
    }
    );
    }

	public void ShowVideo()
	{
		TapsellShowOptions showOptions = new TapsellShowOptions();
		showOptions.backDisabled = true;
		showOptions.immersiveMode = false;
		showOptions.rotationMode = TapsellShowOptions.ROTATION_UNLOCKED;
		showOptions.showDialog = true;
		Tapsell.showAd(DailyAd, showOptions);

		Tapsell.setRewardListener ((TapsellAdFinishedResult result) => {
			if (result.completed && result.rewarded) {
				acceptVidButtonPress = true;
			}

			DailyRewardAdId = null;
		}
		);
	}

//    IEnumerator PingSite()
//    {
//        var sitePing = new Ping("66.85.73.159");
//
//        while (!sitePing.isDone)
//        {
//            yield return null;
//        }
//        Debug.Log(sitePing.time);
//
//
//
//        //har vaght ping shod site boro baraye gereftan time az server
//        StartCoroutine(ShowTime());
//
//    }
//    IEnumerator ShowTime()
//    {
//        CoroutineWithData cd = new CoroutineWithData(this, getTime());
//        yield return cd.coroutine;
//        Debug.Log("result is " + cd.result);  //  'success' or 'fail'
//        long todayTime = long.Parse(cd.result.ToString());
//        //Debug.Log(new DateTime(todayTime).ToShortDateString());
//
//        //1-rooz jari ro az server begir
//        today = new DateTime(todayTime);
//        todayDate.text = "emrooz: " + today.ToString("MM/dd/yyyy");
//
//        //2-akharin rooz save shode az player pref bekhoon
//        lastSaved = DateTime.ParseExact(PlayerPrefs.GetString("lastDateReward"), "MM/dd/yyyy", null);
//        lastDateSaved.text = "akhrin rooz reward:" + PlayerPrefs.GetString("lastDateReward", "chizi sabt nashode");
//
//        //3-moghayese kon bebin ja dare emrooz jayeze begire
//        Debug.Log(lastSaved.Date);
//        Debug.Log(today.Date);
//        if (lastSaved.Date < today.Date)
//        {
//            Debug.Log("mitoone jayeze begire");
//            sibilReward.interactable = true;
//            //4-agar mohegh bood be jayeze popup baz she
//            dailyRewardPopUp.SetActive(true);
//        }
//    }

//    IEnumerator getTime()
//    {
//        //WWW www = new WWW("http://www.saadkhawaja.com/gettime.php");
//        //WWW www = new WWW("http://localhost:11196/TodaysTime.aspx");
//        WWW www = new WWW("http://1000moshtan.somee.com/TodaysTime.aspx");
//        yield return www;
//
//        Debug.Log("Time on the server is now: " + FilterText(www.text).Substring(13));
//
//        if (String.IsNullOrEmpty(www.error))
//        {
//            yield return FilterText(www.text).Substring(13);// "success";
//        }
//        else
//        {
//            yield return "fail";
//        }
//    }
//    public class CoroutineWithData
//    {
//        public Coroutine coroutine { get; private set; }
//        public object result;
//        private IEnumerator target;
//        public CoroutineWithData(MonoBehaviour owner, IEnumerator target)
//        {
//            this.target = target;
//            this.coroutine = owner.StartCoroutine(Run());
//        }
//
//        private IEnumerator Run()
//        {
//            while (target.MoveNext())
//            {
//                result = target.Current;
//                yield return result;
//            }
//        }
//    }
//    public string FilterText(string text)
//    {
//        string pattern = @"\THISISTHETEXT\w+";
//        return Regex.Match(text, pattern).Value;
//    }
//    private void GiveRewardToPlayer(int rewardAmount)
//    {
//        StoreInventory.GiveItem(MoshtanStoreAssets.SIBIL_CURRENCY_ITEM_ID, rewardAmount);
//
//        PlayerPrefs.SetString("lastDateReward",today.ToString("MM/dd/yyyy"));
//
//        dailyRewardPopUp.SetActive(false);
//    }

}
