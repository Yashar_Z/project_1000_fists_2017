﻿using UnityEngine;
using System.Collections;

public class BackDarker_Script : MonoBehaviour {

	public Animator animatorBackDarkerHolder;

	public void BackDarker_WhiteToBlack () {
		EnableDarker ();

//		animatorBackDarkerHolder.Play ("Menu - Click Behind WhiteToBlack Anim 1");
//		animatorBackDarkerHolder.enabled = true;
		//		animatorClickBhindHolder.SetTrigger ("WhiteToBlack Param");

	}

	public void BackDarker_BlackToWhite () {
		animatorBackDarkerHolder.Play ("Menu - Click Behind BlackToWhite Anim 1");
	}

	public void EnableDarker() {
		this.gameObject.SetActive (false);
	}

	public void DisableDarker() {
		this.gameObject.SetActive (false);
	}
}
