﻿using UnityEngine;
using System.Collections;

public class Body_Menu_Script : MonoBehaviour {

	public Menu_BackAnims_Script menuBackAnimsScriptHolder;

	public Animation animBodyEyeBrowsHolder; 
	public ParticleSystem particleBodySymbolHolder;

	public void Pressed_MenuBody_TapAnim () {
		Body_Anim_TapAnim ();

		// For Eyebrows Idle reset & stop
		menuBackAnimsScriptHolder.MenuAnims_BodyEyeBrows_Reset ();

		// Body Symbol Particle
		particleBodySymbolHolder.Emit (2);

        // SOUNDD EFFECTS: Tap on Body in menu (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.Arbade.Play();
        }
	}

	public void Body_Anim_TapAnim () {
		menuBackAnimsScriptHolder.MenuAnims_Body_TapAnim ();
	}

	public void Body_Anim_Idle () {
		menuBackAnimsScriptHolder.MenuAnims_Body_Idle ();

		// For Eyebrows Idle play
		menuBackAnimsScriptHolder.MenuAnims_BodyEyeBrows_Idle ();
	}
}
