﻿using UnityEngine;
using System.Collections;

public class ButtonOver_CanvasScript : MonoBehaviour {

	public Canvas canvasButtonHolder;
	public UnityEngine.UI.GraphicRaycaster raycasterButtonHolder;

	void OnEnable () {
		canvasButtonHolder.enabled = true;
		raycasterButtonHolder.enabled = true;
	}

	void OnDisable () {
		canvasButtonHolder.enabled = false;
		raycasterButtonHolder.enabled = false;
	}

}
