﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Endless_All_Script : MonoBehaviour {

	public Menu_Main_Script menuMainScriptHolder;

	public Animation animEndlessAllHolder;
	public AnimationClip[] animClipEndlessAllArr;

	public Text text_RecordNumber;

	public int intScore_Record;

	// These is COPYied from Popup_EndlessEnd_Script
	public int intEndlessLevel;
	public int intXP_Curr;
	public int intXP_Req_ForWholeLevel;
	public int intXP_Req_ForPartialLevel;
	public float intXP_RatioNormalized_Start;
	public float intXP_RatioNormalized_Target;

	[Header ("Endless Canvas")]
	public Canvas canvasEndlessHolder;
	public GraphicRaycaster graphRayCasterHolder;

	public void MoveIn_Endless () {
		Phone_Stats_Script.Instance.startOfShortCutWhat = Phone_Stats_Script.StartOfShortCutType.Phone_EndlessMenu;
		menuMainScriptHolder.popUpPowUpsParScript.PowerUpsMenu_Enter ();
		transform.localPosition = Vector2.zero;

		animEndlessAllHolder.clip = animClipEndlessAllArr [0];
		animEndlessAllHolder.Play ();

		AnimEvent_CanvasEnable ();

		// Setup endless menu numbers
		Setup_EndlessMenuNumbers ();

		// Resize Bar to zero at the move in
		Endless_BarController.Instance.EndlessBar_ScaleZero ();

		// Show player level in endless menu display
		Endless_BarController.Instance.endlessBarUpdater.Level_DisplaySet ();
	}

	public void EndlessAnimExit () {
		animEndlessAllHolder.clip = animClipEndlessAllArr [1];
		animEndlessAllHolder.Play ();

		Endless_PowUpsLeave ();

		Endless_BarController.Instance.EndlessBar_LeaveAnim ();
	}

	public void Endless_PowUpsLeave () {
		menuMainScriptHolder.popUpPowUpsParScript.PowerUpsMenu_Leave ();
	}

	public void EndlessAnimToIngame () {
		animEndlessAllHolder.clip = animClipEndlessAllArr [2];
		animEndlessAllHolder.Play ();
	}

	public void MoveOut_Endless () {
		transform.localPosition = new Vector2 (3200, 0);

		// This is to disable endless menu after delay
		menuMainScriptHolder.ZoomOut_OnlyMenuActivator ();

		AnimEvent_CanvasDisable ();
	}

	void Awake () {
		AnimEvent_CanvasDisable ();
	}

	public void Pressed_EndlessPlay () {
		PlayerData_Main.Instance.gameMainSpawnType = PlayerData_InGame.GameSpawnType.Endless;
		EndlessAnimToIngame ();

		PlayerData_Main.Instance.WorldNumber = 1;
		PlayerData_Main.Instance.LevelNumber = 1;

		StartCoroutine (menuMainScriptHolder.Prepare_MenuToInGame ());

		// Leave anim for bar prefab when play starts
//		Endless_BarController.Instance.EndlessBar_LeaveAnim ();

		// SOUND EFFECTS - Press Endless Play In Menu (Empty)

		// Disable tut circle for pressing play endless
		if (!PlayerData_Main.Instance.TutSeen_EndlessUnlock) {
			menuMainScriptHolder.menuBackAnimsScriptHolder.Endless_AfterPlayPressFirstTime ();
		}
	}

	public IEnumerator BarQuestionShow () {
		yield return new WaitForSeconds (1);
		Endless_BarController.Instance.EndlessBar_ShowQuestion ();
	}

	public void AnimEvent_BarEnterMenu () {
		Endless_BarController.Instance.EndlessBar_EnterAnim ();

		StartCoroutine (BarQuestionShow());
	}

	public void AnimEvent_CanvasEnable () {
		StartCoroutine (Endless_CanvasEnable ());
	}

	public void AnimEvent_CanvasDisable () {
		canvasEndlessHolder.enabled = false;
		graphRayCasterHolder.enabled = false;
		//		StartCoroutine (World_CanvasDisable ());
	}

	IEnumerator Endless_CanvasEnable () {
		yield return null;
		canvasEndlessHolder.enabled = true;
		graphRayCasterHolder.enabled = true;
	}

	IEnumerator Endless_CanvasDisable () {
		yield return null;
		canvasEndlessHolder.enabled = false;
		graphRayCasterHolder.enabled = false;
	}

	public void Setup_EndlessMenuNumbers () {
		Setup_Score_EndlessEnd ();

		// Show player record
		text_RecordNumber.text = intScore_Record.ToString ();
	}


	// Copied from Popup_EndlessEnd_Script
	public void Setup_Score_EndlessEnd () {
		// Getting values from player data main so if play quits, xp is still added
		intEndlessLevel = PlayerData_Main.Instance.player_EndlessLevel;
		intXP_Curr = PlayerData_Main.Instance.player_XP_Curr;
		intScore_Record = PlayerData_Main.Instance.player_Score_Record;

		Setup_XP_Req ();

		Endless_BarController.Instance.endlessBarUpdater.Setup_BarFixedPosit (intXP_RatioNormalized_Start);
		Endless_BarController.Instance.endlessBarUpdater.Setup_XPforDisplay (intXP_Curr, intXP_Req_ForWholeLevel);
	}

	// Copied from Popup_EndlessEnd_Script
	public void Setup_XP_Req () {
		switch (intEndlessLevel) {
		case 1:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl2;
			break;
		case 2:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl3;
			break;
		case 3:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl4;
			break;
		case 4:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl5;
			break;
		case 5:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl6;
			break;
		case 6:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl7;
			break;
		case 7:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl8;
			break;
		case 8:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl9;
			break;
		case 9:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl10;
			break;
		case 10:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl11;
			break;
		case 11:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl12;
			break;
		case 12:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl13;
			break;
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_LvlMore;
			break;
		default:
			Debug.LogError ("Default Error");
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_LvlMore;
			break;
		}

		// XP Required from current XP amount to first new level
		intXP_Req_ForPartialLevel = intXP_Req_ForWholeLevel - intXP_Curr;

		// Start: Normalized position for xp / bar
		intXP_RatioNormalized_Start = (float) intXP_Curr / intXP_Req_ForWholeLevel;
	}
}
