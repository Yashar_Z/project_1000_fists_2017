﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Event_Menu_Script : MonoBehaviour {

	private static readonly string APP_ID = "1";
	private static readonly string VAS_RECIEVER = "307172";

	public static System.Action<string> OnVAS_PurchaseSuccessful;
	public static System.Action OnVAS_PurchaseUnsuccessful;

	// For Mohammad Logics
	private WWW registerResponse;
	private WWW validationResponse;
	private WWW resendValidationResponse;
	private WWW checkSubscriptionResponse;
	private WWW IAPResponse;
	private WWW ConfirmIAPResponse;

	public Text EnterPhoneTextHolder;
	public Text EnterPromoCodeTextHolder;
	public Text EnterValidationTextHolder;
	public Text EnterShopResponseTextHolder;

	public Text textStatus;
	public Text text_WrongNumber;

	public GameObject[] objEvent_PopUpsArr;
	public Canvas canvasHolder;
	public GraphicRaycaster graphicRayCasterHolder;

	public Popup_Button_Script[] popupButtonScriptArr;
	public Popup_ZeroInput_Script popZeroInputScriptHolder;
	public Event_Success_Script eventSuccessScriptHolder;
	public Menu_Main_Script menuMainScriptHolder;

	// Pop-up for event success window
	public Popup_All_Script popupAllScriptHolder;

	public string SelectedShopID;

	public string PhoneNumber;
	public string UserId;
	public string AccessToken;
	public string LastPurchaseTransactionId;

//	void Start () {
//		Start_Event ();
//	}

	public void Start_Event () {
		PopUp_Event_OpenThis (0);
		textStatus.text = "";

		MoveIn_EventMenues ();

		StartCoroutine (Event_ArtsButtonsEnter ());

		/*
		// TODO: local save ro check kon agar phone number ya userid dar on nabood enterphone ro neshon bede
		if ((PhoneNumber == "") || (UserId == "") || (AccessToken == ""))
		{
			Event_Controller_Activator (true);

			//			pState = PopUpState.EnterPhone;
			PopUp_VAS_OpenThis (0);
			textStatus.text = "";
		}
		else
		{
			//			pState = PopUpState.NoPopUp;
			//			textStatus.text = "checking subscription";
			//			CheckSubscription();

			// Allows to continue (Either to tutorial or to menu);

			//			Debug.LogError ("Yo 1");
			ContinueToGame_FromLoader ();
		}
		*/
	}

	public IEnumerator Event_ArtsButtonsEnter () {
		yield return new WaitForSeconds (1);
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.3F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void PopUp_Event_OpenThis (int whichInt) {
		objEvent_PopUpsArr [whichInt].SetActive (true);
	}

	public void MoveIn_EventMenues () {
		this.transform.localPosition = Vector2.zero;
		canvasHolder.enabled = true;
		graphicRayCasterHolder.enabled = true;
	}

	public void MoveOut_EventMenues () {
		this.transform.localPosition = new Vector2 (3200, 0);
		canvasHolder.enabled = false;
		graphicRayCasterHolder.enabled = false;
	}

	public IEnumerator MoveOut_EventRoutine (float delay) {
		yield return new WaitForSeconds (delay);
		MoveOut_EventMenues ();
	}

	public void Pressed_EventContinue () {
		PopUp_Event_OpenThis (1);
	}

	public void Pressed_SiteDetails () {
		Application.OpenURL ("https://www.instagram.com/hezarmoshtan");
	}

	public void Pressed_PhoneEnter_Accept () {
		string pn = EnterPhoneTextHolder.text;
		Debug.Log(pn);
		string android_id = "12345";

		#if !UNITY_EDITOR && UNITY_ANDROID
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
		AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
		android_id = secure.CallStatic<string>("getString", contentResolver, "android_id");
		#endif
		Debug.Log(android_id);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("DeviceInfo", android_id);
        // agar presenter code khali bood be onvane default 1000 estefade mishe
		if (EnterPromoCodeTextHolder.text == "") {
			postParameters.Add ("PresenterCode", "666");
		} else {
			postParameters.Add ("PresenterCode", EnterPromoCodeTextHolder.text);
		}
		postParameters.Add("Name", "1");
		postParameters.Add("PhoneNumber", pn);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		registerResponse = this.GetComponent<RestUtil>().POST("http://apis.1000moshtan.com/api/Login/Login",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("enter Phone post completed");
				PhoneNumber = pn;
				PlayerPrefs.SetString("PhoneNumber", PhoneNumber);
				textStatus.text = "";
			});
		StartCoroutine(AmaliatRegister(() =>
			{
				Debug.Log("json parse complete");
			}
		));
		textStatus.text = "dar hale ferestandan etelat be server baraye ersal code taeedie";
	}

	private class RegisterResponseModel
	{
		public string Message;
		public string Result;
	}

	public IEnumerator AmaliatRegister(System.Action OnComplete)
	{
		Popup_VasCircleLoading_Start ("ﻩﺭﺎﻤﺷ ﯽﺳﺭﺮﺑ ﻝﺎﺣ ﺭﺩ");

		while (!registerResponse.isDone)
			yield return null;

		if (registerResponse.text != "") {

			Debug.Log (registerResponse.text);

			RegisterResponseModel x = JsonUtility.FromJson<RegisterResponseModel> (registerResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);

			if (OnComplete != null) {
				OnComplete ();
			}

			// In VAS, this was x.Result instead
			if (x.Result == "Successful") {
				Debug.Log ("register completed");

				// Open Verify Number menu
				PopUp_Event_OpenThis (2);
			} else {
				// TODO: Message for phone entery problem (Server problem, number problem and more...). Needs new pop-up

				switch (x.Message) {
				case "PhoneNumber is incorrect":
					text_WrongNumber.text = "!ﺖﺳﺍ ﻩﺎﺒﺘﺷﺍ ﻩﺪﺷ ﺩﺭﺍﻭ ﻩﺭﺎﻤﺷ";
					break;
				case "PresenterCode is incorrect":
					text_WrongNumber.text = "!ﺖﺳﺍ ﻩﺎﺒﺘﺷﺍ ﻩﺪﻨﻨﮐ ﺕﻮﻋﺩ ﺪﮐ";
					break;
				case "Login before":
					text_WrongNumber.text = "!ﺖﺳﺍ ﻩﺪﺷ ﺩﺭﺍﻭ ﻼﺒﻗ ﻩﺭﺎﻤﺷ";
					break;
				case "send sms failed":
					text_WrongNumber.text = "(ﺭﻭﺮﺳ ﻞﮑﺸﻣ) ﺪﯿﻨﮐ ﺵﻼﺗ ﻩﺭﺎﺑﻭﺩ";
					break;
				default:
					text_WrongNumber.text = "!ﺪﯿﻨﮐ ﺵﻼﺗ ﻩﺭﺎﺑﻭﺩ";
					Debug.LogError ("Default Error");
					break;
				}

				// Open Wrong Number
				PopUp_Event_OpenThis (4);

				Debug.Log (x.Message);
//			pState = PopUpState.EnterPhone;
			}

			Popup_VasCircleLoading_End ();
		} 

		// Server response was empty (didn't work)
		else {
			//			textStatus.text = "server rid. anjam nashod";
			PopUp_Event_OpenThis (6);

			Moshtan_Utilties_Script.ShowToast ("لطفا دوباره تلاش کنید!");

			Popup_VasCircleLoading_End ();
		}
	}

	public void Pressed_VerifyNumber_Accept () {
		Debug.LogError ("Pressed_VerifyNumber_Accept");

		string vc = EnterValidationTextHolder.text;
		Debug.Log(vc);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("ValidationCode", vc);
		postParameters.Add("PhoneNumber", PhoneNumber);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		validationResponse = this.GetComponent<RestUtil>().POST("http://apis.1000moshtan.com/api/Login/Validate",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("validation post completed");
				//				pState = PopUpState.NoPopUp;
				textStatus.text = "";
			});
		StartCoroutine(AmaliatValidation(() => { Debug.Log("json parse complete"); }));

		textStatus.text = "dar halle barresi code validation";
		//		pState = PopUpState.NoPopUp;
	}

	public IEnumerator AmaliatValidation(System.Action OnComplete)
	{
		Popup_VasCircleLoading_Start ("ﺪﮐ ﯽﺳﺭﺮﺑ ﻝﺎﺣ ﺭﺩ");

		while (!validationResponse.isDone)
			yield return null;

		if (validationResponse.text != "") {

			Debug.Log (validationResponse.text);

			var x = JsonUtility.FromJson<ValidationResponseModel> (validationResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);
			//Debug.Log (x.AccessToken);
			//Debug.Log (x.UserId);
			//Debug.Log (x.ValidDays);

			if (OnComplete != null) {
				OnComplete ();
			}
			if (x.Result == "Successful") {
				// Code was valid

				// TODO: Save valid. 

				//			textStatus.text = "ba tashakor sms welocome alan vasat miad";
				textStatus.text = "";
				//Debug.LogError ("x userID = " + x.UserId);
				//PlayerPrefs.SetString ("UserId", x.UserId);
				//PlayerPrefs.SetString ("AccessToken", x.AccessToken);
				//UserId = x.UserId;
				//AccessToken = x.AccessToken;

				// Now the code was valid
				PopUp_Event_OpenThis (5);

				// Can Start confetti
				StartCoroutine (SuccessStartRoutine ());

				// Save and more
				PlayerData_Main.Instance.Event_NewYear_SawPopup = true;
				PlayerData_Main.Instance.Event_NewYear_Registered = true;

			} else {
				// Code was invalid
				PopUp_Event_OpenThis (3);
				textStatus.text = "";

				//			textStatus.text = "code validation eshtebah bood";
				//			pState = PopUpState.ResendValidation;
			}

			Popup_VasCircleLoading_End ();
		} 

		// Time out or another problem from server
		else {
			textStatus.text = "Server ride.";

			// Open Servers fail popup
			PopUp_Event_OpenThis (6);

			Moshtan_Utilties_Script.ShowToast ("لطفا دوباره تلاش کنید!");

			//			PopUp_VAS_OpenThis (1);
		}
	}

	public class ValidationResponseModel
	{
		public string Message;
		public string Result;
		//public string AccessToken;
		//public string UserId;
		//public int ValidDays;
	}

	public void Pressed_VerifyNumber_ResendValid () {
		StartCoroutine (Pressed_VerifyNumber_ResendValidCoroutine ());
	}

	public IEnumerator Pressed_VerifyNumber_ResendValidCoroutine () {
		//Pressed_VerifyNumber_ResendValidNOW ();
		yield return new WaitForSeconds (1);
		PopUp_Event_OpenThis (1);
	}
    
	public void Pressed_VerifyNumber_ResendPhone () {
		Debug.LogError ("Pressed_VerifyNumber_ResendPhone");

		PopUp_Event_OpenThis (1);
	}

	public void Pressed_CodeNotCorr_TryAgain () {
		Debug.LogError ("Pressed_CodeNotCorr_TryAgain");

		PopUp_Event_OpenThis (2);
//		PopUp_Event_OpenThis (1);
	}

	public void Pressed_NumberNotReg_Accept () {
		Debug.LogError ("Pressed_NumberNotReg_Accept");

		Application.Quit ();
	}

	public void Pressed_SuccessEnd () {
		StartCoroutine (SuccessEndRoutine ());
	}

	public IEnumerator SuccessStartRoutine () {
		yield return new WaitForSeconds (0.25F);
		eventSuccessScriptHolder.StartSuccess ();
	}

	public IEnumerator SuccessEndRoutine () {
		yield return null;
		eventSuccessScriptHolder.EndSuccess ();

		// Play the story for end of New Year event
		menuMainScriptHolder.menuTutCheckScriptHolder.storyMainScriptHolder.StartStory (1, 11, 0, false);

		// Close button
		menuMainScriptHolder.Event_Close ();

		yield return new WaitForSeconds (0.2F);
		popupAllScriptHolder.PopUp_AnimPlay (1);

		StartCoroutine (MoveOut_EventRoutine (0.3F));
	}

	public void Pressed_ServerFailExit () {
		StartCoroutine (MoveOut_EventRoutine (0.3F));
	}

	public void Pressed_Exit () {
		Pressed_ServerFailExit ();
	}

	public void Pressed_FastSubscribe_FastSub () {
		Debug.LogError ("Pressed_FastSubscribe_FastSub");

		Application.OpenURL("sms:" + VAS_RECIEVER + "?body=1");
		Application.Quit ();
	}

	public void Pressed_FastSubscribe_Exit () {
		Debug.LogError ("Pressed_FastSubscribe_Exit");

		Application.Quit ();
	}

	public void Pressed_ExitCharge_Exit () {
		Debug.LogError ("Pressed_ExitCharge_Exit");

		Application.Quit ();
	}

	
	public IEnumerator Event_Controller_DelayActive () {
		yield return new WaitForSeconds (0.1F);
		Event_Controller_Activator (true);
	}

	public void Event_Controller_Activator (bool whichBool) {
		canvasHolder.enabled = whichBool;
		graphicRayCasterHolder.enabled = whichBool;
		//		this.gameObject.SetActive (whichBool);

		if (whichBool) {
			// Move in trans
			transform.localPosition = Vector2.zero;
		} else {
			// Move out trans
			transform.localPosition = new Vector2 (3200, 0);
		}
	}

	public void Popup_VasCircleLoading_Start (string whatString) {
		StartCoroutine (Event_Controller_DelayActive ());

		popZeroInputScriptHolder.PopUp_ZeroInput_TextChange (whatString);
		PopUp_Event_OpenThis (7);
	}

	public void Popup_VasCircleLoading_End () {
		popZeroInputScriptHolder.PopUp_CircleLoading_Close ();
	}

	//	void Update () {
	//		if (Input.GetKey ("m")) {
	//			popZeroInpitScriptHolder.PopUp_CircleLoading_AnimateStart ();
	//		}
	//	}

}
