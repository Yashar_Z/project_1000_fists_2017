﻿using UnityEngine;
using System.Collections;

public class Event_Success_Script : MonoBehaviour {

	public Animation animEventSuccessHolder;
	public AnimationClip[] animClipSuccessArr;

	public ParticleSystem[] particleSuccessArr;

	public void StartSuccess () {
		animEventSuccessHolder.clip = animClipSuccessArr [0];
		animEventSuccessHolder.Play ();

		for (int i = 0; i < particleSuccessArr.Length; i++) {
			particleSuccessArr [i].Play ();
		}
	}

	public void EndSuccess () {
		animEventSuccessHolder.clip = animClipSuccessArr [1];
		animEventSuccessHolder.Play ();

		for (int i = 0; i < particleSuccessArr.Length; i++) {
			particleSuccessArr [i].Stop ();
		}
	}
}
