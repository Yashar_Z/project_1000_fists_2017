﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Fabric.Answers;

public class Item_Menu_Script : MonoBehaviour {

	public int EGG_PRICE_1_KOOCHOOL;
	public int EGG_PRICE_2_AGHDASI;
	public int EGG_PRICE_3_MOSHTAN;

	public ItemDatabase itemDB;
	public Item_Display_Script[] itemDisplayScriptArr;
	public Reward_Machine_Script rewardMachineScriptHolder;
	public Mustache_Display_Script mustacheDisplayScriptHolder;
	public Menu_Main_Script menuMainScriptHolder;

	public Menu_TutCheck_Script menuTutCheckScriptHolder;

	public Button buttonItemProgressHolder;
	public Button buttonItemProgress_FrontOverHolder;

	// This does not have normal button because 
//	public Button buttonItemRepeatItemHolder;
	public Button buttonItemRepeatItem_FrontOverHolder;

	[Header ("No Sort Stuff")]
	public bool isSortable;

	public ScrollRect scrollItemsHolder;
	public ScrollRect scrollPhoneItemDescripHolder;

//	public AnimationClip[] animClipTwirlArr;
//	public Animation animCamTwirlHolder;

	public Animation[] animEggParentsArr;
	public ParticleSystem[] particleEggSelectArr;

	public GameObject objPhoneParentHolder;
	public GameObject objEggSelectBlockerHolder;
	public GameObject objItemsGridHolder;

	public GameObject objBulletPointHolder_1_2;
	public GameObject objBulletPointHolder_2_2;
	public GameObject objBulletPointHolder_1_1;

	public Transform transItemsGridParentHolder;

	public Color colorItemCount_Normal;
	public Color colorItemCount_Complete;

	public Color colorItemEffect_Normal;
	public Color colorItemEffect_Complete;

	public Color colorItemBullet_Normal;
	public Color colorItemBullet_Complete;

	public Image imageItemCountBackHolder;
	public Image imageBulletPointHolder_1_2;
	public Image imageBulletPointHolder_2_2;
	public Image imageBulletPointHolder_1_1;

	public GameObject objParticleItemCounterHolder;
	public GameObject objTooltipItemProgHolder;

	public Text text_ItemName;
	public Text text_ItemEffect;
	public Text text_ItemDescription;
	public Text text_ItemCount;

	public Text text_EggPrice_Koochool;
	public Text text_EggPrice_Aghdasi;
	public Text text_EggPrice_Moshtan;

	[Header ("Items Canvas")]
	public Canvas canvasItemsHolder;
//	public GraphicRaycaster graphRayCasterHolder;

	[Header ("Phone Stuff")]
	public Item_Display_Script itemDisplayScriptHolder_Phone;
	public Animation animPhoneParentHolder;

	[HideInInspector]
	public bool itemIsEmpty;

	[HideInInspector]
	public int itemNumber_ForPhoneNext;

	[HideInInspector]
	public int eggNumber_ToReturn;

	private int intItemCountCurBuffer;
	private int intItemCountNeedsToComBuffer;

	private float floatAutoScrollPreTarget = 0.039F;
	private float floatAutoScrollTarget = 0.0384F;

	private bool isAutoScrolling_1;
	private bool isAutoScrolling_2;

	// TODO: Check to make sure this should have indeed been commented
//	void OnEnable () {
//		menuMainScriptHolder.Menu_HUD_Update ();
//		ScrollRect_Reset ();
//	}

	void Awake () {
		if (PlayerData_Main.Instance != null) {
			itemDB = PlayerData_Main.Instance.player_ItemDataBase;
		} else {
			itemDB = Resources.Load<ItemDatabase> (@"itemDB");
		}

		AnimEvent_CanvasDisable ();
	}

	void Start () {
		// Get proper egg values (From Balance Constants)
		EGG_PRICE_1_KOOCHOOL = Balance_Constants_Script.EggPrice_Koochool;
		EGG_PRICE_2_AGHDASI = Balance_Constants_Script.EggPrice_Aghdasi;
		EGG_PRICE_3_MOSHTAN = Balance_Constants_Script.EggPrice_Moshtan;

		// Deactivate Phone at the start
		objPhoneParentHolder.SetActive (false);

		Setup_EggPrices ();

		// Get compt for item displays
		itemDisplayScriptArr = GetComponentsInChildren<Item_Display_Script> ();

		// Setup all items
		SetupDisplay_AllItems ();

		// Reset scroll for the start
		ScrollRect_Reset ();
	}

	public void Setup_EggPrices () {
		text_EggPrice_Koochool.text = EGG_PRICE_1_KOOCHOOL.ToString ();
		text_EggPrice_Aghdasi.text = EGG_PRICE_2_AGHDASI.ToString ();
		text_EggPrice_Moshtan.text = EGG_PRICE_3_MOSHTAN.ToString ();
	}

	public void Setup_Phone (Item whatItem, int whichIndex, bool isNextButton) {
		itemDisplayScriptHolder_Phone.thisItem = whatItem;
		text_ItemName.text = whatItem.farsiName;
		text_ItemEffect.text = whatItem.itemEffectDescription;

		objBulletPointHolder_1_2.SetActive (false);
		objBulletPointHolder_2_2.SetActive (false);
		objBulletPointHolder_1_1.SetActive (false);

		if (whatItem.isTwoLine) {
			if (!whatItem.isTwoLineOneEffect) {
				objBulletPointHolder_1_2.SetActive (true);
				objBulletPointHolder_2_2.SetActive (true);
			} else {
				objBulletPointHolder_1_2.SetActive (true);
			}
		} else {
			objBulletPointHolder_1_1.SetActive (true);
		}

		text_ItemDescription.text = whatItem.description;
		text_ItemDescription.text = text_ItemDescription.text +"\n";

		intItemCountCurBuffer = ItemWithAmount.FindItemById (whatItem.itemId).CurAmount;
		intItemCountNeedsToComBuffer = whatItem.needsToComplete;

		// Change color and content of item count base on completeness
		if (intItemCountCurBuffer == intItemCountNeedsToComBuffer) {
			// Complete
			text_ItemCount.text = "<color=#FFFA59>!ﻞﯿﻤﮑﺗ</color>";
			text_ItemCount.fontSize = 28;
			text_ItemCount.transform.localPosition = new Vector2 (text_ItemCount.transform.localPosition.x , -0.5F);

			imageItemCountBackHolder.color = colorItemCount_Complete;
			text_ItemEffect.color = colorItemEffect_Complete;
			imageBulletPointHolder_1_2.color = colorItemBullet_Complete;
			imageBulletPointHolder_2_2.color = colorItemBullet_Complete;
			imageBulletPointHolder_1_1.color = colorItemBullet_Complete;

			// Complete back particle
			objParticleItemCounterHolder.SetActive (true);

		} else {
			// NOT Complete
			text_ItemCount.text = intItemCountCurBuffer.ToString () + " <color=red>/</color> " + intItemCountNeedsToComBuffer.ToString ();
			text_ItemCount.fontSize = 36;
			text_ItemCount.transform.localPosition = new Vector2 (text_ItemCount.transform.localPosition.x , -4.5F);

			imageItemCountBackHolder.color = colorItemCount_Normal;
			text_ItemEffect.color = colorItemEffect_Normal;
			imageBulletPointHolder_1_2.color = colorItemBullet_Normal;
			imageBulletPointHolder_2_2.color = colorItemBullet_Normal;
			imageBulletPointHolder_1_1.color = colorItemBullet_Normal;

			// Complete back particle
			objParticleItemCounterHolder.SetActive (false);
		}

		// Setup the visual
		itemDisplayScriptHolder_Phone.SetupAll_PhoneOnly ();

		// For phone enter anim
		if (!isNextButton) {
			Pressed_PhoneEnter ();
		}

		// Reset Scroll for start of phone
		scrollPhoneItemDescripHolder.verticalNormalizedPosition = 0;
	}

	public void SetupDisplay_AllItems () {
//		Debug.LogWarning ("Setup All Items");
//		itemDisplayScriptArr = GetComponentsInChildren<Item_Display_Script> ();

		// Reset Exclamation number to zero
		NotificationCenter.Instance.intExclaimationCount = 0;

		for (int i = 0; i < itemDisplayScriptArr.Length; i++) {
			itemDisplayScriptArr [i].SetupAll ();
		}

		if (isSortable) {
//			objItemsGridHolder.SetActive (false);

			for (int i = 0; i < itemDisplayScriptArr.Length; i++) {
				itemDisplayScriptArr [i].Item_SortCompleteToFront ();
			}

//			objItemsGridHolder.SetActive (true);

			// Get compt for item displays AGAIN
			itemDisplayScriptArr = GetComponentsInChildren<Item_Display_Script> ();
		}


	}

	public void Setup_PhoneEnter () {
		Debug.LogWarning ("PHONE!!");
		objPhoneParentHolder.SetActive (true);
//		StartCoroutine (PhoneActivator (true, 0.1F));
		animPhoneParentHolder.Play ("Menu - Items PHONE Enter Anim 1 (Legacy)");
	}

	public void Setup_PhoneExit () {
		StartCoroutine (PhoneActivator (false, 0.5F));
		animPhoneParentHolder.Play ("Menu - Items PHONE Leave Anim 1 (Legacy)");

		StartCoroutine (Tutorial_ItemRepeatCheck ());
	}

	IEnumerator PhoneActivator (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		objPhoneParentHolder.SetActive (isActive);
	}

	public void Pressed_PhoneEnter () {
		Setup_PhoneEnter ();

        // SOUNDD EFFECTS - Phone Items Enter (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.VoroodMobile.Play();
        }
    }

	public void Pressed_PhoneExit () {
		// Need to check for Item Repeat tutorial

		Setup_PhoneExit ();

        // SOUNDD EFFECTS - Phone Items Enter (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.VoroodMobile.Play();
        }
    }

	public void Pressed_PhoneNextButton () {
		itemIsEmpty = true;
		while (itemIsEmpty) {
			itemNumber_ForPhoneNext++;
			if (itemNumber_ForPhoneNext > 35) {
				itemNumber_ForPhoneNext = 0;
			}

			if (itemDisplayScriptArr [itemNumber_ForPhoneNext].thisItem.availabilityState != Item.AvailabilityState.Empty) {
				itemIsEmpty = false;
			}
		}

        // SOUNDD EFFECTS - Tap On Generic Button (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.GenericTap2.Play();
        }

        itemDisplayScriptArr [itemNumber_ForPhoneNext].Pressed_ItemDisplayNextButton ();

		// Reset Items description scroll
		scrollPhoneItemDescripHolder.verticalNormalizedPosition = 0;
	}

	// TODO: For New Items:

	// Content (Items Scroll Horiz) INCREASE
	// Items Menu Carpet Parent INCREASE (+ so DECREASE FOR NEGATIVE) BY HALF OF ABOVE

	public void Pressed_PhonePrevButton () {
		itemIsEmpty = true;
		while (itemIsEmpty) {
			itemNumber_ForPhoneNext--;
			if (itemNumber_ForPhoneNext < 0) {
				itemNumber_ForPhoneNext = 36;
			}

			if (itemDisplayScriptArr [itemNumber_ForPhoneNext].thisItem.availabilityState != Item.AvailabilityState.Empty) {
				itemIsEmpty = false;
			}
		}

        // SOUNDD EFFECTS - Tap On Generic Button (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.GenericTap2.Play();
        }

        itemDisplayScriptArr [itemNumber_ForPhoneNext].Pressed_ItemDisplayNextButton ();

		// Reset Items description scroll
		scrollPhoneItemDescripHolder.verticalNormalizedPosition = 0;
	}

	public void Pressed_BuyEgg (int whichEgg) {
		NotificationCenter.Instance.isInsideItemMenu = true;

		switch (whichEgg) {
		case 0:
			if (EggChest_PriceCheck (EGG_PRICE_1_KOOCHOOL)) {
				StartCoroutine (EggClickBlocker_Change (true, 0));
				StartCoroutine (EggClick_CallReward (Reward_Machine_Script.ChestArtType.Koochool, 1.2F));
				EggAnimate_Select (0);
			}
			break;
		case 1:
			if (EggChest_PriceCheck (EGG_PRICE_2_AGHDASI)) {
				StartCoroutine (EggClickBlocker_Change (true, 0));
				StartCoroutine (EggClick_CallReward (Reward_Machine_Script.ChestArtType.Aghdasi, 1.2F));
				EggAnimate_Select (1);
			}
			break;
		case 2:
			if (EggChest_PriceCheck (EGG_PRICE_3_MOSHTAN)) {
				StartCoroutine (EggClickBlocker_Change (true, 0));
				StartCoroutine (EggClick_CallReward (Reward_Machine_Script.ChestArtType.Moshtan, 1.2F));
				EggAnimate_Select (2);

//				rewardMachineScriptHolder.GetReward_InMenu (Reward_Machine_Script.ChestArtType.Moshtan);

				// Achievement for moshtan egg
				if (AchievementManager.Instance != null) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_EggBuy_Mosthan ());
				}
			}
			break;
		default:
			break;
		}

		// Analytics for egg purcahse
		Analytics_EggBuy (whichEgg);

		eggNumber_ToReturn = whichEgg;
	}

	void Analytics_EggBuy(int whichEggInt) {
		if (PlayerData_Main.Instance != null) {
			switch (whichEggInt) {
			case 0:
				Answers.LogCustom (
					"Silver Chest Purchased",
					customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
						{ "Mustache", PlayerData_Main.Instance.player_Mustache }
					}
				);
				break;
			case 1:
				Answers.LogCustom (
					"Golden Chest Purchased",
					customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
						{ "Mustache", PlayerData_Main.Instance.player_Mustache }
					}
				);
				break;
			case 2:
				Answers.LogCustom (
					"Unique Chest Purchased",
					customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
						{ "Mustache", PlayerData_Main.Instance.player_Mustache }
					}
				);
				break;
			default:
				break;
			}
		}
	}

	public bool EggChest_PriceCheck (int whatPrice) {
		if (PlayerData_Main.Instance.player_Mustache >= whatPrice) {
			PlayerData_Main.Instance.Player_Mustache_Add (-whatPrice);
			menuMainScriptHolder.Menu_HUD_Update ();

			Debug.LogWarning ("Buy Egg!");

			// SOUNDD EFFECTS - Buy egg (done)
			if (SoundManager.Instance!= null)
			{
				SoundManager.Instance.SoudeTokhmeMorgh.Play();
			}

			return true;
		} else {
			mustacheDisplayScriptHolder.Mustache_TellGoShop ();
			Debug.LogError ("Show Shop");
			return false;
		}
	}

	public void Pressed_EndOfEggBuy () {
		StartCoroutine (EggClickBlocker_Change (false, 1));
		Invoke ("ReturnEggAnimate_Now", 0.5F);
	}

	public void ReturnEggAnimate_Now () {
		EggAnimate_Return (eggNumber_ToReturn);
	}

	public void EggAnimate_Select (int whichAnim) {
		particleEggSelectArr [whichAnim].Play ();
		animEggParentsArr [whichAnim].Play ("Menu - Items EGG - Any Select Anim 1 (Legacy)");
//		animCamTwirlHolder.clip = animClipTwirlArr [whichAnim];
//		animCamTwirlHolder.Play ();
	}

	public void EggAnimate_Return (int whichAnim) {
		animEggParentsArr [whichAnim].Play ("Menu - Items EGG - Return From Select Anim 1 (Legacy)");
	}

	public void Pressed_ItemQuestion_OLD (int whichQuestion) {
		Debug.LogWarning ("QUESTION!!!");
	}

	public IEnumerator EggClickBlocker_Change (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		objEggSelectBlockerHolder.gameObject.SetActive (isActive);
	}
		
	public IEnumerator EggClick_CallReward (Reward_Machine_Script.ChestArtType whichChest, float delay) {
		yield return new WaitForSeconds (delay);
		rewardMachineScriptHolder.GetReward_InMenu (whichChest);
		rewardMachineScriptHolder.gameObject.SetActive (true);
		rewardMachineScriptHolder.CallFirstChest ();
	}

	public void Pressed_ItemsEmpty () {
		for (int i = 0; i < PlayerData_Main.Instance.player_ItemsAmount.Count; i++) {
			PlayerData_Main.Instance.player_ItemsAmount[i].CurAmount = 0;
			PlayerData_Main.Instance.player_ItemsAmount[i].availabityState = Item.AvailabilityState.Empty;
			PlayerData_Main.Instance.player_ItemsAmount[i].lastAvailablityState = Item.AvailabilityState.Empty;
			PlayerData_Main.Instance.player_ItemsAmount[i].itemExclamationState = false;
		}

		SaveLoad.Save ();

		Debug.Log ("EMPTIED ITEMS!");
	}

	public void Pressed_ItemsGive () {
		// Using Items Current Total instead of Amount of itemwithamout[] because the latter is 60 while the former is the lower, correct one
		for (int i = 0; i < PlayerData_Main.Instance.ItemsCurrentTotal; i++) {
//			Debug.LogError (PlayerData_Main.Instance.player_ItemsAmount [i].ItemID 
//				+ " Item DB = " + itemDB.FindItemByItemId (137).needsToComplete);

			if (PlayerData_Main.Instance.player_ItemsAmount [i].CurAmount
			    < itemDB.FindItemByItemId (PlayerData_Main.Instance.player_ItemsAmount [i].ItemID).needsToComplete)
			{
				PlayerData_Main.Instance.player_ItemsAmount[i].CurAmount++;
			}

			if (PlayerData_Main.Instance.player_ItemsAmount [i].CurAmount
			    < itemDB.FindItemByItemId (PlayerData_Main.Instance.player_ItemsAmount [i].ItemID).needsToComplete) {
				PlayerData_Main.Instance.player_ItemsAmount [i].availabityState = Item.AvailabilityState.Empty;
				PlayerData_Main.Instance.player_ItemsAmount [i].lastAvailablityState = Item.AvailabilityState.Incomplete;
			} else {
				PlayerData_Main.Instance.player_ItemsAmount [i].availabityState = Item.AvailabilityState.Incomplete;
				PlayerData_Main.Instance.player_ItemsAmount [i].lastAvailablityState = Item.AvailabilityState.Complete;
			}
			PlayerData_Main.Instance.player_ItemsAmount[i].itemExclamationState = true;
		}

		this.gameObject.SetActive (true);
		SetupDisplay_AllItems ();
		this.gameObject.SetActive (false);

		SaveLoad.Save ();

		Debug.Log ("GAVE ITEMS!");
	}

	public void ScrollRect_Reset () {
		scrollItemsHolder.horizontalNormalizedPosition = 0;
		ScrollRect_DeActivate ();
	}

	public void ScrollRect_Activate () {
		scrollItemsHolder.enabled = true;
	}

	public void ScrollRect_DeActivate () {
		scrollItemsHolder.enabled = false;
	}

	public void AnimEvent_MoveInItems () {
		ScrollRect_DeActivate ();
		Phone_Stats_Script.Instance.startOfShortCutWhat = Phone_Stats_Script.StartOfShortCutType.Phone_ItemMenu;

		AnimEvent_CanvasEnable ();
	}

	public void MoveOut_Items () {
		transform.localPosition = new Vector2 (3200, 0);
	}

	public void MoveOut_ItemsAll () {
		transform.localPosition = new Vector2 (3200, 0);

		menuMainScriptHolder.ZoomOut_OnlyMenuActivator ();

		AnimEvent_CanvasDisable ();
	}

	// This is for the Item PROGRESS Tutorial only
	public void ItemProgress_FrontOverActivator () {
		StartCoroutine (ItemProgress_FrontOverActivator_NOW ());
	}

	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ItemProgress_FrontOverActivator_NOW () {
		
//		Debug.LogError ("BEFORE");
		buttonItemProgress_FrontOverHolder.gameObject.SetActive (true);
		buttonItemProgress_FrontOverHolder.interactable = false;

		// Set Scroll to Start / Reset for Tutorial
		ScrollRect_ItemProgress_CheckStart ();

		yield return null;
	}

	public IEnumerator ItemProgress_FrontOverActivator_NOW2 () {
		yield return new WaitForSeconds (0.2F);
//		Debug.LogError ("AFTER 1");

		if (buttonItemProgress_FrontOverHolder.interactable != true) {
			isAutoScrolling_1 = false;
			isAutoScrolling_2 = false;

//			Debug.LogError ("AFTER 2");
			buttonItemProgress_FrontOverHolder.interactable = true;
			scrollItemsHolder.horizontalNormalizedPosition = floatAutoScrollTarget;
		}
	}

	// This is for the Item PROGRESS Tutorial only
	public void ScrollRect_ItemProgress_CheckStart () {
		scrollItemsHolder.horizontalNormalizedPosition = 0.01F;
		StartCoroutine (ScrollRect_ItemProgress_BlendStart_LR ());
	}

	// This is for the Item PROGRESS Tutorial only
//	public void ScrollRect_ItemProgress_CheckStartOLD () {
//		if (scrollItemsHolder.horizontalNormalizedPosition < 0.045) {
//			Debug.LogWarning ("LR");
//			StartCoroutine (ScrollRect_ItemProgress_BlendStart_LR ());
//		} else {
//			Debug.LogWarning ("RL");
//			StartCoroutine (ScrollRect_ItemProgress_BlendStart_RL ());
//		}
//	}

	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ScrollRect_ItemProgress_BlendStart_LR () {
		// Start failsafe
		StartCoroutine (crollRect_ItemProgress_BlendEbd_FailSafe ());

		isAutoScrolling_1 = true;
		while (isAutoScrolling_1) {
			StartCoroutine (ScrollRect_ItemProgress_BlendOnGoing1 ());
			yield return new WaitForSeconds (0.01F);
		}

		isAutoScrolling_2 = true;
		while (isAutoScrolling_2) {
			StartCoroutine (ScrollRect_ItemProgress_BlendOnGoing2 ());
			yield return new WaitForSeconds (0.01F);
		}

		StartCoroutine (ItemProgress_FrontOverActivator_NOW2 ());
	}

	// To make sure the tut auto scroll doesn't produce a bug
	public IEnumerator crollRect_ItemProgress_BlendEbd_FailSafe () {
		yield return new WaitForSeconds (2);
		Debug.LogError ("FAIL SAFE!");
		StartCoroutine (ItemProgress_FrontOverActivator_NOW2 ());
	}

	// This is for the Item PROGRESS Tutorial only
//	public IEnumerator ScrollRect_ItemProgress_BlendStart_RL () {
//		isAutoScrolling_1 = true;
//		while (isAutoScrolling_1) {
//			StartCoroutine (ScrollRect_ItemProgress_BlendOnGoing3 ());
//			yield return new WaitForSeconds (0.004F);
//		}
//
//		isAutoScrolling_2 = true;
//		while (isAutoScrolling_2) {
//			StartCoroutine (ScrollRect_ItemProgress_BlendOnGoing4 ());
//			yield return new WaitForSeconds (0.002F);
//		}
//
//		StartCoroutine (ItemProgress_FrontOverActivator_NOW2 ());
//	}

	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ScrollRect_ItemProgress_BlendOnGoing1 () {
		scrollItemsHolder.horizontalNormalizedPosition += 0.003F;
		if (scrollItemsHolder.horizontalNormalizedPosition > 0.08) {
			isAutoScrolling_1 = false;
		}
		yield return null;
	}

	// TODO: Always updated the final destination for scroll (Now is 0.0375)

	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ScrollRect_ItemProgress_BlendOnGoing2 () {
		scrollItemsHolder.horizontalNormalizedPosition -= 0.002F;
		if (scrollItemsHolder.horizontalNormalizedPosition < floatAutoScrollPreTarget) {
			scrollItemsHolder.horizontalNormalizedPosition = floatAutoScrollTarget;
			isAutoScrolling_2 = false;
		}
		yield return null;
	}

	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ScrollRect_ItemProgress_BlendOnGoing3 () {
		scrollItemsHolder.horizontalNormalizedPosition -= 0.001F;
		if (scrollItemsHolder.horizontalNormalizedPosition < 0.02) {
			isAutoScrolling_1 = false;
		}
		yield return null;
	}
		
	// This is for the Item PROGRESS Tutorial only
	public IEnumerator ScrollRect_ItemProgress_BlendOnGoing4 () {
		scrollItemsHolder.horizontalNormalizedPosition += 0.0005F;
		if (scrollItemsHolder.horizontalNormalizedPosition > floatAutoScrollPreTarget) {
			scrollItemsHolder.horizontalNormalizedPosition = floatAutoScrollTarget;
			isAutoScrolling_2 = false;
		}
		yield return null;
	}

	// This is for the Item PROGRESS Tutorial only
	public void Pressed_ItemProgress_FrontOverButton () {
		buttonItemProgressHolder.onClick.Invoke ();
		ScrollRect_Activate ();
		StartCoroutine (ItemProgresss_Tooltip ());

		// New place of TutSeen_BatteryCharge
		PlayerData_Main.Instance.TutSeen_ItemProgress = true;

		// Save after tut was turned true
		SaveLoad.Save ();
	}

	// This is for Item PROGRESS Tutorial only
	public IEnumerator ItemProgresss_Tooltip () {
		yield return new WaitForSeconds (0.4F);
		objTooltipItemProgHolder.SetActive (true);
	}

	// This is for the Item REPEAT Tutorial only
	public void ItemRepeatItem_FrontOverActivator () {
		StartCoroutine (ItemRepeatItem_FrontOverActivator_NOW ());
	}

	// This is for the Item REPEAT Tutorial only
	public IEnumerator ItemRepeatItem_FrontOverActivator_NOW () {
		buttonItemRepeatItem_FrontOverHolder.gameObject.SetActive (true);
		buttonItemRepeatItem_FrontOverHolder.interactable = false;

		Debug.LogError ("BEFORE");
		yield return new WaitForSeconds (1);
		Debug.LogError ("AFTER");

		buttonItemRepeatItem_FrontOverHolder.interactable = true;
	}

	// This is for the Item REPEAT Tutorial only
	public void Pressed_ItemRepeatItem_FrontOverButton () {
		Debug.LogError ("The End");

//		buttonItemRepeatItemHolder.onClick.Invoke ();

		// New place of TutSeen_BatteryCharge
		PlayerData_Main.Instance.TutSeen_ItemRepeatItem = true;

		// Buy Egg Free
		PlayerData_Main.Instance.Player_Mustache_Add (EGG_PRICE_1_KOOCHOOL);
		Pressed_BuyEgg (0);

		// Save after last menu tutorial so far (End of repeat item)
		SaveLoad.Save ();
	}

	public IEnumerator Tutorial_ItemRepeatCheck () {
		menuTutCheckScriptHolder.Menu_TutorialCheck_OnlyItemRepeat ();
		yield return null;
	}

	public void AnimEvent_CanvasEnable () {
		StartCoroutine (Items_CanvasEnable ());
	}

	public void AnimEvent_CanvasDisable () {
		canvasItemsHolder.enabled = false;
//		graphRayCasterHolder.enabled = false;
		//		StartCoroutine (World_CanvasDisable ());
	}

	IEnumerator Items_CanvasEnable () {
		yield return null;
		canvasItemsHolder.enabled = true;
//		graphRayCasterHolder.enabled = true;
	}

	IEnumerator Items_CanvasDisable () {
		yield return null;
		canvasItemsHolder.enabled = false;
//		graphRayCasterHolder.enabled = false;
	}

//	void Update () {
//		Debug.LogError ("Scroll normalized = " + scrollItemsHolder.horizontalNormalizedPosition);
//	}
}
