﻿using UnityEngine;
using System.Collections;

public class Key_Script : MonoBehaviour {

	public Menu_Main_Script menuMainScriptHolder;

	public GameObject keyObjHolder_ON;
//	public GameObject keyObjHolder_OFF;

//	public GameObject objKeyParticleShineHolder;
//	public GameObject objKeyParticleShineBigHolder;

	public Animation animKeyIdle;
	public Animation animKeyEnter;
	public Animation animKeyFreeHolder;

//	void Awake () {
//		objKeyParticleShineHolder.SetActive (false);
//		objKeyParticleShineBigHolder.SetActive (false);
//	}

	public void KeyActive (bool whichBool) {
//		keyObjHolder_ON.transform.localPosition = new Vector3 (640, 0, 0);
		keyObjHolder_ON.transform.localScale = new Vector3 (0, 0, 0);

		keyObjHolder_ON.SetActive (whichBool);
		if (whichBool) {
			KeyAnim_Reset ();
			Invoke ("KeyAnim_Enter", 0.4F);
			Invoke ("KeyAnim_Idle", 1F);
//			KeyAnim_Idle ();
		}
	}

	public void Key_UseAnim () {
		KeyAnim_Reset ();
		animKeyFreeHolder.Play ("Menu - World KEY Free Anim 1 (Legacy)");
	}

	public void KeyAnim_Reset () {
		animKeyIdle.Stop ();
		animKeyIdle.Play ("Menu - World KEY Reset Anim 1 (Legacy)");
	}

	public void KeyAnim_Idle () {
		animKeyIdle.Play ("Menu - World KEY Idle Anim 1 (Legacy)");
	}

	public void KeyAnim_Enter () {
		animKeyEnter.Play ("Menu - World KEY Enter Anim 2 (Legacy)");
	}

	public void Key_MiniCinematic_End () {
		menuMainScriptHolder.Check_AllMiniCutsceneEnd ();
	}
}
