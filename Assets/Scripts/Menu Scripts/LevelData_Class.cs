﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelData_Class
{
    public LevelData_Class()
    {
        star_Unlocked_Level = false;
        star_Unlocked_Untouchable = false;
        star_Unlocked_Unmissable = false;
    }

    public bool star_Unlocked_Level;
    public bool star_Unlocked_Untouchable;
    public bool star_Unlocked_Unmissable;
}
