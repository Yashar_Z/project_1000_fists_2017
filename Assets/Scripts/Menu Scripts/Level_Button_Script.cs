﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level_Button_Script : MonoBehaviour {

	public int levelNumber;
	public GameObject levelStar1;
	public GameObject levelStar2;
	public GameObject levelStar3;
	public GameObject levelLabel_Normal;
	public GameObject levelLabel_ALLStars_1;
	public GameObject levelLabel_ALLStars_2;
	public GameObject levelCard_Back;
	public GameObject levelCard_Front;
	public GameObject levelCard_NewestLight;

	public Image imageLevelLabelHolder;

//	public Color colorCardHolder_Green;
//	public Color colorCardHolder_Blue;

	public Color[] colorLevelNumberArr;
	public Text text_LevelNumberHolder;

//	public Animator levelAnimatorHolder;
	public AnimationClip[] animClipsLevelArr;
	public Animation animLevelHolder;

	public void LevelStatus_Lock () {
		levelCard_Front.SetActive (false);
		levelCard_Back.SetActive (true);
	}

	public void LevelStatus_Unlock () {
		levelCard_Front.SetActive (true);
		levelCard_Back.SetActive (false);
	}

	public void LevelStatus_Minilock () {
		levelCard_Back.SetActive (true);
		levelCard_Front.transform.localScale = new Vector3 (0, 1, 1);
	}

//	public void LevelStatus_SquashFront () {
//		levelCard_Front.transform.localScale = new Vector3 (0, 1, 1);
//	}

	public void LevelStars_Show (bool star1, bool star2, bool star3) {
		StartCoroutine (LevelStars_ShowNOW (star1, star2, star3));
	}

	public IEnumerator LevelStars_ShowNOW (bool star1, bool star2, bool star3) {
		levelStar1.SetActive (star1);
		levelStar2.SetActive (star2);
		levelStar3.SetActive (star3);

		// Check for all 3 stars
		if (star1 && star2 && star3) {
			levelLabel_ALLStars_1.SetActive (true);
			levelLabel_ALLStars_2.SetActive (true);
		} else {
			levelLabel_ALLStars_1.SetActive (false);
			levelLabel_ALLStars_2.SetActive (false);
		}

		if (!star1) {
			text_LevelNumberHolder.color = colorLevelNumberArr [0];
		} else {
			text_LevelNumberHolder.color = colorLevelNumberArr [1];
		}
		yield return null;
	}

	public void LevelButton_BackGlow (bool whatBool) {
		StartCoroutine (LevelButton_BackGlowNOW (whatBool));
	}

	public IEnumerator LevelButton_BackGlowNOW (bool whatBool) {
		levelCard_NewestLight.SetActive (whatBool);

		// If is NOT newest (else is newest) (Now use front layer color only)
//		if (!whatBool) {
//			imageLevelLabelHolder.color = new Color (0, 0.45F, 0.55F, 1);
//		} else {
//			imageLevelLabelHolder.color = new Color (0, 0.6F, 0.75F, 1);
//		}
		yield return null;
	}

	public void LevelButton_FistHitAnim () {
		GetComponentInParent<Worlds_All_Script> ().Worlds_Anim_CamHit (1);
	}

	public void LevelButton_UnlockAnimEnd_GlowStuff () {
		LevelButton_BackGlow (true);

		// Don't use idle anim for glow anymore (Now use animator inside each card!)
//		PlayLevelAnim_Idle ();

		GetComponentInParent<Menu_Main_Script> ().Check_AllMiniCutsceneEnd ();

		// Resize Card to avoid animation bug (New Card is not full size)
		transform.GetChild(1).transform.localScale = new Vector3 (0.86F, 0.86F, 0.86F);
	}

	public void PlayLevelAnim_Selected () {
		animLevelHolder.Stop ();
		animLevelHolder.clip = animClipsLevelArr[0];
		animLevelHolder.Play ();
//		levelAnimatorHolder.Play("Menu - Level Card Selected Anim 1");
	}

	public void PlayLevelAnim_UnlockLevel () {
        //		Debug.LogWarning ("Playing unlock level!?");

        // SOUNDD EFFECTS - Level Unlock Punch (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.MoshtBeCard.PlayDelayed(0.5f);
        }

		animLevelHolder.Stop ();
		animLevelHolder.clip = animClipsLevelArr [7];
		animLevelHolder.Play ();
	}

	public void PlayLevelAnim_Idle () {
		animLevelHolder.Stop ();
		animLevelHolder.clip = animClipsLevelArr[5];
		animLevelHolder.Play ();
		//		levelAnimatorHolder.Play("Menu - Level Card Selected Anim 1");
	}

	public void PlayLevelAnim_Stop () {
		animLevelHolder.Stop ();
	}
		
	public void PlayLevelAnim_ThrowAway_Left (int whichAnim) {
		animLevelHolder.Stop ();
		animLevelHolder.clip = animClipsLevelArr[whichAnim];
//		animLevelHolder.clip = animClipsLevelArr[Random.Range (1,5)];
		animLevelHolder.Play ();

//		animLevelHolder.Play ("Menu - Level CARD ThrowAway Anim 1 (Legacy)");
//		animLevelHolder.Play ("Menu - Level CARD ThrowAway Anim " + Random.Range (1,5).ToString () + " (Legacy)");
//		levelAnimatorHolder.Play("Menu - Level Card ThrowAway Anim " + Random.Range (1,4));
	}

	public void PlayLevelAnim_ThrowAway_Right (int whichAnim) {
		animLevelHolder.Stop ();
		animLevelHolder.clip = animClipsLevelArr[whichAnim];
//		animLevelHolder.clip = animClipsLevelArr[Random.Range (1,5)];
		animLevelHolder.Play ();

//		animLevelHolder.Play ("Menu - Level CARD ThrowAway Anim 1 (Legacy)");
//		animLevelHolder.Play ("Menu - Level CARD ThrowAway Anim " + Random.Range (1,5).ToString () + " (Legacy)");
//		levelAnimatorHolder.Play("Menu - Level Card ThrowAway Anim " + Random.Range (4,7));
	}

}
