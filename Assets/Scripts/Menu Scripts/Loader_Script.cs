﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using TapsellSDK;

public class Loader_Script : MonoBehaviour {

	public GameObject objBlackBack;
	public AnimationClip animClipFistHolder;
	public Animation animFistHolder;

	public bool canRemoveLoader;

	void Start () {
		StartCoroutine (Start_Loader ());
	}

	IEnumerator Start_Loader () {

		// Wait before load
		yield return new WaitForSeconds (0.1F);

        SaveLoad.Load();

		// For VAS Cheat Build
		if (PlayerData_Main.Instance.isFullBuild_VAS_Cheat) {
			SaveLoad.Load_VAS_CheatOnly();
		}

		// Can't remove loading without being called from menu_mainscript
		canRemoveLoader = false;

//		Debug.LogWarning ("Save path: " + Application.persistentDataPath);
        
		// Check at the loader to see if we should go non-VAS (First is NOT VAS)
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			NormalStartGame ();
		}

		// This is VAS
		else {
			if (PlayerData_Main.Instance.isFullBuild_VAS_Cheat) {
                yield return null;
				NormalStartGame ();
			} else {

				// Disable and stop load fists for VAS
				LoaderFists_DisableStop ();

				StartCoroutine (PrepareForVAScontroller ());
			}
		}
	}

	public void Start_FistHitSound () {
		if (SoundManager.Instance != null) {
			SoundManager.Instance.MoshtCrit1.Play ();
		}
	}

	public void NormalStartGame () {
		StartTheDamnGame ();

		DontDestroyOnLoad (this.gameObject);
	}

	public void LoaderFists_DisableStop() {
//		transform.GetChild (0).gameObject.SetActive (false);
//		transform.GetChild (1).gameObject.SetActive (false);
		animFistHolder [animFistHolder.clip.name].speed = 0;
	}

	public void LoaderFists_EnablePlay () {
//		transform.GetChild (0).gameObject.SetActive (true);
//		transform.GetChild (1).gameObject.SetActive (true);
		animFistHolder [animFistHolder.clip.name].speed = 1;

		NormalStartGame ();
	}

	IEnumerator PrepareForVAScontroller () {
		
		while (VAS_MenuController_Script.Instance == null) {
			yield return new WaitForSeconds (0.1F);
		}

		// Moved to INSIDE the check
//		VAS_MenuController_Script.Instance.VAS_Controller_Activator (true);

		// Activate first menu (Log in)
		yield return new WaitForSeconds (1.5F);

		// First we should check to see if save file for phone number and user id and access token exists or not. If not, THEN continue to next line. ELSE, go to game.
		VAS_MenuController_Script.Instance.Start_VAS_Check ();

//		VAS_MenuController_Script.Instance.PopUp_VAS_OpenThis (0);
	}

	public void StartTheDamnGame () {

		SceneManager.LoadSceneAsync ("Menu_Scene");
	}

	public void DestroyTheDamnLoader () {
		StartCoroutine (DestroyTheDamnLoaderDelayed ());
	}

	public void HideTheDamnBlackBack () {
		objBlackBack.SetActive (false);
		animFistHolder [animClipFistHolder.name].speed = 7;

		// Now CAN remove loading without being called from menu_mainscript
		canRemoveLoader = true;

		// Allow Achievement tiles to appear (In order to prevent many tiles appearing at ONCE)
//		LoadingAllowAchievementTiles ();
	}

	public void HideTheDamnBlackBack_SpeedPartOnly () {
		animFistHolder [animClipFistHolder.name].speed = 7;

		// Allow Achievement tiles to appear (In order to prevent many tiles appearing at ONCE)
//		LoadingAllowAchievementTiles ();
	}

//	public void LoadingAllowAchievementTiles () {
//		StartCoroutine (PlayerData_Main.Instance.AllowAchievementTiles ());
//	}

	public void HideTheDamnBlackBack_BackPartOnly () {
		objBlackBack.SetActive (false);

		// Now CAN remove loading without being called from menu_mainscript
		canRemoveLoader = true;
	}

	public IEnumerator DestroyTheDamnLoaderDelayed () {
		while (!canRemoveLoader) {
			yield return new WaitForSeconds (1);
		}

		Destroy (this.gameObject);
	}
}
