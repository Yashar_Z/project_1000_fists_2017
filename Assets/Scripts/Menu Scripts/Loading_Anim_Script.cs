﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Fabric.Answers;

public class Loading_Anim_Script : MonoBehaviour {

	public static Loading_Anim_Script Instance;

	public ParticleSystem particleMustacheLoadingHolder;
	public Text loading_WorldNumberText;
	public Text loading_LevelNumberText;

	public Text loading_HintText;

	public Canvas canvasLoadingHolder;

	[TextArea]
	public string[] hintStringsArr;

	public GameObject loading_BlackScreenObjHolder;

	public Animation loadingAnimHolder;
//	public ParticleSystem particleLoadCircleRingHolder;

	private int intLoadingNumber;

	public void Awake () {
		Instance = this;
		Loading_Clear ();
		loading_BlackScreenObjHolder.SetActive (false);

		// 
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		// 
		MoveIn_LoadingAnims ();
	}

	public void MoveIn_LoadingAnims () {
		this.transform.localPosition = new Vector2 (0, 0);
		canvasLoadingHolder.enabled = true;
	}

	public void MoveOut_LoadingAnims () {
		this.transform.localPosition = new Vector2 (200, 0);
		canvasLoadingHolder.enabled = false;
	}

	public IEnumerator Loading_TextsUpdate () {
		if (PlayerData_Main.Instance.gameMainSpawnType == PlayerData_InGame.GameSpawnType.Campaign) {
			loading_WorldNumberText.text = PlayerData_Main.Instance.WorldNumber_FullName.ToString ();
			loading_LevelNumberText.text = PlayerData_Main.Instance.LevelNumber_FullName.ToString ();
		} else {
			loading_WorldNumberText.text = "ﺩﺭﻮـــــــــــــﮐﺭ";
			loading_LevelNumberText.text = "!ﻥﺎﺘــــــﺸـــﻣ";
		}
		yield return null;
	}

	public IEnumerator LoadingAnim_Start (float blackScreenDelay) {
		// Canvas and trans move in
		MoveIn_LoadingAnims ();

		// Hint Text
		Loading_Hint ();

		// Loading Black Back screen (START)
		StartCoroutine(LoadingBlackBack(true, blackScreenDelay));

		// Mustache Particle Start
		StartCoroutine(LoadingMustache_Particle (true));

        // SOUNDD EFFECTS - Loading start sound (done)
        if (SoundManager.Instance!= null)
        {
            if (MusicManager.Instance!= null)
            {
                MusicManager.Instance.StopMusic();
            }
            SoundManager.Instance.LoadingSound.loop = true;
            SoundManager.Instance.LoadingSound.Play();
        }

		loadingAnimHolder.Play ("Menu - Loading START Anim 1 (Legacy)");
		yield return null;
	}

	public IEnumerator LoadingAnim_Loop () {
		loadingAnimHolder.Play ();
		yield return null;
	}

	public IEnumerator LoadingBlackBack (bool isActive, float delay) {
		yield return new WaitForSeconds (delay);
		loading_BlackScreenObjHolder.SetActive (isActive);
	}

	public void LoadingBlackBack_Instant (bool isActive) {
		loading_BlackScreenObjHolder.SetActive (isActive);
	}

	public IEnumerator LoadingAnim_End_ToLevel () {
		yield return new WaitForSeconds (0.2F);

		// Loading Black Back screen (END)
		StartCoroutine(LoadingBlackBack(false, 0.25F));

		// Mustache Particle Stop
		StartCoroutine(LoadingMustache_Particle (false));

        // SOUNDD EFFECTS - Loading has ended and level titles are displayed (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.LoadingSound.Stop();
            SoundManager.Instance.LoadingSound.loop = false;
            SoundManager.Instance.LoadingSound.Play();
        }


		loadingAnimHolder.Play ("Menu - Loading END To Level Anim 1 (Legacy)");
//		particleLoadCircleRingHolder.Emit (1);
	}

	public IEnumerator LoadingMustache_Particle (bool shouldPlay) {
		if (shouldPlay) {
//			Debug.Log ("Yo 1?");
			yield return new WaitForSeconds (0);
			particleMustacheLoadingHolder.Play ();
		} else {
//			Debug.Log ("Yo 2?");
			yield return new WaitForSeconds (1);
			particleMustacheLoadingHolder.Stop ();
		}
	}

	public IEnumerator LoadingAnim_End_ToMenu () {
		// Loading Black Back screen (END)
		LoadingBlackBack_Instant (false);
//		StartCoroutine(LoadingBlackBack(false, 0));

        // SOUNDD EFFECTS - Loading has ended and menu has opened (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.LoadingSound.Stop();
        }

		loadingAnimHolder.Play ("Menu - Loading END To Menu Anim 1 (Legacy)");
//		particleLoadCircleRingHolder.Emit (1);
		yield return null;
	}

	public void Loading_StartInGame () {
		// New end of loading, level start Setuf
		if (PlayerData_InGame.Instance != null) {
			PlayerData_InGame.Instance.Red_HUDIntroDelay ();
			PlayerData_InGame.Instance.Ref_SpawnerSetup ();
		}

        // ANALYCTICS EVENT - start level
        Answers.LogLevelStart(PlayerData_Main.Instance.player_WorldsUnlocked.ToString() + "-" + PlayerData_Main.Instance.player_LevelsUnlocked.ToString());

		// Check for campaign or endless loading world opener
		if (PlayerData_Main.Instance.gameMainSpawnType == PlayerData_InGame.GameSpawnType.Campaign) {

			// SOUNDD EFFECTS - WORLD MUSICS (done)
			switch (PlayerData_Main.Instance.WorldNumber) {
			case 1:
				PlayerData_InGame.Instance.worldsInGameScriptHolder.world1_MoverHolder.W1_IntroAnims_Delayer ();

				// For first time play, don't start with music
//				if ((MusicManager.Instance != null))

				// This is in case we want music to NOT start
				if ((MusicManager.Instance != null) && !PlayerData_Main.Instance.isFirstTimePlaying) {
					MusicManager.Instance.PlayMusic_WorldCinema ();
				} else {
					Debug.LogError ("Vol = " + AudioListener.volume);
					MusicManager.Instance.audioSourceArr [0].volume = 0.6F;
					MusicManager.Instance.PlayMusic_Menu ();
				}
				
                //			FindObjectOfType<World1_Mover> ().W1_IntroAnims_Delayer ();
				break;
			case 2:
				PlayerData_InGame.Instance.worldsInGameScriptHolder.world2_MoverHolder.W2_IntroAnims_Delayer ();
				if (MusicManager.Instance != null) {
					MusicManager.Instance.PlayMusic_WorldGarage ();
				}
                //			FindObjectOfType<World2_Mover> ().W2_IntroAnims_Delayer ();
				break;
			case 3:
				PlayerData_InGame.Instance.worldsInGameScriptHolder.world3_MoverHolder.W3_IntroAnims_Delayer ();
				if (MusicManager.Instance != null) {
					MusicManager.Instance.PlayMusic_WorldGrandHotel ();
				}
                //			FindObjectOfType<World3_Mover> ().W3_IntroAnims_Delayer ();
				break;
			case 4:
				PlayerData_InGame.Instance.worldsInGameScriptHolder.world4_MoverHolder.W4_IntroAnims_Delayer ();
				if (MusicManager.Instance != null) {
					MusicManager.Instance.PlayMusic_WorldCinemaNight ();
				}
//			FindObjectOfType<World4_Mover> ().W4_IntroAnims_Delayer ();
				break;

			// Case 20 was for ENDLESS
//			case 20:
			//			FindObjectOfType<World1_Mover> ().W1_IntroAnims_Delayer ();
				break;
			default:
				Debug.LogError ("Loading Anim Script, World Loader DEFAULT");
				break;
			}
		} 

		// Endless. No need for case!
		else {
			PlayerData_InGame.Instance.worldsInGameScriptHolder.world1_MoverHolder.W1_IntroAnims_Delayer ();

			if (MusicManager.Instance != null) {
				MusicManager.Instance.PlayMusic_WorldCinemaNight ();
			} 
		}
	}

	public void Loading_Clear () {
		for (int i = 0; i < 4; i++) {
			transform.GetChild (i).gameObject.SetActive (false);
		}
	}

	public void Loading_Hint () {
		intLoadingNumber = Random.Range (0, 26);
		loading_HintText.text = hintStringsArr [intLoadingNumber] + "<size=27> <color=#ffffffff> :" + (intLoadingNumber + 1) + " ﯽﯾﺎﻤﻨﻫﺍﺭ</color> </size>";

		if (PlayerData_Main.Instance.isFirstTimePlaying) {
			loading_HintText.text = "...ﺪﯿﺷﺎﺑ ﺮﻈﺘﻨﻣ ﺎﻔﻄﻟ";
		}

		// For testing hints
//		Invoke ("LoadingHint_StartCycles", 1);
	}

	// SOUND EFFECTS: Sound of level and world name hit in loading (Done)
	public void Loading_NameSound () {
		if (SoundManager.Instance != null) {
			SoundManager.Instance.StartLevelNameShow.Play ();
		}
	}

	public void LoadingHint_StartCycles () {
//		Debug.LogError ("Start of hint cycles");
		intLoadingNumber = 0;
		StartCoroutine (LoadingHint_Cycler ());
	}

	public IEnumerator LoadingHint_Cycler () {
		yield return new WaitForSeconds (0.02F);
		intLoadingNumber++;
		loading_HintText.text = hintStringsArr [intLoadingNumber] + "<size=27> <color=#ffffffff> :" + (intLoadingNumber + 1) + " ﯽﯾﺎﻤﻨﻫﺍﺭ</color> </size>";
		if (intLoadingNumber < 26) {
			StartCoroutine (LoadingHint_Cycler ());
		}
	}

}
