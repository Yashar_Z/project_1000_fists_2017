﻿using UnityEngine;
using System.Collections;

public class Menu_BackAnims_Script : MonoBehaviour {

	[Header ("")]
	public Animation animBodyTitleHolder;
	public Animation animBodyEyeBrowsHolder;
	public Animation animBodyArtActualHolder;
	public Animation animMainAndBodyMoverHolder;

	public Animation animButtonHolder_Items;
	public Animation animButtonHolder_Shop;
	public Animation animButtonHolder_Play;
	public Animation animButtonHolder_Endless;
	public Animation animButtonHolder_Events;

	public Animation animButtonsHolder_ALLParent;

	public AnimationClip[] animClipEndlessButtonArr;
	public AnimationClip[] animClipEventButtonArr;

	public bool isFront_Worlds;
	public bool isFront_Endless;
	public bool isFront_Settings;
	public bool isFront_Shop;
	public bool isFront_Items;
	public bool isFront_Achieves;
	public bool isFront_VidGift;
	// TODO Future Bool
	public bool isFront_LeaderB;

	public GameObject objTooltipEndlessLocked;

	[Header ("For New Menu Button Press")]
	public Menu_Main_Script menuMainScriptHolder;
	public Endless_All_Script endlessAllScriptHolder;
	public Popup_All_Script popUpAllScroptHolder_Items;
	public Popup_All_Script popUpAllScroptHolder_Shop;
	public Popup_All_Script popUpAllScroptHolder_Settings;
	public Popup_All_Script popUpAllScroptHolder_VidGift;
	public Popup_All_Script popUpAllScroptHolder_Achieves;
	public Popup_Settings_Script popUpSettingsScriptHolder;
	public Popup_Achieve_Script popUpAchieveScriptHolder;
	public Event_Menu_Script eventMenuScriptHolder;

	//
	public Transform transWordsMenuHolder;
	public Transform transEndlessHolder;
	public Transform transSettingsMenuHolder;
	public Transform transShopMenuHolder;
	public Transform transItemsMenuHolder;
	public Transform transAchievesMenuHolder;
	public Transform transVidGiftMenuHolder;

	public ParticleSystem particleEndlessButtonHolder;
	public ParticleSystem particleEndlessIdleHolder;

	[Header ("")]
	public Menu_Backdrop_Script[] menuBackdropScriptArr;
	public Menu_Backdrop_Script[] menuBackdropShadowsScriptArr;

	private float floatWaitBeforeMoveIn = 0.04F;

	// For VidGift unavailable / time left tooltip (No straight from vidgift script reference to here)
//	public GameObject objTooltipVidGiftHolder;

	public void Pressed_Menu_Items () {
		StartCoroutine (Pressed_Menu_ItemsRoutine ());
	}

	public IEnumerator Pressed_Menu_ItemsRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		// Main Button Press Resize
		animButtonHolder_Items.Play ();

		isFront_Items = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}

		menuMainScriptHolder.menuTutCheckScriptHolder.Menu_TutorialCheck_OnlyItemRepeat ();

		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transItemsMenuHolder);
		popUpAllScroptHolder_Items.PopUp_AnimPlay (0);
	}

	public void Pressed_Menu_Shop () {
		StartCoroutine (Pressed_Menu_ShopRoutine ());
    }

	public IEnumerator Pressed_Menu_ShopRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		// Main Button Press Resize
		animButtonHolder_Shop.Play ();

		isFront_Shop = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}
			
		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transShopMenuHolder);
		popUpAllScroptHolder_Shop.PopUp_AnimPlay (0);
	}

	public void Pressed_Menu_Play () {
		StartCoroutine (Pressed_Menu_PlayRoutine ());
    }

	public IEnumerator Pressed_Menu_PlayRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		// Main Button Press Resize
		animButtonHolder_Play.Play ();

		isFront_Worlds = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}
			
		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transWordsMenuHolder);
		menuMainScriptHolder.Pressed_PlayToWorlds ();
	}

	public void Pressed_Menu_Endless () {
		if (!menuMainScriptHolder.menuTutCheckScriptHolder.canNotClickEndless) {
			particleEndlessButtonHolder.Play ();

			if (Endless_UnlockedCheck ()) {
				StartCoroutine (Pressed_Menu_EndlessRoutine ());

				// Deactive tut in case it was active
				if (menuMainScriptHolder.menuTutCheckScriptHolder.wasEndlessTutCalled) {
					Endless_AfterButtonUnlocked ();
				}
			} else {
				objTooltipEndlessLocked.SetActive (true);
			}
		} 

		// Meaning tutorial has stopped it
		else {
			if (!menuMainScriptHolder.menuTutCheckScriptHolder.isUnlockingEndless) {
				particleEndlessButtonHolder.Play ();

				// Main Button Press Resize
				animButtonHolder_Endless.clip = animClipEndlessButtonArr [1];
				animButtonHolder_Endless.Play ();

				StartCoroutine (EndlessAvailableRoutine ());

				// This makes sure the unlock animation is not played again after the first press
				menuMainScriptHolder.menuTutCheckScriptHolder.isUnlockingEndless = true;

				// Tick to allow tutorial be deactivated upon opening endless menu
				menuMainScriptHolder.menuTutCheckScriptHolder.wasEndlessTutCalled = true;
			}
		}
	}

	public void Endless_AfterButtonUnlocked () {
		particleEndlessIdleHolder.Stop ();
		particleEndlessIdleHolder.gameObject.SetActive (false);

		// Play next story dialogue (Don't use it yet)
//		menuMainScriptHolder.menuTutCheckScriptHolder.storyMainScriptHolder.StartStory (1, Balance_Constants_Script.TutLevel_EndlessUnlock, 1, false);

		// Force Player to Press Play
		menuMainScriptHolder.menuTutCheckScriptHolder.storyMainScriptHolder
			.storyPostScriptHolder.PostStory_Event_Start (StoryMain_Script.PostEventType.Into_EndlessShowPlay);

//		Moshtan_Utilties_Script.ShowToast ("Save NOW!");
//		Debug.LogError ("Endless tut seen!");
	}

	public void Endless_AfterPlayPressFirstTime () {
		menuMainScriptHolder.menuTutCheckScriptHolder.storyMainScriptHolder.storyPostScriptHolder.tutBlackScriptHolder.TutBlack_ObjectsChange_DeActivate ();
		menuMainScriptHolder.menuTutCheckScriptHolder.wasEndlessTutCalled = false;

		// These only happen at the end of ALL tuts
		PlayerData_Main.Instance.TutSeen_EndlessUnlock = true;
		SaveLoad.Save ();
	}

	public IEnumerator EndlessAvailableRoutine () {
		yield return new WaitForSeconds (3);
		menuMainScriptHolder.menuTutCheckScriptHolder.canNotClickEndless = false;
	}

	public bool Endless_UnlockedCheck () {
		if ((PlayerData_Main.Instance.player_WorldsUnlocked > 1)
			|| PlayerData_Main.Instance.player_LevelsUnlocked >= menuMainScriptHolder.menuTutCheckScriptHolder.tutorialLevel_EndlessUnlock) {
			return true;
		} 
		else {
			return false;
		}
	}

	public IEnumerator Pressed_Menu_EndlessRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		// Main Button Press Resize
		animButtonHolder_Endless.clip = animClipEndlessButtonArr[0];
		animButtonHolder_Endless.Play ();

		isFront_Endless = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}

		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transEndlessHolder);
		endlessAllScriptHolder.MoveIn_Endless ();
	}

	public void Pressed_Menu_Settings () {
		StartCoroutine (Pressed_Menu_SettingsRoutine ());
    }

	public IEnumerator Pressed_Menu_SettingsRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		isFront_Settings = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}

		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transSettingsMenuHolder);
		popUpSettingsScriptHolder.Settings_MoveIn ();
//		popUpAllScroptHolder_Settings.PopUp_AnimPlay (0);
	}

	public void Pressed_Menu_Achievements () {
		StartCoroutine (Pressed_Menu_AchievementsRoutine ());
	}

	public IEnumerator Pressed_Menu_AchievementsRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		isFront_Achieves = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}

		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transAchievesMenuHolder);
		popUpAchieveScriptHolder.Achieves_MoveIn ();
		popUpAllScroptHolder_Achieves.PopUp_AnimPlay (0);
	}
		
	public void Pressed_Menu_LeaderB () {
		isFront_LeaderB = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}
	}
		
	public void Pressed_Menu_VidGift () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			StartCoroutine (Pressed_Menu_VidGiftRoutine ());
		} else {
			Moshtan_Utilties_Script.ShowToast ("این قابلیت در آینده فعال خواهد شد.");
		}
	}

	public IEnumerator Pressed_Menu_VidGiftRoutine () {
		menuMainScriptHolder.ZoomIn_CheckBack ();
		yield return null;

		isFront_VidGift = true;

		// SOUNDD EFFECTS - Button press on main manu (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapMenuStartGame.Play();
		}

		yield return new WaitForSeconds (floatWaitBeforeMoveIn);
		menuMainScriptHolder.Pressed_MoveIn_Button (transVidGiftMenuHolder);
		popUpAllScroptHolder_VidGift.PopUp_AnimPlay (0);
		// No move in code
	}

	public void Pressed_Menu_Event () {
		eventMenuScriptHolder.gameObject.SetActive (true);
		eventMenuScriptHolder.Start_Event ();
	}

	public void Tooltip_VidGift_TimeLeft (GameObject anObj) {
		StartCoroutine (Tooltip_VidGift_TimeLeftRoutine (anObj));
	}

	public IEnumerator Tooltip_VidGift_TimeLeftRoutine (GameObject whatObj) {
		yield return new WaitForSeconds (0.1F);
		whatObj.SetActive (true);
	}

	public IEnumerator Event_Buttons_Enter (float delay) {
		yield return new WaitForSeconds (delay);
		animButtonHolder_Events.clip = animClipEventButtonArr [0];
		animButtonHolder_Events.Play ();
		yield return null;
		animButtonHolder_Events.gameObject.SetActive (true);
	}

	public IEnumerator Event_Buttons_Leave () {
		animButtonHolder_Events.clip = animClipEventButtonArr [1];
		animButtonHolder_Events.Play ();
		yield return null;
		animButtonHolder_Events.gameObject.SetActive (false);
	}

	public void MenuParticles_Play () {
		for (int i = 0; i < menuBackdropScriptArr.Length; i++) {
			menuBackdropScriptArr [i].Particles_PlayNow ();
		}
	}

	public void MenuParticles_Stop () {
		for (int i = 0; i < menuBackdropScriptArr.Length; i++) {
			menuBackdropScriptArr [i].Particles_StopNow ();
		}
	}

	public void MenuIdles_Play () {
//		Debug.LogError ("Play idle NOW! " + this.name);	

		for (int i = 0; i < menuBackdropScriptArr.Length; i++) {
			menuBackdropScriptArr [i].Idle_Play ();
		}

		for (int i = 0; i < menuBackdropShadowsScriptArr.Length; i++) {
			menuBackdropShadowsScriptArr [i].Idle_Play ();
		}
	}

	public void MenuIdles_Stop () {
		for (int i = 0; i < menuBackdropScriptArr.Length; i++) {
			menuBackdropScriptArr [i].Idle_Stop ();
		}

		for (int i = 0; i < menuBackdropShadowsScriptArr.Length; i++) {
			menuBackdropShadowsScriptArr [i].Idle_Stop ();
		}

	}

	public void MenuAnims_AllButtons_Idle () {
		animButtonsHolder_ALLParent.Play ("Menu - Button Big ALL Idle Anim 1 (Legacy)");
	}

	public void MenuAnims_AllButtons_Reset () {
		animButtonsHolder_ALLParent.Play ("Menu - Button Big ALL Reset Anim 1 (Legacy)");
	}

	public void MenuAnims_AllBody_Play () {
//		MenuAnims_Body_Idle ();
		MenuAnims_BodyEyeBrows_Idle ();
		MenuAnims_MainAndBody_Idle ();
		MenuAnims_AllButtons_Idle ();

		StartCoroutine (BodyIdleAnim_Delayed ());
	}

	public IEnumerator BodyIdleAnim_Delayed () {
		yield return new WaitForSeconds (0.2F);
		MenuAnims_Body_Idle ();
	}

	public void MenuAnims_AllBody_Reset () {
		MenuAnims_Body_Reset ();
		MenuAnims_BodyEyeBrows_Reset ();
		MenuAnims_MainAndBody_Reset ();
		MenuAnims_Title_Reset ();
	}

	public void MenuAnims_AllBody_Stop () {
		MenuAnims_Body_Stop ();
		MenuAnims_BodyEyeBrows_Stop ();
		MenuAnims_MainAndBody_Stop ();
		MenuAnims_AllButtons_Reset ();
	}

	public void MenuAnims_MainAndBody_Idle () {
		animMainAndBodyMoverHolder.Play ("Menu - Body MainAndBody Idle Anim 1 (Legacy)");
	}

	public void MenuAnims_MainAndBody_Reset () {
		animMainAndBodyMoverHolder.Play ("Menu - Body MainAndBody Reset Anim 1 (Legacy)");
	}

	public void MenuAnims_MainAndBody_Stop () {
		animMainAndBodyMoverHolder.Stop ();
	}

	public void MenuAnims_Body_TapAnim () {
//		animBodyArtActualHolder.Stop ();
		animBodyArtActualHolder.Play ("Menu - Body Separate Pressed Anim 1 (Legacy)");
	}

	public void MenuAnims_Body_Idle () {
		animBodyArtActualHolder.Play ("Menu - Body Separate Idle Anim 1 (Legacy)");
	}

	public void MenuAnims_Body_Reset () {
		animBodyArtActualHolder.Play ("Menu - Body Separate Reset Anim 1 (Legacy)");
	}

	public void MenuAnims_Body_Stop () {
//		animBodyArtActualHolder.Stop ();
	}

	public void MenuAnims_BodyEyeBrows_Idle () {
		animBodyEyeBrowsHolder.Play ("Menu - Body EyeBrows Idle Anim 1 (Legacy)");
	}

	public void MenuAnims_BodyEyeBrows_Reset () {
		animBodyEyeBrowsHolder.Play ("Menu - Body EyeBrows Reset Anim 1 (Legacy)");
	}

	public void MenuAnims_BodyEyeBrows_Stop () {
		animBodyEyeBrowsHolder.Stop ();
	}

	public void MenuAnims_Title_TapAnim () {
		animBodyTitleHolder.Stop ();
		animBodyTitleHolder.Play ("Menu - Title Pressed Anim 1 (Legacy)");
	}

	public void MenuAnims_Title_Idle () {
		animBodyTitleHolder.Play ("Menu - Title Idle Anim 1 (Legacy)");
	}

	public void MenuAnims_Title_RepeatIntro () {
		animBodyTitleHolder.Stop ();
		animBodyTitleHolder.Play ("Menu - Title Intro Anim 1 (Legacy)");
	}

	public void MenuAnims_Title_Reset () {
		animBodyTitleHolder.Play ("Menu - Title Reset Anim 1 (Legacy)");
	}

	public void MenuAnims_Title_Stop () {
		animBodyTitleHolder.Stop ();
	}

	// Similar to SoundStuff Script
	public void Pressed_MenuButtonSound () {
		// SOUNDD EFFECTS - Back Buttons (done)
		if (SoundManager.Instance!=null)
		{
			SoundManager.Instance.GenericTap1.Play();
		}
	}

//
//	void Update () {
//		Debug.LogWarning ("Is fronts (W, Set, Sho, It): "
//			+ isFront_Worlds 
//			+ " " + isFront_Settings
//			+ " " + isFront_Shop
//			+ " " + isFront_Items);
//	}
//
}
