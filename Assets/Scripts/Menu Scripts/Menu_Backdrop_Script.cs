﻿using UnityEngine;
using System.Collections;

public class Menu_Backdrop_Script : MonoBehaviour {

	[Header ("Backdrop Logic")]
	public bool isShadow;
	public bool isOnTheGround;
	public bool isParticleOwner;
	public bool isStationaryParticle;

	public int intDelayForIdleParticlesStart;
	public int intDelayForIdleParticlesRepeat;

	[Header ("")]
	public Animation animBackDropHolder;
	public Animation animBackDropShadowHolder;

	public AnimationClip[] animClipBackDropArr;
	public Camera camHolder;

	public ParticleSystem particleBackDropHolder;
	public ParticleSystem particleBackDropOnHitHolder;

	[Header ("Unique Stuff")]
	public ParticleSystem particleUnique_KharakHit1;
	public ParticleSystem particleUnique_KharakHit2;
	public ParticleSystem particleUnique_Mil;

	private Vector2 v2_PositBuffer;
	private bool canParticlePlay;

	void OnEnable () {
//		Debug.LogWarning ("Enter!");
		Backdrop_Animate_Intro ();

		if (isParticleOwner) {
			Particles_PlayNow ();
		}
	}

	public void Particles_CanPlay (bool isTrue) {
		canParticlePlay = isTrue;
	}

	public void Idle_Stop () {
		animBackDropHolder.Stop ();
	}

	public void Idle_Play () {
		Backdrop_Animate_Idle ();
	}

	public void Backdrop_Animate (int whichAnim) {
		animBackDropHolder.Stop ();
		animBackDropHolder.clip = animClipBackDropArr [whichAnim];
		if (!isOnTheGround) {
			animBackDropShadowHolder.Stop ();
			animBackDropShadowHolder.clip = animClipBackDropArr [whichAnim];
		}

		animBackDropHolder.Stop ();
		animBackDropHolder.Play ();
		if (!isOnTheGround) {
			animBackDropShadowHolder.Stop ();
			animBackDropShadowHolder.Play ();
		}
	}

	public void Backdrop_Animate_Intro () {
		if (!isShadow && !isOnTheGround) {
			Backdrop_Animate (0);
		}
	}

	public void Backdrop_Animate_IntroDELAYED () {
		if (!isShadow && !isOnTheGround) {
			Invoke ("Backdrop_Animate_Intro", 1);
		} 

		if (isParticleOwner) {
			Particles_PlayNow ();
		}
	}
		
	public void Backdrop_Animate_Idle () {
		if (!isShadow && !isOnTheGround) {
			Backdrop_Animate (1);
		}
	}

	public void BackdropSound_Pressed_Mil () {
        // SOUNDD EFFECTS: Mil Tapped (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.ClickRuMill.Play();
        }

		// Achievement for touching backdrop items
		if (AchievementManager.Instance != null) {
			if (!AchievementManager.Instance.wasBackdropTouched_Mil) {
				AchievementManager.Instance.wasBackdropTouched_Mil = true;
				StartCoroutine	(AchievementManager.Instance.AchieveProg_TouchMenuBackDrop ());
			}
		}
	}

	public void BackdropSound_Pressed_Kharak () {
        // SOUNDD EFFECTS: Kharak (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.Kharak.Play();
        }

		// Achievement for touching backdrop items
		if (AchievementManager.Instance != null) {
			if (!AchievementManager.Instance.wasBackdropTouched_Kharak) {
				AchievementManager.Instance.wasBackdropTouched_Kharak = true;
				StartCoroutine	(AchievementManager.Instance.AchieveProg_TouchMenuBackDrop ());
			}
		}
	}

	public void BackdropSound_Pressed_BoxingBag () {
		float tapBoxingPosition = Camera.main.ScreenToViewportPoint (Input.mousePosition).y;
		if (tapBoxingPosition < 0.69F) {
            //Debug.LogError ("BOXING LOW");
            // SOUNDD EFFECTS: Boxing High (done)
            if (SoundManager.Instance!= null)
            {
                SoundManager.Instance.KiseBoxDown.Play();
            }
			
		}
		else {
			if (tapBoxingPosition < 0.75F) {
                //Debug.LogError ("BOXING MID");
                // SOUNDD EFFECTS: Boxing Mid (Done)
                if (SoundManager.Instance != null)
                {
                    SoundManager.Instance.KiseBoxMid.Play();
                }
            } 
			else 
			{
				//Debug.LogError ("BOXING TOP");
                // SOUNDD EFFECTS: Boxing Low (done)
                if (SoundManager.Instance != null)
                {
                    SoundManager.Instance.KiseBoxUp.Play();
                }
            }
		}

		// Achievement for touching backdrop items
		if (AchievementManager.Instance != null) {
			if (!AchievementManager.Instance.wasBackdropTouched_BoxingBag) {
				AchievementManager.Instance.wasBackdropTouched_BoxingBag = true;
				StartCoroutine	(AchievementManager.Instance.AchieveProg_TouchMenuBackDrop ());
			}
		}
	}

	public void BackdropSound_Pressed_ChainAndRing () {

        // SOUNDD EFFECTS: Chain & Ring Tapped (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.Darhalghe.Play();
            StartCoroutine(SoundManager.Instance.CoPlayDelayedClip(SoundManager.Instance.DarhalgheBargasht, 2.2f));
        }

		// Achievement for touching backdrop items
		if (AchievementManager.Instance != null) {
			if (!AchievementManager.Instance.wasBackdropTouched_ChainAndRing) {
				AchievementManager.Instance.wasBackdropTouched_ChainAndRing = true;
				StartCoroutine	(AchievementManager.Instance.AchieveProg_TouchMenuBackDrop ());
			}
		}
	}


    public void Backdrop_Animate_Pressed () {
//		Debug.LogWarning ("Pressed Backdrop");
		if (!isShadow) {
			Backdrop_Animate (2);
			if (isParticleOwner) {
				Particles_StopNow ();

//				particleBackDropHolder.Stop ();
			}
		}
			
	}

	public void Pressed_Backdrop () {
		Backdrop_Animate_Pressed ();
		Backdrop_Animate_OnHitParticle ();
	}

	public void UniqueEffect_KharakHit () {
		particleUnique_KharakHit1.Play ();
		particleUnique_KharakHit2.Play ();
	}

	public void UniqueEffect_MilHit () {
		// Particle For Hit
		particleUnique_Mil.Play ();

        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.BarkhordMillBeZamin.Play();
        }

		// Shake World
		GetComponentInParent<Menu_Main_Script> ().PlayMainShake ();
	}

	public void Backdrop_Animate_OnHitParticle () {
		if (!isShadow) {
			if (!isStationaryParticle) {
				v2_PositBuffer = camHolder.ScreenToWorldPoint (Input.mousePosition);
				particleBackDropOnHitHolder.transform.position = v2_PositBuffer;
			}
			particleBackDropOnHitHolder.Play ();
		}
	}

	public void UniqueEffect_BoxingBagHit () {
//		if (!isShadow) {
//			v2_PositBuffer = camHolder.ScreenToWorldPoint (Input.mousePosition);
//			particleUnique_BoxingBag.transform.position = v2_PositBuffer;
//			particleUnique_BoxingBag.Play ();
//		}
	}

	public void Particles_PlayNow () {
		if (gameObject.activeInHierarchy && isParticleOwner) {
			Particles_CanPlay (true);
			StartCoroutine (Backdrop_Particle (true, intDelayForIdleParticlesStart));
		}
	}

	public void Particles_StopNow () {
		if (gameObject.activeInHierarchy && isParticleOwner) {
			StartCoroutine (Backdrop_Particle (false, 0));
		}
	}

	public IEnumerator Backdrop_Particle (bool isActive, int delay) {
		if (isActive) {
			yield return new WaitForSeconds (delay);
			if (canParticlePlay) {
				particleBackDropHolder.Play ();
			}

			if (canParticlePlay) {
				StartCoroutine (Backdrop_Particle (true, intDelayForIdleParticlesRepeat));
			}
		} else {
			Particles_CanPlay (false);
//			particleBackDropHolder.Stop ();
			particleBackDropHolder.Clear ();
			yield return null;
		}
	}
}
