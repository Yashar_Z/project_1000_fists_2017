﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Menu_Blockers_Script : MonoBehaviour, IPointerDownHandler {

	public ParticleSystem particleBlockedTapHolder;
	public Transform transBlockedTapHolder;

	private Vector2 v2PressPositBuffer;
	private Vector3 v3PressPositBuffer;

	public void OnPointerDown (PointerEventData eventData)
//	public void OnPointerDown ()
	{

		v2PressPositBuffer = Camera.main.ScreenToWorldPoint (eventData.pressPosition); 
//		Debug.Log (this.gameObject.name + " Was Clicked");
//		Debug.Log (this.gameObject.name + " Was Clicked at " + v2PressPositBuffer);

		v3PressPositBuffer = v2PressPositBuffer;
		v3PressPositBuffer.z = 0;

		particleBlockedTapHolder.Play ();
		transBlockedTapHolder.position = v3PressPositBuffer;
	}
}
