﻿using UnityEngine;
using System.Collections;

public class Menu_Intro_Script : MonoBehaviour {

	public Animation animIntroIdleHolder;
	public Animation animMenuAllHolder;
	public Animation animCameraHolder;

	void IntroIdleAnim () {
		animIntroIdleHolder.Play ();
	}

	void IntroMoveAnim () {
		animMenuAllHolder.Play ("Menu - Intro 1 Anim 3 (Legacy)");
	}

	void IntroEffect_ChromaticTwirl () {
		animCameraHolder.Play ("Menu Camera - Intro ONLYChromatic Anim 1 (Legacy)");
	}

	void IntroSound_Punch () {
		// SOUND EFFECTS - Intro Punch (Empty)
	}

	public void Intro_PunchMoment () {
		IntroEffect_ChromaticTwirl ();
		IntroSound_Punch ();
	}

	public void Intro_ShowTapToPlay () {
        if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.Ario)
        {
            Moshtan_Utilties_Script.ShowToast("برای شروع، تپ کنید!");
        }
		
	}

//	void Update () {
//		if (Input.GetKey ("E")) {
//			Debug.LogError ("EEEEEEEEE");
//		}
//	}
}
