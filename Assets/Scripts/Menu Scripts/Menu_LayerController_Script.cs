﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu_LayerController_Script : MonoBehaviour {

	public Canvas canvasItemsHolder;
	public Canvas canvasShopHolder;
	public Canvas canvasAchieveHolder;

	public RectMask2D rectMaskHolder_Worlds;
	public RectMask2D rectMaskHolder_Items;
	public RectMask2D rectMaskHolder_Shop;

	public enum RectMenuType
	{
		Rect_Worlds,
		Rect_Items,
		Rect_Shop,
		Rect_All
	}

	public void Menu_CanvasChange_ShopFront () {
		// Tick override sortings

//		canvasItemsHolder.overrideSorting = false;
//		canvasShopHolder.overrideSorting = true;
//		canvasAchieveHolder.overrideSorting = false;

		// Only set layer name for proper menu canvas
//		canvasShopHolder.sortingLayerName = "UI Layer";

		StartCoroutine (Menu_CanvasChange (-3, 7, -3));
//		Debug.LogError ("Shop Front!");
	}

	public void Menu_CanvasChange_ItemFront () {
		// Tick override sortings

//		canvasItemsHolder.overrideSorting = true;
//		canvasShopHolder.overrideSorting = false;
//		canvasAchieveHolder.overrideSorting = false;

		// Only set layer name for proper menu canvas
//		canvasItemsHolder.sortingLayerName = "UI Layer";

		StartCoroutine (Menu_CanvasChange (7, -3, -3));
//		Debug.LogError ("Items Front!");
	}

	public void Menu_CanvasChange_AchieveFront () {
		// Tick override sortings

//		canvasItemsHolder.overrideSorting = false;
//		canvasShopHolder.overrideSorting = false;
//		canvasAchieveHolder.overrideSorting = true;

		// Only set layer name for proper menu canvas
//		canvasAchieveHolder.sortingLayerName = "UI Layer";

		StartCoroutine (Menu_CanvasChange (-3, -3, 7));
//		Debug.LogError ("Achieves Front!");
	}

	public IEnumerator Menu_CanvasChange (int intForItems, int intForShop, int intForAchieve) {
		
		canvasItemsHolder.sortingOrder = intForItems;
		canvasShopHolder.sortingOrder = intForShop;
		canvasAchieveHolder.sortingOrder = intForAchieve;

		yield return null;
	}

	public IEnumerator MenuRectMask_Controller (RectMenuType whichRect,bool isActive) {
		yield return new WaitForSeconds (10);
//		Debug.LogError ("Now ENABLED Rect 2Ds");

		switch (whichRect) {
		case RectMenuType.Rect_Worlds:
			rectMaskHolder_Worlds.enabled = isActive;
			break;
		case RectMenuType.Rect_Items:
			rectMaskHolder_Items.enabled = isActive;
			break;
		case RectMenuType.Rect_Shop:
			rectMaskHolder_Shop.enabled = isActive;
			break;
		case RectMenuType.Rect_All:
			rectMaskHolder_Worlds.enabled = isActive;
			rectMaskHolder_Items.enabled = isActive;
			rectMaskHolder_Shop.enabled = isActive;
			break;
		default:
//			Debug.LogError ("NO RECT SWITCH PROBLEM!");
			break;
		}
	}
}
