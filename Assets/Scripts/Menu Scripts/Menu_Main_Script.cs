﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Menu_Main_Script : MonoBehaviour {

	public bool ShowFullIntro;
//	public bool AreMenusMoveOut;

	public Menu_TutCheck_Script menuTutCheckScriptHolder;
	public Phone_ShortCut_Script phoneShortCutScriptHolder;
	public Menu_BackAnims_Script menuBackAnimsScriptHolder;
	public Mustache_Display_Script mustacheDisplayScriptHolder;
	public Menu_LayerController_Script menuLayerControlScriptHolder;
	public AfterLoadDailyReward_Script afterLoadDailyScriptHolder;
	public Popup_PowerUpsParent_Script popUpPowUpsParScript;
	public BackDarker_Script backDarkerScriptHolder;
//	public PowerUps_Menu_Script powUpsMenuScriptHolder;
	public Key_Script keyScriptHolder;

	public Menu_MiniCutscenes_Script miniCutscenesScriptHolder;

	public Transform transMainParentBackHolder;
	public Transform worlds_ContentTransHolder;
	public Transform levels_ContentTransHolder;

	public RectTransform menu_MustacheHolderRecTransHolder_WorldMenu;
	//
	public RectTransform menu_MustacheHolderRecTransHolder_ItemsMenu;
	public RectTransform menu_MustacheHolderRecTransHolder_QuestMenu;
	public RectTransform menu_MustacheHolderRecTransHolder_AchieveMenu;
	public RectTransform menu_MustacheHolderRecTransHolder_EndlessMenu;
	//
	public RectTransform menu_MustacheHolderRecTransHolder_WorldMenu_Shadow;
	public RectTransform menu_MustacheHolderRecTransHolder_ItemsMenu_Shadow;
	public RectTransform menu_MustacheHolderRecTransHolder_AchieveMenu_Shadow;
	public RectTransform menu_MustacheHolderRecTransHolder_EndlessMenu_Shadow;

	// Shop uses its own system / script
//	public RectTransform menu_MustacheHolderRecTransHolder_ShopMenu;

	public GameObject worldsAllParentObjectHolder;
	public GameObject worldSelectClickBlocker2Holder;
	public GameObject IntroStartClickBlocker1Holder;
	public GameObject IntroStartClickBlocker2Holder;
	public GameObject menuBlackBordersHolder;
	public GameObject objShadowMustacheParticleParentHolder;
	public GameObject objFPScounterHolder;
	public GameObject objBatteryDisableFrontHolder;

	public GameObject objForDelButtonsHolder;
	public GameObject objDailyRewardHolder;

	public GameObject objQuitPopUpHolder;
	public GameObject objEndlessButtonLockHolder;

	[Header ("Title - Classic Or VAS")]
	public GameObject objIntroLogoTitleParent;
	public GameObject objIntroVasTitleParent;
	public GameObject[] objIntroVasTitleArr1;
	public GameObject[] objIntroVasTitleArr2;

	[Header ("")]
	public Text text_Mustache_Holder_WorldsPage;
	public Text text_Mustache_Holder_ItemsPage;
	public Text text_Mustache_Holder_ShopPage;
	//
	public Text text_Mustache_Holder_QuestPage;
	public Text text_Mustache_Holder_AchievePage;
	//
	public Text text_Mustache_Holder_EndlessPage;
	// To hold above strings to have only ONE tostring
	public string stringMustacheAmountBuffer;

	public Text text_Stars_Holder;

	public Text text_PopUp_WorldNumber;
	public Text text_PopUp_LevelNumber;

	public Text text_worldMustacheReqShow;
	public Text[] text_worldStarsReqShowArr;

	public GameObject starObj_1;
	public GameObject starObj_2;
	public GameObject starObj_3;

	[Header ("Worlds & Buttons Stuff")]
	public Worlds_All_Script worldsAllScriptHolder;
	public Button playButtonHolder;
	public Button endlessButtonHolder;
	public Button worldsBackButtonHolder;
	public Button[] worldCardButtons_FreeArr;

	public Button buttonHeroBack_BodyHolder;
	public Button buttonHeroBack_HeadHolder;

	public ScrollRect scrollLevelsHolder;
	public ScrollRect scrollWorldsHolder;

	public Animation allMenusAnimRef;
	public Animation animIntroGradsHolder;
	public Animation animIntroAllIdleHolder;
	public Animation animWorldsAllHolder;
	public Animation animCameraHolder;
	public Animation animBodyTitleHolder;

	public Animation animLevelsIdleContentHolder;

	// Battery idle particles
	public ParticleSystem particleBatteryHeatIdle;
	public ParticleSystem particleBatteryFlareIdle;

	public ParticleSystem particleLevelSelected_FadeToBlackMenu;
	public ParticleSystem particleLevelSelected_FadeToBlackMenuFirstTime;
	public ParticleSystem particleLevelSelected_PipeLinesMenu;
	public ParticleSystem particleLevelSelected_PipeMomentMenu;
	public ParticleSystem particleLevelSelected_FullFlashMenu;
	public ParticleSystem particleWorldKeySelectHolder;

	public ParticleSystem particleButtonHit_Items;
	public ParticleSystem particleButtonHit_Play;
	public ParticleSystem particleButtonHit_Endless;
	public ParticleSystem particleButtonHit_Shop;

	public Animator animatorIntroHolder;
	public GameObject introParentObjHolder;

//	[Header ("Back BG, Character & Drops GameObjects")]
//	public GameObject objBackBGHolder;
//	public GameObject objBackCharHolder;
//	public GameObject objBackDropHolder;

	[Header ("Zoom In N Out Objs and Trans")]
	public GameObject objShopMenuParnentHolder;
	public GameObject objItemsMenuParnentHolder;
	public GameObject objWorldsMenuParnentHolder;
	public GameObject objLevelsSelectParentHolder;
	public GameObject objSettingsMenuParnentHolder;
	public GameObject objAchievesMenuParnentHolder;
	public GameObject objVidGiftMenuParnentHolder;	
	public GameObject objEndlessMenuParnentHolder;	

	public GameObject objButtonsMiniBlockerHolder;

//	public Transform transShopMenuParnentHolder;
//	public Transform transItemsMenuParnentHolder;
//	public Transform transWorldsMenuParnentHolder;
//	public Transform transLevelsSelectParentHolder;
//	public Transform transSettingsMenuParnentHolder;
//	public Transform transAchievesMenuParnentHolder;

	[Header ("Zoom DISABLE Objects")]
	public Transform transMainMenuAllHolder;
	public GameObject objMainMenuALLHolder;
	public Canvas canvasMainMenuAllHolder;

	public GameObject objBackDropsHolder;
	public GameObject objBodyTitleHolder;
	public GameObject objBodyElementsHolder;
	public GameObject objMainButtonsHolder;

	public GameObject objPhoneHolder;

	[Header ("Main Buttons")]
	public GameObject objMainButton_Play;
	public GameObject objMainButton_Endless;
	public GameObject objMainButton_Items;
	public GameObject objMainButton_Shop;
	public GameObject objMainButton_Settings;
	public GameObject objMainButton_Achieves;
	public GameObject objMainButton_LeaderB;
	public GameObject objMainButton_VidGift;

	[Header ("")]
	public GameObject objBackDrop_DarkChains_LeftShadow;
	public GameObject objBackDrop_DarkChains_Left;
	public GameObject objBackDrop_DarkChains_RightShadow;
	public GameObject objBackDrop_DarkChains_Right;
	public GameObject objBackDrop_Kharak;
	public GameObject objBackDrop_BigRings1Shadow;
	public GameObject objBackDrop_BigRings1;
	public GameObject objBackDrop_BigRings2Shadow;
	public GameObject objBackDrop_BigRings2;
	public GameObject objBackDrop_BoxingBagShadow;
	public GameObject objBackDrop_BoxingBag;

	public GameObject objBackDrop_Mills_Together;
	public GameObject objBackDrop_Mills_Separate;

	[Header ("")]
	public GameObject objMainTitleMover;

	public enum WorldUnlockType {
		LockedToAvailable,
		ToFree_Stars,
		ToFree_Mustache
	}

	public enum MenuTutType {
		BatteryCharge,
		ItemProgress,
		ItemRepeatItem,
		None
	}

//	private LevelData_Class[] levelDataArr;

//	[SerializeField]
//	private Animator[] worldCarsAnimatorArr;

	[Header ("")]
	public Animation[] worldCardsAnimArr;

	public bool inNewestWorld;

	// For back in world / level select
	public bool isLevelsInFront;

	// This was previously using getcomponent
	public Level_Button_Script[] levelButtonScriptRefArr;

	private Vector2 menu_MustachePositVect2;

	private int world_1_Price_Mustache;
	private int world_1_Price_Star;
	private int world_2_Price_Mustache;
	private int world_2_Price_Star;
	private int world_3_Price_Mustache;
	private int world_3_Price_Star;
	private int world_4_Price_Mustache;
	private int world_4_Price_Star;
	private int world_5_Price_Mustache;
	private int world_5_Price_Star;

	private int world_CurrentPrice_Star;
	private int world_CurrentPrice_Mustache;

	private int world_NumberOfLevels;
	private int world_NumberSelected;

	private int level_NumberSelected;

	private int world_NewOne;
	private int level_NewOne;

	private int levelSelectSideCounter;

	private int levelCheckTut_BatteryCheck;
	private int levelCheckTut_PowUp2;

	private float scrollWorldsNormalizedFloat_Curr;
	private float scrollWorldsNormalizedFloat_Target;
	private float scrollWorldsNormalizedFloat_IncreaseAmount;

	private float scrollLevelNormalizedFloat_Curr;
	private float scrollLevelNormalizedFloat_Target;
	private float scrollLevelNormalizedFloat_IncreaseAmount;

	private float scrollFloatBuffer;

	private float floatWaitBeforeFreeByStars;
	private float floatWaitBeforeUnavailtoAvailable;

	private bool shouldCheck_WorldUnlock;
	private bool isMiniCutsceneOn;
	private bool allowLevelAutoScroll;

	private bool beganZoomIn;
	private bool dontDeleteBackItems;

	private bool newestLevelUnlockPreAnimHappened;

	private bool isNewWorldCheckDone;
	private bool isWorldGoToLevelSelect;

	private bool isMiniCutClickBlockerOn;

	[SerializeField]
	private LevelData_Class[] worldLevelData_ClassRef;

	private System.DateTime eventStart_NewYear;
	private System.DateTime eventEnd_NewYear;

	// Use this for initialization
	void Awake () {
		// New level stuff (For activate level pre-anim)
		newestLevelUnlockPreAnimHappened = false;

		// Show Black Borders
		menuBlackBordersHolder.SetActive (true);

		// Minicutscene situation
		isMiniCutsceneOn = false;

		// Deactivate shadow mustache on awake
//		objShadowMustacheParticleParentHolder.SetActive (false);

		// Prevent world & level unlocked from being zero
		if (PlayerData_Main.Instance.player_WorldsUnlocked == 0) {
			PlayerData_Main.Instance.player_WorldsUnlocked = 1;
		}
		if (PlayerData_Main.Instance.player_LevelsUnlocked == 0) {
			PlayerData_Main.Instance.player_LevelsUnlocked = 1;
		}

		// Get new world
		world_NewOne = PlayerData_Main.Instance.player_WorldsUnlocked;

		if (PlayerData_Main.Instance != null) {
			DeActivateWorlds ();
			ActivateWorlds ();
			PlayerData_Main.Instance.PlayingNewestLevel = false;

			// Remove for del buttons in case 
			if (PlayerData_Main.Instance.isFullBuild || PlayerData_Main.Instance.isFullBuild_VAS_Cheat) {
				objForDelButtonsHolder.SetActive (false);
			} else {
				objForDelButtonsHolder.SetActive (!PlayerData_Main.Instance.isFullBuild);
			}
		}

		// HUD Late Update (No save)
		StartCoroutine (Menu_HUD_Update_NoSave ());

//		worldsAllParentObjectHolder.SetActive (false);

		// Get Animators for world cards
//		worldCarsAnimatorArr = worlds_ContentTransHolder.GetComponentsInChildren<Animator> ();
//		WorldsAnimatorSet (false);

		// Lock all backdrop elements
		MenuBackDrop_LockAll ();

		// Disable body back title
		objBodyTitleHolder.SetActive (false);

		// Disable Phone before start
		objPhoneHolder.SetActive (false);

		// Get World Unlocking Prices (From Balance Constants)
		world_1_Price_Mustache = 0;
	    world_1_Price_Star = 0;
	    world_2_Price_Mustache = Balance_Constants_Script.WorldUnlock_ByMustache_2;
		world_2_Price_Star = Balance_Constants_Script.WorldUnlock_ByStar_2;
		world_3_Price_Mustache = Balance_Constants_Script.WorldUnlock_ByMustache_3;
		world_3_Price_Star = Balance_Constants_Script.WorldUnlock_ByStar_3;
		world_4_Price_Mustache = Balance_Constants_Script.WorldUnlock_ByMustache_4;
		world_4_Price_Star = Balance_Constants_Script.WorldUnlock_ByStar_4;
		world_5_Price_Mustache = Balance_Constants_Script.WorldUnlock_ByMustache_5;
		world_5_Price_Star = Balance_Constants_Script.WorldUnlock_ByStar_5;
	}

//	public void WorldsAnimatorSet (bool whatBool) {
//		for (int i = 0; i < worldCarsAnimatorArr.Length; i++) {
//			worldCarsAnimatorArr [i].enabled = whatBool;
//		}
//	}

	public void DeActivateWorlds () {
		// NEEDS UPDATE
		// Deactivate all worlds before using Player Data
		for (int i = 0; i < worlds_ContentTransHolder.childCount; i++) {
			worlds_ContentTransHolder.GetChild (i).GetChild (0).gameObject.SetActive (false);
			worlds_ContentTransHolder.GetChild (i).GetChild (1).gameObject.SetActive (true);
			worlds_ContentTransHolder.GetChild (i).GetChild (2).gameObject.SetActive (true);
			// Old for activate levels
//			for (int j = 0; j < worlds_ContentTransHolder.GetChild(i).childCount; j++) {
//				worlds_ContentTransHolder.GetChild (i).GetChild(j).gameObject.SetActive (false);
//			}
		}
	}

	public void ActivateWorlds () {
		// NEEDS UPDATE
		// Activate saved worlds by using Player Data
		for (int i = 0; i < PlayerData_Main.Instance.player_WorldsUnlocked; i++) {
			worlds_ContentTransHolder.GetChild (i).GetChild (0).gameObject.SetActive (true);
			worlds_ContentTransHolder.GetChild (i).GetChild (1).gameObject.SetActive (false);
			worlds_ContentTransHolder.GetChild (i).GetChild (2).gameObject.SetActive (false);

			// Old for actiave levels
			// Last world levels (partially unlock levels)
//			if (i == (PlayerData_Main.Instance.player_WorldsUnlocked - 1)) {
//				
//				for (int j = 0; j < PlayerData_Main.Instance.player_LevelsUnlocked; j++) {
//					worlds_ContentTransHolder.GetChild (i).GetChild(j).gameObject.SetActive (true);
//				}
//
//				// Activate Button To Unlock New World IF not on last world
//				if (PlayerData_Main.Instance.player_WorldsUnlocked < 5) {
//					if (PlayerData_Main.Instance.player_LevelsUnlocked == 10) {
//						if (PlayerData_Main.Instance.Show_NewWorld_Button) {
//							worlds_ContentTransHolder.GetChild (i).GetChild (worlds_ContentTransHolder.GetChild (i).childCount - 1).gameObject.SetActive (true);
//						}
//					}
//				}
//
//			} 
//
//			// Before last world (fully unlock levels)
//			else {
//				for (int j = 0; j < (worlds_ContentTransHolder.GetChild (i).childCount - 1); j++) {
//					worlds_ContentTransHolder.GetChild (i).GetChild (j).gameObject.SetActive (true);
//				}
//			}
		}
	}

//	public void Available_NewWorld_Animate_OLD () {
//		worldCarsAnimatorArr [PlayerData_Main.Instance.player_WorldsUnlocked].enabled = true;
//		worldCarsAnimatorArr [PlayerData_Main.Instance.player_WorldsUnlocked].Play ("Menu - World Card AvailableNew Anim 1");
//
////		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (0).gameObject.SetActive (true);
////		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (1).gameObject.SetActive (false);
////		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (2).gameObject.SetActive (false);
//
//	}

//	public void Unlock_NewWorld_Animate_OLD () {
//		worldCarsAnimatorArr [PlayerData_Main.Instance.player_WorldsUnlocked].enabled = true;
//		worldCarsAnimatorArr [PlayerData_Main.Instance.player_WorldsUnlocked - 1].Play ("Menu - World Card UnlockNew Anim 1");
//	}

	public void WorldLogic_UnlockOLD () {
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (0).gameObject.SetActive (true);
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (1).gameObject.SetActive (false);
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (2).gameObject.SetActive (false);
	}

	public void WorldLogic_Available () {
//		Debug.LogWarning ("Yo 1 world_NewOne = " + world_NewOne);
//		Debug.LogError ("Child world_NewOne name = " + worlds_ContentTransHolder.GetChild (1).name);
//		Debug.LogWarning ("NAME = " + worlds_ContentTransHolder.GetChild (world_NewOne).name);
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (0).gameObject.SetActive (false);
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (1).gameObject.SetActive (true);
		worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (2).gameObject.SetActive (false);
//		Debug.LogWarning ("3333 = " + worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (2).name);
//		Debug.LogWarning ("OBJ = " + worlds_ContentTransHolder.GetChild (world_NewOne).GetChild (2).gameObject.activeInHierarchy);
	}

	public void Check_LastWorldAvailable () {
//		Debug.Log ("Last available world check = " + (PlayerData_Main.Instance.player_WorldsUnlocked < menuTutCheckScriptHolder.lastPossibleWorld_ForThisVer).ToString());

		if (PlayerData_Main.Instance.player_WorldsUnlocked < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer) {
			PlayerData_Main.Instance.player_IsOnNotOnLastAvailableWorld = true;
		}
		else {
			PlayerData_Main.Instance.player_IsOnNotOnLastAvailableWorld = false;
		}
	}

	public void Check_NewWorld () {
		keyScriptHolder.KeyActive (false);

//		Debug.LogWarning ("New world checks: PLAYER WORLD UNLOCKED = " + PlayerData_Main.Instance.player_WorldsUnlocked + 
//			"   LAST POSSIBLE WORLD = " + menuTutCheckScriptHolder.lastPossibleWorld_ForThisVer);

		// Check for last available world - function
		Check_LastWorldAvailable ();


		// Scroll to curr worlds
//		ScrollWorlds_Set_CurrWorld ();

		// Check / update requirements of mustache and star
		Get_UnlockRequirements ();

		// Star Req amounts update
		OpenWorld_SetupStarReqTexts ();

		// Mustache Req amounts
		OpenWorld_SetupMustacheReqText ();

		// World Card Animators Stuff
		//		WorldsAnimatorSet (true);
		//		for (int i = 0; i < worldCarsAnimatorArr.Length; i++) {
		//			worldCarsAnimatorArr [i].Play ("Menu - World Default Anim 1");
		//		}

		if (PlayerData_Main.Instance.Show_NewWorld_Allow) {
			// Check for last available world - actual check
			if (PlayerData_Main.Instance.player_IsOnNotOnLastAvailableWorld) {
				NewWorld_UpdateNow_ToAvailable ();
			}

		} else {
			if (!PlayerData_Main.Instance.Show_NewWorld_Remain) {
				// Is about all worlds being 10 levels (Number of levels in a world)

				if (PlayerData_Main.Instance.player_LevelsUnlocked == 10) {
//					Debug.LogError ("WTF!!!!!!!!!!!!!!!!!   11111");

					if (Check_IfLastLevelComplete ()) {
//						Debug.LogError ("WTF!!!!!!!!!!!!!!!!!   22222");
						NewWorld_UpdateNow_ToAvailable ();

						PlayerData_Main.Instance.Show_NewWorld_Remain = true;
//							keyScriptHolder.KeyActive (true);
//							Debug.LogWarning ("World to available"));
						WorldLogic_Available ();
					} 

					// Means last level was NOT completed  (In case of this meaning no NewWorld_Remain set)
					else {
//						Debug.LogError ("WTF!!!!!!!!!!!!!!!!!   33333");
//						Debug.LogWarning ("Needs to unlock more levels");
						keyScriptHolder.KeyActive (false);

						// Allow go to level select because ONLY with the-last-scene-wasIngame you can get here in the first place
						isWorldGoToLevelSelect = true;
					}
				} 

				// Means we haven't unlocked 10 levels
				else {
					// Allow go to level select because ONLY with the-last-scene-wasIngame you can get here in the first place
					isWorldGoToLevelSelect = true;
				}
			} else {
				WorldLogic_Available ();
				Check_CanUnlockWorld ();
			}
		}
//		}

		// Now the check is done
		isNewWorldCheckDone = true;

		// This is for back button in world / level select (The other is in the getnumber_world)
		isLevelsInFront = false;
	}

	public bool Check_IfLastLevelComplete () {
		switch (PlayerData_Main.Instance.player_WorldsUnlocked) {
		case 1:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World1_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 2:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World2_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 3:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World3_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 4:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World4_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 5:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World5_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		default:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World1_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		}

//		Debug.LogError ("WTF!!!!!!!!!!!!!!!!!   44444. = " 
//			+ (PlayerData_Main.Instance.player_WorldsUnlocked < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer));

		// Checking the last index element of worlds levelData array and its completion star
		if (PlayerData_Main.Instance.player_WorldsUnlocked < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer) {
			return worldLevelData_ClassRef [worldLevelData_ClassRef.Length - 1].star_Unlocked_Level;
		} else {
			return false;
		}

//		Debug.Log ("Unlocked?" + worldLevelData_ClassRef [worldLevelData_ClassRef.Length - 1].star_Unlocked_Level);
	}

	public void NewWorld_UpdateNow_ToAvailable () {
		// Activate Blocker
		//			worldSelectClickBlocker2Holder.SetActive (true);

//		Debug.LogWarning ("Did we get here!? (NewWorld Update Now. Remain = TRUE. Allow = FALSE");

		// Add still remain
		PlayerData_Main.Instance.Show_NewWorld_Remain = true;

		// Remove Allow
		PlayerData_Main.Instance.Show_NewWorld_Allow = false;

		// This happens in anim / Animsss
		NewWorld_UnlockAnimate (WorldUnlockType.LockedToAvailable);

		// Save after turning world to available
		SaveLoad.Save ();

		// Set world scroll to make new world be at the center
//		ScrollWorlds_Set_CurrWorld ();

//		Available_NewWorld_Animate();
		//			Invoke ("Available_NewWorld_Animate", 0.5F);

	}

	public void Pressed_NewWorld_AvailableAnimate_w2 () {
		worldCardsAnimArr [1].Stop ();
		worldCardsAnimArr [1].Play ("Menu - World CAR Locked To Available Anim 2 (Legacy)");
	}

	public void Pressed_NewWorld_FreeAnimate_w2 () {
		worldCardsAnimArr [1].Stop ();
		worldCardsAnimArr [1].Play ("Menu - World CAR Available To Free Anim 1 (Legacy)");
	}
//
//	public void Pressed_NewWorld_KeyAnimate_w2 () {
//		keyScriptHolder.Key_UseAnim ();
//
//		KeyAnimate_World2 ();
//		KeyScroll_World2 ();
//	}

//	public void KeyAnimate_World2 () {
//		worldCardsAnimArr [1].Stop ();
//		worldCardsAnimArr [1].Play ("Menu - World CAR Key To Free Anim 1 (Legacy)");
//	}
//
//	public void KeyScroll_World2 () {
//		scrollWorldsHolder.horizontalNormalizedPosition = 0.2F + 0.04F;
//	}

	public void NewWorld_Anim_ToAvailable (int whichWorld) {
		StartCoroutine (AnimOnly_ToAvailable (whichWorld));

		if (!isMiniCutsceneOn) {
			StartCoroutine (ScreenBlocker_MiniCutscene (1));
//			StartCoroutine (ScreenBlocker_Routine (3));
		}

	}

	public IEnumerator AnimOnly_ToAvailable (int whichWorld) {
		yield return new WaitForSeconds (floatWaitBeforeFreeByStars);
		worldCardsAnimArr [whichWorld].Stop ();
		worldCardsAnimArr [whichWorld].Play ("Menu - World CAR Locked To Available Anim 2 (Legacy)");
	}

	public void NewWorld_Anim_ToFree_ByStars (int whichWorld) {
		StartCoroutine (AnimOnly_ToFree_ByStars (whichWorld));

		if (!isMiniCutsceneOn) {
			StartCoroutine (ScreenBlocker_MiniCutscene (1));
//			StartCoroutine (ScreenBlocker_Routine (4.5F));
		}

	}

	public IEnumerator AnimOnly_ToFree_ByStars (int whichWorld) {
		yield return new WaitForSeconds (floatWaitBeforeFreeByStars);
		worldCardsAnimArr [whichWorld].Stop ();
		worldCardsAnimArr [whichWorld].Play ("Menu - World CAR Available To Free Anim 1 (Legacy)");
	}

	public void NewWorld_Anim_ToFree_ByKey (int whichWorld) {
		keyScriptHolder.Key_UseAnim ();
		particleWorldKeySelectHolder.Play ();

		NewWorld_KeyAnim (whichWorld);
		ScrollWorlds_Set_NextWorld_BlendStart ();
//		NewWorld_KeyScroll (whichWorld);

		if (!isMiniCutsceneOn) {
			StartCoroutine (ScreenBlocker_MiniCutscene (1));
//			StartCoroutine (ScreenBlocker_Routine (3));
		}
	}

	public void NewWorld_KeyAnim (int whichWorld) {
		worldCardsAnimArr [whichWorld].Stop ();
		worldCardsAnimArr [whichWorld].Play ("Menu - World CAR Key To Free Anim 1 (Legacy)");
	}

	public void NewWorld_KeyScroll (int whichWorld) {
		scrollWorldsHolder.horizontalNormalizedPosition = (whichWorld * 0.2F) + 0.04F;
	}

	public IEnumerator ScreenBlocker_MiniCutscene (int whichAnim) {
		if (whichAnim == 1) {
			animLevelsIdleContentHolder.Stop ();
			animLevelsIdleContentHolder.Play ("Menu - Level ALL Reset Anim 1 (Legacy)");
			isMiniCutsceneOn = true;
//			Debug.LogError ("Block NOW!");

			// This has all following 3 lines
			MiniCutscenes_Activate ();

//			isMiniCutClickBlockerOn = true;
//			worldSelectClickBlocker2Holder.SetActive (true);
//			animMinicutscenesGrads.Play ("Menu - MiniCutscenes Enter Anim 1 (Legacy)");

		} else {
			isMiniCutsceneOn = false;
//			Debug.LogError ("UUUNBlock NOW!");

			// This has all following 3 lines
			MiniCutscenes_DeActivate ();

//			isMiniCutClickBlockerOn = false;
//			worldSelectClickBlocker2Holder.SetActive (false);
//			animMinicutscenesGrads.Play ("Menu - MiniCutscenes Leave Anim 1 (Legacy)");

			// Idle anim for levels after unlock cutscene
			Invoke ("LevelsIdle_AnimLATE", 2 + floatWaitBeforeFreeByStars);
		}
		yield return null;
	}

	public void MiniCutscenes_Activate () {
		isMiniCutClickBlockerOn = true;
		worldSelectClickBlocker2Holder.SetActive (true);

		miniCutscenesScriptHolder.MiniCutscenes_Enter ();
	}

	public void MiniCutscenes_DeActivate () {
		isMiniCutClickBlockerOn = false;
		worldSelectClickBlocker2Holder.SetActive (false);

		miniCutscenesScriptHolder.MiniCutscenes_Leave ();
	}

	public IEnumerator ScreenBlocker_Routine (float delay) {
		// Before activating world select click blocker, need to enable its canvas
		miniCutscenesScriptHolder.MiniCutscenes_CanvasEnable ();
		worldSelectClickBlocker2Holder.SetActive (true);

		yield return new WaitForSeconds (delay + 0.25F);

		// To prevent this from becoming false while mini cutscene blocker is on
		if (!isMiniCutClickBlockerOn) {
			miniCutscenesScriptHolder.MiniCutscenes_CanvasDisable_Delayed ();
			worldSelectClickBlocker2Holder.SetActive (false);
		}

		StartCoroutine (LevelsIdle_Animate ());
	}

	public void LevelsIdle_AnimLATE () {
		StartCoroutine (LevelsIdle_Animate ());
	}

	public IEnumerator LevelsIdle_Animate () {
//		Debug.LogError ("Mini 1   --------------------------------------");
		yield return new WaitForSeconds (0.5F);
		if (!isMiniCutsceneOn) {
			animLevelsIdleContentHolder.Play ("Menu - Level ALL Idle Anim 1 (Legacy)");
		}
	}

	public void Get_UnlockRequirements () {
		
		switch (PlayerData_Main.Instance.player_WorldsUnlocked + 1) {
		case 1:
			world_CurrentPrice_Star = world_1_Price_Star;
			world_CurrentPrice_Mustache = world_1_Price_Mustache;
			break;
		case 2:
			world_CurrentPrice_Star = world_2_Price_Star;
			world_CurrentPrice_Mustache = world_2_Price_Mustache;
			break;
		case 3:
			world_CurrentPrice_Star = world_3_Price_Star;
			world_CurrentPrice_Mustache = world_3_Price_Mustache;
			break;
		case 4:
			world_CurrentPrice_Star = world_4_Price_Star;
			world_CurrentPrice_Mustache = world_4_Price_Mustache;
			break;
		case 5:
			world_CurrentPrice_Star = world_5_Price_Star;
			world_CurrentPrice_Mustache = world_5_Price_Mustache;
			break;
		default:
			world_CurrentPrice_Star = world_1_Price_Star;
			world_CurrentPrice_Mustache = world_1_Price_Mustache;
			break;
		}
//		Debug.LogWarning ("WorldNumber: " + (PlayerData_Main.Instance.player_WorldsUnlocked + 1) + "  Unlock cost STAR: " + world_CurrentPrice_Star + "  Unlock cost Mustache: " + world_CurrentPrice_Mustache);
	}

	public void Check_CanUnlockWorld () {
		Debug.LogWarning ("Can Unlock World!?");

		// Stars comparison
		if (PlayerData_Main.Instance.player_Stars >= world_CurrentPrice_Star) {
//			Debug.LogError ("UNLOCK NOW!"); 

			// Call General Unlocker
			NewWorld_UnlockAnimate (WorldUnlockType.ToFree_Stars);

			// Update info for worlds
			NewWorld_UpdateInfo ();

			// Save after unlocking the world with stars
			SaveLoad.Save ();

//			Unlock_NewWorld_Animate ();
			keyScriptHolder.KeyActive (false);

			// Should NOT open level select
			isWorldGoToLevelSelect = false;
		} else {
			Check_AllMiniCutsceneEnd ();
//			Debug.LogError ("CAN'T UNLOCK NOW! Not enough stars!"); 
			keyScriptHolder.KeyActive (true);

			// Should DO open level select
			isWorldGoToLevelSelect = true;
		}

	}

//	void Update () {
//		Debug.LogError ("isWorldGoToLevelSelect = " + isWorldGoToLevelSelect);
//	}

	public void Check_AllMiniCutsceneEnd () {
		if (isMiniCutsceneOn) {
			StartCoroutine (ScreenBlocker_MiniCutscene (0));
		}
	}

	public void NewWorld_UnlockAnimate (WorldUnlockType whichUnlock) {
		switch (whichUnlock) {
		case WorldUnlockType.LockedToAvailable:
			NewWorld_Anim_ToAvailable (PlayerData_Main.Instance.player_WorldsUnlocked);

			// Set wait for free by star to zero if they are consecutive and last level was in-game
			floatWaitBeforeFreeByStars = 0;

			// SOUNDD EFFECTS - World Card From from Unavailabel to Available (Done)
			if (SoundManager.Instance != null) {
				SoundManager.Instance.MoshtBeCardChapter.PlayDelayed (0.5F);
			}

//			Debug.LogError ("Anim from Unavailabel to Available");
			break;

		case WorldUnlockType.ToFree_Mustache:
			NewWorld_Anim_ToFree_ByKey (PlayerData_Main.Instance.player_WorldsUnlocked);

			// SOUNDD EFFECTS - World Card From from Available to FreeByMustache (Done)
			if (SoundManager.Instance != null) {
				SoundManager.Instance.BazShodanBaKelid.Play();
			}

//			Debug.LogError ("Anim from Available to FreeByMustache");
			break;

		case WorldUnlockType.ToFree_Stars:
			NewWorld_Anim_ToFree_ByStars (PlayerData_Main.Instance.player_WorldsUnlocked);

			// SOUNDD EFFECTS - World Card From from Available to FreeByStars (Done)
			if (SoundManager.Instance != null) {
				SoundManager.Instance.BazShodanBaStar.PlayDelayed(0.2F);
			}

//			Debug.LogError ("Anim from Available to FreeByStars");
			break;

		default:
			break;
		}
	}

	// Only after unlocking via key / stars
	public void NewWorld_UpdateInfo () {
		// Remove still remain
		PlayerData_Main.Instance.Show_NewWorld_Remain = false;

		// Save the minus one of worlds unlocked
		world_NewOne = PlayerData_Main.Instance.player_WorldsUnlocked;

		// Increase player's unlocked worlds
		PlayerData_Main.Instance.player_WorldsUnlocked++;

		// Increase player's unlocked worlds
		PlayerData_Main.Instance.player_LevelsUnlocked = 1;

		// Save new unlock! (Already save after animation)
//		SaveLoad.Save ();
	}


    public void Menu_HUD_Update () {
		SaveLoad.Save ();
		if (PlayerData_Main.Instance != null) {
            // Update Mustache
			Menu_Update_Mustache (PlayerData_Main.Instance.player_Mustache);

			// Update Stars UI (OLD)
//			Menu_Update_Stars (PlayerData_Main.Instance.player_Stars);
		}
	}

	public IEnumerator Menu_HUD_Update_NoSave () {
		yield return new WaitForSeconds (0.1F);

		if (PlayerData_Main.Instance != null) {
			// Update Mustache
			Menu_Update_Mustache (PlayerData_Main.Instance.player_Mustache);

			// Update Stars UI (OLD)
//			Menu_Update_Stars (PlayerData_Main.Instance.player_Stars);
		}
	}

	public void Menu_Update_Mustache (int newMustache) {
		menu_MustachePositVect2.x = 50 + newMustache.ToString ().Length * 13;
		menu_MustachePositVect2.y = 41;
		menu_MustacheHolderRecTransHolder_WorldMenu.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_ItemsMenu.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_QuestMenu.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_AchieveMenu.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_EndlessMenu.sizeDelta = menu_MustachePositVect2;
		//
		menu_MustacheHolderRecTransHolder_WorldMenu_Shadow.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_ItemsMenu_Shadow.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_AchieveMenu_Shadow.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_EndlessMenu_Shadow.sizeDelta = menu_MustachePositVect2;

//		menu_MustacheHolderRecTransHolder_ShopMenu.sizeDelta = menu_MustachePositVect2;

		stringMustacheAmountBuffer = newMustache.ToString ();

		text_Mustache_Holder_WorldsPage.text = stringMustacheAmountBuffer;
		text_Mustache_Holder_ItemsPage.text = stringMustacheAmountBuffer;
//		text_Mustache_Holder_ShopPage.text = stringMustacheAmountBuffer;
		text_Mustache_Holder_QuestPage.text = stringMustacheAmountBuffer;
		text_Mustache_Holder_AchievePage.text = stringMustacheAmountBuffer;
		text_Mustache_Holder_EndlessPage.text = stringMustacheAmountBuffer;
	}

	public void Menu_Update_Stars (int newStars) {
//		text_Stars_Holder.text = newStars.ToString ();
	}

	public void WorldAnim_LevelSelected_BackAnim () {
		allMenusAnimRef.Play ("Menu - To-Level Anim 3 (Legacy)");
	}

	public void WorldAnim_LevelSelected_MenuAnim () {
		animWorldsAllHolder.Play ("Menu - Level Pressed Play Anim 1 (Legacy)");
	}

	public void WorldParticle_FadeToBlack () {
		particleLevelSelected_FadeToBlackMenu.Emit (1);
	}

	// Fade to black for the first time
	public void WorldParticle_FadeToBlack_FirstTime () {
		particleLevelSelected_FadeToBlackMenuFirstTime.Emit (1);
	}

	public void WorldParticle_FullFlash () {
//		particleLevelSelected_FullFlashMenu.gameObject.SetActive (true);
		particleLevelSelected_FullFlashMenu.Play ();
	}

    public void Pressed_GoToLevelSeven()
    {
        if (PlayerData_Main.Instance !=null)
        {
            PlayerData_Main.Instance.player_LevelsUnlocked = 7;
            PlayerData_Main.Instance.player_PowUp1_Own = 4;
            PlayerData_Main.Instance.player_PowUp2_Own = 4;
            for (int i = 0; i < 6; i++)
            {
                PlayerData_Main.Instance.player_World1_LevelsDataArr[i].star_Unlocked_Level = true;
            }
            PlayerData_Main.Instance.TutSeen_BatteryCharge = true;
            PlayerData_Main.Instance.TutSeen_ItemProgress = true;
            PlayerData_Main.Instance.TutSeen_ItemRepeatItem = true;
            SaveLoad.Save();
        }

    }

	public void Pressed_Play () {
		StartCoroutine ( PlayerData_Main.Instance.LoadScene_Async_Slow ("InGame_Scene"));

//		Application.backgroundLoadingPriority = ThreadPriority.Low;
//		SceneManager.LoadSceneAsync ("InGame_Scene"); 
//		Application.backgroundLoadingPriority = ThreadPriority.Low;
	}

	public void Menu_ActivateLevels () {
		worldsAllScriptHolder.Activate_Levels_All ();
	}

	public void Menu_DeActivateLevels () {
		worldsAllScriptHolder.DeActivate_Levels_All ();
	}
		
	public void GetNumber_World (int worldNumber) {
		// Set Scroll to zero
		ScrollLevels_Set_Zero ();

		// Deactivate levels upon opening menu
//		Menu_DeActivateLevels ();

		// Disable battery front deactivate
		objBatteryDisableFrontHolder.SetActive (false);

        //		powUpsMenuScriptHolder.BatteryCheck ();

        // SOUNDD EFFECTS - Clicked On World Card (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.TapEntekhabMarhale.Play();
        }

		world_NumberSelected = worldNumber;
		switch (world_NumberSelected) {
		case 1:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World1_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 2:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World2_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 3:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World3_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 4:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World4_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		case 5:
			worldLevelData_ClassRef = PlayerData_Main.Instance.player_World5_LevelsDataArr;
			PlayerData_Main.Instance.LevelsPerWorld = 10;
			break;
		}

		// Update World Number & Name
		SetWorldNumberAndName ();

		// Activate levels BEFORE setting them up via co-routines
//		Menu_ActivateLevels ();
			
		// Setup Levels
		StartCoroutine (ActivateAndDeActivateLevels ());

		// Tut check new place
		menuTutCheckScriptHolder.Menu_TutorialCheck (PlayerData_Main.Instance.player_WorldsUnlocked, PlayerData_Main.Instance.player_LevelsUnlocked - 1 );

//		Debug.LogError ("TUT CHECK === " + "LAST world = " + PlayerData_Main.Instance.WorldNumber_Last + "   Player Level Unlocked = " + PlayerData_Main.Instance.player_LevelsUnlocked);

		// Call PowerUps Menu Parent (Bring up battery)
		popUpPowUpsParScript.PowerUpsMenu_Enter ();

		// Scroll levels (old place)

		// This is for back button in world / level select (The other is in the check_newworld)
		isLevelsInFront = true;

		// Tut check for endless tut (SPECIAL)
		StartCoroutine (LevelSelect_ToEndlessTut ());
	}

	public IEnumerator LevelSelect_ToEndlessTut () {
		yield return null;
		if (menuTutCheckScriptHolder.Menu_TutorialCheck_EndlessBackFromLevel ()) {
			yield return null;
			// This is for endless tut
			menuTutCheckScriptHolder.Menu_TutorialCheck_EndlessIntro ();

			// Event Check - No need here

			yield return new WaitForSeconds (1.3F);
			worldsBackButtonHolder.onClick.Invoke ();

			yield return new WaitForSeconds (0.6F);
			worldsBackButtonHolder.onClick.Invoke ();

			yield return null;
			Check_AllMiniCutsceneEnd ();
		}
	}

	public void Pressed_LevelSelect_PowUpsLeave () {
		popUpPowUpsParScript.PowerUpsMenu_Leave ();
	}

	public void FirstTime_WorldAndLevelSet () {
		world_NumberSelected = 1;
		level_NumberSelected = 1;

		SetBothNumberAndName_FirstTime ();
//		SetWorldNumberAndName ();
//		SetLevelNumberAndName ();

		NewestLevel_Check ();
		WorldParticle_FadeToBlack_FirstTime ();

		// Start Loading
//		Loading_Start ();
		Invoke ("Loading_Start", 0.8F);

		// Only speed up loading anim
		StartLoading_HideTheDamnBack_SpeedPart ();

		// Destroy loader (Only if the last scene was not in-game)
		Invoke ("StartLoading_HideTheDamnBack_BackPart" , 0.8F);

		Invoke ("Pressed_Play", 2);
//		Pressed_Play ();
	}

	public void IntroClickBlocker_Start () {
		miniCutscenesScriptHolder.MiniCutscenes_CanvasEnable ();
		IntroStartClickBlocker1Holder.SetActive (true);
		IntroStartClickBlocker2Holder.SetActive (true);
	}

	public void IntroClickBlocker_End () {
		if (PlayerData_Main.Instance != null) {
			// If it is not VAS
			if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
				// Should be inside DailyRewardCheck
//				IntroClickBlocker_DisableNOW ();

				DailyRewardCheck ();
			}

			// It is VAS
			else {
//				VAS_MenuController_Script.Instance.VAS_Controller_Activator (true);
//				StartCoroutine (VAS_MenuController_Script.Instance.VAS_Controller_DelayActive ());

				if (PlayerData_Main.Instance.isFullBuild_VAS_Cheat) {
					DailyRewardCheck ();
				} else {

					// VAS Canvas Activator is inside loading circle
					VAS_MenuController_Script.Instance.CheckSubscription (); 
				}
			}
		}
	}

	public void IntroClickBlocker_DisableNOW () {
		miniCutscenesScriptHolder.MiniCutscenes_CanvasDisable_Delayed ();
		IntroStartClickBlocker1Holder.SetActive (false);
		IntroStartClickBlocker2Holder.SetActive (false);
	}

	public void DailyRewardCheck () {
		StartCoroutine (DailyReward_CheckingRoutine ());

		// Moved the Achievement Prev check to here (From menu)
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.Achievements_CheckForPrevRoutine ());
		}

		// Moved from Start of QuestManager
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestGiverRoutine ());
		}
	}


	// World 3 star move to worlds all script

	public IEnumerator Main_NotifsUpdate () {
		yield return new WaitForSeconds (0.5F);

		// Phone notif stats 
		Phone_Stats_Script.Instance.NotifStats_Update ();
	}

	public IEnumerator DailyReward_CheckingRoutine () {

		IntroClickBlocker_DisableNOW ();

		yield return null;

		// Check to make sure this is NOT the first time in the menu
//		if (!PlayerData_Main.Instance.isFirstToMenu) {
//		if (true) {

		if (afterLoadDailyScriptHolder.IsEligibleForDailyReward ()) {
			objDailyRewardHolder.SetActive (true);

			// This is set to true so when player has daily reward, the endless tut check can happen AFTER its reward was given
			menuTutCheckScriptHolder.canCheckForEndlessTut = true;

			// SOUND EFFECTS - opening paper of daily
			if (SoundManager.Instance != null) {
				SoundManager.Instance.PopupOpenPaper.Play ();
			}
		}

		else {
			// This WAS because the first time to menu
			afterLoadDailyScriptHolder.SetDateAfterVid ();

			// This is for endless tut and event
			AfterDailyCheck_EndlessAndEvent ();
		}

		// This is to make sure daily stuff begins after

//		if (PlayerData_Main.Instance != null && (PlayerData_Main.Instance.player_World1_LevelsDataArr[1].star_Unlocked_Level)) {
//			if (afterLoadDailyScriptHolder.IsEligibleForDailyReward ()) {
//				objDailyRewardHolder.SetActive (true);
//			}
//		}
	}

	public void AfterDailyCheck_EndlessAndEvent () {
		// This is for endless tut
		menuTutCheckScriptHolder.Menu_TutorialCheck_EndlessIntro ();

		// Event Check (Before it, we check to see if there is NO tut stuff happening)
		if (!menuTutCheckScriptHolder.isEndlessTutHappening) {
			Event_Prepare ();
		}
	}

	public void StartLoading_HideTheDamnBack () {
		FindObjectOfType<Loader_Script> ().HideTheDamnBlackBack ();
        Debug.Log("StartLoading_HideTheDamnBack");
    }

	public void StartLoading_HideTheDamnBack_SpeedPart () {
        FindObjectOfType<Loader_Script> ().HideTheDamnBlackBack_SpeedPartOnly ();
        Debug.Log("StartLoading_HideTheDamnBack_SpeedPart");
    }

	public void StartLoading_HideTheDamnBack_BackPart () {
		FindObjectOfType<Loader_Script> ().HideTheDamnBlackBack_BackPartOnly ();
        Debug.Log("StartLoading_HideTheDamnBack_BackPart");
    }

	// For Level Load Stuff 1
	public void SetWorldNumberAndName () {

//		Debug.LogError ("Selected world = " + world_NumberSelected);

		PlayerData_Main.Instance.WorldNumber = world_NumberSelected;
		PlayerData_Main.Instance.GetFullName_World ();
	}

	// For Level Load Stuff 2
	public void SetLevelNumberAndName () {
		PlayerData_Main.Instance.LevelNumber = level_NumberSelected;
		PlayerData_Main.Instance.GetFullName_Level ();
		CheckForFirstLevel ();
	}

	// For Level Load Stuff 3
	public void SetBothNumberAndName_FirstTime () {
		PlayerData_Main.Instance.WorldNumber = world_NumberSelected;
		PlayerData_Main.Instance.LevelNumber = level_NumberSelected;
		PlayerData_Main.Instance.GetFullName_FirstTimeBoth ();
		CheckForFirstLevel ();
	}

	// For Level Load Stuff 4
	public void NewestLevel_Check () {
		//        Debug.LogWarning("Numberrrrr = " + level_NumberSelected + " and unlocked " + PlayerData_Main.Instance.player_LevelsUnlocked);

		PlayerData_Main.Instance.PlayingNewestLevel = false;
		PlayerData_Main.Instance.PlayingALevelForTheFirstTime = false;

		if (PlayerData_Main.Instance.player_WorldsUnlocked == world_NumberSelected) {
			if (level_NumberSelected == PlayerData_Main.Instance.player_LevelsUnlocked) {
				if (NewestLevel_WorldFinder ()) {
					PlayerData_Main.Instance.PlayingNewestLevel = true;
					PlayerData_Main.Instance.PlayingALevelForTheFirstTime = true;
				}
			} 
//			else {
//				PlayerData_Main.Instance.PlayingNewestLevel = false;
//				PlayerData_Main.Instance.PlayingALevelForTheFirstTime = false;
//			}
		} 

//		else {
//			PlayerData_Main.Instance.PlayingNewestLevel = false;
//			PlayerData_Main.Instance.PlayingALevelForTheFirstTime = false;
//		}

//		Debug.LogError ("FIRST TIME = " + PlayerData_Main.Instance.PlayingALevelForTheFirstTime);
	}

	public bool NewestLevel_WorldFinder () {
		switch (world_NumberSelected) {
		case 1:
			if (!PlayerData_Main.Instance.player_World1_LevelsDataArr [level_NumberSelected - 1].star_Unlocked_Level) {
				return true;
			} else {
				return false;
			}
			break;
		case 2:
			if (!PlayerData_Main.Instance.player_World2_LevelsDataArr [level_NumberSelected - 1].star_Unlocked_Level) {
				return true;
			} else {
				return false;
			}
			break;
		case 3:
			if (!PlayerData_Main.Instance.player_World3_LevelsDataArr [level_NumberSelected - 1].star_Unlocked_Level) {
				return true;
			} else {
				return false;
			}
			break;
		case 4:
			if (!PlayerData_Main.Instance.player_World4_LevelsDataArr [level_NumberSelected - 1].star_Unlocked_Level) {
				return true;
			} else {
				return false;
			}
			break;
		case 5:
			if (!PlayerData_Main.Instance.player_World5_LevelsDataArr [level_NumberSelected - 1].star_Unlocked_Level) {
				return true;
			} else {
				return false;
			}
			break;

		default:
			return false;
			break;
		}
	}

	public IEnumerator ActivateAndDeActivateLevels () {
		// Reresh levelButtonScriptArray & check world 1 vs world 2-5 for different number of levels
//		levelButtonScriptRefArr = new Level_Button_Script[worldLevelData_ClassRef.Length];

		#region NEW METHOD

		int intCountLevels;

		// No scroll OR cutscene for old worlds!
		if (world_NumberSelected != PlayerData_Main.Instance.player_WorldsUnlocked) {
			inNewestWorld = false;
			intCountLevels = 10;
		} else {
			inNewestWorld = true;
			intCountLevels = PlayerData_Main.Instance.player_LevelsUnlocked;
		}

		for (int i = 0; i < levelButtonScriptRefArr.Length; i++) {
			yield return null;
			if (i < intCountLevels) {
				levelButtonScriptRefArr [i].LevelStatus_Unlock ();

				//				Debug.LogWarning ("Stars - Level: " + 
				//					worldLevelData_ClassRef [i].star_Unlocked_Level + "  Untouchable: " +
				//					worldLevelData_ClassRef [i].star_Unlocked_Untouchable + "  Unmissable: " +
				//					worldLevelData_ClassRef [i].star_Unlocked_Unmissable);

				levelButtonScriptRefArr [i].LevelStars_Show (
					worldLevelData_ClassRef [i].star_Unlocked_Level,
					worldLevelData_ClassRef [i].star_Unlocked_Untouchable, 
					worldLevelData_ClassRef [i].star_Unlocked_Unmissable
				);
			} 

			// Lock all after certain number
			else {
				levelButtonScriptRefArr [i].LevelStatus_Lock ();
				levelButtonScriptRefArr [i].LevelStars_Show (false, false, false);
				levelButtonScriptRefArr [i].LevelButton_BackGlow (false);
			}
		}

		// Open / unlock new levelo
		if (inNewestWorld) {
			// Precheck in case the new level uuuunlock should happen
			if (PlayerData_Main.Instance.Show_NewLevel_Allow) {
				Debug.Log ("Yo NEW Level UNLOCK ANIMMMMMM!!!");
				newestLevelUnlockPreAnimHappened = true;

				if (!isMiniCutsceneOn) {
					StartCoroutine (ScreenBlocker_MiniCutscene (1));
				}
			}
		}
			
		#endregion

		// PREV / OLD method of unlocking level
		/*

		// 5 levels or 10 levels
		for (int i = 0; i < 10; i++) {
			yield return null;
			if (i < levelButtonScriptRefArr.Length) {
				levelButtonScriptRefArr[i].gameObject.SetActive (true);

				// Old way of enabling
				//				levels_ContentTransHolder.GetChild (i).gameObject.SetActive (true);
				//				levelButtonScriptRefArr = levels_ContentTransHolder.GetComponentsInChildren<Level_Button_Script> ();

				levelButtonScriptRefArr [i].LevelStatus_Lock ();
				levelButtonScriptRefArr [i].LevelStars_Show (false, false, false);
				levelButtonScriptRefArr [i].LevelButton_BackGlow (false);
			}

			else {
				levelButtonScriptRefArr[i].gameObject.SetActive (false);

				// Old way of disabling
				//				levels_ContentTransHolder.GetChild (i).gameObject.SetActive (false);
			}
		}




		// Check if we are at newest level
		if (world_NumberSelected != PlayerData_Main.Instance.player_WorldsUnlocked) {
			for (int i = 0; i < levelButtonScriptRefArr.Length; i++) {
//				levelButtonScriptRefArr = levels_ContentTransHolder.GetComponentsInChildren<Level_Button_Script> ();

				levelButtonScriptRefArr [i].LevelStatus_Unlock ();

//				Debug.LogWarning ("Stars - Level: " + 
//					worldLevelData_ClassRef [i].star_Unlocked_Level + "  Untouchable: " +
//					worldLevelData_ClassRef [i].star_Unlocked_Untouchable + "  Unmissable: " +
//					worldLevelData_ClassRef [i].star_Unlocked_Unmissable);
				
				levelButtonScriptRefArr [i].LevelStars_Show (
					worldLevelData_ClassRef [i].star_Unlocked_Level,
					worldLevelData_ClassRef [i].star_Unlocked_Untouchable, 
					worldLevelData_ClassRef [i].star_Unlocked_Unmissable);
			}

			// No scroll for old worlds!
			inNewestWorld = false;

//			ScrollLevels_Set_OldWorlds ();
		} 

		// Playing in the newest world
		else {
			for (int i = 0; i < PlayerData_Main.Instance.player_LevelsUnlocked; i++) {
				levelButtonScriptRefArr [i].LevelStatus_Unlock ();
				levelButtonScriptRefArr [i].LevelStars_Show (
					worldLevelData_ClassRef [i].star_Unlocked_Level,
					worldLevelData_ClassRef [i].star_Unlocked_Untouchable, 
					worldLevelData_ClassRef [i].star_Unlocked_Unmissable);
			}

			// Precheck in case the new level uuuunlock should happen
			if (PlayerData_Main.Instance.Show_NewLevel_Allow) {
				Debug.Log ("Yo NEW Level UNLOCK ANIMMMMMM!!!");
				newestLevelUnlockPreAnimHappened = true;

				if (!isMiniCutsceneOn) {
					StartCoroutine (ScreenBlocker_MiniCutscene (1));
				}

//				levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].transform.GetChild(1).transform.localScale = new Vector3 (3.1F, 3.1F, 3.1F);
			}

			// In newest level (Scroll comes!)
			inNewestWorld = true;

//			StartCoroutine (ScrollLevels_Set_NewWorlds (0.25F));
		}
		*/

		yield return null;

		// Play idle anim of levels in case there is no cutscene (Regardless of newest level or not)
		if (!isMiniCutsceneOn) {
			StartCoroutine (LevelsIdle_Animate ());
		}

		// Newest Level and world check result
		if (!inNewestWorld) {
			// In an older level
			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].LevelButton_BackGlow (false);
			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].PlayLevelAnim_Stop ();
		} else {
			// In newest world
//			Debug.LogWarning ("New level");

			if (!newestLevelUnlockPreAnimHappened) {
				//			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].transform.GetChild(1).localScale = new Vector3 (1.04F, 1.04F, 1.04F);
				levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].LevelButton_BackGlow (true);
				levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].PlayLevelAnim_Idle ();
			} else {
				levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].LevelStatus_Minilock ();
//				levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].LevelStatus_SquashFront ();
			}
		}
	}
		
	public void Level_UnlockNewAnimate () {
		levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].PlayLevelAnim_UnlockLevel ();
	}
		
	public void Level_UnlockLightNow () {
		if (newestLevelUnlockPreAnimHappened) {
			//			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].transform.GetChild(1).localScale = new Vector3 (1.04F, 1.04F, 1.04F);
			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].LevelButton_BackGlow (true);
			levelButtonScriptRefArr [PlayerData_Main.Instance.player_LevelsUnlocked - 1].PlayLevelAnim_Idle ();
			newestLevelUnlockPreAnimHappened = false;
		}
	}

	public void Level_SendAwayCards () {
//		text_PopUp_LevelNumber
	}

//	public void Yo () {
//		Debug.Log ("Yo");
//	}

	public void CheckForFirstLevel () {
		if (level_NumberSelected == 1) {
			PlayerData_Main.Instance.introAdditionTime = 4;
		} else {
			PlayerData_Main.Instance.introAdditionTime = 0;
		}
	}

	public IEnumerator Prepare_MenuToInGame () {
		// Disable phone vib
		phoneShortCutScriptHolder.PhoneSC_VibEnd ();

		// SOUNDD EFFECTS - Clicked On Level Card (need to change probably)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.TapRadKardaneIntro.Play();
			SoundManager.Instance.ArbadeLoading.PlayDelayed (0.8F);
		}

		// Enable Back Game Objects
		beganZoomIn = false;

		// Zoom & Background art return
		StartCoroutine (ZoomEnable_Now ());
		Invoke ("BackDarker_BlackToWhite", 0.5F);

		// Play All Menu Zoom-out (Body) Anim
		if (PlayerData_Main.Instance.gameMainSpawnType == PlayerData_InGame.GameSpawnType.Campaign) {
			// World menu to in-game anim
			WorldAnim_LevelSelected_MenuAnim ();
		} 

		// Endless menu to in-game anim (Don't need it here. Now straight from Endless Play Button)
//		else {
//			menuBackAnimsScriptHolder.endlessAllScriptHolder.EndlessAnimToIngame ();
//		}

		yield return null;

		// Reset all anims for this
		menuBackAnimsScriptHolder.MenuAnims_AllBody_Reset ();

		// Remove power ups
		Pressed_LevelSelect_PowUpsLeave ();

		// Menu back to level anim
		Invoke("WorldAnim_LevelSelected_BackAnim", 0.1F);

		// Fade to black for menu
		Invoke("WorldParticle_FadeToBlack", 0.3F);

//		particleLevelSelected_PipeLinesMenu.Play ();
		particleLevelSelected_PipeMomentMenu.Play ();

		WorldParticle_FullFlash ();	

		yield return null;

		// Call Load (ACTUAL) & Loading art / object
		Invoke ("Pressed_Play", 2.4F);
		//		Invoke ("Loading_Start", 1.3F);
		Invoke ("Loading_Start", 1.6F);
	}

	public void GetNumber_Level (int levelNumber) {
		// Set spawner type for campaign or endless
		PlayerData_Main.Instance.gameMainSpawnType = PlayerData_InGame.GameSpawnType.Campaign;

		// Disable scroll of levels
		scrollLevelsHolder.enabled = false;

		level_NumberSelected = levelNumber;

		// Sets levels number and name
		SetLevelNumberAndName ();

//		text_PopUp_LevelNumber.text = levelNumber.ToString();

		levelSelectSideCounter = 0;
		for (int i = (level_NumberSelected - 1); i > -1 ; i--) {
//			Debug.LogError ("left i = " + i);
			levelButtonScriptRefArr [i].PlayLevelAnim_ThrowAway_Left (Number_Reducer(levelSelectSideCounter));
			levelSelectSideCounter++;
		}

		levelSelectSideCounter = 0;
		for (int i = (level_NumberSelected - 1); i < 10 ; i++) {
//			Debug.LogError ("right i = " + i);
			levelButtonScriptRefArr [i].PlayLevelAnim_ThrowAway_Right (Number_Reducer(levelSelectSideCounter));
			levelSelectSideCounter++;
		}

		levelButtonScriptRefArr [level_NumberSelected - 1].PlayLevelAnim_Selected ();

		NewestLevel_Check ();

		StartCoroutine (Prepare_MenuToInGame ());
	}

	public int Number_Reducer (int number) {
		if (number < 5) {
			return number;
		} else {
			return 4;
//			number = number - 5;
//			return Number_Reducer (number);
		}
	}

	public void ScrollWorlds_Set_NextWorld_BlendStart () {
		scrollWorldsNormalizedFloat_IncreaseAmount = 0;
		scrollWorldsNormalizedFloat_Curr = scrollWorldsHolder.horizontalNormalizedPosition;
//		whichWorld
//		scrollWorldsNormalizedFloat_Target = (float)(whichWorld) * 0.2F + 0.04F;
		scrollFloatBuffer = (float)PlayerData_Main.Instance.player_WorldsUnlocked;
		scrollWorldsNormalizedFloat_Target = scrollFloatBuffer * 0.243F;

		if (scrollWorldsNormalizedFloat_Curr < scrollWorldsNormalizedFloat_Target) {
			StartCoroutine (ScrollWorlds_Set_NextWorld_BlendGO_UP ());
		} else {
			StartCoroutine (ScrollWorlds_Set_NextWorld_BlendGO_DOWN ());
		}
	}

	public IEnumerator ScrollWorlds_Set_NextWorld_BlendGO_UP () {
		yield return new WaitForSeconds (0.01F);

//		Debug.LogWarning ("Going Up!");

		if (scrollWorldsNormalizedFloat_Curr < scrollWorldsNormalizedFloat_Target) {
			if (scrollWorldsNormalizedFloat_IncreaseAmount < 0.03F) {
				scrollWorldsNormalizedFloat_IncreaseAmount += 0.0004F;
			}

			scrollWorldsNormalizedFloat_Curr += scrollWorldsNormalizedFloat_IncreaseAmount;
			scrollWorldsHolder.horizontalNormalizedPosition = scrollWorldsNormalizedFloat_Curr;
			StartCoroutine (ScrollWorlds_Set_NextWorld_BlendGO_UP ());
		} else {
			ScrollWorlds_Set_NextWorld_Instant ();
		}
	}

	public IEnumerator ScrollWorlds_Set_NextWorld_BlendGO_DOWN () {
		yield return new WaitForSeconds (0.01F);
		Debug.LogWarning ("Going Down!");
		if (scrollWorldsNormalizedFloat_Curr > scrollWorldsNormalizedFloat_Target) {
			if (scrollWorldsNormalizedFloat_IncreaseAmount < 0.03F) {
				scrollWorldsNormalizedFloat_IncreaseAmount += 0.0004F;
			}

			scrollWorldsNormalizedFloat_Curr -= scrollWorldsNormalizedFloat_IncreaseAmount;
			scrollWorldsHolder.horizontalNormalizedPosition = scrollWorldsNormalizedFloat_Curr;
			StartCoroutine (ScrollWorlds_Set_NextWorld_BlendGO_DOWN ());
		} else {
			ScrollWorlds_Set_NextWorld_Instant ();
		}
	}

	public void ScrollWorlds_Set_NextWorld_Instant () {
//		scrollWorldsHolder.horizontalNormalizedPosition = 0.2F + 0.04F;
		scrollWorldsHolder.horizontalNormalizedPosition = scrollWorldsNormalizedFloat_Target;
//		Debug.LogWarning ("Scroll is at: " + scrollWorldsHolder.horizontalNormalizedPosition);
	}

	public void ScrollWorlds_Set_CurrWorld () {
//		Debug.LogWarning ("Cur world?");
		//		scrollWorldsHolder.horizontalNormalizedPosition = 0.2F + 0.04F;
		scrollWorldsHolder.horizontalNormalizedPosition = (float)(PlayerData_Main.Instance.player_WorldsUnlocked - 1) * 0.2F + 0.04F;
		//		Debug.LogWarning ("Scroll is at: " + scrollWorldsHolder.horizontalNormalizedPosition);
	}

	public void ScrollLevels_Set_OldWorlds () {
		scrollLevelsHolder.horizontalNormalizedPosition = (float)(PlayerData_Main.Instance.player_LevelsUnlocked) * 0.2F + 0.04F;
	}

	public IEnumerator ScrollLevels_Set_BlendStart (float delay) {
		
		// Disable scroll during level scroll
		scrollLevelsHolder.enabled = false;

		yield return new WaitForSeconds (delay);

//		Debug.Log ("Scroll levels! START");
		scrollLevelsHolder.enabled = true;

		scrollFloatBuffer = (float)PlayerData_Main.Instance.player_LevelsUnlocked;
		scrollLevelNormalizedFloat_Curr = scrollLevelsHolder.horizontalNormalizedPosition;
		scrollLevelNormalizedFloat_Target = (scrollFloatBuffer - 1) * 0.1F;
		if (scrollFloatBuffer < 4) {
			scrollFloatBuffer = scrollFloatBuffer + 2;
		} else {
			if (scrollFloatBuffer < 6) {
				scrollFloatBuffer = scrollFloatBuffer + 1;
			}
		}

//		scrollLevelNormalizedFloat_IncreaseAmount = 0.01F;
//		scrollLevelNormalizedFloat_IncreaseAmount = ((scrollFloatBuffer - 1) / 10F) * 0.03F;
//		scrollLevelsHolder.horizontalNormalizedPosition = (float)(PlayerData_Main.Instance.player_LevelsUnlocked - 1) * 0.5F;

		allowLevelAutoScroll = true;

		while (allowLevelAutoScroll) {
			StartCoroutine (ScrollLevels_Set_BlendGoingNEW ());
			yield return new WaitForSeconds (Time.deltaTime / 2);
		}
			
		ScrollLevels_End ();
	}

	public IEnumerator ScrollLevels_Set_BlendGoingNEW () {
		if (scrollLevelNormalizedFloat_Curr < scrollLevelNormalizedFloat_Target) {
//			scrollLevelNormalizedFloat_Curr += scrollLevelNormalizedFloat_IncreaseAmount;

			if (scrollLevelNormalizedFloat_Curr < 0.15F) {
				scrollLevelNormalizedFloat_Curr += 0.019F;
			} else {
				if (scrollLevelNormalizedFloat_Curr < 0.3F) {
					scrollLevelNormalizedFloat_Curr += 0.022F;
				} else {
					if (scrollLevelNormalizedFloat_Curr < 0.45F) {
						scrollLevelNormalizedFloat_Curr += 0.025F;
					} else {
						if (scrollLevelNormalizedFloat_Curr < 0.6F) {
							scrollLevelNormalizedFloat_Curr += 0.028F;
						} else {
							scrollLevelNormalizedFloat_Curr += 0.03F;
						}
					}
				}
			}

			scrollLevelsHolder.horizontalNormalizedPosition = scrollLevelNormalizedFloat_Curr;
		} else {
			allowLevelAutoScroll = false;
		}
		yield return null;
	}

	public IEnumerator ScrollLevels_Set_BlendGoingOLD () {
//		Debug.Log ("ScrollING levels");
		yield return new WaitForSeconds (0.004F);
		if (allowLevelAutoScroll) {
			if (scrollLevelNormalizedFloat_Curr < scrollLevelNormalizedFloat_Target) {
			
//			if (scrollLevelNormalizedFloat_IncreaseAmount < ((scrollFloatBuffer - 1) / 10F) * 0.03F) {
//			if (scrollLevelNormalizedFloat_IncreaseAmount < 0.008F) {
//				scrollLevelNormalizedFloat_IncreaseAmount += 0.0002F;
//			}
					
				scrollLevelNormalizedFloat_Curr += scrollLevelNormalizedFloat_IncreaseAmount;
				scrollLevelsHolder.horizontalNormalizedPosition = scrollLevelNormalizedFloat_Curr;
				StartCoroutine (ScrollLevels_Set_BlendGoingOLD ());
			} else {
				allowLevelAutoScroll = false;
				ScrollLevels_End ();
			}
		}
	}

	public void AutoScrollBlend_Stop () {
//		Debug.Log ("Yo PRESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS STOP!");
		if (allowLevelAutoScroll) {
			allowLevelAutoScroll = false;
		}
	}

	public void ScrollLevels_End () {
		// This checks for newest level AFTER returning from menu
		if (newestLevelUnlockPreAnimHappened) {
			newestLevelUnlockPreAnimHappened = false;

			Level_UnlockNewAnimate ();

			PlayerData_Main.Instance.Show_NewLevel_Allow = false;

			// Can save after new level allow has returned to false
			SaveLoad.Save ();

//			ActivateAndDeActivateLevels ();
		}
	}

	public void ScrollLevels_Set_Instant () {
		scrollLevelsHolder.horizontalNormalizedPosition = (float)(PlayerData_Main.Instance.player_LevelsUnlocked) * 0.1F;
	}

	public void ScrollLevels_Set_Zero () {
		scrollLevelsHolder.horizontalNormalizedPosition = 0;
	}

	public void SaveLoad_Now () {
		SaveLoad.Save();
	}

	void Loading_Start () {
		if (Loading_Anim_Script.Instance != null) {
			Debug.LogError ("LOADING!");
			StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_Start (1.2F));
			StartCoroutine (Loading_Anim_Script.Instance.Loading_TextsUpdate ());
		}
	}

	void SaveLoad_Late () {
//		Debug.LogWarning ("Before Save: " + System.DateTime.Now.Ticks);
		SaveLoad_Now ();
//		Debug.LogWarning ("After Save: " + System.DateTime.Now.Ticks);
	}

	void PopUp_Stars_Activate (LevelData_Class[] levelDataArr) {
		starObj_1.SetActive (levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Level);
		starObj_2.SetActive (levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Untouchable);
		starObj_3.SetActive (levelDataArr [PlayerData_Main.Instance.LevelNumber - 1].star_Unlocked_Unmissable);
	}

	void PopUp_Stars_Setup () {
		// Check world to find world NUMBER for levelDataArray
		switch (PlayerData_Main.Instance.WorldNumber) {
		case 1:
			PopUp_Stars_Activate (PlayerData_Main.Instance.player_World1_LevelsDataArr);
			break;
		case 2:
			PopUp_Stars_Activate (PlayerData_Main.Instance.player_World2_LevelsDataArr);
			break;
		case 3:
			PopUp_Stars_Activate (PlayerData_Main.Instance.player_World3_LevelsDataArr);
			break;
		case 4:
			PopUp_Stars_Activate (PlayerData_Main.Instance.player_World4_LevelsDataArr);
			break;
		case 5:
			PopUp_Stars_Activate (PlayerData_Main.Instance.player_World5_LevelsDataArr);
			break;
		}
	}

	public void Pressed_NewWorld_ByMustache () {
		Debug.Log ("PRICE: " + world_CurrentPrice_Mustache);

		// Check to see if player can Unlock New World By Mustache
		if (PlayerData_Main.Instance.player_Mustache > world_CurrentPrice_Mustache) {
			PlayerData_Main.Instance.Player_Mustache_Add (-world_CurrentPrice_Mustache);
			Menu_HUD_Update ();

			// Call General Unlocker
			NewWorld_UnlockAnimate (WorldUnlockType.ToFree_Mustache);

			// Update info for worlds
			NewWorld_UpdateInfo ();

			// Save after unlocking world by key
			SaveLoad.Save ();

			Debug.LogError ("UNLOCK BY KEY");
//			NewWorld_Unlock_OLD ();
		} else {
			GoTellShop_FromMenu ();
			Debug.LogError ("Show Shop");
		}
	}

	public void GoTellShop_FromMenu () {
		StartCoroutine (GoTellShop_FromMenu_Now());
	}


	public IEnumerator GoTellShop_FromMenu_Now () {
		mustacheDisplayScriptHolder.Mustache_TellGoShop ();
		yield return null;
	}

	public void NewWorld_Unlock_NEW () {
		// Falsify show new world button
		PlayerData_Main.Instance.Show_NewWorld_Allow = false;

		// Update World Buttons
		DeActivateWorlds ();
		ActivateWorlds ();

		// Update player unlocked amounts
		PlayerData_Main.Instance.player_WorldsUnlocked++;
		PlayerData_Main.Instance.player_LevelsUnlocked = 1;

		// Check for unlock new world
		Check_NewWorld ();

		// Update HUD (Mustache Display)
		Menu_HUD_Update ();
	}

	public void NewWorld_Unlock_OLD () {
		// Falsify show new world button
		PlayerData_Main.Instance.Show_NewWorld_Allow = false;

		// Remove the New world Button
		worlds_ContentTransHolder.GetChild (PlayerData_Main.Instance.player_WorldsUnlocked - 1)
			.GetChild (10).gameObject.SetActive (false);

		// Update mustache amount
		//PlayerData_Main.Instance.Player_Mustache_Add (-world_2_Price_Mustache);

        Debug.Log("world unlock + 1 :"+PlayerData_Main.Instance.player_WorldsUnlocked + 1);

		// Update World Buttons
		DeActivateWorlds ();
		ActivateWorlds ();

        // Update player unlocked amounts
        PlayerData_Main.Instance.player_WorldsUnlocked++;
		PlayerData_Main.Instance.player_LevelsUnlocked = 1;


		// Check for unlock new world
		Check_NewWorld ();

		// Update HUD (Mustache Display)
		Menu_HUD_Update ();
	}

	public void Pressed_NewWorld_ByStar () {
		// Check to see if player can Unlock New World By Stars
		if (PlayerData_Main.Instance.player_Stars > world_2_Price_Star) {
			NewWorld_Unlock_OLD ();
		} else {
			Debug.LogError ("Show Shop");
		}
	}

	public void ZoomIn_CheckBack () {
		DontDeleteBackItems_False ();
		ZoomIn_Success ();
	}

	public void ZoomIn_CheckBack_DontDelete () {
		DontDeleteBackItems_True ();
		ZoomIn_Success ();
	}

	public void ZoomOut_OnlyMenuActivator () {
		// Menu Elements Activator
		StartCoroutine (MenuActivator_FrontMains_DeActive (0));
	}

	public void ZoomOut_CheckBack (int whichInt) {

		switch (whichInt) {
		case 1:
			// 1 = Items
			menuBackAnimsScriptHolder.isFront_Items = false;
			break;
		case 2:
			// 2 = Worlds
			menuBackAnimsScriptHolder.isFront_Worlds = false;
			break;
		case 3:
			// 3 = Shop
			menuBackAnimsScriptHolder.isFront_Shop = false;
			break;
		case 4:
			// 4 = Settings
			menuBackAnimsScriptHolder.isFront_Settings = false;
			break;
		case 5:
			// 5 = Achieves
			menuBackAnimsScriptHolder.isFront_Achieves = false;
			break;
		case 6:
			// 6 = VidGift
			menuBackAnimsScriptHolder.isFront_VidGift = false;
			break;
		case 7:
			// 7 = Endless
			menuBackAnimsScriptHolder.isFront_Endless = false;
			break;
		default:
			break;
		}

		if (
			!menuBackAnimsScriptHolder.isFront_Items &&
			!menuBackAnimsScriptHolder.isFront_Worlds &&
			!menuBackAnimsScriptHolder.isFront_Shop &&
			!menuBackAnimsScriptHolder.isFront_Settings &&
			!menuBackAnimsScriptHolder.isFront_Achieves &&
			!menuBackAnimsScriptHolder.isFront_VidGift &&
			!menuBackAnimsScriptHolder.isFront_Endless
		) 
			{			
			// Result of if above
			DontDeleteBackItems_False ();
			ZoomOut_Success ();
		}
	}

	public void ZoomOut_CheckBack_Obj (int whichInt) {
		switch (whichInt) {
		case 1:
			if (!objShopMenuParnentHolder.activeInHierarchy &&
				!objWorldsMenuParnentHolder.activeInHierarchy &&
				!objLevelsSelectParentHolder.activeInHierarchy &&
				!objSettingsMenuParnentHolder.activeInHierarchy) 
			{
				ZoomOut_Success ();
			}
			break;
		case 2:
			if (!objShopMenuParnentHolder.activeInHierarchy &&
				!objItemsMenuParnentHolder.activeInHierarchy &&
				!objSettingsMenuParnentHolder.activeInHierarchy) 
			{
				ZoomOut_Success ();
			}
			break;
		case 3:
			if (!objItemsMenuParnentHolder.activeInHierarchy &&
				!objWorldsMenuParnentHolder.activeInHierarchy &&
				!objLevelsSelectParentHolder.activeInHierarchy &&
				!objSettingsMenuParnentHolder.activeInHierarchy) 
			{
				ZoomOut_Success ();
			}
			break;
		case 4:
			if (!objShopMenuParnentHolder.activeInHierarchy &&
				!objItemsMenuParnentHolder.activeInHierarchy &&
				!objWorldsMenuParnentHolder.activeInHierarchy &&
				!objLevelsSelectParentHolder.activeInHierarchy)
			{
				ZoomOut_Success ();
			}
			break;
		default:
			Debug.LogWarning ("Bad Default");
			if (!objShopMenuParnentHolder.activeInHierarchy &&
				!objItemsMenuParnentHolder.activeInHierarchy &&
				!objWorldsMenuParnentHolder.activeInHierarchy &&
				!objLevelsSelectParentHolder.activeInHierarchy &&
				!objSettingsMenuParnentHolder.activeInHierarchy) 
			{
				ZoomOut_Success ();
			}
			break;
		}
	}

	public void DontDeleteBackItems_True () {
		dontDeleteBackItems = true;
	}

	public void DontDeleteBackItems_False () {
		dontDeleteBackItems = false;
	}

	public void BackDarker_BlackToWhite () {
		backDarkerScriptHolder.BackDarker_BlackToWhite ();
	}

	public void BackDarker_WhiteToBlack () {
		backDarkerScriptHolder.gameObject.SetActive (true);
//		backDarkerScriptHolder.BackDarker_BlackToWhite ();
	}

	public void ZoomIn_Success () {
		// For Back Darker
		BackDarker_WhiteToBlack ();

		PlayZoom_In_Fast ();
		if (!dontDeleteBackItems) {
			StartCoroutine (ZoomDisable ());
		}

//		transMainParentBackHolder.localPosition = new Vector2 (1600, 0);

//		menuBackAnimsScriptHolder.MenuParticles_Stop ();
//		menuBackAnimsScriptHolder.MenuIdles_Stop ();
//		menuBackAnimsScriptHolder.MenuAnims_AllBody_Stop ();
	}

	public void ZoomOut_Success () {
		// For Back Darker
		BackDarker_BlackToWhite ();

		PlayZoom_Out_Fast ();
		if (!dontDeleteBackItems) {
			StartCoroutine (ZoomEnable ());
		}

//		transMainParentBackHolder.localPosition = Vector2.zero;

		menuBackAnimsScriptHolder.MenuParticles_Play ();
		menuBackAnimsScriptHolder.MenuIdles_Play ();
		menuBackAnimsScriptHolder.MenuAnims_AllBody_Play ();

		// In case zoom out anim is needed
//		PlayZoom_Out_Fast ();
	}

	public IEnumerator MenuActivator_OnlyStart () {
		yield return new WaitForSeconds (1);
		objShopMenuParnentHolder.SetActive (true);
		objItemsMenuParnentHolder.SetActive (true);
		objWorldsMenuParnentHolder.SetActive (true);
		objLevelsSelectParentHolder.SetActive (true);
		objSettingsMenuParnentHolder.SetActive (true);
		objAchievesMenuParnentHolder.SetActive (true);
		objVidGiftMenuParnentHolder.SetActive (true);
		objEndlessMenuParnentHolder.SetActive (true);
	}
		
	public IEnumerator MenuActivator_FrontMains_Active (GameObject objMenu) {

		// Fix (YES)
		objShopMenuParnentHolder.SetActive (false);
		objItemsMenuParnentHolder.SetActive (false);
		objWorldsMenuParnentHolder.SetActive (false);
//		yield return null;
//		objLevelsSelectParentHolder.SetActive (false);
		objSettingsMenuParnentHolder.SetActive (false);
		objAchievesMenuParnentHolder.SetActive (false);
		objVidGiftMenuParnentHolder.SetActive (false);
		objEndlessMenuParnentHolder.SetActive (false);
//
		// TODO: Fix (YES)
		objMenu.SetActive (true);

//		Debug.LogError ("1111111111111111111111111111111111111");

		yield return null;
	}

	public IEnumerator MenuActivator_FrontMains_DeActive (float delay) {
		yield return new WaitForSeconds (delay);

		// Fix (YES)
		if (!menuBackAnimsScriptHolder.isFront_Shop) {
			objShopMenuParnentHolder.SetActive (false);
		}

		if (!menuBackAnimsScriptHolder.isFront_Items) {
			objItemsMenuParnentHolder.SetActive (false);
		}

		if (!menuBackAnimsScriptHolder.isFront_Worlds) {
			objWorldsMenuParnentHolder.SetActive (false);
		}

//		if (!menuBackAnimsScriptHolder.isFront_Items) {
//			objLevelsSelectParentHolder.SetActive (false);
//		}

		if (!menuBackAnimsScriptHolder.isFront_Settings) {
			objSettingsMenuParnentHolder.SetActive (false);
		}

		if (!menuBackAnimsScriptHolder.isFront_Achieves) {
			objAchievesMenuParnentHolder.SetActive (false);
		}

		if (!menuBackAnimsScriptHolder.isFront_VidGift) {
			objVidGiftMenuParnentHolder.SetActive (false);
		}

		if (!menuBackAnimsScriptHolder.isFront_Endless) {
			objEndlessMenuParnentHolder.SetActive (false);
		}

//		Debug.LogError ("2222222222222222222222222222222222222");

		// TODO: Try disabling at the start

		yield return null;
	}

	public void MenuActivator_BackElements (bool isActive) {
//		objMainMenuALLHolder.SetActive (isActive);

		objBackDropsHolder.SetActive (isActive);
		objBodyTitleHolder.SetActive (isActive);
		objBodyElementsHolder.SetActive (isActive);
		objMainButtonsHolder.SetActive (isActive);
	}

	public IEnumerator ZoomDisable () {
		beganZoomIn = true;
		yield return new WaitForSeconds (0.5F);

		// TODO: Fix This and 2 others (YES)

		// Move menu backdrop and bg to outside of view
		StartCoroutine (MainMene_MoveAllX_Routine ());

//		objMainMenuALLHolder.SetActive (false);
//
//		objBackDropsHolder.SetActive (false);
//		objBodyTitleHolder.SetActive (false);
//		objBodyElementsHolder.SetActive (false);
//		objMainButtonsHolder.SetActive (false);

		beganZoomIn = false;
	}

	IEnumerator MainMene_MoveAllX_Routine () {
		yield return null;
		objMainMenuALLHolder.SetActive (false);
		canvasMainMenuAllHolder.enabled = false;
		transMainMenuAllHolder.localPosition = new Vector2 (-3200, 0);
	}

	IEnumerator MainMene_MoveAllX_Delayed () {
		yield return new WaitForSeconds (0.2F);
		objMainMenuALLHolder.SetActive (false);
		canvasMainMenuAllHolder.enabled = false;
		transMainMenuAllHolder.localPosition = new Vector2 (-3200, 0);
	}

	public void MainMene_MoveAllX_InstantNODISABLE () {
		transMainMenuAllHolder.localPosition = new Vector2 (-3200, 0);
	}

	public void MainMene_MoveAllX_ZeroInstant () {
		objMainMenuALLHolder.SetActive (true);
		canvasMainMenuAllHolder.enabled = true;
		transMainMenuAllHolder.localPosition = Vector2.zero;
	}

	public IEnumerator MainMene_MoveAllX_ZeroDelayed () {
		yield return new WaitForSeconds (0.2F);
		objMainMenuALLHolder.SetActive (true);
		canvasMainMenuAllHolder.enabled = true;
		transMainMenuAllHolder.localPosition = Vector2.zero;
	}

	public IEnumerator ZoomEnable () {
		if (!beganZoomIn) {
			yield return null;

			// TODO: Fix This (is others) (YES)
			MainMene_MoveAllX_ZeroInstant ();
			StartCoroutine (MainMene_MoveAllX_ZeroDelayed ());

//			objMainMenuALLHolder.SetActive (true);

//			objBackDropsHolder.SetActive (true);
//			objBodyTitleHolder.SetActive (true);
//			objBodyElementsHolder.SetActive (true);
//			objMainButtonsHolder.SetActive (true);

			beganZoomIn = false;
		} else {
			yield return new WaitForSeconds (0.5F);
			StartCoroutine (ZoomEnable ());
		}
	}

	public IEnumerator ZoomEnable_Now () {
		// TODO: Fix This (is others) (YES)

		MainMene_MoveAllX_ZeroInstant ();

//		objMainMenuALLHolder.SetActive (true);

//		objBackDropsHolder.SetActive (true);
//		objBodyTitleHolder.SetActive (true);
//		objBodyElementsHolder.SetActive (true);
//		objMainButtonsHolder.SetActive (true);

//		menuBackAnimsScriptHolder.MenuParticles_R ();
//		menuBackAnimsScriptHolder.MenuIdles_R ();

		// Rest all idle anims for zoom out (Also use this for play)
//		menuBackAnimsScriptHolder.MenuAnims_AllBody_Reset ();

		yield return null;
	}

	public void PlayZoom_In_Fast () {
		allMenusAnimRef.Play ("Menu - ZoomIn Fast Anim 1 (Legacy)");
	}

	public void PlayZoom_Out_Fast () {
		allMenusAnimRef.Play ("Menu - ZoomOut Fast Anim 1 (Legacy)");
	}
		
	public void IntroEnd_StartNotifs () {
//		NotificationCenter.Instance.NotifChange_NOW ();
	}

	public void Pressed_PlayToWorlds () {
		// To avoid having disabled all worlds before calling its animation
		worldsAllParentObjectHolder.SetActive (true);

		worldsAllScriptHolder.Worlds_Anim_EnterNow ();
//		ScrollWorlds_Set_CurrWorld ();

		// TODO: Fix !?????? Question
//		objLevelsSelectParentHolder.SetActive (false);

		Check_NewWorld ();
//		Pressed_PlayButtonSound ();

		// ADD phone activator

		// Phone Shortcut Part
//		phoneShortCutScriptHolder.PhoneSC_StartVibCheck ();
	}

	public void PowerUpRechargeAd_RequestAgainCheck () {
		if (Tapsell_Script.PowerUpRechargeAd == null) {
			Tapsell_Script.RequestAdPowerUpRecharge ();
		}
	}

	// TODO: Just REMEMBER! The Pressed_(Menu Name)s and their bools for move in and out are in Menu_BackAnims Script

	public void Pressed_MainButton (GameObject thisObject) {
		PlayZoom_In_Fast ();
		StartCoroutine (Call_MainButton (thisObject));
	}

	public void Pressed_PlayOnlyButton (GameObject thisObject) {
		PlayZoom_In_Fast ();
		StartCoroutine (Call_PlayOnlyButton (thisObject));
	}

	public void Pressed_MoveInShortCut_Button (Transform thisTransform) {
		// (Like other BUT without MenuActivator_FrontMains_Active)

		thisTransform.localPosition = Vector2.zero;
		thisTransform.gameObject.SetActive (true);
	}

	public void Pressed_MoveIn_Button (Transform thisTransform) {
//		Debug.LogWarning ("Move In!");

		thisTransform.localPosition = Vector2.zero;
//		thisTransform.gameObject.SetActive (true);

		StartCoroutine (MenuActivator_FrontMains_Active (thisTransform.gameObject));

		StartCoroutine (SideButtons_Reposition ());
	}

	public void Pressed_MoveIn_FromShortCut () {
	}

	public IEnumerator Call_MainButton (GameObject anObject) {
		objButtonsMiniBlockerHolder.SetActive (true);
		yield return new WaitForSeconds (0.1F);
		anObject.SetActive (true);
		objButtonsMiniBlockerHolder.SetActive (false);
	}

	public IEnumerator SideButtons_Reposition () {
		objMainButton_Settings.transform.localPosition = new Vector2 (-435.4F, 225.5F);
		objMainButton_Achieves.transform.localPosition = new Vector2 (-435.4F, 145.5F);
		objMainButton_LeaderB.transform.localPosition = new Vector2 (-435.4F, 65.5F);
		objMainButton_VidGift.transform.localPosition = new Vector2 (-435.4F, -14.5F);
		yield return null;
	}

	public IEnumerator Call_PlayOnlyButton (GameObject anObject) {
		objButtonsMiniBlockerHolder.SetActive (true);
		yield return new WaitForSeconds (0.1F);
		anObject.SetActive (true);
		Pressed_PlayToWorlds ();
		objButtonsMiniBlockerHolder.SetActive (false);
	}

	public void MenuBackDrop_LockAll () {
		objBackDrop_DarkChains_LeftShadow.SetActive (false);
		objBackDrop_DarkChains_RightShadow.SetActive (false);
		objBackDrop_DarkChains_Left.SetActive (false);
		objBackDrop_DarkChains_Right.SetActive (false);
		objBackDrop_Kharak.SetActive (false);
		objBackDrop_BigRings1Shadow.SetActive (false);
		objBackDrop_BigRings2Shadow.SetActive (false);
		objBackDrop_BigRings1.SetActive (false);
		objBackDrop_BigRings2.SetActive (false);
		objBackDrop_BoxingBagShadow.SetActive (false);
		objBackDrop_BoxingBag.SetActive (false);

		objBackDrop_Mills_Together.SetActive (false);
		objBackDrop_Mills_Separate.SetActive (false);
	}

	public void MenuBackDrop_Unlocker_Part1 () {
		if (MenuBackDrop_Checker (1, 1)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Mills_Separate, 0));
//			Debug.LogWarning ("Back Unlocker 1");
		} 

		if (MenuBackDrop_Checker (2, 1)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Kharak, 0.3F));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Mills_Together, 0.3F));
//			Debug.LogWarning ("Back Unlocker 2");

			// Disable the separate group
			StartCoroutine (BackDrop_lockThis (objBackDrop_Mills_Separate, 0.5F));
		} 
	}

	public void MenuBackDrop_Unlocker_Part2 () {
		if (MenuBackDrop_Checker (1, 5)) {
//			Debug.LogWarning ("Back Unlocker 3");
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings1Shadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings1, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings2Shadow, 0.1F));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings2, 0.1F));
		}

		if (MenuBackDrop_Checker (2, 5)) {
//			Debug.LogWarning ("Back Unlocker 4");
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BoxingBagShadow, 0.7F));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BoxingBag, 0.7F));
		}

		if (MenuBackDrop_Checker (3, 1)) {
//			Debug.LogWarning ("Back Unlocker 5");
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_LeftShadow, 0.9F));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_Left, 0.9F));

			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_RightShadow, 1));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_Right, 1));
		}
	}

	public void MenuBackDrop_Unlocker_BothInstant () {
		if (MenuBackDrop_Checker (1, 1)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Mills_Separate, 0));
		} 

		if (MenuBackDrop_Checker (2, 1)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Kharak, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_Mills_Together, 0));

			// Disable the Separate group
			StartCoroutine (BackDrop_lockThis (objBackDrop_Mills_Separate, 0));
		} 

		if (MenuBackDrop_Checker (1, 5)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings1Shadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings1, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings2Shadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BigRings2, 0));
		}

		if (MenuBackDrop_Checker (2, 5)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BoxingBagShadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_BoxingBag, 0));
		}

		if (MenuBackDrop_Checker (3, 1)) {
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_LeftShadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_Left, 0));

			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_RightShadow, 0));
			StartCoroutine (BackDrop_UnlockThis (objBackDrop_DarkChains_Right, 0));
		}

		Invoke ("ZoomIn_Success", 0.2F);
	}

	public IEnumerator BackDrop_UnlockThis (GameObject obj, float delay) {
		yield return new WaitForSeconds (delay);
		obj.SetActive (true);
	}

	public IEnumerator BackDrop_lockThis (GameObject obj, float delay) {
		yield return new WaitForSeconds (delay);
		obj.SetActive (false);
	}

	public bool MenuBackDrop_Checker (int whichWorld, int whichLevel) {
		if (PlayerData_Main.Instance.player_WorldsUnlocked > whichWorld) {
//			Debug.LogWarning ("CHECKER: whichworld = " + whichWorld + " and logic = TRUE"); 
			return true;
		} else {
			if (PlayerData_Main.Instance.player_WorldsUnlocked == whichWorld) {
//				Debug.LogWarning ("CHECKER: whichworld = " + whichWorld + " and logic = FALSE"); 
				if (PlayerData_Main.Instance.player_LevelsUnlocked >= whichLevel) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	public void PlayMainShake () {
		allMenusAnimRef.Stop ();
		allMenusAnimRef.Play ("Menu - Shake CamLike Anim 1 (Legacy)");
	}

	public void PlayCameraChromatic () {
		animCameraHolder.Play ("Menu Camera - Intro ONLYChromatic Anim 1 (Legacy)");
	}

    // SOUNDD EFFECTS - zarbe barkhord button haye menu
	public void PlayCameraChromatic_ButtonsPart1 () {
		particleButtonHit_Shop.Play ();
		particleButtonHit_Items.Play ();

        //		animCameraHolder.Play ("Menu Camera - Intro ONLYTwirl Anim 1 (Legacy)");
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.ZarbeMenuButton.Play();
        }
	}

	public void PlayCameraChromatic_ButtonsPart2 () {
		particleButtonHit_Play.Play ();
		particleButtonHit_Endless.Play ();

        //		animCameraHolder.Play ("Menu Camera - Intro ONLYTwirl Anim 2 (Legacy)");
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.ZarbeMenuButton.Play();
        }
    }

	public void PlayCameraChromatic_ButtonsPart3 () {
		particleButtonHit_Shop.Play ();
        //		animCameraHolder.Play ("Menu Camera - Intro ONLYTwirl Anim 3 (Legacy)");
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.ZarbeMenuButton.Play();
        }
    }

//	public void PlayIntroPart2_TitleOld () {
//	}

	public void PlayIntroPart2_TitleNIdlesPart () {
		// Title part
		PlayTitleEnabler ();

		// Title Enter Sound!
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.VorudeNeveshte.PlayDelayed(0.1f);
			SoundManager.Instance.VorudeNeveshteBaZanjir.PlayDelayed(1.3f);
		}

		// All Idles part
		menuBackAnimsScriptHolder.MenuAnims_AllBody_Play ();
	}

	public void PlayIntroPart2_ThePreSequel () {
//		PlayTitleEnabler ();
		Invoke ("PlayIntroPart2_TheSequel", 0.5F);
	}

	public void PlayTitleEnabler () {
		objBodyTitleHolder.SetActive (true);
	}

	public void PlayIntroPart2_TheSequel () {
		allMenusAnimRef.Play ("Menu - Intro 1 Anim 3 SEQUEL (Legacy)");

		// Unlock Backdrop Part 2
		MenuBackDrop_Unlocker_Part2 ();

		// Unlock Backdrop Part 1 (IN ANIMATION)
//		MenuBackDrop_Unlocker_Part1 ();

		// Back Hero Body Enable (IN ANIMATION)
//		BackdropHero_ButtonEnabler ();
	}

	public void PlayIntroPart2 () {
        //		allMenusAnimRef.Play ("Menu - Intro 1 Anim 1 (Legacy)");
        //		if (animatorIntroHolder.cli

		// Brings back drop from outside to center for the moment intro idle was pressed
		StartCoroutine (MainMene_MoveAllX_ZeroDelayed ());

		PlayCameraChromatic ();

        // SOUNDD EFFECTS - skip intro (Done)
        if (SoundManager.Instance != null)
        {
            if (SoundManager.Instance.Title.isPlaying)
            {
                SoundManager.Instance.Title.Stop();
            }
            SoundManager.Instance.TapRadKardaneIntro.Play();
        }

		// Start playing shadow mustache particles after intro
//		objShadowMustacheParticleParentHolder.SetActive (true);

		animIntroAllIdleHolder.Stop ();
		animatorIntroHolder.Play ("Menu - Intro Animator 2 Anim 1 (Legacy)");
	}

	public void Event_Prepare () {
		// Result of timed check
		if (Event_CheckTime ()) {

			if (!PlayerData_Main.Instance.Event_NewYear_Registered) {

				if (!PlayerData_Main.Instance.Event_NewYear_SawPopup) {
					menuBackAnimsScriptHolder.Pressed_Menu_Event ();
					PlayerData_Main.Instance.Event_NewYear_SawPopup = true;
				}

				StartCoroutine (menuBackAnimsScriptHolder.Event_Buttons_Enter (0.2F));
			}
		}
	}

	public void Event_Close () {
		if (true) {
			StartCoroutine (menuBackAnimsScriptHolder.Event_Buttons_Leave ());
		}
	}

	public bool Event_CheckTime () {
		eventStart_NewYear = new System.DateTime (2018, 3, 2);
		eventEnd_NewYear = new System.DateTime (2018, 4, 20);
		if (System.DateTime.Now >= eventStart_NewYear && System.DateTime.Now <= eventEnd_NewYear) {
			return true;
		} else {
			return false;
		}
	}

	void IdleBodyAnim() {
		// TODO: Add the idle for body elements with idle from copy(1)
//		animSeparateBodyMoverHolder.Play ();
	}

	void IntroOpens_WorldSelect () {
		playButtonHolder.onClick.Invoke ();
	}

	void IntroOpens_EndlessSelect () {
		endlessButtonHolder.onClick.Invoke ();
	}

	IEnumerator IntroOpens_LevelSelect (float delay) {
		yield return new WaitForSeconds (delay);

		while (!worldsAllScriptHolder.allowOpenLevelsMenu) {
//			Debug.LogError ("SHOW NOW: " + PlayerData_Main.Instance.WorldNumber);
//			GetNumber_World (PlayerData_Main.Instance.WorldNumber);

			yield return new WaitForSeconds (0.1F);
		}
		worldCardButtons_FreeArr [PlayerData_Main.Instance.WorldNumber - 1].onClick.Invoke ();
	}

	void IntroOpens_ClickBlocker_Falsify () {
		if (worldsAllScriptHolder.disallowWorldsClickBlocker2) {
			miniCutscenesScriptHolder.MiniCutscenes_CanvasDisable_Delayed ();
			worldSelectClickBlocker2Holder.SetActive (false);
		} else {
			Invoke ("IntroOpens_ClickBlocker_Falsify", 0.1F);
		}
	}

	void OpenWorld_SetupStarReqTexts () {
		text_worldStarsReqShowArr [0].text = PlayerData_Main.Instance.player_Stars.ToString () + "/" + world_1_Price_Star.ToString ();
		text_worldStarsReqShowArr [1].text = PlayerData_Main.Instance.player_Stars.ToString () + "/" + world_2_Price_Star.ToString ();
		text_worldStarsReqShowArr [2].text = PlayerData_Main.Instance.player_Stars.ToString () + "/" + world_3_Price_Star.ToString ();
		text_worldStarsReqShowArr [3].text = PlayerData_Main.Instance.player_Stars.ToString () + "/" + world_4_Price_Star.ToString ();
		text_worldStarsReqShowArr [4].text = PlayerData_Main.Instance.player_Stars.ToString () + "/" + world_5_Price_Star.ToString ();
	}

	void OpenWorld_SetupMustacheReqText () {
		text_worldMustacheReqShow.text = world_CurrentPrice_Mustache.ToString ();
	}

	void IntroEnd_ToastMessage () {
		//Moshtan_Utilties_Script.ShowToast ("برای شروع، تپ کنید!");
	}

	// Start to check if from in-game
	void Start () {
		// Menu Elements Activator
		StartCoroutine (MenuActivator_FrontMains_DeActive (1));

		// Start check internet availablity
//		if (Application.internetReachability == NetworkReachability.NotReachable) {
//			Debug.LogWarning ("Internet not reachable");
//			Moshtan_Utilties_Script.ShowToast ("Internet not reachable");
//		} else {
//			Debug.LogWarning ("Internet IS reachable");
//			Moshtan_Utilties_Script.ShowToast ("Internet IS reachable");
//		}

		// Check to make sure player has finished
		if (!PlayerData_Main.Instance.isFullBuild_VAS_Cheat) {
			PowUps_TutLevelsWon_Check ();
		}
//		StartCoroutine (PowUps_TutLevelsWon_Check (0.1F));

		// Update start notics
		StartCoroutine (Main_NotifsUpdate());

		// Check to see if fps is allowed
//		if (!PlayerData_Main.Instance.isFullBuild) {
//			if (PlayerData_Main.Instance.allowFPScounter) {
//				objFPScounterHolder.SetActive (true);
//			} else {
//				objFPScounterHolder.SetActive (false);
//			}
//		}

		PlayerData_Main.Instance.TutActive_Battery = false;

		// Click Blocker
		miniCutscenesScriptHolder.MiniCutscenes_CanvasEnable ();
		worldSelectClickBlocker2Holder.SetActive (true);

		// Check for New World results in order to open level select or not
		isNewWorldCheckDone = false;
		isWorldGoToLevelSelect = false;

		// Move backdrop menu out of path before either intro or go to level (From last scene); Using INSTANT Move out
		MainMene_MoveAllX_InstantNODISABLE ();

        // Last scene was in-game
        if (PlayerData_Main.Instance.LastScene_WasInGame_Check ()) {

			// About One Second of additional waiting
			floatWaitBeforeFreeByStars = 0.7F;
			floatWaitBeforeUnavailtoAvailable = 0.7F;

			// Old place of invoke for first half (Its now in the second half)
//			Invoke ("GameToMenu_FirstHalf", 1);

			// Now doesn't go to second half straight. Instead waits in precheck loop until new world situation is resolved into one type of enum
			StartCoroutine (GameToMenu_SecondHalf_PreCheck ());

			// OLD. Now the above line
//			GameToMenu_SecondHalf ();

		// Last scene was NOT in-game
		} else {
			
			// No wait for to available stuff
			floatWaitBeforeUnavailtoAvailable = 0;

			// NO Second of additional waiting
			floatWaitBeforeFreeByStars = 0;

			// Check to see if its first time playing 
			if (IsNotFirstTimePlay ()) {
				// Is NOT the first time playing
				StartCoroutine (MenuStart_NotFirstTime ());
//				Invoke ("MenuStart_NotFirstTime", 1);
//				Debug.LogError ("This is NOT the first time!");
			} 

			// IS the first time playing
			else {
				FirstTime_WorldAndLevelSet ();
//				Debug.LogError ("This is FIRST TIME!");
			}
		}

		// Tapseel Check
		//if (Tapsell_Script.PowerUpRechargeAdId == null) {
		//	Tapsell_Script.RequestAdPowerUpRecharge();
		//}

		// Enable or disable the lock
		StartCoroutine (EndlessLock_Enabler ());

		// Enable gold worlds
		StartCoroutine (Menu_WorldsGoldRoutine());
	}

	public IEnumerator Menu_WorldsGoldRoutine () {
		yield return null;
		worldsAllScriptHolder.WorldsCards_DoOnAwake_Canvas ();

		yield return new WaitForSeconds (0.2F);
		worldsAllScriptHolder.WorldsCards_DoOnAwake_Golds ();
	}

	public IEnumerator EndlessLock_Enabler () {
		yield return null;
		if (PlayerData_Main.Instance.TutSeen_EndlessUnlock) {
			objEndlessButtonLockHolder.SetActive (false);
		} else {
			objEndlessButtonLockHolder.SetActive (true);
		}
	}

	public IEnumerator MenuStart_NotFirstTime () {

		yield return new WaitForSeconds (1);

		// Deactivate Main Buttons & Title & Others
		objMainButton_Play.SetActive (false);
		objMainButton_Endless.SetActive (false);
		objMainButton_Items.SetActive (false);
		objMainButton_Shop.SetActive (false);
		objMainButton_Settings.SetActive (false);
		objMainButton_Achieves.SetActive (false);
		objMainButton_LeaderB.SetActive (false);
		objMainButton_VidGift.SetActive (false);
		//
		objMainTitleMover.SetActive (false);

        // Temp place for vol intro checks
        if (SoundManager.Instance != null) {
			MusicManager.Instance.MusicVolumeIntroCheck ();
		}

		if (MusicManager.Instance != null) {
			SoundManager.Instance.SoundVolumeIntroCheck ();
		}


        // Destroy loader (Only if the last scene was not in-game)
        if (!PlayerData_Main.Instance.isFirstToMenu) {
//			Debug.LogError ("HIDE THE DAMN BACK");
			StartLoading_HideTheDamnBack ();

        } else {
//			Debug.LogError ("FIRST TIME MENU FULL INTRO");

			// To close loading symbol anim from game
			if (Loading_Anim_Script.Instance != null) {
				// Initiate Loading End Anim
				StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_End_ToMenu ());
            }
        }

		// PLACE for intro music and anim and intro obj active and blocker and more
		StartCoroutine (Intro_AnimAndArtActivator ());

		// Select Intro title
		StartCoroutine (Intro_TitleSet());

		// Check to see if want full intro
		//				if (ShowFullIntro) {
		//					introParentObjHolder.SetActive (true);
		//
		//					animatorIntroHolder.Play ("Menu - Intro Animator 1 Anim 1 (Legacy)");
		//				allMenusAnimRef.Play ("Menu - Intro 1 Anim 1 (Legacy)");
		//				} else {
		//					introParentObjHolder.SetActive (false);
		//				}
	}

	IEnumerator Intro_AnimAndArtActivator () {
//		Debug.LogError ("ACTIVATE INTRO");

		// Activate this to prevent skipping before allowed time
		miniCutscenesScriptHolder.MiniCutscenes_CanvasEnable ();
		worldSelectClickBlocker2Holder.SetActive (true);

		//MUSIC - First time in Menu
		if (SoundManager.Instance != null) {
			SoundManager.Instance.Title.Play ();
		}
		if (MusicManager.Instance != null) {
			MusicManager.Instance.PlayMusic_Intro ();
		}

		// Automatically plays this. There is no need for second line to call the anim by name
		introParentObjHolder.SetActive (true);
//		animatorIntroHolder.Play ("Menu - Intro Animator 1 Anim 1 (Legacy)");

		// To Allow skipping intro later or sooner based on firs time play or not (Before reaching LAST line that actuall does the unlocking);
		if (!PlayerData_Main.Instance.isFirstToMenu) { 
			yield return new WaitForSeconds (0.5F);
		} else {
			yield return new WaitForSeconds (5.5F);
		}

		// To prevent this from becoming false while mini cutscene blocker is on
		if (!isMiniCutClickBlockerOn) {
			miniCutscenesScriptHolder.MiniCutscenes_CanvasDisable_Delayed ();
			worldSelectClickBlocker2Holder.SetActive (false);
		}
	}

	IEnumerator Intro_TitleSet () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			objIntroLogoTitleParent.SetActive (true);
			objIntroVasTitleParent.SetActive (false);
		} else {
			Debug.LogError ("here!?");
			objIntroLogoTitleParent.SetActive (false);
			objIntroVasTitleParent.SetActive (true);

			switch (PlayerData_Main.Instance.whatVAS_Type) {
			case PlayerData_Main.VAS_NameType.sibile:
				for (int i = 0; i < 2; i++) {
					objIntroVasTitleArr1 [i].SetActive (true);
					objIntroVasTitleArr2 [i].SetActive (false);
				}
				break;
			case PlayerData_Main.VAS_NameType.moshte:
				for (int i = 0; i < 2; i++) {
					objIntroVasTitleArr1 [i].SetActive (false);
					objIntroVasTitleArr2 [i].SetActive (true);
				}
				break;
			default:
				Debug.LogError ("Default Error");
			break;
			} 
		}
		yield return null;
	}

	// Remember that his is "NOT first" so the true falses inside "ifs" are opposite
	public bool IsNotFirstTimePlay () {
		if (PlayerData_Main.Instance.isFullBuild) {
			if (PlayerData_Main.Instance.player_WorldsUnlocked == 1 && PlayerData_Main.Instance.player_LevelsUnlocked == 1) {
				PlayerData_Main.Instance.isFirstTimePlaying = true;
				return false;
			} else {
				PlayerData_Main.Instance.isFirstTimePlaying = false;
				return true;
			}
		} else {
			return true;
		}
	}
		
	public void GameToMenu_FirstHalf () {
		// Check to make sure player has finished (OLD) (When it was coroutine)
//		StartCoroutine (PowUps_TutLevelsWon_Check (0));

		// Only plays grads anim if last scene was in-game
		animIntroGradsHolder.Play ("Menu FadeIn Enter Anim 1 (Legacy)");

		// MUSIC - Music Menu az ingame bargashte
		MusicManager.Instance.PlayMusic_Menu ();

		// Play Body idle anim for return to menu from ingame
		IdleBodyAnim ();

		// Unlock Backdrop Anim Elements
		PlayTitleEnabler ();
		MenuBackDrop_Unlocker_BothInstant ();
//		MenuBackDrop_Unlocker_Part1 ();
//		MenuBackDrop_Unlocker_Part2 ();

		// To close loading symbol anim from game
		if (Loading_Anim_Script.Instance != null) {
			// Initiate Loading End Anim
			StartCoroutine (Loading_Anim_Script.Instance.LoadingAnim_End_ToMenu ());
		}
	}
		
	public IEnumerator GameToMenu_SecondHalf_PreCheck () {
		// Choose to auto open Campaign OR Endless
		if (PlayerData_Main.Instance.gameMainSpawnType == PlayerData_InGame.GameSpawnType.Campaign) {
			IntroOpens_WorldSelect ();
		} else {
			IntroOpens_EndlessSelect ();
			isNewWorldCheckDone = true;

			// This is to make sure there is no open level
			isWorldGoToLevelSelect = false;
		}

		while (!isNewWorldCheckDone) {
//			Debug.LogError ("ACTUALLY Waiting for world situation to be resolved");
			yield return null;
		}

//		Debug.LogError ("!!!!!!!!!!!!!!!!!!!  isWorldGoToLevelSelect = " + isWorldGoToLevelSelect);

		GameToMenu_SecondHalf ();
	}

	public void GameToMenu_SecondHalf () {
		// Start playing shadow mustache particles in case last scene was in-game
//		objShadowMustacheParticleParentHolder.SetActive (true);

		// Return last scene to normal
		PlayerData_Main.Instance.LastScene_WasInGame_ChangeTo (false);

		// Check to see if it should open world select
		if (PlayerData_Main.Instance.Show_NewWorld_Allow || !isWorldGoToLevelSelect) {
			// NEW place of invoke for first half of GameToMenu (1)
			Invoke ("GameToMenu_FirstHalf", 0.4F);

			// OLD PLACE (Now in precheck)
//			IntroOpens_WorldSelect ();

			// To prevent this from becoming false while mini cutscene blocker is on
			if (!isMiniCutClickBlockerOn) {
				miniCutscenesScriptHolder.MiniCutscenes_CanvasDisable_Delayed ();
				worldSelectClickBlocker2Holder.SetActive (false);
			}
		}

		// In this case, open level select
		else {
			// NEW place of invoke for first half of GameToMenu (2)
			Invoke ("GameToMenu_FirstHalf", 1);

            // Check for vote for us

            Invoke("IntroToLevel_VoteForUs", 1);
			

			// OLD PLACE (Now in precheck)
//			IntroOpens_WorldSelect ();

//			Invoke ("IntroOpens_LevelSelect", 0.55F);

			StartCoroutine (IntroOpens_LevelSelect (0.55F));
			Invoke ("IntroOpens_ClickBlocker_Falsify", 1.1F);
		}

		// Backdrop Enabler Stuff
		MenuBackDrop_Unlocker_Part1();
		MenuBackDrop_Unlocker_Part2 ();

		// Back Hero Body Button Enable
		BackdropHero_ButtonEnabler ();
	}

	public void IntroToLevel_VoteForUs () {
		menuTutCheckScriptHolder.Menu_VoteChecker ();
	}

	public void BackdropHero_ButtonEnabler ( ) {
		buttonHeroBack_BodyHolder.enabled = true;
		buttonHeroBack_HeadHolder.enabled = true;
	}

//	public IEnumerator PowUps_TutLevelsWon_Check (float delay) {

	public void PowUps_TutLevelsWon_Check () {
//		yield return new WaitForSeconds (delay);

		levelCheckTut_BatteryCheck = menuTutCheckScriptHolder.tutorialLevel_BatteryCharge - 1;
		levelCheckTut_PowUp2 = menuTutCheckScriptHolder.tutorialLevel_PowUp2Gain - 1;

        if (!PlayerData_Main.Instance.player_World1_LevelsDataArr [levelCheckTut_BatteryCheck].star_Unlocked_Level) {
            //			Debug.LogWarning ("Tut Level Won Check - Level 4");
            PlayerData_Main.Instance.player_PowUp1_Own = 0;
			PlayerData_Main.Instance.player_PowUp1_Ready = 0;
		}

        if (!PlayerData_Main.Instance.player_World1_LevelsDataArr [levelCheckTut_PowUp2].star_Unlocked_Level) {
//			Debug.LogWarning ("Tut Level Won Check - Level 7");
			PlayerData_Main.Instance.player_PowUp2_Own = 0;
			PlayerData_Main.Instance.player_PowUp2_Ready = 0;
        }

//		if (!PlayerData_Main.Instance.TutSeen_BatteryCharge) {
//		}
	}

	public void IntroAnim_SpeedUp () {
		allMenusAnimRef ["Menu - Intro 1 Anim 3 (Legacy)"].speed = 1.2F;
//		animatorIntroHolder.speed = 1.3F;
	}

	public void Pressed_WorldSelectZoomIn () {
		allMenusAnimRef.Play ("Menu - ZoomIn Fast Anim 1 (Legacy)");
	}

//	public void Pressed_BackObjectsToggle () {
//		objBackCharHolder.SetActive (!objBackCharHolder.activeInHierarchy);
//		objBackDropHolder.SetActive (!objBackDropHolder.activeInHierarchy);
//	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			objQuitPopUpHolder.SetActive (true);
		}

		if (Input.GetKeyDown ("s")) {
			if (Time.timeScale == 1) {
				Time.timeScale = 5;
			} else {
				Time.timeScale = 1;
			}
		}
	}

//    public void Pressed_PlayButtonSound()
//    {
//        // SOUND EFFECT - Play Button press on main manu (done)
//        if (SoundManager.Instance!= null)
//        {
//            SoundManager.Instance.ButtonTap_MenuStart.Play();
//        }
//    }

	// Canvas change functions were here but were moved to Menu_CanvasFront_Script
}
