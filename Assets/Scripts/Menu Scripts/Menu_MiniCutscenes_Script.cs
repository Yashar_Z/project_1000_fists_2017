﻿using UnityEngine;
using System.Collections;

public class Menu_MiniCutscenes_Script : MonoBehaviour {

	public Animation animMiniCutscenesHolder;
	public AnimationClip[] animClipMiniCutscenes;

	public GameObject objWorldSelectClickBlocker;

	public Canvas canvasMiniCutsHolder;
	public UnityEngine.UI.GraphicRaycaster graphicRayMiniHolder; 

	public void MiniCutscenes_Enter () {
		MiniCutscenes_CanvasEnable ();
		animMiniCutscenesHolder.clip = animClipMiniCutscenes [0];
		animMiniCutscenesHolder.Play ();
	}

	public void MiniCutscenes_Leave () {
		animMiniCutscenesHolder.clip = animClipMiniCutscenes [1];
		animMiniCutscenesHolder.Play ();
	}

	public void MiniCutscenes_CanvasEnable () {
		canvasMiniCutsHolder.enabled = true;
		graphicRayMiniHolder.enabled = true;
	}

	public void MiniCutscenes_CanvasDisable () {
		canvasMiniCutsHolder.enabled = false;
		graphicRayMiniHolder.enabled = false;
	}

	public void MiniCutscenes_CanvasDisable_Delayed () {
		StartCoroutine (MiniCutscenes_CanvasDisable_Routine ());
	}

	public IEnumerator MiniCutscenes_CanvasDisable_Routine () {
		yield return new WaitForSeconds (0.5F);
		MiniCutscenes_CanvasDisable ();
	}

	public void ClickBlocker_Activate () {
		objWorldSelectClickBlocker.SetActive (true);
	}

	public void ClickBlocker_DeActivate () {
		objWorldSelectClickBlocker.SetActive (false);
	}
}
