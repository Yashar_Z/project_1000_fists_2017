﻿using UnityEngine;
using System.Collections;

public class Menu_TutCheck_Script : MonoBehaviour {

	public static System.Action OnUnlockPhoneSC;

//	[Header("Last World For End Gating")]
//	public int lastPossibleWorld_ForThisVer;

	[Header("Time to show each tutorial")] 
	[HideInInspector]
	public int tutorialLevel_BatteryCharge;
	[HideInInspector]
	public int tutorialLevel_PowUp2Gain;
	[HideInInspector]
	public int tutorialLevel_ItemProgress;
	[HideInInspector]
	public int tutorialLevel_ItemRepeatItem;
	[HideInInspector]
	public int tutorialLevel_EndlessUnlock;

	public StoryMain_Script storyMainScriptHolder;
	public Phone_ShortCut_Script[] phoneShortCutScriptWorldArr;

	public UnityEngine.UI.Button buttonEndlessHolder;

	public GameObject objVoteUsPopUpHolder;

	// Because tut has begun
	public bool canNotClickEndless;
	public bool isUnlockingEndless;
	public bool wasEndlessTutCalled;

	// For reward machine to check
	public bool canCheckForEndlessTut;

	// For check after event
	public bool isEndlessTutHappening;

	private int hasVotedForUs;

	void Awake () {
		if (PlayerData_Main.Instance != null) {
			tutorialLevel_BatteryCharge = Balance_Constants_Script.TutLevel_BatteryCharge;
			tutorialLevel_PowUp2Gain = Balance_Constants_Script.TutLevel_PowUp2Gain;
			tutorialLevel_ItemProgress = Balance_Constants_Script.TutLevel_ItemProgress;
			tutorialLevel_ItemRepeatItem = Balance_Constants_Script.TutLevel_ItemRepeatItem;
			tutorialLevel_EndlessUnlock = Balance_Constants_Script.TutLevel_EndlessUnlock;
		}
	}

	public void Menu_TutorialCheck (int whatWorld, int whatLevel) {
		if (whatWorld == 1) {

			// Check to see if player has seen battery tutorial
			if (!PlayerData_Main.Instance.TutSeen_BatteryCharge) {
				if (whatLevel == tutorialLevel_BatteryCharge) {
					storyMainScriptHolder.StartStory (1, tutorialLevel_BatteryCharge, 0, false);

//					storyMainScriptHolder.storyPostScriptHolder.PowerUpBatteryTut_Setup ();
				}
			}

			// Check to see if player has seen item progress tutorial
			if (!PlayerData_Main.Instance.TutSeen_ItemProgress) {
//				Debug.LogError ("This far? whatlevel = " + whatLevel);

				if (whatLevel == tutorialLevel_ItemProgress) {
					Menu_Tutorial_World1_Set (0);

					// This used to be i < 3
					for (int i = 0; i < phoneShortCutScriptWorldArr.Length; i++) {
						phoneShortCutScriptWorldArr[i].PhoneSC_TransformNOW ();
					}
				}
			}

			// Check to see if player has seen battery slot tutorial
			if (!PlayerData_Main.Instance.TutSeen_BatterySlot) {
//				Debug.LogError ("This far? whatlevel = " + whatLevel);

				if (whatLevel == tutorialLevel_PowUp2Gain) {
					storyMainScriptHolder.StartStory (1, tutorialLevel_PowUp2Gain, 0, false);
				}
			}
				
//				lastSaved = DateTime.ParseExact(PlayerPrefs.GetString("lastDateReward", "01/01/1991"), "MM/dd/yyyy", null);

		}
	}

	public void Menu_VoteChecker () {
		// Time machine for vote for us (Should be commented unless we want time machine)
//		PlayerPrefs.SetInt ("playerHasVotedForUs", -1);

		hasVotedForUs = PlayerPrefs.GetInt ("playerHasVotedForUs", -1);
//		Debug.LogError ("A       MENU TUT SCRIPT playerHasVotedForUs = " + hasVotedForUs);

		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			
			// Equal to having seen BatterySlot (It is a true value)
			if (hasVotedForUs < 5) {

				// Has unlocked more than one world
				if (PlayerData_Main.Instance.player_WorldsUnlocked > 1) {
					VoteLogicUpdate ();
				} 

				// Has one world unlocked
				else {

					// Only if has played more than 5 levels
					if (PlayerData_Main.Instance.player_LevelsUnlocked > 6) {
						VoteLogicUpdate ();
					}
				}

			}
		}
	}

	public void VoteLogicUpdate () {
		// Has not chosen to vote
		if (hasVotedForUs < 0) {
			//					Debug.Log ("Vote for us please!");
			PlayerPrefs.SetInt ("playerHasVotedForUs", 2);
			objVoteUsPopUpHolder.SetActive (true);
			//					Debug.LogError ("B       MENU TUT SCRIPT playerHasVotedForUs = " + hasVotedForUs);
		} 

		// Means player has NOT give vote but the playerHasVoted is 0 or above
		else {
			hasVotedForUs--;
			//					Debug.LogError ("C       MENU TUT SCRIPT playerHasVotedForUs = " + hasVotedForUs);
			PlayerPrefs.SetInt ("playerHasVotedForUs", hasVotedForUs);
		}
	}

	public void Menu_TutorialCheck_OnlyItemRepeat () {
//		Debug.LogError ("Before TUT");
		if (PlayerData_Main.Instance.TutSeen_ItemProgress && !PlayerData_Main.Instance.TutSeen_ItemRepeatItem) {
//			Debug.LogError ("After TUT");
			storyMainScriptHolder.StartStory (1, tutorialLevel_ItemRepeatItem, 3, false);
		}
	}

	public bool Menu_TutorialCheck_EndlessBackFromLevel () {
		if (!PlayerData_Main.Instance.TutSeen_EndlessUnlock) {
			if ((PlayerData_Main.Instance.player_WorldsUnlocked > 1) || (PlayerData_Main.Instance.player_LevelsUnlocked >= tutorialLevel_EndlessUnlock)) {
				return true;
			} 

			// Has not seen endless tut but also is NOT ready
			else {
				return false;
			}
		} 

		// Has SEEN endless
		else {
			return false;
		}
	}

	public void Menu_TutorialCheck_EndlessIntro () {
		if (!PlayerData_Main.Instance.TutSeen_EndlessUnlock) {
			if ((PlayerData_Main.Instance.player_WorldsUnlocked > 1) || (PlayerData_Main.Instance.player_LevelsUnlocked >= tutorialLevel_EndlessUnlock)) {
				// Disables the endless button for the unlock process
				canNotClickEndless = true;

				// Call story tut for endless (First sequence)
				storyMainScriptHolder.StartStory (1, tutorialLevel_EndlessUnlock, 0, false);

				isEndlessTutHappening = true;

				// Straight to interactive / circle part of intro
//				storyMainScriptHolder.storyPostScriptHolder.PostStory_Event_Start (StoryMain_Script.PostEventType.Into_EndlessUnlock);
			} else {
				isEndlessTutHappening = false;
			}
		} else {
			isEndlessTutHappening = false;
		}
	}

	public IEnumerator Menu_Tutorial_EndlessUnlockAUTO () {
		yield return new WaitForSeconds (1.5F);
	}

	public void Menu_Tutorial_World1_Set (int whichSet) {
		storyMainScriptHolder.StartStory (1, tutorialLevel_ItemProgress, whichSet, false);

		if (whichSet == 0) {
			OnUnlockPhoneSC ();
		}
	}

//	void Update () {
//		if (Input.GetKeyDown ("m")) {
//			Debug.LogError ("NOW FINAL!");
//			Menu_Tutorial_World1_Set (0);
			//			PostEvent_PhoneStats_Show ();
			//			PostEvent_ItemPage_GoTo ();
			//			PostEvent_ItemProgress_GoTo ();
//		}
//	}
}
