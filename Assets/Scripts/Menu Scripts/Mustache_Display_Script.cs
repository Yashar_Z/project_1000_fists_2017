﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mustache_Display_Script : MonoBehaviour {

	public bool isMustache_ButtonOne;
	public bool isMustache_IsInGame;
	public int whichGoTellShopAnim;
	public ParticleSystem particleMustacheDisplayHolder;
	public ParticleSystem particleMustacheGoTellShopLightHolder;

	public GameObject objMustacheAlarmsHolder;
	public GameObject objMustacheShadowsHolder;

	public GameObject objButtonMustacheInGameHolder;
	public GameObject objTooltipTellGoShopHolder;

	public Animation animMustacheMoverHolder;
	public Animation animMustacheParentGoTellHolder;
	public AnimationClip[] animClipMustacheArr;

	void OnEnable () {
		if (!isMustache_IsInGame) {
			MustacheDisplay_AnimateReset ();
//			animMustacheMoverHolder.Stop ();
			Invoke ("MustacheDisplay_AnimateIdle", 0.75F);
		}
	}

	public void MustacheDisplay_Animate (int whichAnim) {
		animMustacheMoverHolder.clip = animClipMustacheArr[whichAnim];
		animMustacheMoverHolder.Play ();
	}

	public void MustacheDisplay_ParentAnimate (int whichAnim) {
		animMustacheParentGoTellHolder.clip = animClipMustacheArr[whichAnim];
		animMustacheParentGoTellHolder.Play ();
	}

	public void Pressed_MustacheDisplay_AnimateClick () {
		MustacheDisplay_Animate (4);
		MustacheDisplay_Particle ();
		Invoke ("MustacheDisplay_AnimateIdle", 0.75F);

		// Turn of alarms the moment it was pressed
		objMustacheAlarmsHolder.SetActive (false);
	}

	public void Pressed_InGameMustacheDisplay_AnimateClick () {
		MustacheDisplay_Animate (11);
		MustacheDisplay_Particle ();
//		Invoke ("MustacheDisplay_AnimateIdle", 0.75F);
	}

	public void MustacheDisplay_Particle () {
		particleMustacheDisplayHolder.Play ();
	}

	public void MustacheDisplay_AnimateReset () {
		if (!isMustache_IsInGame) {
			MustacheDisplay_Animate (7);
		}
	}

	public void MustacheDisplay_AnimateIdle () {
		if (!isMustache_IsInGame) {
			MustacheDisplay_Animate (8);
//			Debug.LogWarning ("5!");
		}
	}

	public void Mustache_TellGoShop () {
		// Old anim based on movers animation
//			MustacheDisplay_Animate (10);
		if (whichGoTellShopAnim == 1) {
			MustacheDisplay_ParentAnimate (9);
		} else {
			//Debug.LogError ("Mustache Tremble NOW!");
			MustacheDisplay_ParentAnimate (10);
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.KamboodSibil.Play();
            }
		}
		particleMustacheGoTellShopLightHolder.Play ();
		objTooltipTellGoShopHolder.SetActive (true);
	}

	public IEnumerator Mustache_ButtonAndShadow_Activator (bool isActive) {
		yield return new WaitForSeconds (0.75F);
//		objMustacheShadowsHolder.SetActive (isActive);
		objButtonMustacheInGameHolder.SetActive (isActive);
	}
}
