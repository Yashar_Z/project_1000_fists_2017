﻿using UnityEngine;
using System.Collections;

public class NotificationCenter : MonoBehaviour {
    #region singleton Boilerplate
    private static NotificationCenter _instance = null;
    public static NotificationCenter Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    #endregion

    public static System.Action OnNotificationChanged;

    public int AchivementNotClaimed
    {
        get
        {
            int c = 0;

            foreach (var achivement in AchievementManager.Instance.AchievementsArr)
            {
                if (achivement.completed && !achivement.claimed)
                {
                    c++;
                }
            }
            return c;
        }
    }

	public int intExclaimationCount;
	public bool canUnlockPhoneSC;

	public bool isInsideItemMenu;

    private void OnEnable()
    {
        Achievement.OnComplete += (achiveName) =>
        {
            //ui variable data changes here
            if (OnNotificationChanged != null)
            {
				PlayerData_Main.Instance.isVibratingPhone = true;
                OnNotificationChanged();
            }

			// Don't show tile here because
//			StartCoroutine (AchieveTile_Add (achievAchievement_TileAllController.Instance.
        };

        Achievement.OnRewardClaimed += (achiveName) =>
        {
            //ui variable data changes here
            if (OnNotificationChanged != null)
            {
                OnNotificationChanged();
            }

//			PlayerData_Main.Instance.isVibratingPhone = true;
        };

		Quest.OnComplete += () =>
		{
			//ui variable data changes here
			if (OnNotificationChanged != null)
			{
				OnNotificationChanged();
			}

			PlayerData_Main.Instance.isVibratingPhone = true;
		};

		Reward_Machine_Script.OnExclamationGain += () =>
		{
			//ui variable data changes here
			if (OnNotificationChanged != null)
			{
				if (!isInsideItemMenu) {
					OnNotificationChanged();
				}
			}

			PlayerData_Main.Instance.isVibratingPhone = true;
		};

		Menu_TutCheck_Script.OnUnlockPhoneSC += () => {
			
			//ui variable data changes here
			if (OnNotificationChanged != null) 
			{
				OnNotificationChanged ();
			}

			PlayerData_Main.Instance.isVibratingPhone = true;
			canUnlockPhoneSC = true;

		};

		Phone_ShortCut_Script.OnVibratingOff += () => {
			
			//ui variable data changes here
			if (OnNotificationChanged != null) {
				OnNotificationChanged ();
			}

			PlayerData_Main.Instance.isVibratingPhone = false;
		};

        //item.OnComplete 
        //ite

    }

	public void NotifChange_NOW () {
		if (OnNotificationChanged != null) {
			OnNotificationChanged ();
		}
	}

//	void OnApplicationPause (bool onDevicePause)
//	{
//		Moshtan_Utilties_Script.ShowToast ("Device Pause - Pause");
//	}

}
