﻿using UnityEngine;
using System.Collections;

public class OneSignal_Script : MonoBehaviour {

    void Start()
    {

        OneSignal.StartInit("ff9cc640-1c62-435f-9366-6b96d7b240f0")
            .HandleNotificationOpened(HandleNotificationOpened)
            .InFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .EndInit();

        //OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);
    }

    // Gets called when the player opens the notification.
    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
    }


}
