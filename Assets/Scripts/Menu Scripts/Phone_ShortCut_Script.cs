﻿using UnityEngine;
using System.Collections;

public class Phone_ShortCut_Script : MonoBehaviour {

	public static System.Action OnVibratingOff;

	public AnimationClip[] animClipPhoneArr;

	public Animation animPhoneHolder;
	public ParticleSystem particlePhoneHolder;

	void OnEnable () {
		NotificationCenter.OnNotificationChanged += Notification_Check;
		PhoneSC_StartVibCheck ();
	}

	void OnDisable () {
		NotificationCenter.OnNotificationChanged -= Notification_Check;
	}

	void Start () {
//		StartCoroutine (PhoneSC_StartVibCheck_Now ());
		if (PlayerData_Main.Instance.TutSeen_ItemProgress) {
			PhoneSC_Transform (true);
		} else {
			if (!NotificationCenter.Instance.canUnlockPhoneSC) {
				PhoneSC_Transform (false);
			}
		}

		PhoneSC_StartVibCheck ();
	}

	void PhoneSC_Transform (bool isInside) {
		if (isInside) {
			transform.localPosition = new Vector2 (-2, 1);
		} else {
			transform.localPosition = new Vector2 (2000, 2000);
		}
	}

	void Notification_Check () {
		if (NotificationCenter.Instance.canUnlockPhoneSC) {
			PhoneSC_Transform (true);
		}
			
		PhoneSC_StartVibCheck ();
	}

	public void PhoneSC_TransformNOW () {
		PhoneSC_Transform (true);
	}

	public void PhoneSC_StartVibCheck () {
		if (this.gameObject.activeInHierarchy) {
			StartCoroutine (PhoneSC_StartVibCheck_Now ());
		}
	}

	public IEnumerator PhoneSC_StartVibCheck_Now () {
		yield return new WaitForSeconds (0.4F);
		if (PlayerData_Main.Instance.isVibratingPhone) {
			PhoneSC_Ringing ();
		} else {
			PhoneSC_Idle ();
		}
		yield return null;
	}

	public void PhoneSC_PlayAnim (int whichAnim) {
//		animPhoneHolder.Stop ();
		animPhoneHolder.clip = animClipPhoneArr [whichAnim];
		animPhoneHolder.Play ();
	}

	public void PhoneSC_Idle () {
		PhoneSC_PlayAnim (0);
	}

	public void PhoneSC_Ringing () {
		PhoneSC_PlayAnim (1);
	}

	public void PhoneSC_Pressed () {
		PhoneSC_PlayAnim (2);
		particlePhoneHolder.Clear ();

		Phone_Stats_Script.Instance.PhoneStats_Enter ();
//		Debug.LogError ("Vib OFF!!!");

//		PlayerData_Main.Instance.isVibratingPhone = false;
		OnVibratingOff ();

		// TODO: Needs shorter save
		SaveLoad.Save ();
	}

	public void PhoneSC_VibEnd () {
		PhoneSC_Reset ();
		particlePhoneHolder.Clear ();
	}

	public void PhoneSC_Reset () {
		PhoneSC_PlayAnim (3);
	}

	public void PhoneSC_ParticleLight () {
		particlePhoneHolder.Play ();
	}

	public void Pressed_PhoneShortCut () {
		PhoneSC_Pressed ();
	}
}
