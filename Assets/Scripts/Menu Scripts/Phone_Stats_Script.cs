﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Phone_Stats_Script : MonoBehaviour {

	public static Phone_Stats_Script Instance; 

	public ItemEffects_Script itemEffectsScriptHolder;
	public Quest_AllController_Script questAllContrllerHolder;
	public Popup_VidGift_Script popupVidGiftScriptHolder;

	public Canvas canvasPhoneStatsHolder;
	public GraphicRaycaster graphiRayCastPhoneHolder;

	// For back press
	public StoryPostEvents_Script storyPostEventsScriptHolder;
	public Menu_BackAnims_Script menuBackAnimsScriptHolder;

	public AnimationClip[] animClipPhoneArr;
	public Animation animPhoneStatsHolder;
	public Animation animPhoneStats_SectionsHolder;

	public Animation animPhoneAttribsMoreHolder;
	public AnimationClip[] animClipAttribsMoreArr;

	public GameObject objPhoneNotifsHolder_Items;
	public GameObject objPhoneNotifsHolder_Achieves;

	public GameObject objMenuNotifsHolder_Items;
	public GameObject objMenuNotifsHolder_Achieves;

	// Moved from popup vidgifts
	public GameObject objMenuNotifsHolder_VidGift;

	public GameObject objAttribsButton_More;
	public GameObject objAttribsButton_Less;

	// Texts for core attribs
	public Text text_PhoneStatsHP_Holder;
	public Text text_PhoneStatsDamage_Holder;
	public Text text_PhoneStatsCrit_Holder;

	// Texts for more attribs
	public Text text_PhoneStats_HPdupe;
	public Text text_PhoneStats_DamageDupe;
	public Text text_PhoneStats_CritDupe;
	public Text text_PhoneStats_ShieldChange;
	public Text text_PhoneStats_MustacheBonus;
	public Text text_PhoneStats_PowUp2Dur;
	public Text text_PhoneStats_LastStand;
	public Text text_PhoneStats_BonusKill;
	public Text text_PhoneStats_LuckyExplode;
	public GameObject objPhoneStats_PickupItemTICK;
	public GameObject objPhoneStats_AntiClingyTICK;

	// Texts for notifs
	public Text text_Notifs_ToItemsHolder;
	public Text text_Notifs_ToAchievesHolder;

	public Text text_Notifs_MenuButton_Items;
	public Text text_Notifs_MenuButton_Achieves;

	public enum StartOfShortCutType {
		Phone_WorldMenu,
		Phone_ItemMenu,
		Phone_ShopMenu,
		Phone_AchievesMenu,
		Phone_EndlessMenu
	}

	public StartOfShortCutType startOfShortCutWhat;
	public StartOfShortCutType startOfShortCutWhatActivated;
	public ScrollRect scrollAttribsHolder;

	[Header ("Back Shortcut Buttons")]
	public Button buttonShortcut_Items;
	public Button buttonShortcut_Achieves;

	public ParticleSystem particleAttribsMore;

	private bool isPhoneShortCutToItems;

	public void PhoneStats_PlayAnim (int whichAnim) {
		animPhoneStatsHolder.Stop ();
		animPhoneStatsHolder.clip = animClipPhoneArr [whichAnim];
		animPhoneStatsHolder.Play ();
	}

	public void PhoneStats_Idle () {
		PhoneStats_PlayAnim (0);
	}

	public void PhoneStats_Enter () {
		PhoneStats_PlayAnim (1);
		PhoneStats_ItemEffects ();

        // SOUNDD EFFECTS - Pressed Phone Shortcut (And Entrance of phone stats) (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.VoroodMobile.Play();
        }


		// To make sure more attribs is returned to normalo
		if (objAttribsButton_Less.activeInHierarchy) {
			Pressed_AttribsLessButton ();
		}

		// Start quest all
		questAllContrllerHolder.QuestsAll_PhoneEnterSetup ();

		// Enable phone canvases
		StartCoroutine (PhoneCanvas_Enable ());
	}

	public void PhoneStats_Leave () {
		PhoneStats_PlayAnim (2);
		PhoneSections_AnimStop ();

        // SOUNDD EFFECTS - Pressed any button on phone stats (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.VoroodMobile.Play();
        }

		// Disable phone canvases (Moved to the event at the end of phone anim)
//		StartCoroutine (PhoneCanvas_Disable ());
    }

	public void PhoneSections_AnimIdle () {
		animPhoneStats_SectionsHolder.Play ();
	}

	public void PhoneSections_AnimStop () {
		animPhoneStats_SectionsHolder.Stop ();
	}

	public void PhoneStats_Reset () {
		PhoneStats_PlayAnim (3);
	}

	public void Pressed_ExitPhone () {
		PhoneStats_Leave ();
	}

	public void Pressed_ShortcutToItems () {
		startOfShortCutWhatActivated = startOfShortCutWhat;
		buttonShortcut_Items.onClick.Invoke ();

		// Because I removed normal move-in, this needs to happen in order to prevent it from being disabled after delay of switch case
		menuBackAnimsScriptHolder.isFront_Items = true;

		// This is to prevent DisablePrev from disabling the items when shortcut was TO items
		isPhoneShortCutToItems = true;

		DisablePrev_AfterShortcut ();
	}

	public void Pressed_ShortcutToAchieves () {
		startOfShortCutWhatActivated = startOfShortCutWhat;
		buttonShortcut_Achieves.onClick.Invoke ();

		// Because I removed normal move-in, this needs to happen in order to prevent it from being disabled after delay of switch case
		menuBackAnimsScriptHolder.isFront_Achieves = true;

		// This is to prevent DisablePrev from disabling the achiees when shortcut was TO achieves
		isPhoneShortCutToItems = false;

		DisablePrev_AfterShortcut ();
	}

	public void DisablePrev_AfterShortcut () {
		StartCoroutine (ShortCut_DisableRoutine ());
//		Debug.Log ("StartOfShortCutType = " + startOfShortCutWhat);
	}

	public IEnumerator ShortCut_DisableRoutine () {
		yield return new WaitForSeconds (0.4F);
		switch (startOfShortCutWhatActivated) {
		case StartOfShortCutType.Phone_WorldMenu:
			storyPostEventsScriptHolder.worldsAllScriptHolder.MoveOut_WorldsAll ();

			// Move out battery stuff
			menuBackAnimsScriptHolder.menuMainScriptHolder.Pressed_LevelSelect_PowUpsLeave ();

			menuBackAnimsScriptHolder.isFront_Worlds = false;

			//			buttonBack_World.onClick.Invoke ();
			break;

		case StartOfShortCutType.Phone_EndlessMenu:
			storyPostEventsScriptHolder.endlessAllScriptHolder.MoveOut_Endless ();

			Debug.LogError ("Yo Endless");

			// Move out battery stuff
			storyPostEventsScriptHolder.endlessAllScriptHolder.Endless_PowUpsLeave ();

			menuBackAnimsScriptHolder.isFront_Endless = false;

			//			buttonBack_Endless.onClick.Invoke ();
			break;

		case StartOfShortCutType.Phone_ItemMenu:
			if (!isPhoneShortCutToItems) {
				storyPostEventsScriptHolder.itemsMenucriptHolder.MoveOut_ItemsAll ();
				menuBackAnimsScriptHolder.isFront_Items = false;
			}
			//			buttonBack_Items.onClick.Invoke ();
			break;

		case StartOfShortCutType.Phone_ShopMenu:
			storyPostEventsScriptHolder.shopPopupScriptHolder.MoveOut_ShopAll ();
			menuBackAnimsScriptHolder.isFront_Shop = false;

			//			buttonBack_Shop.onClick.Invoke ();
			break;

		case StartOfShortCutType.Phone_AchievesMenu:
			if (isPhoneShortCutToItems) {
				storyPostEventsScriptHolder.achievesPopupScriptHolder.MoveOut_AchievesAll ();
				menuBackAnimsScriptHolder.isFront_Achieves = false;

			}
			//			buttonBack_Achieves.onClick.Invoke ();
			break;

		default:
			Debug.LogError ("Default for whatShortCut");
			break;
		}
	}

//	public void Pressed_ShortCutTo_Items () {
//	}

	public void Pressed_AttribsMoreButton () {
		StartCoroutine (AttribScroller (true));

		objAttribsButton_More.SetActive (false);
		objAttribsButton_Less.SetActive (true);

		particleAttribsMore.Play ();
	}

	public void Pressed_AttribsLessButton () {
		StartCoroutine (AttribScroller (false));

		objAttribsButton_More.SetActive (true);
		objAttribsButton_Less.SetActive (false);

		particleAttribsMore.Play ();
	}

	public IEnumerator AttribScroller (bool isActive) {
		if (isActive) {
			animPhoneAttribsMoreHolder.clip = animClipAttribsMoreArr [0];
			animPhoneAttribsMoreHolder.Play ();

			yield return new WaitForSeconds (0.2F);
			scrollAttribsHolder.enabled = true;

		} else {
			scrollAttribsHolder.enabled = false;
			animPhoneAttribsMoreHolder.clip = animClipAttribsMoreArr [1];
			animPhoneAttribsMoreHolder.Play ();
			yield return null;
		}
	}

	public void PhoneStats_ItemEffects () {
		StartCoroutine (PhoneStats_ItemEffects_NOW());

		// TODO: Use for getting time between start of some function and this moment / stopwatch
//		var sw = System.Diagnostics.Stopwatch;
//		sw.Start ();
	}

	public IEnumerator PhoneStats_ItemEffects_NOW () {
		itemEffectsScriptHolder.ItemEffecst_PreLoad ();
		itemEffectsScriptHolder.ItemsEffects_Check ();
		yield return null;
		PhoneStats_Implement ();
	}

	public void PhoneStats_Implement () {
		text_PhoneStatsHP_Holder.text = itemEffectsScriptHolder.itemEffect_HP_Start.ToString ();
		text_PhoneStatsDamage_Holder.text = itemEffectsScriptHolder.itemEffect_Damage_Base.ToString ();
		text_PhoneStatsCrit_Holder.text = itemEffectsScriptHolder.itemEffect_CritChance.ToString () + " %";

		text_PhoneStats_HPdupe.text = itemEffectsScriptHolder.itemEffect_HP_Start.ToString ();
		text_PhoneStats_DamageDupe.text = itemEffectsScriptHolder.itemEffect_Damage_Base.ToString ();
		text_PhoneStats_CritDupe.text = itemEffectsScriptHolder.itemEffect_CritChance.ToString () + " %";

		text_PhoneStats_ShieldChange.text = itemEffectsScriptHolder.itemEffect_ShieldChanceTotal.ToString () + " %";
		text_PhoneStats_MustacheBonus.text = ((itemEffectsScriptHolder.itemEffect_MustacheMultiplier_Cur * 100) - 100).ToString () + " %";
		text_PhoneStats_PowUp2Dur.text = itemEffectsScriptHolder.itemEffect_PowUp2Duration.ToString ();
		text_PhoneStats_LastStand.text = itemEffectsScriptHolder.itemEffect_LastStandShoutCount.ToString ();
		text_PhoneStats_BonusKill.text = itemEffectsScriptHolder.itemEffect_KIllBonus_HealAmount.ToString ();
		Debug.LogWarning ("2 itemEffect_LuckyExplodeChance = " + itemEffectsScriptHolder.itemEffect_LuckyExplodeChance);
		text_PhoneStats_LuckyExplode.text = itemEffectsScriptHolder.itemEffect_LuckyExplodeChance.ToString () + " %";

		if (itemEffectsScriptHolder.itemEffect_ItemValueIncrease == 0) {
			objPhoneStats_PickupItemTICK.SetActive (false);
		} else {
			objPhoneStats_PickupItemTICK.SetActive (true);
		}

		if (itemEffectsScriptHolder.itemEffect_isAntiHF_Sometimes == 0) {
			objPhoneStats_AntiClingyTICK.SetActive (false);
		} else {
			objPhoneStats_AntiClingyTICK.SetActive (true);
		}

		NotifStats_Update ();
	}

	public void NotifStats_Update () {
		// ITEMS change notif size and give text
		NotifStats_UpdateItems ();

		// ACHIEVES Change notif size and give text
		NotifStats_UpdateAchieves ();

		// VIDGIFT Change notif off or on
		NotifStats_UpdateVidGift ();
	}

	public void NotifStats_UpdateAchieves () {
		StartCoroutine (NotifStats_UpdateAchievesRoutine ());
	}

	public IEnumerator NotifStats_UpdateAchievesRoutine () {
		yield return null;
		if (NotificationCenter.Instance.AchivementNotClaimed == 0) {
			objPhoneNotifsHolder_Achieves.SetActive (false);
			objMenuNotifsHolder_Achieves.SetActive (false);
		} else {
			objPhoneNotifsHolder_Achieves.SetActive (true);
			objMenuNotifsHolder_Achieves.SetActive (true);
			PhoneNotif_FontAndText (text_Notifs_ToAchievesHolder, NotificationCenter.Instance.AchivementNotClaimed);
			PhoneNotif_FontAndText (text_Notifs_MenuButton_Achieves, NotificationCenter.Instance.AchivementNotClaimed);
		}
	}

	public void NotifStats_UpdateItems () {
		StartCoroutine (NotifStats_UpdateItemsRoutine ());
	}

	public IEnumerator NotifStats_UpdateItemsRoutine () {
		yield return null;
		if (NotificationCenter.Instance.intExclaimationCount == 0) {
			objPhoneNotifsHolder_Items.SetActive (false);
			objMenuNotifsHolder_Items.SetActive (false);
		} else {
			objPhoneNotifsHolder_Items.SetActive (true);
			objMenuNotifsHolder_Items.SetActive (true);
			PhoneNotif_FontAndText (text_Notifs_ToItemsHolder, NotificationCenter.Instance.intExclaimationCount);
			PhoneNotif_FontAndText (text_Notifs_MenuButton_Items, NotificationCenter.Instance.intExclaimationCount);
		}
	}

	public void NotifStats_UpdateVidGift () {
		StartCoroutine (NotifStats_UpdateVidGiftRoutine ());
	}

	public IEnumerator NotifStats_UpdateVidGiftRoutine () {
		yield return null;

		// Vid is available based on TIME COMPARE and not onesignal
		if (popupVidGiftScriptHolder.VidGift_TimeCompare ()) {
			objMenuNotifsHolder_VidGift.SetActive (true);
		} 

		else {
			objMenuNotifsHolder_VidGift.SetActive (false);
		}
	}

	public void PhoneNotif_FontAndText (Text whichText, int whichInt) {
		if (whichInt < 10) {
			whichText.fontSize = 74;
		} else {
			if (whichInt < 20) {
				whichText.fontSize = 66;
			} else {
				whichText.fontSize = 62;
			}
		}
		whichText.text = whichInt.ToString ();
	}

	void Awake () {
		Instance = this;
		objPhoneNotifsHolder_Items.SetActive (false);

		if (PlayerData_Main.Instance != null) {
			popupVidGiftScriptHolder.dateLastWatched_AdVidGift = PlayerData_Main.Instance.player_dateVidGiftLast;
		}

		// Disable phone canvases at the start / awake of scene
		StartCoroutine (PhoneCanvas_Disable ());
	}

	public void AnimEvent_CanvasDisalbe () {
		// Disable phone canvases
		StartCoroutine (PhoneCanvas_Disable ());
	}

	IEnumerator PhoneCanvas_Enable () {
		yield return null;
		canvasPhoneStatsHolder.enabled = true;
		graphiRayCastPhoneHolder.enabled = true;
	}

	IEnumerator PhoneCanvas_Disable () {
		yield return null;
		canvasPhoneStatsHolder.enabled = false;
		graphiRayCastPhoneHolder.enabled = false;
	}
}
