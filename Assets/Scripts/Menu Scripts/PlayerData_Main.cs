﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class PlayerData_Main : MonoBehaviour {

    #region singleton Boilerplate
    private static PlayerData_Main _instance = null;
    public static PlayerData_Main Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

		Application.targetFrameRate = 60;

		player_ItemDataBase = Resources.Load<ItemDatabase> (@"itemDB");

		// Set fps counter to full build
		if (isFullBuild) {
			allowFPScounter = false;
		}
        
    }
    #endregion

	[HideInInspector]
	public int GameVersion;

	public enum PlayerMainType {
		Play_TestLevels_NoTut,
		Play_TestLevels_YesTut,
		Play_RealLevels_NoTut,
		Play_RealLevels_YesTut,
	}

	public enum OnlineShopType {
		CafeBazaar,
		Myket,
		VAS,
        Ario
	}

	public enum VAS_NameType {
		sibile,
		moshte,
		other
	}

	[Header ("Before FULL Build CHECK")]
	public bool isFullBuild;
	public bool isFullBuild_VAS_Cheat; 

	[Header ("Before Other Builds CHECK")]
	public OnlineShopType whatShopType;
	public PlayerMainType whatPlayType;
	public VAS_NameType whatVAS_Type;
	public bool allowFPScounter;

	[Header("Last World For End Gating")]
	// TODO: Needs to be increased for new worlds
	public int lastPossibleWorld_ForThisVer;

//	[Header("Change number for achievements and other updatable stuff")]
//	public int uniqueAchievementTarg_AllAchieves;
//	public int uniqueAchievementTarg_AllItems;


	[HideInInspector]
	public PlayerData_InGame.GameSpawnType gameMainSpawnType;

	// Levels & Progress
	[Header ("")]
    public int player_WorldsUnlocked;
	public int player_LevelsUnlocked;

    // Items & Mustache
    private int _player_Mustache;

    // Mohammad
	[Header ("Items")]
	public int ItemsCurrentTotal;
    public List<ItemWithAmount> player_ItemsAmount;

	[HideInInspector]
	public ItemDatabase player_ItemDataBase;

	[HideInInspector]
    public bool PowerUpRechargeVideoButtonAvailabilty;
    // End Mohammad

    [Header ("")]
	public int player_Mustache;

	[HideInInspector]
	public int player_MustacheSpent;

//    {
//    	get	{ return StoreInventory.GetItemBalance(MoshtanStoreAssets.SIBIL_CURRENCY_ITEM_ID); }
//    	set { this._player_Mustache = value;}
//    }
    public int player_Stars;

	[Header ("")]
	public int player_HP;
	public int player_Damage;
	public int player_CritChance;
	public int player_ShieldChance;

	[Header ("")]
	public int player_PowUp1_Own;
	public int player_PowUp2_Own;
	public int player_PowUp1_Ready;
	public int player_PowUp2_Ready;

	[Header ("Endless Stuff")]
	public int player_Score_Curr;
	public int player_Score_Record;

	public int player_EndlessLevel;
	public int player_XP_Curr;

	public bool player_Google_PreviouslySignedIn;

	[Header ("")]
	public float player_PowUp2Duration;
	public bool player_IsOnNotOnLastAvailableWorld;

	[Header ("Sound / Music Stuff")]
	public bool player_Sound_ON;
	public bool player_Music_ON;

    //mohammad
    public string PowUp1_StartCharge_Time;
    public string PowUp2_StartCharge_Time;
    //end mohammad

    public LevelData_Class[] player_World1_LevelsDataArr;
	public LevelData_Class[] player_World2_LevelsDataArr;
	public LevelData_Class[] player_World3_LevelsDataArr;
	public LevelData_Class[] player_World4_LevelsDataArr;
	public LevelData_Class[] player_World5_LevelsDataArr;

	public int[] worldsGoldStarsArr;

    public int WorldNumber;
    public int LevelNumber;
	public int WorldNumber_Last = 1;
	public int LevelNumber_Last = 1;
	public int LevelsPerWorld = 10;

	public string WorldNumber_FullName;
	public string LevelNumber_FullName;
	public bool lastLevelWasWon;

	// Player seen or not seen tutorials
	[Header ("Tutorial Seen Categories")]
	public bool TutSeen_BatteryCharge;
	public bool TutSeen_ItemProgress;
	public bool TutSeen_ItemRepeatItem;
	public bool TutSeen_BatterySlot;
	public bool TutSeen_EndlessUnlock;

	// Tutseen for 2nd Costumes
	[Header ("")]
	public bool TutSeen_2ndCostume_Fat;
	public bool TutSeen_2ndCostume_Thin;
	public bool TutSeen_2ndCostume_Muscle;
	public bool TutSeen_2ndCostume_Giant;

	[Header ("Event (New Years)")]
	public bool Event_NewYear_SawPopup;
	public bool Event_NewYear_Registered;

	// For Old Save Only
	private int TutSeen_BatteryCharge_INT;
	private int TutSeen_ItemProgress_INT;
	private int TutSeen_ItemRepeatItem_INT;

	[Header ("Level Stuff (Menu)")]
	public bool PlayingNewestLevel;
	public bool PlayingALevelForTheFirstTime;
	public bool Show_NewLevel_Allow;

	[Header ("World Stuff (Menu)")]
	public bool Show_NewWorld_Allow;
	public bool Show_NewWorld_Remain;
	public bool GoToUpgradesMenu;

	public bool isVibratingPhone;

	[Header ("Future Stuff")]
//	public bool[] boolFutureArr1;
//	public bool[] boolFutureArr2;
//	public int[] intFutureArr1;
//	public int[] intFutureArr2;

	[HideInInspector]
	public bool TutActive_Battery;

	[HideInInspector]
	public float introAdditionTime;

	[HideInInspector]
	public bool isFirstTimePlaying;

	[HideInInspector]
	public bool isFirstToMenu;

	[HideInInspector]
	public bool canShowAchieveTiles = false;

	// Now use balance for tut level unlocks
//	[HideInInspector]
//	public int playerTutLevel_BatteryCharge = 2;
//	[HideInInspector]
//	public int playerTutLevel_PowUp2Gain = 5;
//	[HideInInspector]
//	public int playerTutLevel_ItemProgress = 3;
//	[HideInInspector]
//	public int playerTutLevel_ItemRepeatItem = 3;
//	[HideInInspector]
//	public int playerTutLevel_EndlessUnlock = 7;

	[Header ("Vid Gift Timer")]
	public System.DateTime player_dateVidGiftLast;
	public bool isVideoAvailable_VidGift;

	private bool LastScene_WasInGame;

	public void Player_Mustache_Add (int amount) {
//        Debug.Log("code reached here");
//        StoreInventory.GiveItem(MoshtanStoreAssets.SIBIL_CURRENCY_ITEM_ID, amount);
		player_Mustache += amount;
		player_MustacheSpent = 0;

		// Means cost / mustache was spent
		if (amount < 0) {
			player_MustacheSpent = -amount;
		}

		// Achievment for holding onto too much money
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_Mustache_Hoarder_Set (player_Mustache));
			StartCoroutine (AchievementManager.Instance.AchieveProg_Mustache_Spender_AddAmount (player_MustacheSpent));
		}
			
		SaveLoad.Save ();
	}

	public void GetFullName_World () {
		switch (WorldNumber) {
		case 1:
			WorldNumber_FullName = "ﻢﮑﯾ  ﻞــــﺼﻓ";
			break;
		case 2:
			WorldNumber_FullName = "ﻡﻭﺩ  ﻞــــﺼﻓ";
			break;
		case 3:
			WorldNumber_FullName = "ﻡﻮﺳ  ﻞــــﺼﻓ";
			break;
		case 4:
			WorldNumber_FullName = "ﻡﺭﺎﻬﭼ  ﻞــــﺼﻓ";
			break;
		case 5:
			WorldNumber_FullName = "ﻢﺠﻨﭘ  ﻞــــﺼﻓ";
			break;
		default:
			Debug.LogError ("Default Error!");
			break;
		}
	}

	public void GetFullName_Level () {
		switch (LevelNumber) {
		case 1:
			LevelNumber_FullName = "ﻢﮑﯾ  ﻪﻠﺣﺮﻣ";
			break;
		case 2:
			LevelNumber_FullName = "ﻡﻭﺩ  ﻪﻠﺣﺮﻣ";
			break;
		case 3:
			LevelNumber_FullName = "ﻡﻮﺳ  ﻪﻠﺣﺮﻣ";
			break;
		case 4:
			LevelNumber_FullName = "ﻡﺭﺎﻬﭼ  ﻪﻠﺣﺮﻣ";
			break;
		case 5:
			LevelNumber_FullName = "ﻢﺠﻨﭘ  ﻪﻠﺣﺮﻣ";
			break;
		case 6:
			LevelNumber_FullName = "ﻢﺸﺷ  ﻪﻠﺣﺮﻣ";
			break;
		case 7:
			LevelNumber_FullName = "ﻢﺘﻔﻫ  ﻪﻠﺣﺮﻣ";
			break;
		case 8:
			LevelNumber_FullName = "ﻢﺘﺸﻫ  ﻪﻠﺣﺮﻣ";
			break;
		case 9:
			LevelNumber_FullName = "ﻢﻬﻧ  ﻪﻠﺣﺮﻣ";
			break;
		case 10:
			LevelNumber_FullName = "ﻢﻫﺩ ﻪﻠﺣﺮﻣ";
			break;
		default:
			LevelNumber_FullName = "ﻥﻼﻓ ﻪﻠﺣﺮﻣ";
			break;
		}
	}

	public void GetFullName_FirstTimeBoth () {
		if (PlayerData_Main.Instance.whatShopType != OnlineShopType.VAS) {
			// Title for none VAS
			WorldNumber_FullName = "ﺭﺍﺰــــــــــــﻫ";
			LevelNumber_FullName = "ﻥﺎﺘـــــــﺸﻣ";
		} else {

			switch (PlayerData_Main.Instance.whatVAS_Type) {
			case PlayerData_Main.VAS_NameType.sibile:

				// Title for VAS version
				WorldNumber_FullName = "ﻞـــﯿﺒﯿــــﺳ";
				LevelNumber_FullName = "ﻦــﯿــﺸـــﺗﺁ";
				break;

			case PlayerData_Main.VAS_NameType.moshte:

				// Title for VAS2 version
				WorldNumber_FullName = "ﺖــــﺸـــــﻣ";
				LevelNumber_FullName = "ﻦــﯿــﺸـــﺗﺁ";
				break;

			default:
				Debug.LogError ("Default Error");
				break;
			}
		}
	}

//	Old World and level Names
//	public void GetFullName_World_OLD() {
//		switch (WorldNumber) {
//		case 1:
//			WorldNumber_FullName = "ﺭﺎﻨﭼﺎﭘ  ﻪﻠﺤﻣ";
//			break;
//		case 2:
//			WorldNumber_FullName = "ﺏﻻﻭﺩ  ﻪﻠﺤﻣ";
//			break;
//		case 3:
//			WorldNumber_FullName = "ﺞﯾﻮﻫ  ﻪﻠﺤﻣ";
//			break;
//		case 4:
//			WorldNumber_FullName = "ﻮﻠﻗﻭﺩﺮﯿﺗ  ﻪﻠﺤﻣ";
//			break;
//		case 5:
//			WorldNumber_FullName = "ﯽﻟﺎﺧ  ﻪﻠﺤﻣ";
//			break;
//		default:
//			Debug.LogError ("ERRORRRR!!!");
//			break;
//		}
//	}
//
//	public void GetFullName_Level_OLD () {
//		switch (LevelNumber) {
//		case 1:
//			LevelNumber_FullName = "ﻢﮑﯾ  ﺭﺬﮔ";
//			break;
//		case 2:
//			LevelNumber_FullName = "ﻡﻭﺩ  ﺭﺬﮔ";
//			break;
//		case 3:
//			LevelNumber_FullName = "ﻡﻮﺳ  ﺭﺬﮔ";
//			break;
//		case 4:
//			LevelNumber_FullName = "ﻡﺭﺎﻬﭼ  ﺭﺬﮔ";
//			break;
//		case 5:
//			LevelNumber_FullName = "ﻢﺠﻨﭘ  ﺭﺬﮔ";
//			break;
//		case 6:
//			LevelNumber_FullName = "ﻢﺸﺷ  ﺭﺬﮔ";
//			break;
//		case 7:
//			LevelNumber_FullName = "ﻢﺘﻔﻫ  ﺭﺬﮔ";
//			break;
//		case 8:
//			LevelNumber_FullName = "ﻢﺘﺸﻫ  ﺭﺬﮔ";
//			break;
//		case 9:
//			LevelNumber_FullName = "ﻢﻬﻧ  ﺭﺬﮔ";
//			break;
//		case 10:
//			LevelNumber_FullName = "ﻢﻫﺩ ﺭﺬﮔ";
//			break;
//		default:
//			LevelNumber_FullName = "ﻥﻼﻓ ﺭﺬﮔ";
//			break;
//		}
//	}
//

	public IEnumerator LoadScene_Async_Slow (string whatSceneString) {
		AsyncOperation AO = SceneManager.LoadSceneAsync (whatSceneString); 
		Application.backgroundLoadingPriority = ThreadPriority.Low;
		AO.allowSceneActivation = false;

		while(AO.progress < 0.9f)
		{
			yield return null;
		}

		//Fade the loading screen out here

		AO.allowSceneActivation = true;
	}

	public void LastScene_WasInGame_ChangeTo (bool newValue) {
		LastScene_WasInGame = newValue;
	}

	public bool LastScene_WasInGame_Check () {
		return LastScene_WasInGame;
	}

	void Start() 
    {
		GoToUpgradesMenu = false;
		lastLevelWasWon = false;
		LastScene_WasInGame_ChangeTo (false);
        //SaveLoad.Load();
    }
    void OnDestroy()
    {
        //SaveLoad.Save();
    }

	private static int saved_LevelsUnlocked;
	private static int saved_Mustache;
	private static int saved_PU1_Own;
	private static int saved_PU2_Own;
	private static int saved_PU1_Ready;
	private static int saved_PU2_Ready;

	private static int saved_TutSeen_BatteryCharge;


    void OnEnable() {
//		Load_Data ();
	}

	void OnDisable() {
//		Save_Data ();
	}

	void Save_Data_OLD () {
		PlayerPrefs.SetInt ("saved_LevelsUnlocked", player_LevelsUnlocked);
		PlayerPrefs.SetInt ("saved_Mustache", player_Mustache);
		PlayerPrefs.SetInt ("saved_PU1_Own", player_PowUp1_Own);
		PlayerPrefs.SetInt ("saved_PU2_Own", player_PowUp2_Own);
		PlayerPrefs.SetInt ("saved_PU1_Ready", player_PowUp1_Ready);
		PlayerPrefs.SetInt ("saved_PU2_Ready", player_PowUp2_Ready);

		// Tutorial Stuff (Only for battery now)
		if (!TutSeen_BatteryCharge) {
			TutSeen_BatteryCharge_INT = 0;
		} else {
			TutSeen_BatteryCharge_INT = 1;
		}
		PlayerPrefs.SetInt ("saved_TutSeen_BatteryCharge", TutSeen_BatteryCharge_INT);
	}

	void Load_Data_OLD () {
		player_LevelsUnlocked = PlayerPrefs.GetInt ("saved_LevelsUnlocked", 0);
		player_Mustache = PlayerPrefs.GetInt ("saved_Mustache", 0);
		player_PowUp1_Own = PlayerPrefs.GetInt ("saved_PU1_Own", 0);
		player_PowUp2_Own = PlayerPrefs.GetInt ("saved_PU2_Own", 0);
		player_PowUp1_Ready = PlayerPrefs.GetInt ("saved_PU1_Ready", 0);
		player_PowUp2_Ready = PlayerPrefs.GetInt ("saved_PU2_Ready", 0);

        // First time player with no save (Because levels unlocked is never 0 unless there was no save)
        if (player_LevelsUnlocked == 0)
        {
            player_LevelsUnlocked = 1;
        }

        //// TEMP (Star Fill)
        //for (int i = 0; i < player_LevelsUnlocked - 1; i++) {
        //	player_World1_LevelsDataArr [i].star_Unlocked_Level = true;
        //	player_World1_LevelsDataArr [i].star_Unlocked_Unmissable = true;
        //	player_World1_LevelsDataArr [i].star_Unlocked_Untouchable = true;
        //}

        // Tutorial Stuff (Only for battery now)
        TutSeen_BatteryCharge_INT = PlayerPrefs.GetInt ("saved_TutSeen_BatteryCharge");
		if (TutSeen_BatteryCharge_INT == 0) {
			TutSeen_BatteryCharge = false;
		} else {
			TutSeen_BatteryCharge = true;
		}

	}

	public IEnumerator AllowAchievementTiles () {
		yield return new WaitForSeconds (1);

		// This is to prevent achieve tiles from appearing all at once
		canShowAchieveTiles = true;

//		Debug.LogWarning ("canShowAchieveTiles is NOW = " + canShowAchieveTiles);
	}
}
