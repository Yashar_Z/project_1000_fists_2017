﻿using UnityEngine;
using System.Collections;

public class PowerUp_SlotActivateScript : MonoBehaviour {

	public int whichSlot;
	public PowerUps_Menu_Script powMenuScriptHolder;

	private ParticleSystem[] particleSlotHolder;

	public void PowUp_AddSlotNOW () {
		if (whichSlot == 1) {
			powMenuScriptHolder.PowerUp1_SlotAdd ();
		} else {
			powMenuScriptHolder.PowerUp2_SlotAdd ();
		}
	}

	public void PowUp_ChargeParticle_Start () {
		particleSlotHolder = GetComponentsInChildren <ParticleSystem> ();
		particleSlotHolder[0].Play ();
	}

	public void PowUp_ChargeParticle_Stop () {
		particleSlotHolder[0].Stop ();
		particleSlotHolder[2].Play ();
	}
}
