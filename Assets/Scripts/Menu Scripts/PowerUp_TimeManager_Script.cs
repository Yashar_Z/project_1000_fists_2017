﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class PowerUp_TimeManager_Script {

	private readonly TimeSpan POWERUPS_DECRESED_TIME_FROM_ITEM_102 = TimeSpan.FromSeconds(120);
	private readonly TimeSpan POWERUPS_DECRESED_TIME_FROM_ITEM_118 = TimeSpan.FromSeconds(120);
	private readonly TimeSpan POWERUPS_DECRESED_TIME_FROM_ITEM_119 = TimeSpan.FromSeconds(120);

	//agar item felan ro dasht az zaman e charge kam kon
		
	private readonly TimeSpan MAX_POWUP1_CHARGETIME; 
	private readonly TimeSpan MAX_POWUP2_CHARGETIME; 

	public int PowUp1_Own;
	public int PowUp2_Own;

	public int PowUp1_Ready;
	public int PowUp2_Ready;

	public DateTime PowUp1_LastHit_Time;
	public DateTime PowUp2_LastHit_Time;

	public DateTime PowUp1_StartCharge_Time;
	public DateTime PowUp2_StartCharge_Time;

	public PowerUp_TimeManager_Script ()
	{
		//StartChargeTimes_TimeMachine ();
		StartChargeTimes_GetTimeData ();

		// Recharge times (From Balance Constants)
		MAX_POWUP1_CHARGETIME = TimeSpan.FromSeconds (Balance_Constants_Script.PowUp1_RechargeTime);
		MAX_POWUP2_CHARGETIME = TimeSpan.FromSeconds (Balance_Constants_Script.PowUp2_RechargeTime);

		if (PlayerData_Main.Instance != null) { 

			if (ItemWithAmount.FindItemById (102).availabityState == Item.AvailabilityState.Complete) {
				MAX_POWUP1_CHARGETIME -= POWERUPS_DECRESED_TIME_FROM_ITEM_102;
				MAX_POWUP2_CHARGETIME -= POWERUPS_DECRESED_TIME_FROM_ITEM_102;
			}

			if (ItemWithAmount.FindItemById (118).availabityState == Item.AvailabilityState.Complete) {
				MAX_POWUP1_CHARGETIME -= POWERUPS_DECRESED_TIME_FROM_ITEM_118;
			}

			if (ItemWithAmount.FindItemById (119).availabityState == Item.AvailabilityState.Complete) {
				MAX_POWUP2_CHARGETIME -= POWERUPS_DECRESED_TIME_FROM_ITEM_119;
			}
		}

		int x = 0;
		int y = 0;

		// If the time difference between now and last save is too high, you should charge more powerupready points (PU1)
		while (DateTime.Now - PowUp1_StartCharge_Time > MAX_POWUP1_CHARGETIME 
			&& PlayerData_Main.Instance.player_PowUp1_Ready < PlayerData_Main.Instance.player_PowUp1_Own) {

			// Only for pow up 1 since it needs to AVOID being recharged before seeing battery tutorial
			if (PlayerData_Main.Instance.TutSeen_BatteryCharge) {
				PlayerData_Main.Instance.player_PowUp1_Ready++;		
			}

			PowUp1_StartCharge_Time = PowUp1_StartCharge_Time.AddSeconds (MAX_POWUP1_CHARGETIME.TotalSeconds);
			PlayerData_Main.Instance.PowUp1_StartCharge_Time = PowUp1_StartCharge_Time.ToString("MM/dd/yyyy HH:mm:ss");

//			Debug.LogError ("PowUp1_StartCharge_Time = " + PowUp1_StartCharge_Time);

			if (x > 10) {
				break;
			}
			x++;
		}

		// If the time difference between now and last save is too high, you should charge more powerupready points (PU2)
		while (DateTime.Now - PowUp2_StartCharge_Time > MAX_POWUP2_CHARGETIME 
			&& PlayerData_Main.Instance.player_PowUp2_Ready < PlayerData_Main.Instance.player_PowUp2_Own) {
			PlayerData_Main.Instance.player_PowUp2_Ready++;		

			PowUp2_StartCharge_Time = PowUp2_StartCharge_Time.AddSeconds (MAX_POWUP2_CHARGETIME.TotalSeconds);
			PlayerData_Main.Instance.PowUp2_StartCharge_Time = PowUp2_StartCharge_Time.ToString("MM/dd/yyyy HH:mm:ss");

			//			Debug.LogError ("PowUp1_StartCharge_Time = " + PowUp1_StartCharge_Time);

			if (y > 10) {
				break;
			}
			y++;
		}

		// After the loop where all recharged PUs were added
        SaveLoad.Save();
//		Debug.LogError ("NOW! " + DateTime.Now);
	}

//	public void SetPows ()
//	{
//		PowUp1_Own = PlayerData_Main.Instance.player_PowUp1_Own;
//		PowUp2_Own = PlayerData_Main.Instance.player_PowUp2_Own;
//		PowUp1_Ready = PlayerData_Main.Instance.player_PowUp1_Ready;
//		PowUp2_Ready = PlayerData_Main.Instance.player_PowUp2_Ready;
//	}

	void StartChargeTimes_TimeMachine () {
        PlayerData_Main.Instance.PowUp1_StartCharge_Time = DateTime.Now.AddDays(0).ToString("MM/dd/yyyy HH:mm:ss");
        PlayerData_Main.Instance.PowUp2_StartCharge_Time = DateTime.Now.AddDays(0).ToString("MM/dd/yyyy HH:mm:ss");
        //PlayerPrefs.SetString ("PowUp1_StartCharge_Time", DateTime.Now.ToString ("MM/dd/yyyy HH:mm:ss"));
        //PlayerPrefs.SetString ("PowUp2_StartCharge_Time", DateTime.Now.ToString ("MM/dd/yyyy HH:mm:ss"));
    }

	void StartChargeTimes_GetTimeData () {
        // Get StartChargeTime 1 and 2

        PowUp1_StartCharge_Time = DateTime.ParseExact(PlayerData_Main.Instance.PowUp1_StartCharge_Time, "MM/dd/yyyy HH:mm:ss", null);
        PowUp2_StartCharge_Time = DateTime.ParseExact(PlayerData_Main.Instance.PowUp2_StartCharge_Time, "MM/dd/yyyy HH:mm:ss", null);
  //      PowUp1_StartCharge_Time = DateTime.ParseExact (PlayerPrefs.GetString ("PowUp1_StartCharge_Time", 
		//	DateTime.Now.AddSeconds (-1).ToString ("MM/dd/yyyy HH:mm:ss")), "MM/dd/yyyy HH:mm:ss", null);
		//PowUp2_StartCharge_Time = DateTime.ParseExact (PlayerPrefs.GetString ("PowUp2_StartCharge_Time", 
		//	DateTime.Now.AddSeconds (-1).ToString ("MM/dd/yyyy HH:mm:ss")), "MM/dd/yyyy HH:mm:ss", null);
	}

	public string PowUp1_RemainingTime
	{
		//				string fixedTime = x.Hour + " " + x.Minute + " " + x.Second;
		get { 
			if (isPU1_fullyCharged ()) {
				return "Full!";
			} else {
				DateTime timeToShow1 = new DateTime() + (MAX_POWUP1_CHARGETIME - (DateTime.Now - PowUp1_StartCharge_Time));
				return timeToShow1.ToString("HH:mm:ss");
			}
		}
	}

	public string PowUp2_RemainingTime
	{
		//				string fixedTime = x.Hour + " " + x.Minute + " " + x.Second;
		get { 
			if (isPU2_fullyCharged ()) {
				return "Full!";
			} else {
				DateTime timeToShow2 = new DateTime() + (MAX_POWUP2_CHARGETIME - (DateTime.Now - PowUp2_StartCharge_Time));
				return timeToShow2.ToString("HH:mm:ss");
			}
		}
	}

	public bool isPU1_fullyCharged () {
		if (PlayerData_Main.Instance.player_PowUp1_Own <= PlayerData_Main.Instance.player_PowUp1_Ready) {
			return true;
		} else {
			return false;
		}
	}

	public bool isPU2_fullyCharged () {
		if (PlayerData_Main.Instance.player_PowUp2_Own <= PlayerData_Main.Instance.player_PowUp2_Ready) {
			return true;
		} else {
			return false;
		}
	}

	// Function that can set startcharge_time 
	public void PowUp1_StartChargeSet () {
		if (isPU1_fullyCharged()) {
			PowUp1_StartCharge_Time = DateTime.Now;
		}
	}

	public void PowUp2_StartChargeSet () {
		if (isPU2_fullyCharged()) {
			PowUp2_StartCharge_Time = DateTime.Now;
		}
	}

	// Function that is allowed to increase the ready power up number (1)
	public void PowUp1_TimeCompare () {
//		Debug.LogError ("Fully2: " + isPU1_fullyCharged()); 

		// Since the difference is greater, we can increase start charge time
		if ((DateTime.Now - PowUp1_StartCharge_Time) > MAX_POWUP1_CHARGETIME 
			&& !isPU1_fullyCharged()) {

			if (PlayerData_Main.Instance.TutSeen_BatteryCharge) {
				PlayerData_Main.Instance.player_PowUp1_Ready++;
			}

			// Save in case of once slot recharge over time (PU1)
            SaveLoad.Save();

			if (!isPU1_fullyCharged ()) {
				PowUp1_StartCharge_Time = DateTime.Now;
                PlayerData_Main.Instance.PowUp1_StartCharge_Time = PowUp1_StartCharge_Time.ToString("MM/dd/yyyy HH:mm:ss");

                //PlayerPrefs.SetString("PowUp1_StartCharge_Time", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
			}
		}
	}

	// Function that is allowed to increase the ready power up number (2)
	public void PowUp2_TimeCompare () {
//		Debug.LogError ("Fully1: " + isPU2_fullyCharged()); 

		// Since the difference is greater, we can increase start charge time
		if ((DateTime.Now - PowUp2_StartCharge_Time) > MAX_POWUP2_CHARGETIME 
			&& !isPU2_fullyCharged()) {

			PlayerData_Main.Instance.player_PowUp2_Ready++;

			// Save in case of once slot recharge over time (PU1)
            SaveLoad.Save();

			if (!isPU2_fullyCharged ()) {
				PowUp2_StartCharge_Time = DateTime.Now;
                PlayerData_Main.Instance.PowUp2_StartCharge_Time = PowUp2_StartCharge_Time.ToString("MM/dd/yyyy HH:mm:ss");
                //PlayerPrefs.SetString("PowUp2_StartCharge_Time", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
			}
		}
	}
}
