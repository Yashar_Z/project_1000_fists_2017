﻿using UnityEngine; 
using UnityEngine.UI;
using System.Collections;
using System;
using Fabric.Answers;

public class PowerUps_Menu_Script : MonoBehaviour {

	[Header ("Power Up Slot Prices")]
	public int[] PowerUp1_SlotPriceArr;
	public int[] PowerUp2_SlotPriceArr;

	[Header ("")]
	public int PowerUpBOTH_EachChargeCost;

	[Header ("")]
	public Menu_Main_Script menuMainScriptHolder;
	public Mustache_Display_Script mustacheDisScriptHolder;
	public GameObject Popup_PowUpSlotBuy_ObjHolder;

	public GameObject wiresImageObjHolder_Left;
	public GameObject wiresParticlesObjHolder_Left;
	public GameObject wiresImageObjHolder_Right;
	public GameObject wiresParticlesObjHolder_Right;
	public GameObject batteryAllObjHolder;

	public GameObject particleBatteryHeatIdleHolder;
	public GameObject particleBatteryFlareIdleHolder;

	public GameObject objTooltip2ButtonsRight;
	public GameObject objTooltip2ButtonsLeft;
	public GameObject objTooltipSlotTut;

	// Game object of tapsell vid
	public GameObject objVidIcon_ON;

	// No vid Tooltip
	public GameObject objTooltipNoVidBatHolder;

	public Text textSlotPriceHolder;
	public Text textSlotTypeNameHolder;
	public Text textRechargePariceHolder;
	public Text textRechargePriceMiniHolder;
	public Text textPowUp1_RemainingTimeHolder;
	public Text textPowUp2_RemainingTimeHolder;

	public Button buttonRechargePriceHolder;
	public Button buttonHiddenSlotPU_Holder;
	public Button buttonNoCloseSlotPU_Holder;

	[Header ("Stacks Transforms")]
	public Transform PowUpStacksParHolder1;
	public Transform PowUpStacksParHolder2;
	public Transform PowUpStacksFrontHolder1;
	public Transform PowUpStacksFrontHolder2;
	public Transform PowUpStacksHairHolder1;
	public Transform PowUpStacksHairHolder2;
//	public Transform PowUp1_PartsParentTransHolder;
//	public Transform PowUp2_PartsParentTransHolder;
	public ParticleSystem PowUp1_MainReadyParticle;
	public ParticleSystem PowUp2_MainReadyParticle;
	public ParticleSystem[] particlePowUp2_MovingShieldsArr;
	public ParticleSystem[] particleBatteryPressArr;

	// Charge Particles
	public ParticleSystem particlePreChargeHolder;
	public ParticleSystem particleChargeDarkenHolder;
	public ParticleSystem particleChargeDarkenForSlotHolder;
	public ParticleSystem particleChargeEndHolder;

	public ParticleSystem particleSingleRechargeHolder_PU1;
	public ParticleSystem particleSingleRechargeHolder_PU2;

	// Battery Anim Holders
	[Header ("Pow Up Anim Holders")]
	public Animation animBatteryNonTopParentHolder;
	public Animation animChargePowUpBatteryHolder;
	public Animation animChargePowUpCameraHolder;
	public Animation animChargePowUpMenuHolder;

	// Countdown Anim Holders
	public Animation animCountdownParentHolder1;
	public Animation animCountdownParentHolder2;

	// Anim clip for first time battery
//	public AnimationClip animClipBatteryFirstTime; 

	private ParticleSystem[] PowUp1_ParticlesArr;
	//	private ParticleSystem[] PowUp2_ParticlesArr;
	private Animation animSlotPlusSelectedRef1;
	private Animation animSlotPlusSelectedRef2;

	private bool batteryIsDown;
	private bool powUpsCanRecharge;

//	private bool powUp1_Ready;
//	private bool powUp2_Ready;

	private float powerUp1_Full;
	private float powerUp2_Full;
	private float powerUp1_AmountCurr;
	private float powerUp2_AmountCurr;

	[Header ("")]
	[SerializeField]
	private int powerUp1_Count_Own;
	[SerializeField]
	private int powerUp2_Count_Own;

	private int powerUp1_Count_Ready;
	private int powerUp2_Count_Ready;
	private int powerUp1_Count_ReadyOLD;
	private int powerUp2_Count_ReadyOLD;

	private int PowerUpBOTH_AllChargesCost;
	private int powerUp_whichPUSlot;
	private int powerUp_PriceOfSlot;
	[SerializeField]
	private Image[] powerUp1_StacksArr;
	[SerializeField]
	private Image[] powerUp2_StacksArr;
	[SerializeField]
	private ParticleSystem[] PowUp1_StacksReadyParticlesArr;
	[SerializeField]
	private ParticleSystem[] PowUp2_StacksReadyParticlesArr;
	private PowerUp_TimeManager_Script timeManager;

	void Awake () {
//		PowUp1_StacksReadyParticlesArr = PowUpStacksParHolder1.GetComponentsInChildren<ParticleSystem>();
//		PowUp2_StacksReadyParticlesArr = PowUpStacksParHolder2.GetComponentsInChildren<ParticleSystem>();
//		PowUp1_ParticlesArr = PowUp1_PartsParentTransHolder.GetComponentsInChildren<ParticleSystem> ();
		timeManager = new PowerUp_TimeManager_Script();

		// Power Up Recharge Setup
//		powUp1_Ready = false;
//		powUp2_Ready = false;

		powerUp_whichPUSlot = 0;

		// Power Up Slots Prices (From Balance Constants)
		PowerUp1_SlotPriceArr[1] = Balance_Constants_Script.PowUp1_SlotPrice_2;
		PowerUp1_SlotPriceArr[2] = Balance_Constants_Script.PowUp1_SlotPrice_3;
		PowerUp1_SlotPriceArr[3] = Balance_Constants_Script.PowUp1_SlotPrice_4;
		PowerUp1_SlotPriceArr[4] = Balance_Constants_Script.PowUp1_SlotPrice_5;
		PowerUp2_SlotPriceArr[1] = Balance_Constants_Script.PowUp2_SlotPrice_2;
		PowerUp2_SlotPriceArr[2] = Balance_Constants_Script.PowUp2_SlotPrice_3;
		PowerUp2_SlotPriceArr[3] = Balance_Constants_Script.PowUp2_SlotPrice_4;
		PowerUp2_SlotPriceArr[4] = Balance_Constants_Script.PowUp2_SlotPrice_5;

		// Battery Placement
		batteryIsDown = false;

		Invoke ("PowerUpsALL_FirstTime", 0.1F);

		// Disable all of battery at awake
		batteryAllObjHolder.SetActive (false);

		// Get recharge price per slot (From Balance Constants)
		PowerUpBOTH_EachChargeCost = Balance_Constants_Script.BatteryRecharge_PerSlot;
	}

    private void OnEnable ()
    {
        //Tapsell_Script.OnAdPowerUpRechargeAvailable += ActivateVideoButton;
        //Tapsell_Script.OnAdPowerUpRechargeUnAvailable += DeactivateVideoButton;

        StartCoroutine (PowerUp1_TimeCheck ());
		StartCoroutine (PowerUp2_TimeCheck ());

		BatteryCheck ();
    }

	private void OnDisable () {
		//Tapsell_Script.OnAdPowerUpRechargeAvailable -= ActivateVideoButton;
		//Tapsell_Script.OnAdPowerUpRechargeUnAvailable -= DeactivateVideoButton;
	}

    void ActivateVideoButton()
    {
		StartCoroutine (RechargeByVid_Icon_Activator(true));
    }

    void DeactivateVideoButton()
    {
		StartCoroutine (RechargeByVid_Icon_Activator(false));
    }

    void Start () {
		StartCoroutine (StartPowerUp1_TimeCheck ());
		StartCoroutine (StartPowerUp2_TimeCheck ());

		// Stop Battery Anim thingy
//		Debug.LogError ("Stop happen!?");
//		animChargePowUpBatteryHolder.Stop ();

		// Check to see if we should disable battery
		BeforePowerUps_CheckAll ();

		// To send battery down in case player hasn't seen battery tut
		PreTut_BatteryDown ();
    }

	void PreTut_BatteryDown () {
		if (!PlayerData_Main.Instance.TutSeen_BatteryCharge) {
			animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery Leave Anim 1 (Legacy)");
		}
	}

	void VidButtonCheck () {
        if (PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty || Tapligh_Script.Instance.TaplighAvailable)
        {
            ActivateVideoButton();
        }
        else
        {
            DeactivateVideoButton();
        }

        //if (PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty)
		//{
		//	ActivateVideoButton();
		//}
		//else
		//{
		//	DeactivateVideoButton();
		//}
	}

	public IEnumerator StartPowerUp1_TimeCheck () {
//		Debug.LogWarning ("Check 1");

		yield return new WaitForSeconds (1);
		StartCoroutine (PowerUp1_TimeCheck ());
	}

	public IEnumerator StartPowerUp2_TimeCheck () {
//		Debug.LogWarning ("Check 2");

		yield return new WaitForSeconds (1.5F);
		StartCoroutine (PowerUp2_TimeCheck ());
	}

	public void AddReady () {
		if (timeManager.isPU1_fullyCharged()) {
			timeManager.PowUp1_StartCharge_Time = DateTime.Now;
		}
		PlayerData_Main.Instance.player_PowUp1_Ready--;
	}
		
	public IEnumerator RechargeByVid_Icon_Activator (bool isActive) {
		objVidIcon_ON.SetActive (isActive);
		yield return null;
	}

	public IEnumerator PreRechargeTooltips () {
		yield return new WaitForSeconds (0.4F);
		objTooltip2ButtonsRight.SetActive (true);
		objTooltip2ButtonsLeft.SetActive (true);
	}

	public void Pressed_PowUps_PreRecharge () {
		animBatteryNonTopParentHolder.Stop ();
		animBatteryNonTopParentHolder.Play ("Menu PowUp - Battery Call Pressed Anim 1 (Legacy)");

		// Call delayed tooltips
		StartCoroutine (PreRechargeTooltips ());

		// Status of video icon (On or off) should be passed to the method
		if (PlayerData_Main.Instance != null)
		{
			Debug.LogError ("Tapsell = " + PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty);
			VidButtonCheck ();
		}

		// Particles for pressing battery before 2 buttons
		particleBatteryPressArr[2].Emit (1);
//		particleBatteryPressArr[3].Emit (1);
	}

	public void Pressed_PowUps_PostRecharge () {
		animBatteryNonTopParentHolder.Stop ();
		animBatteryNonTopParentHolder.Play ("Menu PowUp - Battery NewOnes Pressed Anim 1 (Legacy)");
	}

	public void Pressed_PowUps_RechargeByVideo () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			if (objVidIcon_ON.activeInHierarchy) {

				if (Tapligh_Script.Instance.TaplighAvailable) {
					Tapligh_Script.Instance.OnTaplighRewardEligible += ToRecharge;
					Tapligh_Script.Instance.ShowAd ();
				} else if (PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty) {
					Tapsell_Script.ShowVideoPowerUpRecharge (ToRecharge);
				}
				//Tapsell_Script.ShowVideoPowerUpRecharge(ToRecharge);
				DeactivateVideoButton ();
				PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
				Tapligh_Script.Instance.TaplighAvailable = false;
//          Debug.LogError ("Pressed Charge By Video!");

			} else {
			
				objTooltipNoVidBatHolder.SetActive (true);
//			Debug.LogError ("Show Tooltip");

			}
		} 

		// VAS pressed video for battery
		else {
			Moshtan_Utilties_Script.ShowToast ("این قابلیت در آینده فعال خواهد شد.");
		}
	}

	public void Pressed_PowUps_RechargeByMustache () {
		if (PlayerData_Main.Instance.player_Mustache >= PowerUpBOTH_AllChargesCost) {
			// Decrease Player Mustache 
			PlayerData_Main.Instance.Player_Mustache_Add (-PowerUpBOTH_AllChargesCost);

			// Start recharge stuff
			ToRecharge ();

		} else {
			// Battery Fail / No-Enough-Mustache Particles (Change, now for prerecharge elsewhere);

			mustacheDisScriptHolder.Mustache_TellGoShop ();

			Debug.LogError ("SHOW SHOP!");
		}
	}

	public void ToRecharge () {
        Tapligh_Script.Instance.OnTaplighRewardEligible -= ToRecharge;
        // Battery Activation Particles
        particleBatteryPressArr[0].Emit (1);
		particleBatteryPressArr[1].Emit (1);

		// Update HUD Number
		menuMainScriptHolder.Menu_HUD_Update ();

		// Show darken and flash particles
		//			Debug.LogError ("YO!");
		particleChargeDarkenHolder.Play ();

		// SOUNDD EFFECTS - Pressed Battery And Animation Sounds (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.BatteryCharge.Play();
		}

		// Mustache Animation Effects
		animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery Recharge Anim 1 (Legacy)");
		animChargePowUpMenuHolder.Play ("Menu PowUp - ALL Charged Anim 1 (Legacy)");

		// Quest Stuff: For Battery Use
		if (QuestManager.Instance != null) {
			StartCoroutine (QuestManager.Instance.QuestProg_BatteryUse ());
		}

		// Analytics event for battery use
		Analytics_BatteryUse ();

		// Cam Twirl Animation Effect
		// 			animChargePowUpCameraHolder.Play ("Menu Camera - Battery CAM Twirl Anim 1 (Legacy)");

	}

	void Analytics_BatteryUse () {
		Answers.LogCustom(
			"Battery Used",
			customAttributes: new System.Collections.Generic.Dictionary<string, object>(){
				
//			{"Usage Method", PlayerData_Main.Instance.player_Mustache},

			{"Unlocked Level", PlayerData_Main.Instance.player_WorldsUnlocked.ToString() + "-" + PlayerData_Main.Instance.player_LevelsUnlocked.ToString()},
		}
		);
	}

	public void FirstTimeBattery_BeforeStory () {
//		Debug.LogError ("Battery First Time: Leave PreEnter");

		animChargePowUpBatteryHolder.gameObject.SetActive (true);
		animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery Leave Anim 1 (Legacy)");

//		Debug.LogError ("Playing Leave PreEnter? " + animChargePowUpBatteryHolder.IsPlaying ("Menu PowUp - Battery Leave Anim 1 (Legacy)"));
	}

	public void FirstTimeBattery_ForTutorial () {
//		Debug.LogError ("Battery First Time: Enter");

		animChargePowUpBatteryHolder.gameObject.SetActive (true);
		animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery First Time Anim 1 (Legacy)");

//		Debug.LogError ("Evil Battery 2 " + animChargePowUpBatteryHolder.isPlaying);
//		animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery First Time Anim 1 (Legacy)", PlayMode.StopAll);
//		Debug.LogError ("Evil Battery 3 " + animChargePowUpBatteryHolder.isPlaying);
//		StartCoroutine (FirstTimeBattery_Routine ());
	}

	IEnumerator FirstTimeBattery_Routine () {
		yield return new WaitForSeconds (0.1F);

//		animChargePowUpBatteryHolder.clip = animClipBatteryFirstTime;
//		animChargePowUpBatteryHolder.Play ();
		animChargePowUpBatteryHolder.Play ();
	}

//	void Update () {
//		Debug.LogError ("Evil Battery Update: " + animChargePowUpBatteryHolder.gameObject.activeInHierarchy);
//
////		if (animChargePowUpBatteryHolder.clip != null) {
////			Debug.LogError ("animChargePowUpBatteryHolder.clip.name = ");
////			Debug.LogError (animChargePowUpBatteryHolder.clip.name);
////			Debug.LogError (animChargePowUpBatteryHolder.isPlaying);
////		}
//	}

	public void Pressed_RechargeBattery_FrontOverButton () {
//		Debug.LogWarning ("Front Over Was Pressed!");

		// New place of TutSeen_BatteryCharge
		PlayerData_Main.Instance.TutSeen_BatteryCharge = true;

		PlayerData_Main.Instance.Player_Mustache_Add (PowerUpBOTH_AllChargesCost);
		PlayerData_Main.Instance.TutActive_Battery = false;
		Pressed_PowUps_RechargeByMustache ();

		// Activates slot tooltip
		StartCoroutine (SlotTooltip_Enable ());
	}

	public IEnumerator SlotTooltip_Enable () {
		yield return new WaitForSeconds (3F);
		objTooltipSlotTut.SetActive (true);
	}

	public void PowerUpBOTH_RechargeNOW () {
		PlayerData_Main.Instance.player_PowUp1_Ready = PlayerData_Main.Instance.player_PowUp1_Own;
		PlayerData_Main.Instance.player_PowUp2_Ready = PlayerData_Main.Instance.player_PowUp2_Own;

		// Do this to make sure battery goes down
		batteryIsDown = false;
		PowerUpsALL_FirstTime ();

		// Save after power ups were recharged
		SaveLoad.Save ();
	}

	public void PowerUpBOTH_AnimateSlots () {
		animChargePowUpMenuHolder.Play ("Menu PowUp - ALL Charged Anim 1 (Legacy)");
	}

	public void PowerUpBatteryTut_Setup () {
//		Debug.LogError ("This happen!?");
		PlayerData_Main.Instance.player_PowUp1_Own = 1;
		PlayerData_Main.Instance.player_PowUp2_Own = 0;

		PlayerData_Main.Instance.player_PowUp1_Ready = 0;
		PlayerData_Main.Instance.player_PowUp2_Ready = 0;

		PowerUpsALL_FirstTime ();

		FirstTimeBattery_ForTutorial ();
	}

	IEnumerator PowerUp1_TimeCheck () {
		while (true) {
			powerUp1_Count_ReadyOLD = PlayerData_Main.Instance.player_PowUp1_Ready;
			timeManager.PowUp1_TimeCompare ();

			if (powerUp1_Count_ReadyOLD != PlayerData_Main.Instance.player_PowUp1_Ready) {
//				Debug.LogError ("UPDATE 1");
				PowerUpsALL_FirstTime ();

				particleSingleRechargeHolder_PU1.Play ();
			}

			textPowUp1_RemainingTimeHolder.text = timeManager.PowUp1_RemainingTime;
//			Debug.LogError ("UPDATE 111111");
			yield return new WaitForSeconds (1);
		}
	}

	IEnumerator PowerUp2_TimeCheck () {
		while (true) {
			powerUp2_Count_ReadyOLD = PlayerData_Main.Instance.player_PowUp2_Ready;
			timeManager.PowUp2_TimeCompare ();

			if (powerUp2_Count_ReadyOLD != PlayerData_Main.Instance.player_PowUp2_Ready) {
//				Debug.LogError ("UPDATE 2");
				PowerUpsALL_FirstTime ();

				particleSingleRechargeHolder_PU2.Play ();
			}

			textPowUp2_RemainingTimeHolder.text = timeManager.PowUp2_RemainingTime;
//			Debug.LogError ("UPDATE 222222");
			yield return new WaitForSeconds (1);
		}
	}

	IEnumerator Battery_IdleParticles (bool isActive) {
		particleBatteryHeatIdleHolder.SetActive (isActive);
		particleBatteryFlareIdleHolder.SetActive (isActive);

		yield return null;
	}

	public void PowerUpBOTH_UpdateStuff () {
		if (!timeManager.isPU1_fullyCharged() || !timeManager.isPU2_fullyCharged()) {
			// Actiavte idle particles
//			StartCoroutine (Battery_IdleParticles(true));

			// Get full charge cost
			PowerUpBOTH_AllChargesCost = (((PlayerData_Main.Instance.player_PowUp1_Own - PlayerData_Main.Instance.player_PowUp1_Ready)
			+ (PlayerData_Main.Instance.player_PowUp2_Own - PlayerData_Main.Instance.player_PowUp2_Ready)) * PowerUpBOTH_EachChargeCost);

			// Should show battery
			textRechargePariceHolder.text = PowerUpBOTH_AllChargesCost.ToString ();
			textRechargePriceMiniHolder.text = PowerUpBOTH_AllChargesCost.ToString ();
			buttonRechargePriceHolder.interactable = true;
//			Debug.LogWarning ("Interactable true");

		} else {
			// Deactivate idle particles
//			StartCoroutine (Battery_IdleParticles(false));

			// Should NOT show battery
			textRechargePariceHolder.text = "FULL!";
			buttonRechargePriceHolder.interactable = false;
//			Debug.LogWarning ("Interactable false");
		}

		// Only happens while battery tutorial is active
		if (PlayerData_Main.Instance.TutActive_Battery) {
			PlayerData_Main.Instance.player_PowUp1_Ready = 0; 
//			Invoke ("PowerUpBOTH_UpdateStuff", 1);
		}
	}

	public void PowerUp1_TimeShow () {
		textPowUp1_RemainingTimeHolder.text = timeManager.PowUp1_RemainingTime;
	}

	public void PowerUp2_TimeShow () {
		textPowUp2_RemainingTimeHolder.text = timeManager.PowUp2_RemainingTime;
	}

	void PowerUpsALL_FirstTime() {
		PowerUps_Reset_FirstTime ();
		PowerUp1_Setup_FirstTime ();
		PowerUp2_Setup_FirstTime ();
		PowerUpBOTH_UpdateStuff ();
		CountdownButton_Check ();
		BatteryCheck ();
	}

	void PowerUps_Reset_FirstTime () {
		powerUp1_Count_Own = PlayerData_Main.Instance.player_PowUp1_Own;
		powerUp2_Count_Own = PlayerData_Main.Instance.player_PowUp2_Own;

		PowUpStacksFrontHolder1.gameObject.SetActive (false);
		PowUpStacksFrontHolder2.gameObject.SetActive (false);

		for (int i = 0; i < 5; i++) {
			PowUpStacksParHolder1.GetChild (i).gameObject.SetActive (false);
			PowUpStacksParHolder2.GetChild (i).gameObject.SetActive (false);
		}

		PowerUps_Reset ();
	}

	void PowerUps_Reset () {
		for (int i = 0; i < 5; i++) {
			PowUpStacksParHolder1.GetChild (i).GetChild (1).gameObject.SetActive (false);
			PowUpStacksParHolder2.GetChild (i).GetChild (1).gameObject.SetActive (false);

			// Reset Charging Effects
			PowUpStacksParHolder1.GetChild (i).GetChild (0).GetChild (0).gameObject.SetActive (false);
			PowUpStacksParHolder2.GetChild (i).GetChild (0).GetChild (0).gameObject.SetActive (false);
		}
	}

	void PowerUp1_Setup_FirstTime () {
		if (powerUp1_Count_Own > 0) {
			PowUpStacksFrontHolder1.gameObject.SetActive (true);
			for (int i = 0; i < powerUp1_Count_Own; i++) {
				PowUpStacksParHolder1.GetChild (i).gameObject.SetActive (true);
			}
			PowerUp1_Setup ();
		} else {
			PowUpStacksHairHolder1.gameObject.SetActive (false);
		}
	}

	void PowerUp2_Setup_FirstTime () {
		if (powerUp2_Count_Own > 0) {
			PowUpStacksFrontHolder2.gameObject.SetActive (true);
			for (int i = 0; i < powerUp2_Count_Own; i++) {
				PowUpStacksParHolder2.GetChild (i).gameObject.SetActive (true);
			}
			PowerUp2_Setup ();
		} else {
			PowUpStacksHairHolder2.gameObject.SetActive (false);
		}
	}

	void PowerUp1_Setup () {
		PowerUp1_SlotPlus ();

		// Charing Particle Play
//		PowerUp1_RechargeParticle (true);

		powerUp1_Count_Ready = PlayerData_Main.Instance.player_PowUp1_Ready;
		if (powerUp1_Count_Ready > 0) {
			// Activate Front Button & Art
			PowUpStacksHairHolder1.gameObject.SetActive (true);
			PowUpStacksFrontHolder1.GetChild (0).gameObject.SetActive (false);
			PowUpStacksFrontHolder1.GetChild (1).gameObject.SetActive (true);

			// Main Particle Play
//			PowUp1_MainReadyParticle.Play ();

			// Stacks Setup (Is ready or not)
			for (int i = 0; i < powerUp1_Count_Ready; i++) {
				PowUpStacksParHolder1.GetChild (i).GetChild (1).gameObject.SetActive (true);
//				PowUp1_StacksReadyParticlesArr [i].Play ();
				//				powerUp1_StacksArr [i].enabled = true;
			}

			// Activate Button if ready
			//		if (powerUp1_Count_Own > 0) {
			//			PowUp1_ButtonHolder.enabled = true;
			//			PowUpFillerImageHolder1.enabled = true;
			//			PowUp1_MainReadyParticle.Play ();
			//		} else {
			//			PowUp1_ButtonHolder.enabled = false;
			//			PowUpFillerImageHolder1.enabled = false;
			//		}
		} else {
			PowUpStacksHairHolder1.gameObject.SetActive (false);
			PowUpStacksFrontHolder1.GetChild (0).gameObject.SetActive (true);
			PowUpStacksFrontHolder1.GetChild (1).gameObject.SetActive (false);
		}
	}

	void PowerUp2_Setup () {
		PowerUp2_SlotPlus ();

		// Charing Particle Play
//		PowerUp2_RechargeParticle (true);

		powerUp2_Count_Ready = PlayerData_Main.Instance.player_PowUp2_Ready;
		if (powerUp2_Count_Ready > 0) {
			// Activate Front Button & Art
			PowUpStacksHairHolder2.gameObject.SetActive (true);
			PowUpStacksFrontHolder2.GetChild (0).gameObject.SetActive (false);
			PowUpStacksFrontHolder2.GetChild (1).gameObject.SetActive (true);

			// Main Particle Play
//			PowUp2_MainReadyParticle.Play ();

			// Stacks Setup (Is ready or not)
			for (int i = 0; i < powerUp2_Count_Ready; i++) {
				PowUpStacksParHolder2.GetChild (i).GetChild (1).gameObject.SetActive (true);
//				PowUp2_StacksReadyParticlesArr [i].Play ();
				//				powerUp1_StacksArr [i].enabled = true;
			}
		} else {
			PowUpStacksHairHolder2.gameObject.SetActive (false);
			PowUpStacksFrontHolder2.GetChild (0).gameObject.SetActive (true);
			PowUpStacksFrontHolder2.GetChild (1).gameObject.SetActive (false);
		}
	}

	void PowerUpBOTH_RechargeParticle_AllSTOP () {
		PowerUp1_RechargeParticle (false);
		PowerUp2_RechargeParticle (false);
	}

	void PowerUp1_RechargeParticle (bool whatBool) {
		if (!timeManager.isPU1_fullyCharged()) {
			PowUpStacksParHolder1.GetChild (powerUp1_Count_Own - 1).GetChild (0).GetChild (0).gameObject.SetActive (whatBool);
		}
	}

	void PowerUp2_RechargeParticle (bool whatBool) {
		if (!timeManager.isPU2_fullyCharged()) {
			PowUpStacksParHolder2.GetChild (powerUp2_Count_Own - 1).GetChild (0).GetChild (0).gameObject.SetActive (whatBool);
		}
	}
		
	void PowerUp1_SlotPlus () {
//		Debug.LogError ("powerUp1_Count_Own = " + powerUp1_Count_Own);
		if (powerUp1_Count_Own < 5) {
			for (int i = 0; i < 5; i++) {
				PowUpStacksParHolder1.GetChild (i).GetChild (2).gameObject.SetActive (false);
			}

			// Show Slot Plus Icon
			PowUpStacksParHolder1.GetChild (powerUp1_Count_Own).gameObject.SetActive (true);
			PowUpStacksParHolder1.GetChild (powerUp1_Count_Own).GetChild (2).gameObject.SetActive (true);
			PowUpStacksParHolder1.GetChild (powerUp1_Count_Own).GetChild (0).gameObject.SetActive (false);
		} else {
			// New Solution

//			Debug.LogError ("1: Full pow ups solution");

			for (int i = 0; i < 5; i++) {
				PowUpStacksParHolder1.GetChild (i).GetChild (2).gameObject.SetActive (false);
			}

			// Old Solution
//			PowUpStacksParHolder1.GetChild (powerUp1_Count_Own - 1).GetChild (2).gameObject.SetActive (false);
		}
	}

	void PowerUp2_SlotPlus () {
//		Debug.LogError ("powerUp2_Count_Own = " + powerUp2_Count_Own);
		if (powerUp2_Count_Own < 5) {
			for (int i = 0; i < 5; i++) {
				PowUpStacksParHolder2.GetChild (i).GetChild (2).gameObject.SetActive (false);
			}

			// Show Slot Plus Icon
			PowUpStacksParHolder2.GetChild (powerUp2_Count_Own).gameObject.SetActive (true);
			PowUpStacksParHolder2.GetChild (powerUp2_Count_Own).GetChild (2).gameObject.SetActive (true);
			PowUpStacksParHolder2.GetChild (powerUp2_Count_Own).GetChild (0).gameObject.SetActive (false);
		} else {

//			Debug.LogError ("2: Full pow ups solution");

			// New Solution
			for (int i = 0; i < 5; i++) {
				PowUpStacksParHolder2.GetChild (i).GetChild (2).gameObject.SetActive (false);
			}

			// Old Solution
//			PowUpStacksParHolder2.GetChild (powerUp2_Count_Own - 1).GetChild (2).gameObject.SetActive (false);
		}
	}

	public void PowerUp1_SlotPress () {
		powerUp_whichPUSlot = 1;

        // SOUNDD EFFECTS - Slot 1 Pow Up (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.PopUpKharidPowerupSlot.Play();
        }

        // Slot Plus Anims
        animSlotPlusSelectedRef1 = PowUpStacksParHolder1.GetChild (powerUp1_Count_Own).GetChild (2).GetComponent<Animation> ();
		animSlotPlusSelectedRef1.Play ("Menu PowUp - Selected Pre-Popup Slot Anim 1 (Legacy)");

		textSlotPriceHolder.text = PowerUp1_SlotPriceArr [PlayerData_Main.Instance.player_PowUp1_Own].ToString ();
		textSlotTypeNameHolder.text = "<size=30>" + "ﻥﺎﺘﺸﻣﺭﺍﺰﻫ" + "</size>";

		Popup_PowUpSlotBuy_ObjHolder.SetActive (true);
	}

	public void PowerUp1_SlotAdd () {
		PlayerData_Main.Instance.player_PowUp1_Own++;
		PlayerData_Main.Instance.player_PowUp1_Ready = PlayerData_Main.Instance.player_PowUp1_Own;
		PowerUpsALL_FirstTime ();
		PowerUp1_Charge_SlotBonus ();

		// Animate power up items for hit
		animChargePowUpMenuHolder.Play ("Menu PowUp - ONE Charged Anim 1 (Legacy)");

		// Save the moment new slot added (PU1)
		SaveLoad.Save ();
	}

	public void PowerUp2_SlotPress () {
		powerUp_whichPUSlot = 2;

        // SOUNDD EFFECTS - Slot 2 Pow Up (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.PopUpKharidPowerupSlot.Play();
        }

        // Slot Plus Anims
        animSlotPlusSelectedRef2 = PowUpStacksParHolder2.GetChild (powerUp2_Count_Own).GetChild (2).GetComponent<Animation> ();
		animSlotPlusSelectedRef2.Play ("Menu PowUp - Selected Pre-Popup Slot Anim 1 (Legacy)");

		textSlotPriceHolder.text = PowerUp2_SlotPriceArr [PlayerData_Main.Instance.player_PowUp2_Own].ToString ();
		textSlotTypeNameHolder.text = "<size=34>" + "ﻦﺗ ﻦﯿﯾﻭﺭ" + "</size>";

		Popup_PowUpSlotBuy_ObjHolder.SetActive (true);
	}

	public void PowerUp2_SlotAdd () {
		PlayerData_Main.Instance.player_PowUp2_Own++;
		PlayerData_Main.Instance.player_PowUp2_Ready = PlayerData_Main.Instance.player_PowUp2_Own;
		PowerUpsALL_FirstTime ();
		PowerUp2_Charge_SlotBonus ();

		// Animate power up items for hit
		animChargePowUpMenuHolder.Play ("Menu PowUp - ONE Charged Anim 2 (Legacy)");

		// Save the moment new slot added (PU2)
		SaveLoad.Save ();
	}

	public IEnumerator PowerUp_Charge_SingleSmall (int whichPU, int whichSlot, bool hasDelay, int howManyOwn) {
		if (hasDelay) {
			yield return new WaitForSeconds ((whichSlot + 1) * 0.1F);
		} else {
			yield return new WaitForSeconds ((howManyOwn - whichSlot - 1) * 0.08F);
		}
		if (whichPU == 1) {
			PowUpStacksParHolder1.GetChild (whichSlot).GetChild (3).GetComponent<ParticleSystem> ().Play ();
		} else {
			PowUpStacksParHolder2.GetChild (whichSlot).GetChild (3).GetComponent<ParticleSystem> ().Play ();
		}
	}

	public IEnumerator PowerUp_Charge_SingleBig (int whichPU, bool hasDelay) {
		if (whichPU == 1) {
			if (hasDelay) {
				yield return new WaitForSeconds (PlayerData_Main.Instance.player_PowUp1_Own * 0.08F);
			} else {
				yield return null;
			}
			PowUpStacksFrontHolder1.GetChild (2).GetComponent<ParticleSystem> ().Play ();
		} else {
			if (hasDelay) {
				yield return new WaitForSeconds (PlayerData_Main.Instance.player_PowUp2_Own * 0.08F);
			} else {
				yield return null;
			}
			PowUpStacksFrontHolder2.GetChild (2).GetComponent<ParticleSystem> ().Play ();
		}
	}

	public void PowerUp1_Charge_AllBattery () {
		StartCoroutine (PowerUp_Charge_SingleBig (1, false));
		for (int i = 0; i < powerUp1_Count_Own; i++) {
			StartCoroutine( PowerUp_Charge_SingleSmall (1, i, true, 0));
		}
	}

	public void PowerUp2_Charge_AllBattery () {
		StartCoroutine (PowerUp_Charge_SingleBig (2, false));
		for (int i = 0; i < powerUp2_Count_Own; i++) {
			StartCoroutine (PowerUp_Charge_SingleSmall (2, i, true, 0));
		}
	}


	public void PowerUp1_Charge_SlotBonus () {
		particleChargeEndHolder.Play ();
		StartCoroutine (PowerUp_Charge_SingleBig (1, true));
		for (int i = powerUp1_Count_Own - 1 ; i >= 0 ; i--) {
			StartCoroutine( PowerUp_Charge_SingleSmall (1, i, false, PlayerData_Main.Instance.player_PowUp1_Own));
		}
	}

	public void PowerUp2_Charge_SlotBonus () {
		particleChargeEndHolder.Play ();
		StartCoroutine (PowerUp_Charge_SingleBig (2, true));
		for (int i = powerUp1_Count_Own - 1 ; i >= 0 ; i--) {
			StartCoroutine( PowerUp_Charge_SingleSmall (2, i, false, PlayerData_Main.Instance.player_PowUp2_Own));
		}
	}

	public void PowerUpBOTH_ChargeAll () {
		particleChargeEndHolder.Play ();
		PowerUp1_Charge_AllBattery ();
		PowerUp2_Charge_AllBattery ();
	}

	public void PowerUpSlotAnimIdle_1 () {
		animSlotPlusSelectedRef1.Play ("Menu PowUp - Slot Idle Anim 1 (Legacy)");
	}

	public void PowerUpSlotAnimIdle_2 () {
		animSlotPlusSelectedRef2.Play ("Menu PowUp - Slot Idle Anim 2 (Legacy)");
	}

	public void	PowerUpSlotAnimIdle_CHECK () {
		if (powerUp_whichPUSlot == 1) {
			animSlotPlusSelectedRef1.Play ("Menu PowUp - Slot Idle Anim 1 (Legacy)");
		} else {
			animSlotPlusSelectedRef2.Play ("Menu PowUp - Slot Idle Anim 2 (Legacy)");
		}
	}

	public void PowerUpBOTH_PriceCheck () {
		// Which Slot?
//		Debug.LogWarning ("Which slot = " + powerUp_whichPUSlot);

		if (powerUp_whichPUSlot == 1) {
			// Check to see if player can affor said slot
			powerUp_PriceOfSlot = PowerUp1_SlotPriceArr [PlayerData_Main.Instance.player_PowUp1_Own];

//			Debug.LogError ("PRICE 1 = " + powerUp_PriceOfSlot);

			if (PlayerData_Main.Instance.player_Mustache >= powerUp_PriceOfSlot) {
				PlayerData_Main.Instance.Player_Mustache_Add (-powerUp_PriceOfSlot);
				animSlotPlusSelectedRef1.Play ("Menu PowUp - Add Slot 1 Anim 1 (Legacy)");

				// Update HUD Number
				menuMainScriptHolder.Menu_HUD_Update ();

				// Darken Particle For Slot Add
				particleChargeDarkenForSlotHolder.Play ();

                // SOUNDD EFFECTS - Power Up SLOT Pop-up Purchase Animation (done)
                if (SoundManager.Instance!= null)
                {
                    SoundManager.Instance.KharidPowerUpSlot.Play();
                }

				// Send yes to the hidden yes button
				buttonHiddenSlotPU_Holder.onClick.Invoke ();

//				PowerUp1_SlotAdd ();
			} else {
				menuMainScriptHolder.GoTellShop_FromMenu ();
				Pressed_ButtonForNoToSlotBuy ();

				Debug.LogError ("SHOW SHOP!");

				PowerUpSlotAnimIdle_1 ();
			}

		} else {
			// Check to see if player can affor said slot
			powerUp_PriceOfSlot = PowerUp2_SlotPriceArr [PlayerData_Main.Instance.player_PowUp2_Own];

//			Debug.LogError ("PRICE 2 = " + powerUp_PriceOfSlot);

			if (PlayerData_Main.Instance.player_Mustache >= powerUp_PriceOfSlot) {
				PlayerData_Main.Instance.Player_Mustache_Add (-powerUp_PriceOfSlot);

				animSlotPlusSelectedRef2.Play ("Menu PowUp - Add Slot 2 Anim 1 (Legacy)");

				// Update HUD Number
				menuMainScriptHolder.Menu_HUD_Update ();

                // SOUNDD EFFECTS - Power Up SLOT Pop-up Purchase Animation (done)
                if (SoundManager.Instance != null)
                {
                    SoundManager.Instance.KharidPowerUpSlot.Play();
                }

                // Send yes to the hidden yes button
                buttonHiddenSlotPU_Holder.onClick.Invoke ();

				// Darken Particle For Slot Add
				particleChargeDarkenForSlotHolder.Play ();
//				PowerUp2_SlotAdd ();
			} else {
				menuMainScriptHolder.GoTellShop_FromMenu ();
				Pressed_ButtonForNoToSlotBuy ();
				Debug.LogError ("SHOW SHOP!");
				PowerUpSlotAnimIdle_2 ();
			}
		}
	}

	public void Pressed_AddSlotPowUp2_Now () {
//		PlayerData_Main.Instance.Player_Mustache_Add (-powerUp_PriceOfSlot);

		// Need to add getComp for animRef
		animSlotPlusSelectedRef2 = PowUpStacksParHolder2.GetChild (PlayerData_Main.Instance.player_PowUp2_Own).GetChild (2).GetComponent<Animation> ();
		animSlotPlusSelectedRef2.Play ("Menu PowUp - Add Slot 2 Anim 1 (Legacy)");

		// Update HUD Number
//		menuMainScriptHolder.Menu_HUD_Update ();

		// SOUNDD EFFECTS - Power Up SLOT Pop-up Purchase Animation (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.KharidPowerUpSlot.Play();
		}

		// Send yes to the hidden yes button
//		buttonHiddenSlotPU_Holder.onClick.Invoke ();

		// Darken Particle For Slot Add
		particleChargeDarkenForSlotHolder.Play ();
	}

	public void Pressed_ButtonForNoToSlotBuy () {
		buttonNoCloseSlotPU_Holder.onClick.Invoke ();
	}

	public void Pressed_Countdown1 () {
		animCountdownParentHolder1.Play ();
	}

	public void Pressed_Countdown2 () {
		animCountdownParentHolder2.Play ();
	}
		
	public void CountdownButton_Check () {
		animCountdownParentHolder1.gameObject.SetActive (!timeManager.isPU1_fullyCharged());
		animCountdownParentHolder2.gameObject.SetActive (!timeManager.isPU2_fullyCharged());
	}

	public void BeforePowerUps_CheckAll () {
		BeforePowerUps_Battery ();
		BeforePowerUps_Wire ();

		// LOWER the battery for players who haven't seen tut
		if (!PlayerData_Main.Instance.TutSeen_BatteryCharge) {
			batteryAllObjHolder.transform.localPosition = new Vector2 (0, -160);
		}
	}

	public void BeforePowerUps_Wire () {
		// Check for before getting power up 1
//		if (PlayerData_Main.Instance.player_PowUp1_Own == 0) {
//			wiresImageObjHolder_Right.SetActive (false);
//			wiresParticlesObjHolder_Right.SetActive (false);
//
			// In this case, all battery must be disabled
//			BeforePowerUps_Battery ();
//		}

		// Check for before getting power up 1
		if (PlayerData_Main.Instance.player_PowUp2_Own == 0) {
			wiresImageObjHolder_Left.SetActive (false);
			wiresParticlesObjHolder_Left.SetActive (false);
		}
	}

	public void BeforePowerUps_Battery () {
		if (PlayerData_Main.Instance.player_PowUp1_Own == 0) {
			batteryAllObjHolder.SetActive (false);
		} else {
			batteryAllObjHolder.SetActive (true);
		}
	}

	public void BatteryFirstTime_ObjActive () {
		batteryAllObjHolder.SetActive (true);
	}

//	void Update () {
//		Debug.LogError ("IsBatteryDown = " + batteryIsDown);
//	}

	public void BatteryCheck () {
//		Debug.LogWarning ("Battery Check? ACTIVE? " + this.gameObject.activeInHierarchy);
//		Debug.LogWarning ("Battery Check? IS DOWN? " + batteryIsDown);

		if (PlayerData_Main.Instance.TutSeen_BatteryCharge) {
			if (timeManager.isPU1_fullyCharged () && timeManager.isPU2_fullyCharged ()) {
				// Deactivate idle particles
//			StartCoroutine (Battery_IdleParticles(false));

				if (!batteryIsDown) {
//				if (this.gameObject.activeInHierarchy) {

					if (PlayerData_Main.Instance.TutSeen_BatteryCharge) {
						animChargePowUpBatteryHolder.Play ("Menu PowUp - Battery Leave Anim 1 (Legacy)");
						batteryIsDown = true;

//					Debug.LogWarning ("Battery Check? IS LAST! Also, is play? " + animChargePowUpBatteryHolder.isPlaying);

						// Should NOT show battery
						textRechargePariceHolder.text = "FULL!";
						buttonRechargePriceHolder.interactable = false;

						// Check to avoid sending battery down during battery tutorial
						//					if (PlayerData_Main.Instance.TutActive_Battery) {
						//						animChargePowUpBatteryHolder.Stop ();
						//						batteryIsDown = false;
						//					}
					}

//				}
				} 

			// Battery IS down (But in case animation wasn't played, this moves it down)
			else {
					animChargePowUpBatteryHolder.transform.localPosition = new Vector3 (0, -160, 0);
				}
			} 
		} 

		// Both are NOT fully charged
		else {
//			Debug.LogError ("WTF Battery 2");

			FirstTimeBattery_BeforeStory ();
			//				FirstTimeBattery_ForTutorial ();
		}
	}
}