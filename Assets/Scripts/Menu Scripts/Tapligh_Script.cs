﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tapligh_Script : MonoBehaviour
{
    #region singleton Boilerplate
    private static Tapligh_Script _instance = null;
    public static Tapligh_Script Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    #endregion

    string TOKEN = "XJHMMHNYN4NT8FN1G3SRPJEXDJ8LKJ";
    string UNIT_CODE = "670BBA2FD43174332D480B03916187";

    bool testMode = false;
    string _token = "";

    public bool TaplighAvailable;

    public Action OnTaplighRewardEligible;
    public Action OnTaplighVideoAvailable;

    void Start()
    {

        TaplighInterface.Instance.SetTestEnable(testMode);

        TaplighInterface.Instance.InitializeTapligh(TOKEN);
        TaplighInterface.Instance.OnAdListener = OnAdResult;
        TaplighInterface.Instance.OnRewardReadyListener = OnRewardReady;
        GetResultArguments("0;;;");

        StartCoroutine(IsInitialized(() =>
        {
            if (TaplighInterface.Instance.IsInitializeDone())
            {
                LoadAd();
            }
        }));
        //LoadAd();
    }

    /************************************************ Main methods ************************************************/

    public void LoadAd()
    {
        TaplighInterface.Instance.OnLoadReadyListener = OnAdReady;
        TaplighInterface.Instance.OnLoadErrorListener = OnLoadError;
        TaplighInterface.Instance.LoadAd(UNIT_CODE);
    }

    public void ShowAd()
    {
        TaplighInterface.Instance.ShowAd(UNIT_CODE);
    }

    public void SetTestMode()
    {
        testMode = !testMode;
        string str = "";

        if (testMode)
        {
            str = "ON";
        }
        else
        {
            str = "OFF";
        }

        TaplighInterface.Instance.SetTestEnable(testMode);
        Debug.Log(str);
        //testModeText.text  = "Test Mode is : " + str;
    }

    public void VerifyToken()
    {
        TaplighInterface.Instance.OnTokenVerifyFinishedListener = OnVerifyListener;
        TaplighInterface.Instance.VerifyToken(_token);
    }

    public void GetTaplighVersion()
    {
        //verifiedToken.text = "Tapligh SDK Version : " + TaplighInterface.Instance.GetTaplighVersion();
        Debug.Log("Tapligh SDK Version : " + TaplighInterface.Instance.GetTaplighVersion());
    }


    /************************************************ Private methods ************************************************/

    private void OnAdResult(AdResult result, string token)
    {
        string message = "Ad Result : ";

        switch (result)
        {
            case AdResult.BAD_TOKEN_USED: message += "Bad Token Used"; break;
            case AdResult.INTERNAL_ERROR: message += "Internal Error"; break;
            case AdResult.NO_AD_READY: message += "No Ad Ready"; break;
            case AdResult.NO_INTERNET_ACSSES: message += "No Internet Access"; break;
            case AdResult.AD_VIEWED_COMPLETELY: message += "Ad View Completelty"; break;
            case AdResult.AD_CLICKED: message += "Ad Clicked"; break;
            case AdResult.AD_IMAGE_CLOSED: message += "Ad Image Closed"; break;
            case AdResult.AD_VIDEO_CLOSED_AFTER_FULL_VIEW: message += "Ad Closed After Full View"; break;
            case AdResult.AD_VIDEO_CLOSED_ON_VIEW: message += "Ad Video Closed On View"; break;
        }

        Debug.Log(message);
        //verifiedToken.text = message;
    }

    private void OnRewardReady(string reward)
    {
        Debug.Log("Your Reward is : (" + reward + ")");
        //verifiedToken.text = "Reward : " + reward;
        TaplighAvailable = false;
        if (OnTaplighRewardEligible != null)
        {
            OnTaplighRewardEligible();
        }
        LoadAd();
    }

    private void OnAdReady(string unit, string token)
    {
        Debug.Log("LOAD SUCCESSFULL. THE TOKEN IS : " + token);
        Debug.Log("LOAD SUCCESSFULL. THE UNIT IS : " + unit);
        if (OnTaplighVideoAvailable!= null)
        {
            OnTaplighVideoAvailable();
        }
        TaplighAvailable = true;
        _token = token;
    }

    private void OnLoadError(string unit, LoadErrorStatus error)
    {
        string message = "On Load Error : ";

        switch (error)
        {
            case LoadErrorStatus.NO_INTERNET_ACCSSES: message += "No Internet Access"; break;
            case LoadErrorStatus.BAD_TOKEN_USED: message += "Bad Token Used"; break;
            case LoadErrorStatus.AD_UNIT_DISABLED: message += "Ad Unit Disabled"; break;
            case LoadErrorStatus.AD_UNIT_NOT_FOUND: message += "Ad Unit Not Found"; break;
            case LoadErrorStatus.INTERNAL_ERROR: message += "Internal Error"; break;
            case LoadErrorStatus.NO_AD_READY: message += "No Ad Ready"; break;
        }

        TaplighAvailable = false;
        Debug.Log("ON LOAD ERROR : THE UNIT IS : " + unit);
        Debug.Log(message);
    }

    private List<string> GetResultArguments(string result)
    {
        List<string> arguments = new List<string>();

        int deviderIndex = result.IndexOf(';');
        arguments.Add(result.Substring(0, deviderIndex));
        Debug.Log("First Message : " + arguments[0]);

        for (; deviderIndex < result.Length; deviderIndex++)
        {
            if (result[deviderIndex] != ';')
                break;
        }

        arguments.Add(result.Substring(deviderIndex));
        Debug.Log("First Message : " + arguments[1]);

        return arguments;
    }

    private void OnVerifyListener(TokenResult tokenResult)
    {

        //verifiedToken.text = "Token Verify : ";
        //switch (tokenResult)
        //{
        //    case TokenResult.INTERNAL_ERROR: verifiedToken.text += "ENTERNAL ERRROR"; break;
        //    case TokenResult.NOT_USED: verifiedToken.text += "NOT USED"; break;
        //    case TokenResult.SUCCESS: verifiedToken.text += "SUCCESS"; break;
        //    case TokenResult.TOKEN_EXPIRED: verifiedToken.text += "TOKEN EXPIRED"; break;
        //    case TokenResult.TOKEN_NOT_FOUND: verifiedToken.text += "TOKEN NOT FOUND"; break;

        //}
        Debug.Log("Token Verify : " + tokenResult.ToString());
    }

    private IEnumerator IsInitialized(System.Action onComplete)
    {
        int c = 0;
        while (c < 20)
        {
            if (TaplighInterface.Instance.IsInitializeDone())
                break;
            yield return new WaitForSeconds(0.5f);
            c++;
        }
        if (onComplete != null)
        {
            onComplete();
        }
    }

}
