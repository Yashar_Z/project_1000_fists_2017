﻿using UnityEngine;
using System.Collections;
using TapsellSDK;

public class Tapsell_Script : MonoBehaviour {

    public static System.Action OnAdPowerUpRechargeAvailable;
    public static System.Action OnAdPowerUpRechargeUnAvailable;
    public static string PowerUpRechargeAdId = null;
	public static string VidGiftAdId = null;

    public static TapsellAd PowerUpRechargeAd = null;
    public static TapsellAd VidGiftAd = null;
    public static TapsellAd BannerGiftAd = null;

    void Start () {
        Tapsell.initialize("rhgkhpecrojbgcoatondfnrqlqomramckdhjmbmnnstralsihpgjabcskqfoqcndfcflqf");
        Tapsell.setDebugMode(true);
        RequestAdPowerUpRecharge();
		RequestAd_VidGift ();
        RequestAd_BannerGift();
    }

    public static void RequestAdPowerUpRecharge()
    {
        PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
		Tapsell.requestAd(Balance_Constants_Script.Tapsell_BatteryRechargeString, true,
            (TapsellAd result) =>
        {
            // onAdAvailable
            if (OnAdPowerUpRechargeAvailable!= null)
            {
                OnAdPowerUpRechargeAvailable();
            }
            PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = true;
            Debug.Log("Action: onAdAvailable");
            PowerUpRechargeAd = result;
            PowerUpRechargeAdId = result.adId; // store this to show the ad later
        },
        (string zoneId) =>
        {
            if (OnAdPowerUpRechargeUnAvailable != null)
            {
                OnAdPowerUpRechargeUnAvailable();
            }
            PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
            // onNoAdAvailable
            Debug.Log("No Ad Available");
        },
        (TapsellError error) =>
        {
            if (OnAdPowerUpRechargeUnAvailable != null)
            {
                OnAdPowerUpRechargeUnAvailable();
            }
            PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
            // onError
            Debug.Log(error.error);
        },
        (string zoneId) =>
        {
            if (OnAdPowerUpRechargeUnAvailable != null)
            {
                OnAdPowerUpRechargeUnAvailable();
            }
            PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
            // onNoNetwork
            Debug.Log("No Network");
        },
        (TapsellAd result) =>
        {
            // onExpiring
            Debug.Log("Expiring");
            if (OnAdPowerUpRechargeUnAvailable != null)
            {
                OnAdPowerUpRechargeUnAvailable();
            }
            PlayerData_Main.Instance.PowerUpRechargeVideoButtonAvailabilty = false;
            // this ad is expired, you must download a new ad for this zone
            PowerUpRechargeAdId = null;
            PowerUpRechargeAd = null;
            RequestAdPowerUpRecharge();
        }
    );
    }

    public static void ShowVideoPowerUpRecharge(System.Action OnRewardEligible)
    {
        TapsellShowOptions showOptions = new TapsellShowOptions();
        showOptions.backDisabled = true;
        showOptions.immersiveMode = false;
        showOptions.rotationMode = TapsellShowOptions.ROTATION_UNLOCKED;
        showOptions.showDialog = true;
        Tapsell.showAd(PowerUpRechargeAd, showOptions);

        Tapsell.setRewardListener((TapsellAdFinishedResult result) =>
        {
            if (result.completed && result.rewarded)
            {
                if (OnRewardEligible!=null)
                {
                    OnRewardEligible();
                }
            }
            PowerUpRechargeAdId = null;
            PowerUpRechargeAd = null;
            RequestAdPowerUpRecharge();
        }
        );
    }

	public static void RequestAd_VidGift()
	{
		Debug.Log ("Request Ad for Vid Gift");

		PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
		Tapsell.requestAd(Balance_Constants_Script.Tapsell_VidGift, true,
			(TapsellAd result) =>
			{
				// onAdAvailable
				PlayerData_Main.Instance.isVideoAvailable_VidGift = true;
				Debug.Log("Action: onAdAvailable");
                VidGiftAd = result;
                VidGiftAdId = result.adId; // store this to show the ad later
			},
			(string zoneId) =>
			{
				PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
				// onNoAdAvailable
				Debug.Log("No Ad Available");
			},
			(TapsellError error) =>
			{
				PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
				// onError
				Debug.Log(error.error);
			},
			(string zoneId) =>
			{
				PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
				// onNoNetwork
				Debug.Log("No Network");
			},
			(TapsellAd result) =>
			{
				// onExpiring
				Debug.Log("Expiring");

				PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
				// this ad is expired, you must download a new ad for this zone

				VidGiftAdId = null;
                VidGiftAd = null;
                RequestAd_VidGift();
			}
		);
	}
    public static void RequestAd_BannerGift()
    {
        Debug.Log("Request Ad for Banner Gift");

        PlayerData_Main.Instance.isVideoAvailable_VidGift = false;
        Tapsell.requestAd(Balance_Constants_Script.Tapsell_BannerGift, true,
            (TapsellAd result) =>
            {
                // onAdAvailable
                Debug.Log("Action: onAdAvailable");
                BannerGiftAd = result;
            },
            (string zoneId) =>
            {
                // onNoAdAvailable
                Debug.Log("No Ad Available");
            },
            (TapsellError error) =>
            {
                // onError
                Debug.Log(error.error);
            },
            (string zoneId) =>
            {
                // onNoNetwork
                Debug.Log("No Network");
            },
            (TapsellAd result) =>
            {
                // onExpiring
                Debug.Log("Expiring");

                BannerGiftAd = null;
                RequestAd_BannerGift();
            }
        );
    }

    public static void ShowVideo_VidGift (System.Action OnRewardEligible)
	{
		TapsellShowOptions showOptions = new TapsellShowOptions();
		showOptions.backDisabled = false;
		showOptions.immersiveMode = false;
		showOptions.rotationMode = TapsellShowOptions.ROTATION_UNLOCKED;
		showOptions.showDialog = true;

		Tapsell.showAd(VidGiftAd, showOptions);

		Tapsell.setRewardListener((TapsellAdFinishedResult result) =>
			{
                Debug.Log(" result.completed " + result.completed);
                Debug.Log(" result.rewarded " + result.rewarded);
                if (result.completed && result.rewarded)
				{
					if (OnRewardEligible!= null)
					{
						OnRewardEligible();
					}
				}
				VidGiftAdId = null;
                VidGiftAd = null;
                //RequestAd_VidGift();
			}
		);
	}
    public static void ShowBanner_VidGift(System.Action OnRewardEligible)
    {
        TapsellShowOptions showOptions = new TapsellShowOptions();
        showOptions.backDisabled = false;
        showOptions.immersiveMode = true;
        showOptions.rotationMode = TapsellShowOptions.ROTATION_UNLOCKED;
        showOptions.showDialog = true;

        Tapsell.showAd(BannerGiftAd, showOptions);

        Tapsell.setRewardListener((TapsellAdFinishedResult result) =>
        {
            if (OnRewardEligible != null)
            {
                OnRewardEligible();
            }

            BannerGiftAd = null;
            RequestAd_BannerGift();
        }
        );
    }
}
