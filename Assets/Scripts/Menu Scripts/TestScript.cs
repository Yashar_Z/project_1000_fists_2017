﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

    public GameObject redSquare;
    public GameObject greenSquare;
    public GameObject redSquareClaim;
    public GameObject greenSquareClaim;
    public GameObject text;

	/*
    private void OnEnable()
    {
        NotificationCenter.OnNotificationChanged += NC;
    }

    private void NC()
    {
        text.SetActive(true);
        text.GetComponent<Text>().text = "NOTIF CHANGED";
        Debug.Log(AchievementManager.Instance.Achievements[1].completed);
        if (AchievementManager.Instance.Achievements[0].completed)
        {
            Debug.Log("EARNED 0");
            redSquare.SetActive(true);
            redSquareClaim.SetActive(true);
        }
        if (AchievementManager.Instance.Achievements[1].completed)
        {
            Debug.Log("EARNED 1");
            greenSquare.SetActive(true);
            greenSquareClaim.SetActive(true);
        }
        if (AchievementManager.Instance.Achievements[0].claimed)
        {
            Debug.Log("CLAIMED 0");
            redSquare.SetActive(false);
            redSquareClaim.SetActive(false);
        }
        if (AchievementManager.Instance.Achievements[1].claimed)
        {
            Debug.Log("CLAIMED 1");
            greenSquare.SetActive(false);
            greenSquareClaim.SetActive(false);
        }
        StartCoroutine(ClearNotifText());
    }

    IEnumerator ClearNotifText()
    {
        yield return new WaitForSeconds(2f);
        text.GetComponent<Text>().text = "";
    }
    
    // Use this for initialization
    void Start () {
        redSquare.SetActive(false);
        greenSquare.SetActive(false);
        redSquareClaim.SetActive(false);
        greenSquareClaim.SetActive(false);
        text.SetActive(false);

        StartCoroutine(GreenAchivementGiver());
    }




    IEnumerator GreenAchivementGiver()
    {
        while (true)
        {
            AchievementManager.Instance.AddProgressToAchievement("b", 1);
            Debug.Log("green progress: " + AchievementManager.Instance.Achievements[1].currentProgress);
            if (AchievementManager.Instance.Achievements[1].completed)
            {
                break;
            }
            yield return new WaitForSeconds(1);
        }       
    }

    public void Pressed_ClaimReward(string achivementName)
    {
        AchievementManager.Instance.ClaimAchievementReward(achivementName);
    }

    public void Pressed_RedAchivementGiver()
    {
        AchievementManager.Instance.AddProgressToAchievement("a", 1);
    }
    */
}
