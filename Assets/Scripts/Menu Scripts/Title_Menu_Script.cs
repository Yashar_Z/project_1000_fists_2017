﻿using UnityEngine;
using System.Collections;

public class Title_Menu_Script : MonoBehaviour {

	public Menu_BackAnims_Script menuBackAnimsScriptHolder;

	public ParticleSystem particleMustache1;
	public ParticleSystem particleMustache2;

	public GameObject objMainTitleHolder1;
	public GameObject objMainTitleHolder2;

	public GameObject objVASTitleHolder1;
	public GameObject objVASTitleHolder2;

	public GameObject[] objMenuTitleVasTitleArr1;
	public GameObject[] objMenuTitleVasTitleArr2;

	void Start () {
		if (PlayerData_Main.Instance != null) {
			if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.VAS) {
				objMainTitleHolder1.SetActive (false);
				objMainTitleHolder2.SetActive (false);

				objVASTitleHolder1.SetActive (true);
				objVASTitleHolder2.SetActive (true);

				switch (PlayerData_Main.Instance.whatVAS_Type) {
				case PlayerData_Main.VAS_NameType.sibile:
					for (int i = 0; i < 2; i++) {
						objMenuTitleVasTitleArr1 [i].SetActive (true);
						objMenuTitleVasTitleArr2 [i].SetActive (false);
					}
					break;
				case PlayerData_Main.VAS_NameType.moshte:
					for (int i = 0; i < 2; i++) {
						objMenuTitleVasTitleArr1 [i].SetActive (false);
						objMenuTitleVasTitleArr2 [i].SetActive (true);
					}
					break;
				default:
					Debug.LogError ("Default Error");
					break;
				}
			}
		}
	}

	public void Particle_TitleToMustache () {
		particleMustache1.Play ();
		particleMustache2.Play ();
	}

	public void Pressed_MenuTitle_TapAnim () {
		Title_Anim_TapAnim ();

        // SOUNDD EFFECTS - click roye matn hezar moshtan main menu (done)
        if (SoundManager.Instance != null)
        {
			SoundManager.Instance.SibilShodaneNeveshte.PlayDelayed(0.01f);
            SoundManager.Instance.VorudeNeveshte.PlayDelayed(2.00f);
            SoundManager.Instance.VorudeNeveshteBaZanjir.PlayDelayed(3.2f);
        }

    }

    public void Title_Anim_TapAnim () {
		menuBackAnimsScriptHolder.MenuAnims_Title_TapAnim ();
	}

	public void Title_Anim_RepeatIntro () {
		menuBackAnimsScriptHolder.MenuAnims_Title_RepeatIntro ();
	}

	public void Title_Anim_Idle () {
		menuBackAnimsScriptHolder.MenuAnims_Title_Idle ();
	}
}
