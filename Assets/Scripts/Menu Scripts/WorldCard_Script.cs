﻿using UnityEngine;
using System.Collections;

public class WorldCard_Script : MonoBehaviour {

	public Animator animatorWorldCardHolder;

	void Animator_Disable () {
		animatorWorldCardHolder.enabled = false;
	}

	void Animator_Enable () {
		animatorWorldCardHolder.enabled = true;
	}

}
