﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class World_Button_Script : MonoBehaviour {

	public GameObject objTooltipWorldHolder;
	public Popup_Tooltips_Script textToolTipWorldHolder;

	public Button buttonWorld_FreeToLogicHolder;
	public Button buttonWorld_AvailableToLogicHolder;
	public Button buttonWorld_LockedToLogicHolder;

	private bool allowWorldScaling;
	private float floatScaleBuffer;

//		Debug.LogWarning ("NAMEEEE: " + GetComponentInParent<Menu_Main_Script> ().transform.name);

	public void WorldButton_FistHitAnim (int whichHit) {
		GetComponentInParent<Worlds_All_Script> ().Worlds_Anim_CamHit (whichHit);
	}

	public void WorldAnimEnd_ToAvailable () {
		GetComponentInParent<Menu_Main_Script> ().Check_CanUnlockWorld ();
	}

	public void WorldAnimEnd_ToFree_Stars () {
		GetComponentInParent<Menu_Main_Script> ().Check_AllMiniCutsceneEnd ();
	}

	public void Pressed_FreeToLogicHolder () {
		buttonWorld_FreeToLogicHolder.onClick.Invoke ();
	}

	public void Pressed_AvailableToLogicHolder () {
		buttonWorld_AvailableToLogicHolder.onClick.Invoke ();
	}

	public void Pressed_LockedToLogicHolder () {
		buttonWorld_LockedToLogicHolder.onClick.Invoke ();
	}

	public void Pressed_LockedWorld () {
		Debug.LogError ("World Unlocked = " + PlayerData_Main.Instance.player_WorldsUnlocked);

		if (PlayerData_Main.Instance.player_WorldsUnlocked < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer) {
			textToolTipWorldHolder.stringToolTipsText = " ﯽﻨﮐ ﻡﻮﻤﺗ ﻭﺭ " + PlayerData_Main.Instance.player_WorldsUnlocked + " ﻞﺼﻓ ﺪﯾﺎﺑ ﻝﻭﺍ";
		} else {
			textToolTipWorldHolder.stringToolTipsText = "(!ﺭﻮﺸﮐ ﺮﺳﺍﺮﺳ ﯼﺎﻫ ﯽﺷﻮﮔ ﺭﺩ) ﯼﺩﻭﺯ ﻪﺑ";
		}

		objTooltipWorldHolder.SetActive (true);
	}

	public void Pressed_AvailableWorld () {
		if (PlayerData_Main.Instance.player_WorldsUnlocked < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer) {
			textToolTipWorldHolder.stringToolTipsText = ".ﻦﮐ ﻩﺩﺎﻔﺘﺳﺍ ﺪﯿﻠﮐ ﺯﺍ ﺎﯾ ،ﻦﮐ ﻊﻤﺟ ﻩﺭﺎﺘﺳ ﯽﻓﺎﮐ ﻩﺯﺍﺪﻧﺍ ﻪﺑ ﺎﯾ";
		} else {
			textToolTipWorldHolder.stringToolTipsText = "(!ﺭﻮﺸﮐ ﺭﺍﺮﺳ ﯼﺎﻫ ﯽﺷﻮﮔ ﺭﺩ) ﯼﺩﻭﺯ ﻪﺑ";
		}

		objTooltipWorldHolder.SetActive (true);
	}

	public void Pressed_WorldButtonSelectedScale () {
//		StartCoroutine (WorldButton_Scale ());
	}

	IEnumerator WorldButton_Scale () {
		allowWorldScaling = true;
		floatScaleBuffer = 0.45F;
		transform.localScale = new Vector3 (floatScaleBuffer, floatScaleBuffer, floatScaleBuffer);

		while (allowWorldScaling) {
			floatScaleBuffer += 0.12F;
			transform.localScale = new Vector3 (floatScaleBuffer, floatScaleBuffer, floatScaleBuffer);
			yield return new WaitForSeconds (0.01F);
			if (transform.localScale.x > 1) {
				allowWorldScaling = false;
				Debug.LogError ("Size reached");
			}
		}

		yield return null;
	}
}
