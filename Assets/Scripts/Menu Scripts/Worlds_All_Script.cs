﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Worlds_All_Script : MonoBehaviour {

	public Text text_Page_NameCurr;
	public bool allowOpenLevelsMenu;
	public bool disallowWorldsClickBlocker2;

	public GameObject levelsAllObjectHolder;
	public GameObject objTooltipPhoneStatsHolder_Attribs;
	public GameObject objTooltipPhoneQuestsHolder_Quest;
	public GameObject objTooltipSlotBuyHolder;

	public GameObject[] objWorldSelectGoldArr;
	public GameObject[] objWorldSelectGoldBackArr;

	public Menu_Main_Script mainMenuScripHolder;

	public Button buttonPhoneSCutHolder;
	public Button buttonPhoneSC_FrontOverHolder;

	public Button buttonPhoneStats_FrontOverHolder;
	public Button buttonPhoneQuests_FrontOverHolder;

	public Button buttonPhoneToItemsHolder;
	public Button buttonPhoneToItems_FrontOverHolder;

	public Button buttonBatterySlot_AddNowHolder;
	public Button buttonBatterySlot_FrontOverHolder;

	// For new level back button and replacement of Level Click Behind
	public Button buttonLevelBackButtonHolder;

	public ParticleSystem particleBatteryHeatIdle;
	public ParticleSystem particleBatteryFlareIdle;

	public Animation worldsAllAnimRef;
	public ScrollRect scrollLevelsHolder;

	[Header ("Worlds Canvas")]
	public Canvas canvasWorldHolder;
	public GraphicRaycaster graphRayCasterHolder;

	// For worlds select / levels select parent
	public Canvas canvasWorldsSelectParent;
	public Canvas canvasLevelssSelectParent;

//	public UnityEngine.UI.ScrollRect worldsScrollHolder;

//	private string[] text_Page_NamesArr;
	private string menu_page_name;

	// Called via menu main script
	public void WorldsCards_DoOnAwake_Golds () {

		// This disabled all worlds on return to menu from game
//		AnimEvent_CanvasDisable ();
	}


	public void WorldsCards_DoOnAwake_Canvas () {
		// Activate gold fronts
		WorldsGoldActivator ();
	}

	void Start () {
		// tick bools for auto open anims
		disallowWorldsClickBlocker2 = false;
		allowOpenLevelsMenu = false;

		// To have "Which mahale" at start
		PageName_Reset ();

		// Disable battery idle particles at the start
		BatteryParticles_DeActivateObj ();

//		worldsAllAnimRef = GetComponent<Animation> ();
	}

	// Called view somethingOnAwake
	void WorldsGoldActivator () {
		Worlds3Stars_ReCheckAchieves ();

		// Worlds Count (Is based on available worlds in menu tut check script)
		for (int i = 0; i < PlayerData_Main.Instance.lastPossibleWorld_ForThisVer; i++) {
			if (PlayerData_Main.Instance.worldsGoldStarsArr [i] > 29) {
				objWorldSelectGoldArr [i].SetActive (true);
				objWorldSelectGoldBackArr [i].SetActive (true);
			}
		}
	}

	public void Worlds3Stars_ReCheckAchieves () {
//		Debug.LogWarning ("stars = " + PlayerData_Main.Instance.player_Stars);

		if (AchievementManager.Instance != null) {
			AchievementManager.Instance.Achieve_PrevWorlds3Stars ();

//			StartCoroutine (AchievementManager.Instance.AchieveProg_StarSet (PlayerData_Main.Instance.player_Stars));
//
//			StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_1Set (PlayerData_Main.Instance.worldsGoldStarsArr [0]));
//			StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_2Set (PlayerData_Main.Instance.worldsGoldStarsArr [1]));
//			StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_3Set (PlayerData_Main.Instance.worldsGoldStarsArr [2]));
//			StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_4Set (PlayerData_Main.Instance.worldsGoldStarsArr [3]));
//			StartCoroutine (AchievementManager.Instance.AchieveProg_World3Star_5Set (PlayerData_Main.Instance.worldsGoldStarsArr [4]));
		}
	}

	public void Pressed_WorldsLevelsBack () {
		if (mainMenuScripHolder.isLevelsInFront) {
			buttonLevelBackButtonHolder.onClick.Invoke ();
		} else {
			// Copied from old button
			Worlds_Anim_ExitNow ();
			mainMenuScripHolder.ZoomOut_CheckBack (2);
		}
	}

	public void Worlds_Anim_Play (string animName) {
		worldsAllAnimRef.Stop ();
		worldsAllAnimRef.Play (animName);
	}

	public void Worlds_Anim_EnterNow () {
		worldsAllAnimRef.Stop ();
		worldsAllAnimRef.Play ("Menu - World Enter Anim 1 (Legacy)");
	}

	public void Worlds_Anim_ExitNow () {
		worldsAllAnimRef.Stop ();
		worldsAllAnimRef.Play ("Menu - World Exit Anim 1 (Legacy)");
	}

	public void Worlds_Anim_CamHit (int whichCamHit) {
		switch (whichCamHit) {
		case 1:
			Worlds_Anim_Play ("Menu - CamLike - Level Unlock Anim 1 (Legacy)");
			break;
		case 2:
			Worlds_Anim_Play ("Menu - CamLike - World Available Anim 1 (Legacy)");
			break;
		case 3:
			Worlds_Anim_Play ("Menu - CamLike - World Available Anim 1 (Legacy)");
			break;
		default:
			Worlds_Anim_Play ("Menu - CamLike - World Available Anim 1 (Legacy)");
			break;
		}
	}

	public void Worlds_Scroll_Reset () {
//		worldsScrollHolder.horizontalNormalizedPosition = 0F;
//		Invoke ("Worlds_Scroll_ResetNOW", 0.2F);
	}

	public void Worlds_Scroll_ResetNOW () {
//		worldsScrollHolder.horizontalNormalizedPosition = 0F;
	}

	public void MoveOut_WorldsAll () {
		transform.localPosition = new Vector2 (3200, 0);

		canvasWorldsSelectParent.enabled = false;
		canvasLevelssSelectParent.enabled = false;

		mainMenuScripHolder.ZoomOut_OnlyMenuActivator ();

		AnimEvent_CanvasDisable ();
	}

	public void AnimEvent_MoveInWorlds () {
		Phone_Stats_Script.Instance.startOfShortCutWhat = Phone_Stats_Script.StartOfShortCutType.Phone_WorldMenu;

		canvasWorldsSelectParent.enabled = true;
		canvasLevelssSelectParent.enabled = false;

		AnimEvent_CanvasEnable ();

//		Debug.LogWarning ("Move in Worlds Canvas!");
	}

	public void AnimEvent_LevelsCanvasEnable () {
		StartCoroutine (LevelsCanvasEnable ());
	}

	IEnumerator LevelsCanvasEnable () {
		canvasLevelssSelectParent.enabled = true;
		yield return new WaitForSeconds (0.2F);
		canvasWorldsSelectParent.enabled = false;
	}

	public void AnimEvent_LevelsCanvasDisable () {
		StartCoroutine (LevelsCanvasDisable ());
	}

	IEnumerator LevelsCanvasDisable () {
		canvasWorldsSelectParent.enabled = true;
		yield return new WaitForSeconds (0.2F);
		canvasLevelssSelectParent.enabled = false;
	}

	public void AllWorlds_Scroll () {
		mainMenuScripHolder.ScrollWorlds_Set_CurrWorld ();
	}

	public void LevelsScroll_Enable () {
		scrollLevelsHolder.enabled = true;
	}

	public void LevelsScroll_Disable () {
		scrollLevelsHolder.enabled = false;
	}

	public void DeActivate_Worlds_All () {
		this.gameObject.SetActive (false);
	}

	public void DeActivate_Levels_All () {
//		levelsAllObjectHolder.SetActive (false);
	}

	public void Activate_Levels_All () {
//		levelsAllObjectHolder.SetActive (true);
	}

	public void PageName_Change () {
		text_Page_NameCurr.text = PlayerData_Main.Instance.WorldNumber_FullName;
	}

	public void PageName_Reset () {
		text_Page_NameCurr.text = "ﻞﺼﻓ ﺏﺎﺨﺘﻧﺍ";
	}

	public void PageName_Reset_OLD () {
		text_Page_NameCurr.text = "؟ﻪﻠﺤﻣ ﻡﻭﺪﮐ";
	}

	public void AutoOpenLevels_Allow () {
		// TODO: Check for unlock world
		allowOpenLevelsMenu = true;
	}

	public void AutoOpenBlocker_DisAllow () {
		disallowWorldsClickBlocker2 = true;
	}

	public void Go_CheckCanUnlock () {
		mainMenuScripHolder.Check_CanUnlockWorld ();
	}

	public void LevelScroll_EndUnlock () {
		mainMenuScripHolder.Level_UnlockLightNow ();
	}

	public void LevelScroll_Check () {
		// Also blocker disable
		AutoOpenBlocker_DisAllow ();

//		Invoke ("AutoOpenBlocker_DisAllow", 0.4F);

		if (mainMenuScripHolder.inNewestWorld) {
			StartCoroutine (mainMenuScripHolder.ScrollLevels_Set_BlendStart (0));
		} else {
			mainMenuScripHolder.ScrollLevels_Set_Zero ();
		}
	}

	public void PhoneSC_FrontOverActivator () {
		StartCoroutine (PhoneSC_FrontOverActivator_NOW ());
	}

	public IEnumerator PhoneSC_FrontOverActivator_NOW () {
		buttonPhoneSC_FrontOverHolder.gameObject.SetActive (true);
		buttonPhoneSC_FrontOverHolder.interactable = false;
//		Debug.LogError ("BEFORE");
		yield return new WaitForSeconds (0.5F);
//		Debug.LogError ("AFTER");
		buttonPhoneSC_FrontOverHolder.interactable = true;
	}

	// Pay Attention! There are TWO Phone elements. This one is for shortcut but the others in Item_Menu_script are for ITEM INFO PHONE!!!
	public void Pressed_PhoneSC_FrontOverButton () {
		buttonPhoneSCutHolder.onClick.Invoke ();
	}

	public void PhoneStats_FrontOverActivator () {
		StartCoroutine (PhoneStats_FrontOverActivator_NOW ());
	}

	public IEnumerator PhoneStats_FrontOverActivator_NOW () {
		buttonPhoneStats_FrontOverHolder.gameObject.SetActive (true);
		buttonPhoneStats_FrontOverHolder.interactable = false;

		yield return new WaitForSeconds (0.4F);
		// Show tooltip as front over button is called upon
		objTooltipPhoneStatsHolder_Attribs.SetActive (true);

//		Debug.LogError ("BEFORE Stats");
		yield return new WaitForSeconds (1);
//		Debug.LogError ("AFTER Stats");
		buttonPhoneStats_FrontOverHolder.interactable = true;
	}

	public void PhoneQuests_FrontOverActivator () {
		StartCoroutine (PhoneQuests_FrontOverActivator_NOW ());
	}

	public IEnumerator PhoneQuests_FrontOverActivator_NOW () {
		buttonPhoneQuests_FrontOverHolder.gameObject.SetActive (true);
		buttonPhoneQuests_FrontOverHolder.interactable = false;

		yield return new WaitForSeconds (0.4F);
		// Show tooltip as front over button is called upon
		objTooltipPhoneQuestsHolder_Quest.SetActive (true);

//		Debug.LogError ("BEFORE Quests");
		yield return new WaitForSeconds (1);
//		Debug.LogError ("AFTER Quests");

		buttonPhoneQuests_FrontOverHolder.interactable = true;
	}

	public void ToItemPage_FrontOverActivator () {
		StartCoroutine (ToItemPage_FrontOverActivator_NOW ());
	}

	public IEnumerator ToItemPage_FrontOverActivator_NOW () {
		buttonPhoneToItems_FrontOverHolder.gameObject.SetActive (true);
		buttonPhoneToItems_FrontOverHolder.interactable = false;

//		Debug.LogError ("BEFORE");
		yield return new WaitForSeconds (1);
//		Debug.LogError ("AFTER");
		buttonPhoneToItems_FrontOverHolder.interactable = true;
	}
		
	public void Pressed_ToItemPage_FrontOverButton () {
		buttonPhoneToItemsHolder.onClick.Invoke ();
	}

	public void BatterySlot_FrontOverActivator () {
		StartCoroutine (BatterySlot_FrontOverActivator_NOW ());
	}

	public IEnumerator BatterySlot_FrontOverActivator_NOW () {
		buttonBatterySlot_FrontOverHolder.gameObject.SetActive (true);
		buttonBatterySlot_FrontOverHolder.interactable = false;

//		Debug.LogError ("BEFORE");
		yield return new WaitForSeconds (1);
//		Debug.LogError ("AFTER");
		buttonBatterySlot_FrontOverHolder.interactable = true;
	}
		
	public void Pressed_BatterySlot_FrontOverButton () {
//		buttonBatterySlot_AddNowHolder.onClick.Invoke ();
		StartCoroutine (BatSlotTooltip_Enable ());

		PlayerData_Main.Instance.TutSeen_BatterySlot = true;
		SaveLoad.Save ();
	}

	public IEnumerator BatSlotTooltip_Enable () {
		yield return new WaitForSeconds (3.2F);
		objTooltipSlotBuyHolder.SetActive (true);
	}

//	public void BatteryParticles_Activate () {
//		StartCoroutine (BatteryParticles_ActivatorNOW (true));
//	}
//
//	public void BatteryParticles_DeActivate () {
//		StartCoroutine (BatteryParticles_ActivatorNOW (false));
//	}

//	public void BatteryParticles_ActivateObj () {
//		particleBatteryHeatIdle.gameObject.SetActive (true);
//		particleBatteryFlareIdle.gameObject.SetActive (true);
//
//	}
//

	public void BatteryParticles_DeActivateObj () {
		particleBatteryHeatIdle.gameObject.SetActive (false);
		particleBatteryFlareIdle.gameObject.SetActive (false);
		
	}

	public void AnimEvent_CanvasEnable () {
		StartCoroutine (World_CanvasEnable ());
	}

	public void AnimEvent_CanvasDisable () {
		canvasWorldHolder.enabled = false;
//		graphRayCasterHolder.enabled = false;
//		StartCoroutine (World_CanvasDisable ());
	}

	IEnumerator World_CanvasEnable () {
		yield return null;
		canvasWorldHolder.enabled = true;
//		graphRayCasterHolder.enabled = true;
	}

	IEnumerator World_CanvasDisable () {
		yield return null;
		canvasWorldHolder.enabled = false;
//		graphRayCasterHolder.enabled = false;
	}

//	public IEnumerator BatteryParticles_ActivatorNOW (bool isActive) {
//		yield return null;
//		if (isActive) {
//			particleBatteryHeatIdle.Play ();
//			particleBatteryFlareIdle.Play ();
//		} else {
//			particleBatteryHeatIdle.Stop ();
//			particleBatteryFlareIdle.Stop ();
//		}
//	}

}
