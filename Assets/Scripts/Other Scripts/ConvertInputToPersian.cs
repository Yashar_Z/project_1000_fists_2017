﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConvertInputToPersian : MonoBehaviour {

	public Text Main;

	public void OnEnteredText(string text)
	{
		Main.text = text.faConvert();
	}
}
