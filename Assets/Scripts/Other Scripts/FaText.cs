﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;


[RequireComponent(typeof(Text))]
public class FaText : MonoBehaviour {

	Text _text;

	public int maxCharInRow = 15;

	public string text
	{
		set
		{ 
			_text.text = ConvertMultiline (value.faConvert (), maxCharInRow);
		}
	}

	void Awake()
	{
		_text = GetComponent<Text> ();
	}

	public string ConvertMultiline(string str, int maxCharInRow)
	{
		string result = "";
		string[] words = str.Split (' ');

		int wordEndInd = words.Length - 1;

//		string rowText = "";

		int usedWords;

		while (wordEndInd >= 0)
		{
			string row = GetFirstRow (words, maxCharInRow, wordEndInd, out usedWords);

			result += row;

			wordEndInd -= usedWords;

			if (wordEndInd >= 0)
				result += "\n";
		}


		return result;
	}

	string GetFirstRow(string[] words, int rowSize, int lastInd, out int usedWords)
	{
		if (words.Length == 0) {
			usedWords = 0;
			return "";
		}
		
		int ind = lastInd;

		int counter = 0;

		while (ind >= 0) {
		
			counter += (words [ind].Length + 1);

			if (counter - 1 > rowSize)
				break;

			ind--;
		}

		usedWords = lastInd - ind;

		if (ind == lastInd)
			throw new System.Exception ("row length is too small");

		//combining words:
		string result = "";

		for (int i = ind + 1; i <= lastInd; ++i) {
			result += words [i] + " ";
		}
		return result;
	}
}
