﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Farsi_Fixer_Script : MonoBehaviour {

	private Text myText;

	// Use this for initialization
	void Awake () {
		myText = GetComponent<Text> ();
		myText.text = myText.text.faConvert();
	}
}
