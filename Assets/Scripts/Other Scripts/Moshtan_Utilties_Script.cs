﻿using UnityEngine;
using System.Collections;

public class Moshtan_Utilties_Script {

	public static void ShowToast(string message)
	{
		#if !UNITY_EDITOR && UNITY_ANDROID
		    AndroidJavaClass plugin = new AndroidJavaClass("com.appete.unityplugin.ToastHelper");
		    AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		    AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		    plugin.CallStatic("ShowToast", activity, message);
		#endif
	}

    public static void SendSMS(string phoneNumber, string message)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
		    AndroidJavaClass plugin = new AndroidJavaClass("com.appete.unityplugin.SendSmsHelper");
		    AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		    AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		    plugin.CallStatic("sendSMSMessage", activity,phoneNumber, message);
#endif
    }

}
