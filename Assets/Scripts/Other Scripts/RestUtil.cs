﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class RestUtil : MonoBehaviour
{

    private string results;

    public string Results
    {
        get
        {
            return results;
        }
    }

    public WWW GET(string url, Action onComplete)
    {
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www, onComplete));
        return www;
    }

	public WWW POST(string url, Dictionary<string, string> headers, Dictionary<string, string> post, Action onComplete)
	{
		WWWForm form = new WWWForm();

		foreach (KeyValuePair<string, string> header_arg in headers)
		{
			form.headers.Add(header_arg.Key, header_arg.Value);
		}

		foreach (KeyValuePair<string, string> post_arg in post)
		{
			form.AddField(post_arg.Key, post_arg.Value);
		}


		WWW www = new WWW(url, form);

		StartCoroutine(WaitForRequest(www, onComplete));
		return www;
	}

    private IEnumerator WaitForRequest(WWW www, Action onComplete)
    {
        yield return www;
        // check for errors
        if (www.error == null || www.error == "")
        {
            results = www.text;
            onComplete();
        }
        else
        {
            Debug.Log(www.error);
        }
    }
}
