﻿using UnityEngine;
using System.Collections;

public class Popup_Achieve_Script : MonoBehaviour {

	public UnityEngine.UI.ScrollRect scrollAchievesHolder;
//	public UnityEngine.UI.GraphicRaycaster graphicRaycastAchievesHolder;
	public Canvas canvasAchieveHolder;

	public Achievement_AllController_Script achieveAllControlScriptHolder;
	public Achievement_ActivatorScript achieveActivatorScriptHolder;

	public UnityEngine.UI.Button buttonBackAchievesLogic;

	public bool wasScrolledRight_1;
	public bool wasScrolledRight_2;
	public bool wasScrolledRight_3;

	// This is called from achievements main menu button
	public void Achieves_MoveIn () {
		// Force re-center Settings Mover (Posit and Anim)
		this.transform.localPosition = Vector3.zero;

		Phone_Stats_Script.Instance.startOfShortCutWhat = Phone_Stats_Script.StartOfShortCutType.Phone_AchievesMenu;

		// Use anim event for setup items
//		StartCoroutine (PopUpAchieves_SetupItems ());

		// Moved to above coroutine
//		achieveAllControlScriptHolder.AchievesAll_PhoneEnterSetup ();

		AnimEvent_CanvasEnable ();

		StartCoroutine (ScrollBoolsReset ());
	}

	IEnumerator ScrollBoolsReset () {
		yield return new WaitForSeconds (0.3F);
		wasScrolledRight_1 = false;
		wasScrolledRight_2 = false;
		wasScrolledRight_3 = false;
	}

	public void ScrollRectAchieves_Reset () {
		StartCoroutine (ScrollRectAchieves_ResetRoutine ());
	}

	void Awake () {
		AnimEvent_CanvasDisable ();
	}

	public void Pressed_BackAchieves () {
		buttonBackAchievesLogic.onClick.Invoke ();

		// New routine with nulls deactivator
		StartCoroutine (achieveActivatorScriptHolder.AchievePackage_DelayedDisables ());

		// This was mostly instant disable (Now use above via achieve activator)
//		StartCoroutine (PopUpAchieves_DisableItems ());

		// Deactivate Box Collider for achieve activator
//		achieveAllControlScriptHolder.achieveActivatorScriptHolder.BoxCollider_DeActivate ();
	}

	public IEnumerator PopUpAchieves_DisableItems () {
		yield return null;
//		yield return new WaitForSeconds (0.1F);
		achieveAllControlScriptHolder.AhieveAll_EnableDisabler (false);
	}

	public void AnimEvent_SetupItems () {
		scrollAchievesHolder.horizontalNormalizedPosition = -0.05F;
		StartCoroutine (PopUpAchieves_SetupItems ());
	}

	public IEnumerator PopUpAchieves_SetupItems () {
//		yield return null;
		// Activate Box Collider for achieve activator
//		achieveAllControlScriptHolder.achieveActivatorScriptHolder.BoxCollider_Activate ();

		yield return null;
		achieveAllControlScriptHolder.AchievesAll_PhoneEnterSetup ();
	}

	IEnumerator ScrollRectAchieves_ResetRoutine () {
		scrollAchievesHolder.enabled = false;
		yield return null;
		scrollAchievesHolder.horizontalNormalizedPosition = 0;
		yield return null;
//		scrollAchievesHolder.enabled = true;
	}

	public void AnimEven_ScrollResetInstant () {
//		scrollAchievesHolder.enabled = false;
		scrollAchievesHolder.horizontalNormalizedPosition = 0;
	}

	public void AnimEvent_ScrollEnable () {
		scrollAchievesHolder.enabled = true;
	}

	public void MoveOut_AchievesAll () {
		transform.localPosition = new Vector2 (3200, 0);

		achieveAllControlScriptHolder.AhieveAll_EnableDisabler_INSTANTLY (false);

		this.gameObject.SetActive (false);
		AnimEvent_CanvasDisable ();
	}

	public void AnimEvent_MoveOutAchieves () {
		MoveOut_AchievesAll ();
	}

	public void AnimEvent_CanvasEnable () {
		StartCoroutine (Achieve_CanvasEnable ());
	}

	public void AnimEvent_CanvasDisable () {
		canvasAchieveHolder.enabled = false;
//		graphicRaycastAchievesHolder.enabled = false;
//		StartCoroutine (Achieve_CanvasDisable ());
	}

	public IEnumerator Achieve_CanvasEnable () {
		
		canvasAchieveHolder.enabled = true;
//		graphicRaycastAchievesHolder.enabled = true;
		yield return null;
	}

	public IEnumerator Achieve_CanvasDisable () {
		
		canvasAchieveHolder.enabled = false;
//		graphicRaycastAchievesHolder.enabled = false;
		yield return null;
	}

	public void ScrollActivatorCheck () {
		if (!wasScrolledRight_1) {
			if (scrollAchievesHolder.horizontalNormalizedPosition > 0.04F) {
				StartCoroutine (achieveAllControlScriptHolder.AhieveAll_EnableWithMiniDelays_NumberToNumber (10, 20));
				wasScrolledRight_1 = true;
				//				Debug.LogError ("wasScrolledRight_1");
			}
		} else {
			if (!wasScrolledRight_2) {
				if (scrollAchievesHolder.horizontalNormalizedPosition > 0.16F) {
					StartCoroutine (achieveAllControlScriptHolder.AhieveAll_EnableWithMiniDelays_NumberToNumber (20, 40));
					wasScrolledRight_2 = true;
					//				Debug.LogError ("wasScrolledRight_2");
				}
			} else {
				if (!wasScrolledRight_3) {
					if (scrollAchievesHolder.horizontalNormalizedPosition > 0.4F) {
						StartCoroutine (achieveAllControlScriptHolder.AhieveAll_EnableWithMiniDelays_NumberToEnd (40));
						wasScrolledRight_3 = true;
						//				Debug.LogError ("wasScrolledRight_3");
					}
				}
			}
		}
	}
		
	public void WhileScrolling () {
//		ScrollActivatorCheck ();
	}

	void Update () {
		ScrollActivatorCheck ();
	}

}
