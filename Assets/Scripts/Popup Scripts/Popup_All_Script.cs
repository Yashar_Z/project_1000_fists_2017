﻿using UnityEngine;
using System.Collections;

public class Popup_All_Script : MonoBehaviour {

	public AnimationClip[] animPopUpsArr;
	public Animation animPopUpsHolder;
//	public GameObject buttonNoObjHolder;

	public bool isMoveOutOnDisable;

	public Animation[] animButtonsAnimsArr;
	private int animIntBy10;

//	public void Awake () {
//		PopUp_DisableObj ();
//	}

	public void OnEnable () {
		if (!isMoveOutOnDisable) {
			PopUp_AnimPlay (0);
		}
	}

	public void PopUp_AnimPlay (int whichAnim) {
		animPopUpsHolder.clip = animPopUpsArr[whichAnim];
		animPopUpsHolder.Play ();
	}

	public void PopUp_AnimaNClipPlay (int whichAnimNClip) {
		animIntBy10 = whichAnimNClip / 10;
		animButtonsAnimsArr [animIntBy10].clip = animPopUpsArr [whichAnimNClip - animIntBy10];
		animButtonsAnimsArr [animIntBy10].Play ();
	}

	public void PopUp_DisableObj () {
		if (!isMoveOutOnDisable) {
			this.gameObject.SetActive (false);
		} else {
			MoveOut_AnyPopUp ();
		}
	}

	public void PopUp_MoveOutAndDisable () {
		transform.localPosition = new Vector2 (3200, 0);
		this.gameObject.SetActive (false);
	}

	public void MoveOut_AnyPopUp () {
		transform.localPosition = new Vector2 (3200, 0);
	}

	public void PopUp_DisableOtherButtons () {
	}

//	public void PopUp_ButtonNO () {
//		buttonNoObjHolder.GetComponent<UnityEngine.UI.Button> ().onClick.Invoke ();
//	}
}
