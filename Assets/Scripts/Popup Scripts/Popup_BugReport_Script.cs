﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup_BugReport_Script : MonoBehaviour {
	//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;

	public InputField inputField_Name;
	public InputField inputField_Description;
	public Button buttonSendRealHolder;

	public GameObject objSendingTextHolder;
	public GameObject objUnsuccessfulTextHolder;

	public bool wasSuccessful;

	private string string_EnteredName;
	private string string_EnteredDescription;

	private int intTransCounter = 0;
	private Transform[] transBugRepALL_Arr;

	void OnEnable () {
		Invoke ("ButtonAnim_StartEnters", 0.2F);
		Reset_InputFields ();

		// SOUNDD EFFECTS - Pause Pressed (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.PressedPause.Play();
			SoundManager.Instance.PopUp_Pause.Play();
		}
	}

	void Start () {
		transBugRepALL_Arr = GetComponentsInChildren <Transform> ();
		intTransCounter = 47;
	}

	void Reset_InputFields () {
		inputField_Name.text = "";
		inputField_Description.text = "";
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (0);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void Tick_All_NextTransform () {
		intTransCounter--;
		Debug.LogError ("Number: " + intTransCounter);
//		Moshtan_Utilties_Script.ShowToast ("Ticked: " + transBugRepALL_Arr [intTransCounter].name);
		transBugRepALL_Arr [intTransCounter].gameObject.SetActive (false);
	}

	public void Tick_1B1_NextTransform () {
		intTransCounter--;
		transBugRepALL_Arr [intTransCounter + 1].gameObject.SetActive (true);

//		Moshtan_Utilties_Script.ShowToast ("Ticked: " + transBugRepALL_Arr [intTransCounter].name);
		transBugRepALL_Arr [intTransCounter].gameObject.SetActive (false);
	}

	public void Pressed_Send_BugReport () {
		// Show sending message on press
		objSendingTextHolder.SetActive (true);

		string_EnteredName = inputField_Name.text;
		string_EnteredDescription = inputField_Description.text;

		GetComponent<BugReport_Main_Script> ().SendReport (string_EnteredName, string_EnteredDescription,Success_Check);

		// For successful
//		Success_Check ();
	}

	public void Success_Check (bool wasReallySuccessful) {
		// Remove sending message on press
		objSendingTextHolder.SetActive (false);

		wasSuccessful = wasReallySuccessful;
		if (wasSuccessful) {
			Debug.Log ("Success!");
			buttonSendRealHolder.onClick.Invoke ();
		} else {
			Debug.Log ("No Success");
			Unsuccessful_Show ();
		}
	}

	public void Unsuccessful_Show () {
		objUnsuccessfulTextHolder.SetActive (true);
		Invoke ("Unsuccessful_Hide", 1.6F);
	}

	public void Unsuccessful_Hide () {
		objUnsuccessfulTextHolder.SetActive (false);
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}
}
