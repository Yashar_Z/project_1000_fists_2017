﻿using UnityEngine;
using System.Collections;

public class Popup_Button_Script : MonoBehaviour {

	public AnimationClip[] animClipsButtonGeneralArr;
	public Animation animButtonHolder;
	public Transform transButtonMoverHolder;

	public void OnEnable () {
		transButtonMoverHolder.localScale = Vector3.zero;

		//		Debug.LogWarning ("My name is: " + transform.name +  "  And my parent is: " 
		//			+ transform.parent.name + "  And my parents parent is: " + transform.parent.parent.name);
	}

	public void Button_PlayAnim (int whichAnim) {
		animButtonHolder.clip = animClipsButtonGeneralArr [whichAnim];
		animButtonHolder.Play ();
	}

	public void Button_PlayAnim_LeaveUp () {
		Button_PlayAnim (1);

        // SOUNDD EFFECTS: Pressed Normal Pop-up button (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.GenericTap2.Play();
        }
	}

	public void Button_PlayAnim_LeaveRight () {
		Button_PlayAnim (2);
	}

	public void Button_PlayAnim_LeaveLeft () {
		Button_PlayAnim (3);
	}

	public void ButtonDaily_PlayAnim_LeaveUp () {
		Button_PlayAnim (4);

		// SOUNDD EFFECTS: Pressed Normal Pop-up button (done)
		if (SoundManager.Instance!= null)
		{
			SoundManager.Instance.GenericTap2.Play();
			SoundManager.Instance.SoudeTokhmeMorgh.Play();
		}
	}
}
