﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Popup_DailyReward_Script : MonoBehaviour {

	public Reward_Machine_Script rewardMachineScriptHolder;
	public AfterLoadDailyReward_Script afterLoadDailyHolder;
	public Popup_Tooltips_Script textTooltipNoVidHolder;

	public Canvas canvasDailyHolder;

	public Text textDailyAmount_YesVid;
	public Text textDailyAmount_NoVid;

	public Button buttonNoVidHolder;
	public Button buttonYesVidHolder;
	public Button buttonYesVidFRONTHolder;

	public GameObject objToolTipNoVidAvailable;
	public GameObject objToolTipTapOnOR;

	private bool boolCheckingYesVid;

	private int intRandomDailyNumber;
	private int intDailyRewardAmount_NoVid;
	private int intDailyRewardAmount_YesVid;

	private int intDailyRewardAmount_Chosen;

//	public Popup_Button_Script[] popupButtonScriptArr;

	void OnEnable () {
//		Invoke ("ButtonAnim_StartEnters", 0.4F);

		MoveIn_DailyReward ();

		intRandomDailyNumber = Random.Range (0, 100);
		if (intRandomDailyNumber < 44) {
			// is daily number 1
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid1;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid1;
		} 
		else if (intRandomDailyNumber < 82) {
			// is daily number 2
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid2;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid2;
		}
		else if (intRandomDailyNumber < 91){
			// is daily number 3
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid3;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid3;
		}
		else if (intRandomDailyNumber < 96) {
			// is daily number 4
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid4;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid4;
		}
		else if (intRandomDailyNumber < 99) {
			// is daily number 5
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid5;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid5;
		}
		else
		{
			// is daily number 6
			intDailyRewardAmount_NoVid = Balance_Constants_Script.DailyReward_NoVid6;
			intDailyRewardAmount_YesVid = Balance_Constants_Script.DailyReward_YesVid6;
		}

		textDailyAmount_NoVid.text = intDailyRewardAmount_NoVid.ToString ();
		textDailyAmount_YesVid.text = intDailyRewardAmount_YesVid.ToString ();

		// SOUND EFFECTS - Daily Pop-up Enter? (EMPTY)
	}

	void OnDisable () {
		MoveOut_DailyReward ();
	}

//	void ButtonAnim_StartEnters () {
//		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
//			StartCoroutine( ButtonAnim_Enter (i));
//		}
//	}

//	IEnumerator ButtonAnim_Enter (int whichButton) {
//		yield return new WaitForSeconds (whichButton * 0.1F);
//		popupButtonScriptArr [whichButton].Button_PlayAnim (0);
//
//		// SOUND EFFECTS - Daily One Button Pop-in (EMPTY)
//	}

//	public void PopUp_InvokeButton (int whichButton) {
//		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
//	}
		
	void MoveIn_DailyReward () {
		canvasDailyHolder.enabled = true;
		this.transform.localPosition = Vector2.zero;
	}

	void MoveOut_DailyReward () {
		canvasDailyHolder.enabled = false;
		this.transform.localPosition = new Vector2 (3200, 0);
	}

	public void Pressed_YesVidFrontButton () {
//		if (afterLoadDailyHolder.DailyRewardAdId == null) {
//			// Enable no video available tooltip
//			NoVid_Tooltip_Message (afterLoadDailyHolder.intMessageDailyRewardAd);
//		} 
//
//		else {

		// Disable now (TODO: Enable later)
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {

			if (SoundManager.Instance != null) {
				SoundManager.Instance.GenericTap2.Play ();
			}

            if (Tapligh_Script.Instance.TaplighAvailable)
            {
                Tapligh_Script.Instance.OnTaplighRewardEligible += InvokeYesVidButton;
                StartCoroutine(DisableAndEnableButtonsAfterSomeSeconds());
                Tapligh_Script.Instance.ShowAd();

            }
            else if (afterLoadDailyHolder.videoAvailable)
            {
                StartCoroutine(YesVid_Checking());
            }
            else
            {
                objToolTipNoVidAvailable.SetActive(true);
                //			NoVid_Tooltip_Message (5);
            }
		} 

		else {
			Moshtan_Utilties_Script.ShowToast ("این قابلیت در آینده فعال خواهد شد.");
		}

//		}
	}

	public void NoVid_Tooltip_Message (int whatInt) {

		switch (whatInt) {
		case 1:
			textTooltipNoVidHolder.stringToolTipsText = "--------------------";
			break;
		case 2:
			textTooltipNoVidHolder.stringToolTipsText = ".ﺖﺴﯿﻧ ﺩﻮﺟﻮﻣ ﻮﯾﺪﯾﻭ";
			break;
		case 3:
			textTooltipNoVidHolder.stringToolTipsText = ".ﺩﺭﺍﺩ ﻞﮑﺸﻣ ﻪﮑﺒﺷ";
			break;
		case 4:
			textTooltipNoVidHolder.stringToolTipsText = ".ﺖﺴﯿﻧ ﻞﺻﻭ ﺖﻧﺮﺘﻨﯾﺍ";
			break;
		case 5:
			textTooltipNoVidHolder.stringToolTipsText = "!ﺪﯿﺷﺎﺑ ﺭﻮﺒﺻ ...ﻮﯾﺪﯾﻭ ﺩﻮﻠﻧﺍﺩ ﻝﺎﺣ ﺭﺩ";
			break;
		default:
			textTooltipNoVidHolder.stringToolTipsText = ".ﺪﺷ ﺩﺎﺠﯾﺍ ﻮﯾﺪﯾﻭ ﺶﺨﭘ ﺭﺩ ﻞﮑﺸﻣ";
			break;
		}

		objToolTipNoVidAvailable.SetActive (true);
	}

	public void Pressed_TapOnOR () {
		objToolTipTapOnOR.SetActive (true);
	}

    IEnumerator DisableAndEnableButtonsAfterSomeSeconds()
    {
        // Disable buttons while checking
        buttonNoVidHolder.enabled = false;
        buttonYesVidHolder.enabled = false;
        buttonYesVidFRONTHolder.enabled = false;
        yield return new WaitForSeconds(5f);
        // Disable buttons while checking
        buttonNoVidHolder.enabled = true;
        buttonYesVidHolder.enabled = true;
        buttonYesVidFRONTHolder.enabled = true;
    }

    IEnumerator YesVid_Checking () {

        StartCoroutine(DisableAndEnableButtonsAfterSomeSeconds());
		afterLoadDailyHolder.ShowVideo ();

		boolCheckingYesVid = true;
		while (!afterLoadDailyHolder.acceptVidButtonPress) {
			yield return new WaitForSeconds (0.1F);
		}

		// Enable buttons AFTER checking
//		buttonNoVidHolder.enabled = true;
//		buttonYesVidHolder.enabled = true;
//		buttonYesVidFRONTHolder.enabled = true;

		InvokeYesVidButton ();
	}

	public void InvokeYesVidButton () {
        Tapligh_Script.Instance.OnTaplighRewardEligible -= InvokeYesVidButton;
        buttonYesVidHolder.onClick.Invoke ();
	}

	public void DailyAmountSet_NoVid () {
		intDailyRewardAmount_Chosen = intDailyRewardAmount_NoVid;
	}

	public void DailyAmountSet_YesVid () {
		intDailyRewardAmount_Chosen = intDailyRewardAmount_YesVid;
	}
		
	public void RewardMachine_CallFromDaily () {
		Debug.LogError ("REWARD = " + intDailyRewardAmount_Chosen);

		// Comment this if you want to get multiple daily rewards
		afterLoadDailyHolder.SetDateAfterVid ();

		// Uncomment this if you want to get multiple daily rewards
//		afterLoadDailyHolder.AfterDaily_TimeMachine ();

		rewardMachineScriptHolder.GetReward_DailyRewardFull (intDailyRewardAmount_Chosen);
		rewardMachineScriptHolder.gameObject.SetActive (true);
		rewardMachineScriptHolder.CallFirstChest ();

		// This is to make sure items are not updated after reward machine (Because Items menu is OFF)
		rewardMachineScriptHolder.isDontUpdateItems = true;

		// Push notif for the moment reward anim plays
		PostOnesignalNotif ();
	}

	private void PostOnesignalNotif()
	{
		string extraMessage = "Waiting to get a OneSignal userId.";
		OneSignal.IdsAvailable((userId, pushToken) => {
			if (pushToken != null)
			{
				var notification = new Dictionary<string, object>();
				string[] notificationString = new string[3];
				notificationString [0] = "کمیسر بسته روزانتون رو فرستاده";
				notificationString [1] = "اقدسی جایزه روزانتون رو با عشق آماده کرد";
				notificationString[2] = "جایزه روزانه شما آماده است";

				notification["contents"] = new Dictionary<string, string>() { { "en", notificationString[Random.Range(0 ,3)]} };
				notification["include_player_ids"] = new List<string>() { userId };
				var remaining = (System.DateTime.Now.AddDays(1).Date - System.DateTime.Now).TotalMinutes;

				notification["send_after"] = System.DateTime.Now.ToUniversalTime().AddMinutes(remaining).ToString("U");

				extraMessage = "Posting test notification now.";
				OneSignal.PostNotification(notification, (responseSuccess) => {
					extraMessage = "Notification posted successful! " + OneSignalPush.MiniJSON.Json.Serialize(responseSuccess);
				}, (responseFailure) => {
					extraMessage = "Notification failed to post:\n" + OneSignalPush.MiniJSON.Json.Serialize(responseFailure);
				});
			}
			else
				extraMessage = "ERROR: Device is not registered.";
		});

		Debug.LogError ("OneSignal Push Notif for daily reward. extraMessage = " + extraMessage);
	}
}
