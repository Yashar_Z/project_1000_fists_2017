﻿using UnityEngine;
using System.Collections;

public class Popup_DeathContine_Script : MonoBehaviour {
	public Popup_Button_Script[] popupButtonScriptArr;
	public Popup_Button_Script[] popupButtonGameOverScriptArr;

	public Canvas canvasDeathContHolder;
	public UnityEngine.UI.GraphicRaycaster graphRayCastHolder;

	public ParticleSystem[] particleBandageSmokeArr;

	public Animation animAghdasiHolder;
	public Animation animButtonPlayIdleHolder;

	void Awake () {
		DeathContinue_CanvasDisable ();
//		StartCoroutine (DeathContin_CanvasController (false));
	}

	void OnEnable () {
		DeathContinue_CanvasEnable ();
//		StartCoroutine (DeathContin_CanvasController (true));
	}

	void OnDisable () {
		// Instantly disable on false
		DeathContinue_CanvasDisable ();

//		Debug.LogError ("Removed Death Canvas");
	}

	public void AnimEvent_CanvasDisable () {
		DeathContinue_CanvasDisable ();
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	void ButtonAnim_GameOver_Enters () {
		for (int i = 0; i < popupButtonGameOverScriptArr.Length; i++) {
			StartCoroutine( ButtonGameOver_Anim_Enter (i));
		}
	}

	public void ButtonIdleAnim_Play () {
		animButtonPlayIdleHolder.Play ();
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	IEnumerator ButtonGameOver_Anim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.2F);
		popupButtonGameOverScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void DeathCanvas_Disable () {
		// Used at the end of return to game with continue anim / close death-continue paper (anim event)
		DeathContinue_CanvasDisable ();
//		StartCoroutine (DeathContin_CanvasController (false));
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void AghdasiAnim_Enter () {
		animAghdasiHolder.Play ("End Popup - Death Aghdasi Enter Anim 1 (Legacy)");
	}

	public void AghdasiAnim_Leave () {
		animAghdasiHolder.Play ("End Popup - Death Aghdasi Leave Anim 1 (Legacy)");
	}

	public void AghdasiAnim_EndlessEnd () {
		animAghdasiHolder.Play ("End Popup - Death Aghdasi EndlessEnd Anim 1 (Legacy)");
	}

	public void GameOver_BandagePoof () {
		particleBandageSmokeArr [0].Play ();
		particleBandageSmokeArr [1].Play ();
	}

	public void ButtonsEnter () {
		Invoke ("ButtonAnim_StartEnters", 0.25F);
	}

	public void ButtonsGameOverEnter () {
        // SOUND EFFECTS - Game Over Pop-up (EMPTY)

		Invoke ("ButtonAnim_GameOver_Enters", 0.25F);
	}

	public IEnumerator DeathContin_CanvasController (bool isActive) {
		canvasDeathContHolder.enabled = isActive;
		yield return null;
	}

	public void DeathContin_CanvasControllerINSTANT (bool isActive) {
		if (isActive) {
			DeathContinue_CanvasEnable ();
		} else {
			DeathContinue_CanvasDisable ();
		}
	}

	public void DeathContinue_CanvasEnable () {
		canvasDeathContHolder.enabled = true;
		graphRayCastHolder.enabled = true;

		MoveIn_DeathContinueParent ();
	}

	public void DeathContinue_CanvasDisable () {
		canvasDeathContHolder.enabled = false;
		graphRayCastHolder.enabled = false;

		MoveOut_DeathContinueParent ();
	}


	public void MoveIn_DeathContinueParent () {
		transform.parent.parent.localPosition = Vector2.zero;
	}

	public void MoveOut_DeathContinueParent () {
		transform.parent.parent.localPosition = new Vector2 (2700, 0);
	}
}
