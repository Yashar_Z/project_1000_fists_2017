﻿using UnityEngine;
using System.Collections;

public class Popup_End_Script : MonoBehaviour {
	public Popup_Button_Script[] popupButtonScriptArr;
	public Animation animAghdasiHolder;
	public Animation animButtonPlayIdleHolder;

	public GameObject objToolTip_StarUnlockedHolder;
	public GameObject objToolTip_StarUntouchableHolder;
	public GameObject objToolTip_StarUnmissableHolder;

	public End_Level_Script endLevelScriptHolder;

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
		Invoke ("ButtonIdleAnim_Play", 0.8F);
	}

	public void ButtonIdleAnim_Play () {
		animButtonPlayIdleHolder.Play ();
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.2F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void AghdasiAnim_Enter () {
		animAghdasiHolder.Play ("End Popup - Vic Aghdasi Enter Anim 1 (Legacy)");
	}

	public void AghdasiAnim_Leave () {
		animAghdasiHolder.Play ("End Popup - Vic Aghdasi Leave Anim 1 (Legacy)");
	}

	public void SetAndShowStars () {
		endLevelScriptHolder.SetupEnd_Stars ();
	}

	public void ButtonsEnter () {
		Invoke ("ButtonAnim_StartEnters", 0.25F);
	}

	public void End_AutoToolTips () {
//		Debug.LogError ("BEFORE world = " + endLevelScriptHolder.worldNumber_End + " and level " + endLevelScriptHolder.levelNumber_End);

		if (PlayerData_InGame.Instance != null) {
			if (endLevelScriptHolder.worldNumber_End < 2 && endLevelScriptHolder.levelNumber_End < 4) {
//			Debug.LogError ("After End check");

				StartCoroutine (Tooltip_AutoEnable (objToolTip_StarUnlockedHolder, 0.25F));
				StartCoroutine (Tooltip_AutoEnable (objToolTip_StarUntouchableHolder, 0.5F));
				StartCoroutine (Tooltip_AutoEnable (objToolTip_StarUnmissableHolder, 0.75F));

				Invoke ("End_AutoToolTips", 3);
			}
		}
	}

	public IEnumerator Tooltip_AutoEnable (GameObject obj, float delay) {
		yield return new WaitForSeconds (delay);
		obj.SetActive (true);
	}
}
