﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup_EndlessEnd_Script : MonoBehaviour {

	public Canvas canvasEndlessEndHolder;
	public Canvas canvasTheEndHolder;

	public Popup_Button_Script[] popUpButtonScripArr;
//	public Endless_BarController endlessBarControllerScript;
	public Endless_PotionsController endlessPotionsControllerScript;
	public Reward_Machine_Script rewardMachineScriptHolder;

	public GameObject objRecordBreakNowHolder;
	public GameObject objAghdasiEndlessHolder;
	public GameObject objTheEndHolder;

	public int intScore_Curr;
	public int intScore_Target;
	public int intScore_TargetBeforeBuffer;
	public int intScore_Record;

	public int intScore_Blocks;

	// These need to be the same as endless_all_script
	public int intEndlessLevel;
	public int intXP_Curr;
	public int intXP_Req_ForWholeLevel;
	public int intXP_Req_ForPartialLevel;
	public float intXP_RatioNormalized_Start;
	public float intXP_RatioNormalized_Target;

	public Text text_Score_Curr;
	public Text text_Score_Record;

	public Text text_EndStats_MaxMultiplier;
	public Text text_EndStats_MustacheGained;
	public Text text_EndStats_PercentCorrect;
	public Text text_EndStats_EnemiesKilled;

	public Button buttonEndlessRestart;
	public Button buttonEndlessToMenu;

	public Animation animEndlessEndPopupHolder;
	public AnimationClip[] animClipEndlessEndPopUpArr;

	public ParticleSystem particleScoreCountComplete;
	public ParticleSystem particleScoreCountIncreasing_Bar;
	public ParticleSystem particleScoreCountIncreasing_Number;

	public ParticleSystem particleScoreBarIncreasingSpark;
	public ParticleSystem particleScoreBarLevelUpEndCircle;

	public ParticleSystem particleTheEndSparks;

	private bool wasRecordBroken;

	void Start () {
		intScore_Target = PlayerData_InGame.Instance.p1_Score_Curr;
		intScore_Record = PlayerData_InGame.Instance.p1_Score_Record;

		popUpButtonScripArr [0].gameObject.SetActive (false);
		popUpButtonScripArr [1].gameObject.SetActive (false);

		// Resize Bar to zero at the start
		Endless_BarController.Instance.EndlessBar_ScaleZero ();

		EndlessEnd_PlayAnim ();

		// End level / play stats
		Setup_Stats_EndlessEnd ();

		// Initial numbers for endless end scores
		Setup_Score_EndlessEnd ();

		// Initialize display numbers
		Setup_AssignFirstTimeScore ();

		// Level display number
		Setup_LevelDisplay ();
	}

	public void Endless_TheEnd () {
		objTheEndHolder.SetActive (true);
		canvasTheEndHolder.enabled = true;
		canvasTheEndHolder.transform.localPosition = Vector2.zero;

		TheEnd_RecordCheck ();
	}

	void TheEnd_RecordCheck () {

		if (PlayerData_InGame.Instance.p1_Score_Curr > PlayerData_InGame.Instance.p1_Score_Record) {
			particleTheEndSparks.Play ();

			// SOUNDD EFFECTS - Music for new record in endless victory (done endless)
			if (SoundManager.Instance != null) {
				MusicManager.Instance.StopMusic();	
				SoundManager.Instance.EndlessEndWithRecord.Play ();	
			}
		} 

		// Record was not broken
		else {

			// SOUNDD EFFECTS - Music for no new record (done endless)
			if (SoundManager.Instance != null) {
				MusicManager.Instance.StopMusic();	
				SoundManager.Instance.EndlessEnd.Play ();	
			}
		}
	}

	public void MoveIn_EndlessEnd () {
        // SOUNDD EFFECTS - Music for win (done endless)
        if (PlayerData_InGame.Instance.p1_Score_Curr > PlayerData_InGame.Instance.p1_Score_Record)
        {
            if (MusicManager.Instance != null)
            {
                MusicManager.Instance.PlayMusic_EndlessMenuRecord();
            }
        }
        else
        {
            if (MusicManager.Instance != null)
            {
                MusicManager.Instance.PlayMusic_EndlessMenu();
            }
        }

		// For late play of win sound
//		AfterTheEnd_RecordCheck ();

		gameObject.SetActive (true);
		StartCoroutine (Canvas_EnableDisableRoutine (true));

		// Bring in the Aghdasi
		objAghdasiEndlessHolder.SetActive (true);

		// Delayed setting of position
		StartCoroutine (MoveIn_EndlessEnd_DelayedPart ());
	}

	public void Pressed_RewardsOkForEggs () {
		// Only call fixed reward / chest in case there IS at least one egg related reward
		if (PlayerData_InGame.Instance.isEndlessEggsRewarding) {
			EndlessReward_Fixed ();
		} 

		// To allow continuation of bar completion (No need!)
//		else {
//			PlayerData_InGame.Instance.isEndlessEggsRewarding = false;
//		}
	}
		
	public void EndlessReward_Fixed () {
		LevelManifestRewardData rewardForEndlessLevelUp = Endless_BarController.Instance.rewardEndless_LevelManifest;
//		LevelManifestRewardData rewardForEndlessLevelUp = new LevelManifestRewardData ();

		rewardMachineScriptHolder.GetReward_InGame (rewardForEndlessLevelUp);
		rewardMachineScriptHolder.gameObject.SetActive (true);
		rewardMachineScriptHolder.CallFirstChest ();
	}

	public IEnumerator MoveIn_EndlessEnd_DelayedPart () {
		yield return new WaitForSeconds (0.1F);
		transform.localPosition = Vector2.zero;
	}

	public void MoveOut_EndlessEnd () {
		transform.localPosition = new Vector2 (3600, 0);
		Canvas_Disable ();
		gameObject.SetActive (false);
	}

	public void Setup_Stats_EndlessEnd () {
		text_EndStats_MaxMultiplier.text = PlayerData_InGame.Instance.hudScriptHolder.endlessScoreController.intCombo_MultiMaxRecord.ToString ();

		PlayerData_InGame.Instance.endLevelScriptHolder.SetupEnd_MustacheMultiCalculate ();
		text_EndStats_MustacheGained.text = PlayerData_InGame.Instance.stats_MustacheGained.ToString ();

		PlayerData_InGame.Instance.endLevelScriptHolder.SetupEnd_UnmissableCalculate ();
		text_EndStats_PercentCorrect.text = PlayerData_InGame.Instance.endLevelScriptHolder.intPercentCorretHit_Endless.ToString ();

		text_EndStats_EnemiesKilled.text = PlayerData_InGame.Instance.stats_EnemiesKilled.ToString();;
	}

	public void Setup_Score_EndlessEnd () {
		// Getting values from player data main so if play quits, xp is still added
		intEndlessLevel = PlayerData_InGame.Instance.p1_EndlessLevel;
		intXP_Curr = PlayerData_InGame.Instance.p1_XP_Curr;
		intScore_Record = PlayerData_InGame.Instance.p1_Score_Record;

		Setup_XP_Req ();

		Endless_BarController.Instance.endlessBarUpdater.Setup_BarFixedPosit (intXP_RatioNormalized_Start);
		Endless_BarController.Instance.endlessBarUpdater.Setup_XPforDisplay (intXP_Curr, intXP_Req_ForWholeLevel);

		// Now use the function in the start
//		intScore_Curr = 0;
//		text_Score_Curr.text = intScore_Curr.ToString ();
//		text_Score_Record.text = intScore_Record.ToString ();
	}

	public void Setup_AssignFirstTimeScore () {
		intScore_Curr = 0;

  		text_Score_Curr.text = intScore_Curr.ToString ();
    	text_Score_Record.text = intScore_Record.ToString ();
	}

	public void Setup_LevelDisplay () {
		Endless_BarController.Instance.endlessBarUpdater.Level_DisplaySet ();
	}

	public void Setup_XP_Req () {
		switch (intEndlessLevel) {
		case 1:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl2;
			break;
		case 2:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl3;
			break;
		case 3:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl4;
			break;
		case 4:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl5;
			break;
		case 5:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl6;
			break;
		case 6:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl7;
			break;
		case 7:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl8;
			break;
		case 8:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl9;
			break;
		case 9:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl10;
			break;
		case 10:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl11;
			break;
		case 11:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl12;
			break;
		case 12:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl13;
			break;
		case 13:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl14;
			break;
		case 14:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_Lvl15;
			break;
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_LvlMore;
			break;
		default:
			Debug.LogError ("Default Error");
			intXP_Req_ForWholeLevel = Balance_Constants_Script.Endless_XP_Req_LvlMore;
			break;
		}

		// XP Required from current XP amount to first new level
		intXP_Req_ForPartialLevel = intXP_Req_ForWholeLevel - intXP_Curr;

		// Start: Normalized position for xp / bar
		intXP_RatioNormalized_Start = (float) intXP_Curr / intXP_Req_ForWholeLevel;
	}

	public void Compare_ScoreToXP () {
//		Debug.LogError ("intScore_Target = " + intScore_Target + "    and intXP_Req_ForPartialLevel = " + intXP_Req_ForPartialLevel
//			+ "     intXP_Curr = " + intXP_Curr);
		particleScoreBarIncreasingSpark.Play ();

//		Debug.LogError ("intScore_Target = " + intScore_Target);
//		Debug.LogError ("intScore_Target + intXP_Curr = " + (intScore_Target + intXP_Curr).ToString ());

		// This is required in order to add current score TO current XP loaded from player data main or ingame
		intScore_Target += intXP_Curr;

		// We have to reach a full level and then continue
		if (intScore_Target > intXP_Req_ForWholeLevel) {
			
//			if (intScore_Target > intXP_Req_ForPartialLevel) {
			Endless_BarController.Instance.endlessBarUpdater.Setup_StartAll (1);
		}

		// We are only continuing to middle of bar (End)
		else {
			// Target: Normalized position for xp / bar
			intXP_RatioNormalized_Target = (float) intScore_Target / intXP_Req_ForWholeLevel;

			Endless_BarController.Instance.endlessBarUpdater.Setup_StartAll (intXP_RatioNormalized_Target);
		}
	}

	public void Endless_LevelUp_Start () {
		// Update the score to remainder for next level
		intScore_Target -= intXP_Req_ForWholeLevel;

		// Previous code
//		intScore_Target -= intXP_Req_ForPartialLevel;

		// Level up logic
		PlayerData_InGame.Instance.p1_EndlessLevel++;
		PlayerData_InGame.Instance.p1_XP_Curr = 0;

		intEndlessLevel = PlayerData_InGame.Instance.p1_EndlessLevel;

		// Stop level up effect for this
		particleScoreCountIncreasing_Bar.Stop ();
		particleScoreBarIncreasingSpark.Stop ();

		// Go to rewards for levelup
		StartCoroutine (Endless_LevelUp_OpenRewards ());
	}

	public IEnumerator Endless_LevelUp_OpenRewards () { 
		// Can go to prepare for level up anim
		Endless_BarController.Instance.EndlessBar_PrepareLevelUp ();

		yield return null;

		Setup_Score_EndlessEnd ();

		yield return new WaitForSeconds (2);

		// Update display for normal bar prefab
		Setup_LevelDisplay ();

//		yield return new WaitForSeconds (0.5F);

		// Should be commented for final build (Moved to popup-endlessrewards)
//		Endless_LevelUp_End ();
	}

	public void Endless_RewardsEndChecker () {
		StartCoroutine (Endless_RewardsEndCheckerROUTINE ());
	}

	public IEnumerator Endless_RewardsEndCheckerROUTINE () {
		yield return new WaitForSeconds (0.3F);

		// Waiting for reward machine to falsify this
		while (PlayerData_InGame.Instance.isEndlessEggsRewarding) {
			yield return new WaitForSeconds (0.1F);
		}

		Endless_LevelUp_End ();
	}

	public void Endless_LevelUp_End () {
		// Bar increase after level-up stuff reaches final amount (Score count has ended)
		StartCoroutine (Setup_BarIncrease ());
	}

	public void Endless_LevelUp_EndOfAll () {
		// Stop level up effect for this
		particleScoreCountIncreasing_Bar.Stop ();
		particleScoreBarIncreasingSpark.Stop ();

		// Particle on the bar head
		particleScoreBarLevelUpEndCircle.Play ();

		// Pass values to play main before saving
		PlayerData_InGame.Instance.PassValues_Endless_InGameToMain ();

		// Pass XP Value from endless end to playerdata in game so it can be passed to player data main (A few lines down the line)
		PlayerData_InGame.Instance.p1_XP_Curr = intXP_Curr;

		// Can now tap on question
		Endless_BarController.Instance.EndlessBar_ShowQuestion ();

//		Debug.LogError ("intXP_Curr = " + intXP_Curr);
//		Debug.LogError ("PlayerData_InGame.Instance.p1_XP_Curr = " + PlayerData_InGame.Instance.p1_XP_Curr);

		if (PlayerData_Main.Instance != null) {
			// Save data after end of endless count
			SaveLoad.Save ();

			// Send score to google
			EndScore_SendGoogleRecord ();
		}

		// Enable endless end buttons at the end of ALL
		buttonEndlessRestart.interactable = true;
		buttonEndlessToMenu.interactable = true;

		// Enter buttons
		StartCoroutine (Endless_ButtonsEnter ());
	}

    public void EndScore_SendGoogleRecord()
    {
        // intScore_Target is the final score of player
        PlayGamesScript.Instance.AddScoreToLeaderboard(GPGSIds.leaderboard, intScore_Curr);

        Fabric.Answers.Answers.LogCustom(
            "Endless Score",
            customAttributes: new System.Collections.Generic.Dictionary<string, object>()
            {
                { "Player Damage", PlayerData_Main.Instance.player_Damage },
                { "Player Score", intScore_Curr }
            });
    }

	public IEnumerator Score_CountRoutine () {
//		var sw = new System.Diagnostics.Stopwatch ();
//		sw.Start ();

		// SCORE Counting particle
		particleScoreCountIncreasing_Number.Play ();

		// SOUNDD EFFECTS - Score Increase START (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessScoreIncreaseLoop.Play ();	
		}

		while (intScore_Curr < intScore_Target) {
			
			if ((intScore_Curr + 100000) <= intScore_Target) {
				intScore_Curr = intScore_Curr + 100000;
//				Debug.LogError ("Increase by 100000");
				yield return new WaitForSeconds (Time.deltaTime / 5);
				} else {
				if ((intScore_Curr + 10000) <= intScore_Target) {
					intScore_Curr = intScore_Curr + 4000;
//					Debug.LogError ("Increase by 4000");
					yield return new WaitForSeconds (Time.deltaTime / 2);
					} else {
					if ((intScore_Curr + 1000) <= intScore_Target) {
						intScore_Curr = intScore_Curr + 400;
//						Debug.LogError ("Increase by 400");
						yield return new WaitForSeconds (Time.deltaTime);
						} else {
							if ((intScore_Curr + 100) <= intScore_Target) {
								intScore_Curr = intScore_Curr + 40;
//								Debug.LogError ("Increase by 40");
								yield return new WaitForSeconds (Time.deltaTime);
							} else {
								if ((intScore_Curr + 36) <= intScore_Target) {
									intScore_Curr = intScore_Curr + 8;
//									Debug.LogError ("Increase by 6");
									yield return new WaitForSeconds (Time.deltaTime * 2);
								} else {
									if ((intScore_Curr + 13) <= intScore_Target) {
										intScore_Curr = intScore_Curr + 3;
//										Debug.LogError ("Increase by 3");
										yield return new WaitForSeconds (Time.deltaTime * 4);
									} else {
										intScore_Curr = intScore_Curr + 1;
//										Debug.LogError ("Increase by 3");
										yield return new WaitForSeconds (Time.deltaTime * 5);
									}
								}
							}
						}
					}
				}

			text_Score_Curr.text = intScore_Curr.ToString ();
			StartCoroutine (Score_RecordIncrease ());

			yield return null;
		}

		// SOUNDD EFFECTS - Score Increase END (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.EndlessScoreIncreaseLoop.Stop ();	
			SoundManager.Instance.EndlessPayaneShomareshScore.Play ();
		}

		// SCORE Counting particle stop
		particleScoreCountIncreasing_Number.Stop ();

//		sw.Stop ();

		text_Score_Curr.text = intScore_Target.ToString ();
		StartCoroutine (Score_RecordIncrease ());

//		Debug.LogError ("Finished! at " + sw.ElapsedMilliseconds);

		// Particle to show the score count was finished (Moved from inside of setup_barincrease)
		particleScoreCountComplete.Play ();

		// Bar increase after score reaches final amount (Score count has ended)
		StartCoroutine (Setup_BarIncrease ());
	}

	public IEnumerator Score_RecordMoment () {
		yield return null;
		objRecordBreakNowHolder.SetActive (true);

		yield return new WaitForSeconds (0.3F);
		EndlessEnd_ShakeAnim ();

		// SOUNDD EFFECTS - Score Record Break Moment (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.Mosht1.Play ();	
		}
	}

	public IEnumerator Score_RecordIncrease () {
		yield return null;
		if (intScore_Curr > intScore_Record) {
			if (!wasRecordBroken) {
				wasRecordBroken = true;
				StartCoroutine (Score_RecordMoment ());

				// Update the value of record broken
				PlayerData_InGame.Instance.p1_Score_Record = intScore_Target;
			} else {
				text_Score_Record.text = intScore_Curr.ToString ();
			}
		}

	}

	public IEnumerator Setup_BarIncrease () {

		yield return new WaitForSeconds (0.55F);
		particleScoreCountIncreasing_Bar.Play ();

		// Check to see if score is more than req xp for level up
		Compare_ScoreToXP ();
	}

	public void AnimEvent_ScoreCountStart () {
		StartCoroutine (Score_CountRoutine ());

		// Stop cam idle anim
		PlayerData_InGame.Instance.hudScriptHolder.camScriptHolder.Cam_IdleAnim_Stop ();
	}

	public void AnimEvent_BarEnter () {
		Endless_BarController.Instance.EndlessBar_EnterAnim ();
	}

	public void AnimEvent_MoveOut () {
		MoveOut_EndlessEnd ();
	}

	public IEnumerator Endless_ButtonsEnter () {
		yield return new WaitForSeconds (0.5F);
		popUpButtonScripArr [0].gameObject.SetActive (true);
		popUpButtonScripArr [0].Button_PlayAnim (0);
		yield return new WaitForSeconds (0.2F);
		popUpButtonScripArr [1].gameObject.SetActive (true);
		popUpButtonScripArr [1].Button_PlayAnim (0);
	}

	public void EndlessEnd_PlayAnim () {
		animEndlessEndPopupHolder.clip = animClipEndlessEndPopUpArr [0];
		animEndlessEndPopupHolder.Play ();
	}

	public void EndlessEnd_ShakeAnim () {
		animEndlessEndPopupHolder.clip = animClipEndlessEndPopUpArr [1];
		animEndlessEndPopupHolder.Play ();
	}

	public IEnumerator Canvas_EnableDisableRoutine (bool whichBool) {
		yield return null;
		if (whichBool) {
			Canvas_Enable ();
		} else {
			Canvas_Disable ();
		}
	}

	public void Canvas_Enable () {
		canvasEndlessEndHolder.enabled = true;
	}

	public void Canvas_Disable () {
		canvasEndlessEndHolder.enabled = false;
	}

}
