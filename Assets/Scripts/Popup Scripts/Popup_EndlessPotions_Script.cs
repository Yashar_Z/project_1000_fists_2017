﻿using UnityEngine;
using System.Collections;

public class Popup_EndlessPotions_Script : MonoBehaviour {

	public Canvas canvasPotionsHolder;
	public UnityEngine.UI.ScrollRect scrollPotionsHolder;

	public Animation animEndlessPopupHolder;
	public AnimationClip[] animClipEndlesSPopUpArr;

//	void Awake () {
//		MoveOut_Potions ();
//	}

	public void MoveIn_Potions () {
		gameObject.SetActive (true);
		transform.localPosition = Vector2.zero;
		StartCoroutine (Canvas_EnableDisableRoutine (true));
	}

	public void MoveOut_Potions () {
		transform.localPosition = new Vector2 (3600, 0);
		Canvas_Disable ();
		gameObject.SetActive (false);
	}

	public void AnimEvent_MoveOut () {
		MoveOut_Potions ();
	}

	public void AnimEvent_ScrollEnable () {
		scrollPotionsHolder.enabled = true;
	}

	public void AnimEvent_ScrollDisable () {
		scrollPotionsHolder.enabled = false;
	}

	public void AnimEvent_ScrollReset () {
		scrollPotionsHolder.horizontalNormalizedPosition = 0;
	}

	public IEnumerator Canvas_EnableDisableRoutine (bool whichBool) {
		yield return null;
		if (whichBool) {
			Canvas_Enable ();
		} else {
			Canvas_Disable ();
		}
	}

	public void Endless_PlayAnim () {
		animEndlessPopupHolder.clip = animClipEndlesSPopUpArr [1];
		animEndlessPopupHolder.Play ();
	}

	public void Canvas_Enable () {
		canvasPotionsHolder.enabled = true;
	}

	public void Canvas_Disable () {
		canvasPotionsHolder.enabled = false;
	}

}
