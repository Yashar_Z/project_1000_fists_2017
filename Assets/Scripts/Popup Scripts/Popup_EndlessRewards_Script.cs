﻿using UnityEngine;
using System.Collections;

public class Popup_EndlessRewards_Script : MonoBehaviour {

	public Animation animPopupEndlessRewards;
	public AnimationClip[] animClipEndlessRewardsArr;

	public Popup_Button_Script[] popupButtonScriptArr;

	public Roadmap_ItemScript[] roadItemScriptArr;

	public Canvas canvasHolder;

	public UnityEngine.UI.Text text_EndlessLevel;
	public UnityEngine.UI.Image[] imageRewardCardsArr;

	public GameObject[] objRewardItemsArr;

	public int intRewardLevel;
	public Endless_PotionsClass.PotionRewardType potionRewardType1;
	public Endless_PotionsClass.PotionRewardType potionRewardType2;
	public Endless_PotionsClass.PotionRewardType potionRewardType3;
	public Endless_PotionsClass.PotionRewardType potionRewardType4;

	public ParticleSystem particleLevel_EnergyGain;
	public ParticleSystem particleLevel_UpSparks;
	public ParticleSystem particleLevel_UpScale;
	public ParticleSystem particleLevel_UpShine;

	public Transform transEndlessRewardItemsParent;

	private LevelManifestRewardData endlessLevelManifestHolder;

	private int intEggCount_Mustache;
	private int intEggCount_Koochool;
	private int intEggCount_Aghdasi;
	private int intEggCount_Mosthan;

	private int intEgg_MustacheAmount;

//	void OnEnable () {
//		Invoke ("ButtonAnim_StartEnters", 0.4F);
		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
//	}

//	void ButtonAnim_StartEnters () {
//		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
//			StartCoroutine( ButtonAnim_Enter (i));
//		}
//	}

	public void Animate_EndlessRewards_Enter () {
		animPopupEndlessRewards.clip = animClipEndlessRewardsArr [0];
		animPopupEndlessRewards.Play ();
	}

	public void Animate_EndlessRewards_Leave () {
		animPopupEndlessRewards.clip = animClipEndlessRewardsArr [1];
		animPopupEndlessRewards.Play ();
	}

	public IEnumerator Setup_RewardRoadmapItems () {
		intRewardLevel = PlayerData_InGame.Instance.p1_EndlessLevel ;

		// Because the level was already increased
		intRewardLevel--;

		// Moved to later anim event
//		particleLevel_EnergyGain.Play ();

		// Display player level before levelup
		text_EndlessLevel.text = (intRewardLevel).ToString ();

		// After displaying the level, the intRewardLevel should check for max level (In that case, max level's reward is repeated)
		if ((intRewardLevel + 1) > PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr.Length) {
			// Use this in case you want the last reward to be repeated
//			intRewardLevel = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr.Length - 1;

			// Give empty rewards
			potionRewardType1 = Endless_PotionsClass.PotionRewardType.Empty;
			potionRewardType2 = Endless_PotionsClass.PotionRewardType.Empty;
			potionRewardType3 = Endless_PotionsClass.PotionRewardType.Empty;
			potionRewardType4 = Endless_PotionsClass.PotionRewardType.Empty;
		} 

		// Meaning that player has NOT reached max level
		else {
			potionRewardType1 = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr [intRewardLevel].Potion_Reward1;
			potionRewardType2 = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr [intRewardLevel].Potion_Reward2;
			potionRewardType3 = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr [intRewardLevel].Potion_Reward3;
			potionRewardType4 = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.endlessPotionRewardsArr [intRewardLevel].Potion_Reward4;
		}

		roadItemScriptArr[0].Setup_RoadmapItem (potionRewardType1);
		roadItemScriptArr[1].Setup_RoadmapItem (potionRewardType2);

		yield return null;

		roadItemScriptArr[2].Setup_RoadmapItem (potionRewardType3);
		roadItemScriptArr[3].Setup_RoadmapItem (potionRewardType4);

		yield return null;

		Colorize_RoadmapRewardBack (potionRewardType1, 0);
		Colorize_RoadmapRewardBack (potionRewardType2, 1);

		yield return null;

		Colorize_RoadmapRewardBack (potionRewardType3, 2);
		Colorize_RoadmapRewardBack (potionRewardType4, 3);

		Setup_EndlessEggRewards ();
	}

	public void GetAmount_EggMustacheReward () {
		switch (intRewardLevel + 1) {
		case 1:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl1;
			break;
		case 2:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl2;
			break;
		case 3:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl3;
			break;
		case 4:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl4;
			break;
		case 5:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl5;
			break;
		case 6:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl6;
			break;
		case 7:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl7;
			break;
		case 8:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl8;
			break;
		case 9:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl9;
			break;
		case 10:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl10;
			break;
		case 11:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl11;
			break;
		case 12:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl12;
			break;
		case 13:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl13;
			break;
		case 14:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl14;
			break;
		case 15:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_Lvl15;
			break;
		case 16:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_LvlMore;
			break;
		case 17:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_LvlMore;
			break;

		default:
			intEgg_MustacheAmount = Balance_Constants_Script.Endless_EggMustacheReward_LvlMore;
			Debug.LogError ("Default Error");
			break;
		}
		Debug.LogError ("intEgg_MustacheAmount = " + intEgg_MustacheAmount); 

		if (intEgg_MustacheAmount == 0) {
			intEgg_MustacheAmount = 200;
		}
	}

	public void RepositionRewardItems () {
		int intRewardCardsCount = 0;
		for (int i = 0; i < transEndlessRewardItemsParent.childCount; i++) {
			if (transEndlessRewardItemsParent.GetChild (i).gameObject.activeInHierarchy) {
				intRewardCardsCount++;
			}
		}
		switch (intRewardCardsCount) {
		case 1:
			transEndlessRewardItemsParent.localPosition = new Vector2 (198, -20);
			break;
		case 2:
			transEndlessRewardItemsParent.localPosition = new Vector2 (132, -20);
			break;
		case 3:
			transEndlessRewardItemsParent.localPosition = new Vector2 (64, -20);
			break;
		case 4:
			transEndlessRewardItemsParent.localPosition = new Vector2 (0, -20);
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}
	}

	public void Setup_EndlessEggRewards () {
		Setup_EndlessEggRewards_Each (potionRewardType1);
		Setup_EndlessEggRewards_Each (potionRewardType2);
		Setup_EndlessEggRewards_Each (potionRewardType3);
		Setup_EndlessEggRewards_Each (potionRewardType4);

		int intTotalEggRewardCount = intEggCount_Koochool + intEggCount_Aghdasi + intEggCount_Mosthan + intEggCount_Mustache;

		// Make sure there is at least ONE egg related reward to call reward machine
		if (intTotalEggRewardCount > 0) {

			// To put the shown reward cards at the center instead of side
			RepositionRewardItems ();
			
			Endless_BarController.Instance.rewardEndless_LevelManifest = new LevelManifestRewardData ();
			endlessLevelManifestHolder = Endless_BarController.Instance.rewardEndless_LevelManifest;

			// Get egg mustache values from balance sheet
			GetAmount_EggMustacheReward ();

			endlessLevelManifestHolder.Mustache = new int[intEggCount_Mustache];
			for (int i = 0; i < endlessLevelManifestHolder.Mustache.Length; i++) {
				endlessLevelManifestHolder.Mustache [i] = intEgg_MustacheAmount;

				// This was for old method with fixed mustache reward
//				endlessLevelManifestHolder.Mustache [i] = Balance_Constants_Script.Endless_EggMustacheReward;
			}

			endlessLevelManifestHolder.Items = new int[0];

			endlessLevelManifestHolder.RandomChests = new Chest.ChestType[intTotalEggRewardCount];
			int rewardCounter = 0;

			for (int i = 0; i < intEggCount_Koochool; i++) {
				endlessLevelManifestHolder.RandomChests [rewardCounter] = Chest.ChestType.Silver;
				rewardCounter++;
			}

			for (int i = 0; i < intEggCount_Aghdasi; i++) {
				endlessLevelManifestHolder.RandomChests [rewardCounter] = Chest.ChestType.Gold;
				rewardCounter++;
			}

			for (int i = 0; i < intEggCount_Mosthan; i++) {
				endlessLevelManifestHolder.RandomChests [rewardCounter] = Chest.ChestType.Unique;
				rewardCounter++;
			}

			endlessLevelManifestHolder.FixedChests = new FixedChest[0];

			// This means the rewards popup will CALL reward machine
			PlayerData_InGame.Instance.isEndlessEggsRewarding = true;

			Endless_BarController.Instance.rewardEndless_LevelManifest = endlessLevelManifestHolder;
		}


		else {
			// This means the rewards popup will NOT call reward machine
			PlayerData_InGame.Instance.isEndlessEggsRewarding = false;
		}
	}

	public void Setup_EndlessEggRewards_Each (Endless_PotionsClass.PotionRewardType rewardType) {
		switch (rewardType) {
		case Endless_PotionsClass.PotionRewardType.Egg_Koochool:
			intEggCount_Koochool++;
			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Aghdasi:
			intEggCount_Aghdasi++;
			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Mosthan:
			intEggCount_Mosthan++;
			break;
		case Endless_PotionsClass.PotionRewardType.Egg_Mustache:
			intEggCount_Mustache++;
			break;
		default:
			break;
		}
	}


	void Colorize_RoadmapRewardBack (Endless_PotionsClass.PotionRewardType rewardPotionType, int whichImage) {
		switch (rewardPotionType) {
		case Endless_PotionsClass.PotionRewardType.Empty:
			objRewardItemsArr[whichImage].SetActive (false);
			break;

		case Endless_PotionsClass.PotionRewardType.Egg_Koochool:
		case Endless_PotionsClass.PotionRewardType.Egg_Aghdasi:
		case Endless_PotionsClass.PotionRewardType.Egg_Mosthan:
		case Endless_PotionsClass.PotionRewardType.Egg_Mustache:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[0];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		case Endless_PotionsClass.PotionRewardType.Potion_Damage_Min:
		case Endless_PotionsClass.PotionRewardType.Potion_Damage_Max:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[1];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		case Endless_PotionsClass.PotionRewardType.Potion_HP_Min:
		case Endless_PotionsClass.PotionRewardType.Potion_HP_Max:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[2];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		case Endless_PotionsClass.PotionRewardType.Potion_Crit_Min:
		case Endless_PotionsClass.PotionRewardType.Potion_Crit_Max:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[3];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		case Endless_PotionsClass.PotionRewardType.Potion_LastStand_Min:
		case Endless_PotionsClass.PotionRewardType.Potion_LastStand_Max:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[4];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		case Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Min:
		case Endless_PotionsClass.PotionRewardType.Potion_HeadStart_Max:
			imageRewardCardsArr[whichImage].color = PlayerData_InGame.Instance.hudScriptHolder.endlessPotionsController.colorPotionBackArr[5];
			objRewardItemsArr[whichImage].SetActive (true);
			break;

		default:
			Debug.LogError ("Default Error");
			break;
		}
	}

	public void MoveIn_EndlessRewards() {
		gameObject.SetActive (true);
		canvasHolder.enabled = true;
		transform.position = Vector2.zero;
	}

	public void AnimEvent_MoveOut_EndlessRewards() {
		// End of level-up rewards (Used to be in popupEndlessEnd)
		PlayerData_InGame.Instance.hudScriptHolder.popUpEndlessEndScript.Endless_RewardsEndChecker ();

		transform.position = new Vector2 (0, 1600);
		canvasHolder.enabled = false;
		gameObject.SetActive (false);
	}

	public void AnimEvent_UpdatePlayerLevel () {
		// Display player AFTER before levelup (The intReward + 1 part is because we reduced on earlier)
		text_EndlessLevel.text = (intRewardLevel + 1).ToString ();

		// Particles for the moment of level up
		particleLevel_UpSparks.Play ();
		particleLevel_UpScale.Play ();
		particleLevel_EnergyGain.Stop ();

		// Show button with certain delay
		StartCoroutine (RewardsOkButtonRountine ());
	}

	public void AnimEvent_EnergyGainParticle () {
		// Moved here from Setup_RewardRoadmapItems
		particleLevel_EnergyGain.Play ();
	}

	public void AnimEvent_ShineParticle () {
		// Moved here from Setup_RewardRoadmapItems
		particleLevel_UpShine.Play ();
	}

	public IEnumerator RewardsOkButtonRountine () {
		yield return new WaitForSeconds (1.5F);
		popupButtonScriptArr [0].Button_PlayAnim (0);
	}

	public void Pressed_RewardsOkButton () {
		// But uses the popupall anim via button (This is UN-USED) (Bar update continue event is in the anim)
		Animate_EndlessRewards_Leave ();
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}
}
