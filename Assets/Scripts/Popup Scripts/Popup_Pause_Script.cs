﻿using UnityEngine;
using System.Collections;

public class Popup_Pause_Script : MonoBehaviour {
//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;
	public Animation animMoshtanHolder;

	public Canvas canvasPausePopUpHolder;

	void Awake () {
		canvasPausePopUpHolder.enabled = false;
		this.gameObject.SetActive (false);
	}

	void OnEnable () {
		Invoke ("ButtonAnim_StartEnters", 0.2F);
		MoshtanAnim_Enter ();

        // SOUNDD EFFECTS - Pause Pressed (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.PressedPause.Play();
            SoundManager.Instance.PopUp_Pause.Play();
        }

		// Enable Canvas Onenable
		StartCoroutine (PausePopUp_CanvasController (true));

		// Move IN
		transform.localPosition = Vector2.zero;
	}

	void OnDisable () {
		// Disable Canvas Ondisable
		canvasPausePopUpHolder.enabled = false;

		// Move OUT
		transform.localPosition = new Vector2 (4000, 0);
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void MoshtanAnim_Enter () {
		animMoshtanHolder.Play ("Game Popup - Pause Moshtan Enter Anim 1 (Legacy)");
	}

	public void MoshtanAnim_Leave () {
		animMoshtanHolder.Play ("Game Popup - Pause Moshtan Leave Anim 1 (Legacy)");
	}

	public void PauseCanvasDisable () {
		StartCoroutine (PausePopUp_CanvasController (false));
	}

	public IEnumerator PausePopUp_CanvasController (bool isActive) {
		canvasPausePopUpHolder.enabled = isActive;
		yield return null;
	}
}
