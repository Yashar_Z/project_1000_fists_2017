﻿using UnityEngine;
using System.Collections;

public class Popup_PowerUpsParent_Script : MonoBehaviour {

	public Animation animPowUpsMenuParent;

	public Canvas canvasPowUpParHolder;
	public UnityEngine.UI.GraphicRaycaster graphRayCasterPowUpHolder;

	public AnimationClip[] animClipPowUpsMenuParArr;

	public bool isPowUpsBatteryUp;

	public void PowerUpsMenu_Enter () {
		animPowUpsMenuParent.clip = animClipPowUpsMenuParArr [0];
		animPowUpsMenuParent.Play ();

		MoveIn_PowUpsMenu ();

		isPowUpsBatteryUp = true;
	}

	public void PowerUpsMenu_Leave () {
		if (isPowUpsBatteryUp) {
			animPowUpsMenuParent.clip = animClipPowUpsMenuParArr [1];
			animPowUpsMenuParent.Play ();
		
			isPowUpsBatteryUp = false;

		// Moved to move out
//		StartCoroutine (CanvasDisable ());
		}
	}

	public void MoveIn_PowUpsMenu () {
		transform.localPosition = Vector2.zero;

		StartCoroutine (CanvasEnable ());
	}

	// In "Menu PowUp Parent - Leave Anim"
	public void AnimEvent_MoveOutPowUpsMenu () {
		transform.localPosition = new Vector2 (3200, 0);

		StartCoroutine (CanvasDisable ());
	}

	IEnumerator CanvasEnable () {
		yield return null;
		canvasPowUpParHolder.enabled = true;
		graphRayCasterPowUpHolder.enabled = true;
	}

	IEnumerator CanvasDisable () {
		yield return null;
		canvasPowUpParHolder.enabled = false;
		graphRayCasterPowUpHolder.enabled = false;
	}
}
