﻿using UnityEngine;
using System.Collections;

public class Popup_QuitBack_Script : MonoBehaviour {

	public Canvas canvasQuitHolder;
	public UnityEngine.UI.GraphicRaycaster graphicRayCasterHolder;

	public Popup_Button_Script[] popupButtonScriptArr;

	void MoveIn_QuitBack () {
		this.transform.localPosition = Vector2.zero;
		canvasQuitHolder.enabled = true;
		graphicRayCasterHolder.enabled = true;
	}

	void MoveOut_QuitBack () {
		this.transform.localPosition = new Vector2 (3200, 0);
		canvasQuitHolder.enabled = false;
		graphicRayCasterHolder.enabled = false;
	}

	void OnEnable () {
		MoveIn_QuitBack ();

		Invoke ("ButtonAnim_StartEnters", 0.4F);

		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
	}

	void OnDisable () {
		MoveOut_QuitBack ();
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void Pressed_QuitNo () {
		StartCoroutine (QuitNo_Delayed ());
	}

	public void Pressed_QuitYes () {
		Application.Quit ();

		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#endif
	}

	public IEnumerator QuitNo_Delayed () {
		yield return new WaitForSeconds (0.4F);
	}
}
