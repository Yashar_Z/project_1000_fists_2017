﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup_Settings_Script : MonoBehaviour {

    public static System.Action OnMusicToggled;
    public static System.Action OnSfxToggled;

    public Menu_Main_Script menuMainScriptHolder;

	public Popup_All_Script popAllScriptHolder;
	public Popup_Button_Script[] popupButtonScriptArr;
	public AnimationClip[] animClipPopUpSettingsArr;
	public AnimationClip[] animClipBackButtonArr;
	public Animation animPopUpSettingsHolder;
	public Animation animBackButtonHolder;
	public Animation animMoveOnlyHolder;
	public Animation animMustacheSigHolder;

	public ParticleSystem particlePageFadeHolder;

	public GameObject musicIconParentObjHolder_ON;
	public GameObject soundIconParentObjHolder_ON;
	public GameObject objGooglePlayObjHolder_ON;

	public Transform transSettingsMoverHolder;

	public Text text_Version;
	public Text text_Page_SettingsName;
	public Button buttonSettingsClickBehindHolder;

	[Header ("Items Canvas")]
	public Canvas canvasSettingsHolder;
	public GraphicRaycaster graphRayCasterHolder;

	public enum SettingsMenuState {
		Middle = 0,
		Left_AboutUs,
		Right_ContactUs
	}

	private SettingsMenuState settingsMenuState_Curr;

	void Awake () {
		AnimEvent_CanvasDisable ();
	}

	void OnEnable () {
        //		Invoke ("ButtonAnim_StartEnters", 0.2F);

        // Default State for volume icons
        //		UpdateSoundMusicIcons ();

        // Default State for Settings State
        //		settingsMenuState_Curr = SettingsMenuState.Middle;

        // Force re-center Settings Mover (Posit and Anim)
        //		SettingsMover_ReCenter_NOW ();

        // Show Back Button Start Animate
        //		BackButton_Animate_Direct (0);
        
		if (PlayerData_Main.Instance != null) {
			text_Version.text = "Version " + Application.version;
		}
	}

	void UpdateSoundMusicIcons () {
		if (PlayerData_Main.Instance != null) {
			musicIconParentObjHolder_ON.SetActive (PlayerData_Main.Instance.player_Music_ON);
			soundIconParentObjHolder_ON.SetActive (PlayerData_Main.Instance.player_Sound_ON);
		}
	}

	void ButtonAnim_StartEnters () {
		this.gameObject.SetActive (true);
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void Settings_MoveIn () {
		popAllScriptHolder.PopUp_AnimPlay (0);

		Invoke ("ButtonAnim_StartEnters", 0.2F);

		// Default State for volume icons
		UpdateSoundMusicIcons ();

		// Default State for Settings State
		settingsMenuState_Curr = SettingsMenuState.Middle;

		// Force re-center Settings Mover (Posit and Anim)
		SettingsMover_ReCenter_NOW ();

		// Show Back Button Start Animate
		BackButton_Animate_Direct (0);

		// Set Name To Main
		PageName_Settings ();

		AnimEvent_CanvasEnable ();

		if (GooglePlayGames.PlayGamesPlatform.Instance.IsAuthenticated ()) {
			objGooglePlayObjHolder_ON.SetActive (true);
		}

		// To determine the off / on status of Google play button (No solely rely on playgame script call back for sign in)
//		if (GooglePlayGames.PlayGamesPlatform.Instance.IsAuthenticated ()) {
//			objGooglePlayObjHolder_ON.SetActive (true);
//		} 
//
//		else {
//			objGooglePlayObjHolder_ON.SetActive (false);
//		}
    }

	public void MoveOut_SettingsAll () {
		transform.localPosition = new Vector2 (3200, 0);

		menuMainScriptHolder.ZoomOut_OnlyMenuActivator ();

		AnimEvent_CanvasDisable ();
	}

	public void SettingsMover_ReCenter_Late () {
		Invoke ("SettingsMover_ReCenter_NOW", 0.5F);

		// Animate Close Button
		BackButton_Animate_Direct (1);

		if (settingsMenuState_Curr != SettingsMenuState.Middle) {
			animMoveOnlyHolder.Play ("Settings Popup - Mover Only Exit GoSmall Anim 1 (Legacy)");
			CloseAllButtons_Rescale_Instant ();
		} else {
			CloseAllButtons_Rescale_Anim ();
		}

		// Save after closing settings
		SaveLoad.Save ();
	}

	public void SettingsMover_ReCenter_NOW () {
		transSettingsMoverHolder.localPosition = Vector3.zero;
		animMoveOnlyHolder.Play ("Settings Popup - Mover Only Enter ReSize Anim 1 (Legacy)");
	}

	public void Pressed_MusicVol () {
		if (PlayerData_Main.Instance.player_Music_ON) {
			PlayerData_Main.Instance.player_Music_ON = false;
		} else {
			PlayerData_Main.Instance.player_Music_ON = true;
		}

        //  Music Vol Toggle ()
        if (OnMusicToggled!=null)
        {
            OnMusicToggled();
        }

		UpdateSoundMusicIcons ();

		// TODO: Music Vol Toggle Stuff
	}

	public void Pressed_SoundVol () {
		if (PlayerData_Main.Instance.player_Sound_ON) {
			PlayerData_Main.Instance.player_Sound_ON = false;
		} else {
			PlayerData_Main.Instance.player_Sound_ON = true;
		}
        //if (SoundManager.Instance != null)
        //{
        //    SoundManager.Instance.GenericTap2.Play();
        //}
        if (OnSfxToggled!= null)
        {
            OnSfxToggled();
        } 

		//  SoundFX Vol Toggle ()
		UpdateSoundMusicIcons ();
	}

	public void SettingAnimate_Direct (int whichAnim) {
		animPopUpSettingsHolder.clip = animClipPopUpSettingsArr [whichAnim];
		animPopUpSettingsHolder.Play ();

		if (whichAnim < 2) {
			BackButton_Animate_Direct (3);
			settingsMenuState_Curr = (SettingsMenuState)(whichAnim + 1);
		}
	}

	public void SettingChangeState (SettingsMenuState whichState) {
		settingsMenuState_Curr = whichState;
	}

	public void Pressed_Back () {
		Debug.LogWarning ("Press Back in Settings");

		particlePageFadeHolder.Emit (1);

		switch (settingsMenuState_Curr) {
		case SettingsMenuState.Middle:
			buttonSettingsClickBehindHolder.onClick.Invoke ();
			BackButton_Animate_Direct (1);
			// Removed this is the back dark version
//			Close_ToZoomOutCheck ();
			break;
		case SettingsMenuState.Left_AboutUs:
			settingsMenuState_Curr = 0;
			SettingAnimate_Direct (2);
			BackButton_Animate_Direct (2);
			Pressed_Back_BringBackButtons ();
			break;
		case SettingsMenuState.Right_ContactUs:
			settingsMenuState_Curr = 0;
			SettingAnimate_Direct (3);
			BackButton_Animate_Direct (2);
			Pressed_Back_BringBackButtons ();
			break;
		default:
			buttonSettingsClickBehindHolder.onClick.Invoke ();
			BackButton_Animate_Direct (1);
			Debug.LogError ("Default WTF!");
			break;
		}
	}

	public void CloseAllButtons_Rescale_Anim () {
		// Old 2x2 placement for settings buttons
//		popupButtonScriptArr [0].Button_PlayAnim_LeaveLeft ();
//		popupButtonScriptArr [1].Button_PlayAnim_LeaveRight ();
//		popupButtonScriptArr [2].Button_PlayAnim_LeaveLeft ();
//		popupButtonScriptArr [3].Button_PlayAnim_LeaveRight ();

		// New 1x5 placement for settings buttons
		popupButtonScriptArr [0].Button_PlayAnim_LeaveLeft ();
		popupButtonScriptArr [1].Button_PlayAnim_LeaveLeft ();
		popupButtonScriptArr [2].Button_PlayAnim_LeaveUp ();
		popupButtonScriptArr [3].Button_PlayAnim_LeaveRight ();
		popupButtonScriptArr [4].Button_PlayAnim_LeaveRight ();

	}

	public void CloseAllButtons_Rescale_Instant () {
		popupButtonScriptArr [0].transform.GetChild (0).localScale = Vector3.zero;
		popupButtonScriptArr [1].transform.GetChild (0).localScale = Vector3.zero;
		popupButtonScriptArr [2].transform.GetChild (0).localScale = Vector3.zero;
		popupButtonScriptArr [3].transform.GetChild (0).localScale = Vector3.zero;
		popupButtonScriptArr [4].transform.GetChild (0).localScale = Vector3.zero;
	}

	public void BackButton_Animate_Direct (int whichAnim) {
		animBackButtonHolder.clip = animClipBackButtonArr [whichAnim];
		animBackButtonHolder.Play ();
	}

	public void Pressed_Back_BringBackButtons () {
		Invoke ("ButtonAnim_StartEnters", 0.2F);
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void Close_ToZoomOutCheck () {
		menuMainScriptHolder.ZoomOut_CheckBack (4);
	}

	public void Pressed_SettingsLink_Insta () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			Application.OpenURL ("https://www.instagram.com/hezarmoshtan");
		} else {
            if (PlayerData_Main.Instance.whatVAS_Type == PlayerData_Main.VAS_NameType.moshte)
            {
                Application.OpenURL("https://www.instagram.com/moshtatashin/");
            }
            else
            {
                Application.OpenURL("https://www.instagram.com/sibil_atashin");
            }
		}
	}

	public void Pressed_SettingsLink_Telegram () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			Application.OpenURL ("https://t.me/hezarmoshtan");
		} else {
            if (PlayerData_Main.Instance.whatVAS_Type == PlayerData_Main.VAS_NameType.moshte)
            {
                Application.OpenURL("https://t.me/moshte_atashin");
            }
            else
            {
                Application.OpenURL("https://t.me/sibilatashin");
            }
			
		}
	}

	public void Pressed_SettingsLink_Email () {
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {	
			Application.OpenURL ("mailto:info@appete.ir");
		} else {
            if (PlayerData_Main.Instance.whatVAS_Type == PlayerData_Main.VAS_NameType.moshte)
            {
                Application.OpenURL("mailto:info@moshtatashin.ir");
            }
            else
            {
                Application.OpenURL("mailto:info@sibilatashin.ir");
            }
			
		}

		// For iOS
//		Application.OpenURL ("sms:09102106959&body=I am test!");

		// For Android
//		Application.OpenURL ("sms:09102106959?body=I am test!");
	}

	public void Pressed_SettingsMustacheSignal () {
		animMustacheSigHolder.Stop ();
		animMustacheSigHolder.Play ();
	}

	public void PageName_Credits () {
		text_Page_SettingsName.text = "ﺪﯿﻟﻮﺗ ﻢﯿﺗ";

		// Achievement for looking at the credits
		if (AchievementManager.Instance != null) {
			StartCoroutine (AchievementManager.Instance.AchieveProg_Credits ()); 
		}
	}

	public void PageName_ContactUs () {
		text_Page_SettingsName.text = "ﺎﻣ ﺎﺑ ﺱﺎﻤﺗ";
	}

	public void PageName_Settings () {
		text_Page_SettingsName.text = "ﺕﺎﻤﯿﻈﻨﺗ";
	}

	public void PageName_ChangeNow () {
	}

	public void AnimEvent_CanvasEnable () {
		StartCoroutine (Settings_CanvasEnable ());
	}

	public void AnimEvent_CanvasDisable () {
		canvasSettingsHolder.enabled = false;
		graphRayCasterHolder.enabled = false;
		//		StartCoroutine (World_CanvasDisable ());
	}

	IEnumerator Settings_CanvasEnable () {
		yield return null;
		canvasSettingsHolder.enabled = true;
		graphRayCasterHolder.enabled = true;
	}

	IEnumerator Settings_CanvasDisable () {
		yield return null;
		canvasSettingsHolder.enabled = false;
		graphRayCasterHolder.enabled = false;
	}
}
