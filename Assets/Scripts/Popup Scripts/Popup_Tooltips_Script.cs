﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup_Tooltips_Script : MonoBehaviour {

	public int NumberOfLines;

	[Header ("")]

	[TextArea]
	public string stringToolTipsText;
	public int intTooltipTime;

	[Header ("")]
	public Text[] text_TooltipTextArr;
	public GameObject[] objDialogueParentDirArr;
	public RectTransform[] rectTransDialogueArtMidArr;

	public Animation animToolTipHolder;

	public enum Tooltip_Direction
	{
		Top_Right,
		Top_Left,
		Down_Right,
		Down_Left
	}

	public Tooltip_Direction toolTipDirection;

	private RectTransform rectTransDialogueArtMid_Curr;
	private RectTransform rectTransDialogueArtLeft_Curr;
	private RectTransform rectTransDialogueArtRight_Curr;

	private GameObject objDialogueParent_Curr;
	private Text text_TooltipText_Curr;
	private float floatDialogueParentBuffer;

	private int indexExtender;

	void OnEnable () {
		// Old animate places
//		StartCoroutine (Tooltip_Animate_Enter ());
//		StartCoroutine (Tooltip_Animate_Leave ());

		for (int i = 0; i < 8; i++) {
			if (objDialogueParentDirArr [i].activeInHierarchy) {
				objDialogueParentDirArr [i].SetActive (false);
			}
		}

		// For Lines Count
		if (NumberOfLines < 2) {
			indexExtender = 0;
		} else {
			indexExtender = 4;
		}

		switch (toolTipDirection) {
		case Tooltip_Direction.Top_Right:
			text_TooltipText_Curr = text_TooltipTextArr [0 + indexExtender];
			objDialogueParent_Curr = objDialogueParentDirArr [0 + indexExtender];
			rectTransDialogueArtMid_Curr = rectTransDialogueArtMidArr [0 + indexExtender];
			break;

		case Tooltip_Direction.Top_Left:
			text_TooltipText_Curr = text_TooltipTextArr [1 + indexExtender];
			objDialogueParent_Curr = objDialogueParentDirArr [1 + indexExtender];
			rectTransDialogueArtMid_Curr = rectTransDialogueArtMidArr [1 + indexExtender];
			break;

		case Tooltip_Direction.Down_Right:
			text_TooltipText_Curr = text_TooltipTextArr [2 + indexExtender];
			objDialogueParent_Curr = objDialogueParentDirArr [2 + indexExtender];
			rectTransDialogueArtMid_Curr = rectTransDialogueArtMidArr [2 + indexExtender];
			break;

		case Tooltip_Direction.Down_Left:
			text_TooltipText_Curr = text_TooltipTextArr [3 + indexExtender];
			objDialogueParent_Curr = objDialogueParentDirArr [3 + indexExtender];
			rectTransDialogueArtMid_Curr = rectTransDialogueArtMidArr [3 + indexExtender];
			break;

		default:
			break;
		}

		objDialogueParent_Curr.SetActive (true);

		text_TooltipText_Curr.text = stringToolTipsText;

		StartCoroutine (Tooltip_LengthUpdate ());

	}

	public void Tooltip_DeActivateNow () {
		Tooltip_Activator (false);
	}

	public void Tooltip_Activator (bool isActive) {
		this.gameObject.SetActive (isActive);
	}

	public IEnumerator Tooltip_LengthUpdate () {

		// Wait until it is ready to show (text.x has been set)
		while (text_TooltipText_Curr.rectTransform.sizeDelta.x == 0) {
			Debug.Log ("size x = " + text_TooltipText_Curr.rectTransform.sizeDelta.x);
			yield return null;
		}
//		Debug.Log ("size x = " + text_TooltipText_Curr.rectTransform.sizeDelta.x);

//		yield return new WaitForSeconds (0.01F);
		floatDialogueParentBuffer = (text_TooltipText_Curr.rectTransform.sizeDelta.x - 83) * 0.92F;

		// To prevent size going over 400
		if (floatDialogueParentBuffer > 400) {
//			Debug.LogWarning ("Above 400!!!! and BEFORE font size = " + text_TooltipText_Curr.fontSize);
			text_TooltipText_Curr.fontSize = Mathf.RoundToInt (text_TooltipText_Curr.fontSize * (400 / floatDialogueParentBuffer));
			floatDialogueParentBuffer = 400;

//			Debug.LogWarning ("Above 400!!!! and AFTER font size = " + text_TooltipText_Curr.fontSize);
//			Debug.LogWarning ("Calculate = " + Mathf.RoundToInt ((400 / floatDialogueParentBuffer)));
		}

//		Debug.LogWarning ("Above Parent Name = " + text_TooltipText_Curr.rectTransform.parent.name);
//		Debug.LogError ("text_TooltipText_Curr.rectTransform.sizeDelta.x = " + text_TooltipText_Curr.rectTransform.sizeDelta.x
//			+ " and its parent name: " + text_TooltipText_Curr.transform.parent.name);

//		Debug.LogWarning ("floatDialogueParentBuffer = " + floatDialogueParentBuffer);

		rectTransDialogueArtMid_Curr.sizeDelta = new Vector2 (floatDialogueParentBuffer, rectTransDialogueArtMid_Curr.sizeDelta.y);

		// Check for number of lines (OLD)
//		if (NumberOfLines < 2) {
//			rectTransDialogueArtMid_Curr.sizeDelta = new Vector2 (floatDialogueParentBuffer, 53);
//		} else {
//			rectTransDialogueArtMid_Curr.sizeDelta = new Vector2 (floatDialogueParentBuffer, 75);
//			rectTransDialogueArtLeft_Curr = rectTransDialogueArtMid_Curr.GetChild (0).GetComponent<RectTransform>();
//			rectTransDialogueArtRight_Curr = rectTransDialogueArtMid_Curr.GetChild (1).GetComponent<RectTransform>();
//		}	

//		Debug.LogError ("Yo 3");

		// Old animate
		StartCoroutine (Tooltip_Animate_Enter ());
		StartCoroutine (Tooltip_Animate_Leave ());
//		yield return null;
	}

	public IEnumerator Tooltip_Animate_Enter () {
		animToolTipHolder.Play ("Tooltip Popup - Enter Anim 1 (Legacy)");
		yield return null;
	}

	public IEnumerator Tooltip_Animate_Leave () {
		yield return new WaitForSeconds (intTooltipTime);
		if (!animToolTipHolder.isPlaying) {
			animToolTipHolder.Play ("Tooltip Popup - Leave Anim 1 (Legacy)");
		}
	}

	public void Tooltip_Animate_InstantLeave () {
		animToolTipHolder.Play ("Tooltip Popup - Leave Anim 1 (Legacy)");
	}

}
