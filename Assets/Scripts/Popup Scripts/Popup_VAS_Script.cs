﻿using UnityEngine;
using System.Collections;

public class Popup_VAS_Script : MonoBehaviour {

	//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;

	void OnDisable () {
		PopUp_VasTrans_MoveOut ();
	}

	void OnEnable () {
		PopUp_VasTrans_MoveIn ();
		Invoke ("ButtonAnim_StartEnters", 0.4F);

		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
	}

	void PopUp_VasTrans_MoveOut () {
		this.transform.localPosition = new Vector2 (3200, 0);
	}

	void PopUp_VasTrans_MoveIn () {
		this.transform.localPosition = Vector2.zero;
	}

	void ButtonAnim_StartEnters () {
		if (popupButtonScriptArr.Length != 0) {
			for (int i = 0; i < popupButtonScriptArr.Length; i++) {
				StartCoroutine (ButtonAnim_Enter (i));
			}
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);

		popupButtonScriptArr [whichButton].gameObject.SetActive (true);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void PopUp_DisbleOthers (Transform thisTrans) {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			if (popupButtonScriptArr [i].transform != thisTrans) {
				popupButtonScriptArr [i].gameObject.SetActive (false);
			}
		}
	}
}
