﻿using UnityEngine;
using System.Collections;

public class Popup_VoteForUs_Script : MonoBehaviour {

	//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;

	public Animation animAghdasiVoteHolder;
	public Canvas canvasVotePopUpHolder;

	void OnEnable () {
		Invoke ("ButtonAnim_StartEnters", 0.4F);

		// Movein (Move object to center of the view)
		this.transform.localPosition = new Vector2 (0, 0);

		AghdasiVote_Activator (true);
		canvasVotePopUpHolder.enabled = true;

		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void Pressed_NoVote () {
		AghdasiVote_Activator (false);

//		int intVoteBuffer = PlayerPrefs.GetInt ("playerHasVotedForUs", 0);
//		Debug.LogError ("1      VOTE SCRIPT playerHasVotedForUs = " + intVoteBuffer);
//
//		intVoteBuffer++;
//
//		if (intVoteBuffer > 0) {
//			PlayerPrefs.SetInt ("playerHasVotedForUs", -3);
//		} 
//
//		else {
//			PlayerPrefs.SetInt ("playerHasVotedForUs", intVoteBuffer);
//		}
//
//		Debug.LogError ("2      VOTE SCRIPT playerHasVotedForUs = " + intVoteBuffer);
	}

	public void Pressed_VoteForUsLine () {
		AghdasiVote_Activator (false);

		// Sets the voted to 1 to prevent displaying this any farther
		PlayerPrefs.SetInt ("playerHasVotedForUs", 5);

		switch (PlayerData_Main.Instance.whatShopType) {
		case PlayerData_Main.OnlineShopType.CafeBazaar:
			Application.OpenURL ("https://cafebazaar.ir/app/com.headsupgames.hezarmoshtan/");
			break;
		case PlayerData_Main.OnlineShopType.Myket:
			Application.OpenURL ("https://myket.ir/app/com.headsupgames.hezarmoshtan");
			break;
		case PlayerData_Main.OnlineShopType.Ario:
                ArioInAppPurchase.Instance.Rate();
            break;
            case PlayerData_Main.OnlineShopType.VAS:
                break;
		default:
			break;
		} 
	}

	public void AghdasiVote_Activator (bool whichBool) {
		
		// true / enter
		if (whichBool) {
			animAghdasiVoteHolder.Play ("Vote Popup - Aghdasi Enter Anim 1 (Legacy)");
		}

		// false / leave
		if (!whichBool) {
			animAghdasiVoteHolder.Play ("Vote Popup - Aghdasi Leave Anim 1 (Legacy)");
		}

	}
}
