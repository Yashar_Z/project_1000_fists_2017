﻿using UnityEngine;
using System.Collections;

public class Popup_YesNo_Script : MonoBehaviour {
	//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;

	void OnEnable () {
		Invoke ("ButtonAnim_StartEnters", 0.4F);

		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}
}
