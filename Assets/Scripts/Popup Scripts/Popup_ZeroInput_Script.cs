﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup_ZeroInput_Script : MonoBehaviour {

	public Popup_All_Script popupAllScriptHolder;

	public Text textZeroInputHolder;

	//	public Button[] buttonPauseArr;
	public Popup_Button_Script[] popupButtonScriptArr;

	public Animation[] animLoadingCircleArr;

	void OnDisable () {
		PopUp_VasTrans_MoveOut ();
	}

	void OnEnable () {
		PopUp_VasTrans_MoveIn ();
//		Invoke ("ButtonAnim_StartEnters", 0.4F);

		PopUp_CircleLoading_AnimateStart ();

		// SOUND EFFECTS - Yes / No Pop-up Pressed (EMPTY)
	}

	void PopUp_VasTrans_MoveOut () {
		this.transform.localPosition = new Vector2 (3200, 0);
	}

	void PopUp_VasTrans_MoveIn () {
		this.transform.localPosition = new Vector2 (0, 0);
	}

	void ButtonAnim_StartEnters () {
		if (popupButtonScriptArr.Length != 0) {
			for (int i = 0; i < popupButtonScriptArr.Length; i++) {
				StartCoroutine (ButtonAnim_Enter (i));
			}
		}
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);

		popupButtonScriptArr [whichButton].gameObject.SetActive (true);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - Yes / No One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void PopUp_DisbleOthers (Transform thisTrans) {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			if (popupButtonScriptArr [i].transform != thisTrans) {
				popupButtonScriptArr [i].gameObject.SetActive (false);
			}
		}
	}

	public void PopUp_CircleLoading_AnimateStart () {
		// Resize all circles to zero
		for (int i = 0; i < animLoadingCircleArr.Length; i++) {
			animLoadingCircleArr [i].transform.localScale = Vector3.zero;
		}

		StartCoroutine (PopUp_CircleLoading_AnimateRoutine ());
	}

	IEnumerator PopUp_CircleLoading_AnimateRoutine () {
		for (int i = 0; i < animLoadingCircleArr.Length; i++) {
			animLoadingCircleArr [i].Play ();
			yield return new WaitForSeconds (0.1F);
		}
	}

	public void PopUp_CircleLoading_Close () {
		popupAllScriptHolder.PopUp_AnimPlay (1);
	}

	public void PopUp_ZeroInput_TextChange (string stringText) {
		textZeroInputHolder.text = stringText;
	}
}
