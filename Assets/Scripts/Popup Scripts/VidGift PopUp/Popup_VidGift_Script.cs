﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Popup_VidGift_Script : MonoBehaviour {

	private static readonly double TIME_BETWEEN_HOURS = 6;
	private readonly System.TimeSpan TIME_BETWEEN_VIDGIFTS = System.TimeSpan.FromHours (TIME_BETWEEN_HOURS); 

	public VidGift_EggScript[] vidGiftEggScriptArr;
	public VidGift_EggScript[] vidGiftEggScriptArr_DisplayOnly;

	public Menu_MiniCutscenes_Script miniCutscenesScriptHolder;
	public Reward_Machine_Script rewardMachineScriptHolder;
	public Menu_BackAnims_Script menuBackAnimsScriptHolder;

	public Transform transRouletteEggsHolder;
	public Transform transDisplayEggsHolder;

	public Button buttonVidGiftMenuHolder;
	public Button buttonVidGiftBackClickHolder;
	public Button buttonVidGiftBackButtonHolder;
	public Popup_Tooltips_Script popupTooltipScriptHolder;

//	public UnityEngine.UI.Text textTimeRemainVidGift;

	public Canvas canvasVidGiftHolder;
	public GraphicRaycaster graphicRaycastVidGiftHolder;

	public GameObject objTooltip_TimeLeft;

	// This was for vid gift available notif obj that was moved to Phone_Stats_Script BUT it is here to disable after successful roulette
	public GameObject objNotif_VidGift_TimeNow;

	// Bunny Heads
	public GameObject objBunnyHeads_Moshtan;
	public GameObject objBunnyHeads_Aghdasi;

//	public GameObject objTooltip_NoVid;

	public System.DateTime dateLastWatched_AdVidGift;

	public ItemDatabase itemDB_VidGift;

	/* 2 arrays = First ARRAY is item IDs with large number (Like 500) for mustache ones while 
	 * the second ARRAY has mustache amount in indexes that match mustache ones in first ARRAY
	 * */

	public int[] intItemIDsArr;
	public int[] intMustacheAmountsArr;

	public int[] intItemIDsArr_DisplayOnly;
	public int[] intMustacheAmountsArr_DisplayOnly;

	public int[] listItem;
	public int[] listMustache;

	public Animation animDisplayEggsAllHolder;
	public Animation animButtonHolder;
	public Animation animBunnyFaceHolder;

	public ParticleSystem particleButtonPressSuccess;

	private string stringTimeToShow_HoursAndMins;
	private int intGiftScriptMinSizeBuffer;
	private VidGift_EggScript vidGiftScriptMinSizeBuffer;
	private float floatMinSizeBuffer;
	private int intMustacheIndexCount;

	private bool isListSetupingDone;

	private bool hasButtonLeftAlready;

	void Awake () {
//		Debug.LogError ("AWAKE!");

		if (PlayerData_Main.Instance != null) {
			itemDB_VidGift = PlayerData_Main.Instance.player_ItemDataBase;
		} else {
			itemDB_VidGift = Resources.Load<ItemDatabase> (@"itemDB");
		}

		// Was here but moved out because it needs to happen SOONER (Now in phone stats)
//		dateLastWatched_AdVidGift = PlayerData_Main.Instance.player_dateVidGiftLast;

//		VidGift_TimeReport ();

		// Moved from VidGift_EggsSetupAll
		PrepareForSetup ();

		AnimEvent_CanvasDisable ();
	}

	void BunnyFace_Randomize () {
		if (Random.Range (0, 2) == 0) {
			objBunnyHeads_Moshtan.SetActive (true);
			objBunnyHeads_Aghdasi.SetActive (false);
		} else {
			objBunnyHeads_Moshtan.SetActive (false);
			objBunnyHeads_Aghdasi.SetActive (true);
		}
	}

	public void AnimEvent_MoveIn_VidGift () {
		transform.localPosition = Vector2.zero;
		AnimEvent_CanvasEnable ();
	}

	public void AnimEvent_MoveOut_VidGift () {
		transform.localPosition = new Vector2 (3200, 0);
		AnimEvent_CanvasDisable	();
	}

	public void TimeMachine_ForVidGift_NOW () {
		dateLastWatched_AdVidGift = System.DateTime.Now;
	}

	public void TimeMachine_ForVidGift_Add10Hours () {
		dateLastWatched_AdVidGift = dateLastWatched_AdVidGift.AddHours (-10);
		PlayerData_Main.Instance.player_dateVidGiftLast = dateLastWatched_AdVidGift;

//		Debug.LogError ("System.DateTime.Now - dateLastWatched_AdVidGift) > TIME_BETWEEN_VIDGIFTS = " +
//		(System.DateTime.Now - dateLastWatched_AdVidGift).ToString () + " and " + TIME_BETWEEN_VIDGIFTS.ToString ());
	}

//	void Start () {
//		Invoke ("PrepareForSetup", 0.5F);
//		StartCoroutine (PrepareForSetup());
//	}

	void PrepareForSetup () {
		vidGiftEggScriptArr = transRouletteEggsHolder.GetComponentsInChildren <VidGift_EggScript> ();
		vidGiftEggScriptArr_DisplayOnly = transDisplayEggsHolder.GetComponentsInChildren <VidGift_EggScript> ();

		StartCoroutine (SetuptItemIDsArray ());
	}

	void OnEnable () {
		StartCoroutine (VidAvailable_DelayedForEditor ());
//		StartCoroutine (VidGift_EggsSetupAll ());
	}

	public IEnumerator VidAvailable_DelayedForEditor () {
		yield return new WaitForSeconds (3);

//		#if UNITY_EDITOR
		if (Application.isEditor) {
			PlayerData_Main.Instance.isVideoAvailable_VidGift = true;
			Moshtan_Utilties_Script.ShowToast ("Editor !");
			Debug.Log ("Editor !");
		}
//		#endif
	}

	public void AnimEvent_SetupVidGift () {
		StartCoroutine (VidGift_EggsSetupAll ());
	}

	public void AnimEvent_CanvasEnable () {
		canvasVidGiftHolder.enabled = true;
		graphicRaycastVidGiftHolder.enabled = true;

		hasButtonLeftAlready = false;
	}

	public void AnimEvent_CanvasDisable () {
		canvasVidGiftHolder.enabled = false;
		graphicRaycastVidGiftHolder.enabled = false;

		hasButtonLeftAlready = false;
	}

	public IEnumerator VidGift_EggsSetupAll () {
		yield return new WaitForSeconds (0.1F);

		while (!isListSetupingDone) {
			yield return null;
		}

		// Previous place of SetupItemIDs
		yield return null;

		// Fill Roulette Eggs
		for (int i = 0; i < vidGiftEggScriptArr.Length; i++) {
			// Check for item or mustache
			if (intItemIDsArr [i] > 0) {
				vidGiftEggScriptArr [i].VidGift_ShowEgg (itemDB_VidGift.FindItemByItemId (intItemIDsArr [i]));
			} 
			// IS mustache
			else {
				// Removed mustache amount randomizer
//				Debug.LogWarning ("Index of mustache = " + i);

				vidGiftEggScriptArr [i].VidGift_ShowMustache (intMustacheAmountsArr[i]);
			}
		}

		yield return null;

		// Fill display Eggs (Changed normal intItemIDsArr to _DisplayOnly)
		for (int i = 0; i 	< vidGiftEggScriptArr_DisplayOnly.Length; i++) {
			// Check for item or mustache
			if (intItemIDsArr_DisplayOnly [i] > 0) {
				vidGiftEggScriptArr_DisplayOnly [i].VidGift_ShowEgg (itemDB_VidGift.FindItemByItemId (intItemIDsArr_DisplayOnly [i]));
			}
			// IS mustache
			else {
				// Removed mustache amount randomizer
//				Debug.LogWarning ("Index of mustache = " + i);

				vidGiftEggScriptArr_DisplayOnly [i].VidGift_ShowMustache (intMustacheAmountsArr_DisplayOnly[i]);
			}
		}

		// Now summon Egges
		Setup_DisplayEggBodies ();

		yield return new WaitForSeconds (0.5F);

		// Now summon the button
		VidGift_Button_Enter ();
	}

	public void VidGift_Button_Enter () {
		animButtonHolder.Play ("VidGift Popup - Button Enter Anim 1 (Legacy)");
	}

	public void VidGift_Button_Leave () {
		animButtonHolder.Play ("VidGift Popup - Button Leave Anim 1 (Legacy)");
	}

	public void VidGift_Button_LeaveCheck () {
		if (!hasButtonLeftAlready) {
			VidGift_Button_Leave ();
		}
	}

	public void VidGift_Button_Pressed () {
		animButtonHolder.Play ("VidGift Popup - Button Leave Anim 1 (Legacy)");
		particleButtonPressSuccess.Play ();

		hasButtonLeftAlready = true;

		// Disable back button when egg roulette starts
		buttonVidGiftBackButtonHolder.enabled = false;
	}

	public void VidGift_Button_FailPressed () {
		animButtonHolder.Play ("VidGift Popup - Button FailPress Anim 1 (Legacy)");
	}

	public void VidGift_FindWinner () {
		StartCoroutine (VidGift_FindWinnerRoutine ());
	}

	public IEnumerator VidGift_FindWinnerRoutine () {
		floatMinSizeBuffer = 0;

		yield return new WaitForSeconds (0.1F);

		for (int i = 0; i < intItemIDsArr.Length; i++) {
			if (floatMinSizeBuffer < vidGiftEggScriptArr [i].transform.localScale.x) {
				floatMinSizeBuffer = vidGiftEggScriptArr [i].transform.localScale.x;
				vidGiftScriptMinSizeBuffer = vidGiftEggScriptArr [i];
				intGiftScriptMinSizeBuffer = i;
			}
		}

		for (int i = 0; i < intItemIDsArr.Length; i++) {
			if (i != intGiftScriptMinSizeBuffer) {
				vidGiftEggScriptArr [i].VidGift_Egg_Leave ();
			} else {
				vidGiftEggScriptArr [i].VidGift_Egg_Win ();
			}
		}

		yield return new WaitForSeconds (1.5F);

//		Debug.LogWarning ("Winner is = " + vidGiftScriptMinSizeBuffer.itemThisEgg.farsiName);

		// If for checking if winner is item or mustache
		VidGift_WinnerCallRewardMachine (vidGiftScriptMinSizeBuffer);

		yield return new WaitForSeconds (0.25F);

		// Close the vid gift menu in the middle
		BackButton_VidGift ();

		// Turn off notif
		objNotif_VidGift_TimeNow.SetActive (false);

		// Debugs for times
//		VidGift_TimeReport ();
	}

	public void BackButton_VidGift () {
		buttonVidGiftBackClickHolder.onClick.Invoke ();
	}

	public void SetTime_LastVidGiftWatched () {
		TimeMachine_ForVidGift_NOW ();

        // For time machine, this part needs to be commented
        if (PlayerData_Main.Instance != null)
        {
            PlayerData_Main.Instance.player_dateVidGiftLast = dateLastWatched_AdVidGift;
        }

        // Notification send for next 6 hours
        PostOnesignalNotif_VidGift ();
	}

	public void VidGift_WinnerCallRewardMachine (VidGift_EggScript whichVidGift) {
		if (!whichVidGift.isItemEgg) {
			rewardMachineScriptHolder.GetReward_DailyRewardShort (whichVidGift.intEggMustacheAmount);
		} else {
			rewardMachineScriptHolder.GetReward_VidGiftItem (whichVidGift.intEggItemID);
		}

		rewardMachineScriptHolder.gameObject.SetActive (true);
		rewardMachineScriptHolder.CallFirstChest ();

		// This is to make sure updates are not updated after reward machine
		rewardMachineScriptHolder.isDontUpdateItems = true;
	}

	public IEnumerator VidGift_AfterWinnerReset () {
		yield return new WaitForSeconds (1);
	}

	public IEnumerator SetuptItemIDsArray () {
		yield return null;
		// Test fill
//		SetupItemIDs_TestFill ();

		isListSetupingDone = false;

		MakeListForVideoGifts ();

		intItemIDsArr = new int[vidGiftEggScriptArr.Length];
		intMustacheAmountsArr = new int[vidGiftEggScriptArr.Length];

		intItemIDsArr_DisplayOnly = new int[vidGiftEggScriptArr.Length];
		intMustacheAmountsArr_DisplayOnly = new int[vidGiftEggScriptArr.Length];

		// For straight set
		for (int i = 0; i < intItemIDsArr.Length; i++) {

			intItemIDsArr [i] = listItem [i];
			intMustacheAmountsArr [i] = listMustache [i];

			if (i < 5) {
				intItemIDsArr_DisplayOnly [i] = listItem [i];
				intMustacheAmountsArr_DisplayOnly [i] = listMustache [i];
			}
		}

		// For a bit of randomization of arr items
		intItemIDsArr [0] = listItem [0];
		intMustacheAmountsArr [0] = listMustache [0];
		intItemIDsArr [2] = listItem [1];
		intMustacheAmountsArr [2] = listMustache [1];
		intItemIDsArr [4] = listItem [2];
		intMustacheAmountsArr [4] = listMustache [2];
		intItemIDsArr [6] = listItem [3];
		intMustacheAmountsArr [6] = listMustache [3];
		intItemIDsArr [8] = listItem [4];
		intMustacheAmountsArr [8] = listMustache [4];
		//
		intItemIDsArr [1] = listItem [5];
		intMustacheAmountsArr [1] = listMustache [5];
		intItemIDsArr [3] = listItem [6];
		intMustacheAmountsArr [3] = listMustache [6];
		intItemIDsArr [5] = listItem [7];
		intMustacheAmountsArr [5] = listMustache [7];
		intItemIDsArr [7] = listItem [8];
		intMustacheAmountsArr [7] = listMustache [8];
		intItemIDsArr [9] = listItem [9];
		intMustacheAmountsArr [9] = listMustache [9];

		isListSetupingDone = true;
	}

	public void SetupItemIDs_TestFill () {
		// This part is only for test
		intMustacheIndexCount = 0;
		intItemIDsArr = new int[vidGiftEggScriptArr.Length];
		intMustacheAmountsArr = new int[vidGiftEggScriptArr.Length];

		for (int i = 0; i < intItemIDsArr.Length; i++) {
			intItemIDsArr [i] = Random.Range (101, 136);
		}

		for (int i = 0; i < intMustacheAmountsArr.Length; i++) {
			intMustacheAmountsArr[i] = Random.Range (500,5000);
		}

		//		intMustacheAmountsArr = new int[intMustacheIndexCount];
	}

	public void Pressed_VidGift_BunnyFace () {
		animBunnyFaceHolder.Play ();
	}

	public void Pressed_VidGift_MenuButton () {
		if (VidGift_TimeCompare ()) {
			buttonVidGiftMenuHolder.onClick.Invoke ();

			Tapsell_RequestAgainCheck ();

			// To choose between Aghdasi and Moshtan bunny heads
			BunnyFace_Randomize ();
		} 

		else {
			Report_TimeRemain ();
//			objTooltip_TimeLeft.SetActive (true);
		}
	}

	public void Tapsell_RequestAgainCheck () {
		if (Tapsell_Script.VidGiftAd == null) {
			Tapsell_Script.RequestAd_VidGift ();
		}
	}

	public void VidGift_TimeReport () {
		Debug.LogError ("Vid Gift Last from PlayerData = " + PlayerData_Main.Instance.player_dateVidGiftLast);
		Debug.LogError ("Vid Gift Last from popUp = " + dateLastWatched_AdVidGift);
		Debug.LogError ("Vid Gift NOW = " + System.DateTime.Now);
		Debug.LogError ("Vid Gift DIFFERENCE = " + (System.DateTime.Now - dateLastWatched_AdVidGift));
	}

	public bool VidGift_TimeCompare () {
        //		VidGift_TimeReport ();

        // Time machine (Should be commented for final build)
        //TimeMachine_ForVidGift_Add10Hours();

        if ((System.DateTime.Now - dateLastWatched_AdVidGift) > TIME_BETWEEN_VIDGIFTS) {
			return true;
		} else {
			return false;
		}
	}

	public void Report_TimeRemain () {
		System.TimeSpan timeSpanToNextVidGift = TIME_BETWEEN_VIDGIFTS - (System.DateTime.Now - dateLastWatched_AdVidGift);

		// Get total minues
		int intTotalHoursVidGift = (int)timeSpanToNextVidGift.TotalHours;
		int intTotalMinutesVidGift = (int)timeSpanToNextVidGift.TotalMinutes - (intTotalHoursVidGift * 60);

//		Debug.LogError ("Time Left: " + System.Convert.ToDateTime (timeSpanToNextVidGift.ToString ()).ToString ("HH:mm"));

		string stringTimeToShow_Hours = intTotalHoursVidGift.ToString ();
		string stringTimeToShow_Mins = intTotalMinutesVidGift.ToString ();

//		stringTimeToShow_HoursAndMins = "ﻪﻘﯿﻗﺩ " + "<color=red>" + stringTimeToShow_Mins + "</color>" + " ﻭ ﺖﻋﺎﺳ " + "<color=red>" + stringTimeToShow_Hours + "</color>";

		stringTimeToShow_HoursAndMins = "         ﻪﻘﯿﻗﺩ " + "<color=red>" + stringTimeToShow_Mins + "</color>" + " ﻭ ﺖﻋﺎﺳ " + "<color=red>" + stringTimeToShow_Hours + "</color>";

//		stringTimeToShow_HoursAndMins = System.Convert.ToDateTime (timeSpanToNextVidGift.ToString ()).ToString ("HH:mm");

		popupTooltipScriptHolder.stringToolTipsText = " :ﯼﺪﻌﺑ ﯼﻮﯾﺪﯾﻭ ﺎﺗ ﻩﺪﻧﺎﻣ ﻥﺎﻣﺯ" + "\n" + stringTimeToShow_HoursAndMins;
		menuBackAnimsScriptHolder.Tooltip_VidGift_TimeLeft (objTooltip_TimeLeft);

//		StartCoroutine (Report_TimeRemain_Routine ());
	}

	public void Setup_DisplayEggBodies () {
		animDisplayEggsAllHolder.Play ("VidGift Popup - Enter Egg Displays Anim 1 (Legacy)");
	}

	public void Setup_DisplayEggBodies_Leave () {
		animDisplayEggsAllHolder.Play ("VidGift Popup - Leave Egg Displays Anim 1 (Legacy)");
	}


	public void MakeListForVideoGifts()
	{
		// 2 ta list 10 ta ee dar avali be ezaye iteam ha item id biad va be jaye sibil ha -1
		// dar list dovom bejaye item id ha -1 va amount sibil ha be jaye 
		listItem = new int[10];
		listMustache = new int[10];

		//avalin item chance beyne item 2 ta common va 1 rare hast
		switch (UnityEngine.Random.Range(0, 2))
		{
		case 0:
			listItem[2] = GetCommonItemId();
			listMustache[2] = -1;
			break;
		case 1:
			listItem[2] = GetRareItemId();
			listMustache[2] = -1;
			break;
		case 2:
			listItem[2] = GetCommonItemId();
			listMustache[2] = -1;
			break;
		default:
			Debug.LogError("default switch");
			break;
		}

		//dovomin item common hast
		int uniqueCommonItemId = GetUniqueCommonItemId();
		if (uniqueCommonItemId > 0)
		{
			listItem[1] = uniqueCommonItemId;
			listMustache[1] = -1;
		}
		else
		{
			// It was previously the whole thing but now is only for the else
			listItem[1] = GetCommonItemId();
			listMustache[1] = -1;
		}

		//sevomin item chance beyne common va 250 sibil ya 200 sibil
		switch (UnityEngine.Random.Range(0,3))
		{
		case 0:
			listItem[3] = GetCommonItemId();
			listMustache[3] = -1;
			break;
		case 1:
			listItem[3] = -1;

			// 250 reward
			listMustache[3] = Balance_Constants_Script.VidGift_MustacheReward_Biggest;
			break;
		case 2:
			listItem[3] = -1;

			// 200 reward
			listMustache[3] = Balance_Constants_Script.VidGift_MustacheReward_Big;
			break;
		default:
			Debug.LogError("default switch");
			break;
		}

		//charomi va 5 omi 150 sibil
		listItem[0] = -1;
		listMustache[0] = Balance_Constants_Script.VidGift_MustacheReward_Mid;

		listItem[4] = -1;
		listMustache[4] = Balance_Constants_Script.VidGift_MustacheReward_Mid;
		//baghie 100 sibil
		for (int i = 5; i < 10; i++)
		{
			listItem[i] = -1;

			// 100 reward
			listMustache[i] = Balance_Constants_Script.VidGift_MustacheReward_Small;
		}
	}

	public int GetCommonItemId()
	{
		var itemList = PlayerData_Main.Instance.player_ItemDataBase.GetList();

		int commonItemsCount = 0;

		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].itemTag == Item.ItemTag.Common)
			{
				commonItemsCount++;
			}
		}

		//addad random koochiktar az fullodd bedast biar
		int randomNum = UnityEngine.Random.Range(0, commonItemsCount);

		int c=0;
		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].itemTag == Item.ItemTag.Common)
			{
				c++;
				if (randomNum < c)
				{
					return itemList[k].itemId;
				}
			}
		}
		return -1;
	}

	public int GetRareItemId()
	{
		var itemList = PlayerData_Main.Instance.player_ItemDataBase.GetList();

		int rareItemsCount = 0;

		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].itemTag == Item.ItemTag.Rare)
			{
				rareItemsCount++;
			}
		}

		//addad random koochiktar az fullodd bedast biar
		int randomNum = UnityEngine.Random.Range(0, rareItemsCount);

		int c = 0;
		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].itemTag == Item.ItemTag.Rare)
			{
				c++;
				if (randomNum < c)
				{
					return itemList[k].itemId;
				}
			}
		}
		return -1;
	}

	int GetUniqueCommonItemId()
	{
		var itemList = PlayerData_Main.Instance.player_ItemDataBase.GetList();

		int commonItemsCount = 0;

		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].availabilityState != Item.AvailabilityState.Complete && itemList[k].itemTag == Item.ItemTag.Common)
			{
				commonItemsCount++;
			}
		}

		int randomNum = UnityEngine.Random.Range(0, commonItemsCount);

		int c = 0;
		for (int k = 0; k < itemList.Count; k++)
		{
			if (itemList[k].availabilityState != Item.AvailabilityState.Complete && itemList[k].itemTag == Item.ItemTag.Common)
			{
				c++;
				if (randomNum < c)
				{
					return itemList[k].itemId;
				}
			}
		}
		return -1;
	}

	private void PostOnesignalNotif_VidGift()
	{
		string extraMessage = "Waiting to get a OneSignal userId.";
		OneSignal.IdsAvailable((userId, pushToken) => {
			if (pushToken != null)
			{
				var notification = new Dictionary<string, object>();
				string[] notificationString = new string[4];
				notificationString [0] = "کمیسر میگه بیا جایزه بگیر";
				notificationString [1] = "فیلم و جایزه آوردن برات";
				notificationString [2] = "بیا ویدیو رو ببین و جایزه رو ببر";
				notificationString [3] = "فرنگیس از بانه جنس جدید آورده!";

				notification["contents"] = new Dictionary<string, string>() { { "en", notificationString[Random.Range(0 ,4)]} };
				notification["include_player_ids"] = new List<string>() { userId };

                notification["send_after"] = System.DateTime.Now.ToUniversalTime().AddHours(TIME_BETWEEN_HOURS).ToString("U");

                // Time machine for test (Should be commeted for full build)
                //notification["send_after"] = System.DateTime.Now.ToUniversalTime().AddMinutes(1).ToString("U");

                //Debug.LogError("send_after = " + System.DateTime.Now.ToUniversalTime().AddMinutes(1).ToString("U"));
                extraMessage = "Posting test notification now.";
				OneSignal.PostNotification(notification, (responseSuccess) => {
					extraMessage = "Notification posted successful! " + OneSignalPush.MiniJSON.Json.Serialize(responseSuccess);
				}, (responseFailure) => {
					extraMessage = "Notification failed to post:\n" + OneSignalPush.MiniJSON.Json.Serialize(responseFailure);
				});
			}
			else
				extraMessage = "ERROR: Device is not registered.";
		});

		Debug.LogError ("OneSignal Push Notif for daily reward. extraMessage = " + extraMessage);
	}
}
