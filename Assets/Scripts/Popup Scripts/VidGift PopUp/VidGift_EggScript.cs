﻿using UnityEngine;
using System.Collections;

public class VidGift_EggScript : MonoBehaviour {

	public GameObject[] objEggRibbonsArr;
	public GameObject objItemMustacheTypeHolder;
	public Item_Display_Script itemDisplayScriptHolder;

	public Item itemThisEgg;
	public int intEggMustacheAmount;
	public int intEggItemID;

	public bool isItemEgg;

	public Animation animSingleEggHolder;
	public AnimationClip[] animClipSingleEggArr;

	public Animation animEggParentHolder;
	public AnimationClip[] animClipEggParentHolder;

	public ParticleSystem particleEggWinHolder;

	public UnityEngine.UI.Text text_ItemMustacheAmount;

	public void VidGift_ShowEgg (Item eggItem) {
		isItemEgg = true;

		itemThisEgg = eggItem;
		intEggItemID = eggItem.itemId;

		itemDisplayScriptHolder.SetupAll_VidEggsOnly (eggItem);
		switch (eggItem.itemTag) {
		case Item.ItemTag.Common:
			objEggRibbonsArr [0].SetActive (true);
			break;
		case Item.ItemTag.Rare:
			objEggRibbonsArr [1].SetActive (true);
			break;
		case Item.ItemTag.Legendary:
			objEggRibbonsArr [2].SetActive (true);
			break;
		default:
			break;
		}

		objItemMustacheTypeHolder.SetActive (false);
	}

	public void VidGift_ShowMustache (int amount) {
		isItemEgg = false;

		intEggMustacheAmount = amount;
		text_ItemMustacheAmount.text = intEggMustacheAmount.ToString ();

		objEggRibbonsArr [3].SetActive (true);

		// Selecting the mustache ribbon color
		if (amount == Balance_Constants_Script.VidGift_MustacheReward_Small) {
			objEggRibbonsArr [3].transform.GetChild (2).GetComponent <UnityEngine.UI.Image> ().color = new Color32 (60, 60, 60, 255);
			objEggRibbonsArr [3].transform.GetChild (3).GetComponent <UnityEngine.UI.Image> ().color = new Color32 (60, 60, 60, 255);

			text_ItemMustacheAmount.color = new Color32 (70, 70, 70, 255);
			objItemMustacheTypeHolder.transform.GetChild (0).GetComponent <UnityEngine.UI.Image> ().color = new Color32 (255, 225, 45, 255);

//			text_ItemMustacheAmount.fontSize -= 2;
		} else {

//			text_ItemMustacheAmount.fontSize += 8;
		}

		objItemMustacheTypeHolder.SetActive (true);
	}

	public void VidGift_Egg_Enter () {
		animSingleEggHolder.clip = animClipSingleEggArr [0];
		animSingleEggHolder.Play ();
	}

	public void VidGift_Egg_Leave () {
		animSingleEggHolder.clip = animClipSingleEggArr [1];
		animSingleEggHolder.Play ();
	}

	public void VidGift_Egg_Win () {
		animSingleEggHolder.clip = animClipSingleEggArr [2];
		animSingleEggHolder.Play ();

		particleEggWinHolder.Play ();
	}

	public void VidGift_Egg_Reset () {
		animEggParentHolder.clip = animClipEggParentHolder [0];
		animEggParentHolder.Play ();
	}
}
