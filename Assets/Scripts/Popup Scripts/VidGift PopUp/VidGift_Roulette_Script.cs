﻿using UnityEngine;
using System.Collections;
using TapsellSDK;

public class VidGift_Roulette_Script : MonoBehaviour {

	public Popup_VidGift_Script popUpVidGiftScriptHolder;

	public int intRoulette_Counter;
	public int intRoulette_Threshold200;
	public int intRoulette_Threshold150;
	public int intRoulette_Threshold100;
	public int intRoulette_Threshold75;
	public int intRoulette_Threshold50;
	public int intRoulette_Threshold25;
	public int intRoulette_Threshold0;

	public Animation animRouletteHolder;
	public AnimationClip animClipRoulette;

	public GameObject objTooltipNoVidGift;

	// For Test
	private float speedTest;
	private int counterTest;
	private int intRoulette_Counter_Test;
	private int[] intCountTestArr;

	public void VidGift_Roulette_Start () {
		intRoulette_Counter = Random.Range (36, 55);
		intRoulette_Threshold200 = Random.Range (24, 29);
		intRoulette_Threshold150 = Random.Range (15, 19);
		intRoulette_Threshold100 = Random.Range (10, 13);
		intRoulette_Threshold75 = Random.Range (6, 8);
		intRoulette_Threshold50 = Random.Range (4, 6);
		intRoulette_Threshold25 = Random.Range (1, 3);

//		Debug.LogWarning ("Counter: " + intRoulette_Counter + "   200: " + intRoulette_Threshold200 +
//			"   100: " + intRoulette_Threshold100 + "   50: " + intRoulette_Threshold50 + "   25: " + 
//			intRoulette_Threshold25);

		animRouletteHolder [animClipRoulette.name].speed = 2;
	}

	#region CounterRandomTest
	public void VidGift_Roulette_Start_Test () {
		intRoulette_Counter_Test = Random.Range (36, 55);
		intRoulette_Threshold200 = Random.Range (24, 29);
		intRoulette_Threshold150 = Random.Range (15, 19);
		intRoulette_Threshold100 = Random.Range (10, 13);
		intRoulette_Threshold75 = Random.Range (6, 8);
		intRoulette_Threshold50 = Random.Range (4, 6);
		intRoulette_Threshold25 = Random.Range (1, 3);

//		intRoulette_Counter_Test = 45;
//		intRoulette_Threshold200 = 24;
//		intRoulette_Threshold150 = 15;
//		intRoulette_Threshold100 = 10;
//		intRoulette_Threshold75 = 6;
//		intRoulette_Threshold50 = 4;
//		intRoulette_Threshold25 = 1;
	}

	public void Call_VidGift_CountCheckFor_Test () {
		intCountTestArr = new int[10];

		for (int i = 0; i < 100000; i++) {
			VidGift_Roulette_Start_Test ();
			speedTest = 100;
			counterTest = 0;
			VidGift_CountCheckTesTFor_BUFFER ();
		}

		for (int i = 0; i < 10; i++) {
			Debug.Log ((i + 1).ToString () + " times: " + intCountTestArr [i]);
		}
	}

	public void VidGift_CountCheckTesTFor_BUFFER () {
		VidGift_CountCheckTesTFor ();
	}

	public void VidGift_CountCheckTesTFor () {
		counterTest++;
		if (counterTest > 9) {
			counterTest = 0;
		}

		intRoulette_Counter_Test -= 2;
		//		Debug.LogWarning ("intRoulette_Counter = " + intRoulette_Counter);

//		Debug.Log ("PRE IF intRoulette_Counter_Test? " + intRoulette_Counter_Test);

		if (intRoulette_Counter_Test > intRoulette_Threshold200) {
			VidGift_SpeedChecker_Test (2F);
		} else {
			if (intRoulette_Counter_Test > intRoulette_Threshold150) {
				VidGift_SpeedChecker_Test (1.2F);
			} else {
				if (intRoulette_Counter_Test > intRoulette_Threshold100) {
					VidGift_SpeedChecker_Test (0.75F);
				} else {
					if (intRoulette_Counter_Test > intRoulette_Threshold75) {
						VidGift_SpeedChecker_Test (0.54F);
					} else {
						if (intRoulette_Counter_Test > intRoulette_Threshold50) {
							VidGift_SpeedChecker_Test (0.32F);
						} else {
							if (intRoulette_Counter_Test > intRoulette_Threshold25) {
								VidGift_SpeedChecker_Test (0.2F);
							} else {
								VidGift_SpeedChecker_Test (0);
							}
						}
					}
				}
			}
		}
	}

	public void VidGift_SpeedChecker_Test (float whichSpeed) {
//		Debug.Log ("intRoulette_Counter_Test? " + intRoulette_Counter_Test);
//		Debug.Log ("intRoulette_Counter_Test speed? " + whichSpeed);
		if (speedTest != whichSpeed) {
			
			speedTest = whichSpeed;

//			Debug.Log ("Pre Bob intRoulette_Counter_Test! " + intRoulette_Counter_Test);

			if (whichSpeed == 0) {
				intCountTestArr [counterTest]++;
//				Debug.Log ("Bob!");
			} else {
				VidGift_CountCheckTesTFor ();
			}
		}

		else {
			VidGift_CountCheckTesTFor ();
		}
	}
	#endregion

	public void VidGift_CountCheck () {
		intRoulette_Counter -= 2;
//		Debug.LogWarning ("intRoulette_Counter = " + intRoulette_Counter);

		if (intRoulette_Counter > intRoulette_Threshold200) {
			VidGift_SpeedChecker (2F);
		} else {
			if (intRoulette_Counter > intRoulette_Threshold150) {
				VidGift_SpeedChecker (1.2F);
			} else {
				if (intRoulette_Counter > intRoulette_Threshold100) {
					VidGift_SpeedChecker (0.75F);
				} else {
					if (intRoulette_Counter > intRoulette_Threshold75) {
						VidGift_SpeedChecker (0.54F);
					} else {
						if (intRoulette_Counter > intRoulette_Threshold50) {
							VidGift_SpeedChecker (0.32F);
						} else {
							if (intRoulette_Counter > intRoulette_Threshold25) {
								VidGift_SpeedChecker (0.2F);
							} else {
								VidGift_SpeedChecker (0);
							}
						}
					}
				}
			}
		}
	}

	public void VidGift_SpeedChecker (float whichSpeed) {
		if (animRouletteHolder [animClipRoulette.name].speed != whichSpeed) {

			animRouletteHolder [animClipRoulette.name].speed = whichSpeed;

			if (whichSpeed == 0) {
				animRouletteHolder.Stop ();
				popUpVidGiftScriptHolder.VidGift_FindWinner ();

				// Only start this after winner was found
				StartCoroutine (MiniCutscenes_VidGift_End (1));
			}
		}
	}

	public void Pressed_RouletteStart () {
        // For Test 
        //		Call_VidGift_CountCheckFor_Test ();
        if (Tapligh_Script.Instance.TaplighAvailable)
        {
            Tapligh_Script.Instance.OnTaplighRewardEligible += RouletteStart_Success;
            Tapligh_Script.Instance.ShowAd();
        }
        else if (PlayerData_Main.Instance.isVideoAvailable_VidGift)
        {
            //RouletteStart_Success ();
            if (!Application.isEditor)
            {
                Tapsell_Script.ShowVideo_VidGift(RouletteStart_Success);
            }
            else
            {
                RouletteStart_Success();
            }
        }
        // No vid available
        else
        {
            if (Tapsell_Script.BannerGiftAd != null)
            {
                Tapsell_Script.ShowBanner_VidGift(RouletteStart_Success);
            }
            else
            {
                RouletteStart_Failed();
            }
        }
    }
    void bannerTest()
    {
//        Moshtan_Utilties_Script.ShowToast("show banner");
        Tapsell.requestBannerAd("5a51f2cde995ee00012e4994",  BannerType.BANNER_300x250, Gravity.CENTER, Gravity.CENTER);
    }

	public void RouletteStart_Success () {

        Tapligh_Script.Instance.OnTaplighRewardEligible -= RouletteStart_Success;

        VidGift_Roulette_Start ();

		animRouletteHolder.clip = animClipRoulette;
		animRouletteHolder [animClipRoulette.name].normalizedTime = 0.45F;
		animRouletteHolder.Play ();

		popUpVidGiftScriptHolder.transDisplayEggsHolder.gameObject.SetActive (false);

		popUpVidGiftScriptHolder.VidGift_Button_Pressed ();

		// Call mini cutscene stuff for start of roulette
		StartCoroutine (MiniCutscenes_VidGift_Start());

		// Set last vid watch date to NOW
		popUpVidGiftScriptHolder.SetTime_LastVidGiftWatched ();
	}

	public void RouletteStart_Failed () {
		// Show tooltip that it is not available
		objTooltipNoVidGift.SetActive (true);

		// Fail press anim
		popUpVidGiftScriptHolder.VidGift_Button_FailPressed ();
	}

	public IEnumerator MiniCutscenes_VidGift_Start () {
		popUpVidGiftScriptHolder.miniCutscenesScriptHolder.MiniCutscenes_Enter ();
		popUpVidGiftScriptHolder.miniCutscenesScriptHolder.ClickBlocker_Activate ();
		yield return null;
	}

	public IEnumerator MiniCutscenes_VidGift_End (int waitTime) {
		yield return new WaitForSeconds (waitTime);
		popUpVidGiftScriptHolder.miniCutscenesScriptHolder.MiniCutscenes_Leave ();

		yield return new WaitForSeconds (0.25F);
//		buttonVidGiftHolder.

		yield return new WaitForSeconds (1.5F);
		popUpVidGiftScriptHolder.miniCutscenesScriptHolder.ClickBlocker_DeActivate ();
	}

//	void Update () {
//		Debug.LogError ("Anim Speed  = " + animRouletteHolder [animClipRoulette.name].speed);
//	}
}
