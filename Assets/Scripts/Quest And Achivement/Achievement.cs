﻿using UnityEngine;

[System.Serializable]
public class Achievement
{
    public static System.Action<string> OnComplete;
    public static System.Action<string> OnRewardClaimed;

	public int AchievementID;

    public string name;
    public string farsiName;
	[TextArea]
    public string description;
    public int completedReward; 
    public int targetProgress;

    //agar point system baraye reward dar nazar begirim
    [HideInInspector]
    public int rewardPoints= 0;
    //[HideInInspector]
    public bool completed = false;
    //[HideInInspector]
    public bool claimed = false;
    //[HideInInspector]
    public int currentProgress = 0;

	// fields for deleted
	public bool earned;
	public bool Earned;

    public bool AddProgress(int progress)
    {
        if (completed)
        {
            return false;
        }

        currentProgress += progress;
        if (currentProgress >= targetProgress)
        {
            completed = true;
            if (OnComplete != null)
            {
				AchievementCompleteMoment ();
            }
            return true;
        }

        return false;
    }

	public void ShowAchieveTile () {
		Achievement_TileAllController.Instance.AchieveTile_AddNOW (farsiName);
	}

    public bool SetProgress(int progress)
    {
        if (completed)
        {
            return false;
        }

        currentProgress = progress;
        if (progress >= targetProgress)
        {
            completed = true;
            if (OnComplete != null)
            {
				AchievementCompleteMoment ();
            }

            return true;
        }

        return false;
    }

	public void AchievementCompleteMoment () {
		OnComplete(name);

		//				Debug.LogError ("Achieve WTF 2!? + " + PlayerData_Main.Instance.canShowAchieveTiles);

		// Achievement tile at the moment of completion (The check is for returning players and to prevent them front getting 10 tiles at once)
		if (PlayerData_Main.Instance.canShowAchieveTiles) {
			ShowAchieveTile ();
		}

		// Achievement complete to add to all achieves
		if (AchievementManager.Instance != null) {
			AchievementManager.Instance.AchieveProg_AllAchieves_AddNOW ();
		}

		// Update notifs number for menu
		if (Phone_Stats_Script.Instance != null) {
			Phone_Stats_Script.Instance.NotifStats_UpdateAchieves ();
		}
			
		SaveLoad.Save ();

		Fabric.Answers.Answers.LogCustom (
			"Achievement Unlocked",
			customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
				{ "Name", name }
			}
		);
	}

    public bool ClaimReward()
    {

        if (claimed)
        {
            return false;
        }
        claimed = true;

        if (OnRewardClaimed != null)
        {
            OnRewardClaimed(name);
        }

        return true;
    }

}