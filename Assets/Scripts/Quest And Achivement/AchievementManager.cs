﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System;

public class AchievementManager : MonoBehaviour
{
    #region singleton Boilerplate
    private static AchievementManager _instance = null;
    public static AchievementManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

    }
    #endregion

    public Achievement[] AchievementsArr;

	public bool wasBackdropTouched_Kharak;
	public bool wasBackdropTouched_Mil;
	public bool wasBackdropTouched_ChainAndRing;
	public bool wasBackdropTouched_BoxingBag;

    private int currentRewardPoints = 0;
    private int totalRewardPoints = 0;

//    void Start()
//    {
        //ValidateAchievements();
        //UpdateRewardPointTotals();
//    }

    //private void Update()
    //{
    //    //Debug.Log("sibilhoarder achivement is earned: " + GetAchievementByName("SibilHoarder").Earned + "sibil achivement amount" + GetAchievementByName("SibilHoarder").currentProgress);
    //    //Debug.Log("kill achivement is earned: " + GetAchievementByName("ThinKosh").Earned + "kill achivement amount" + GetAchievementByName("ThinKosh").currentProgress);
    //}

    private void ValidateAchievements()
    {
        ArrayList usedNames = new ArrayList();
        foreach (Achievement achievement in AchievementsArr)
        {
            if (achievement.rewardPoints < 0)
            {
                Debug.LogError("AchievementManager::ValidateAchievements() - Achievement with negative RewardPoints! " + achievement.name + " gives " + achievement.rewardPoints + " points!");
            }

            if (usedNames.Contains(achievement.name))
            {
                Debug.LogError("AchievementManager::ValidateAchievements() - Duplicate achievement names! " + achievement.name + " found more than once!");
            }
            usedNames.Add(achievement.name);
        }
    }

    private Achievement GetAchievementByName (string achievementName)
    {
        return AchievementsArr.FirstOrDefault(achievement => achievement.name == achievementName);
    }

	private Achievement GetAchievementByID (int achievementID)
	{
//		return Achievements.FirstOrDefault(achievement => achievement.name == achievementID);

		Achievement tempAchieve = new Achievement ();

		//		Achievements [achievementID - 100].

		switch (achievementID) {
		case 100:	
			tempAchieve = AchievementsArr [0];
			break;
		case 101:	
			tempAchieve = AchievementsArr [1];
			break;
		case 102:	
			tempAchieve = AchievementsArr [2];
			break;
		case 103:	
			tempAchieve = AchievementsArr [3];
			break;
		case 104:	
			tempAchieve = AchievementsArr [4];
			break;
		case 105:	
			tempAchieve = AchievementsArr [5];
			break;
		case 106:	
			tempAchieve = AchievementsArr [6];
			break;
		case 107:	
			tempAchieve = AchievementsArr [7];
			break;
		case 108:	
			tempAchieve = AchievementsArr [8];
			break;
		case 109:	
			tempAchieve = AchievementsArr [9];
			break;
		case 110:	
			tempAchieve = AchievementsArr [10];
			break;
		case 111:	
			tempAchieve = AchievementsArr [11];
			break;
		case 112:	
			tempAchieve = AchievementsArr [12];
			break;
		case 113:	
			tempAchieve = AchievementsArr [13];
			break;
		case 114:	
			tempAchieve = AchievementsArr [14];
			break;
		case 115:	
			tempAchieve = AchievementsArr [15];
			break;
		case 116:	
			tempAchieve = AchievementsArr [16];
			break;
		case 117:	
			tempAchieve = AchievementsArr [17];
			break;
		case 118:	
			tempAchieve = AchievementsArr [18];
			break;
		case 119:	
			tempAchieve = AchievementsArr [19];
			break;
		case 120:	
			tempAchieve = AchievementsArr [20];
			break;
		case 121:	
			tempAchieve = AchievementsArr [21];
			break;
		case 122:	
			tempAchieve = AchievementsArr [22];
			break;
		case 123:	
			tempAchieve = AchievementsArr [23];
			break;
		case 124:	
			tempAchieve = AchievementsArr [24];
			break;
		case 125:	
			tempAchieve = AchievementsArr [25];
			break;
		case 126:	
			tempAchieve = AchievementsArr [26];
			break;
		case 127:	
			tempAchieve = AchievementsArr [27];
			break;
		case 128:	
			tempAchieve = AchievementsArr [28];
			break;
		case 129:	
			tempAchieve = AchievementsArr [29];
			break;
		case 130:	
			tempAchieve = AchievementsArr [30];
			break;
		case 131:	
			tempAchieve = AchievementsArr [31];
			break;
		case 132:	
			tempAchieve = AchievementsArr [32];
			break;
		case 133:	
			tempAchieve = AchievementsArr [33];
			break;
		case 134:	
			tempAchieve = AchievementsArr [34];
			break;
		case 135:	
			tempAchieve = AchievementsArr [35];
			break;
		case 136:	
			tempAchieve = AchievementsArr [36];
			break;
		case 137:	
			tempAchieve = AchievementsArr [37];
			break;
		case 138:	
			tempAchieve = AchievementsArr [38];
			break;
		case 139:	
			tempAchieve = AchievementsArr [39];
			break;
		case 140:	
			tempAchieve = AchievementsArr [40];
			break;
		case 141:	
			tempAchieve = AchievementsArr [41];
			break;
		case 142:	
			tempAchieve = AchievementsArr [42];
			break;
		case 143:	
			tempAchieve = AchievementsArr [43];
			break;
		case 144:	
			tempAchieve = AchievementsArr [44];
			break;
		case 145:	
			tempAchieve = AchievementsArr [45];
			break;
		case 146:	
			tempAchieve = AchievementsArr [46];
			break;
		case 147:	
			tempAchieve = AchievementsArr [47];
			break;
		case 148:	
			tempAchieve = AchievementsArr [48];
			break;
		case 149:	
			tempAchieve = AchievementsArr [49];
			break;
		case 150:	
			tempAchieve = AchievementsArr [50];
			break;
		case 151:	
			tempAchieve = AchievementsArr [51];
			break;
		case 152:	
			tempAchieve = AchievementsArr [52];
			break;
		case 153:	
			tempAchieve = AchievementsArr [53];
			break;
		case 154:	
			tempAchieve = AchievementsArr [54];
			break;
		case 155:	
			tempAchieve = AchievementsArr [55];
			break;
		case 156:	
			tempAchieve = AchievementsArr [56];
			break;
		case 157:	
			tempAchieve = AchievementsArr [57];
			break;
		case 158:	
			tempAchieve = AchievementsArr [58];
			break;
		case 159:	
			tempAchieve = AchievementsArr [59];
			break;
		case 160:	
			tempAchieve = AchievementsArr [60];
			break;
		case 161:	
			tempAchieve = AchievementsArr [61];
			break;
		case 162:	
			tempAchieve = AchievementsArr [62];
			break;
		case 163:	
			tempAchieve = AchievementsArr [63];
			break;
		case 164:	
			tempAchieve = AchievementsArr [64];
			break;
		case 165:	
			tempAchieve = AchievementsArr [65];
			break;
		case 166:	
			tempAchieve = AchievementsArr [66];
			break;
		case 167:	
			tempAchieve = AchievementsArr [67];
			break;
		case 168:	
			tempAchieve = AchievementsArr [68];
			break;
		case 169:	
			tempAchieve = AchievementsArr [69];
			break;
		case 170:	
			tempAchieve = AchievementsArr [70];
			break;
		case 171:	
			tempAchieve = AchievementsArr [71];
			break;
		case 172:	
			tempAchieve = AchievementsArr [72];
			break;
//		case 172:	
//			tempAchieve = Achievements [72];
//			break;
//		case 173:	
//			tempAchieve = Achievements [73];
			//			break;Achievement_TileAllController
//		case 174:	
//			tempAchieve = Achievements [74];
//			break;
//		case 175:	
//			tempAchieve = Achievements [75];
//			break;
//		case 176:	
//			tempAchieve = Achievements [76];
//			break;
//		case 177:	
//			tempAchieve = Achievements [77];
//			break;
//		case 178:	
//			tempAchieve = Achievements [78];
//			break;

		default:
			break;
		}

		return tempAchieve;
	}

    private void UpdateRewardPointTotals()
    {
        currentRewardPoints = 0;
        totalRewardPoints = 0;

        foreach (Achievement achievement in AchievementsArr)
        {
            if (achievement.completed)
            {
                currentRewardPoints += achievement.rewardPoints;
            }

            totalRewardPoints += achievement.rewardPoints;
        }
    }

    private void AchievementEarned()
    {
        UpdateRewardPointTotals();      
    }

//   public void AddProgressToAchievement(string achievementName, int progressAmount)
	public void AddProgressToAchievement(int achievementID, int progressAmount)
    {
		Achievement achievement = GetAchievementByID(achievementID);
        if (achievement == null)
        {
			Debug.LogWarning("AchievementManager::AddProgressToAchievement() - Trying to add progress to an achievemnet that doesn't exist: " + achievementID);
            return;
        }

        if (achievement.AddProgress(progressAmount))
        {
            AchievementEarned();
        }
    }

//   public void SetProgressToAchievement(string achievementName, int newProgress)
	public void SetProgressToAchievement(int achievementID, int newProgress)
    {
		Achievement achievement = GetAchievementByID(achievementID);
        if (achievement == null)
        {
			Debug.LogWarning("AchievementManager::SetProgressToAchievement() - Trying to add progress to an achievemnet that doesn't exist: " + achievementID);
            return;
        }

        if (achievement.SetProgress(newProgress))
        {
            AchievementEarned();
        }
    }

//  public void ClaimAchievementReward(string achievementName)
	public void ClaimAchievementReward(int achievementID)
    {
		Achievement achievement = GetAchievementByID(achievementID);
        if (achievement == null)
        {
			Debug.LogWarning("AchievementManager::SetProgressToAchievement() - Trying to claim an achievemnet that doesn't exist: " + achievementID);
            return;
        }
        achievement.ClaimReward();
    } 

	public void Achieve_PrevWorlds3Stars () {
		StartCoroutine (AchieveProg_StarSet (PlayerData_Main.Instance.player_Stars));

		StartCoroutine (AchieveProg_World3Star_1Set (PlayerData_Main.Instance.worldsGoldStarsArr [0]));
		StartCoroutine (AchieveProg_World3Star_2Set (PlayerData_Main.Instance.worldsGoldStarsArr [1]));
		StartCoroutine (AchieveProg_World3Star_3Set (PlayerData_Main.Instance.worldsGoldStarsArr [2]));
	}

	public IEnumerator Achievements_CheckForPrevRoutine () {
		yield return new WaitForSeconds (1);

		try {
			Achievements_CheckForPrevNOW ();
		} catch (System.Exception e){
			Debug.Log ("Prev Check try / catch: " + e.Message);
		}

	}

	public void Achievements_CheckForPrevNOW () {
//		Debug.Log					  ("Prev Stage 1");
//		Moshtan_Utilties_Script.ShowToast ("Prev Stage 1 Toast");

		if ((PlayerData_Main.Instance.player_WorldsUnlocked > 1) || (PlayerData_Main.Instance.player_LevelsUnlocked > 1)) {
//			StartCoroutine (AchieveProg_LevelComplete_First ());
			AchieveProg_LevelComplete_FirstNonRoutine ();
		}

//		Debug.Log					  ("Prev Stage 2");
//		Moshtan_Utilties_Script.ShowToast ("Prev Stage 2 Toast");

		// World starts (normal)
		AchievemenCertainProgSet (101, PlayerData_Main.Instance.player_Stars);
		AchievemenCertainProgSet (113, PlayerData_Main.Instance.player_Stars);
		AchievemenCertainProgSet (128, PlayerData_Main.Instance.player_Stars);
		// TODO: Stuff for world 4 update

		switch (PlayerData_Main.Instance.player_WorldsUnlocked) {
		case 1:
//			Debug.LogError ("YO 1");
			break;
		case 2:
//			Debug.LogError ("YO 2");
			StartCoroutine (AchieveProg_WorldComplete_1 ());
			break;
		case 3:
//			Debug.LogError ("YO 3");
//			Debug.Log					  ("Prev Stage 2.1");
//			Moshtan_Utilties_Script.ShowToast ("Prev Stage 2.1 Toast");

			StartCoroutine (AchieveProg_WorldComplete_1 ());
			StartCoroutine (AchieveProg_WorldComplete_2 ());

			if (PlayerData_Main.Instance.player_World3_LevelsDataArr [9].star_Unlocked_Level) {
				StartCoroutine (AchieveProg_WorldComplete_3 ());
			}

//			Debug.Log					  ("Prev Stage 2.2");
//			Moshtan_Utilties_Script.ShowToast ("Prev Stage 2.2 Toast");
			break;

			// TODO: Stuff for world 4 update
//		case 4:
//			StartCoroutine (AchieveProg_WorldComplete_1 ());
//			StartCoroutine (AchieveProg_WorldComplete_2 ());
//			StartCoroutine (AchieveProg_WorldComplete_3 ());
//			break;
//		case 5:
//			StartCoroutine (AchieveProg_WorldComplete_1 ());
//			StartCoroutine (AchieveProg_WorldComplete_2 ());
//			StartCoroutine (AchieveProg_WorldComplete_3 ());
//			StartCoroutine (AchieveProg_WorldComplete_4 ());
//			break;

		default:
			break;
		}

//		Debug.Log					  ("Prev Stage 3");
//		Moshtan_Utilties_Script.ShowToast ("Prev Stage 3 Toast");

		// World Starts (3 Star levels)
		AchievemenCertainProgSet (112, PlayerData_Main.Instance.worldsGoldStarsArr[0]);
		AchievemenCertainProgSet (121, PlayerData_Main.Instance.worldsGoldStarsArr[1]);
		AchievemenCertainProgSet (130, PlayerData_Main.Instance.worldsGoldStarsArr[2]);
		// TODO: Stuff for world 4 update

//		Debug.Log					  ("Prev Stage 4");
//		Moshtan_Utilties_Script.ShowToast ("Prev Stage 4 Toast");

		// Prepare for complete items
		var itemDBtemp = Resources.Load<ItemDatabase> (@"itemDB").GetList ();
		bool wasLegendaryFound = false;

//		Debug.Log					  ("Prev Stage 5");
//		Moshtan_Utilties_Script.ShowToast ("Prev Stage 5 Toast");

		// Reset for complete items to zero to prevent the bug
		StartCoroutine (AchieveProg_CompleteItems_All_Reset ());

		// For complete items
		for (int i = 0; i < PlayerData_Main.Instance.player_ItemsAmount.Count; i++) {
			if (PlayerData_Main.Instance.player_ItemsAmount [i].availabityState == Item.AvailabilityState.Complete) {
				StartCoroutine (AchieveProg_CompleteItems_15 ());
				StartCoroutine (AchieveProg_CompleteItems_All ());

//				Debug.Log					  ("Prev Stage 6");
//				Moshtan_Utilties_Script.ShowToast ("Prev Stage 6 Toast");

				// For having one legendary item
				if (!wasLegendaryFound) {
					if (itemDBtemp [i].itemTag == Item.ItemTag.Legendary) {
						StartCoroutine (AchieveProg_GetLegendaryItem ());
						wasLegendaryFound = true;
					}
				}

//				Debug.Log					  ("Prev Stage 7");
//				Moshtan_Utilties_Script.ShowToast ("Prev Stage 7 Toast");

				// For getting moms picture
				if (PlayerData_Main.Instance.player_ItemsAmount [i].ItemID == 102) {
					StartCoroutine (AchieveProg_GetMomsPicture ());
				}
			}
		}

		// Only after prev check, can allow achievement tiles
		if (!PlayerData_Main.Instance.canShowAchieveTiles) {
			StartCoroutine (PlayerData_Main.Instance.AllowAchievementTiles ());
		}

		// For having a lot of mustache unspent
//		if (PlayerData_Main.Instance.player_Mustache > 100000) {
//			StartCoroutine (AchieveProg_Mustache_Hoarder ());
//		}

		// For completeing all / most achievements (Don't need it)
//		for (int i = 0; i < AchievementsArr.Length; i++) {
//			if (AchievementsArr [i].completed) {
//				StartCoroutine (AchieveProg_AllAchieves ());
//			}
//		}
	}

	public void AchievemenCertainProgSet (int whichAchieve, int whichProg) {
		SetProgressToAchievement (whichAchieve, whichProg);
	}

	public IEnumerator AchieveProg_LevelComplete_First () {
		AddProgressToAchievement (100, 1);
		yield return null;
	}


    public void AchieveProg_LevelComplete_FirstNonRoutine () {
		AddProgressToAchievement (100, 1);
	}

	public IEnumerator AchieveProg_StarGain () {
		AddProgressToAchievement (101, 1);
		AddProgressToAchievement (113, 1);
		AddProgressToAchievement (128, 1);

		// World 4
//		AddProgressToAchievement (172, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_StarSet (int starAmount) {
		SetProgressToAchievement (101, starAmount);
		SetProgressToAchievement (113, starAmount);
		SetProgressToAchievement (128, starAmount);

		// World 4
		//		AddProgressToAchievement (172, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Thin1 () {
		AddProgressToAchievement (102, 1);
		AddProgressToAchievement (103, 1);
		AddProgressToAchievement (104, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Thin2 () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Fat1 () {
		AddProgressToAchievement (105, 1);
		AddProgressToAchievement (106, 1);
		AddProgressToAchievement (107, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Fat2 () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Muscle1 () {
		AddProgressToAchievement (108, 1);
		AddProgressToAchievement (109, 1);
		AddProgressToAchievement (110, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Muscle2 () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Giant1 () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Giant2 () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_HF1 () {
		AddProgressToAchievement (114, 1);
		AddProgressToAchievement (115, 1);
		AddProgressToAchievement (116, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_HFX () {
		AddProgressToAchievement (117, 1);
		AddProgressToAchievement (118, 1);
		AddProgressToAchievement (119, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_Barrel () {
		AddProgressToAchievement (122, 1);
		AddProgressToAchievement (123, 1);
		AddProgressToAchievement (124, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_BarrelX () {
		AddProgressToAchievement (125, 1);
		AddProgressToAchievement (126, 1);
		AddProgressToAchievement (127, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillEnemy_BarrelQ () {
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);
		AddProgressToAchievement (888, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_WorldComplete_1 () {
		AddProgressToAchievement (111, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_WorldComplete_2 () {
		AddProgressToAchievement (120, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_WorldComplete_3 () {
		AddProgressToAchievement (129, 1);

		yield return null;
	}

//	public IEnumerator AchieveProg_WorldComplete_4 () {
//		AddProgressToAchievement (176, 1);
//
//		yield return null;
//	}

	public IEnumerator AchieveProg_World3Star_1 () {
		AddProgressToAchievement (112, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_World3Star_1Set (int starAnount) {
		SetProgressToAchievement (112, starAnount);

		yield return null;
	}

	public IEnumerator AchieveProg_World3Star_2 () {
		AddProgressToAchievement (121, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_World3Star_2Set (int starAnount) {
		SetProgressToAchievement (121, starAnount);

		yield return null;
	}

	public IEnumerator AchieveProg_World3Star_3 () {
		AddProgressToAchievement (130, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_World3Star_3Set (int starAnount) {
		SetProgressToAchievement (130, starAnount);

		yield return null;
	}

	public IEnumerator AchieveProg_KillBy_PowUp1 () {
		AddProgressToAchievement (131, 1);

		yield return null;
	}

//	public IEnumerator AchieveProg_World3Star_4 () {
//		AddProgressToAchievement (177, 1);
//
//		yield return null;
//	}

//	public IEnumerator AchieveProg_World3Star_5 () {
//		AddProgressToAchievement (???, 1);
//
//		yield return null;
//	}

	public IEnumerator AchieveProg_PowerUpUse_Any () {
		AddProgressToAchievement (132, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_PowerUpUse_8in1_Reset () {
		SetProgressToAchievement (133, 0);

		yield return null;
	}

	public IEnumerator AchieveProg_PowerUpUse_8in1_Increase () {
		AddProgressToAchievement (133, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_LevelComplete_ContinueUsed () {
		AddProgressToAchievement (134, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_1Health4Times_Reset () {
		SetProgressToAchievement (135, 0);

		yield return null;
	}

	public IEnumerator AchieveProg_1Health4Times_Increase () {
		AddProgressToAchievement (135, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_1Health4Times_Complete () {
		AddProgressToAchievement (135, 4);

		yield return null;
	}

	public int AchieveProg_1Health4Times_Progress () {
		return GetAchievementByID (135).currentProgress;
	}

	// Achieve was removed
	//	public IEnumerator AchieveProg_10TimesPlayLevel () {
	//		AddProgressToAchievement (136, 1);
	//
	//		yield return null;
	//	}

	public IEnumerator AchieveProg_LevelStar_Unmissable () {
		AddProgressToAchievement (136, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_EggBuy_Mosthan () {
		AddProgressToAchievement (137, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_CompleteItems_15 () {
		AddProgressToAchievement (138, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_CompleteItems_All () {
		AddProgressToAchievement (139, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_CompleteItems_All_Reset () {
		SetProgressToAchievement (139, 0);

		yield return null;
	}

	public IEnumerator AchieveProg_GetLegendaryItem () {
		AddProgressToAchievement (140, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_GetMomsPicture () {
		AddProgressToAchievement (141, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_TouchMenuBackDrop () {
		AddProgressToAchievement (142, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_Mustache_Hoarder_Set (int whichMustache) {
		AchievemenCertainProgSet (143, whichMustache);

		yield return null;
	}

	public IEnumerator AchieveProg_Mustache_Hoarder_Add () {
		AddProgressToAchievement (143, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_Mustache_Spender_Set (int whichMustache) {
		AchievemenCertainProgSet (144, whichMustache);
		AchievemenCertainProgSet (145, whichMustache);
		AchievemenCertainProgSet (146, whichMustache);

		yield return null;
	}

	public IEnumerator AchieveProg_Mustache_Spender_AddOne () {
		AddProgressToAchievement (144, 1);
		AddProgressToAchievement (145, 1);
		AddProgressToAchievement (146, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_Mustache_Spender_AddAmount (int whichMustache) {
		AddProgressToAchievement (144, whichMustache);
		AddProgressToAchievement (145, whichMustache);
		AddProgressToAchievement (146, whichMustache);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_1_3 () {
		// Get all pickup items at 1-3
		AddProgressToAchievement (147, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_1_8 () {
		// Get all pickup items at 1-3
		AddProgressToAchievement (148, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_1_10 () {
		// Get all pickup items at 1-3
		AddProgressToAchievement (149, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_2_4 () {
		// Get all pickup items
		AddProgressToAchievement (150, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_2_7 () {
		// Get all pickup items
		AddProgressToAchievement (151, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_2_9 () {
		// Get all pickup items
		AddProgressToAchievement (152, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_2_10 () {
		// Get all pickup items
		AddProgressToAchievement (153, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_3_5 () {
		// MORE than 80 punches
		AddProgressToAchievement (154, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_3_8 () {
		// LESS than Precision %35 
		AddProgressToAchievement (155, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_UniqueLevel_3_10 () {
		// LESS than Precision %35 
		AddProgressToAchievement (156, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_Credits () {
		AddProgressToAchievement (157, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_PhotographStare () {
		AddProgressToAchievement (158, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_PipeNotHit () {
		AddProgressToAchievement (159, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_NoPunches () {
		AddProgressToAchievement (160, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_DoPhotograph_100 () {
		AddProgressToAchievement (161, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_DoPipe_1000 () {
		AddProgressToAchievement (162, 1);

		yield return null;
	}
		
	public IEnumerator AchieveProg_DoHeartSteal_100 () {
		AddProgressToAchievement (163, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_DoLastStand_100 () {
		AddProgressToAchievement (164, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_DoShieldMiss_100 () {
		AddProgressToAchievement (165, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_DoCrit_50000 () {
		AddProgressToAchievement (166, 1);

		yield return null;
	}

	public IEnumerator AchieveProg_KillBy_Explosion () {
		AddProgressToAchievement (167, 1);

		yield return null;
	}

//	public IEnumerator AchieveProg_HedgehogToHear () {
//		AddProgressToAchievement (169, 1);
//
//		yield return null;
//	}

	public IEnumerator AchieveProg_HF_ClingySwatter () {
		AddProgressToAchievement (168, 1);

		yield return null;
	}

	public void AchieveProg_AllAchieves_AddNOW () {
		StartCoroutine (AchieveProg_AllAchieves ());
	}

	// ALL ACHIEVES
	public IEnumerator AchieveProg_AllAchieves () {
		AddProgressToAchievement (169, 1);

		yield return null;
	}

	public void AchieveProg_CompleteQuest_AddNOW () {
		StartCoroutine (AchieveProg_CompleteQuest ());
	}

	public IEnumerator AchieveProg_CompleteQuest () {
		AddProgressToAchievement (170, 1);
		AddProgressToAchievement (171, 1);
		AddProgressToAchievement (172, 1);

		yield return null;
	}

}


//nemoone estefade
// public AchievementManager AchievementManager;
// if(MoshtanDied())
//      AchievementManager.AddProgressToAchievement("ezraeel ooooomad", 1.0f);
//
// if(MustacheAmountChange())
//      AchievementManager.SetProgressToAchievement("sag sibil", float(playerData.mustacheAmount));
//