﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementPackage_Script : MonoBehaviour {

	public Achievement_AllController_Script achieveAllControllerHolder;

	public GameObject objAchieveRewards_Incomplete;
	public GameObject objAchieveRewards_Complete;
	public GameObject objAchieveRewards_Claimed;
//	public GameObject objQuestRewardBoxFull;

	public GameObject objToolTipAchieve;
	public GameObject objParticleAchieveFlashHolder;

//	public GameObject objBackReadyShadowHolder;
//	public GameObject objParticleClaimMomentHolder;

	public Achievement achieveThisPacakgeBuffer;

//	public Canvas canvasAchieveButtonHolder;

	public Text textAchieveTitleHolder;
	public Text textAchieveDescriptHolder;

//	public Text textAchieveProgressHolder_Curr;
//	public Text textAchieveProgressHolder_Max;

	public Text textAchieveProgressHolder_All;
	public Text textAchieveRewardsHolder;

	[Header ("Mustache Display Stuff")]
	//	public Text textQuestMustacheDisplay;
	public Mustache_Collector mustacheCollectorHolder;

//	public Animation animAchievePackHolder;
	public Animation animMustacheMoverHolder;
//	public ParticleSystem particleClaimMomentFlash;
//	public ParticleSystem particleFullFlashHolder;

	[TextArea]
	public string stringAchieveDescript;
	public string stringAchieveTitle;

	public int intAchieveProgress_Curr;
	public int intAchieveProgress_Max;
	public int intAchieveReward;

	public Shadow shadowAchieveAllBackBox;
	public Image imageAchieveAllBackBox;
	//
	public Image imageAchieveIconAndTextBox;
	public Image imageAchieveIcon_BoxBack;
	public Image imageAchieveIcon_IconBackCircle;
	//
	public Image imageAchieveTitle_BoxBack;
//	public Image imageAchieveTitle_Text;
	//
	public Image imageAchieveDescript_BoxBack;
//	public Image imageAchieveDescript_Text;

	public Image imageAchievementProg;

	// Moved to all
//	public Color[] colorAchieve_AllBackBoxArr;
//	public Color[] colorAchieve_IconAndTextBoxArr;
//	public Color[] colorAchieve_IconBackBoxArr;
//	public Color[] colorAchieve_IconSerrCircleArr;
//	public Color[] colorAchieveTitleBack;
//	public Color[] colorAchieveTitleText;
//	public Color[] colorAchieveDescriptBack;
//	public Color[] colorAchieveDescriptText;

	//	private Color colorBackBuffer;
	private Achievement_AllController_Script.AchievePackStatus achievePackStatus;
	private string stringAchieveProgAll;
	private int intAchieveProgAllLength;
	private bool isAchieveComplete;
	private bool isAchieveActive;

//	void Start () {
		//		isAchieveComplete = false;
//	}

	public void Achieve_Setup (Achievement whatAchieve) {
		achieveThisPacakgeBuffer = whatAchieve;

		//		Debug.LogError ("questThisPacakgeBuffer DESCRIPT = " + questThisPacakgeBuffer.Description);

		Achieve_GetManagerValues ();

		Achieve_SetupProgress ();
		Achieve_SetupReward ();
		Achieve_SetupText ();

		Achieve_GetStatus ();

		// Sets the change to complete or not (Previously at the end of Start)
		Achieve_StatusQ ();
	}

	public void Achieve_Activator (bool whichBool) {

//		if (whichBool) {
//			if (!animAchievePackHolder.isPlaying) {
//				this.transform.localScale = Vector3.one;
//			}
//		} else {
//			animAchievePackHolder.Stop ();
//			this.transform.localScale = Vector3.zero;
//		}

		//		this.gameObject.SetActive (whichBool);
	}



	public void Achieve_GetManagerValues () {
		intAchieveProgress_Curr = achieveThisPacakgeBuffer.currentProgress;
		intAchieveProgress_Max = achieveThisPacakgeBuffer.targetProgress;

		stringAchieveDescript = achieveThisPacakgeBuffer.description;
		stringAchieveTitle = achieveThisPacakgeBuffer.farsiName;
		isAchieveComplete = achieveThisPacakgeBuffer.completed;
		intAchieveReward = achieveThisPacakgeBuffer.completedReward;

		imageAchievementProg.fillAmount = (float) intAchieveProgress_Curr / (float) intAchieveProgress_Max;

//		isAchieveActive = achieveThisPacakgeBuffer.IsActiveToday;
	}

	public void Achieve_SetupProgress () {
//		textAchieveProgressHolder_Curr.text = intAchieveProgress_Curr.ToString ();
//		textAchieveProgressHolder_Max.text = intAchieveProgress_Max.ToString ();

		stringAchieveProgAll = intAchieveProgress_Curr.ToString () + "<color=red> / </color>" + intAchieveProgress_Max.ToString ();

//		intAchieveProgAllLength = Mathf.FloorToInt (Mathf.Log10 (intAchieveProgress_Curr) + 1) + Mathf.FloorToInt (Mathf.Log10 (intAchieveProgress_Max) + 1);

		intAchieveProgAllLength = stringAchieveProgAll.Length - 22;
		textAchieveProgressHolder_All.text = stringAchieveProgAll;

		switch (intAchieveProgAllLength) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			textAchieveProgressHolder_All.fontSize = 26;
			break;
		case 6:
		case 7:
			textAchieveProgressHolder_All.fontSize = 22;
			break;
		case 8:
		case 9:
			textAchieveProgressHolder_All.fontSize = 21;
			break;
		case 10:
			textAchieveProgressHolder_All.fontSize = 17;
			break;
		case 11:
			textAchieveProgressHolder_All.fontSize = 15;
			break;
		case 12:
			textAchieveProgressHolder_All.fontSize = 14;
			break;
		case 13:
			textAchieveProgressHolder_All.fontSize = 13;
			break;
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
			textAchieveProgressHolder_All.fontSize = 11;
			break;
		default:
			textAchieveProgressHolder_All.fontSize = 11;
			break;
		}

		// Old resize method
//		if (intAchieveProgAllLength > 5) {
//			textAchieveProgressHolder_All.fontSize = Mathf.RoundToInt(((float)5 / intAchieveProgAllLength) * textAchieveProgressHolder_All.fontSize);
//		}
	}

	public void Achieve_SetupText () {
		textAchieveDescriptHolder.text = stringAchieveDescript;
		textAchieveTitleHolder.text = stringAchieveTitle;
	}

	public void Achieve_SetupReward () {
		textAchieveRewardsHolder.text = intAchieveReward.ToString();
	}

	// Don't use it for now
	public void Achieve_IsActive () {
		Achieve_Activator (isAchieveActive);
	}

	public void Achieve_GetStatus () {
		// Achievement is incomplete
		if (!achieveThisPacakgeBuffer.completed) {
			achievePackStatus = Achievement_AllController_Script.AchievePackStatus.Incomplete;
//			Debug.LogWarning ("Achieve Incomplete");
		} 

		else {
			// Achievement is complete but unclaimed
			if (!achieveThisPacakgeBuffer.claimed) {
				achievePackStatus = Achievement_AllController_Script.AchievePackStatus.Completed;
//				Debug.LogWarning ("Achieve Completed");
			} 

			// Achievement is complete AND claimed
			else {
				achievePackStatus = Achievement_AllController_Script.AchievePackStatus.Claimed;
//				Debug.LogWarning ("Achieve Claimed");
			}
		}
	}

	public void Achieve_StatusQ () {
		switch (achievePackStatus) {
		case Achievement_AllController_Script.AchievePackStatus.Incomplete:
			Achieve_Incomplete ();
			break;
		case Achievement_AllController_Script.AchievePackStatus.Completed:
			Achieve_Complete ();
			break;
		case Achievement_AllController_Script.AchievePackStatus.Claimed:
			Achieve_Claimed ();
			break;
		default:
			Achieve_Incomplete ();
			break;
		}
	}


	public void Achieve_Incomplete () {
		objAchieveRewards_Incomplete.SetActive (true);
		objAchieveRewards_Complete.SetActive (false);
		objAchieveRewards_Claimed.SetActive (false);

		// Flashing particle
//		StartCoroutine (Achieve_FlashParticle (false));

		// Back shadow
		shadowAchieveAllBackBox.enabled = false;
//		objBackReadyShadowHolder.SetActive (false);

		// Do NOT Increase Total Achieve progress
//		achieveAllControllerHolder.intAchieveTotalProg_Curr;

		imageAchieveAllBackBox.color = achieveAllControllerHolder.colorAchieve_AllBackBoxArr[0];
		imageAchieveIconAndTextBox.color = achieveAllControllerHolder.colorAchieve_IconAndTextBoxArr[0];

		imageAchieveIcon_BoxBack.color = achieveAllControllerHolder.colorAchieve_IconBackBoxArr[0];
		imageAchieveIcon_IconBackCircle.color = achieveAllControllerHolder.colorAchieve_IconSerrCircleArr [0];

		imageAchieveTitle_BoxBack.color = achieveAllControllerHolder.colorAchieveTitleBack[0];
		textAchieveTitleHolder.color = achieveAllControllerHolder.colorAchieveTitleText[0];

		imageAchieveDescript_BoxBack.color = achieveAllControllerHolder.colorAchieveDescriptBack[0];
		textAchieveDescriptHolder.color = achieveAllControllerHolder.colorAchieveDescriptText[0];

		// Move in hierarchy for sort (Nothing for incomplete)
//		transform.SetSiblingIndex (0);
	}

	public void Achieve_Complete () {
		objAchieveRewards_Incomplete.SetActive (false);
		objAchieveRewards_Complete.SetActive (true);
		objAchieveRewards_Claimed.SetActive (false);

		// Flashing particle
//		StartCoroutine (Achieve_FlashParticle (true));

		// Back shadow
		shadowAchieveAllBackBox.enabled = true;
//		objBackReadyShadowHolder.SetActive (true);

		// Increase Total Achieve progress
		achieveAllControllerHolder.intAchieveTotalProg_Curr++;

		imageAchieveAllBackBox.color = achieveAllControllerHolder.colorAchieve_AllBackBoxArr[1];
		imageAchieveIconAndTextBox.color = achieveAllControllerHolder.colorAchieve_IconAndTextBoxArr[1];

		imageAchieveIcon_BoxBack.color = achieveAllControllerHolder.colorAchieve_IconBackBoxArr[1];
		imageAchieveIcon_IconBackCircle.color = achieveAllControllerHolder.colorAchieve_IconSerrCircleArr [1];

		imageAchieveTitle_BoxBack.color = achieveAllControllerHolder.colorAchieveTitleBack[1];
		textAchieveTitleHolder.color = achieveAllControllerHolder.colorAchieveTitleText[1];

		imageAchieveDescript_BoxBack.color = achieveAllControllerHolder.colorAchieveDescriptBack[1];
		textAchieveDescriptHolder.color = achieveAllControllerHolder.colorAchieveDescriptText[1];

		// Move in hierarchy for sort
		transform.SetAsFirstSibling ();
	}

	public void Achieve_Claimed () {
		objAchieveRewards_Incomplete.SetActive (false);
		objAchieveRewards_Complete.SetActive (false);
		objAchieveRewards_Claimed.SetActive (true);

		// Flashing particle
//		StartCoroutine (Achieve_FlashParticle (false));

		// Back shadow
		shadowAchieveAllBackBox.enabled = false;
//		objBackReadyShadowHolder.SetActive (false);

		// Increase Total Achieve progress
		achieveAllControllerHolder.intAchieveTotalProg_Curr++;

		imageAchieveAllBackBox.color = achieveAllControllerHolder.colorAchieve_AllBackBoxArr[2];
		imageAchieveIconAndTextBox.color = achieveAllControllerHolder.colorAchieve_IconAndTextBoxArr[2];

		imageAchieveIcon_BoxBack.color = achieveAllControllerHolder.colorAchieve_IconBackBoxArr[1];
		imageAchieveIcon_IconBackCircle.color = achieveAllControllerHolder.colorAchieve_IconSerrCircleArr [1];

		imageAchieveTitle_BoxBack.color = achieveAllControllerHolder.colorAchieveTitleBack[1];
		textAchieveTitleHolder.color = achieveAllControllerHolder.colorAchieveTitleText[1];

		imageAchieveDescript_BoxBack.color = achieveAllControllerHolder.colorAchieveDescriptBack[1];
		textAchieveDescriptHolder.color = achieveAllControllerHolder.colorAchieveDescriptText[1];

		// Move in hierarchy for sort
		transform.SetAsLastSibling ();
	}

	IEnumerator Achieve_FlashParticle (bool whichBool) {
		objParticleAchieveFlashHolder.SetActive (false);
		if (whichBool) {
			yield return new WaitForSeconds (0.5F);
			objParticleAchieveFlashHolder.SetActive (true);
		} 
		else {
			yield return null;
		}
	}

	public void Pressed_AchieveGainCheck () {
		switch (achievePackStatus) {
		case Achievement_AllController_Script.AchievePackStatus.Incomplete:
			objToolTipAchieve.SetActive (true);
			break;
		case Achievement_AllController_Script.AchievePackStatus.Completed:
			Achieve_RewardNow ();
			break;
		case Achievement_AllController_Script.AchievePackStatus.Claimed:
			break;
		default:
			break;
		}
	}

	public void Achieve_RewardNow () {
		//		Debug.LogError ("Give Reward!");

		// Achieve particle claim moment flash
//		particleClaimMomentFlash.transform.SetParent (this.transform.parent.parent);
//		particleClaimMomentFlash.Play ();
//		Destroy (particleClaimMomentFlash.gameObject, 2);

//		objParticleClaimMomentHolder.SetActive (true);

		// Display mustache and GIVE it actually
		AchievementManager.Instance.ClaimAchievementReward (achieveThisPacakgeBuffer.AchievementID);
		Achieve_RewardMustacheShow ();

		// Update notifs number for Achieves
		Phone_Stats_Script.Instance.NotifStats_UpdateAchieves ();

		// SOUNDD EFFECTS - Achievement Package Pressed (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.AchievementMenuClaimed.Play ();	
		}
	}

//	public void Achieve_ButtonCanvas_Control (bool whichBool) {
//		canvasAchieveButtonHolder.enabled = whichBool;
//	}

	public void Achieve_RewardMustacheShow () {
		mustacheCollectorHolder.MustacheCollector_GiveMeFromQuestAchieve (intAchieveReward);

		// Removes mustache gain
		achieveAllControllerHolder.AchieveFromAll_RewardRoutine ();

		// Turn this quest package off before setup
		//		Quest_Activator (false);
	}
}
