﻿using UnityEngine;
using System.Collections;

public class Achievement_ActivatorScript : MonoBehaviour {

	public Achievement_AllController_Script achieveAllControllerScriptHolder;

	public IEnumerator AchievePackage_DelayedDisables () {
		yield return new WaitForSeconds (0.2F);
		for (int i = 0; i < achieveAllControllerScriptHolder.achievPackScriptArr.Length; i++) {
			achieveAllControllerScriptHolder.achievPackScriptArr [i].gameObject.SetActive (false);
			yield return null;
		}
	}

	// Old activator that was based on collider
//	public BoxCollider2D boxCollider2DHolder;
//
//	public void BoxCollider_Activate () {
//		boxCollider2DHolder.enabled = true;
//	}
//
//	public void BoxCollider_DeActivate () {
//		boxCollider2DHolder.enabled = false;
//	}
//
//	void OnTriggerEnter2D(Collider2D other) {
//		Debug.LogError ("ENTER Other name = " + other.name);
//		other.gameObject.SetActive (true);
//	}
//
//	void OnTriggerExit2D(Collider2D other) {
//		Debug.LogError ("EXIT Other name = " + other.name);
//		other.gameObject.SetActive (false);
//	}
}
