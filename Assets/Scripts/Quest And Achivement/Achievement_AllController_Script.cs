﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Achievement_AllController_Script : MonoBehaviour {

	public AchievementPackage_Script[] achievPackScriptArr;
	public Achievement[] achieveRefArr;

//	public Text text_AchieveTotalProg_Curr;
//	public Text text_AchieveTotalProg_Target;

	public GameObject objFrontAllBlocker;

	public Text text_AchieveTotalProg_All;

	public Image imageAchieveTotalProg;

	public int intAchieveTotalProg_Curr;
	public int intAchieveTotalProg_Target;
	public int intAchievePackSiblingIndexBuffer;

	public enum AchievePackStatus
	{
		Incomplete,
		Completed,
		Claimed
	}

	public Transform transAchieveMustacheDisplay;
	public Transform transContentAchievesGridHolder;
	public Transform transAchievePackageRef;

	[Header ("Mustache Display Stuff")]
	//	public Text textQuestMustacheDisplay;
//	public Mustache_Collector mustacheCollectorHolder;

	public Animation animMustacheMoverHolder;
	public ParticleSystem particleFullFlashHolder;
	public ParticleSystem particleMoveRaysHolder;

	[Header ("Colors for packages")]
	public Color[] colorAchieve_AllBackBoxArr;
	public Color[] colorAchieve_IconAndTextBoxArr;
	public Color[] colorAchieve_IconBackBoxArr;
	public Color[] colorAchieve_IconSerrCircleArr;
	public Color[] colorAchieveTitleBack;
	public Color[] colorAchieveTitleText;
	public Color[] colorAchieveDescriptBack;
	public Color[] colorAchieveDescriptText;

	void Awake () {
//		AchieveAll_EnableDisabler (false);

		// Now use scene drag and drop
//		achievPackScriptArr = GetComponentsInChildren <AchievementPackage_Script>();

		// Disable all at the start
//		AhieveAll_EnableDisabler (false);

//		AhieveAll_EnableDisabler_INSTANTLY (false);
	}

	void OnEnable () {
		// TODO: Mohammad for listener

//		AchievesAll_PhoneEnterSetup ();
	}

	public void AhieveAll_EnableDisabler (bool whichBool) {
		StartCoroutine (AhieveAll_EnableDisablerRoutine (whichBool));

//		for (int i = 0; i < achievPackScriptArr.Length; i++) {
//			achievPackScriptArr [i].gameObject.SetActive (whichBool);
//		}
	}

	public void AhieveAll_EnableDisabler_INSTANTLY (bool whichBool) {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].gameObject.SetActive (whichBool);
		}

//		StartCoroutine (AhieveAll_ButtonCanvasesRoutine (whichBool));
	}

	IEnumerator AhieveAll_EnableDisablerRoutine (bool whichBool) {
		yield return null;
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].gameObject.SetActive (whichBool);
		}

//		StartCoroutine (AhieveAll_ButtonCanvasesRoutine (whichBool));
	}

	IEnumerator AhieveAll_EnableWithMiniDelays () {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			yield return null;
			transContentAchievesGridHolder.GetChild(i).gameObject.SetActive (true);
		}
		yield return null;
	}

	public IEnumerator AhieveAll_EnableWithMiniDelays_ToNumber (int count) {
		for (int i = 0; i < count; i++) {
			yield return null;
//			Debug.LogError ("Yo 1");
			transContentAchievesGridHolder.GetChild(i).gameObject.SetActive (true);
		}
		yield return null;
	}

	public IEnumerator AhieveAll_EnableWithMiniDelays_NumberToNumber (int count1, int count2) {
		for (int i = (count1 - 1); i < count2; i++) {
			yield return null;
//			Debug.LogError ("Yo 2");
			transContentAchievesGridHolder.GetChild(i).gameObject.SetActive (true);
		}
		yield return null;
	}

	public IEnumerator AhieveAll_EnableWithMiniDelays_NumberToEnd (int count1) {
		for (int i = (count1 - 1); i < achievPackScriptArr.Length; i++) {
			yield return null;
//			Debug.LogError ("Yo 3");
			transContentAchievesGridHolder.GetChild(i).gameObject.SetActive (true);
		}
		yield return null;
	}

	// Added this to all enable and disables (bool whichBool ones)
	IEnumerator AhieveAll_ButtonCanvasesRoutine (bool whichBool) {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			yield return null;
//			achievPackScriptArr [i].Achieve_ButtonCanvas_Control (whichBool);
		}
	}

//	void Update_OLD () {
//		if (Input.GetKey ("v")) {
//			//			questPackScriptArr [1].Quest_Complete ();
//			this.transform.parent.parent.gameObject.SetActive (true);
//		}
//
//		if (Input.GetKey ("b")) {
//			//			questPackScriptArr [1].Quest_Complete ();
//			achieveRefArr[3].completed = true;
//			achieveRefArr [3].currentProgress = achieveRefArr [3].targetProgress;
//			AchievesAll_Setup ();
//		}
//
//		if (Input.GetKey ("n")) {
//			//			questPackScriptArr [1].Quest_Complete ();
//			achieveRefArr[4].completed = true;
//			achieveRefArr [4].currentProgress = achieveRefArr [4].targetProgress;
//			AchievesAll_Setup ();
//		}
//
//		if (Input.GetKey ("m")) {
//			//			questPackScriptArr [1].Quest_Complete ();
//			achieveRefArr[2].completed = true;
//			achieveRefArr [2].currentProgress = achieveRefArr [2].targetProgress;
//			AchievesAll_Setup ();
//		}
//
//		if (Input.GetKey ("t")) {
//			Time.timeScale = 1;
//		}
//	}
//

	public void AchievesAll_PhoneEnterSetup () {
		// Resize all packages to 1 in case it was in mid queue scale anim when last left here
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].transform.localScale = Vector3.one;
		}

		// Enable disabled packages from awake

		// TODO: Working
//		AchieveAll_EnableDisabler (true);

//		StartCoroutine (AhieveAll_EnableDisablerRoutine (true));

		AchievesAll_Setup ();
	}

	public void AchievesAll_Activator (bool whichBool) {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].Achieve_Activator (whichBool);
		}

//		StartCoroutine (AhieveAll_ButtonCanvasesRoutine (whichBool));
	}

	public void AchieveAll_EnableDisabler (bool whichBool) {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].gameObject.SetActive (whichBool);
		}

//		StartCoroutine (AhieveAll_ButtonCanvasesRoutine (whichBool));
	}

	public void AchievesAll_Setup () {
//		StartCoroutine (AchievesAll_SetupRoutine ());
		AchievesAll_SetupNOW ();
	}

	public IEnumerator AchievesAll_SetupRoutine () {
		objFrontAllBlocker.SetActive (true);
		yield return new WaitForSeconds (0.3F);
		AchievesAll_SetupNOW ();
	}

	public void AchievesAll_SetupNOW () {
		// For resize from quest, not used here
//		AchievesAll_Activator (true);

		// TODO: Working
//		AhieveAll_EnableDisabler (true);

		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].objToolTipAchieve.SetActive (false);
		}

		// Resizes the mustache display to zero
//		MustacheDisplayResize_Zero ();

//		Debug.LogError ("Setup ALL!");
		achieveRefArr = AchievementManager.Instance.AchievementsArr;

		// TODO: Increase actual achievement number for future

		// Getting the total number of achieves
		intAchieveTotalProg_Target = achievPackScriptArr.Length;
//		intAchieveTotalProg_Target = achieveRefArr.Length;

		// Reset number of progress
		intAchieveTotalProg_Curr = 0;

//		Debug.LogWarning ("achievPackScriptArr.Length = " + achievPackScriptArr.Length);
//		Debug.LogWarning ("achieveRefArr.Length = " + achieveRefArr.Length);

		// This was previous used instead of Achievepackagessetup
//		for (int i = 0; i < achievPackScriptArr.Length; i++) {
//			//			if (questRefArr [i].IsActiveToday) {
//
//			achievPackScriptArr [i].Achieve_Setup (achieveRefArr[i]);
//			//			}
//		}
		StartCoroutine (AchievePackagesSetupAll ());

		// Setup total progress visual
		StartCoroutine (AchieveTotalProgress_Setup());

		for (int i = 0; i < transContentAchievesGridHolder.childCount; i++) {
			transAchievePackageRef = transContentAchievesGridHolder.GetChild (i);
			intAchievePackSiblingIndexBuffer = transAchievePackageRef.GetSiblingIndex ();
			if ((intAchievePackSiblingIndexBuffer % 2) == 0) {
				transAchievePackageRef.localPosition = new Vector2 ((Mathf.RoundToInt (intAchievePackSiblingIndexBuffer / 2) + 1)
				* 363, 50.5F);
//					* 363, -99.5F);
			} else {
				transAchievePackageRef.localPosition = new Vector2 ((Mathf.RoundToInt (intAchievePackSiblingIndexBuffer / 2) + 1)
				* 363, -65.5F);
//				* 363, -215.5F);

			}
//			Debug.LogError ("transAchievePackageRef.localPosition.y = " + transAchievePackageRef.localPosition.y);

//			transAchievePackageRef.GetComponent<renderer>().
		}

		StartCoroutine (AhieveAll_EnableWithMiniDelays_ToNumber (10));
	}

	public IEnumerator AchievePackagesSetupAll () {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
			achievPackScriptArr [i].Achieve_Setup (achieveRefArr[i]);
		}
		yield return null;
	}

	public void AchieveFromAll_RewardRoutine () {
		StartCoroutine (AchieveFromAll_RewardNow ());
	}

	public IEnumerator AchieveFromAll_RewardNow () {
		objFrontAllBlocker.SetActive (true);

		// Full flash for reward now / mustache gain
		particleFullFlashHolder.Play ();

		// Move rays particle
		particleMoveRaysHolder.Play ();

		// Hide attribs / stats
//		MustacheAttribs_Activator (false);

//		yield return new WaitForSeconds (0.1F);
//		AhieveAll_EnableDisabler (false);
//		yield return new WaitForSeconds (0.1F);
		
//		AhieveAll_EnableDisabler (true);

		yield return new WaitForSeconds (0.4F);

		// Setup quests again
		//		Debug.LogError ("111111111111111");
		AchievesAll_Setup ();

		objFrontAllBlocker.SetActive (false);
	}

	public IEnumerator QuestFromAll_RewardNow_Part2 () {
		yield return new WaitForSeconds (0.5F);
	}

	public void MustacheDisplayResize_Zero () {
		transAchieveMustacheDisplay.localScale = Vector3.zero;
	}

	public void MustacheAttribs_Activator (bool whichBool) {
		for (int i = 0; i < achievPackScriptArr.Length; i++) {
//			objStatsDisplayArr [i].SetActive (whichBool);
		}
	}

	public IEnumerator AchieveTotalProgress_Setup () {
		yield return null;

		imageAchieveTotalProg.fillAmount = (float) intAchieveTotalProg_Curr / (float) intAchieveTotalProg_Target;

//		text_AchieveTotalProg_Curr.text = intAchieveTotalProg_Curr.ToString ();
//		text_AchieveTotalProg_Target.text = intAchieveTotalProg_Target.ToString ();

		yield return null;

		text_AchieveTotalProg_All.text = intAchieveTotalProg_Curr.ToString () +  "<color=red> / </color>" + intAchieveTotalProg_Target.ToString ();

		// Used this for total achieves but is not needed
//		AchievementManager.Instance.AchievemenCertainProgSet (169, intAchieveTotalProg_Curr);
	}
}
