﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Achievement_TileAllController : MonoBehaviour {

	public static Achievement_TileAllController Instance;

//	public Canvas canvasAchieveTileHolder;
	public string achieveFarsiName;

	public GameObject objAchieveTileSource;
	public Transform transAchiveTileGrid;

	void Awake () {
		Instance = this;
	}

	void Update_OLD () {
		if (Input.GetKeyDown ("x")) {

			achieveFarsiName = AchievementManager.Instance.AchievementsArr [Random.Range (0, 6)].farsiName;
			StartCoroutine (AchieveTile_Add (achieveFarsiName));
//			Debug.LogError ("farsiName = " + achieveFarsiName);
		}
	}

	public void AchieveTile_AddNOW (string achName) {
		StartCoroutine (AchieveTile_Add (achName));
	}

	public IEnumerator AchieveTile_Add (string achieveName) {
//		Debug.LogError ("Achieve Tile = " + achieveName);

		GameObject objNewTile = Instantiate (objAchieveTileSource);
		objNewTile.transform.SetParent (transAchiveTileGrid);
//		objNewTile.transform.localScale = Vector3.one;

		// Recenter the grid
		transAchiveTileGrid.position = new Vector3 (0, transAchiveTileGrid.position.y , transAchiveTileGrid.position.z);

		objNewTile.GetComponent<Achievement_TilePack> ().textAchieveTileName.text = achieveName;

		yield return null;

		// SOUNDD EFFECTS - Achievement Tile Appear (empty endless)
		if (SoundManager.Instance != null) {
			SoundManager.Instance.AchievementIngameEarned.Play ();	
		}
	}
}
