﻿using UnityEngine;
using System.Collections;

public class Achievement_TilePack : MonoBehaviour {

	public Canvas canvasAchieveTileHolder;
	public UnityEngine.UI.Text textAchieveTileName;

//	void OnEnable () {
//		canvasAchieveTileHolder.enabled = true;
//	}

	void OnDisable () {
		canvasAchieveTileHolder.enabled = false;
	}

	public void AchieveTileCanvas_Enabler () {
		canvasAchieveTileHolder.enabled = true;
		this.transform.localScale = Vector3.one;
	}

	public void Destroy_AchieveTile () {
		Destroy (this.gameObject);
	}
}
