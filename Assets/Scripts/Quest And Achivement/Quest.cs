﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Quest {

    public static System.Action OnComplete;
    public static System.Action<string> OnRewardClaimed;

    public enum QuestDifficulties
    {
        Easy,Medium,Hard
    }

	public int QuestID;

    public string QuestName;
    public string FarsiName;
    public string Description;
    public int CompletedReward;
    public int[] TargetProgress;

    //[HideInInspector]
    public int TargetProgressIndex;

    //[HideInInspector]
    public bool Completed;
    //[HideInInspector]
    public bool Claimed;
    //[HideInInspector]
    public int CurrentProgress;

    public bool IsActiveToday;
    public bool IsInQueue;

    public QuestDifficulties QuestDifficulty;
    public int LevelRequirementWorld;
    public int LevelRequirementLevel;


    public bool AddProgress(int progress)
    {
        if (Completed)
        {
            return false;
        }
        if (IsActiveToday)
        {
            CurrentProgress += progress;

            if (CurrentProgress >= TargetProgress[TargetProgressIndex])
            {
                Completed = true;
                if (OnComplete != null)
                {
                    OnComplete();
                }

				// Quest completion achievements
				AchievementManager.Instance.AchieveProg_CompleteQuest_AddNOW ();

				// Fabric for quest completed
				Fabric.Answers.Answers.LogCustom (
					"Quest Completed",
					customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
						{ "Name", QuestName }
					}
				);

                return true;
            }
        }
        return false;
    }
    public bool SetProgress(int progress)
    {
        if (Completed)
        {
            return false;
        }

        if (IsActiveToday)
        {
            CurrentProgress = progress;
            if (progress >= TargetProgress[TargetProgressIndex])
            {
                Completed = true;
                if (OnComplete != null)
                {
                    OnComplete();
                }

				// Quest completion achievements
				AchievementManager.Instance.AchieveProg_CompleteQuest_AddNOW ();

				Fabric.Answers.Answers.LogCustom (
					"Quest Completed",
					customAttributes: new System.Collections.Generic.Dictionary<string, object> () {
						{ "Name", QuestName }
					}
				);

                return true;
            }
        }

        return false;
    }

    public bool ClaimReward()
    {
        if (Claimed)
        {
            return false;
        }
        Claimed = true;
        if (OnRewardClaimed != null)
        {
            OnRewardClaimed(QuestName);
        }
        return true;
    }
}
