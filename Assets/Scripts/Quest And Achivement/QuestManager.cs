﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;

public class QuestManager : MonoBehaviour {
    #region singleton Boilerplate
    private static QuestManager _instance = null;
    public static QuestManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

    }
    #endregion

    public static Action<int> OnTransitionFromQueueToActiveQuest;

    // hame quest ha
    public Quest[] Quests;
    //quest haye jari
    public Quest[] ActiveQuests;
    //quest haye dar saf
    public Quest[] QueueQuests;

    public DateTime LastQuestGivenDate;

	void Start () {
		// Should be commented for full build (Time Machine)
//		LastQuestGivenDate = DateTime.Now.AddDays (-1);

//		Debug.LogError ("Quest Time Date = " + LastQuestGivenDate.ToString ());

		// Moved this to menu start (Close to prevcheck for achievement)
//		StartCoroutine (QuestGiver ());

		// Should be commented for full build (Time Machine)
//		Quest_TimeMachine ();
	}

	void Quest_TimeMachine () {
		LastQuestGivenDate.AddDays (-1);
		StartCoroutine (QuestGiverRoutine ());
	}

	public void AddProgressToQuest(int questID, int progressAmount)
    {
//      Quest quest = GetQuestByName(questName);
		Quest quest = GetQuestByID(questID);

        if (quest == null)
        {
			Debug.LogWarning("QuestManager::AddProgressToQuest() - Trying to add progress to a quest that doesn't exist: " + questID);
            return;
        }

        quest.AddProgress(progressAmount);
    }

	public void ClaimQuestReward(int questID)
    {
//		Quest quest = GetQuestByName(questName);
		Quest quest = GetQuestByID(questID);

        if (quest == null)
        {
			Debug.LogWarning("QuestManager::ClaimQuestReward() - Trying to claim a quest that doesn't exist: " + questID);
            return;
        }
        quest.ClaimReward();
        quest.IsActiveToday = false;
        switch (quest.QuestDifficulty)
        {
            case Quest.QuestDifficulties.Easy:
                if (QueueQuests[0].IsInQueue)
                {
                    ActiveQuests[0] = QueueQuests[0];
                    QueueQuests[0].IsInQueue = false;
                    ActiveQuests[0].IsActiveToday = true;
                    ActiveQuests[0].CurrentProgress = 0;
                    ActiveQuests[0].Completed = false;
                    ActiveQuests[0].Claimed = false;
                    if (OnTransitionFromQueueToActiveQuest != null)
                    {
                        OnTransitionFromQueueToActiveQuest(0);
                    }
                }
                break;
            case Quest.QuestDifficulties.Medium:
                if (QueueQuests[1].IsInQueue)
                {
                    ActiveQuests[1] = QueueQuests[1];
                    QueueQuests[1].IsInQueue = false;
                    ActiveQuests[1].IsActiveToday = true;
                    ActiveQuests[1].CurrentProgress = 0;
                    ActiveQuests[1].Completed = false;
                    ActiveQuests[1].Claimed = false;
                    if (OnTransitionFromQueueToActiveQuest != null)
                    {
                        OnTransitionFromQueueToActiveQuest(1);
                    }
                }
                break;
            case Quest.QuestDifficulties.Hard:
                if (QueueQuests[2].IsInQueue)
                {
                    ActiveQuests[2] = QueueQuests[2];
                    QueueQuests[2].IsInQueue = false;
                    ActiveQuests[2].IsActiveToday = true;
                    ActiveQuests[2].CurrentProgress = 0;
                    ActiveQuests[2].Completed = false;
                    ActiveQuests[2].Claimed = false;

                    if (OnTransitionFromQueueToActiveQuest != null)
                    {
                        OnTransitionFromQueueToActiveQuest(2);
                    }
                }
                break;
            default:
                break;
        }
		Debug.LogError ("Claim Reward has happened from QuestManager");
    }

    private Quest GetQuestByName(string questName)
    {
        return Quests.FirstOrDefault(quest => quest.QuestName == questName);
    }

	private Quest GetQuestByID (int questID) {
		Quest tempQuest = new Quest ();

//		Quests [questID - 100].

		switch (questID) {
		case 100:	
			tempQuest = Quests [0];
			break;
		case 101:	
			tempQuest = Quests [1];
			break;
		case 102:	
			tempQuest = Quests [2];
			break;
		case 103:	
			tempQuest = Quests [3];
			break;
		case 104:	
			tempQuest = Quests [4];
			break;
		case 105:	
			tempQuest = Quests [5];
			break;
		case 106:	
			tempQuest = Quests [6];
			break;
		case 107:	
			tempQuest = Quests [7];
			break;
		case 108:	
			tempQuest = Quests [8];
			break;
		case 109:	
			tempQuest = Quests [9];
			break;
		case 110:	
			tempQuest = Quests [10];
			break;
		case 111:	
			tempQuest = Quests [11];
			break;
		case 112:	
			tempQuest = Quests [12];
			break;

		case 113:	
			tempQuest = Quests [13];
			break;

		case 114:	
			tempQuest = Quests [14];
			break;
		case 115:	
			tempQuest = Quests [15];
			break;
		case 116:	
			tempQuest = Quests [16];
			break;
		case 117:	
			tempQuest = Quests [17];
			break;
		case 118:	
			tempQuest = Quests [18];
			break;
		case 119:	
			tempQuest = Quests [19];
			break;
		case 120:	
			tempQuest = Quests [20];
			break;
		case 121:	
			tempQuest = Quests [21];
			break;
		case 122:	
			tempQuest = Quests [22];
			break;
		case 123:	
			tempQuest = Quests [23];
			break;
		case 124:	
			tempQuest = Quests [24];
			break;
		case 125:	
			tempQuest = Quests [25];
			break;
		case 126:	
			tempQuest = Quests [26];
			break;
		case 127:	
			tempQuest = Quests [27];
			break;
		case 128:	
			tempQuest = Quests [28];
			break;
		case 129:	
			tempQuest = Quests [29];
			break;
		case 130:	
			tempQuest = Quests [30];
			break;
		case 131:	
			tempQuest = Quests [31];
			break;
		case 132:	
			tempQuest = Quests [32];
			break;
		case 133:	
			tempQuest = Quests [33];
			break;
		case 134:	
			tempQuest = Quests [34];
			break;
		case 135:	
			tempQuest = Quests [35];
			break;
		case 136:	
			tempQuest = Quests [36];
			break;
		case 137:	
			tempQuest = Quests [37];
			break;
		case 138:	
			tempQuest = Quests [38];
			break;
		case 139:	
			tempQuest = Quests [39];
			break;
		case 140:	
			tempQuest = Quests [40];
			break;
		case 141:	
			tempQuest = Quests [41];
			break;
		case 142:	
			tempQuest = Quests [42];
			break;
		case 143:	
			tempQuest = Quests [43];
			break;
		case 144:	
			tempQuest = Quests [44];
			break;
		case 145:	
			tempQuest = Quests [45];
			break;
		case 146:	
			tempQuest = Quests [46];
			break;
		case 147:	
			tempQuest = Quests [47];
			break;
		case 148:	
			tempQuest = Quests [48];
			break;
		case 149:	
			tempQuest = Quests [49];
			break;
		case 150:	
			tempQuest = Quests [50];
			break;
		case 151:	
			tempQuest = Quests [51];
			break;
		case 152:	
			tempQuest = Quests [52];
			break;
		case 153:	
			tempQuest = Quests [53];
			break;
		case 154:	
			tempQuest = Quests [54];
			break;
		case 155:	
			tempQuest = Quests [55];
			break;
		case 156:	
			tempQuest = Quests [56];
			break;
		case 157:	
			tempQuest = Quests [57];
			break;
		case 158:	
			tempQuest = Quests [58];
			break;
		case 159:	
			tempQuest = Quests [59];
			break;
		case 160:	
			tempQuest = Quests [60];
			break;
		case 161:	
			tempQuest = Quests [61];
			break;
		case 162:	
			tempQuest = Quests [62];
			break;
		case 163:	
			tempQuest = Quests [63];
			break;
		case 164:	
			tempQuest = Quests [64];
			break;
		case 165:	
			tempQuest = Quests [65];
			break;
		case 166:	
			tempQuest = Quests [66];
			break;
		case 167:	
			tempQuest = Quests [67];
			break;
		case 168:	
			tempQuest = Quests [68];
			break;
		case 169:	
			tempQuest = Quests [69];
			break;
		case 170:	
			tempQuest = Quests [70];
			break;
		case 171:	
			tempQuest = Quests [71];
			break;


		default:
			break;
		}

		return tempQuest;
	}

    //3 bar baraye easy-medium-hard quest mide
    //tarikh emrooz ro ba tarikh given quest check mikone agar emrooz bood ke quest haye emrooz dade shode return mishe
    //aval tarikh emrooz ro vared mikone
    //dovom check mikone ke aya quest kamel nashode hast to slot ya na
    // agar nabood ke quest jadid ezafe mikone
    // agar bood negah mikone ke aya dar saf quest hast ya na..agar dar saf chizi nabood be saf ezafe mikone agar bood return

//	public IEnumerator QuestGiver_FirstTime () {
//		yield return null;
//	}

	public IEnumerator QuestGiverRoutine () {
		yield return new WaitForSeconds (1.2F);

		try {
			QuestGiver ();
		} catch (System.Exception e){
			Debug.Log ("Quest Giver try / catch: " + e.Message);
		}

	}

	public void QuestGiver()
    {
		if (DateTime.Now.Date <= LastQuestGivenDate.Date) {
//			yield break;
			return;
		}

        //3 bar loop baraye easy medium hard 0= easy 1= medium 2= hard
        for (int i = 0; i < 3; i++)
        {
            if (ActiveQuests[i].IsActiveToday)
            {
                //agar quest emrooz hanoz active hast check kon ke to saf chizi hast ya na
                if (QueueQuests[i].IsInQueue)
                {
                    //agar to saf hast ke bikhial sho boro edame halghe
                    continue;
                }
                //agar nist dakhele saf bezar
                switch (i)
                {
                    case 0:
                        QueueQuests[i] = PickQuest(Quest.QuestDifficulties.Easy);
                        break;
                    case 1:
                        QueueQuests[i] = PickQuest(Quest.QuestDifficulties.Medium);
                        break;
                    case 2:
                        QueueQuests[i] = PickQuest(Quest.QuestDifficulties.Hard);
                        break;
                    default:
                        break;
                }

				GetQuestByID(QueueQuests[i].QuestID).IsInQueue = true;

//				Quest quest = GetQuestByID(QueueQuests[i].QuestID);
//
//				if (quest == null)
//				{
//					Debug.LogWarning("QuestManager::QuestGiver() - Trying to give a quest that doesn't exist: " + QueueQuests[i].QuestID);
//					return;
//				}

//              GetQuestByName(QueueQuests[i].QuestName).IsInQueue = true;
            }

            else
            {
                switch (i)
                {
                    case 0:
                        ActiveQuests[i] = PickQuest(Quest.QuestDifficulties.Easy);
                        break;
                    case 1:
                        ActiveQuests[i] = PickQuest(Quest.QuestDifficulties.Medium);
                        break;
                    case 2:
                        ActiveQuests[i] = PickQuest(Quest.QuestDifficulties.Hard);
                        break;
                    default:
                        break;
                }

//				var q = GetQuestByName(ActiveQuests[i].QuestName);

//				Quest quest = GetQuestByID(ActiveQuests[i].QuestID);
//
//				if (quest == null)
//				{
//					Debug.LogWarning("QuestManager::QuestGiver() - Trying to give a quest that doesn't exist: " + QueueQuests[i].QuestID);
//					return;
//				}

				var quest = GetQuestByID(ActiveQuests[i].QuestID);

				quest.IsActiveToday = true;
				quest.CurrentProgress = 0;
				quest.Completed = false;
				quest.Claimed = false;
            }
        }

        LastQuestGivenDate = DateTime.Now;

//		Debug.LogError ("END Quest Time Date = " + LastQuestGivenDate.ToString ());
    }

    // algorithm e dadan quest:bar asas difficulty az pool agar level requirement meet shod  
    //  az target progress arayash yeki bardar random 

    // active shodan ye quest: progress ro bayad reset kard
    // activetoday set she
    // bad az claim agar dar que chizi hast bayad on active she va dar quest haye asli gharar begire

    private Quest PickQuest(Quest.QuestDifficulties difficulty)
    {
        Quest q = new Quest();
        List<Quest> tempQlist = new List<Quest>();
        int numberOfQuest = 0;
        for (int i = 0; i < Quests.Length; i++)
        {
			if ((Quests[i].QuestDifficulty == difficulty) && MeetLevelRequirement(Quests[i]))
            {
                numberOfQuest++;
                tempQlist.Add(Quests[i]);
            }
        }

//		Debug.LogWarning ("Quest Length = " + tempQlist.Count);
		int chosenQuest = UnityEngine.Random.Range(0, numberOfQuest);
		q = tempQlist [chosenQuest];

		// TODO: For checking certain quest (For actual game must be commented)
//		q = tempQlist [15];
//		Debug.LogWarning ("PICK Quest Name = " + q.FarsiName);
//		Debug.LogWarning ("PICK Quest ID = " + q.QuestID);

        q.TargetProgressIndex = UnityEngine.Random.Range(0, q.TargetProgress.Length);
        return q ;
    }

    private bool MeetLevelRequirement(Quest quest)
    {
        if (PlayerData_Main.Instance != null)
        {
			// To make sure a bigger world unlocked is not stopped by lower level unlocked
			if (quest.LevelRequirementWorld < PlayerData_Main.Instance.player_WorldsUnlocked) {
				return true;
			}
				
            //player data main world and level bayad bozorgtar mosavi requirement e quest bashe
            if (quest.LevelRequirementWorld == PlayerData_Main.Instance.player_WorldsUnlocked
                && quest.LevelRequirementLevel <= PlayerData_Main.Instance.player_LevelsUnlocked)
            {
                return true;
            }
        }

//        else
//        {
//            if (quest.LevelRequirementWorld <= 1
//                && quest.LevelRequirementLevel <= 6)
//            {
//                return true;
//            }
//        }

        return false;
    }
		
	public IEnumerator QuestProg_PowerUpUse_Any () {
		AddProgressToQuest (100, 1);
		AddProgressToQuest (101, 1);
		AddProgressToQuest (102, 1);

//		QuestManager.Instance.AddProgressToQuest ("PowerUp_Using_Easy", 1);
//		QuestManager.Instance.AddProgressToQuest ("PowerUp_Using_Medium", 1);
//		QuestManager.Instance.AddProgressToQuest ("PowerUp_Using_Hard", 1);

		yield return null;
	}

	public IEnumerator QuestProg_PowerUpUse_PowUp1 () {
		AddProgressToQuest (103, 1);
		AddProgressToQuest (104, 1);
		AddProgressToQuest (105, 1);

//		QuestManager.Instance.AddProgressToQuest ("PunchPowerUp_Using_Easy", 1);
//		QuestManager.Instance.AddProgressToQuest ("PunchPowerUp_Using_Medium", 1);
//		QuestManager.Instance.AddProgressToQuest ("PunchPowerUp_Using_Hard", 1);

		yield return null;
	}

	public IEnumerator QuestProg_PowerUpUse_PowUp2 () {
		AddProgressToQuest (106, 1);
		AddProgressToQuest (107, 1);
		AddProgressToQuest (108, 1);

//		QuestManager.Instance.AddProgressToQuest ("ShieldPowerUp_Using_Easy", 1);
//		QuestManager.Instance.AddProgressToQuest ("ShieldPowerUp_Using_Medium", 1);
//		QuestManager.Instance.AddProgressToQuest ("ShieldPowerUp_Using_Hard", 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Any () {

		AddProgressToQuest (109, 1);
		AddProgressToQuest (110, 1);
		AddProgressToQuest (111, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Fat1 () {
		AddProgressToQuest (112, 1);
		AddProgressToQuest (113, 1);
		AddProgressToQuest (114, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Fat2 () {
		AddProgressToQuest (115, 1);
		AddProgressToQuest (116, 1);
		AddProgressToQuest (117, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Thin1 () {
		AddProgressToQuest (118, 1);
		AddProgressToQuest (119, 1);
		AddProgressToQuest (120, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Thin2 () {
		AddProgressToQuest (121, 1);
		AddProgressToQuest (122, 1);
		AddProgressToQuest (123, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Muscle1 () {
		AddProgressToQuest (124, 1);
		AddProgressToQuest (125, 1);
		AddProgressToQuest (126, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Muscle2 () {
		AddProgressToQuest (127, 1);
		AddProgressToQuest (128, 1);
		AddProgressToQuest (129, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Giant1 () {
		AddProgressToQuest (130, 1);
		AddProgressToQuest (131, 1);
		AddProgressToQuest (132, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Giant2 () {
		AddProgressToQuest (133, 1);
		AddProgressToQuest (134, 1);
		AddProgressToQuest (135, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_HF () {
		AddProgressToQuest (136, 1);
		AddProgressToQuest (137, 1);
		AddProgressToQuest (138, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_HFX () {
		AddProgressToQuest (139, 1);
		AddProgressToQuest (140, 1);
		AddProgressToQuest (141, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_Barrel () {
		AddProgressToQuest (142, 1);
		AddProgressToQuest (143, 1);
		AddProgressToQuest (144, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_BarrelX () {
		AddProgressToQuest (145, 1);
		AddProgressToQuest (146, 1);
		AddProgressToQuest (147, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillEnemy_BarrelQ() {
		AddProgressToQuest (148, 1);
		AddProgressToQuest (149, 1);
		AddProgressToQuest (150, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillBy_PowUp1 () {
		AddProgressToQuest (151, 1);
		AddProgressToQuest (152, 1);
		AddProgressToQuest (153, 1);

		yield return null;
	}

	public IEnumerator QuestProg_KillBy_Explosion () {
		AddProgressToQuest (154, 1);
		AddProgressToQuest (155, 1);
		AddProgressToQuest (156, 1);

		yield return null;
	}

	public IEnumerator QuestProg_LevelComplete () {
		AddProgressToQuest (157, 1);
		AddProgressToQuest (158, 1);
		AddProgressToQuest (159, 1);

		yield return null;
	}

	public IEnumerator QuestProg_BatteryUse () {
		AddProgressToQuest (160, 1);
		AddProgressToQuest (161, 1);
		AddProgressToQuest (162, 1);

		yield return null;
	}

	public IEnumerator QuestProg_MustacheGain_One () {
		AddProgressToQuest (163, 1);
		AddProgressToQuest (164, 1);
		AddProgressToQuest (165, 1);

		yield return null;
	}

	public IEnumerator QuestProg_MustacheGain_Amount (int howMuch) {
		AddProgressToQuest (163, howMuch);
		AddProgressToQuest (164, howMuch);
		AddProgressToQuest (165, howMuch);

		yield return null;
	}

	public IEnumerator QuestProg_BubbleExplode () {
		AddProgressToQuest (166, 1);
		AddProgressToQuest (167, 1);
		AddProgressToQuest (168, 1);

		yield return null;
	}

	public IEnumerator QuestProg_MissShiled () {
		AddProgressToQuest (169, 1);
		AddProgressToQuest (170, 1);
		AddProgressToQuest (171, 1);

		yield return null;
	}

}
