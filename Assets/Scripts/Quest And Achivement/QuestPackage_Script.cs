﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuestPackage_Script : MonoBehaviour {

	public Quest_AllController_Script questAllControllerHolder;

	public GameObject objQuestTickHolder;
	public GameObject objQuestRewardBoxFull;

	public GameObject objToolTipQuest;
	public GameObject objParticleQuestFlashHolder;

	public Quest questThisPacakgeBuffer;

	public Text textQuestDescriptHolder;
	public Text textQuestProgressHolder;
	public Text textQuestRewardsHolder;

	[Header ("Mustache Display Stuff")]
//	public Text textQuestMustacheDisplay;
	public Mustache_Collector mustacheCollectorHolder;

	public Animation animQuestPackHolder;
	public Animation animMustacheMoverHolder;
//	public ParticleSystem particleFullFlashHolder;

	[TextArea]
	public string stringDescript;

	public int intQuestProgress_Curr;
	public int intQuestProgress_Max;
	public int intQuestReward;

	public Image imageQuestPackageBack;

	public Color colorQuestWhite;
	public Color colorQuestYellow;

//	private Color colorBackBuffer;
	private bool isQuestComplete;
	private bool isQuestActive;

	void Start () {
//		isQuestComplete = false;
	}

	public void Quest_Setup (Quest whatQuest) {
		questThisPacakgeBuffer = whatQuest;

//		Debug.LogError ("questThisPacakgeBuffer DESCRIPT = " + questThisPacakgeBuffer.Description);

		Quest_GetManagerValues ();

		Quest_SetupProgress ();
		Quest_SetupReward ();
		Quest_SetupText ();

		Quest_IsActive ();

		// Sets the change to complete or not (Previously at the end of Start)
		Quest_IsCompleteQ ();
	}

	public void Quest_Activator (bool whichBool) {

		if (whichBool) {
			if (!animQuestPackHolder.isPlaying) {
				this.transform.localScale = Vector3.one;
			}
		} else {
			animQuestPackHolder.Stop ();
			this.transform.localScale = Vector3.zero;
		}

//		this.gameObject.SetActive (whichBool);
	}



	public void Quest_GetManagerValues () {
		intQuestProgress_Curr = questThisPacakgeBuffer.CurrentProgress;
		intQuestProgress_Max = questThisPacakgeBuffer.TargetProgress[questThisPacakgeBuffer.TargetProgressIndex];

		stringDescript = questThisPacakgeBuffer.Description + " " + intQuestProgress_Max;
		isQuestComplete = questThisPacakgeBuffer.Completed;
		intQuestReward = questThisPacakgeBuffer.CompletedReward;

		isQuestActive = questThisPacakgeBuffer.IsActiveToday;
	}

	public void Quest_SetupProgress () {
		textQuestProgressHolder.text = "(" + "<size=42><color=#00A500FF>" + intQuestProgress_Curr 
			+ "</color></size>" + " / " + "<size=42><color=#008000ff>" + intQuestProgress_Max + "</color></size>" + ")";
	}

	public void Quest_SetupText () {
		textQuestDescriptHolder.text = stringDescript;
	}

	public void Quest_SetupReward () {
		textQuestRewardsHolder.text = intQuestReward.ToString();
	}

	public void Quest_IsActive () {
		Quest_Activator (isQuestActive);
	}

	public void Quest_IsCompleteQ () {
		if (isQuestComplete) {
//			isQuestComplete = true;
			Quest_Complete ();
		} else {
//			isQuestComplete = false;
			Quest_NotComplete ();
		}
	}

	public void Quest_Complete () {
//		isQuestComplete = true;

		objQuestTickHolder.SetActive (true);
		objQuestRewardBoxFull.SetActive (true);

		imageQuestPackageBack.color = colorQuestYellow;
		objParticleQuestFlashHolder.SetActive (true);
	}

	public void Quest_NotComplete () {
//		isQuestComplete = false;

		objQuestTickHolder.SetActive (false);
		objQuestRewardBoxFull.SetActive (false);

		imageQuestPackageBack.color = colorQuestWhite;
		objParticleQuestFlashHolder.SetActive (false);
	}

	public void Pressed_QuestGainCheck () {
		if (isQuestComplete) {
			Quest_RewardNow ();
		} else {
			objToolTipQuest.SetActive (true);
		}
	}

	public void Quest_RewardNow () {
//		Debug.LogError ("Give Reward!");

		// Animate quest package for queue
		animQuestPackHolder.Play ();

		// Do this in case they were turned off
		questAllControllerHolder.MustacheAttribs_Activator (true);

		// Display mustache and GIVE it actually
		QuestManager.Instance.ClaimQuestReward (questThisPacakgeBuffer.QuestID);
		Quest_RewardMustacheShow ();

		// SOUNDD EFFECTS - Quest Claimed / Pressed
		if (SoundManager.Instance != null) {
			SoundManager.Instance.AchievementMenuClaimed.Play ();
		}
	}

	public void Quest_RewardMustacheShow () {
		mustacheCollectorHolder.MustacheCollector_GiveMeFromQuestAchieve (intQuestReward);

		// Removes mustache gain
		questAllControllerHolder.QuestFromAll_RewardRoutine ();

		// Turn this quest package off before setup
//		Quest_Activator (false);
	}
}
