﻿using UnityEngine;
using System.Collections;

public class Quest_AllController_Script : MonoBehaviour {

	public QuestPackage_Script[] questPackScriptArr;
	public Quest[] questRefArr;

	public Transform transQuestMustacheDisplay;

	public GameObject[] objStatsDisplayArr;

	[Header ("Mustache Display Stuff")]
	//	public Text textQuestMustacheDisplay;
//	public Mustache_Collector mustacheCollectorHolder;

	public Animation animMustacheMoverHolder;
	public ParticleSystem particleFullFlashHolder;
	public ParticleSystem particleMoveRaysHolder;

	void Awake () {
		QuestsAll_EnableDisabler (false);
	}

	void Update_OLD () {
		if (Input.GetKey ("q")) {
//			questPackScriptArr [1].Quest_Setup ();
		}

		if (Input.GetKey ("b")) {
			//			questPackScriptArr [1].Quest_Complete ();
			questRefArr[0].Completed = true;
			questRefArr [0].CurrentProgress = questRefArr [0].TargetProgress [questRefArr [0].TargetProgressIndex];
			QuestsAll_Setup ();
		}

		if (Input.GetKey ("n")) {
//			questPackScriptArr [1].Quest_Complete ();
			questRefArr[1].Completed = true;
			questRefArr [1].CurrentProgress = questRefArr [1].TargetProgress [questRefArr [1].TargetProgressIndex];
			QuestsAll_Setup ();
		}

		if (Input.GetKey ("m")) {
			//			questPackScriptArr [1].Quest_Complete ();
			questRefArr[2].Completed = true;
			questRefArr [2].CurrentProgress = questRefArr [2].TargetProgress [questRefArr [2].TargetProgressIndex];
			QuestsAll_Setup ();
		}

		if (Input.GetKey ("t")) {
			Time.timeScale = 1;
		}
	}

	public void QuestsAll_PhoneEnterSetup () {
		// Resize all packages to 1 in case it was in mid queue scale anim when last left here
		for (int i = 0; i < 3; i++) {
			questPackScriptArr [i].transform.localScale = Vector3.one;
		}

		// Enable disabled packages from awake
		QuestsAll_EnableDisabler (true);

		QuestsAll_Setup ();
	}

	public void QuestsAll_Activator (bool whichBool) {
		for (int i = 0; i < 3; i++) {
			questPackScriptArr [i].Quest_Activator (whichBool);
		}
	}

	public void QuestsAll_EnableDisabler (bool whichBool) {
		for (int i = 0; i < 3; i++) {
			questPackScriptArr [i].gameObject.SetActive (whichBool);
		}
	}

	public void QuestsAll_Setup () {
		QuestsAll_Activator (true);

		for (int i = 0; i < 3; i++) {
			questPackScriptArr [i].objToolTipQuest.SetActive (false);
		}

		// Resizes the mustache display to zero
		MustacheDisplayResize_Zero ();

//		Debug.LogError ("Setup ALL!");
		questRefArr = QuestManager.Instance.ActiveQuests;

		for (int i = 0; i < 3; i++) {
//			if (questRefArr [i].IsActiveToday) {

			questPackScriptArr [i].Quest_Setup (questRefArr[i]);

//			}
		}
	}

	public void QuestFromAll_RewardRoutine () {
		StartCoroutine (QuestFromAll_RewardNow ());
	}

	public IEnumerator QuestFromAll_RewardNow () {
		// Full flash for reward now / mustache gain
		particleFullFlashHolder.Play ();
		particleMoveRaysHolder.Play ();

		// Hide attribs / stats
		MustacheAttribs_Activator (false);

		// Setup quests again
//		Debug.LogError ("111111111111111");
		QuestsAll_Setup ();

		yield return new WaitForSeconds (1.2F);

		animMustacheMoverHolder.Play ("HUD Mustache DEATH Start Anim 1 (Legacy)");
//		Debug.LogError ("333333333333333");

		// Show attribs / stats
		yield return new WaitForSeconds (0.5F);
		MustacheAttribs_Activator (true);
	}

	public IEnumerator QuestFromAll_RewardNow_Part2 () {
		yield return new WaitForSeconds (0.5F);
	}

	public void MustacheDisplayResize_Zero () {
		transQuestMustacheDisplay.localScale = Vector3.zero;
	}

	public void MustacheAttribs_Activator (bool whichBool) {
		for (int i = 0; i < 4; i++) {
			objStatsDisplayArr [i].SetActive (whichBool);
		}
	}
}
