﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class Chest
{
    //wildcard yani ba yek ehtemali rare item dar miad
    //be onvan e mesal agar percent e wildcard 0.10 bashad 10% wildcard rare mishavad va 90% common mishavad
    private readonly int SILVER_CHEST_COMMON_AMOUNT = 3;
    private readonly int SILVER_CHEST_RARE_AMOUNT = 0;
    private readonly int SILVER_CHEST_WILDCARD_AMOUNT = 0;
    private readonly float SILVER_CHEST_WILDCARD_PERCENT = 0;

    private readonly int GOLD_CHEST_COMMON_AMOUNT = 3;
    private readonly int GOLD_CHEST_RARE_AMOUNT = 1;
    private readonly int GOLD_CHEST_WILDCARD_AMOUNT = 1;
    private readonly float GOLD_CHEST_WILDCARD_PERCENT = 0.10f;

    private readonly int UNIQUE_CHEST_COMMON_AMOUNT = 5;
    private readonly int UNIQUE_CHEST_RARE_AMOUNT = 1;
    private readonly int UNIQUE_CHEST_WILDCARD_AMOUNT = 1;
	private readonly float UNIQUE_CHEST_WILDCARD_PERCENT = 0.50f;
    private readonly float UNIQUE_CHEST_WILDCARD_HAPPEN_MINIMUM_BRACKET = 0.3f;
    private readonly float UNIQUE_CHEST_WILDCARD_HAPPEN_MAXIMUM_BRACKET = 3.0f;
	private readonly int UNIQUE_CHEST_UNIQUE_ITEMS_AMOUNT = 2;

    private readonly float ITEM_NEAR_COMPLETION_FIRST_INCREASE_AT_PERCENT = 0.60f;
    private readonly int ITEM_NEAR_COMPLETION_FIRST_INCREASE_ODDS_MULTIPLIER = 2;

    private readonly float ITEM_NEAR_COMPLETION_SECOND_INCREASE_AT_PERCENT = 0.80f;
    private readonly int ITEM_NEAR_COMPLETION_SECOND_INCREASE_ODDS_MULTIPLIER = 4;

    private ChestType chestType;
    private int commonAmount;
    private int rareAmount;
    private int surplusAmount;

    //ItemDatabase itemDb = (ItemDatabase)UnityEditor.AssetDatabase.LoadAssetAtPath(@"Assets/Database/itemDB.asset", typeof(ItemDatabase));

	// This causes an instant null when ctor of Chest is called (Even via chest.open)
	ItemDatabase itemDb = PlayerData_Main.Instance.player_ItemDataBase;
//	ItemDatabase itemDb = Resources.Load<ItemDatabase>(@"itemDB");

    List<Item> itemList;
    List<Item> tempItemListWithFixedOdds;

    public enum ChestType
    {
        Silver, Gold, Unique
    }
    /// <summary>
    /// Chest Class that can be Opened
    /// </summary>
    /// <param name="ct">type of chest using ChestType enum</param>
    public Chest(ChestType ct)
    {
//		Debug.LogError ("Yo 1");

        itemList = itemDb.GetList();
        tempItemListWithFixedOdds = FixItemListOdds();

//		Debug.LogError ("Yo 2");

        chestType = ct;
        switch (ct)
        {
            case ChestType.Silver:
                commonAmount = SILVER_CHEST_COMMON_AMOUNT;
                rareAmount = SILVER_CHEST_RARE_AMOUNT;
                AddWildcardsToItemPool(SILVER_CHEST_WILDCARD_AMOUNT, SILVER_CHEST_WILDCARD_PERCENT);
                break;
            case ChestType.Gold:
                commonAmount = GOLD_CHEST_COMMON_AMOUNT;
                rareAmount = GOLD_CHEST_RARE_AMOUNT;
                AddWildcardsToItemPool(GOLD_CHEST_WILDCARD_AMOUNT, GOLD_CHEST_WILDCARD_PERCENT);
                break;
            case ChestType.Unique:
                commonAmount = UNIQUE_CHEST_COMMON_AMOUNT;
                rareAmount = UNIQUE_CHEST_RARE_AMOUNT;
                AddWildcardsToItemPool(UNIQUE_CHEST_WILDCARD_AMOUNT, UNIQUE_CHEST_WILDCARD_PERCENT);
                break;
            default:
                break;
        }
    }

    private void AddWildcardsToItemPool(int wildcardAmount, float wildcardPercent)
    {
        //agar unique chest bood va kharej az baze taeen shode baraye wildcard
        //niaz be tasmim giri darad .. be in mana ke be player hal dade shavad
        float rareItemGatheredRatio = PlayerGatheredItemCount(Item.ItemTag.Rare);
        float commonItemGatheredRatio = PlayerGatheredItemCount(Item.ItemTag.Common);

        if (chestType == ChestType.Unique &&
            rareItemGatheredRatio != 0.0f &&
            (commonItemGatheredRatio / rareItemGatheredRatio) < UNIQUE_CHEST_WILDCARD_HAPPEN_MINIMUM_BRACKET)
        {
            commonAmount += wildcardAmount;
        }
        else if (chestType == ChestType.Unique &&
           rareItemGatheredRatio != 0.0f &&
           (commonItemGatheredRatio / rareItemGatheredRatio) > UNIQUE_CHEST_WILDCARD_HAPPEN_MAXIMUM_BRACKET)
        {
            rareAmount += wildcardAmount;
        }
        else
        {
            for (int i = 0; i < wildcardAmount; i++)
            {

                if (UnityEngine.Random.value <= wildcardPercent)
                {
                    rareAmount++;
                }
                else
                {
                    commonAmount++;
                }
            }
        }

        //hala ke tedad rare o common moshakhas shode 
        //baraye chest unique check shavad ke aya tedad kafi az item ha moonde 
        //agar namande ast be moteghayere surplus ezafe shavad ke badan bar mabnaye on tedad item rare random dade shavad
        if (chestType == ChestType.Unique)
        {
            int remainingRare = itemDb.totalNumberOfRareItems - itemDb.totalNumberOfCollectedRareItems;
            int remainingCommon = itemDb.totalNumberOfCommonItems - itemDb.totalNumberOfCollectedCommonItems;

            if (remainingRare < rareAmount && remainingCommon < commonAmount)
            {
                surplusAmount += commonAmount - remainingCommon;
                surplusAmount += rareAmount - remainingRare;
                commonAmount = remainingCommon;
                rareAmount = remainingRare;
            }
            else if (remainingRare < rareAmount && remainingCommon >= commonAmount)
            {
                if (commonAmount + (rareAmount - remainingRare) <= remainingCommon)
                {
                    commonAmount += rareAmount - remainingRare;
                    rareAmount = remainingRare;
                }
                else
                {
                    surplusAmount += rareAmount - (remainingCommon - commonAmount);
                    commonAmount = remainingCommon;
                    rareAmount = remainingRare;
                }
            }
            else if (remainingCommon < commonAmount && remainingRare >= rareAmount)
            {
                if (rareAmount + (commonAmount - remainingCommon) <= remainingRare)
                {
                    rareAmount += commonAmount - remainingCommon;
                    commonAmount = remainingCommon;
                }
                else
                {
                    surplusAmount += commonAmount - (remainingRare - rareAmount);
                    rareAmount = remainingRare;
                    commonAmount = remainingCommon;
                }
            }
        }
    }

    /// <summary>
    /// chand darsad az kole item haye bazi tavasote player gerefte shode ast
    /// </summary>
    /// <param name="Item Tag (Common or Rare)"></param>
    /// <returns>darsad takmil shode</returns>
    private float PlayerGatheredItemCount(Item.ItemTag itemTag)
    {
        float totalGathered = 0;
        float totalItems = 0;

        #region EzafeShodeBadAzLegendary
        if (itemTag == Item.ItemTag.Rare)
        {
			for (int i = 0; i < itemList.Count; i++) {
				if (itemList[i].itemTag == Item.ItemTag.Rare || itemList[i].itemTag == Item.ItemTag.Legendary)
				{
					totalGathered += itemList[i].currentAmount;
					totalItems += itemList[i].needsToComplete;
				}
			}
        }
        #endregion
        else
        {
			for (int i = 0; i < itemList.Count; i++) {
				if (itemList[i].itemTag == itemTag)
				{
					totalGathered += itemList[i].currentAmount;
					totalItems += itemList[i].needsToComplete;
				}
			}
        }
        return totalGathered / totalItems;
    }

    [Serializable]
    public class ChestItem
    {
        public int AbsoluteMustacheReward;
        public int MaxAmount;
        public int CurAmount;
        public bool JustGotCompleted;
        public Item Item;
        public bool IsConvertedToSibil;
    }

    /// <summary>
    /// Open The Initiated Chest
    /// </summary>
    /// <returns>A list of Items</returns>


	public List<ChestItem> Open()
	{
		bool justGotCompleted;
		List<ChestItem> chestItems = new List<ChestItem>();
		int itemNumber = 0;
		ChestItem chestItem = null;
		Item item = null;
		bool addOneMoreForUnique = false;

		int uniqueCap = UNIQUE_CHEST_UNIQUE_ITEMS_AMOUNT;

		//unique boodan shomareye kodom item haye chest unique
		// 2 adad random beine 0 ta 6 peyda kon

		UnityEngine.Random a = new UnityEngine.Random(); // replace from new Random(DateTime.Now.Ticks.GetHashCode());
		// Since similar code is done in default constructor internally
		List<int> randomList = new List<int>();

		// entekhab chand adad random unique
		int MyNumber = 0;
		while (randomList.Count < UNIQUE_CHEST_UNIQUE_ITEMS_AMOUNT)
		{
			MyNumber =  UnityEngine.Random.Range (0, UNIQUE_CHEST_COMMON_AMOUNT+UNIQUE_CHEST_RARE_AMOUNT+UNIQUE_CHEST_WILDCARD_AMOUNT);
			if (!randomList.Contains(MyNumber))
				randomList.Add(MyNumber);
		}
		foreach (var itemq in randomList)
		{
			Debug.Log( "unique dar index" + itemq);
		}

		int itemInChestCounter = 0;
		for (int j = 0; j < rareAmount; j++)
		{
			if (addOneMoreForUnique == true)
			{
				//item can not be null here
				item.Increase(out justGotCompleted);
				chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
				chestItems.Add(chestItem);

				if (item.availabilityState != Item.AvailabilityState.Complete)
				{
					addOneMoreForUnique = true;
					continue;
				}
				else
				{
					addOneMoreForUnique = false;
					continue;
				}
			}

			if (chestType == ChestType.Unique && uniqueCap >0 && randomList.Contains(itemInChestCounter))
			{
				Debug.LogError("unique at:" + itemInChestCounter);
				itemNumber = ChooseAUniqueRareItemFromList();
				uniqueCap--;
			}
			else
			{
				itemNumber = ChooseARareItemFromList();
			}
			//-1 yani hichi peyda nashod amma 0 yani avalin item
			if (itemNumber >= 0)
			{
				item = itemList[itemNumber];
				if (!item.Increase(out justGotCompleted))
				{
					chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
					chestItems.Add(chestItem);
					//Debug.Log("item kamel shode ast..ba sibil handle kon");
				}
				else
				{
					chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
					chestItems.Add(chestItem);
				}


				itemInChestCounter++;

				//if (chestType == ChestType.Unique && item.availabilityState != Item.AvailabilityState.Complete)
				//{
				//    addOneMoreForUnique = true;
				//}
			}
			else
			{
				Debug.LogError("no item found");
			}
		}

		addOneMoreForUnique = false;
		itemNumber = 0;
		item = null;
		for (int j = 0; j < commonAmount; j++)
		{
			if (addOneMoreForUnique == true)
			{
				item.Increase(out justGotCompleted);
				chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
				chestItems.Add(chestItem);

				if (item.availabilityState != Item.AvailabilityState.Complete)
				{
					addOneMoreForUnique = true;
					continue;
				}
				else
				{
					addOneMoreForUnique = false;
					continue;
				}
			}
			if (chestType == ChestType.Unique && uniqueCap >0 && randomList.Contains(itemInChestCounter))
			{
				Debug.LogError("unique at:" + itemInChestCounter);
				itemNumber = ChooseAUniqueCommonItemFromList();
				uniqueCap--;
			}
			else
			{
				itemNumber = ChooseACommonItemFromList();
			}
			//-1 yani hichi peyda nashod amma 0 yani avalin item
			if (itemNumber >= 0)
			{
				item = itemList[itemNumber];
				if (!item.Increase(out justGotCompleted))
				{
					chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
					chestItems.Add(chestItem);
					//Debug.Log("item kamel shode ast..ba sibil handle kon");
				}
				else
				{
					chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
					chestItems.Add(chestItem);
				}
				itemInChestCounter++;

				//if (chestType == ChestType.Unique && item.availabilityState != Item.AvailabilityState.Complete)
				//{
				//    addOneMoreForUnique = true;
				//}
			}
		}

		itemNumber = 0;
		item = null;
		//baraye item haye ezafi be soorat e random item rare dahad ke tabiatan be sibil tabdil mishavad
		for (int i = 0; i < surplusAmount; i++)
		{
			itemNumber = ChooseARareItemFromList();
			item = itemList[itemNumber];

			chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true, JustGotCompleted = false, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
			chestItems.Add(chestItem);
		}

		// Save after opening a random chest
		SaveLoad.Save();
		return chestItems;
	}

	#region oldChestOpen
//    public List<ChestItem> Open()
//    {
//        bool justGotCompleted;
//        List<ChestItem> chestItems = new List<ChestItem>();
//        int itemNumber = 0;
//        ChestItem chestItem = null;
//        Item item = null;
//        bool addOneMoreForUnique = false;
//        for (int j = 0; j < rareAmount; j++)
//        {
//            if (addOneMoreForUnique == true)
//            {
//                //item can not be null here
//                item.Increase(out justGotCompleted);
//                chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//                chestItems.Add(chestItem);
//
//                if (item.availabilityState != Item.AvailabilityState.Complete)
//                {
//                    addOneMoreForUnique = true;
//                    continue;
//                }
//                else
//                {
//                    addOneMoreForUnique = false;
//                    continue;
//                }
//            }
//
//            if (chestType == ChestType.Unique)
//            {
//                itemNumber = ChooseAUniqueRareItemFromList();
//            }
//            else
//            {
//                itemNumber = ChooseARareItemFromList();
//            }
//            //-1 yani hichi peyda nashod amma 0 yani avalin item
//            if (itemNumber >= 0)
//            {
//                item = itemList[itemNumber];
//                if (!item.Increase(out justGotCompleted))
//                {
//                    chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true , JustGotCompleted = justGotCompleted ,CurAmount = item.currentAmount , MaxAmount = item.needsToComplete};
//                    chestItems.Add(chestItem);
//                    //Debug.Log("item kamel shode ast..ba sibil handle kon");
//                }
//                else
//                {
//                    chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false , JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//                    chestItems.Add(chestItem);
//                }
//
//                if (chestType == ChestType.Unique && item.availabilityState != Item.AvailabilityState.Complete)
//                {
//                    addOneMoreForUnique = true;
//                }
//            }
//        }
//
//        addOneMoreForUnique = false;
//        itemNumber = 0;
//        item = null;
//
//        for (int j = 0; j < commonAmount; j++)
//        {
//            if (addOneMoreForUnique == true)
//            {
//                item.Increase(out justGotCompleted);
//                chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//                chestItems.Add(chestItem);
//
//                if (item.availabilityState != Item.AvailabilityState.Complete)
//                {
//                    addOneMoreForUnique = true;
//                    continue;
//                }
//                else
//                {
//                    addOneMoreForUnique = false;
//                    continue;
//                }
//            }
//            if (chestType == ChestType.Unique)
//            {
//                itemNumber = ChooseAUniqueCommonItemFromList();
//            }
//            else
//            {
//                itemNumber = ChooseACommonItemFromList();
//            }
//            //-1 yani hichi peyda nashod amma 0 yani avalin item
//            if (itemNumber >= 0)
//            {
//                item = itemList[itemNumber];
//                if (!item.Increase(out justGotCompleted))
//                {
//                    chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//                    chestItems.Add(chestItem);
//                    //Debug.Log("item kamel shode ast..ba sibil handle kon");
//                }
//                else
//                {
//                    chestItem = new ChestItem() { Item = item, IsConvertedToSibil = false, JustGotCompleted = justGotCompleted, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//                    chestItems.Add(chestItem);
//                }
//
//                if (chestType == ChestType.Unique && item.availabilityState != Item.AvailabilityState.Complete)
//                {
//                    addOneMoreForUnique = true;
//                }
//            }
//        }
//
//        itemNumber = 0;
//        item = null;
//        //baraye item haye ezafi be soorat e random item rare dahad ke tabiatan be sibil tabdil mishavad
//        for (int i = 0; i < surplusAmount; i++)
//        {
//            itemNumber = ChooseARareItemFromList();
//            item = itemList[itemNumber];
//
//            chestItem = new ChestItem() { Item = item, IsConvertedToSibil = true,JustGotCompleted = false, CurAmount = item.currentAmount, MaxAmount = item.needsToComplete };
//            chestItems.Add(chestItem);
//        }
//
//		// Save after opening a random chest
//        SaveLoad.Save();
//        return chestItems;
//    }
	#endregion

    public List<ChestItem> OpenFixed(int[] itemIds)
    {
        List<ChestItem> chestItems = new List<ChestItem>();
        bool justGotCompleted;

        foreach (var itemId in itemIds)
        {
            ChestItem chestItem = new ChestItem();
            Item item = itemList.Find(x => x.itemId == itemId);
            chestItem.Item = item;
            if (!item.Increase(out justGotCompleted))
            {
                chestItem.IsConvertedToSibil = true;
            }
            chestItem.JustGotCompleted = justGotCompleted;
            chestItem.CurAmount = item.currentAmount;
            chestItem.MaxAmount = item.needsToComplete;
            chestItems.Add(chestItem);
        }

		// Save after opening a fixed chest
        SaveLoad.Save();
        return chestItems;
    }

    //ghavanin unique chest:
    //1- agar hame item haye common tamom shode bood(ya item haye rare tamam shode bood)
    //be jaye item common item unique rare dade shavad
    //2- agar hame item ha kamel shode bood item rare random bedahad
    //3- hame item ha tarjihan yek shekl bashad..masalan agar 3 item common 2 item rare bayad dade shavad
    // 3 item common yek shekl mesle ab havij bastani va 2 item rare yek shekl mese dizi sangi dade shavad
    // agar itemi kamel shod dar in proce item unique digari az haman jens dade shavad

    private int ChooseAUniqueCommonItemFromList()
    {
        int fullOdds = 0;
        //az bein item hayee ke rare hastand yeki be sorat random entekhab konad

        //majmooe odd ha ro be dast biar
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].availabilityState != Item.AvailabilityState.Complete && itemList[k].itemTag == Item.ItemTag.Common)
            {
                fullOdds += tempItemListWithFixedOdds[k].oddsOfHappening;
            }
        }

        //addad random koochiktar az fullodd bedast biar
        int randomNum = UnityEngine.Random.Range(0, fullOdds);

        int count = 0;
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].availabilityState != Item.AvailabilityState.Complete && itemList[k].itemTag == Item.ItemTag.Common)
            {
                count += tempItemListWithFixedOdds[k].oddsOfHappening;
                if (randomNum < count)
                {
                    return k;
                }
            }
        }

        return -1;
    }
    private int ChooseAUniqueRareItemFromList()
    {


        int fullOdds = 0;
        //az bein item hayee ke rare hastand yeki be sorat random entekhab konad

        //majmooe odd ha ro be dast biar
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].availabilityState != Item.AvailabilityState.Complete && (itemList[k].itemTag == Item.ItemTag.Rare || itemList[k].itemTag == Item.ItemTag.Legendary))
            {
                fullOdds += tempItemListWithFixedOdds[k].oddsOfHappening;
            }
        }

        //addad random koochiktar az fullodd bedast biar
        int randomNum = UnityEngine.Random.Range(0, fullOdds);

        int count = 0;
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].availabilityState != Item.AvailabilityState.Complete && (itemList[k].itemTag == Item.ItemTag.Rare || itemList[k].itemTag == Item.ItemTag.Legendary))
            {
                count += tempItemListWithFixedOdds[k].oddsOfHappening;
                if (randomNum < count)
                {
                    return k;
                }
            }
        }

        return -1;
    }
    private int ChooseARareItemFromList()
    {
        int fullOdds = 0;
        //az bein item hayee ke rare hastand yeki be sorat random entekhab konad

        //majmooe odd ha ro be dast biar
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].itemTag == Item.ItemTag.Rare || itemList[k].itemTag == Item.ItemTag.Legendary)
            {
                fullOdds += tempItemListWithFixedOdds[k].oddsOfHappening;
            }
        }

        //addad random koochiktar az fullodd bedast biar
        int randomNum = UnityEngine.Random.Range(0, fullOdds);

        int count = 0;
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].itemTag == Item.ItemTag.Rare || itemList[k].itemTag == Item.ItemTag.Legendary)
            {
                count += tempItemListWithFixedOdds[k].oddsOfHappening;
                if (randomNum < count)
                {
                    return k;
                }
            }
        }

        return -1;
    }
    private int ChooseACommonItemFromList()
    {
        int fullOdds = 0;
        //az bein item hayee ke rare hastand yeki be sorat random entekhab konad

        //majmooe odd ha ro be dast biar
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].itemTag == Item.ItemTag.Common)
            {
                fullOdds += tempItemListWithFixedOdds[k].oddsOfHappening;
            }
        }

        //addad random koochiktar az fullodd bedast biar
        int randomNum = UnityEngine.Random.Range(0, fullOdds);

        int count = 0;
        for (int k = 0; k < itemList.Count; k++)
        {
            if (itemList[k].itemTag == Item.ItemTag.Common)
            {
                count += tempItemListWithFixedOdds[k].oddsOfHappening;
                if (randomNum < count)
                {
                    return k;
                }
            }
        }
        return -1;
    }

    /// <summary>
    /// Changes ItemList oddOfHappening according to items completed number.The more item is near completion the higher its odds become.
    /// </summary>
    /// <param name="itemList"></param>
    /// <returns></returns>
    private List<Item> FixItemListOdds()
    {
        List<Item> tempList = new List<Item>();

		for (int i = 0; i < itemList.Count; i++) {
			var tempItem = new Item(
				itemList[i].itemTag,
				itemList[i].itemId,
				itemList[i].name,
				itemList[i].oddsOfHappening,
				itemList[i].needsToComplete,
				itemList[i].farsiName,
				itemList[i].description,
				itemList[i].itemEffectDescription);
			//tempItem.currentAmount = item.currentAmount;
			tempList.Add(tempItem);
		}

        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].availabilityState != Item.AvailabilityState.Complete)
            {
                //aval bebin ke chand darsad item kamel shode
                float itemCompletedPercent = itemList[i].currentAmount / (float)itemList[i].needsToComplete;
                //agar az dovomin darsad bozorgtar bood be mizan dovomin meghdar odds item ro afzayeshesh bede
                //masalan agar az 80% bishtar bood 3 barabar esh kon
                if (itemCompletedPercent >= ITEM_NEAR_COMPLETION_SECOND_INCREASE_AT_PERCENT)
                {
                    Debug.LogWarning("odd second increase happened for: " + itemList[i].name);
                    tempList[i].oddsOfHappening *= ITEM_NEAR_COMPLETION_SECOND_INCREASE_ODDS_MULTIPLIER;
                }
                //dar gheire in soorat az az avalin meghdar bozorg tar bood
                // be mizan avalin meghdar afzayesh bede
                else if (itemCompletedPercent >= ITEM_NEAR_COMPLETION_FIRST_INCREASE_AT_PERCENT)
                {
                    Debug.LogWarning("odd first increase happened for: " + itemList[i].name);
                    tempList[i].oddsOfHappening *= ITEM_NEAR_COMPLETION_FIRST_INCREASE_ODDS_MULTIPLIER;
                }
            }
        }

        return tempList;
    }
}

