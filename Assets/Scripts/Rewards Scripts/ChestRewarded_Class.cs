﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ChestRewarded_Class  {

    //TODO: make private
	public Reward_Machine_Script.ChestArtType thisChest_ChestType;
	public List<Chest.ChestItem> itemsForChestArr;// rename to itemsForChestList? need to instantiate?

	/*
    public ChestRewarded_Class[] chestRewardsArr;

    ItemDatabase itemDb = Resources.Load<ItemDatabase>(@"itemDB");

    /// <summary>
    /// constructor baraye safhe upgrade - az chestRewardArr[0] estefade shavad
    /// </summary>
    /// <param name="chestArtType"> general nabayad bashe chestarttype</param>
    public ChestRewarded_Class(Reward_Machine_Script.ChestArtType chestArtType)
    {
        chestRewardsArr = new ChestRewarded_Class[0];
        switch (chestArtType)
        {
            case Reward_Machine_Script.ChestArtType.Koochool:
                chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
                chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Silver).Open();
                break;
            case Reward_Machine_Script.ChestArtType.Aghdasi:
                chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
                chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Gold).Open();
                break;
            case Reward_Machine_Script.ChestArtType.Moshtan:
                chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
                chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Unique).Open();
                break;
            case Reward_Machine_Script.ChestArtType.General:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// constructor baraye chesthaye akhar marhale - az chestRewardArr kamel estefade shavad
    /// </summary>
    /// <param name="levelRewadData"> az manifest khoonde mishavad</param>
    public ChestRewarded_Class(LevelManifestRewardData levelRewadData)
    {
        int chestCount=0;
        if (levelRewadData.Mustache.Length>0 || levelRewadData.Items.Length>0)
        {
            chestCount++;
        }
        if (levelRewadData.RandomChests.Length>0)
        {
            chestCount+= levelRewadData.RandomChests.Length;
        }
        if (levelRewadData.FixedChests.Length > 0)
        {
            chestCount += levelRewadData.FixedChests.Length;
        }

        chestRewardsArr = new ChestRewarded_Class[chestCount];

        int chestNumber = 0;
        //chest general - shamel sibil o item haye moshakhas dar editor
        if (levelRewadData.Mustache.Length > 0 || levelRewadData.Items.Length > 0)
        {
            chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.General;
            if (levelRewadData.Mustache.Length > 0)
            {
                foreach (var mustache in levelRewadData.Mustache)
                {
                    chestRewardsArr[chestNumber].itemsForChestArr.Add(new Chest.ChestItem() { AbsoluteMustacheReward = mustache });
                }
            }
            if (levelRewadData.Items.Length > 0)
            {
                chestRewardsArr[chestNumber].itemsForChestArr = new Chest(Chest.ChestType.Silver).OpenFixed(levelRewadData.Items);
                //bool justGotCompleted;

                //foreach (var itemId in levelRewadData.Items)
                //{
                //    Chest.ChestItem chestItem = new Chest.ChestItem();
                //    Item item = itemDb.FindItemByItemId(itemId);
                //    chestItem.Item = item;
                //    if (!item.Increase(out justGotCompleted))
                //    {
                //        chestItem.IsConvertedToSibil = true;
                //    }
                //    chestItem.JustGotCompleted = justGotCompleted;
                //    chestItem.CurAmount = item.currentAmount;
                //    chestItem.MaxAmount = item.needsToComplete;

                //    chestRewardsArr[chestNumber].itemsForChestArr.Add(chestItem);
                //}       
            }
            chestNumber++;
        }
        //chest random
        if (levelRewadData.RandomChests.Length > 0)
        {
            foreach (var chestType in levelRewadData.RandomChests)
            {
                switch (chestType)
                {
                    case Chest.ChestType.Silver:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
                        break;
                    case Chest.ChestType.Gold:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
                        break;
                    case Chest.ChestType.Unique:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
                        break;
                    default:
                        break;
                }
                chestNumber++;
            }
        }
        //chest fixed - ke item ha dar editor moshakhas shode and
        if (levelRewadData.FixedChests.Length > 0)
        {
            foreach (var chestType in levelRewadData.FixedChests)
            {
                switch (chestType.type)
                {
                    case Chest.ChestType.Silver:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
                        break;
                    case Chest.ChestType.Gold:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
                        break;
                    case Chest.ChestType.Unique:
                        chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
                        chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
                        break;
                    default:
                        break;
                }
                chestNumber++;
            }
        }


    }
    */
}

