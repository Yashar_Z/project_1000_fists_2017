﻿using UnityEngine;
//using System.Linq;
using System.Collections;

[System.Serializable]
public class Item
{
    public enum ItemTag
    {
        Common, Rare, Legendary
    }
    [SerializeField]
    public ItemTag itemTag;

    [SerializeField]
    public int itemId;

    [SerializeField]
    public string name;

    [SerializeField]
    public int oddsOfHappening;

    [SerializeField]
    public string farsiName;

    [SerializeField]
    public string description;

    [SerializeField]
    public string itemEffectDescription;

	[SerializeField]
	public bool isTwoLine;

	[SerializeField]
	public bool isTwoLineOneEffect;

	[Header ("Amounts")]
	[SerializeField]
	public int needsToComplete;

	public ItemWithAmount itemByID;

    public int currentAmount
    {
        get
        {
            if (PlayerData_Main.Instance != null)
            {
				return ItemWithAmount.FindItemById (itemId).CurAmount;
            }
            else
            {
                return 0;
            }
            
        }

    }

	public bool exclamationState {
		get
		{
			if (PlayerData_Main.Instance != null)
			{
				return ItemWithAmount.FindItemById (itemId).itemExclamationState;
			}
			else
			{
				return true;
			}

		}
	}

    //[HideInInspector]
    //public int currentAmount;
    public Item(ItemTag it, int id, string nm, int odds, int max, string fn, string desc,string shrtDesc)
    {
        itemTag = it;
        itemId = id;
        name = nm;
        oddsOfHappening = odds;
        needsToComplete = max;
        farsiName = fn;
        description = desc;
        itemEffectDescription = shrtDesc;
    }

    public enum AvailabilityState
    {
        Empty,
        Incomplete,
        Complete
    }

    public AvailabilityState availabilityState
    {
        get
        {
            if (currentAmount <= 0)
            {
                return AvailabilityState.Empty;
            }
            else if (currentAmount < needsToComplete)
            {
                return AvailabilityState.Incomplete;
            }
            else
            {
                return AvailabilityState.Complete;
            }

        }
    }

    public bool Increase(out bool JustGotCompleted)
    {
		itemByID = ItemWithAmount.FindItemById (itemId);
        if (availabilityState != AvailabilityState.Complete)
        {
			
			itemByID.CurAmount++;
			itemByID.availabityState = this.availabilityState;
            //currentAmount++;
            Debug.Log(name + "  increased to " + currentAmount);
            if (availabilityState == AvailabilityState.Complete)
            {
                JustGotCompleted = true;
            }
            else
            {
                JustGotCompleted = false;
            }
            
            return true;
        }
        else
        {
            Debug.LogError(name + " cant be increased");
            JustGotCompleted = false;
            return false;
        }
    }

}
