﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class ItemDatabase : ScriptableObject
{
    [SerializeField]
    private List<Item> database;

    void OnEnable()
    {
        if (database == null)
            database = new List<Item>();
    }

    public void Add(Item item)
    {
        database.Add(item);
    }

    public void Remove(Item item)
    {
        database.Remove(item);
    }

    public void RemoveAt(int index)
    {
        database.RemoveAt(index);
    }

    public int COUNT
    {
        get { return database.Count; }
    }

    public Item ItemAtIndex(int index)
    {
        return database.ElementAt(index);
    }

    public Item FindItemByItemId(int itemId)
    {
        return database.First(x => x.itemId == itemId);
    }

    public Item ItemWithName(string n)
    {
        return database.First(x => x.name == n);
    }

    public void SortAlphabeticallyAtoZ()
    {
        database.Sort((x, y) => string.Compare(x.name, y.name));
    }

    public void SortByItemId()
    {
        database.Sort((x, y) => (x.itemId).CompareTo(y.itemId));
    }

    public List<Item> GetList()
    {
        return database;
    }

    public int totalNumberOfCommonItems
    {
        get
        {
            int temp = 0;
			for (int i = 0; i < database.Count; i++) {
				if (database[i].itemTag == Item.ItemTag.Common)
				{
					temp += database[i].needsToComplete;
				}
			}
            return temp;
        }
    }
    public int totalNumberOfCollectedCommonItems
    {
        get
        {
            int temp = 0;
			for (int i = 0; i < database.Count; i++) {
				if (database[i].itemTag == Item.ItemTag.Common)
				{
					temp += database[i].currentAmount;
				}
			}

            return temp;
        }
    }
    public int totalNumberOfRareItems
    {
        get
        {
            int temp = 0;
			for (int i = 0; i < database.Count; i++) {
				if (database[i].itemTag == Item.ItemTag.Rare)
				{
					temp += database[i].needsToComplete;
				}
			}
				
            return temp;
        }
    }
    public int totalNumberOfCollectedRareItems
    {
        get
        {
            int temp = 0;

			for (int i = 0; i < database.Count; i++) {
				if (database[i].itemTag == Item.ItemTag.Rare)
				{
					temp += database[i].currentAmount;
				}
			}

            return temp;
        }
    }
}