﻿using UnityEngine;
using System.Collections;

public class ItemEffects_Script : MonoBehaviour {

	private readonly float ITEM_EFFECT_101_LUCKY_EXPLODE = 5;
	// 102 : PowerUp_TimeManager_Script For outside game (Pow Ups 1 and 2)
	private readonly int ITEM_EFFECT_103_HP_INCREASE = 3;
	private readonly int ITEM_EFFECT_104_CRIT_CHANCE_INC = 7;
	private readonly int ITEM_EFFECT_105_SHIELD_CHANCE_INC = 12;
	private readonly int ITEM_EFFECT_106_KILL_BONUS = 1;
	private readonly int ITEM_EFFECT_107_HP_INCREASE = 2;
	private readonly int ITEM_EFFECT_108_HP_INCREASE = 12;
	private readonly int ITEM_EFFECT_109_HP_INCREASE = 2;
	private readonly int ITEM_EFFECT_110_HP_INCREASE = 3;
	private readonly int ITEM_EFFECT_111_MUSTACHE_MULT = 20;
	private readonly int ITEM_EFFECT_112_CRIT_CHANCE_INC = 4;
	private readonly int ITEM_EFFECT_113_HP_INCREASE = 3;
	private readonly int ITEM_EFFECT_113_DMG_INCREASE = 1;
	private readonly int ITEM_EFFECT_114_HP_INCREASE = 2;
	private readonly int ITEM_EFFECT_115_DMG_INCREASE = 1;
	private readonly int ITEM_EFFECT_116_HP_INCREASE = 3;
	private readonly int ITEM_EFFECT_116_DMG_INCREASE = 1;
	// 117 is last stand shout
	// 118 : PowerUp_TimeManager_Script - For outside game (Pow Ups 1)
	// 119 : PowerUp_TimeManager_Script - For outside game (Pow Ups 2)
	private readonly int ITEM_EFFECT_120_CRIT_CHANCE_INC = 8;
	private readonly int ITEM_EFFECT_121_MUSTACHE_MULT = 50;
	private readonly int ITEM_EFFECT_122_SHIELD_CHANCE_INC = 6;
	private readonly int ITEM_EFFECT_123_DMG_INCREASE = 1;
	private readonly int ITEM_EFFECT_124_DMG_INCREASE = 1;
	// 125 for change the hedgehog to item (// TO DO: Much later)
	private readonly int ITEM_EFFECT_125_ANTI_HEDGEHOG = 40;
	private readonly int ITEM_EFFECT_126_HP_INCREASE = 2; 
	private readonly int ITEM_EFFECT_126_CRIT_CHANCE_INC = 4; 
	private readonly int ITEM_EFFECT_127_POWUP2_DURATION_INC = 3; 
	private readonly int ITEM_EFFECT_128_DMG_INCREASE = 1;
	// 129 is pick-up item value +1
	private readonly int ITEM_EFFECT_130_CRIT_CHANCE_INC = 8;
	private readonly int ITEM_EFFECT_130_DMG_INCREASE = 1;
	private readonly int ITEM_EFFECT_131_DMG_INCREASE = 2;
	private readonly int ITEM_EFFECT_132_MUSTACHE_MULT = 10;
	private readonly int ITEM_EFFECT_133_ANTI_HF = 40;
	private readonly int ITEM_EFFECT_134_KILL_BONUS = 2;
	private readonly int ITEM_EFFECT_135_POWUP2_DURATION_INC = 2; 
	private readonly float ITEM_EFFECT_136_LUCKY_EXPLODE = 2;
	// 137 is last stand shout

	[Header ("Start Values")]
	public float itemEffect_LuckyExplodeChance;
	public int itemEffect_ItemValueIncrease;
	public int itemEffect_isAntiHF_Sometimes;
	public int itemEffect_isAntiHedgehog_Sometimes;
	public int itemEffect_HP_Start;
	public int itemEffect_HP_Curr;
	public int itemEffect_CritChance;
	public int itemEffect_ShieldChanceTotal;
	public int itemEffect_KIllBonus_HealAmount;
	public float itemEffect_MustacheMultiplier_Cur;
	public int itemEffect_Damage_Base;
	public int itemEffect_LastStandShoutCount;
	public float itemEffect_PowUp2Duration;

	public void ItemEffecst_PreLoad () {
		itemEffect_HP_Start = PlayerData_Main.Instance.player_HP;
		itemEffect_HP_Curr = PlayerData_Main.Instance.player_HP;
		itemEffect_Damage_Base = PlayerData_Main.Instance.player_Damage; 
		itemEffect_CritChance = PlayerData_Main.Instance.player_CritChance;
		itemEffect_ShieldChanceTotal = PlayerData_Main.Instance.player_ShieldChance;

		itemEffect_PowUp2Duration = PlayerData_Main.Instance.player_PowUp2Duration;


		// Base / One value for start of mustache multiplier
		itemEffect_MustacheMultiplier_Cur = 1;

		// Base / Zero value for lucky explode chance
		itemEffect_LuckyExplodeChance = 0;

		// Base for pickup item value increase 
		itemEffect_ItemValueIncrease = 0;

		// Base for last stand
		itemEffect_LastStandShoutCount = 0;

		// False for chance of anti-HF
		itemEffect_isAntiHF_Sometimes = 0;

		// False for chance of anti-Hedgehog
		itemEffect_isAntiHedgehog_Sometimes = 0;
	}

	public void ItemsEffects_Check () {
//		Debug.LogError ("This IS HP = " + itemEffect_HP_Curr + " and " + itemEffect_HP_Start);

		for (int i = 0; i < PlayerData_Main.Instance.player_ItemsAmount.Count; i++) {
			if (PlayerData_Main.Instance.player_ItemsAmount[i].availabityState == Item.AvailabilityState.Complete) {
				switch (PlayerData_Main.Instance.player_ItemsAmount[i].ItemID) {
				case 101:
					itemEffect_LuckyExplodeChance += ITEM_EFFECT_101_LUCKY_EXPLODE;
					Debug.LogWarning ("itemEffect_LuckyExplodeChance = " + itemEffect_LuckyExplodeChance);
					break;

				case 102:
					break;

				case 103:
					itemEffect_HP_Start += ITEM_EFFECT_103_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 104:
					itemEffect_CritChance += ITEM_EFFECT_104_CRIT_CHANCE_INC;
					break;

				case 105:
					itemEffect_ShieldChanceTotal += ITEM_EFFECT_105_SHIELD_CHANCE_INC;
					break;

				case 106:
					itemEffect_KIllBonus_HealAmount += ITEM_EFFECT_106_KILL_BONUS;
					break;

				case 107:
					itemEffect_HP_Start += ITEM_EFFECT_107_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 108:
					itemEffect_HP_Start += ITEM_EFFECT_108_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 109:
					itemEffect_HP_Start += ITEM_EFFECT_109_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 110:
					itemEffect_HP_Start += ITEM_EFFECT_110_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 111:
					itemEffect_MustacheMultiplier_Cur += ((float)ITEM_EFFECT_111_MUSTACHE_MULT) / 100F;
					break;

				case 112:
					itemEffect_CritChance += ITEM_EFFECT_112_CRIT_CHANCE_INC;
					break;

				case 113:
					itemEffect_HP_Start += ITEM_EFFECT_113_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					itemEffect_Damage_Base += ITEM_EFFECT_113_DMG_INCREASE;
					break;

				case 114:
					itemEffect_HP_Start += ITEM_EFFECT_114_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					break;

				case 115:
					itemEffect_Damage_Base += ITEM_EFFECT_115_DMG_INCREASE;
					break;

				case 116:
					itemEffect_HP_Start += ITEM_EFFECT_116_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					itemEffect_Damage_Base += ITEM_EFFECT_116_DMG_INCREASE;
					break;

				case 117:
					itemEffect_LastStandShoutCount++;
					break;

				case 118:
					break;

				case 119:
					break;

				case 120:
					itemEffect_CritChance += ITEM_EFFECT_120_CRIT_CHANCE_INC;
					break;

				case 121:
					itemEffect_MustacheMultiplier_Cur += ((float)ITEM_EFFECT_121_MUSTACHE_MULT) / 100F;
					break;

				case 122:
					itemEffect_ShieldChanceTotal += ITEM_EFFECT_122_SHIELD_CHANCE_INC;
					break;

				case 123:
					itemEffect_Damage_Base += ITEM_EFFECT_123_DMG_INCREASE;
					break;

				case 124:
					itemEffect_Damage_Base += ITEM_EFFECT_124_DMG_INCREASE;
					break;

				case 125:
					itemEffect_isAntiHedgehog_Sometimes += ITEM_EFFECT_125_ANTI_HEDGEHOG;
					break;

				case 126:
					itemEffect_HP_Start += ITEM_EFFECT_126_HP_INCREASE;
					itemEffect_HP_Curr = itemEffect_HP_Start;
					itemEffect_CritChance += ITEM_EFFECT_126_CRIT_CHANCE_INC;
					break;

				case 127:
					itemEffect_PowUp2Duration += ITEM_EFFECT_127_POWUP2_DURATION_INC;
					break;

				case 128:
					itemEffect_Damage_Base += ITEM_EFFECT_128_DMG_INCREASE;
					break;

				case 129:
					// TODO: Increase Pickup Item's effectiveness
					itemEffect_ItemValueIncrease += 1;
					break;

				case 130:
					itemEffect_CritChance += ITEM_EFFECT_130_CRIT_CHANCE_INC;
					itemEffect_Damage_Base += ITEM_EFFECT_130_DMG_INCREASE;
					break;

				case 131:
					itemEffect_Damage_Base += ITEM_EFFECT_131_DMG_INCREASE;
					break;

				case 132:
					itemEffect_MustacheMultiplier_Cur += ((float)ITEM_EFFECT_132_MUSTACHE_MULT) / 100F;
					break;

				case 133:
					itemEffect_isAntiHF_Sometimes += ITEM_EFFECT_133_ANTI_HF;
					break;

				case 134:
					itemEffect_KIllBonus_HealAmount += ITEM_EFFECT_134_KILL_BONUS;
					break;

				case 135:
					itemEffect_PowUp2Duration += ITEM_EFFECT_135_POWUP2_DURATION_INC;
					break;

				case 136:
					itemEffect_LuckyExplodeChance += ITEM_EFFECT_136_LUCKY_EXPLODE;
					break;

				case 137:
					itemEffect_LastStandShoutCount++;
					break;

				default:
					break;
				}
			}
		}
//		Debug.LogError ("This should be HP = " + itemEffect_HP_Curr + " and " + itemEffect_HP_Start);
	}
}
