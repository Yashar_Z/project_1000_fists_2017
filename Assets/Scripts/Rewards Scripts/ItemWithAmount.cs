﻿using UnityEngine;
using System.Collections;
using System.Linq;

[System.Serializable]
public class ItemWithAmount {
	public int ItemID;
	public int CurAmount;

	public Item.AvailabilityState availabityState;
	public Item.AvailabilityState lastAvailablityState;
	public bool itemExclamationState;

	public static ItemWithAmount FindItemById(int itemId ){
		return PlayerData_Main.Instance.player_ItemsAmount.First (i => i.ItemID == itemId);
	}
}
