﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Item_Display_Script : MonoBehaviour {

	public bool isForDisplay;
	public bool isForPhone;
	public int displayID;

	public Item thisItem;
	public Chest.ChestItem thisChestItem; 

	public int thisItemID;
	public Sprite[] itemSpriteArr;
	public Color[] colorItemCircleArr;

	[Header("")]
	public Sprite[] itemRarirtySpriteArr_Common;
	public Sprite[] itemRarirtySpriteArr_Rare;
	public Sprite[] itemRarirtySpriteArr_Legendary;

	[Header("")]
	public GameObject itemFrontObj_Empty;
	public GameObject itemFrontObj_Incomplete;
	public GameObject itemFrontObj_Complete;

	public GameObject itemCircleShineParentObjHolder;
	public GameObject itemCircleShineObj_Common;
	public GameObject itemCircleShineObj_Rare;
	public GameObject itemCircleShineObj_Legendary;

	public Image spriteItemActive;
	public Image spriteItemCircleActive;

	public Image spriteItemCompleteness_Empty;
	public Image spriteItemCompleteness_Incomplete;
	public Image spriteItemCompleteness_Complete;

	[Header("Display Only")]
	public Animation anim_ItemDisplayHolder;
	public Transform transItemShadowsParentHolder;
	public Text text_ItemCounterHolder;
	public int intItemDisplayCount_Curr;
	public int intItemDisplayCount_Req;

	public Item_Menu_Script itemMenuScriptHolder;

	[Header("Rewards Only")]
	public int int_ItemToMustacheBuffer;
	public Reward_Machine_Script rewardsMachineScriptHolder;
	//	public ParticleSystem particleItemToMustacheHolder;

	[SerializeField]
	private Color colorItemBackActive;
	private Sprite[] itemRarity_ActiveArr;
	private int itemID;
	private Item.ItemTag itemTag;
	private Item.AvailabilityState itemAvailablity;

	private Transform transItemMover;
	private bool canDisplayExclamation;
	private bool isSortOrdered;

	private ItemWithAmount itemWithAmountBuffer;

//	private ItemDatabase itemDb = Resources.Load<ItemDatabase>(@"itemDB");
//	private List<Item> itemList;

	void Start () {
		isSortOrdered = false;
//		itemList = itemDb.GetList();
//		itemList.Find (x => x.itemId == thisItemID

//		if (!isForPhone) {
//			SetupAll ();
//		}
	}


	public void SetupRewardOnly () {
		Debug.LogError ("Item ID = " + itemID);
		Item_Setup_Clean ();
		Item_Setup ();

		Item_Setup_MainArt ();
		Item_Setup_Rarity ();

		// Check to see if availablity state has changed
		Item_Complete_ExclamationCheck ();

		Item_Setup_RewardNow ();
	}

	public void SetupAll () {
		Item_Setup_Clean ();
		Item_Setup ();

		Item_Setup_MainArt ();
		Item_Setup_Rarity ();

		Item_Setup_Completeness ();
	}

	public void SetupAll_VidEggsOnly (Item whichItem) {
		isForDisplay = false;
		thisItem = whichItem;

		Item_Setup_Clean ();
		Item_Setup ();

		Item_Setup_MainArt ();
		Item_Setup_Rarity ();

		// No completeness for this
//		Item_Setup_Completeness ();

		Item_Setup_BeComplete ();
	}

	public void SetupAll_PhoneOnly () {
		isForDisplay = false;
		SetupAll ();
	}

	public void Item_Setup () {
		
		if (isForDisplay) {
//			itemMenuScriptHolder = GetComponentInParent <Item_Menu_Script> ();

//			Debug.LogError ("ITEMMENUSCRIP HOLDER NAME = " + itemMenuScriptHolder.name);
			thisItem = itemMenuScriptHolder.itemDB.FindItemByItemId (displayID);
			intItemDisplayCount_Curr = thisItem.currentAmount;
			intItemDisplayCount_Req = thisItem.needsToComplete;

			// Get Trans of Item Mover
			transItemMover = transItemShadowsParentHolder.parent;

			// Idle Anim
			StartCoroutine (itemDisplay_IdleAnim ((thisItem.itemId - 80) / 8F));
//			Invoke ("itemDisplay_IdleAnim", (thisItem.itemId - 80) / 8F);
		}

		itemID = thisItem.itemId;
		itemTag = thisItem.itemTag;
		itemAvailablity = thisItem.availabilityState;
	}

	public void Item_Setup_Clean () {

		// Clean rarity items
		itemFrontObj_Empty.SetActive (false);
		itemFrontObj_Incomplete.SetActive (false);
		itemFrontObj_Complete.SetActive (false);

		// Clean common shines
		itemCircleShineParentObjHolder.SetActive (false);
		itemCircleShineObj_Common.SetActive (false);
		itemCircleShineObj_Rare.SetActive (false);
		itemCircleShineObj_Legendary.SetActive (false);
	}

	public void Item_Setup_MainArt () {
		switch (itemID) {
		case 101:
			spriteItemActive.sprite = itemSpriteArr [0];
			break;
		case 102:
			spriteItemActive.sprite = itemSpriteArr [1];
			break;
		case 103:
			spriteItemActive.sprite = itemSpriteArr [2];
			break;
		case 104:
			spriteItemActive.sprite = itemSpriteArr [3];
			break;
		case 105:
			spriteItemActive.sprite = itemSpriteArr [4];
			break;
		case 106:
			spriteItemActive.sprite = itemSpriteArr [5];
			break;
		case 107:
			spriteItemActive.sprite = itemSpriteArr [6];
			break;
		case 108:
			spriteItemActive.sprite = itemSpriteArr [7];
			break;
		case 109:
			spriteItemActive.sprite = itemSpriteArr [8];
			break;
		case 110:
			spriteItemActive.sprite = itemSpriteArr [9];
			break;
		case 111:
			spriteItemActive.sprite = itemSpriteArr [10];
			break;
		case 112:
			spriteItemActive.sprite = itemSpriteArr [11];
			break;
		case 113:
			spriteItemActive.sprite = itemSpriteArr [12];
			break;
		case 114:
			spriteItemActive.sprite = itemSpriteArr [13];
			break;
		case 115:
			spriteItemActive.sprite = itemSpriteArr [14];
			break;
		case 116:
			spriteItemActive.sprite = itemSpriteArr [15];
			break;
		case 117:
			spriteItemActive.sprite = itemSpriteArr [16];
			break;
		case 118:
			spriteItemActive.sprite = itemSpriteArr [17];
			break;
		case 119:
			spriteItemActive.sprite = itemSpriteArr [18];
			break;
		case 120:
			spriteItemActive.sprite = itemSpriteArr [19];
			break;
		case 121:
			spriteItemActive.sprite = itemSpriteArr [20];
			break;
		case 122:
			spriteItemActive.sprite = itemSpriteArr [21];
			break;
		case 123:
			spriteItemActive.sprite = itemSpriteArr [22];
			break;
		case 124:
			spriteItemActive.sprite = itemSpriteArr [23];
			break;
		case 125:
			spriteItemActive.sprite = itemSpriteArr [24];
			break;
		case 126:
			spriteItemActive.sprite = itemSpriteArr [25];
			break;
		case 127:
			spriteItemActive.sprite = itemSpriteArr [26];
			break;
		case 128:
			spriteItemActive.sprite = itemSpriteArr [27];
			break;
		case 129:
			spriteItemActive.sprite = itemSpriteArr [28];
			break;
		case 130:
			spriteItemActive.sprite = itemSpriteArr [29];
			break;
		case 131:
			spriteItemActive.sprite = itemSpriteArr [30];
			break;
		case 132:
			spriteItemActive.sprite = itemSpriteArr [31];
			break;
		case 133:
			spriteItemActive.sprite = itemSpriteArr [32];
			break;
		case 134:
			spriteItemActive.sprite = itemSpriteArr [33];
			break;
		case 135:
			spriteItemActive.sprite = itemSpriteArr [34];
			break;
		case 136:
			spriteItemActive.sprite = itemSpriteArr [35];
			break;
		case 137:
			spriteItemActive.sprite = itemSpriteArr [36];
			break;
		default:
			spriteItemActive.sprite = itemSpriteArr [0];
			break;
		}
	}

	public void Item_Setup_Rarity () {
		switch (itemTag) {
		case Item.ItemTag.Common:
			colorItemBackActive = colorItemCircleArr [0];
			itemRarity_ActiveArr = itemRarirtySpriteArr_Common;
			itemCircleShineObj_Common.SetActive (true);

			// Set value for item to mustache conversion
			if (!isForDisplay && !isForPhone) {
				int_ItemToMustacheBuffer = Item_Values.Instance.ItemToMustacheValue_Common;
			}
			break;
		case Item.ItemTag.Rare:
			colorItemBackActive = colorItemCircleArr [1];
			itemRarity_ActiveArr = itemRarirtySpriteArr_Rare;
			itemCircleShineObj_Rare.SetActive (true);

			// Set value for item to mustache conversion
			if (!isForDisplay && !isForPhone) {
				int_ItemToMustacheBuffer = Item_Values.Instance.ItemToMustacheValue_Rare;
			}
			break;
		case Item.ItemTag.Legendary:
			colorItemBackActive = colorItemCircleArr [2];
			itemRarity_ActiveArr = itemRarirtySpriteArr_Legendary;
			itemCircleShineObj_Legendary.SetActive (true);

			// Set value for item to mustache conversion
			if (!isForDisplay && !isForPhone) {
				int_ItemToMustacheBuffer = Item_Values.Instance.ItemToMustacheValue_Legendary;
			}
			break;
		default:
			colorItemBackActive = colorItemCircleArr [0];
			itemRarity_ActiveArr = itemRarirtySpriteArr_Common;
			itemCircleShineObj_Common.SetActive (true);

			// Set value for item to mustache conversion
			if (!isForDisplay && !isForPhone) {
				int_ItemToMustacheBuffer = Item_Values.Instance.ItemToMustacheValue_Common;
			}
			break;
		}

		// Circle Color
		spriteItemCircleActive.color = colorItemBackActive;

		// Getting rarity sprites for completeness states
		spriteItemCompleteness_Empty.sprite = itemRarity_ActiveArr [0];
		spriteItemCompleteness_Incomplete.sprite  = itemRarity_ActiveArr [0];
		spriteItemCompleteness_Complete.sprite  = itemRarity_ActiveArr [1];
	}

	public void Item_Setup_BeComplete () {
		itemFrontObj_Empty.SetActive (false);
		itemFrontObj_Incomplete.SetActive (false);
		itemFrontObj_Complete.SetActive (true);
	}

	public void Item_Setup_Completeness () {

		switch (itemAvailablity) {
		case Item.AvailabilityState.Empty:
//			Debug.LogError ("EMPTY");
			itemFrontObj_Empty.SetActive (true);
			itemFrontObj_Incomplete.SetActive (false);
			itemFrontObj_Complete.SetActive (false);

			// To turn the black front of empty item
			if (isForDisplay) {
				spriteItemActive.transform.GetChild (0).gameObject.SetActive (true);
				ItemDisplay_Reposition (0);
			}
			break;
		case Item.AvailabilityState.Incomplete:
//			Debug.LogError ("INCOMPLETE");
			itemFrontObj_Empty.SetActive (false);
			itemFrontObj_Incomplete.SetActive (true);
			itemFrontObj_Complete.SetActive (false);

			// Activate circle shine parent
			itemCircleShineParentObjHolder.SetActive (true);

			// To remove  the black front of non-empty item
			if (isForDisplay) {
				spriteItemActive.transform.GetChild (0).gameObject.SetActive (false);
				ItemDisplay_Reposition (1);
			}

			break;
		case Item.AvailabilityState.Complete:
//			Debug.LogError ("COMPLETE");
			itemFrontObj_Empty.SetActive (false);
			itemFrontObj_Incomplete.SetActive (false);
			itemFrontObj_Complete.SetActive (true);

			// To remove  the black front of non-empty item
			if (isForDisplay) {
				spriteItemActive.transform.GetChild (0).gameObject.SetActive (false);
				ItemDisplay_Reposition (2);
			}

			break;
		default:
			itemFrontObj_Empty.SetActive (true);
			itemFrontObj_Incomplete.SetActive (false);
			itemFrontObj_Complete.SetActive (false);

			// To remove  the black front of non-empty item
			if (isForDisplay) {
				spriteItemActive.transform.GetChild (0).gameObject.SetActive (false);
			}

			break;
		}

		// Display exclamation in display mode
		if (isForDisplay) {
			itemWithAmountBuffer = ItemWithAmount.FindItemById (itemID);
			if (itemWithAmountBuffer.itemExclamationState) {
				ItemDisplay_ExclamationMark (true);

				// Sort Test
//				Debug.LogWarning ("Before Sort Check");
//				if (itemAvailablity == Item.AvailabilityState.Complete) {
//					isSortOrdered = true;
//				}
			}
		}
	}

	public void Item_SortCompleteToFront () {
		Debug.LogWarning ("After Sort Check AND isSortOrdered = " + isSortOrdered);
		if (isSortOrdered) {
			transform.SetSiblingIndex (0);
		}
	}

	// Check to see if availablity state has changed
	public void Item_Complete_ExclamationCheck () {
		if (!isForDisplay) {

			itemWithAmountBuffer = ItemWithAmount.FindItemById (itemID);
//			Debug.LogWarning ("current state of : " + itemWithAmountBuffer.ItemID + " is: " + itemWithAmountBuffer.lastAvailablityState);
//			Debug.LogWarning ("new state of " + itemWithAmountBuffer.ItemID + " is: " + itemAvailablity);

			if (itemWithAmountBuffer.lastAvailablityState != itemAvailablity) {
				itemWithAmountBuffer.lastAvailablityState = itemAvailablity;
				itemWithAmountBuffer.itemExclamationState = true;
				//				ItemDisplay_ExclamationMark (true);
			}
		}
	}

	public void Item_Setup_RewardNow () {
		// Activate circle shine parent
		itemCircleShineParentObjHolder.SetActive (true);

		itemFrontObj_Empty.SetActive (false);
		itemFrontObj_Incomplete.SetActive (true);
		itemFrontObj_Complete.SetActive (false);
	}

	public void Item_CompleteTransform () {
		rewardsMachineScriptHolder.Item_CompleteTransform_Particles ();

		// DeActivate circle shine parent
		itemCircleShineParentObjHolder.SetActive (false);

		itemFrontObj_Empty.SetActive (false);
		itemFrontObj_Incomplete.SetActive (false);
		itemFrontObj_Complete.SetActive (true);

		Invoke ("Item_CompleteTextAnim", 0.1F);
	}

	public void Item_CompleteTextAnim () {
		// Item Name Animation for moment of completion
		rewardsMachineScriptHolder.ItemTexts_Animate (4);
	}

	public void Item_CompleteIdle () {
//		Item_PlayCompleteIdle ();
	}

	public void Item_PlayCompleteIdle () {
//		rewardsMachineScriptHolder.ItemSingleAnim_CompleteIdle ();
//		Debug.LogWarning ("YAY!?")
		this.GetComponent<Animation> ().Stop ();
		this.GetComponent<Animation> ().Play ("Egg Item - Completed Idle Anim 1 (Legacy)");
	}

	public void ItemToMustache_ParticleNLogic () {
//		particleItemToMustacheHolder.Play ();
//		rewardsMachineScriptHolder.ItemReward_Mustache_Shoot (int_ItemToMustacheBuffer);
	}

	public void ItemDisplay_Reposition (int shadowNumber) {
		switch (shadowNumber) {
		case 0:
			transItemMover.localPosition = new Vector3 (0, 25, 0);

			// Black in front of item counter
			transItemShadowsParentHolder.GetChild (3).gameObject.SetActive (true);

			// Disable Select Button
			transItemShadowsParentHolder.GetChild (4).gameObject.SetActive (false);

			ItemDisplay_ShowNumbers ();
			break;

		case 1:
			// Comp / incomp shadows
			transItemShadowsParentHolder.GetChild (0).gameObject.SetActive (true);
			transItemShadowsParentHolder.GetChild (1).gameObject.SetActive (false);

			// Black in front of item counter
			transItemShadowsParentHolder.GetChild (3).gameObject.SetActive (false);
			transItemMover.localPosition = new Vector3 (0, 25, 0);

			// Enable Select Button
			transItemShadowsParentHolder.GetChild (4).gameObject.SetActive (true);

			ItemDisplay_ShowNumbers ();
			break;

		case 2:
			// Comp / incomp shadows
			transItemShadowsParentHolder.GetChild (0).gameObject.SetActive (false);
			transItemShadowsParentHolder.GetChild (1).gameObject.SetActive (true);

			transItemMover.localPosition = Vector3.zero;
			transItemShadowsParentHolder.GetChild (2).gameObject.SetActive (false);
			transItemShadowsParentHolder.GetChild (3).gameObject.SetActive (false);

			// Enable Select Button
			transItemShadowsParentHolder.GetChild (4).gameObject.SetActive (true);
			break;

	default:
			break;
		}
	}

	public void ItemDisplay_ShowNumbers () {
		text_ItemCounterHolder.text = intItemDisplayCount_Curr.ToString() + " <color=red>/</color> " + intItemDisplayCount_Req.ToString();
	}

	public void ItemDisplay_ExclamationMark (bool isActive) {
		transItemMover.GetChild (4).gameObject.SetActive (isActive);

		if (isActive) {
			// Add exclamation and change count
			NotificationCenter.Instance.intExclaimationCount++;
		} else {
			// Remove exclamation and change count
			NotificationCenter.Instance.intExclaimationCount--;
		}
	}

	public IEnumerator itemDisplay_IdleAnim (float delay) {
		yield return new WaitForSeconds (delay);

		anim_ItemDisplayHolder.Play ();
	}

	public void Pressed_ItemDisplayToPhone () {
		itemMenuScriptHolder.Setup_Phone (thisItem, 2, false);
		itemMenuScriptHolder.itemNumber_ForPhoneNext = transform.GetSiblingIndex ();
		Debug.LogWarning ("Index = " + itemMenuScriptHolder.itemNumber_ForPhoneNext);

		// Remove exclamation by clicking
		if (itemWithAmountBuffer.itemExclamationState) {
			itemWithAmountBuffer.itemExclamationState = false;
			ItemDisplay_ExclamationMark (false);
		}

		// Update notifs number for items
		Phone_Stats_Script.Instance.NotifStats_UpdateItems ();
	}

	public void ExclamationRemove () {
		itemWithAmountBuffer = ItemWithAmount.FindItemById (itemID);
		if (itemWithAmountBuffer.itemExclamationState) {
			itemWithAmountBuffer.itemExclamationState = false;
			ItemDisplay_ExclamationMark (false);
		}

		// Update notifs number for items
		Phone_Stats_Script.Instance.NotifStats_UpdateItems ();
	}

	public void Pressed_ItemDisplayNextButton () {
		ExclamationRemove ();
		itemMenuScriptHolder.Setup_Phone (thisItem, 2, true);
		itemMenuScriptHolder.itemNumber_ForPhoneNext = transform.GetSiblingIndex ();
		Debug.LogWarning ("Index = " + itemMenuScriptHolder.itemNumber_ForPhoneNext);
	}
}