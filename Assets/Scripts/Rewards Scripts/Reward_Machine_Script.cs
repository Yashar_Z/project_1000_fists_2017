﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Reward_Machine_Script : MonoBehaviour {

	public static System.Action OnExclamationGain;

	[Header("Chest Reward Array")]
	//public ChestRewarded_Class[] chestRewardsArr;
	public List<ChestRewarded_Class> chestRewardsArr;

	[Header("")]
	public Canvas canvasRewardsHolder;
	public GraphicRaycaster graphicRayRewardsHolder;

	public Sprite[] spriteEggFullArr;
	public Sprite[] spriteEggBroken_TopArr;
	public Sprite[] spriteEggBroken_BottomArr;

	public enum ChestArtType {
		Koochool = 0,
		Aghdasi,
		Moshtan,
		General
	}

	[Header("Item Text Info Elements")]
	public Text itemText_Name;
	public Text itemText_Amount_CurrentNMax;
	public Text itemText_Effect;
	public Text itemText_Description;

	public Transform transItemTextsParentHolder;
	public Color[] colorItemRarityTextsArr;
	public Color[] colorItemAmountTextArr;

	public ParticleSystem particleItemTextNameHolder;
	public ParticleSystem particleItemTextAmountHolder;
	public ParticleSystem particleItemTextEffectHolder;

	[Header("")]
	public Image image_FullEgg_Main;
	public Image image_BrokenEgg_Top;
	public Image image_BrokenEgg_Bottom;

	public Animation animRewardMachineHolder;
	public Animation animSpotlightHolder;
	public Animation animRewardGradsHolder;
	public Animation animItemSingleHolder;
	public Animation animItemTextsHolder;
	public Animation animMustacheMoverHolder;

	public Animator animatorRewardGradSingleHolder;

	public GameObject eggLightsObjHolder;
	public GameObject eggItemObjHolder;
	public GameObject itemMoverObjHolder;
	public GameObject eggFistImageHolder;
	public GameObject objMustacheMoverParentHolder;

	public Transform transMustacheMoverHolder;

	[Header("Item Animation")]
	public AnimationClip[] animClipItemSingleArr;
	public AnimationClip[] animClipItemTextsArr;

	public ParticleSystem[] eggParticlesArr;
	public ParticleSystem[] particleEggItemShowHolder;
	public ParticleSystem[] particleItemCompleteTransformArr;

	public Item_Display_Script itemDisplayScriptHolder;
	public Camera_InGame_Script camInGameScriptHolder;
	public ParticleSystem particleItemToMustacheHolder;

	[Header("Mustache Script & Stuff")]
	public GameObject mustacheParentObjHolder;
	public Mustache_Script mustacheScriptHolder;
	public End_Level_Script endLevelScriptHolder;
	public Menu_TutCheck_Script menuTutCheckScriptHolder;

	// ONLY for menu scene
	[Header("ONLY For Menu Scene")]
	public Item_Menu_Script itemMenuScriptHolder;

	[Header("")]
	public AnimationClip[] animClipsRewardMachineArr;
	public bool isChestTouchAllowed;
	public bool isDuringChestAnim;
	public bool isItemGettingCompleted;

	public bool isDontUpdateItems;

	public Text text_Mustache_Holder_RewardsPage;
	public RectTransform reward_MustacheHolderRecTransHolder;

	private Sprite SpriteEggFull_Active;
	private Sprite SpriteEggBroken_Top_Active;
	private Sprite SpriteEggBroken_Bottom_Active;

	private int chestNumberBuffer;
	private int chestCounter;
	private int chestCapacity;
	private int itemCounter;
	private int mustacheValue;

	private Chest.ChestItem chestItemBuffer;
	private Chest.ChestItem chestItemBufferForAchieve;

	private bool itemConvertIsDone;
	private bool isRewardMustache;
	private bool isItemTurnedToMustache;

	private bool isMustacheDisplayIn;

	// For convas deactivator
	private bool isRewardsStarted;

	private Vector2 rewards_MustachePositVect2;
	private Vector3 v3_mustacheItemScaleBuffer;
	private GameObject instantiated_mustacheBuffer;

	private ItemDatabase itemDb ;
//	private Item_Menu_Script itemMenuScriptRef;

	void Awake () {
		this.gameObject.SetActive (false);
		chestCounter = 0;
		itemCounter = 0;
		isRewardMustache = false;
	}

	public void Pressed_ItemsClear () {
		for (int i = 0; i < PlayerData_Main.Instance.player_ItemsAmount.Count; i++) {
			PlayerData_Main.Instance.player_ItemsAmount[i].CurAmount = 0;
			PlayerData_Main.Instance.player_ItemsAmount[i].availabityState = Item.AvailabilityState.Empty;
		}
	}
		
	/// <summary>
	/// constructor baraye safhe upgrade - az chestRewardArr[0] estefade shavad
	/// </summary>
	/// <param name="chestArtType"> general nabayad bashe chestarttype</param>
	public void GetReward_InMenu (Reward_Machine_Script.ChestArtType chestArtType)
	{
		switch (chestArtType)
		{
		case Reward_Machine_Script.ChestArtType.Koochool:
			chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
			chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Silver).Open();
			break;
		case Reward_Machine_Script.ChestArtType.Aghdasi:
			chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
			chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Gold).Open();
			break;
		case Reward_Machine_Script.ChestArtType.Moshtan:
			chestRewardsArr[0].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
			chestRewardsArr[0].itemsForChestArr = new Chest(Chest.ChestType.Unique).Open();
			break;
		case Reward_Machine_Script.ChestArtType.General:
			break;
		default:
			break;
		}
	}

	/// <summary>
	/// constructor baraye chesthaye akhar marhale - az chestRewardArr kamel estefade shavad
	/// </summary>
	/// <param name="levelRewadData"> az manifest khoonde mishavad</param>
	public void GetReward_InGame(LevelManifestRewardData levelRewadData)
	{
		int chestCount=0;
		if (levelRewadData.Mustache.Length>0 || levelRewadData.Items.Length>0)
		{
			chestCount++;
		}
		if (levelRewadData.RandomChests.Length>0)
		{
			chestCount+= levelRewadData.RandomChests.Length;
		}
		if (levelRewadData.FixedChests.Length > 0)
		{
			chestCount += levelRewadData.FixedChests.Length;
		}

		chestRewardsArr = new List<ChestRewarded_Class>();

		for (int i = 0; i < chestCount; i++) {
			chestRewardsArr.Add (new ChestRewarded_Class {
				itemsForChestArr = new System.Collections.Generic.List<Chest.ChestItem> (),
				thisChest_ChestType = ChestArtType.Aghdasi
			});
		}

		int chestNumber = 0;
		//chest general - shamel sibil o item haye moshakhas dar editor
		if (levelRewadData.Mustache.Length > 0 || levelRewadData.Items.Length > 0)
		{
			chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.General;

//			var x = new Chest (Chest.ChestType.Silver).Open () [0];
//			chestRewardsArr[chestNumber].itemsForChestArr.Add(x ) ;

			if (levelRewadData.Mustache.Length > 0)
			{
				foreach (var mustache in levelRewadData.Mustache)
				{
					chestRewardsArr[chestNumber].itemsForChestArr.Add(new Chest.ChestItem() { AbsoluteMustacheReward = mustache });
				}
			}
			if (levelRewadData.Items.Length > 0)
			{
				chestRewardsArr[chestNumber].itemsForChestArr.AddRange(new Chest(Chest.ChestType.Silver).OpenFixed(levelRewadData.Items));


				//bool justGotCompleted;

				//foreach (var itemId in levelRewadData.Items)
				//{
				//    Chest.ChestItem chestItem = new Chest.ChestItem();
				//    Item item = itemDb.FindItemByItemId(itemId);
				//    chestItem.Item = item;
				//    if (!item.Increase(out justGotCompleted))
				//    {
				//        chestItem.IsConvertedToSibil = true;
				//    }
				//    chestItem.JustGotCompleted = justGotCompleted;
				//    chestItem.CurAmount = item.currentAmount;
				//    chestItem.MaxAmount = item.needsToComplete;

				//    chestRewardsArr[chestNumber].itemsForChestArr.Add(chestItem);
				//}       
			}	

			chestNumber++;
		}

		//chest random
		if (levelRewadData.RandomChests.Length > 0)
		{
			foreach (var chestType in levelRewadData.RandomChests)
			{
				switch (chestType)
				{
				case Chest.ChestType.Silver:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
					break;
				case Chest.ChestType.Gold:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
//					var x = new Chest(Chest.ChestType.Gold).Open();
					break;
				case Chest.ChestType.Unique:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType).Open();
					break;
				default:
					break;
				}
				chestNumber++;
			}
		}

		//chest fixed - ke item ha dar editor moshakhas shode and
		if (levelRewadData.FixedChests.Length > 0)
		{
			foreach (var chestType in levelRewadData.FixedChests)
			{
				switch (chestType.type)
				{
				case Chest.ChestType.Silver:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Koochool;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
					break;
				case Chest.ChestType.Gold:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Aghdasi;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
					break;
				case Chest.ChestType.Unique:
					chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.Moshtan;
					chestRewardsArr[chestNumber].itemsForChestArr = new Chest(chestType.type).OpenFixed(chestType.FixedItems);
					break;
				default:
					break;
				}
				chestNumber++;
			}
		}
	}

	public void GetReward_DailyRewardFull (int mustacheDaily) {
		int[] mustacheDailyArr = new int[1];
		mustacheDailyArr [0] = mustacheDaily;

//		int[] itemDailyArr = new int[1];
//		itemDailyArr [0] = 120;

		LevelManifestRewardData levelManifestForReward = new LevelManifestRewardData () {
			FixedChests = new FixedChest[0],
			Items = new int[0],

//			Items = itemDailyArr,

			Mustache = mustacheDailyArr,
			RandomChests = new Chest.ChestType[0]
			
		};

		GetReward_InGame (levelManifestForReward);
	}

	public void GetReward_VidGiftItem (int intItemRewardID) {
		// Create array to pass the ID to openFixed
		int[] intItemRewardIDArr;
		intItemRewardIDArr = new int[1];
		intItemRewardIDArr [0] = intItemRewardID;

		chestRewardsArr = new List<ChestRewarded_Class>();

		chestRewardsArr.Add (new ChestRewarded_Class {
			itemsForChestArr = new System.Collections.Generic.List<Chest.ChestItem> (),
			thisChest_ChestType = ChestArtType.Aghdasi
		});

		int chestNumber = 0;

		chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.General;
		chestRewardsArr[chestNumber].itemsForChestArr.AddRange(new Chest(Chest.ChestType.Silver).OpenFixed(intItemRewardIDArr)); 
	}

	public void GetReward_DailyRewardShort (int mustacheDaily) {
		chestRewardsArr = new List<ChestRewarded_Class>();

		chestRewardsArr.Add (new ChestRewarded_Class {
			itemsForChestArr = new System.Collections.Generic.List<Chest.ChestItem> (),
			thisChest_ChestType = ChestArtType.Aghdasi
		});

		int chestNumber = 0;
		//chest general - shamel sibil o item haye moshakhas dar editor
		if (true)
		{
			chestRewardsArr[chestNumber].thisChest_ChestType = Reward_Machine_Script.ChestArtType.General;
			chestRewardsArr[chestNumber].itemsForChestArr.Add(new Chest.ChestItem() { AbsoluteMustacheReward = mustacheDaily });

//			chestNumber++;
		}
	}

	void Start () {
//		CallFirstChest ();
	}

	void Rewards_Preset () {
		v3_mustacheItemScaleBuffer = mustacheParentObjHolder.transform.localScale;
	}

	void OnEnable () {
		if (PlayerData_InGame.Instance != null) {
			// Prevent gameplay gestures after reward machine arrives
			PlayerData_InGame.Instance.gameplayGestureAllowed = false;

			// Set layers of mustache
//			PlayerData_InGame.Instance.Mustache_SortingOrderTo (5);

			// Pause the game
			EventTrigger.PauseTheGame ();
		}
		ItemSingleAnim_Reset ();
		Rewards_Preset ();

		itemConvertIsDone = true;
		isChestTouchAllowed = false;
		isDuringChestAnim = false;
		isMustacheDisplayIn = false;
		eggLightsObjHolder.SetActive (false);
		eggItemObjHolder.SetActive (false);

		animSpotlightHolder.Play ("Egg Spotlight - Enter Anim 1 (Legacy)");

		// Instead of 4 grads, use one animator
//		animRewardGradsHolder.Play ("Egg Reward Grads Start Anim 1 (Legacy)");
		animatorRewardGradSingleHolder.gameObject.SetActive (true);
//		animatorRewardGradSingleHolder.Play ("Egg Reward Grad SINGLE Enter Anim 1 (Legacy)");

		// Move this to center
		this.transform.localPosition = Vector3.zero;

		// Item Texts Disable
		ItemTexts_Animate (3);
	}

	void RewardMustache_Enter () {
		isMustacheDisplayIn = true;
		animMustacheMoverHolder.Play ("HUD Mustache DEATH Return Anim 1 (Legacy)");
	}

	IEnumerator RewardMustache_Leave (float delay) {
		yield return new WaitForSeconds (delay);

		isMustacheDisplayIn = false;
		animMustacheMoverHolder.Play ("HUD Mustache DEATH Start Anim 1 (Legacy)");

		// Deactivate rewards mustache display (NEW PLACE)
		Invoke ("RewardMustache_DeActivate", 0.4F);
	}

	void RewardMustache_Activate () {
		transMustacheMoverHolder.localScale = Vector3.zero;
		objMustacheMoverParentHolder.SetActive (true);
//		Debug.Log ("Active");
	}

	void RewardMustache_DeActivate () {
		transMustacheMoverHolder.localScale = Vector3.zero;
		objMustacheMoverParentHolder.SetActive (false);
//		Debug.Log ("DeActive");
	}

	void OnDisable () {
//		Invoke ("RewardMustache_Leave", 1);
//		RewardMustache_Leave ();

		if (PlayerData_InGame.Instance != null) {
			// Set layers of mustache
//			PlayerData_InGame.Instance.Mustache_SortingOrderTo (2);
		}
	}

	public void SelectChest () {
		animItemSingleHolder.Stop ();
//		Debug.LogError ("REWARD chestcounter = " + chestCounter);
		Setup_ActiveChest (chestRewardsArr[chestCounter].thisChest_ChestType);

//		Debug.LogError ("BEFORE");
//		yield return new WaitForSeconds (waitFor);	
//		Debug.LogError ("AFTER");

		// Animate egg's start
		if (chestCounter == 0) {
			EggAnimate (0);
		} else {
			EggAnimate (0);
		}

        // SOUNDD EFFECTS - nozoole tokhme morgh (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.NozooleTokhmeMorgh.Play();
        }

		// Set capacity
		chestCapacity = chestRewardsArr[chestCounter].itemsForChestArr.Count;
	}

	void Setup_ActiveChest (ChestArtType whatType) {
//		Debug.LogError ("CHEST TYPE NUMBER = " + (int)whatType);
		chestNumberBuffer = (int)whatType;

		// Setup Chest Parts
		SetEggsArt_Full (chestNumberBuffer);
		SetEggsArt_Top (chestNumberBuffer);
		SetEggsArt_Bottom (chestNumberBuffer);

		image_FullEgg_Main.sprite = SpriteEggFull_Active;
		image_BrokenEgg_Top.sprite = SpriteEggBroken_Top_Active;
		image_BrokenEgg_Bottom.sprite = SpriteEggBroken_Bottom_Active;
	}

	public void SetEggsArt_Full (int whichArt) {
		SpriteEggFull_Active = spriteEggFullArr[whichArt];
	}

	public void SetEggsArt_Top (int whichArt) {
		SpriteEggBroken_Top_Active = spriteEggBroken_TopArr[whichArt];
	}

	public void SetEggsArt_Bottom (int whichArt) {
		SpriteEggBroken_Bottom_Active = spriteEggBroken_BottomArr[whichArt];
	}

	public void Rewards_Update_Mustache (int newMustache) {
		rewards_MustachePositVect2.x = 50 + newMustache.ToString ().Length * 13;
		rewards_MustachePositVect2.y = 41;
		reward_MustacheHolderRecTransHolder.sizeDelta = rewards_MustachePositVect2;
		//		menu_MustacheHolderRecTransHolder_ShopMenu.sizeDelta = menu_MustachePositVect2;

		text_Mustache_Holder_RewardsPage.text = newMustache.ToString();
		//		text_Mustache_Holder_ShopPage.text = newMustache.ToString();
	}

	public void CallFirstChest () {
		
		// Call Reward mustache (old place)
//		RewardMustache_Activate ();
//		RewardMustache_Enter ();

//		Invoke ("RewardMustache_Enter", 4);

		// Activate canvas for Start of rewards
		isRewardsStarted = true;
		StartCoroutine (RewardsCanvas_Activator (true));

        // SOUNDD EFFECTS - Reward Machine Start (done) (NEEDS 0.75 Delay)
        if (MusicManager.Instance!= null)
        {
            MusicManager.Instance.PlayMusic_Chests();
        }

		Invoke ("SelectChest", 0.75F);
	}

	public void CallChest_Check () {
		// Check to see if has gone above last chest or not
		if (chestCounter < chestRewardsArr.Count) {
			Invoke ("CallChest_Accept", 0.55F);
		} else {
			// Play leave anim for rewards mustache display
			if (isItemTurnedToMustache || isRewardMustache) {
				// Later because last reward had mustache
				StartCoroutine (RewardMustache_Leave (0.5F));

//				Debug.LogError ("LATEEEEEEEEEEEEEEEER");
			} else {
				// Instant because there was no mustache
				StartCoroutine (RewardMustache_Leave (0));

//				Debug.LogError ("INSTAAAAAAAAAAAAAANT");
			}

			// Check to make sure we ARE in menu
            if (PlayerData_InGame.Instance == null)
            {
                if (MusicManager.Instance != null)
                {
                    MusicManager.Instance.PlayMusic_Menu();
                }

				// Endless Unlock Tut - Only check in case we are in menu and the canCheck is true
				if (menuTutCheckScriptHolder.canCheckForEndlessTut) {
					menuTutCheckScriptHolder.canCheckForEndlessTut = false;

					// New check for endless and event
					itemMenuScriptHolder.menuMainScriptHolder.AfterDailyCheck_EndlessAndEvent ();

					// Old check for endless and event
//					menuTutCheckScriptHolder.Menu_TutorialCheck_EndlessIntro ();
//
//					// Event Check
//					itemMenuScriptHolder.menuMainScriptHolder.Event_Check ();
				}
            }

			// Deactivate rewards mustache display (OLD PLACE)
//			Invoke ("RewardMustache_DeActivate", 0.4F);

			Invoke ("CallChest_End", 0.1F);
		}
	}

	public void CallChest_Accept () {
		SelectChest ();
		ItemSingleAnim_Reset ();
	}

	public void CallChest_End () {
		Debug.LogError ("End of Rewards");

		// To make sure alarm / vibration is activated after end of items menu rewards
		if (NotificationCenter.Instance != null) {
			if (NotificationCenter.Instance.isInsideItemMenu) {
				NotificationCenter.Instance.isInsideItemMenu = false;
			}
		}

		// Deactivate canvas for Start of rewards with DELAY
		isRewardsStarted = false;
		StartCoroutine (RewardsCanvas_Activator (false));

		// Remove mustache display (Old place)
//		Invoke ("RewardMustache_DeActivate", 1);
//		RewardMustache_Leave ();

		animSpotlightHolder.Play ("Egg Spotlight - Leave Anim 1 (Legacy)");

		// Instead of 4 grads, use 1 animator
//		animRewardGradsHolder.Play ("Egg Reward Grads Finish Anim 1 (Legacy)");
		animatorRewardGradSingleHolder.SetTrigger ("EggGradTrigger");

		// Set chest counter to zero
		chestCounter = 0;

		if (PlayerData_InGame.Instance != null) {
			if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
				RewardMachine_CallVictory ();
			} 

			// End of egg rewards in-game BUT only for endless
			else {
				// This tick is for endless mode (To show endless egg rewards have ended and XP bar can continue after "while loop")
				PlayerData_InGame.Instance.isEndlessEggsRewarding = false;
			}

		} else {
			if (!isDontUpdateItems) {
				RewardMachine_UpdateItemDisplay ();
			} 

			// The above check is for updating items in case of vid gift where the items menu is disabled (Problem for coroutines)
			else {
				isDontUpdateItems = false;	
			}
		}

//      Invoke("RewardMachine_CallVictory", 0.1F);

        Invoke ("RewardMachine_DisableMachine", 1F);
	}

	IEnumerator RewardsCanvas_Activator (bool whichBool) {
		if (whichBool) {
			yield return null;
		} else {
			yield return new WaitForSeconds (1F);
		}

		// Control canvas and graphic raycaster for rewards canvas
		canvasRewardsHolder.enabled = isRewardsStarted;
		graphicRayRewardsHolder.enabled = isRewardsStarted;
	}

	void EggAnimate (int whichAnim) {
		animRewardMachineHolder.Stop ();
		animRewardMachineHolder.clip = animClipsRewardMachineArr[whichAnim];
		animRewardMachineHolder.Play ();
	}

	void EggEnter_First_Fast () {
		animRewardMachineHolder.Play ();
	}

	void EggEnter_First_Slow () {
		animRewardMachineHolder.Play ();
	}

	void EggEnter_BeyondFirst_Fast () {
		animRewardMachineHolder.Play ();
	}

    // 1 , 2, 3 migire baraye switch kardan beyne seda haye mokhtalef
    private int cycleItemSound = 1;

	void NewItem_SoundSelect_Select () {
		// SOUNDD EFFECTS - Other Egg Touch (done)
		if (SoundManager.Instance != null) {
			switch (chestItemBuffer.Item.itemTag) {
			case Item.ItemTag.Common:
				SoundManager.Instance.CommonItem.Play ();
				break;
			case Item.ItemTag.Rare:
				SoundManager.Instance.RareItem.Play ();
				break;
			case Item.ItemTag.Legendary:
				SoundManager.Instance.LegendaryItem.Play ();
				break;
			default:
				SoundManager.Instance.CommonItem.Play ();
				Debug.LogError ("Default Error");
				break;
			}
		}
	}

	void NewItem_SoundSelect_Random () {
		// SOUNDD EFFECTS - Other Egg Touch (done)
		if (SoundManager.Instance != null)
		{
			//                    Debug.Log(cycleItemSound);

//			switch (chestItemBuffer.Item.itemTag) {
//			case Item.ItemTag.Common:
//				SoundManager.Instance.CommonItem.Play();
//				break;
//			case Item.ItemTag.Rare:
//				SoundManager.Instance.RareItem.Play();
//				break;
//			case Item.ItemTag.Legendary:
//				SoundManager.Instance.LegendaryItem.Play();
//				break;
//			default:
//				SoundManager.Instance.CommonItem.Play();
//				Debug.LogError ("Default Error");
//				break;
//			}

            switch (cycleItemSound)
            {
                case 1:
                    SoundManager.Instance.CommonItem.Play();
                    break;
                case 2:
                    SoundManager.Instance.RareItem.Play();
                    break;
                case 3:
                    SoundManager.Instance.LegendaryItem.Play();
                    break;
                default:
                    SoundManager.Instance.CommonItem.Play();
                    break;
            }
            cycleItemSound++;
            if (cycleItemSound >3)
            {
                cycleItemSound = 1;
            }

		}
	}

	void OpenChest_NewItem () {
		isDuringChestAnim = true;
		isItemGettingCompleted = false;
//		Debug.LogWarning ("CHEST OPEN!");

		// Remove mustache display (Old place)
//		if (isMustacheDisplayIn) {
//			Invoke ("RewardMustache_DeActivate", 1.1F);
//			Invoke ("RewardMustache_Leave", 0.7F);
//		}

		// Check to see if it is the last item
		if (itemCounter != chestCapacity) {

			// Activate item mover in case it was disabled by mustache
			itemMoverObjHolder.SetActive (true);

			// Check to see if it is first item or not (For egg cracking open)
			if (itemCounter != 0) {

				// Sound for first item
				NewItem_SoundSelect_Random ();

                eggParticlesArr[2].Play ();
				eggParticlesArr [6].Play ();

				EggAnimate (4);
			} else {

                //SOUNDD EFFECTS -Start Egg Break (done)
                if (SoundManager.Instance != null)
                {
                    SoundManager.Instance.ShekastaneTokhmeMorghVaItem.Play();
                }

				// Sound for first item (Isn't used)
//				NewItem_SoundSelect_Random ();

                eggParticlesArr [0].Play ();
				eggParticlesArr [1].Play ();
				eggParticlesArr [2].Play ();
				eggParticlesArr [3].Play ();

				EggAnimate (3);
			}

			// Disable idle particles and move them into proper place (These events were on the first and second frames of both egg touched anims);
			EggShineParticle_Activate (4);
			EggShineParticle_DeActivate (5);

//			Debug.LogWarning ("New item: " + chestRewardsArr [chestCounter].thisChest_ItemsArr [itemCounter]);

			// Eggs light activate
			eggLightsObjHolder.SetActive (true);

			// Update item display logic
			OpenChest_ItemActual ();

			// Increase item counter
			itemCounter++;

			if (itemCounter == chestCapacity) {
				// Eggs light deactivate
				eggLightsObjHolder.SetActive (false);
			}

		} else {
//			Debug.LogError ("Call next chest!");

			// Eggs light deactivate
			eggLightsObjHolder.SetActive (false);

			// Increase item counter
			itemCounter = 0;

			// Item Texts Show
			if (!isRewardMustache) {
				// Only show text exit when the last item was not mustache
				ItemTexts_Animate (1);
			}

			// Chest counter increase
			chestCounter++;

			EggAnimate (5);

            // SOUNDD EFFECTS - Biroon raftane Tokhme morgh (done)
            if (SoundManager.Instance!=null)
            {
                SoundManager.Instance.RaftaneTokhmeMorgh.Play();

            }

			// Check to see if its the end of chests or not
			CallChest_Check ();

//			SelectChest (chestCounter);
//			Invoke ("OpenChest_NewItem", 2);
		}
	}

	public void OpenChest_ItemActual () {
		if (chestRewardsArr [chestCounter].itemsForChestArr [itemCounter].AbsoluteMustacheReward == 0) {
			isRewardMustache = false;
			ItemReward_Display ();
//			Debug.LogWarning ("New item: " + chestRewardsArr [chestCounter].itemsForChestArr [itemCounter].Item.name);
		} else {
			isRewardMustache = true;

			// Call Reward mustache (for mustache reward) (OLD) (Now in mustache shoot)
			StartCoroutine (RewardMachine_MustacheDisplayAll(0));

//			RewardMustache_Activate ();
//			RewardMustache_Enter ();
//			RewardMustache_SetupText ();

//			ItemReward_Mustache_Prepare ();
//			Debug.LogWarning ("Reward MUSTACHE: " + chestRewardsArr [chestCounter].itemsForChestArr [itemCounter].AbsoluteMustacheReward);
		}
	}

	public void RewardMustache_SetupText () {
		if (PlayerData_InGame.Instance != null) {
			Rewards_Update_Mustache (PlayerData_InGame.Instance.p1_Moustache);
		} else {
			Rewards_Update_Mustache (PlayerData_Main.Instance.player_Mustache);
		}
	}

	public void ItemReward_Mustache_Prepare () {
		// Mustache Stuff
		GameObject newMustacheObj = Instantiate (mustacheParentObjHolder);
		instantiated_mustacheBuffer = newMustacheObj;
		instantiated_mustacheBuffer.transform.SetParent (transform);
		instantiated_mustacheBuffer.transform.localScale = Vector3.zero;
	}

	public void ItemReward_ShowMustacheDisplay () {

		// Remove mustache display
		if (isRewardMustache) {

			Debug.LogError ("SHOWWWWWWWWWWWWWWWW Mustache DISPLAYYYYYYYYYYYYYYYYY - Reward is Mustache");
			Debug.LogError ("SHOWWWWWWWWWWWWWWWW Mustache DISPLAYYYYYYYYYYYYYYYYY - Reward is Mustache");

			// New place to CALL Mustache Display
			StartCoroutine (RewardMachine_MustacheDisplayAll(0));
		}
	}

	public void ItemReward_PrepareMustacheShoot () {
		if (!itemConvertIsDone) {
			particleItemToMustacheHolder.Play ();
			itemMoverObjHolder.SetActive (false);
			if (PlayerData_InGame.Instance != null) {
				camInGameScriptHolder.camParentAnimHolder.Play ("Cam Attack Right Anim 1 (Legacy)");
			}
			ItemReward_Mustache_Shoot (itemDisplayScriptHolder.int_ItemToMustacheBuffer);
			ItemTexts_Animate (2);
		}
	}

	public void ItemReward_Mustache_Shoot (int mustacheAmount) {
		// Mustache Stuff
		GameObject newMustacheObj = Instantiate (mustacheParentObjHolder);
		newMustacheObj.transform.SetParent (transform);
		newMustacheObj.transform.localPosition = new Vector3 (newMustacheObj.transform.localPosition.x, newMustacheObj.transform.localPosition.y, 0);
		newMustacheObj.transform.localScale = v3_mustacheItemScaleBuffer;
		mustacheScriptHolder = newMustacheObj.GetComponent<Mustache_Script> ();
//		Debug.LogError ("OUT OF RANGE chestCounter: " + chestCounter + " OUT OF RANGE itemCounter: " + itemCounter);

		if (PlayerData_InGame.Instance != null) {
			mustacheScriptHolder.ShootMustache_Awards (mustacheAmount);
		} else {
			mustacheScriptHolder.ShootMustache (mustacheAmount);
		}
	}

	public void EggShineParticle_Activate (int whichParticle) {
		eggParticlesArr [whichParticle].Play ();
	}

	public void EggShineParticle_DeActivate (int whichParticle) {
		eggParticlesArr [whichParticle].Stop ();
	}

	public void EggItemShowParticle () {
		particleEggItemShowHolder[0].Play ();
		particleEggItemShowHolder[1].Play ();
	}

	public void Convert_Check () {
		itemConvertIsDone = true;
	}

	public void Complete_Check () {
		isItemGettingCompleted = false;
	}

	public void ItemRevealMoment () {
		EggItemShowParticle ();
		isChestTouchAllowed = false;
		if (isRewardMustache) {
			ItemReward_Mustache_Shoot (chestRewardsArr [chestCounter].itemsForChestArr [itemCounter - 1].AbsoluteMustacheReward);
//			Debug.LogError ("Name: " + transform.GetChild (0).gameObject.name);
			itemMoverObjHolder.SetActive (false);

			// Disable Fist In This Case
			eggFistImageHolder.SetActive (false);
		} else {
			// Item Texts Show
			ItemTexts_Animate (0);
			Item_ConvertToMustache_Check ();
			Item_CompleteTransform_Check ();
			Item_FirstItemCheck ();
		}
	}

	public void Item_ConvertToMustache_Check () {
		// Check convert to mustache
		if (!chestRewardsArr [chestCounter].itemsForChestArr [itemCounter - 1].IsConvertedToSibil) {
//			Debug.LogWarning ("YO 1! Don't convert to mustache");

			itemConvertIsDone = true;

			// For check to see if last reward is mustache so the mustache display can leave later
			isItemTurnedToMustache = false;

			eggFistImageHolder.SetActive (false);
			ItemSingleAnim_Idle ();
			ItemTextColor_Amount (0);
		} else {
//			Debug.LogWarning ("YO 2! Convert to MUSTACHE!");

			// Call Reward mustache (OLD) (Now in mustache shoot)
			if (!isMustacheDisplayIn) {
				StartCoroutine (RewardMachine_MustacheDisplayAll (0.4F));
			}

//			RewardMustache_Activate ();
//			RewardMustache_Enter ();
//			RewardMustache_SetupText ();

			itemConvertIsDone = false;

			// For check to see if last reward is mustache so the mustache display can leave later
			isItemTurnedToMustache = true;

			eggFistImageHolder.SetActive (true);
			ItemSingleAnim_ToMustacheConvert ();
			ItemTextColor_Amount (2);
		}
	}

	public IEnumerator RewardMachine_MustacheDisplayAll (float delay) {

		yield return new WaitForSeconds (delay);

		if (!isMustacheDisplayIn) {
			StartCoroutine (RewardMustache_Triplets ());
		}

//		yield return new WaitForSeconds (2);
//		RewardMustache_Leave ();

//		yield return new WaitForSeconds (0.4F);
//		RewardMustache_DeActivate ();
	}

	public IEnumerator RewardMustache_Triplets () {
//		yield return new WaitForSeconds (delay);
		RewardMustache_Activate ();
		RewardMustache_Enter ();
		RewardMustache_SetupText ();
		yield return null;
	}

	public void Item_CompleteTransform_Check () {
		if (chestRewardsArr [chestCounter].itemsForChestArr [itemCounter - 1].JustGotCompleted) {
			isChestTouchAllowed = false;
			isItemGettingCompleted = true;
			ItemSingleAnim_CompleteTransform ();

			// Update notif vibration bool via event in Notifcenter
			ExclamationAdd ();

			// Achievement for item completion
			if (AchievementManager.Instance != null) {
				StartCoroutine (AchievementManager.Instance.AchieveProg_CompleteItems_15 ());
				StartCoroutine (AchievementManager.Instance.AchieveProg_CompleteItems_All ());

				chestItemBufferForAchieve = chestRewardsArr [chestCounter].itemsForChestArr [itemCounter - 1];

				// For getting legendary item achievement
				if (chestItemBufferForAchieve.Item.availabilityState == Item.AvailabilityState.Complete &&
					chestItemBufferForAchieve.Item.itemTag == Item.ItemTag.Legendary) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_GetLegendaryItem ());
				}

				// For getting mother's picture
				if (chestItemBufferForAchieve.Item.itemId == 102) {
					StartCoroutine (AchievementManager.Instance.AchieveProg_GetMomsPicture());
				}
					
			}
		}
	}

	public void Item_FirstItemCheck () {
		if (chestRewardsArr [chestCounter].itemsForChestArr [itemCounter - 1].CurAmount == 1) {
			// Update notif vibration bool via event in Notifcenter
			ExclamationAdd ();
		}
	}

	public void ExclamationAdd () {
		if (OnExclamationGain != null)
		{
			OnExclamationGain ();
		}
	}

	public void Item_CompleteTransform_Particles () {
		particleItemCompleteTransformArr [0].Play ();
		particleItemCompleteTransformArr [1].Play ();
		particleItemCompleteTransformArr [2].Play ();
	}

	public void ItemReward_Display () {
		chestItemBuffer = chestRewardsArr [chestCounter].itemsForChestArr [itemCounter];
		itemDisplayScriptHolder.thisItem = chestItemBuffer.Item;
		itemDisplayScriptHolder.SetupRewardOnly ();
		ItemReward_InfoText ();
	}

	public void ItemReward_InfoText () {
		itemText_Name.text = chestItemBuffer.Item.farsiName;
		itemText_Amount_CurrentNMax.text = chestItemBuffer.CurAmount.ToString () + " / " + chestItemBuffer.MaxAmount.ToString ();
		itemText_Effect.text = chestItemBuffer.Item.itemEffectDescription;
		itemText_Description.text = chestItemBuffer.Item.description;

		// Resposition Items place for convert to mustache items
		if (chestItemBuffer.IsConvertedToSibil) {
			transItemTextsParentHolder.localPosition = new Vector3 (-12	, -2, 0);
		} else {
			transItemTextsParentHolder.localPosition = new Vector3 (8, 134, 0);
		}

		// colorItemRarityTextsArr

		// Rarity based color for title
		ItemTextColor_Name ();
	}

    

	public void ItemTextColor_Name () {
        switch (chestItemBuffer.Item.itemTag) {
		case Item.ItemTag.Common:
			itemText_Name.color = colorItemRarityTextsArr [0];
                break;
		case Item.ItemTag.Rare:
			itemText_Name.color = colorItemRarityTextsArr [1];
                break;
		case Item.ItemTag.Legendary:
			itemText_Name.color = colorItemRarityTextsArr [2];
                break;
		default:
			itemText_Name.color = colorItemRarityTextsArr [0];
			break;
		}
	}

	public void ItemTextColor_Amount (int whichColorSet) {
		itemText_Amount_CurrentNMax.color = colorItemAmountTextArr [whichColorSet];

		// For repeat text
		if (whichColorSet == 2) {
			itemText_Amount_CurrentNMax.text = "(!ﯽﻓﺎﺿﺍ)    " + itemText_Amount_CurrentNMax.text;
		}
	}

	public void ItemSingleAnim_Idle () {
		Debug.LogWarning ("IDLE Normal!");
		animItemSingleHolder.Rewind ();
		animItemSingleHolder.clip = animClipItemSingleArr [0];
		animItemSingleHolder.Rewind ();
		animItemSingleHolder.Play ();
	}

	public void ItemSingleAnim_Reset () {
		animItemSingleHolder.Stop ();
		animItemSingleHolder.clip = animClipItemSingleArr [1];
		animItemSingleHolder.Play ();
	}

	public void ItemSingleAnim_ToMustacheConvert () {
		animItemSingleHolder.Stop ();
		animItemSingleHolder.clip = animClipItemSingleArr [2];
		animItemSingleHolder.Play ();

        // SOUNDD EFFECTS - Item Be Sibil (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.ItemBeSibil.Play();
			SoundManager.Instance.Mosht1.PlayDelayed (1.2F);
        }
	}

	public void ItemSingleAnim_CompleteTransform () {
		Debug.LogWarning ("COMPLETE!");
		animItemSingleHolder.Stop ();
		animItemSingleHolder.clip = animClipItemSingleArr [3];
		animItemSingleHolder.Play ();

        // SOUNDD EFFECTS - Takmil Item (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.TakmilItem.PlayDelayed(0.2f);
        }

	}

	public void ItemSingleAnim_CompleteIdle () {
		Debug.LogWarning ("IDLE COMPLETE");
		animItemSingleHolder.Stop ();
		animItemSingleHolder.clip = animClipItemSingleArr [4];
		animItemSingleHolder.Play ();
	}

	public void Item_ChestAllowed_TrueCheck () {
		if (itemConvertIsDone && !isItemGettingCompleted) {
			IsChestTouchAllowed_True ();
		}
	}

//	public void Item_ChestAllowed_TrueNOW () {
//		isChestTouchAllowed = true;
//		if (!isItemGettingCompleted) {
//			Invoke ("IsChestTouchAllowed_True", 1F);
//		} else {
//			Invoke ("IsChestTouchAllowed_True", 4F);
//		}
//	}

	public void ItemTexts_Animate (int whichAnim) {
		animItemTextsHolder.Stop ();
		animItemTextsHolder.clip = animClipItemTextsArr [whichAnim];
		animItemTextsHolder.Play ();

		// Check to play particle for item completion
		if (whichAnim == 4) {
			// For completion text
			itemText_Amount_CurrentNMax.text = "(!ﻞﯿﻤﮑﺗ)    " + itemText_Amount_CurrentNMax.text;

			ItemTextColor_Amount (1);
			Invoke ("ItemTexts_Particle_Name", 0.1F);
			Invoke ("ItemTexts_Particle_Amount", 0.3F);
			Invoke ("ItemTexts_Particle_Effect", 0.5F);
		}
	}

	void ItemTexts_Particle_Name () {
		particleItemTextNameHolder.Emit (1);
	}

	void ItemTexts_Particle_Amount () {
		particleItemTextAmountHolder.Emit (1);
	}

	void ItemTexts_Particle_Effect () {
		particleItemTextEffectHolder.Emit (1);
	}

	public void IsChestTouchAllowed_True () {
		isChestTouchAllowed = true;
	}

	public void RewardMachine_GradsRemove () {
		// Instead of 4 grads, use 1 animator
//		animRewardGradsHolder.Play ("Egg Reward Grads Finish Anim 1 (Legacy)");
		animatorRewardGradSingleHolder.SetTrigger ("EggGradTrigger");
	}

	public void RewardMachine_CallVictory () {
//		Invoke ("RewardMachine_HUD_MustacheRemove", 1.5F);

		endLevelScriptHolder.End_BeginVictory_PauseNRest ();
//		Invoke ("RewardMachine_DisableMachine", 1);
	}

	public void RewardMachine_UpdateItemDisplay () {

		itemMenuScriptHolder.SetupDisplay_AllItems ();
		itemMenuScriptHolder.Pressed_EndOfEggBuy ();
	}

	public void RewardMachine_HUD_MustacheRemove () {
		PlayerData_InGame.Instance.Player_Victory_MustacheLateRemove ();
	}

	public void RewardMachine_DisableMachine () {
		animatorRewardGradSingleHolder.gameObject.SetActive (false);
		this.gameObject.SetActive (false);
	}

	void Update () {
		if (isChestTouchAllowed && !isDuringChestAnim) {
			if (Input.GetMouseButtonDown (0)) {
				// Item Texts Disable
				ItemTexts_Animate (3);

				isChestTouchAllowed = false;
				OpenChest_NewItem ();
			}
		}

//		if (Input.GetKeyDown ("i")) {
//			Debug.LogWarning ("WTF!?");
//			ItemSingleAnim_Idle ();
//		}
//		Debug.Log ("chestCounter = " + chestCounter);
	}
}
