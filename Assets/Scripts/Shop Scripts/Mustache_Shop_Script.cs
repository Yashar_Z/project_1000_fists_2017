﻿using UnityEngine;
using System.Collections;

public class Mustache_Shop_Script : MonoBehaviour {

	public int mustacheAmount;
	public int whichMustacheSet;
	public Transform mustacheTargetTransHolder;

	public ParticleSystem mustacheParticleExplodeHolder;
	public Animation mustacheAnimHolder;
	public Vector3 v3_MustacheTarget;
	public Vector3 mustacheAngle;

	public GameObject objTargTempHolder;

	public ParticleSystem[] particleMustacheShopArr;

	private ParticleSystem mustacheParticleHolder;

	// Use this for initialization
	void Start () {
		//		v3_MustacheTarget = mustacheTargetTransHolder.position;
		v3_MustacheTarget = new Vector3 (9, 4.5F, 0);
		mustacheAngle = Vector3.zero;
	}

	void MustacheMoveStart () {
		mustacheAnimHolder.Play ();
		//		Debug.LogError ("MUSTACHE!2");
	}

	public void Mustache_Colorize (Color newColor) {
		mustacheParticleHolder.startColor = newColor;
		mustacheParticleExplodeHolder.startColor = newColor;
	}

	public void MustacheMoveSpeed () {
		if (whichMustacheSet == 4) {
//			Debug.LogWarning ("NOOOOOTTT FAST!");
			mustacheAnimHolder [mustacheAnimHolder.clip.name].speed = 0.75F;
		} else {
//			Debug.LogWarning ("FAST!");
			mustacheAnimHolder [mustacheAnimHolder.clip.name].speed = 1;
		}
	}

	public void ShootMustache_Shop (int newMustache) {
		mustacheAmount = newMustache;
		mustacheAngle.z = Mathf.Atan2 ((v3_MustacheTarget.y - transform.position.y)
			, (v3_MustacheTarget.x - transform.position.x)) * Mathf.Rad2Deg;
		transform.eulerAngles = mustacheAngle;

		ShootMustache_SelectParticleSet ();
		mustacheParticleHolder.Play ();
		Invoke ("MustacheMoveStart", 0.01F);

		// Update stats for mustache (Old Place)
		//		PlayerData_InGame.Instance.stats_MustacheGained += newMustache;
	}

	public void ShootMustache_SelectParticleSet () {
		mustacheParticleHolder = particleMustacheShopArr [whichMustacheSet];
//		Debug.LogWarning ("Angle: " + (-mustacheAngle));
		mustacheParticleHolder.transform.localEulerAngles = -mustacheAngle;
	}

	public void ShootMustache_Awards (int newMustache) {
		mustacheAmount = newMustache;
		transform.eulerAngles = new Vector3 (0, 0, 13);
		mustacheParticleHolder.Play ();
		Invoke ("MustacheMoveStart", 0.01F);

		// Update stats for mustache
//		PlayerData_InGame.Instance.stats_MustacheGained += newMustache;
	}

	public void ShootTarget_Temp (int number) {
		objTargTempHolder.name = "Target Temp " + (number + 1).ToString ();
//		objTargTempHolder.transform.SetParent (transform.root);
		objTargTempHolder.transform.position = v3_MustacheTarget;
	}
}
