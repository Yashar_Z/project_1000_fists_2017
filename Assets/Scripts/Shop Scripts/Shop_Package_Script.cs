﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Fabric.Answers;

public class Shop_Package_Script : MonoBehaviour {

//	public Mustache_Shop_Script mustacheShopScriptHolder;
	public GameObject objShopMustacheHolder;

	public Text text_MustacheHolder;
	public Text text_MustacheOfferPercentHolder;
	public Text text_PriceHolder_NoOffer;
	public Text text_PriceHolder_Prev;
	public Text text_PriceHolder_AfterOffer;

	public GameObject objShopRewardOfferStarHolder;
	public GameObject objShopPriceNoOfferHolder;
	public GameObject objShopPricePrevHolder;
	public GameObject objShopPriceAfterOfferHolder;

	public GameObject objShopNoBuyPopUpHolder;
	public GameObject[] objShopGlows;

	public AnimationClip[] animClipsButtonGeneralArr;
	public Animation animButtonHolder;
	public Animation animSparkArtHolder;
	public Animation animRewardsIdleHolder;
	public Animation[] animShopBackShinesArr;
	public Animation[] animShopBackContentsArr;
//	public Transform transButtonMoverHolder;

	public Transform transShopBackStuffHolder;
	public Transform transRewardParentHolder;
	public Transform transMustacheBufferPositHolder;

	public ParticleSystem particleGoMustacheFullscreenHolder;
	public ParticleSystem particleSelectSparkHolder;

	public int price_NoOfferINT; // Offer actually means percent-off / discount
	public int price_PrevINT;
	public int price_AfterOfferINT;  
	public int reward_ActualINT;
	public int reward_OfferPercentINT;

	public int whichShopINT;
	public bool hasOffer;

//	private GameObject objShopMustacheHolder;
	private Mustache_Shop_Script mustacheShopScriptRef;
	private Transform transMustacheSourceRef;
	private Transform transMustacheinstantiatedRef;

    //	void Start () {
    //	}

    private void ShopEvent_Successful (string package_itemId)
    {
        int purchaseId = -1;
        switch (package_itemId)
        {
            case CafeBazaarIapManager.SIBIL_0_PACK:
            //case MoshtanStoreAssets.SIBIL_0_PACK_ITEM_ID:
                purchaseId = 0;
                break;
            case CafeBazaarIapManager.SIBIL_1_PACK:
            //case MoshtanStoreAssets.SIBIL_1_PACK_ITEM_ID:
                purchaseId = 1;
                break;
            case CafeBazaarIapManager.SIBIL_2_PACK:
            //case MoshtanStoreAssets.SIBIL_2_PACK_ITEM_ID:
                purchaseId = 2;
                break;
            case CafeBazaarIapManager.SIBIL_3_PACK:
            //case MoshtanStoreAssets.SIBIL_3_PACK_ITEM_ID:
                purchaseId = 3;
                break;
            case CafeBazaarIapManager.SIBIL_4_PACK:
            //case MoshtanStoreAssets.SIBIL_4_PACK_ITEM_ID:
                purchaseId = 4;
                break;
            default:
                break;
        }
        if (purchaseId == whichShopINT)
        {
			particleGoMustacheFullscreenHolder.Play ();
            Package_BuyNow();

            // SOUNDD EFFECTS - Shop successfull purchase (done)
            if (SoundManager.Instance!= null)
            {
                SoundManager.Instance.Kharid.Play();
            }

            Answers.LogCustom(
                    "Package Bought Successfuly",
                    customAttributes: new System.Collections.Generic.Dictionary<string, object>() {
                        { "Mustache", PlayerData_Main.Instance.player_Mustache },
                        { "Package ItemId", package_itemId }
                    }
                );

        }
    }

	private void ShopEvent_Unsuccessful () {
		GetComponentInParent<Shop_Popup_Script> ().Shop_PurchaseBlockers_DeActivate ();
		NoBuy_PopUp_Show ();

//		Purchase_ClickBlocker (false);

		StartCoroutine (Delayed_Deactivate ());
	}

	private IEnumerator Delayed_Deactivate () {
		yield return new WaitForSeconds (0.2F);
		GetComponentInParent<Shop_Popup_Script> ().Shop_PurchaseBlockers_DeActivate ();
	}

	void OnEnable () {
		whichShopINT = this.transform.GetSiblingIndex ();

		// Old object get for shop mustache
		transMustacheSourceRef = objShopMustacheHolder.transform;
	}

	public void ShopPackage_LateEnable () {
//		Debug.LogError ("SHOP SUBSCRIBED!");
		//MoshtanEventHandler.OnPurchaseSuccessful += ShopEvent_Successful;
  //      MoshtanEventHandler.OnPurchaseCancelled += ShopEvent_Unsuccessful;

        if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.VAS) {
			VAS_MenuController_Script.OnVAS_PurchaseSuccessful += ShopEvent_Successful;
			VAS_MenuController_Script.OnVAS_PurchaseUnsuccessful += ShopEvent_Unsuccessful;
		}
        else if(PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
        {
            CafeBazaarEventListener.OnPurchaseSuccessful += ShopEvent_Successful;
            CafeBazaarEventListener.OnPurchaseCancelled += ShopEvent_Unsuccessful;
        }
        else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
        {
            ArioGamesIapManager.OnArioPurchaseSuccessful += ShopEvent_Successful;
            ArioGamesIapManager.OnArioPurchaseCancelled += ShopEvent_Unsuccessful;
        }

		SetupShop_Background ();
		SetupShop_Numbers ();
		SetupShop_Texts ();
		SetupShop_Offers ();

		// New Spark Art Animate
		animSparkArtHolder.Play ("Menu - Shop Spark Reset Anim 1 (Legacy)");

		// For ordered delay of idle anims for BOTH the spark and back shine
		StartCoroutine (SparkNShine_IdlePlay (whichShopINT * 0.25F));
	}

	public void ShopPackage_LateDisable ()
	{
//		Debug.LogError ("SHOP UNNNN SUBSCRIBED!");
		//MoshtanEventHandler.OnPurchaseSuccessful -= ShopEvent_Successful;
		//MoshtanEventHandler.OnPurchaseCancelled -= ShopEvent_Unsuccessful;

		if (PlayerData_Main.Instance != null) {
			if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.VAS) {
				VAS_MenuController_Script.OnVAS_PurchaseSuccessful -= ShopEvent_Successful;
				VAS_MenuController_Script.OnVAS_PurchaseUnsuccessful -= ShopEvent_Unsuccessful;
			}
            else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.CafeBazaar)
            {
                CafeBazaarEventListener.OnPurchaseSuccessful -= ShopEvent_Successful;
                CafeBazaarEventListener.OnPurchaseCancelled -= ShopEvent_Unsuccessful;
            }
            else if (PlayerData_Main.Instance.whatShopType == PlayerData_Main.OnlineShopType.Ario)
            {
                ArioGamesIapManager.OnArioPurchaseSuccessful -= ShopEvent_Successful;
                ArioGamesIapManager.OnArioPurchaseCancelled -= ShopEvent_Unsuccessful;
            }
		}
	}

    public void OnEnable_Old () {

  //      MoshtanEventHandler.OnPurchaseSuccessful += ShopEvent_Successful;
		//MoshtanEventHandler.OnPurchaseCancelled += ShopEvent_Unsuccessful;
//		MoshtanEventHandler.OnPurchaseCancelled += () => {
//			NoBuy_PopUp_Show ();
//		};
        
        whichShopINT = this.transform.GetSiblingIndex ();

//		transButtonMoverHolder.localScale = Vector3.zero;

		// Old object get for shop mustache
		transMustacheSourceRef = objShopMustacheHolder.transform;
		//		objShopMustacheHolder = mustacheShopScriptHolder.gameObject;

		//		if (whichShopINT == 2 || whichShopINT == 4) {
		//			hasOffer = true;
		//		}

		SetupShop_Background ();
		SetupShop_Numbers ();
		SetupShop_Texts ();
		SetupShop_Offers ();

		// New Spark Art Animate
		animSparkArtHolder.Play ("Menu - Shop Spark Reset Anim 1 (Legacy)");

		// For ordered delay of idle anims for BOTH the spark and back shine
		StartCoroutine (SparkNShine_IdlePlay (whichShopINT * 0.25F));

		// Old particle animate
//		StartCoroutine (Particle_IdlePlay (whichShopINT * 0.25F));
	}

    private void OnDisable_OLD()
    {
  //      MoshtanEventHandler.OnPurchaseSuccessful -= ShopEvent_Successful;
		//MoshtanEventHandler.OnPurchaseCancelled -= ShopEvent_Unsuccessful;
    }

    public void SetupShop_Numbers () {
		price_NoOfferINT = Shop_Values.Instance.ShopPrice_NoOffer_Arr [whichShopINT];
		price_PrevINT = price_NoOfferINT;
		price_AfterOfferINT = Shop_Values.Instance.ShopPrice_AfterOffer_Arr [whichShopINT];
		reward_ActualINT = Shop_Values.Instance.ShopReward_Arr [whichShopINT];
		hasOffer = Shop_Values.Instance.ShopHasOffer_Arr [whichShopINT];
	}

	public void SetupShop_Background () {
		transShopBackStuffHolder.GetChild (whichShopINT).gameObject.SetActive (true);
	}

	public void SetupShop_Offers () {

		// Turn off back glows
		for (int i = 0; i < 3; i++) {
			objShopGlows [i].SetActive (false);
		}

		if (hasOffer) {
			objShopRewardOfferStarHolder.SetActive (true);
			objShopPriceNoOfferHolder.SetActive (false);
			objShopPriceAfterOfferHolder.SetActive (true);
			objShopPricePrevHolder.SetActive (true);

			animShopBackContentsArr[whichShopINT].Play ();
			objShopGlows [whichShopINT].SetActive (true);

			// For the position of reward amount
			transRewardParentHolder.localPosition = new Vector3 (-13, 0, 0);

		} else {
			objShopRewardOfferStarHolder.SetActive (false);
			objShopPriceNoOfferHolder.SetActive (true);
			objShopPriceAfterOfferHolder.SetActive (false);
			objShopPricePrevHolder.SetActive (false);

			// For the position of reward amount
			transRewardParentHolder.localPosition = new Vector3 (0, 0, 0);
		}
	}

	public void SetupShop_Texts () {
		// Setup Some Numbers
		price_PrevINT = price_NoOfferINT;
		if (hasOffer) {
			reward_OfferPercentINT = Mathf.RoundToInt (((float)(price_PrevINT-price_AfterOfferINT) / (float)price_PrevINT) * 100);
		}

		// Reward Texts
		text_MustacheHolder.text = reward_ActualINT.ToString();
		if (hasOffer) {
			text_MustacheOfferPercentHolder.text = "-" + reward_OfferPercentINT.ToString () + "%";
		}

		// Price Texts
		text_PriceHolder_NoOffer.text = price_NoOfferINT.ToString();
		text_PriceHolder_Prev.text = price_PrevINT.ToString();
		text_PriceHolder_AfterOffer.text = price_AfterOfferINT.ToString();
	}

	public void Button_PlayAnim (int whichAnim) {
		animButtonHolder.clip = animClipsButtonGeneralArr [whichAnim];
		animButtonHolder.Play ();
	}

	public void NoBuy_PopUp_Show () {
		objShopNoBuyPopUpHolder.SetActive (true);
	}

	IEnumerator SparkNShine_IdlePlay (float delay) {
		yield return new WaitForSeconds (delay);
		animSparkArtHolder.Play ("Menu - Shop Spark Idle Anim 1 (Legacy)");
		animRewardsIdleHolder.Play ();
//		yield return new WaitForSeconds (delay);
		if (!hasOffer) {
			// Nor short loop / long loop
			animShopBackShinesArr [whichShopINT].Play ("Menu - Shop BackShine ALL Idle Anim 2 (Full Masked) (Legacy)");
		} else {
			// Short Loop for offer
			animShopBackShinesArr [whichShopINT].Play ("Menu - Shop BackShine ALL Idle Anim 3 (Full ShortLoop) (Legacy)");
		}
	}

	IEnumerator Particle_IdlePlay (float delay) {
		particleSelectSparkHolder.Stop ();
		yield return new WaitForSeconds (delay);
		particleSelectSparkHolder.Play ();
	}

	public void Particle_ShowThis () {
//		particleSelectSparkHolder.gameObject.SetActive (true);
	}

	public void Particle_HideThis () {
//		particleSelectSparkHolder.gameObject.SetActive (false);
//		Debug.LogWarning ("Spark: " + particleSelectSparkHolder.name + "  is  " + particleSelectSparkHolder.gameObject.activeInHierarchy);
	}

	public void Pressed_ShopPackage () {
        //		Purchase_ClickBlocker (true);

        // Shop is NOT for VAS
        if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS)
        {
            switch (whichShopINT)
            {
                case 0:
                    PackBuyingHandler._Handler_FirstPackBuy();
                    break;
                case 1:
                    PackBuyingHandler._Handler_SecondPackBuy();
                    break;
                case 2:
                    PackBuyingHandler._Handler_ThirdPackBuy();
                    break;
                case 3:
                    PackBuyingHandler._Handler_FourthPackBuy();
                    break;
                case 4:
                    PackBuyingHandler._Handler_FifthPackBuy();
                    break;
                default:
                    Debug.Log("default switch");
                    break;
            }
        }

        // Shop IS for VAS
        else
        {

            // Disabled Shop (TODO: Later enable)
            if (false)
            {

                Debug.LogError("Pressed shop package item");
                Moshtan_Utilties_Script.ShowToast("TOASTY! Shop Package Pressed");

                string shopItemID;
                switch (whichShopINT)
                {
                    case 0:
                        shopItemID = CafeBazaarIapManager.SIBIL_0_PACK;
                        //shopItemID = MoshtanStoreAssets.SIBIL_0_PACK_ITEM_ID;
                        break;
                    case 1:
                        shopItemID = CafeBazaarIapManager.SIBIL_1_PACK;
                        //shopItemID = MoshtanStoreAssets.SIBIL_1_PACK_ITEM_ID;
                        break;
                    case 2:
                        shopItemID = CafeBazaarIapManager.SIBIL_2_PACK;
                        //shopItemID = MoshtanStoreAssets.SIBIL_2_PACK_ITEM_ID;
                        break;
                    case 3:
                    case 4:
                        shopItemID = CafeBazaarIapManager.SIBIL_3_PACK;
                        //shopItemID = MoshtanStoreAssets.SIBIL_3_PACK_ITEM_ID;
                        break;
                    default:
                        Debug.Log("default switch");
                        shopItemID = CafeBazaarIapManager.SIBIL_0_PACK;
                        //shopItemID = MoshtanStoreAssets.SIBIL_0_PACK_ITEM_ID;
                        break;
                }

                VAS_MenuController_Script.Instance.SelectedShopID = shopItemID;

                // Open loading
                // Add check with server for first true or false
                // Open appropriate popup

                VAS_MenuController_Script.Instance.VAS_Controller_Activator(true);
                VAS_MenuController_Script.Instance.Popup_VasCircleLoading_Start("ﺭﺎﺒﺘﻋﺍ ﯽﺳﺭﺮﺑ ﻝﺎﺣ ﺭﺩ");

                VAS_MenuController_Script.Instance.ShopIAP_FirstCheck(shopItemID);

                //				VAS_MenuController_Script.Instance.PopUp_VAS_OpenThis (7);
            }
            else
            {
                Moshtan_Utilties_Script.ShowToast("این قابلیت در آینده فعال خواهد شد.");
                ShopEvent_Unsuccessful();
            }
        }
    }

	public void Purchase_ClickBlocker (bool isActive) {
	}

	public void Purchase_FailPopUp () {
		objShopNoBuyPopUpHolder.SetActive (true);
	}

	public void Package_BuyNow () {
//		Debug.LogWarning ("POSIT: " + this.transform.position);

		ShopButton_PlayAnim_Leave ();
		Invoke ("ShopButton_PlayAnim_ReEnter", 1);

//		transMustacheBufferPositHolder.position = this.transform.position;
		transMustacheBufferPositHolder.position = transShopBackStuffHolder.position;

		GameObject newMustache = Instantiate (objShopMustacheHolder);
		transMustacheinstantiatedRef = newMustache.transform;
		transMustacheinstantiatedRef.SetParent (transMustacheBufferPositHolder);
		transMustacheinstantiatedRef.localPosition = transMustacheSourceRef.localPosition;
		transMustacheinstantiatedRef.localScale = new Vector3 (54, 54, 54);

//		newMustache.name = "BOB";
		mustacheShopScriptRef = newMustache.GetComponent<Mustache_Shop_Script> ();

		mustacheShopScriptRef.whichMustacheSet = whichShopINT;
		mustacheShopScriptRef.MustacheMoveSpeed ();
		mustacheShopScriptRef.ShootMustache_Shop (reward_ActualINT);
//		mustacheShopScriptRef.ShootTarget_Temp (whichShopINT);
	}

//	public IEnumerator ShopBackShines_Animate (float delay) {
//		
//		animShopBackShinesHolder.Play ();
//	}

	public void ShopButton_PlayAnim_Leave () {
		Button_PlayAnim (0);
	}	

	public void ShopButton_PlayAnim_ReEnter () {
		Button_PlayAnim (1);
	}
}
