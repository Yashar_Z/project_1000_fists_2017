﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shop_Popup_Script : MonoBehaviour {

	public Menu_Main_Script menuMainScriptHolder;
	public Popup_All_Script popUpAllScriptHolder;

	public Canvas canvasShopHolder;
//	public GraphicRaycaster graphRaycasterHolder;

	public Text text_Shop_MustacheHolder;
//	public Text text_Shop_StarHolder;

	public Shop_Package_Script[] shopPackScriptArr;
	public Popup_Button_Script[] popupButtonScriptArr;
	public RectTransform menu_MustacheHolderRecTransHolder_ShopMenu;
	public RectTransform menu_MustacheHolderRecTransHolder_ShopMenuShadow;
	public ScrollRect scrollShopHolder;

	public ParticleSystem hud_Mustache_ParticleHolder;
	public ParticleSystem hud_Mustache_BuyInitiateParticleHolder;
	public Animation hud_Mustache_AnimHolder;

	public Button buttonPopUpOK_NoBuyHolder;

	public GameObject objShopPurchasedBlockHolder;

	private int intShopPackageCount;
	private Vector2 menu_MustachePositVect2;
//	[SerializeField]
//	private Shop_Package_Script[] shopPackageScriptArr;

	void Start () {
		ScrollRect_Reset ();

		// Move out shop in case we are in-game
//		if (PlayerData_InGame.Instance != null) {
//			MoveOut_ShopAll ();
//		}

//		popUpAllScriptHolder.PopUp_AnimPlay (1);

		// Instead of Awake and canvasdisable like others
		StartCoroutine (ShopInGame_CanvasController(false));
	}

	void OnEnable () {
		Shop_HUD_Update ();

        //		shopPackageScriptArr = GetComponentsInChildren<Shop_Package_Script> ();
        //		Invoke ("ButtonAnim_StartEnters", 0.2F);
        //		ShopAllParticles_Activate ();
        //		ScrollRect_Reset ();
        //		StartCoroutine (ScrollRect_ResetCoroutine ());

        // SOUND EFFECTS - Shop Pressed (EMPTY)
	}

//	public void ShopAllParticles_Activate () {
//		for (int i = 0; i < shopPackageScriptArr.Length; i++) {
//			shopPackageScriptArr [i].Particle_ShowThis ();
//		}
//	}
//
//	public void ShopAllParticles_DeActivate () {
//		for (int i = 0; i < shopPackageScriptArr.Length; i++) {
//			shopPackageScriptArr [i].Particle_HideThis ();
//		}
//	}

	void ButtonAnim_StartEnters () {
		for (int i = 0; i < popupButtonScriptArr.Length; i++) {
			StartCoroutine( ButtonAnim_Enter (i));
		}
	}

	public void Pressed_MustacheToShop () {
	}

	public void MoveIn_Shop_FromShortCut () {
		this.gameObject.SetActive (true);

		this.transform.localPosition = Vector2.zero;

		if (PlayerData_InGame.Instance != null) {
			transform.parent.localPosition = Vector2.zero;
		}
	}

	public void MoveIn_Shop () {
//		Debug.LogError ("MOVE INNNNNNNNN SHOP!!!!!!!!!!!!");

		ShopPackages_EnableAll ();
		Invoke ("ButtonAnim_StartEnters", 0.2F);
		Shop_HUD_Update ();

		// Enable canvas for in-game
		StartCoroutine (ShopInGame_CanvasController (true));

		if (PlayerData_InGame.Instance == null) {
//			
//
//		} else {
			Phone_Stats_Script.Instance.startOfShortCutWhat = Phone_Stats_Script.StartOfShortCutType.Phone_ShopMenu;
		}

		// This is start (In events of moshtanEventHandler)
		//Soomla.Store.SoomlaStore.StartIabServiceInBg ();
	}

	public void MoveOut_ShopAll () {
		ShopPackages_DisableAll ();
		transform.localPosition = new Vector2 (3200, 0);

		// Disable canvas for in-game
		StartCoroutine (ShopInGame_CanvasController(false));

		if (PlayerData_InGame.Instance != null) {
			// Ingame
			transform.parent.localPosition = new Vector2 (500, 0);

			this.gameObject.SetActive (false);
		} else {
			// Not Ingame

			menuMainScriptHolder.ZoomOut_OnlyMenuActivator ();
			Debug.LogError ("Close Shop!");
		}
	}

	public IEnumerator ShopInGame_CanvasController (bool isActive) {
//		if (PlayerData_InGame.Instance != null) {

		canvasShopHolder.enabled = isActive;
//		graphRaycasterHolder.enabled = isActive;

//		}
		yield return null;
	}

	public void ShopPackages_EnableAll () {
		StartCoroutine (ShopPackages_EnableAll_Now ());
	}

	public IEnumerator ShopPackages_EnableAll_Now () {
		// Shop count for none VAS (5-10)
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			intShopPackageCount = 5;
		} 
		// Shop count for VAS (4)
		else {
			intShopPackageCount = 4;
		}

		for (int i = 0; i < shopPackScriptArr.Length; i++) {

			// Only activate allowed shop
			if (i < intShopPackageCount) {
				shopPackScriptArr [i].ShopPackage_LateEnable ();
			} 

			// is is bigger than shop count
			else {
				shopPackScriptArr [i].gameObject.SetActive (false);
			}
		}
		yield return null;
	}

	public void ShopPackages_DisableAll () {
		StartCoroutine (ShopPackages_DisableAll_Now ());
	}

	public IEnumerator ShopPackages_DisableAll_Now () {
		for (int i = 0; i < shopPackScriptArr.Length; i++) {
			shopPackScriptArr [i].ShopPackage_LateDisable ();
		}
		yield return null;
	}

	public void Shop_HUD_Update () {
		if (PlayerData_Main.Instance != null) {
			// Update Mustache
			Shop_Update_Mustache (PlayerData_Main.Instance.player_Mustache);

			// Update Stars
//			Shop_Update_Stars (PlayerData_Main.Instance.player_Stars);

			// Animate Mustache
			Shop_AnimateMustache_Gain ();

			// Disable Purchase Blocker
			Shop_PurchaseBlockers_DeActivate ();
		}
	}

	public void Shop_Update_Mustache (int newMustache){
		menu_MustachePositVect2.x = 50 + newMustache.ToString ().Length * 13;
		menu_MustachePositVect2.y = 41;
		menu_MustacheHolderRecTransHolder_ShopMenu.sizeDelta = menu_MustachePositVect2;
		menu_MustacheHolderRecTransHolder_ShopMenuShadow.sizeDelta = menu_MustachePositVect2;

		text_Shop_MustacheHolder.text = PlayerData_Main.Instance.player_Mustache.ToString ();
	}

	public void Shop_AnimateMustache_Gain () {
		hud_Mustache_AnimHolder.Stop ();
		hud_Mustache_AnimHolder.Play ("HUD Mustache MENU Gain Anim 1 (Legacy)");
		hud_Mustache_ParticleHolder.Emit (1);
	}

	public void Shop_Update_Stars (int newStars) {
//		text_Shop_StarHolder.text = newStars.ToString ();
	}

	IEnumerator ButtonAnim_Enter (int whichButton) {
		yield return new WaitForSeconds (whichButton * 0.1F);
		popupButtonScriptArr [whichButton].Button_PlayAnim (0);

		// SOUND EFFECTS - One Button Pop-in (EMPTY)
	}

	public void PopUp_InvokeButton (int whichButton) {
		popupButtonScriptArr [whichButton].GetComponentInChildren<UnityEngine.UI.Button> ().onClick.Invoke ();
	}

	public void Shop_PurchaseBlockers_Activate () {
		objShopPurchasedBlockHolder.SetActive (true);
		hud_Mustache_BuyInitiateParticleHolder.Play ();

//		Invoke ("Shop_PurchaseBlockers_DeActivate", 2F);
	}

	public void Shop_PurchaseBlockers_DeActivate () {
		objShopPurchasedBlockHolder.SetActive (false);
	}
		
	public void AllShop_Scroll () {
		scrollShopHolder.horizontalNormalizedPosition = 0;
		ScrollRect_DeActivate ();
	}

	public IEnumerator ScrollRect_ResetCoroutine () {
		yield return new WaitForSeconds (0.2F);
		Debug.LogError ("RESET!");
		ScrollRect_Reset ();
		yield return null;
	}

	public void ScrollRect_Reset () {
		ScrollRect_Activate ();
		scrollShopHolder.horizontalNormalizedPosition = 0;
		ScrollRect_DeActivate ();
	}

	public void ScrollRect_Activate () {
		scrollShopHolder.enabled = true;
	}

	public void ScrollRect_DeActivate () {
		scrollShopHolder.enabled = false;
	}

	public void Pressed_ForNoBuyOK () {
		buttonPopUpOK_NoBuyHolder.onClick.Invoke ();
	}
}
