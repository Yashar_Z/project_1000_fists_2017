﻿using UnityEngine;
using System.Collections;
using System;

public class MusicManager : MonoBehaviour {

    private static readonly float MAXIMUM_MUSIC_VOLUME_ALLOWED = 0.8f;

    private float musicVolume;

    private static MusicManager _instance = null;
    public static MusicManager Instance
    {
        get
        {
            return _instance;
        }
    }

	public AudioSource[] audioSourceArr;

	private AudioSource audioSourceCurrent;

	void Awake () {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        audioSourceCurrent = audioSourceArr [0];
	}

    public void MusicVolumeIntroCheck()
    {
        if (PlayerData_Main.Instance.player_Music_ON)
        {
            musicVolume = MAXIMUM_MUSIC_VOLUME_ALLOWED;
        }
        else
        {
            musicVolume = 0.0f;
        }

		SetMusicVolumes ();
    }

    private void OnEnable()
    {
        Popup_Settings_Script.OnMusicToggled += () =>
        {
            if (PlayerData_Main.Instance.player_Music_ON)
            {
                musicVolume = MAXIMUM_MUSIC_VOLUME_ALLOWED;
            }
            else
            {
                musicVolume = 0.0f;
            }
			SetMusicVolumes ();
        };
    }

	private void SetMusicVolumes () {
		foreach (var aso in audioSourceArr)
		{
			aso.volume = musicVolume;
		}
	}

    public void PlayMusic_Intro()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[0];
        audioSourceCurrent.PlayDelayed(2.2f);
    }
    public void PlayMusic_Menu () {
        if (UnityEngine.Random.Range(0,2) ==1)
        {
            audioSourceCurrent.Stop();
            audioSourceCurrent = audioSourceArr[0];
            audioSourceCurrent.Play();
        }
        else
        {
            //music jadid menu
            audioSourceCurrent.Stop();
            audioSourceCurrent = audioSourceArr[7];
            audioSourceCurrent.Play();
        }

	}

	public void PlayMusic_WorldCinema () {
		audioSourceCurrent.Stop ();
		audioSourceCurrent = audioSourceArr [1];
		audioSourceCurrent.PlayDelayed (1f);
	}
    public void PlayMusic_WorldGarage()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[2];
        audioSourceCurrent.PlayDelayed(1f);
    }
    public void PlayMusic_WorldGrandHotel()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[3];
        audioSourceCurrent.PlayDelayed(1f);
    }
    public void PlayMusic_WorldCinemaNight()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[8];
        audioSourceCurrent.PlayDelayed(1f);
    }
    public void PlayMusic_Victory()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[4];
        audioSourceCurrent.PlayDelayed(0.5f);
    }
    public void PlayMusic_Chests()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[5];
        audioSourceCurrent.PlayDelayed(0.1f);
    }
    public void PlayMusic_EndlessMenu()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[9];
        audioSourceCurrent.PlayDelayed(0.1f);
    }
    public void PlayMusic_EndlessMenuRecord()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[10];
        audioSourceCurrent.PlayDelayed(0.1f);
    }
    public void PlayMusic_GameOver()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[6];
        audioSourceCurrent.Play();
    }

    public void PlayMusic_MoonlightSonata()
    {
        audioSourceCurrent.Stop();
        audioSourceCurrent = audioSourceArr[6];
        audioSourceCurrent.PlayDelayed(10);
    }

    public void StopMusic()
    {
        audioSourceCurrent.Stop();
    }
    public void PauseMusic()
    {
        audioSourceCurrent.Pause();
    }
    public void UnPauseMusic()
    {
        audioSourceCurrent.UnPause();
    }


    public IEnumerator MusicPitch_Lower_Transition()
    {
        while (audioSourceCurrent.pitch > 0.7f)
        {
            audioSourceCurrent.pitch -= 0.1f;
            Debug.Log(audioSourceCurrent.pitch);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public IEnumerator MusicPitch_HigherTransition()
    {
        while (audioSourceCurrent.pitch < 1f)
        {
            audioSourceCurrent.pitch += 0.1f;
            Debug.Log(audioSourceCurrent.pitch);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void BlendMusic(int newMusic)
    {
        StartCoroutine(MusicVolume_FadeToZero(() =>
        {
            audioSourceCurrent.Stop();
            audioSourceCurrent = audioSourceArr[newMusic];
            audioSourceCurrent.PlayDelayed(0.1f);
            StartCoroutine(MusicVolume_FadeToFull(() => { Debug.Log("music blend ended"); }));
        }
        ));
    }

    public IEnumerator MusicVolume_Lower()
    {
//        Debug.Log("lower start"+audioSourceCurrent.volume);
        float targetVolume = audioSourceCurrent.volume/4;
        while (audioSourceCurrent.volume > targetVolume)
        {
            audioSourceCurrent.volume -= 0.1f;
//            Debug.Log("lower progress" +audioSourceCurrent.volume);
            yield return new WaitForSeconds(0.05f);
        }
//        Debug.Log("lower finish"+audioSourceCurrent.volume);
    }
    public IEnumerator MusicVolume_Higher()
    {
        float targetVolume = audioSourceCurrent.volume*4;
        while (audioSourceCurrent.volume < targetVolume)
        {
            audioSourceCurrent.volume += 0.1f;
//            Debug.Log(audioSourceCurrent.volume);
            yield return new WaitForSeconds(0.05f);
        }
//        Debug.Log(audioSourceCurrent.volume);
    }

    public IEnumerator MusicVolume_FadeToFull (Action onFinished) {

        while (audioSourceCurrent.volume < MAXIMUM_MUSIC_VOLUME_ALLOWED)
        {
            audioSourceCurrent.volume += 0.05F;
            yield return new WaitForSeconds(0.05F);
        }
        onFinished();
	}

	public IEnumerator MusicVolume_FadeToZero(Action onFinished)
    {
        
        while (audioSourceCurrent.volume > 0)
        {
            audioSourceCurrent.volume -= 0.05F;
            yield return new WaitForSeconds(0.05F);
        }
        onFinished();
	}
//	void Update () {
//		Debug.LogWarning ("Music vol = " + musicVolume);
//	}
}
