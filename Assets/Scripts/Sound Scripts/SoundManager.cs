﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    private static readonly float MAXIMUM_SFX_VOLUME_ALLOWED = 1.0f;

    private static SoundManager _instance = null;
    public static SoundManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    [Header("Main Menu")]
    public AudioSource Title;
    public AudioSource TapRadKardaneIntro;
    public AudioSource TapMenuStartGame;
    public AudioSource TapEntekhabMarhale;
    public AudioSource BatteryCharge;
    public AudioSource PopUpKharidPowerupSlot;
	public AudioSource PopupOpenPaper;
    public AudioSource KharidPowerUpSlot;
    public AudioSource Kharid;
    public AudioSource MoshtBeCard;
    public AudioSource MoshtBeCardChapter;
    public AudioSource BazShodanBaKelid;
    public AudioSource BazShodanBaStar;
    public AudioSource AchievementMenuClaimed;
    public AudioSource EndlessScoreIncreaseLoop;
    public AudioSource EndlessPayaneShomareshScore;
    public AudioSource EndlessHarkateNavareSabz;
    public AudioSource EndlessLevelUpPhaseOne;
    public AudioSource EndlessLevelUpPhaseTwo;
    public AudioSource EndlessEnd;
    public AudioSource EndlessEndWithRecord;
    public AudioSource EndlessUnlock;

    [Header("InGame")]
    public AudioSource ShieldPowerupClicked;
    public AudioSource ShieldPowerupTime;
    public AudioSource GameOverSound;
    public AudioSource PressedPause;
    public AudioSource FistPowerupClicked;
	public AudioSource FistPowerupSpark;
    public AudioSource MoshtBeSepar1;
    public AudioSource MoshtBeSepar2;
    public AudioSource MoshtBeSepar3;
    public AudioSource MoshtBeSepar4;
    public AudioSource TerekidaneHobab;
    public AudioSource PopUp_Pause;
    public AudioSource Mosht1;
    public AudioSource Mosht2;
    public AudioSource MoshtCrit1;
    public AudioSource MoshtMikhorim1;
    public AudioSource Enfejar1;
    public AudioSource Enfejar2;
    public AudioSource PlayerMiss;
    public AudioSource PipeHit;
    public AudioSource PipeCrit;
    public AudioSource Zang;
    public AudioSource CameraClick;
    public AudioSource AhanrobaContinous;
    public AudioSource KerkerehSound;
    public AudioSource LastStandBruceLee;
    public AudioSource MoshtBeAkharinEnemy;
    public AudioSource Ghalb;
    public AudioSource Tigh;
    public AudioSource GrandHotelDoor;
    public AudioSource CinemaDoor;
    public AudioSource StarHit;
	public AudioSource StartLevelNameShow;
    public AudioSource EndlessComboMultiplierIncrease;
    public AudioSource EndlessComboMultiplierDecrease;
    public AudioSource PotionBattery;
    public AudioSource PotionHeadStart;
    public AudioSource PotionMosht;
    public AudioSource PotionGhalb;
    public AudioSource PotionMoshtCrit;
    public AudioSource PotionKhashmEzhdeha;
    public AudioSource AchievementIngameEarned;

    [Header("Others")]
    public AudioSource VoroodMobile;
    public AudioSource GenericTap1;
    public AudioSource GenericTap2;
    public AudioSource LoadingSound;
    public AudioSource KamboodSibil;
    public AudioSource SibilToGholak;
    public AudioSource ZarbeMenuButton;
	public AudioSource ArbadeLoading;

    [Header("Reward Machine")]
    public AudioSource ItemShow;
    public AudioSource CommonItem;
    public AudioSource RareItem;
    public AudioSource LegendaryItem;
    public AudioSource TakmilItem;
    public AudioSource ItemBeSibil;
    public AudioSource ShekastaneTokhmeMorghVaItem;
    public AudioSource RaftaneTokhmeMorgh;
    public AudioSource NozooleTokhmeMorgh;
    public AudioSource SoudeTokhmeMorgh;

    [Header("Main Menu Backdrop")]
    public AudioSource ClickRuMill;
    public AudioSource BarkhordMillBeZamin;
    public AudioSource Kharak;
    public AudioSource SibilShodaneNeveshte;
    public AudioSource VorudeNeveshteBaZanjir;
    public AudioSource VorudeNeveshte;
    public AudioSource Arbade;
    public AudioSource KiseBoxDown;
    public AudioSource KiseBoxMid;
    public AudioSource KiseBoxUp;
    public AudioSource Darhalghe;
    public AudioSource DarhalgheBargasht;


    private float sfxVolume;


    public void SoundVolumeIntroCheck()
    {
        if (PlayerData_Main.Instance.player_Sound_ON)
        {
            sfxVolume = MAXIMUM_SFX_VOLUME_ALLOWED;
        }
        else
        {
            sfxVolume = 0.0f;
        }
		SetSoundVolumes ();
    }

    private void OnEnable()
    {
        Popup_Settings_Script.OnSfxToggled += () =>
        {
            if (PlayerData_Main.Instance.player_Sound_ON)
            {
                sfxVolume = MAXIMUM_SFX_VOLUME_ALLOWED;
            }
            else
            {
                sfxVolume = 0.0f;
            }

			SetSoundVolumes ();
        }; 
    }

    private void SetSoundVolumes()
    {

        Title.volume = sfxVolume;
        TapRadKardaneIntro.volume = sfxVolume;
        TapMenuStartGame.volume = sfxVolume;
        TapEntekhabMarhale.volume = sfxVolume;
        BatteryCharge.volume = sfxVolume;
        PopUpKharidPowerupSlot.volume = sfxVolume;
        PopupOpenPaper.volume = sfxVolume;
        KharidPowerUpSlot.volume = sfxVolume;
        Kharid.volume = sfxVolume;
        MoshtBeCard.volume = sfxVolume;
        MoshtBeCardChapter.volume = sfxVolume;
        BazShodanBaKelid.volume = sfxVolume;
        BazShodanBaStar.volume = sfxVolume;
        AchievementMenuClaimed.volume = sfxVolume; ;
        EndlessComboMultiplierIncrease.volume = sfxVolume; ;
        EndlessComboMultiplierDecrease.volume = sfxVolume; ;
        EndlessScoreIncreaseLoop.volume = sfxVolume; ;
        EndlessPayaneShomareshScore.volume = sfxVolume; ;
        EndlessHarkateNavareSabz.volume = sfxVolume; ;
        EndlessLevelUpPhaseOne.volume = sfxVolume; ;
        EndlessLevelUpPhaseTwo.volume = sfxVolume; ;
        EndlessEnd.volume = sfxVolume; ;
        EndlessEndWithRecord.volume = sfxVolume;
        EndlessUnlock.volume = sfxVolume;

        ShieldPowerupClicked.volume = sfxVolume;
        ShieldPowerupTime.volume = sfxVolume;
        GameOverSound.volume = sfxVolume;
        PressedPause.volume = sfxVolume;
        FistPowerupClicked.volume = sfxVolume;
        FistPowerupSpark.volume = sfxVolume;
        MoshtBeSepar1.volume = sfxVolume;
        MoshtBeSepar2.volume = sfxVolume;
        MoshtBeSepar3.volume = sfxVolume;
        MoshtBeSepar4.volume = sfxVolume;
        TerekidaneHobab.volume = sfxVolume;
        PopUp_Pause.volume = sfxVolume;
        Mosht1.volume = sfxVolume * 0.35F;
        Mosht2.volume = sfxVolume * 0.35F;
        MoshtCrit1.volume = sfxVolume;
        MoshtMikhorim1.volume = sfxVolume;
        Enfejar1.volume = sfxVolume;
        Enfejar2.volume = sfxVolume;
        PlayerMiss.volume = sfxVolume;
        PipeHit.volume = sfxVolume;
        PipeCrit.volume = sfxVolume;
        Zang.volume = sfxVolume;
        CameraClick.volume = sfxVolume;
        AhanrobaContinous.volume = sfxVolume;
        KerkerehSound.volume = sfxVolume;
        LastStandBruceLee.volume = sfxVolume;
        MoshtBeAkharinEnemy.volume = sfxVolume;
        Ghalb.volume = sfxVolume;
        Tigh.volume = sfxVolume;
        GrandHotelDoor.volume = sfxVolume;
        CinemaDoor.volume = sfxVolume * 0.3F;
        StarHit.volume = sfxVolume;
        StartLevelNameShow.volume = sfxVolume;
        PotionBattery.volume = sfxVolume;
        PotionHeadStart.volume = sfxVolume;
        PotionMosht.volume = sfxVolume;
        PotionGhalb.volume = sfxVolume;
        PotionMoshtCrit.volume = sfxVolume;
        PotionKhashmEzhdeha.volume = sfxVolume;
        AchievementIngameEarned.volume = sfxVolume;

        VoroodMobile.volume = sfxVolume;
        GenericTap1.volume = sfxVolume;
        GenericTap2.volume = sfxVolume;
        LoadingSound.volume = sfxVolume;
        KamboodSibil.volume = sfxVolume;
        SibilToGholak.volume = sfxVolume * 0.2F;
        ZarbeMenuButton.volume = sfxVolume;
        ArbadeLoading.volume = sfxVolume;

        ItemShow.volume = sfxVolume;
        CommonItem.volume = sfxVolume;
        RareItem.volume = sfxVolume;
        LegendaryItem.volume = sfxVolume;
        TakmilItem.volume = sfxVolume;
        ItemBeSibil.volume = sfxVolume;
        ShekastaneTokhmeMorghVaItem.volume = sfxVolume;
        RaftaneTokhmeMorgh.volume = sfxVolume;
        NozooleTokhmeMorgh.volume = sfxVolume;
        SoudeTokhmeMorgh.volume = sfxVolume;


        ClickRuMill.volume = sfxVolume;
        BarkhordMillBeZamin.volume = sfxVolume;
        Kharak.volume = sfxVolume;
        SibilShodaneNeveshte.volume = sfxVolume;
        VorudeNeveshteBaZanjir.volume = sfxVolume;
        VorudeNeveshte.volume = sfxVolume;
        Arbade.volume = sfxVolume;
        KiseBoxDown.volume = sfxVolume;
        KiseBoxMid.volume = sfxVolume;
        KiseBoxUp.volume = sfxVolume;
        Darhalghe.volume = sfxVolume;
        DarhalgheBargasht.volume = sfxVolume;
    }

    public IEnumerator CoPlayDelayedClip(AudioSource source, float _delay)
    {
        yield return new WaitForSeconds(_delay);
        source.PlayOneShot(source.clip);
    }
}
