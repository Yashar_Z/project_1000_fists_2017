﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoryMain_Script : MonoBehaviour {

	public Button storyButtonHolder;
	public ParticleSystem storyNextTriangleParticleHolder;
	public StoryPostEvents_Script storyPostScriptHolder;

	public GameObject storyNextTriangleObjHolder;
	public GameObject storyFacesObjHoldeR_Happy;
	public GameObject storyFacesObjHoldeR_Normal;
	public GameObject storyFacesObjHoldeR_Worried;
	public GameObject storyFacesObjHoldeR_Crying;

	public GameObject storyObj_DialogueHolder;
	public GameObject storyObj_AghdasiHolder;
	public GameObject storyObj_MoshtanHolder;
	public GameObject storyObj_GradsHolder;
	public GameObject storyObj_ButtonHolder;

	public int storySet_World;
	public int storySet_Level;
	public int storySet_Sequence;

	public Animation storyNextTriangleAnimHolder;
	public Animation storyGradsAnimHolder;

	public Canvas canvasStoryHolder;
	public GraphicRaycaster graphicRayStoryHolder;

	public Text dialogueTexTHolder;
	public Transform storySetsTransHolder;
	public Transform storySetsMenuTransHolder;
	public Transform dialogueBoxTransHolder;
	public Transform dialogueArtTransHolder;
	public Transform dialogueArtOnlyBoxTransHolder;
	public Animation animStoryParentHolder;

	public enum PostEventType {
		Into_HandMove,
		Into_Crit,
		Into_PowUp1,
		Into_PowUp2,
		None,
		Into_BatteryCharge,
		Into_PhoneSC,
		Into_ItemPage,
		Into_ItemProgress,
		Into_ItemRepeatItem,
		Into_BatterySlot,

		// This one is not used
		Into_EndlessUnlock,

		Into_EndlessShowLock,
		Into_EndlessShowLeaderboard,
		Into_EndlessShowPlay,

		// 2nd Costumes
		Into_2ndCost_Fat,
		Into_2ndCost_Thin,
		Into_2ndCost_Muscle,
		Into_2ndCost_Giant,

		// Event
		Into_NewYearEvent
	}

	public enum TalkerType 
	{
		Aghdasi_Happy,
		Aghdasi_Normal,
		Aghdasi_Worried,
		Aghdasi_Crying,
		Moshtan,
		Tut,
		None
	}

	public enum StoryAnimType {
		Intro,
		Outro,
		Story,
		Tutorial_In,
		Tutorial_Continue,
		Tutorial_Out
	}

	private PostEventType postEventType_Current;
	private TalkerType whoTalks_Previous;
	private TalkerType whoTalks_Current;
	private string whoTalks_PreviousString;
	private string whoTalks_CurrentString;
	[SerializeField]
	private string aghdasiTalks_FaceString;

//	private Vector3 vec3_DialoguePosit_Aghdasi;
//	private Vector3 vec3_DialoguePosit_Mosthan;

	[SerializeField]
	private int dialogueCounter;
	private int dialogueLength;

//	private int tutorialItemCounter;

	private bool faceShouldChange;
	[SerializeField]
	private bool backFromTutorial;
	private bool isPausable;
//	private bool tutTextUpsideDown;

	private StoryAnimType thisStoryAnimType;
	private StorySet_Script storySetRef;

	// Use this for initialization
//	void Start () {
//		vec3_DialoguePosit_Aghdasi = new Vector3 (-128, -147, 0);
//		vec3_DialoguePosit_Mosthan = new Vector3 (128, -147, 0);

//		isPausable = false;
//		StoryMode_DeActivate ();
//	}

	void OnEnable () {
		isPausable = false;
	}

	public void StoryNumbers_SetNow (int theWorld, int theLevel, int theSequence, bool canPause) {
		storySet_World = theWorld;
		storySet_Level = theLevel;
		storySet_Sequence = theSequence;
		isPausable = canPause;
	}

	public void StartStory (int theWorld, int theLevel, int theSequence, bool canPause) {
		// Activate story before getting to coroutines and such
		transform.parent.gameObject.SetActive (true);

//		Debug.LogError ("Before can pause 2");
		isPausable = canPause;
		Story_Pause ();

		Story_GradsAnim_Start ();
		StoryButton_Interactive_Deactivate ();
		whoTalks_Previous = TalkerType.None;
		whoTalks_Current = TalkerType.None;

		backFromTutorial = false;
		faceShouldChange = false;
//		tutTextUpsideDown = false;

//		tutorialItemCounter = 0;
		dialogueCounter = 0;

//		storySet_World = PlayerData_InGame.Instance.worl

		storySet_World = theWorld;
		storySet_Level = theLevel;
		storySet_Sequence = theSequence;

		GetStorySet (storySet_World, storySet_Level, storySet_Sequence);
	}

	public void GetStorySet (int whichWorld, int whichLevel,int whichSet) {
		if (isPausable) {
			storySetRef = storySetsTransHolder.GetChild (whichWorld - 1).GetChild (whichLevel - 1).GetChild (whichSet).GetComponent<StorySet_Script> ();
		} else {
//			Debug.LogWarning ("whichWorld = " + whichWorld + "  whichLevel = " + whichLevel + "  whichSet = " + whichSet);
			storySetRef = storySetsMenuTransHolder.GetChild (whichWorld - 1).GetChild (whichLevel - 1).GetChild (whichSet).GetComponent<StorySet_Script> ();
		}
		Story_Dialogue_Next ();

		// What post story event?
		postEventType_Current = storySetRef.postStoryEventType;

		// Activate story canvas
		StartCoroutine (StoryCanvas_Activator(true));
	}

//	public void StoryParentEnabler (bool whichInt) {
//		objStoryParentObj.SetActive (whichInt);
//		Debug.LogError ("Before can pause 1");
//	}

	public void Pressed_StoryButton () {
//		Debug.LogError ("Pressed!");

		if (SoundManager.Instance != null) {
			SoundManager.Instance.GenericTap1.Play ();
		}

		Story_Dialogue_Next ();
	}

	public void Pressed_Bad () {
//		StartStory ();
		Debug.LogError ("BAD!");
	}

	public void Story_Dialogue_Next() {
		// Update Button
		StoryButton_Interactive_Deactivate ();

		if (dialogueCounter >= storySetRef.talkerArr.Length) {
			thisStoryAnimType = StoryAnimType.Outro;
			Story_NewAnim_Play ();
//			Debug.LogError ("End Story!");
		} else {
			Talker_Update (storySetRef.talkerArr [dialogueCounter]);
			Story_NewAnim_Play ();
//			Debug.LogError ("Continue Story!");
		}
	}

	public void Talker_Update (TalkerType whoTalksNew) {

		if (!backFromTutorial) {
			// This is story screen
			if (whoTalksNew.ToString ().Length > 3) {
				if (whoTalks_Current != TalkerType.None) {
					whoTalks_Previous = whoTalks_Current;
					thisStoryAnimType = StoryAnimType.Story;
				} else {
					thisStoryAnimType = StoryAnimType.Intro;
				}
				whoTalks_Current = whoTalksNew;
			} 

			// This is going to tutorial fullscreen
			else {
				thisStoryAnimType = StoryAnimType.Tutorial_In;
			}
		} 
		// This is BACK from tutorial
		else {
//			Debug.LogWarning ("Yo 1");
			if (whoTalksNew != TalkerType.Tut) {
				// Tut part is over

//				Debug.LogWarning ("Yo 2");
				backFromTutorial = false;
				thisStoryAnimType = StoryAnimType.Tutorial_Out;
				whoTalks_Current = whoTalksNew;

			} else {
				// Tut part is NOT over
			
				thisStoryAnimType = StoryAnimType.Tutorial_Continue;

//				DialogueText_Update ();
			}
		}
	}

	public void DialogueText_Update () {
		DialogueText_Scale_Tut ();

		dialogueTexTHolder.text = storySetRef.dialogueArr [dialogueCounter];
		dialogueCounter++;
	}

	public void DialogueText_Scale_Moshtan () {
		if (whoTalks_CurrentString == "Moshtan") {
			dialogueArtTransHolder.localScale = new Vector3 (-1, 1, 1);
		} else {
			dialogueArtTransHolder.localScale = Vector3.one;
		}
	}

	public void DialogueText_Scale_Tut () {
		if (backFromTutorial) {
			//			tutTextUpsideDown = false;
			dialogueArtOnlyBoxTransHolder.transform.localPosition = new Vector3 (0, -25, 0);
			dialogueArtTransHolder.localScale = new Vector3 (1, -1, 1);
		} else {
			dialogueArtOnlyBoxTransHolder.transform.localPosition = Vector3.zero;
			DialogueText_Scale_Moshtan ();
		}
	}
		
	public void Story_NewAnim_Play () {
		whoTalks_PreviousString = whoTalks_Previous.ToString ();
		whoTalks_CurrentString = whoTalks_Current.ToString ();
		if (whoTalks_PreviousString.Length > 7) {
			faceShouldChange = true;
			dialogueLength = whoTalks_PreviousString.Length - 8;
			aghdasiTalks_FaceString = whoTalks_PreviousString.Substring (8, dialogueLength);
			whoTalks_PreviousString = whoTalks_PreviousString.Substring (0, 7);
		}

		if (whoTalks_CurrentString.Length > 7) {
			faceShouldChange = true;
			dialogueLength = whoTalks_CurrentString.Length - 8;
			aghdasiTalks_FaceString = whoTalks_CurrentString.Substring (8, dialogueLength);
			whoTalks_CurrentString = whoTalks_CurrentString.Substring (0, 7);
		}

		if (faceShouldChange) {
			Story_FaceUpdate ();
		}

		switch (thisStoryAnimType) {
		case StoryAnimType.Intro:
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - Intro Anim 1 (Legacy)");
			Invoke ("StoryButton_Interactive_Activate", 1.2F);

			// SOUND EFFECTS - Story "phoosh" sound (need change)
			if (SoundManager.Instance != null)
			{
				SoundManager.Instance.CoPlayDelayedClip (SoundManager.Instance.PlayerMiss, 0.13F);
			}

			break;

		case StoryAnimType.Outro:
			Story_GradsAnim_Finish ();
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - Leave Anim 1 (Legacy)");

			// SOUND EFFECTS - Story "phoosh" sound (need change)
			if (SoundManager.Instance != null)
			{
				SoundManager.Instance.CoPlayDelayedClip (SoundManager.Instance.PlayerMiss, 0.13F);
			}

			break;

		case StoryAnimType.Story:
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_PreviousString + " TO " + whoTalks_CurrentString + " Anim 1 (Legacy)");
			Invoke ("StoryButton_Interactive_Activate", 0.5F);

			// Check to see if talker changes or not
			if (whoTalks_PreviousString != whoTalks_CurrentString) {
				// SOUND EFFECTS - Story "phoosh" sound (need change)
				if (SoundManager.Instance != null) {
					SoundManager.Instance.PlayerMiss.PlayDelayed (0.13F);
					SoundManager.Instance.CoPlayDelayedClip (SoundManager.Instance.PlayerMiss, 0.3F);
				}
			}

			break;

		case StoryAnimType.Tutorial_In:
//			Debug.LogError ("TUT IN!");
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - To Tut Anim 1 (Legacy)");
			storySetRef.PlayTutorialAnim_Enter ();
			backFromTutorial = true;
			Invoke ("StoryButton_Interactive_Activate", storySetRef.tutorialWaitTime);

			// SOUND EFFECTS - Story "phoosh" sound (need change)
			if (SoundManager.Instance != null)
			{
				SoundManager.Instance.CoPlayDelayedClip (SoundManager.Instance.PlayerMiss, 0.13F);
			}

			break;

		case StoryAnimType.Tutorial_Continue:
//			Debug.LogWarning ("Tut Continue????");
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - Tut to Tut Dialogue Anim 1 (Legacy)");
			Invoke ("StoryButton_Interactive_Activate", 0.5F);
			break;

		case StoryAnimType.Tutorial_Out:
//			Debug.LogError ("TUT OUT!");
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - From Tut Anim 1 (Legacy)");
			storySetRef.PlayTutorialAnim_Leave ();
			Invoke ("StoryButton_Interactive_Activate", 0.7F);

			// SOUND EFFECTS - Story "phoosh" sound (need change)
			if (SoundManager.Instance != null)
			{
				SoundManager.Instance.CoPlayDelayedClip (SoundManager.Instance.PlayerMiss, 0.13F);
			}

			break;

		default:
			Story_GradsAnim_Finish ();
			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - Leave Anim 1 (Legacy)");
			break;
		}
		animStoryParentHolder [animStoryParentHolder.clip.name].speed = 1.3F;

//		if (!thisIsOutro) {
//			if (!thisIsIntro) {
//				animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_PreviousString + " TO " + whoTalks_CurrentString + " Anim 1 (Legacy)");
//				Invoke ("StoryButton_Interactive_Activate", 0.75F);
//			} 
//		else {
//			// This is INTRO!
//				animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - Intro Anim 1 (Legacy)");
//				Invoke ("StoryButton_Interactive_Activate", 1.5F);
//			}
//		} else {
//			// This is OUTRO
//			Story_GradsAnim_Finish ();
//			animStoryParentHolder.clip = animStoryParentHolder.GetClip ("Story - " + whoTalks_CurrentString + " - Leave Anim 1 (Legacy)");
//		}
//

		animStoryParentHolder.Play ();
    }

	public void Story_FaceUpdate () {
		switch (aghdasiTalks_FaceString) {
		case "Happy":
			storyFacesObjHoldeR_Happy.SetActive (true);
			storyFacesObjHoldeR_Normal.SetActive (false);
			storyFacesObjHoldeR_Worried.SetActive (false);
			storyFacesObjHoldeR_Crying.SetActive (false);
			break;
		case "Normal":
			storyFacesObjHoldeR_Happy.SetActive (false);
			storyFacesObjHoldeR_Normal.SetActive (true);
			storyFacesObjHoldeR_Worried.SetActive (false);
			storyFacesObjHoldeR_Crying.SetActive (false);
			break;
		case "Worried":
			storyFacesObjHoldeR_Happy.SetActive (false);
			storyFacesObjHoldeR_Normal.SetActive (false);
			storyFacesObjHoldeR_Worried.SetActive (true);
			storyFacesObjHoldeR_Crying.SetActive (false);
			break;
		case "Crying":
			storyFacesObjHoldeR_Happy.SetActive (false);
			storyFacesObjHoldeR_Normal.SetActive (false);
			storyFacesObjHoldeR_Worried.SetActive (false);
			storyFacesObjHoldeR_Crying.SetActive (true);
			break;
		default:
			Debug.LogError ("FACE = " + aghdasiTalks_FaceString);
			storyFacesObjHoldeR_Happy.SetActive (false);
			storyFacesObjHoldeR_Normal.SetActive (false);
			storyFacesObjHoldeR_Worried.SetActive (false);
			storyFacesObjHoldeR_Crying.SetActive (false);
			break;
		}
	}

	public void Story_End () {
		Story_UnPause ();
		StoryMode_DeActivate ();

		// Check to see what post-story event we should do
		storyPostScriptHolder.PostStory_Event_Start (postEventType_Current);

		// DeActivate story canvas
		StartCoroutine (StoryCanvas_Activator(false));
	}

	IEnumerator StoryCanvas_Activator (bool whichBool) {
		canvasStoryHolder.enabled = whichBool;
		graphicRayStoryHolder.enabled = whichBool;

		// Turn object off ONLY at the end of story
		if (!whichBool) {
			transform.parent.gameObject.SetActive (false);
		}

		yield return null;
	}

	public void Story_GradsAnim_Start () {
		storyGradsAnimHolder.clip = storyGradsAnimHolder.GetClip ("Story Grads Start Anim 1 (Legacy)");
		storyGradsAnimHolder.Play ();
	}

	public void Story_GradsAnim_Finish () {
		storyGradsAnimHolder.clip = storyGradsAnimHolder.GetClip ("Story Grads Finish Anim 1 (Legacy)");
		storyGradsAnimHolder.Play ();
	}

	void StoryButton_Interactive_Activate () {
		storyNextTriangleObjHolder.SetActive (true);
		storyNextTriangleAnimHolder.Stop ();
		storyNextTriangleAnimHolder.Play ();
		storyNextTriangleParticleHolder.Emit (1);

		storyButtonHolder.interactable = true;
	}

	void StoryButton_Interactive_Deactivate () {
		storyNextTriangleObjHolder.SetActive (false);
		storyNextTriangleAnimHolder.Stop ();

		storyButtonHolder.interactable = false;
	}

	public void StoryMode_Activate () {
		storyObj_DialogueHolder.SetActive (true);
		storyObj_AghdasiHolder.SetActive (true);
		storyObj_MoshtanHolder.SetActive (true);
		storyObj_GradsHolder.SetActive (true);
		storyObj_ButtonHolder.SetActive (true);

	}

	public void StoryMode_DeActivate () {
		storyObj_DialogueHolder.SetActive (false);
		storyObj_AghdasiHolder.SetActive (false);
		storyObj_MoshtanHolder.SetActive (false);
		storyObj_GradsHolder.SetActive (false);
		storyObj_ButtonHolder.SetActive (false);
	}

	public void Story_Pause () {
//		Debug.LogError ("PAUSABLE 1 = " + isPausable);

		if (isPausable) {
			EventTrigger.PauseTheGame ();
		}
	}

//	void Update () {
//		Debug.LogError ("PAUSABLE 1 = " + isPausable);
//	}

	public void Story_UnPause () {
//		Debug.LogError ("PAUSABLE 2 = " + isPausable);

		if (isPausable) {
			EventTrigger.UnPauseTheGame ();
		}
	}

	public void Story_TestPlay (bool isInGameTutorial) {
		StartStory (storySet_World, storySet_Level, storySet_Sequence, isInGameTutorial);
	}

//		if (storySetRef.talkerArr [dialogueCounter] == TalkerType.Aghdasi) {
//				DialogueBox_Posit_Aghdasi ();
//			} else {
//				DialogueBox_Posit_Moshtan ();
//			}
//	public void DialogueBox_Posit_Aghdasi () {
//		dialogueBoxTransHolder.localPosition = vec3_DialoguePosit_Aghdasi;
//		Debug.LogError ("Aghdasi Story!");
//	}
//	public void DialogueBox_Posit_Moshtan () {
//		dialogueBoxTransHolder.localPosition = vec3_DialoguePosit_Mosthan;
//		Debug.LogError ("Moshtan Story!");
//	}
}
