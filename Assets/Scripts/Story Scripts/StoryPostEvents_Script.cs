﻿using UnityEngine;
using System.Collections;

public class StoryPostEvents_Script : MonoBehaviour {

	public PowerUps_Script powUpsScriptHolder;
	public End_Level_Script endLevelScriptHolder;
	public PowerUps_Menu_Script powUpsMenuScriptHolder;

	public Worlds_All_Script worldsAllScriptHolder;
	public Endless_All_Script endlessAllScriptHolder;
	public Item_Menu_Script itemsMenucriptHolder;
	public Shop_Popup_Script shopPopupScriptHolder;
	public Popup_Achieve_Script achievesPopupScriptHolder;

	public Transform transTutBigBack;
	public Transform transTutSmallPictos;
	public Transform transTutBigPictos;

	public GameObject pauseDisableArtObjHolder;
	public GameObject powUp1DisableObjHolder;
	public GameObject powUp2DisableObjHolder;

	public GameObject objFirstTimeClickBlocker;
	public Canvas canvasFirstTimeTuts;

//	public GameObject powUp1OverButtonObjHolder;

	public Animation animTutBlackCircleHolder;
	public TutBlackCircle_Script tutBlackScriptHolder;

	private int critChance_OrigNumber;
	private int intFirstTutCounter;

	private int intTimeScale_Normal = 1;
	private int intTimeScale_Fast = 5;

	private bool isFirstEnemyDead = false;
	private char charFirstTut;

	public void PostStory_Event_Start (StoryMain_Script.PostEventType whatPostEvent) {
		switch (whatPostEvent) {
		case StoryMain_Script.PostEventType.Into_HandMove:
			PostEvent_HandMove ();
			break;
		case StoryMain_Script.PostEventType.Into_Crit:
			PostEvent_Crit_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_PowUp1:
			PostEvent_PowerUp1_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_PowUp2:
			PostEvent_PowerUp2_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_BatteryCharge:
			PostEvent_Battery_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_PhoneSC:
			PostEvent_PhoneSC_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_ItemPage:
			PostEvent_ItemPage_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_ItemProgress:
			PostEvent_ItemProgress_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_ItemRepeatItem:
			PostEvent_ItemRepeatItem_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_BatterySlot:
			PostEvent_BatterySlot_Set ();
			break;
		case StoryMain_Script.PostEventType.Into_EndlessShowLock:
			PostEvent_EndlessShowLock ();
			break;
		case StoryMain_Script.PostEventType.Into_EndlessShowLeaderboard:
			PostEvent_EndlessShowLeaderboard ();
			break;
		case StoryMain_Script.PostEventType.Into_EndlessShowPlay:
			PostEvent_EndlessShowPlay ();
			break;
		case StoryMain_Script.PostEventType.Into_2ndCost_Fat:
			PostEvent_2ndCost_Fat ();
			break;
		case StoryMain_Script.PostEventType.Into_2ndCost_Thin:
			PostEvent_2ndCost_Thin ();
			break;
		case StoryMain_Script.PostEventType.Into_2ndCost_Muscle:
			PostEvent_2ndCost_Muscle ();
			break;
		case StoryMain_Script.PostEventType.Into_2ndCost_Giant:
			PostEvent_2ndCost_Giant ();
			break;
		case StoryMain_Script.PostEventType.Into_NewYearEvent:
			PostEvent_EventNewYear ();
			break;
		case StoryMain_Script.PostEventType.None:
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}
	}

	private void PostEvent_HandMove () {
		HandMove_InteractiveActive ();
	}

	private void PostEvent_Crit_Set () {
		Crit_100Percent_Activate ();
	}

	private void PostEvent_PowerUp1_Set () {
		PowersNPause_Blocker_Activate ();
		Invoke ("PostEvent_PowerUp1_OwnIncrease", 1F);
	}

	private void PostEvent_PowerUp2_Set () {
		PowersNPause_Blocker_Activate ();
		Invoke ("PostEvent_PowerUp2_OwnIncrease", 1F);
	}

	private void PostEvent_Battery_Set () {
		PostEvent_Battery_Present ();
//		Invoke ("PostEvent_Battery_Present", 0);
	}

	private void PostEvent_PhoneSC_Set () {
		PostEvent_PhoneShortCut_GoTo ();
//		Invoke ("PostEvent_PhoneShortCut_GoTo", 0);
	}

	private void PostEvent_ItemPage_Set () {
		PostEvent_ItemPage_GoTo ();
//		Invoke ("PostEvent_ItemPage_GoTo", 0);
	}

	private void PostEvent_ItemProgress_Set () {
		PostEvent_ItemProgress_GoTo ();
//		Invoke ("PostEvent_ItemProgress_GoTo", 0);
	}

	private void PostEvent_ItemRepeatItem_Set () {
		PostEvent_ItemRepeatItem_Present ();
//		Invoke ("PostEvent_ItemRepeatItem_Present", 0);
	}

	private void PostEvent_BatterySlot_Set () {
		PostEvent_BatSlot_Present ();
	}

//	private void PostEvent_EndlessUnlock () {
//		PostEvent_Endless_PresentUnlock ();
//	}

	private void PostEvent_EndlessShowLock () {
		PostEvent_Endless_PresentShowLock ();
	}

	private void PostEvent_EndlessShowLeaderboard() {
		PostEvent_Endless_PresentShowLeaderboard ();
	}

	private void PostEvent_EndlessShowPlay () {
		PostEvent_Endless_PresentShowPlay ();
	}

	private void PostEvent_2ndCost_Fat () {
		Done_2ndCost_Fat ();
	}

	private void PostEvent_2ndCost_Thin () {
		Done_2ndCost_Thin ();
	}

	private void PostEvent_2ndCost_Muscle () {
		Done_2ndCost_Muscle ();
	}

	private void PostEvent_2ndCost_Giant () {
		Done_2ndCost_Giant ();
	}

	private void PostEvent_EventNewYear () {
		PostEvent_EventNewYearPresent ();
	}

	private void HandMove_InteractiveActive () {
		PlayerData_InGame.Instance.gameplayGestureAllowed = false;

		// Includes gesture stop, hud enter and more
		StartCoroutine (FirstTimeTutRoutine_Start_1 ());
	}

	private IEnumerator FirstTimeTutRoutine_StartTEMP() {
		yield return new WaitForSeconds (4F);
	}

	private IEnumerator FirstTimeTutRoutine_Start_1 () {
		intFirstTutCounter = 1;
		charFirstTut = 'D';

		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
		objFirstTimeClickBlocker.SetActive (true);

		canvasFirstTimeTuts.gameObject.SetActive (true);
		canvasFirstTimeTuts.enabled = true;

		Time.timeScale = intTimeScale_Fast;

		HandMove_MusicPitchSpeed (1.5F);

		yield return new WaitForSeconds (3F);
//		yield return new WaitForSeconds (5.5F);

		// Start checking to make sure enemy is in the right position
		bool isEnemyInRightZ = false;

		while (endLevelScriptHolder.enemiesSpawnHereTransHolder.childCount == 0) {
			yield return new WaitForSeconds (0.1F);
		}

		endLevelScriptHolder.FirstKillCheck_Start ();

		while (!isEnemyInRightZ) {
//			Debug.LogWarning ("Body buffer z = " + endLevelScriptHolder.transFirstEnemyRef.localPosition.z);
			yield return new WaitForSeconds (0.1F); 
			if (endLevelScriptHolder.transFirstEnemyRef.localPosition.z < 0.25F) {
				isEnemyInRightZ = true;
			}
		}

		// Show Tut parts animate (Non-HUD parts)
		HandMove_TutPartsAnimate_1 ();

		// Positions of tut parts (FIRST TIME)
		HandMove_TransScale (transTutBigBack, 1, 1);
		HandMove_TransPositRotate (transTutBigBack, 0, 0, 0);
		HandMove_TransPositRotate (transTutSmallPictos, 44, 192, 0);
		HandMove_TransPositRotate (transTutBigPictos, -237, 96.8F, 0);

//		objFirstTimeClickBlackBack.SetActive (true);

		Time.timeScale = intTimeScale_Normal;

		Debug.LogError ("YO!");

		HandMove_Freeze ();
		HandMove_MusicPitchSpeed (1);

		// Fix this and replace with event (Listener to hurt enemies)
		StartCoroutine (HandMove_EnemyChecker ());
	}

	private void HandMove_TransPositRotate (Transform transThis, float positX, float positY, int rotateZ) {
		transThis.localPosition = new Vector3 (positX, positY, 0);
		transThis.localEulerAngles = new Vector3 (0, 0, rotateZ);
	}

	private void HandMove_TransScale(Transform transThis, int scaleX, int scaleY) {
		transThis.localScale = new Vector3 (scaleX, scaleY, 1);
	}

	private void HandMove_Musicparts () {
		// Play first level's music AFTER tutorial
		StartCoroutine (HandMove_MusicPartsBlend());
	}

	private void HandMove_MusicPitchSpeed (float whichSpeed) {
		if (MusicManager.Instance != null) {
			MusicManager.Instance.audioSourceArr [0].pitch = whichSpeed;
		}
	}

	IEnumerator HandMove_MusicPartsBlend () {
		bool isMenuVolDown = false;
		while (!isMenuVolDown) {
			yield return new WaitForSeconds (0.05F);
			MusicManager.Instance.audioSourceArr [0].volume -= 0.06F;

			if (MusicManager.Instance.audioSourceArr [0].volume <= 0) {
				MusicManager.Instance.audioSourceArr [0].volume = 0;
				isMenuVolDown = true;
			}
		}

		if (MusicManager.Instance != null)
		{
			// Return music vol to normal
			MusicManager.Instance.audioSourceArr [0].volume = 1;

			MusicManager.Instance.PlayMusic_WorldCinema();
		}
	}

	private void HandMove_TutPartsAnimate_1 () {
		// Play intro HUD anim only AFTER tutorial
		PlayerData_InGame.Instance.FirstTimePlay_IntroAnimateTutparts_1 ();
	}

	private void HandMove_TutPartsAnimate_2and3 (int whichAnim) {
		// Play intro HUD anim only AFTER tutorial
		PlayerData_InGame.Instance.FirstTimePlay_IntroAnimateTutparts_2and3 (whichAnim);
	}

	private IEnumerator HandMove_EnemyChecker () {
		yield return new WaitForSeconds (0.2F);

		PlayerData_InGame.Instance.gameplayGestureAllowed = true;

		EventManager.StartListening ("Hurt Enemies", HandMove_HurtCheck);

		isFirstEnemyDead = false;
		while (!isFirstEnemyDead) {

//			Debug.LogError ("Waiting for player!");
			yield return null;

//			if (endLevelScriptHolder.enemiesSpawnHereTransHolder.childCount == 0) {
//				isFirstEnemyDead = true;
//				switch (intFirstTutCounter) {
//				case 1:
//					StartCoroutine (FirstTimeTutRoutine_End_1 ());
//					break;
//				case 2:
//					StartCoroutine (FirstTimeTutRoutine_End_2 ());
//					break;
//				case 3:
//					StartCoroutine (FirstTimeTutRoutine_End_3 ());
//					break;
//				default:
//					StartCoroutine (FirstTimeTutRoutine_End_1 ());
//					break;
//				}
//			}
		}

		switch (intFirstTutCounter) {
		case 1:
			StartCoroutine (FirstTimeTutRoutine_End_1 ());
			break;
		case 2:
			StartCoroutine (FirstTimeTutRoutine_End_2 ());
			break;
		case 3:
			StartCoroutine (FirstTimeTutRoutine_End_3 ());
			break;
		case 4:
			StartCoroutine (FirstTimeTutRoutine_End_4 ());
			break;
		default:
			StartCoroutine (FirstTimeTutRoutine_End_1 ());
			break;
		}

		EventManager.StopListening ("Hurt Enemies", HandMove_HurtCheck);
	}

	private void HandMove_HurtCheck () {
		if (EventTrigger.theKey_Shape1 [0] == charFirstTut || EventTrigger.theKey_Shape2 [0] == charFirstTut) {
			isFirstEnemyDead = true;
		}
	}

	private void HandMove_Freeze () {
		EventTrigger.FreezeTheGame ();
	}

	private void HandMove_UnFreeze () {
		EventTrigger.UnFreezeTheGame ();
	}

	private IEnumerator FirstTimeTutRoutine_End_1 () {
		HandMove_UnFreeze ();

		// Stop tut parts
		PlayerData_InGame.Instance.FirstTimePlay_StopTutparts_YesLeave ();

		yield return null;

		// Second tut routine start
		StartCoroutine (FirstTimeTutRoutine_Start_2 ());

		// Remove first time stuff (OLD) (Now after third tut)
//		StartCoroutine (FirstTimeTutRoutine_Remove ());
	}

	private IEnumerator FirstTimeTutRoutine_End_2 () {
		HandMove_UnFreeze ();

		// Stop tut parts
		PlayerData_InGame.Instance.FirstTimePlay_StopTutparts_YesLeave ();

		yield return null;

		// Second tut routine start
		StartCoroutine (FirstTimeTutRoutine_Start_3 ());

		// Remove first time stuff (OLD) (Now after third tut)
		//		StartCoroutine (FirstTimeTutRoutine_Remove ());
	}

	private IEnumerator FirstTimeTutRoutine_End_3 () {
		// No unfreeze for this bad boy!
//		HandMove_UnFreeze ();

		// Stop tut parts (Not for this one)
		PlayerData_InGame.Instance.FirstTimePlay_StopTutparts_NoLeave ();

		yield return null;

		// Second tut routine start
		StartCoroutine (FirstTimeTutRoutine_Start_4 ());

		// Remove first time stuff (OLD) (Now after third tut)
		//		StartCoroutine (FirstTimeTutRoutine_Remove ());
	}

	private IEnumerator FirstTimeTutRoutine_End_4 () {
		// Stop tut parts
		PlayerData_InGame.Instance.FirstTimePlay_StopTutparts_YesLeave ();

		// Increase player mustache by 3 (Mustache Works)
//		StartCoroutine (PlayerData_InGame.Instance.PlayerMustache_Gain (3));

		HandMove_UnFreeze ();

		// Start level music here
		yield return new WaitForSeconds (0.5F);

		HandMove_Musicparts ();

		yield return new WaitForSeconds (1.25F);

		// Leave tut parts (End of all)
		PlayerData_InGame.Instance.FirstTimePlay_StopTutparts_EndAll ();

		yield return null;

		// Second tut routine start
		StartCoroutine (FirstTimeTutRoutine_Remove ());
	}

	private IEnumerator FirstTimeTutRoutine_Start_2 () {
		intFirstTutCounter = 2;
		charFirstTut = 'U';

		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
		objFirstTimeClickBlocker.SetActive (true);

		yield return new WaitForSeconds (1);

		HandMove_MusicPitchSpeed (1.5F);
		Time.timeScale = intTimeScale_Fast;

		yield return new WaitForSeconds (3);

		// Start checking to make sure enemy is in the right position
		bool isEnemyInRightZ = false;

		while (endLevelScriptHolder.enemiesSpawnHereTransHolder.childCount == 0) {
			yield return new WaitForSeconds (0.1F);
		}

		endLevelScriptHolder.FirstKillCheck_Start ();

		while (!isEnemyInRightZ) {
//			Debug.LogWarning ("Body buffer z = " + endLevelScriptHolder.transFirstEnemyRef.localPosition.z);
			yield return new WaitForSeconds (0.1F); 
			if (endLevelScriptHolder.transFirstEnemyRef.localPosition.z < 0.25F) {
				isEnemyInRightZ = true;
			}
		}

		// Show Tut parts animate (Non-HUD parts)
		HandMove_TutPartsAnimate_2and3 (2);

		// Positions of tut parts (SECOND TIME)
		HandMove_TransScale (transTutBigBack, 1, -1);
		HandMove_TransPositRotate (transTutBigBack, 520, 0, 0);
		HandMove_TransPositRotate (transTutSmallPictos, -174, 132, 180);
		HandMove_TransPositRotate (transTutBigPictos, 237, -90, 180);

		Time.timeScale = intTimeScale_Normal;

		HandMove_Freeze ();
		HandMove_MusicPitchSpeed (1);

		// Fixed this and replaced it with event (Listener to hurt enemies)
		StartCoroutine (HandMove_EnemyChecker ());
	}

	private IEnumerator FirstTimeTutRoutine_Start_3 () {
		intFirstTutCounter = 3;
		charFirstTut = 'L';

		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
		objFirstTimeClickBlocker.SetActive (true);

		yield return new WaitForSeconds (1);

		HandMove_MusicPitchSpeed (1.5F);
		Time.timeScale = intTimeScale_Fast;

		yield return new WaitForSeconds (3);

		// Start checking to make sure enemy is in the right position
		bool isEnemyInRightZ = false;

		while (endLevelScriptHolder.enemiesSpawnHereTransHolder.childCount == 0) {
			yield return new WaitForSeconds (0.1F);
		}

		endLevelScriptHolder.FirstKillCheck_Start ();

		while (!isEnemyInRightZ) {
//			Debug.LogWarning ("Body buffer z = " + endLevelScriptHolder.transFirstEnemyRef.localPosition.z);
			yield return new WaitForSeconds (0.1F); 
			if (endLevelScriptHolder.transFirstEnemyRef.localPosition.z < 0.25F) {
				isEnemyInRightZ = true;
			}
		}

		// Show Tut parts animate (Non-HUD parts)
		HandMove_TutPartsAnimate_2and3 (3);

		// Positions of tut parts (THIRD TIME)
		HandMove_TransScale (transTutBigBack, 1, 1);
		HandMove_TransPositRotate (transTutBigBack, 0, 0, 0);
		HandMove_TransPositRotate (transTutSmallPictos, 70, 163, -90);
		HandMove_TransPositRotate (transTutBigPictos, -130, 0, -90);

		Time.timeScale = intTimeScale_Normal;

		HandMove_Freeze ();
		HandMove_MusicPitchSpeed (1);

		// TODO: Fix this and replace with event (Listener to hurt enemies)
		StartCoroutine (HandMove_EnemyChecker ());
	}

	private IEnumerator FirstTimeTutRoutine_Start_4 () {
		intFirstTutCounter = 4;
		charFirstTut = 'R';

		PlayerData_InGame.Instance.gameplayGestureAllowed = false;
		objFirstTimeClickBlocker.SetActive (true);

		// Because both hits are for one enemy, the following parts were turned into comment

		yield return null;

//		Time.timeScale = intTimeScale_Fast;
//		yield return new WaitForSeconds (4);
//
//		bool isEnemyInRightZ = false;
//
//		while (endLevelScriptHolder.enemiesSpawnHereTransHolder.childCount == 0) {
//			yield return new WaitForSeconds (0.1F);
//		}
//
//		endLevelScriptHolder.FirstKillCheck_Start ();
//
//		while (!isEnemyInRightZ) {
//			Debug.LogWarning ("Body buffer z = " + endLevelScriptHolder.transFirstEnemyRef.localPosition.z);
//			yield return new WaitForSeconds (0.01F); 
//			if (endLevelScriptHolder.transFirstEnemyRef.localPosition.z < 0.25F) {
//				isEnemyInRightZ = true;
//			}
//		}

		// Show Tut parts animate (Non-HUD parts)
		HandMove_TutPartsAnimate_2and3 (4);

		// Positions of tut parts (FOURTH TIME)
		HandMove_TransScale (transTutBigBack, 1, 1);
		HandMove_TransPositRotate (transTutBigBack, 0, 0, 0);
		HandMove_TransPositRotate (transTutSmallPictos, 13, 163, 90);
		HandMove_TransPositRotate (transTutBigPictos, -330, 0, 90);

		Time.timeScale = intTimeScale_Normal;

		// Wasn't unfreezed.
//		HandMove_Freeze ();

		StartCoroutine (HandMove_EnemyChecker ());
	}

	private IEnumerator FirstTimeTutRoutine_Remove () {
		yield return new WaitForSeconds (2);
		objFirstTimeClickBlocker.SetActive (false);

		canvasFirstTimeTuts.gameObject.SetActive (false);
		canvasFirstTimeTuts.enabled = false;
	}

	private void Crit_100Percent_Activate () {
		if (PlayerData_InGame.Instance != null) {
			critChance_OrigNumber = PlayerData_InGame.Instance.p1_CritChance;
			PlayerData_InGame.Instance.p1_CritChance = 100;

			Invoke ("Crit_100Percent_DeActivate", 4F);
		}
	}

	private void Crit_100Percent_DeActivate () {
		if (PlayerData_InGame.Instance != null) {
			PlayerData_InGame.Instance.p1_CritChance = critChance_OrigNumber;
		}
	}

	private void PostEvent_PowerUp1_OwnIncrease () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition (new Vector3 (416, -206, 0), Vector3.one);
		
		PostEvent_PowerUp1_InteractiveStart ();
		EventTrigger.PauseTheGame ();

//		powUp1OverButtonObjHolder.SetActive (true);

		if (PlayerData_InGame.Instance.p1_PowUp1_Count_Own == 0) {
			PlayerData_InGame.Instance.p1_PowUp1_Count_Own = 1;
			PlayerData_InGame.Instance.p1_PowUp1_Count_Ready = 1;
		}

		powUpsScriptHolder.FirstTimePowerUp_ForTutorial (1);
		PowersNPause_Blocker_DeActivate ();
	}

	private void PostEvent_PowerUp2_OwnIncrease () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition (new Vector3 (-416, -206, 0), new Vector3 (-1 ,1 ,1));
//		Debug.LogWarning ("YO!");

		PostEvent_PowerUp2_InteractiveStart ();
		EventTrigger.PauseTheGame ();

//		powUp1OverButtonObjHolder.SetActive (true);

		if (PlayerData_InGame.Instance.p1_PowUp2_Count_Own == 0) {
			PlayerData_InGame.Instance.p1_PowUp2_Count_Own = 1;
			PlayerData_InGame.Instance.p1_PowUp2_Count_Ready = 1;
		}

		powUpsScriptHolder.FirstTimePowerUp_ForTutorial (2);
		PowersNPause_Blocker_DeActivate ();
	}

	private void PowersNPause_Blocker_Activate () {
		pauseDisableArtObjHolder.SetActive(true);
		if (PlayerData_InGame.Instance.p1_PowUp1_Count_Ready > 0) {
			powUp1DisableObjHolder.SetActive (true);
		}
		if (PlayerData_InGame.Instance.p1_PowUp2_Count_Ready > 0) {
			powUp2DisableObjHolder.SetActive (true);
		}
	}

	private void PowersNPause_Blocker_DeActivate () {
		pauseDisableArtObjHolder.SetActive(false);
		powUp1DisableObjHolder.SetActive(false);
		powUp2DisableObjHolder.SetActive(false);
	}

	private void PostEvent_PowerUp1_InteractiveStart () {
		animTutBlackCircleHolder.Play ();
	}

	private void PostEvent_PowerUp2_InteractiveStart () {
		animTutBlackCircleHolder.Play ();
	}

	private void PostEvent_Battery_InteractiveStart () {
		animTutBlackCircleHolder.Play ();
	}

	private void PostEvent_MenuAny_InteractiveStart () {
//		Debug.LogWarning ("INTERACTIVE 1!");
		animTutBlackCircleHolder.Play ();
	}

	private void PostEvent_MenuAny_InteractiveStop () {
//		Debug.LogWarning ("INTERACTIVE 2!");
		animTutBlackCircleHolder.Stop ();
	}

	private void PostEvent_Battery_Present () {

		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (0, -166, 0));

		powUpsMenuScriptHolder.BatteryFirstTime_ObjActive ();

		PostEvent_MenuAny_InteractiveStart ();

		// Proper check to remove pow ups
//		if (PlayerData_Main.Instance.player_PowUp1_Ready > 0) {
//
//			// Update Charge Time
//			PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
//			PlayerData_Main.Instance.player_PowUp1_Own = 1;
//			PlayerData_Main.Instance.player_PowUp1_Ready = 0;
//		}

		// Moved to menu tut check
		PostEvent_BatteryTutSetup ();

		// WTF! It should NOT be ingame
//		if (PlayerData_InGame.Instance.p1_PowUp1_Count_Ready > 0) {
//			PlayerData_InGame.Instance.p1_PowUp1_Count_Ready = 0;
//
//			// Update Charge Time
//			PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
//			PlayerData_InGame.Instance.p1_PowUp1_Count_Ready = 0;
//		}

//		powUpsMenuScriptHolder.FirstTimeBattery_ForTutorial ();
	}

	private void PostEvent_PhoneShortCut_GoTo () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-330, 216, 0));
		tutBlackScriptHolder.TutBlack_ToCircleVarStuff_Value (0.8F);

		// Mohammad
//		if (QuestManager.Instance != null) {
//			QuestManager.Instance.QuestGiver_FirstTime ();
//		}

		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.PhoneSC_FrontOverActivator ();
	}

	public void PostEvent_PhoneStats_Show () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (125, 20, 0));
		tutBlackScriptHolder.TutBlack_ToSquareStuff ();

		PostEvent_MenuAny_InteractiveStop ();
		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.PhoneStats_FrontOverActivator ();
	}

	public void PostEvent_PhoneQuest_Show () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-125, 20, 0));
		tutBlackScriptHolder.TutBlack_ToSquareStuff ();

		PostEvent_MenuAny_InteractiveStop ();
		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.PhoneQuests_FrontOverActivator ();
	}

	private void PostEvent_ItemPage_GoTo () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (127, -360, 0));
		tutBlackScriptHolder.TutBlack_ToRectangleSmallStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.ToItemPage_FrontOverActivator ();
	}

	private void PostEvent_ItemProgress_GoTo () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (0, 80, 0));
//		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (249, 80, 0));
		tutBlackScriptHolder.TutBlack_ToCircleSmallStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		itemsMenucriptHolder.ItemProgress_FrontOverActivator ();
	}

	private void PostEvent_ItemRepeatItem_Present () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
//		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (17, -131, 0));
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-314, -177, 0));

		// New Form
		tutBlackScriptHolder.TutBlack_ToOvalSmallStuff ();

		// Old Form
//		tutBlackScriptHolder.TutBlack_ToRectangleBigStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		itemsMenucriptHolder.ItemRepeatItem_FrontOverActivator ();
	}

	public void PostEvent_BatteryTutSetup () {
		powUpsMenuScriptHolder.PowerUpBatteryTut_Setup ();
	}

	private void PostEvent_BatSlot_Present () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-424, -310, 0));
		tutBlackScriptHolder.TutBlack_ToCircleSmallStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.BatterySlot_FrontOverActivator ();
	}

//	private void PostEvent_Endless_PresentUnlock () {
//		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
//		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-85, -125, 0));
//		tutBlackScriptHolder.TutBlack_ToShapeEndless ();
//
//		PostEvent_MenuAny_InteractiveStart ();
//
////		worldsAllScriptHolder.ToItemPage_FrontOverActivator ();
//	}

	private void PostEvent_Endless_PresentShowLock () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-85, -125, 0));
		tutBlackScriptHolder.TutBlack_ToShapeEndless ();

		PostEvent_MenuAny_InteractiveStart ();

		worldsAllScriptHolder.mainMenuScripHolder.endlessButtonHolder.onClick.Invoke ();

        // SOUNDD EFFECTS - Unlock Endless Menu Button (done endless)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.EndlessUnlock.Play();
        }

		//		worldsAllScriptHolder.ToItemPage_FrontOverActivator ();
	}

	private void PostEvent_Endless_PresentShowLeaderboard () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (-145, 40, 0));
		tutBlackScriptHolder.TutBlack_ToCircleNormalStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		//		worldsAllScriptHolder.ToItemPage_FrontOverActivator ();
	}

	private void PostEvent_Endless_PresentShowPlay () {
		tutBlackScriptHolder.TutBlack_ObjectsChange_Activate ();
		tutBlackScriptHolder.TutBlack_Reposition_WithBlocker (new Vector3 (306, 12, 0));
		tutBlackScriptHolder.TutBlack_ToCircleNormalStuff ();

		PostEvent_MenuAny_InteractiveStart ();

		//		worldsAllScriptHolder.ToItemPage_FrontOverActivator ();
	}

	private void Done_2ndCost_Fat (){
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.TutSeen_2ndCostume_Fat = true;
		}
	}

	private void Done_2ndCost_Thin (){
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.TutSeen_2ndCostume_Thin = true;
		}
	}

	private void Done_2ndCost_Muscle (){
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.TutSeen_2ndCostume_Muscle = true;
		}
	}

	private void Done_2ndCost_Giant (){
		if (PlayerData_Main.Instance != null) {
			PlayerData_Main.Instance.TutSeen_2ndCostume_Giant = true;
		}
	}
		
	private void PostEvent_EventNewYearPresent () {
		itemsMenucriptHolder.rewardMachineScriptHolder.GetReward_DailyRewardFull (1397);
		itemsMenucriptHolder.rewardMachineScriptHolder.gameObject.SetActive (true);
		itemsMenucriptHolder.rewardMachineScriptHolder.CallFirstChest ();

		// This is to make sure items are not updated after reward machine (Because Items menu is OFF)
		itemsMenucriptHolder.rewardMachineScriptHolder.isDontUpdateItems = true;
	}

//	void Update () {
//		if (Input.GetKeyDown ("m")) {
//			Debug.LogError ("NOW 3!");
//			PostEvent_PhoneShortCut_GoTo ();
//			PostEvent_PhoneStats_Show ();
//			PostEvent_ItemPage_GoTo ();
//			PostEvent_ItemProgress_GoTo ();
//		}
//	}
}