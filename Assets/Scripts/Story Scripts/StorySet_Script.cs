﻿using UnityEngine;
using System.Collections;

public class StorySet_Script : MonoBehaviour {

	public int tutorialWaitTime;
	public Animation tutorialAnimHolder;

	[Header("Who talks")]
	public StoryMain_Script.TalkerType[] talkerArr;
	[Header("What they say")]
	[TextArea]
	public string[] dialogueArr;
	[Header("What happens after dialogue?")]
	public StoryMain_Script.PostEventType postStoryEventType;

	void Start () {
	}

	public void PlayTutorialAnim_Enter () {
		tutorialAnimHolder.Play ("Tutorial Phone Enter Anim 1 (Legacy)");
	}

	public void PlayTutorialAnim_Leave () {
		tutorialAnimHolder.Play ("Tutorial Phone Leave Anim 1 (Legacy)");
	}
}
