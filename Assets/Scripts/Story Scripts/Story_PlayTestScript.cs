﻿using UnityEngine;
using System.Collections;

public class Story_PlayTestScript : MonoBehaviour {

	public StoryMain_Script storyMainScriptHolder;

	[Header ("What World?")]
	public int WorldNumber;
	[Header ("What Level?")]
	public int LevelNumber;
	[Header ("Which Set?")]
	public int SetNumber;
	[Header ("Is Tutorial For An In-Game Scene?")]
	public bool IsInGameTutorial;

	public void Story_PlayTest () {
		storyMainScriptHolder.StartStory (WorldNumber, LevelNumber, SetNumber - 1, IsInGameTutorial);
	}
}
