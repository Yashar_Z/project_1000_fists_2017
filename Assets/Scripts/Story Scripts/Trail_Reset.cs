﻿using UnityEngine;
using System.Collections;

public class Trail_Reset : MonoBehaviour {

	public TrailRenderer trailHolder;
	
	public void TrailReset () {
		trailHolder.Clear ();
	}
}
