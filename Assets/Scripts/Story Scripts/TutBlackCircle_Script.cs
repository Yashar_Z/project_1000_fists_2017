﻿using UnityEngine;
using System.Collections;

public class TutBlackCircle_Script : MonoBehaviour {

	public GameObject tutBlackCircleObjHolder;
	public GameObject tutBlackCircleBlockerHolder;

	public Canvas canvasTutBack_CircleHolder;
	public Canvas canvasTutBack_BackBlockHolder;

	public UnityEngine.UI.Image imageTutCircleHolder;

	public Animation animTutBlackHolder;

	public void AnimTutBlack_PlayIdle () {
		animTutBlackHolder.Play ("Story - Tutorial Black Circle Idle Anim 1 (Legacy)");
	}

	public void TutBlack_ObjectsChange_Activate () {
		// Scale the whole thing to 9 times so the animation start of 9 looks smooth
		tutBlackCircleObjHolder.transform.localScale = new Vector3 (9, 9, 9);
		tutBlackCircleObjHolder.SetActive (true);
		tutBlackCircleBlockerHolder.SetActive (true);

		// Canvas Tut Enable
		StartCoroutine (TutBack_CanvasController (true));
	}

	public void TutBlack_ObjectsChange_DeActivate () {
		tutBlackCircleObjHolder.SetActive (false);
		tutBlackCircleBlockerHolder.SetActive (false);

		// Canvas Tut Disable
		StartCoroutine (TutBack_CanvasController (false));
	}

	public void TutBlack_Reposition (Vector3 newPos, Vector3 newBlockerScale) {
		tutBlackCircleObjHolder.transform.localPosition = newPos;
		tutBlackCircleBlockerHolder.transform.localScale = newBlockerScale;
		imageTutCircleHolder.enabled = true;

//		Debug.LogError ("TUT POS: " + tutBlackCircleObjHolder.transform.localPosition);
	}

	public void TutBlack_Reposition_WithBlocker (Vector3 newPos) {
		tutBlackCircleObjHolder.transform.localPosition = newPos;
		tutBlackCircleBlockerHolder.transform.localPosition = newPos;
		imageTutCircleHolder.enabled = true;

//		Debug.LogError ("TUT POS With Blocker (Circle): " + tutBlackCircleObjHolder.transform.localPosition);
//		Debug.LogError ("TUT POS With Blocker (Blocker: " + tutBlackCircleBlockerHolder.transform.localPosition);
	}

	public void TutBlack_ToSquareStuff () {
		transform.localScale = new Vector3 (1.25F, 1, 1);
		imageTutCircleHolder.enabled = false;
	}

	public void TutBlack_ToRectangleSmallStuff () {
		transform.localScale = new Vector3 (1.25F, 0.36F, 1);
		imageTutCircleHolder.enabled = false;
	}

	public void TutBlack_ToRectangleBigStuff () {
		transform.localScale = new Vector3 (2.4F, 0.7F, 1);
		imageTutCircleHolder.enabled = false;
	}

	public void TutBlack_ToShapeEndless () {
		transform.localScale = new Vector3 (1.4F, 1.4F, 1);
		imageTutCircleHolder.enabled = true;
	}

	public void TutBlack_ToCircleSmallStuff () {
		transform.localScale = new Vector3 (0.6F, 0.6F, 0.6F);
		imageTutCircleHolder.enabled = true;
	}

	public void TutBlack_ToOvalSmallStuff () {
//		transform.localScale = new Vector3 (0.7F, 0.83F, 0.7F);
		transform.localScale = new Vector3 (0.62F, 0.72F, 0.62F);
		imageTutCircleHolder.enabled = true;
	}

	public void TutBlack_ToCircleVarStuff_Value (float whichSize) {
		transform.localScale = new Vector3 (whichSize, whichSize, whichSize);
		imageTutCircleHolder.enabled = true;
	}

	public void TutBlack_ToCircleNormalStuff () {
		transform.localScale = Vector3.one;
		imageTutCircleHolder.enabled = true;
	}

	public IEnumerator TutBack_CanvasController (bool isActive) {
		canvasTutBack_CircleHolder.enabled = isActive;
		canvasTutBack_BackBlockHolder.enabled = isActive;
		yield return null;
	}
}
