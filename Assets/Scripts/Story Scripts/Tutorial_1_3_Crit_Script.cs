﻿using UnityEngine;
using System.Collections;

public class Tutorial_1_3_Crit_Script : MonoBehaviour {

	public GameObject tutArmObjHolder;
	public GameObject tutArmFistsHolder;
	public TrailRenderer trailHolder;

	// Use this for initialization
	void Awake () {
		tutArmObjHolder.SetActive (false);
		tutArmFistsHolder.SetActive (false);
		trailHolder.sortingLayerName = "UI Layer";
		trailHolder.sortingOrder = 9;
	}

	public void TrailReset () {
		trailHolder.Clear ();
	}
}
