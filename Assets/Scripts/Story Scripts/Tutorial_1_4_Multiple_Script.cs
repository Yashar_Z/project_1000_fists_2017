﻿using UnityEngine;
using System.Collections;

public class Tutorial_1_4_Multiple_Script : MonoBehaviour {

	public GameObject tutNonArmObjHolder;
	public GameObject tutArmFistsHolder;
	public TrailRenderer trailHolder;

	// Use this for initialization
	void Awake () {
		tutNonArmObjHolder.SetActive (false);
		tutArmFistsHolder.SetActive (false);
		trailHolder.sortingLayerName = "UI Layer";
		trailHolder.sortingOrder = 9;
	}

	public void TrailReset () {
		trailHolder.Clear ();
	}
}
