﻿using UnityEngine;
using System.Collections;

public class Tutorial_1_6_Pickups_Script : MonoBehaviour {

	public GameObject tutItemsObjHolder;

	public Animation animTutPickupolder;

	// Use this for initialization
	void Awake () {
		tutItemsObjHolder.SetActive (false);
		animTutPickupolder [animTutPickupolder.clip.name].speed = 0.4F;
	}
}
