﻿using UnityEngine;
using System.Collections;

public class Tutorial_1_7_Vn8_Script : MonoBehaviour {

	public GameObject tutArmObjHolder;
	public GameObject tutArmEnemyHolder;
	public TrailRenderer trailHolder;

	void Awake () {
		tutArmObjHolder.SetActive (false);
		tutArmEnemyHolder.SetActive (false);
		trailHolder.sortingLayerName = "UI Layer";
		trailHolder.sortingOrder = 9;
	}

	public void TrailReset () {
		trailHolder.Clear ();
	}
}
