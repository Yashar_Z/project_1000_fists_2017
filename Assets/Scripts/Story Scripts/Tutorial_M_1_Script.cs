﻿using UnityEngine;
using System.Collections;

public class Tutorial_M_1_Script : MonoBehaviour {

	public ParticleSystem particleBatteryChargeHolder;

	public void BatterParticleEmit () {
		particleBatteryChargeHolder.Emit (1);
	}
}
