using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VAS_MenuController_Script : MonoBehaviour {

	public static VAS_MenuController_Script Instance;

	private static readonly string APP_ID = "4";
    private static readonly string VAS_RECIEVER_1Sibil = "405995";
    private static readonly string VAS_RECIEVER_2Mosht = "3075038";
    private string VAS_RECIEVER = "";

    private static readonly string ADS_COMPANIES_CONFIRMATION_TEXT = "sbapp-hubapp-autumn-0";

    public static System.Action<string> OnVAS_PurchaseSuccessful;
	public static System.Action OnVAS_PurchaseUnsuccessful;

	// For Mohammad Logics
	private WWW registerResponse;
	private WWW validationResponse;
	private WWW resendValidationResponse;
	private WWW checkSubscriptionResponse;
	private WWW IAPResponse;
	private WWW ConfirmIAPResponse;

	public Text EnterPhoneTextHolder;
	public Text EnterValidationTextHolder;
	public Text EnterShopResponseTextHolder;

	public Text textVAS_RecieverNumber_NumberNotReg;
	public Text textVAS_RecieverNumber_FastSbus;

	public Text textStatus;

	public GameObject[] objVAS_PopUpsArr;
	public Canvas canvasHolder;
	public GameObject objEventSystemHolder;
	public GraphicRaycaster graphicRayCasterHolder;

	public Popup_ZeroInput_Script popZeroInputScriptHolder;

	// For VAS shop packages
	public string SelectedShopID;

	public string PhoneNumber;
	public string UserId;
	public string AccessToken;
	public string LastPurchaseTransactionId;

	public string RegisterUniqueToken;
	public string RegisterTransactionId;

	public string IapUniqueToken;

	void Awake () {
		Instance = this;
		VAS_Controller_Activator (false);

		DontDestroyOnLoad (this.gameObject);

		// Number Texts 
		textVAS_RecieverNumber_NumberNotReg.text = VAS_RECIEVER;
		textVAS_RecieverNumber_FastSbus.text = VAS_RECIEVER;
	}

	public void Start_VAS_Check () {
		// To empty / delete playprefs

		//PlayerPrefs.SetString("PhoneNumber", "");
		//PlayerPrefs.SetString("UserId", "");
		//PlayerPrefs.SetString("AccessToken", "");

		PhoneNumber = PlayerPrefs.GetString("PhoneNumber", "");
		UserId = PlayerPrefs.GetString("UserId", "");
		AccessToken = PlayerPrefs.GetString("AccessToken", "");

		Debug.LogError ("PlayPrefs.PhoneNumber = " + PhoneNumber);

        // Set VAS Reciever Final Based on Build
        switch (PlayerData_Main.Instance.whatVAS_Type)
        {
            case PlayerData_Main.VAS_NameType.sibile:
                VAS_RECIEVER = VAS_RECIEVER_1Sibil;
                break;
            case PlayerData_Main.VAS_NameType.moshte:
                VAS_RECIEVER = VAS_RECIEVER_2Mosht;
                break;
            case PlayerData_Main.VAS_NameType.other:
                break;
            default:
                break;
        }

		// TODO: local save ro check kon agar phone number ya userid dar on nabood enterphone ro neshon bede
		if ((PhoneNumber == "") || (UserId == "") || (AccessToken == ""))
		{
			VAS_Controller_Activator (true);

			//			pState = PopUpState.EnterPhone;
			PopUp_VAS_OpenThis (0);
			textStatus.text = "";
		}
		else
		{
			//			pState = PopUpState.NoPopUp;
			//			textStatus.text = "checking subscription";
			//			CheckSubscription();

			// Allows to continue (Either to tutorial or to menu);

			//			Debug.LogError ("Yo 1");
			ContinueToGame_FromLoader ();
		}
	}

	public void PopUp_VAS_OpenThis (int whichInt) {
		objVAS_PopUpsArr [whichInt].SetActive (true);
		//		Debug.LogError ("Yo!?");

		switch (whichInt) {
		case 0:
		case 1:
		case 2:
			break;
		case 3:
			textVAS_RecieverNumber_NumberNotReg.text = VAS_RECIEVER;
			break;
		case 4:
			textVAS_RecieverNumber_FastSbus.text = VAS_RECIEVER;
			break;
		default:
			break;
		}
	}

	public void Pressed_PhoneEnter_Accept () {
		Debug.LogError ("Pressed_PhoneEnter_Accept");

		string pn = EnterPhoneTextHolder.text;
		Debug.Log(pn);
		string android_id = "12345";

		#if !UNITY_EDITOR && UNITY_ANDROID
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
		AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
		android_id = secure.CallStatic<string>("getString", contentResolver, "android_id");
		#endif
		Debug.Log(android_id);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("DeviceId", android_id);
		postParameters.Add("Platform", "Android");
		postParameters.Add("PlatformVersion", "1");
		postParameters.Add("SimType", "HA");
		postParameters.Add("PhoneNumber", pn);
		postParameters.Add("AppId", APP_ID);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		registerResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/Login/Register",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("enter Phone post completed");
				PhoneNumber = pn;
				PlayerPrefs.SetString("PhoneNumber", PhoneNumber);
				//				pState = PopUpState.EnterValidation;
				textStatus.text = "";
			});
		StartCoroutine(AmaliatRegister(() =>
			{
				Debug.Log("json parse complete");
			}
		));

		//		pState = PopUpState.NoPopUp;
		textStatus.text = "dar hale ferestandan etelat be server baraye ersal code taeedie";
	}

	private class RegisterResponseModel
	{
		public string UniqueToken;
		public string TransactionId;
		public string Message;
		public string Result;
	}

	public IEnumerator AmaliatRegister(System.Action OnComplete)
	{
		Popup_VasCircleLoading_Start ("ﺕﺎﻋﻼﻃﺍ ﻝﺎﺳﺭﺍ ﻝﺎﺣ ﺭﺩ");

		while (!registerResponse.isDone)
			yield return null;

		if (registerResponse.text != "") {

			Debug.Log (registerResponse.text);

			RegisterResponseModel x = JsonUtility.FromJson<RegisterResponseModel> (registerResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);
			Debug.Log(x.UniqueToken);
			Debug.Log(x.TransactionId);

			if (OnComplete != null) {
				OnComplete ();
			}
			if (x.Result == "successful") {
				Debug.Log ("register completed");

				RegisterUniqueToken = x.UniqueToken;
				RegisterTransactionId = x.TransactionId;

				PopUp_VAS_OpenThis (1);
			} else {
				// TODO: Message for phone entery problem (Server problem, number problem and more...). Needs new pop-up

				PopUp_VAS_OpenThis (1);

				Debug.Log (x.Message);
				//			pState = PopUpState.EnterPhone;
			}

			Popup_VasCircleLoading_End ();
		} 

		// Server response was empty (didn't work)
		else {
			//			textStatus.text = "server rid. anjam nashod";
			PopUp_VAS_OpenThis (0);

			Moshtan_Utilties_Script.ShowToast ("اشکال در سرور");

			Popup_VasCircleLoading_End ();
		}
	}

	public void Pressed_VerifyNumber_Accept () {
		Debug.LogError ("Pressed_VerifyNumber_Accept");

		string vc = EnterValidationTextHolder.text;
		Debug.Log(vc);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("ValidationCode", vc);
		postParameters.Add("PhoneNumber", PhoneNumber);
		postParameters.Add("AppId", APP_ID);
		postParameters.Add("UniqueToken", RegisterUniqueToken);
		postParameters.Add("OTPTransactionId", RegisterTransactionId);
        postParameters.Add("Source", ADS_COMPANIES_CONFIRMATION_TEXT);

        var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		validationResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/Login/Validate",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("validation post completed");
				//				pState = PopUpState.NoPopUp;
				textStatus.text = "";
			});
		StartCoroutine(AmaliatValidation(() => { Debug.Log("json parse complete"); }));

		textStatus.text = "dar halle barresi code validation";
		//		pState = PopUpState.NoPopUp;
	}

	public IEnumerator AmaliatValidation(System.Action OnComplete)
	{
		Popup_VasCircleLoading_Start ("ﺕﺎﻋﻼﻃﺍ ﻝﺎﺳﺭﺍ ﻝﺎﺣ ﺭﺩ");

		while (!validationResponse.isDone)
			yield return null;

		if (validationResponse.text != "") {

			Debug.Log (validationResponse.text);

			var x = JsonUtility.FromJson<ValidationResponseModel> (validationResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);
			Debug.Log (x.AccessToken);
			Debug.Log (x.UserId);
			Debug.Log (x.ValidDays);

			if (OnComplete != null) {
				OnComplete ();
			}
			if (x.Result == "successful") {
				// Code was valid

				// TODO: Save valid. 

				//			textStatus.text = "ba tashakor sms welocome alan vasat miad";
				textStatus.text = "";
				Debug.LogError ("x userID = " + x.UserId);
				PlayerPrefs.SetString ("UserId", x.UserId);
				PlayerPrefs.SetString ("AccessToken", x.AccessToken);
				UserId = x.UserId;
				AccessToken = x.AccessToken;

				// Now can continue the game
				//				Debug.LogError ("Yo 2");
				ContinueToGame_FromLoader ();
			} else {
				// Code was invalid
				PopUp_VAS_OpenThis (2);
				textStatus.text = "";

				//			textStatus.text = "code validation eshtebah bood";
				//			pState = PopUpState.ResendValidation;
			}

			Popup_VasCircleLoading_End ();
		} 

		// Time out or another problem from server
		else {
			textStatus.text = "Server ride.";

			// Open Servers fail popup
			PopUp_VAS_OpenThis (10);

			Moshtan_Utilties_Script.ShowToast (".اشکال در سرور");

			//			PopUp_VAS_OpenThis (1);
		}
	}

	public class ValidationResponseModel
	{
		public string Message;
		public string Result;
		public string AccessToken;
		public string UserId;
		public int ValidDays;
	}

	public void Pressed_VerifyNumber_ResendValid () {
		StartCoroutine (Pressed_VerifyNumber_ResendValidCoroutine ());
	}

	public IEnumerator Pressed_VerifyNumber_ResendValidCoroutine () {
		Pressed_VerifyNumber_ResendValidNOW ();
		yield return new WaitForSeconds (1);
		PopUp_VAS_OpenThis (1);
	}

	public void Pressed_VerifyNumber_ResendValidNOW () {
		Debug.LogError ("Pressed_VerifyNumber_ResendValid");

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("PhoneNumber", PhoneNumber);
		postParameters.Add("AppId", APP_ID);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		resendValidationResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/Login/ResendValidationCode",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("resend validation post completed");
				textStatus.text = "";
			});
		StartCoroutine(AmaliatResendValidation(() => { Debug.Log("json parse complete"); }));

		textStatus.text = "dar halle ersal mojadad validation code";
		//		pState = PopUpState.EnterValidation;
	}

	public IEnumerator AmaliatResendValidation(System.Action OnComplete)
	{
		while (!resendValidationResponse.isDone)
			yield return null;

		Debug.Log(resendValidationResponse.text);

		var x = JsonUtility.FromJson<ResendValidationResponseModel>(resendValidationResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Result == "successful")
		{
			Debug.Log(x.Result);
			RegisterUniqueToken = x.UniqueToken;
			RegisterTransactionId = x.TransactionId;
		}
		else
		{
			Debug.Log(x.Message);
		}
	}

	public class ResendValidationResponseModel
	{
		public string Message;
		public string Result;
		public string UniqueToken;
		public string TransactionId;
	}

	public void Pressed_VerifyNumber_ResendPhone () {
		Debug.LogError ("Pressed_VerifyNumber_ResendPhone");

		PopUp_VAS_OpenThis (0);
	}

	public void Pressed_CodeNotCorr_TryAgain () {
		Debug.LogError ("Pressed_CodeNotCorr_TryAgain");

		PopUp_VAS_OpenThis (1);
	}

	public void Pressed_NumberNotReg_Accept () {
		Debug.LogError ("Pressed_NumberNotReg_Accept");

		Application.Quit ();
	}

	public void Pressed_FastSubscribe_FastSub () {
		Debug.LogError ("Pressed_FastSubscribe_FastSub");

		Application.OpenURL("sms:" + VAS_RECIEVER + "?body=1");
		Application.Quit ();
	}

	public void Pressed_FastSubscribe_Exit () {
		Debug.LogError ("Pressed_FastSubscribe_Exit");

		Application.Quit ();
	}

	public void Pressed_ExitCharge_Exit () {
		Debug.LogError ("Pressed_ExitCharge_Exit");

		Application.Quit ();
	}

	public void CheckSubscription()
	{
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();

		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", APP_ID);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		Debug.LogError ("UserID = " + UserId + "   AppId = " + APP_ID);

		checkSubscriptionResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/Subscription/CheckSubscription",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("check subscription post completed");
				//				pState = PopUpState.NoPopUp;
				textStatus.text = "";
			});
		StartCoroutine(AmaliatCheckSubscription(() => { Debug.Log("json parse complete"); }));

		//		pState = PopUpState.NoPopUp;
	}

	public class CheckSubscriptionResponseModel
	{
		public string Message;
		public string Result;
	}

	public IEnumerator AmaliatCheckSubscription(System.Action OnComplete)
	{

		Popup_VasCircleLoading_Start ("ﯽﺳﺭﺮﺑ ﻝﺎﺣ ﺭﺩ");
		//		StartCoroutine (VAS_Controller_DelayActive ());

		while (!checkSubscriptionResponse.isDone)
			yield return null;

		// Close loading circle after while
		Popup_VasCircleLoading_End ();

		if (checkSubscriptionResponse.text != "") {

			Debug.Log (checkSubscriptionResponse.text);

			var x = JsonUtility.FromJson<ValidationResponseModel> (checkSubscriptionResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);

			if (OnComplete != null) {
				OnComplete ();
			}
			if (x.Result == "successful") {
				//			textStatus.text = "vared bazi mishavad inja user";
				textStatus.text = "";

				// Now can continue the game
				ContinueToGame_FromMenu ();

				// TODO: For future use
				//				public static string UserNotSubscribed = "user not subscribed";
				//				public static string UserSubscriptionExpired = "user subscription expired";
				//				public static string UserLastUnchargedSubscription = "user last uncharged subscribe";
				//				public static string UserUnchargedSubscrption = "user uncharged subscribe";
				//				public static string UserSubscribed = "user is subscribed";

				//				switch (x.Message) {
				//				case "user is subscribed":
				//					break;
				//				case "user last uncharged subscribe":
				//					break;
				//				case "user uncharged subscribe":
				//					break;
				//				default:
				//					break;
				//				}

			} else {

				VAS_Controller_Activator (true);

				Debug.LogError ("Result != Successful");

				switch (x.Message) {
				case "user not subscribed":
					textStatus.text = "subscribe nist user";
					Debug.LogError ("user not subscribed");
					PopUp_VAS_OpenThis (4);
					break;
				case "user subscription expired":
					textStatus.text = "subscribed but pool nadaare";
					Debug.LogError ("user subscription expired");
					PopUp_VAS_OpenThis (5);
					break;
					//				case "user last uncharged subscribe":
					//					break;
					//				case "user uncharged subscribe":
					//					break;
				default:
					Debug.LogError ("default");
					textStatus.text = "subscribe nist user";
					PopUp_VAS_OpenThis (4);
					break;
				}


				//			pState = PopUpState.FastSubscribe;
			}
		}

		// No proper response from server
		else {
			Debug.LogError ("Server ride");

			Moshtan_Utilties_Script.ShowToast (".اشکال در سرور");

			// Open Servers fail popup
			PopUp_VAS_OpenThis (10);
		}
	}


	public void ShopIAP_FirstCheck (string packID) {
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("InAppTitle", packID);
		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", APP_ID);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		IAPResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/InAppPurchases/InAppPurchase",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("IAP post completed");
				textStatus.text = "";
			});
		StartCoroutine(AmaliatIAP(() => { Debug.Log("json parse complete"); }));
		textStatus.text = "dar halle ersal ettelat e kharid be server";

		//		pState = PopUpState.NoPopUp;
	}

	public class IAPResponseModel
	{
		public string Message;
		public string Result;
		public string TransactionId;
		public string UniqueToken;
	}

	public IEnumerator AmaliatIAP(System.Action OnComplete)
	{
		while (!IAPResponse.isDone)
			yield return null;

		// Close loading circle after while
		Popup_VasCircleLoading_End ();

		if (IAPResponse.text != "") {

			Debug.Log (IAPResponse.text);

			var x = JsonUtility.FromJson<IAPResponseModel> (IAPResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);

			if (OnComplete != null) {
				OnComplete ();
			}

			if (x.Result == "successful") {
				//textStatus.text = "kharid sabt shod ba id " + x.Message;
				//LastPurchaseTransactionId = x.Message;

				Debug.Log(x.TransactionId);
				Debug.Log(x.UniqueToken);
				Debug.Log(x.Message);
				textStatus.text = "kharid sabt shod ba id " + x.TransactionId;
				LastPurchaseTransactionId = x.TransactionId;

				PopUp_VAS_OpenThis (7);
			} else {
				textStatus.text = "kharid ba moshkel movajeh shod";
				PopUp_VAS_OpenThis (9);

				// TODO: Message from the above passed as the message for popup

				//			pState = PopUpState.NoPopUp;
			}
		}

		// Server ride
		else {
			Debug.LogError ("Server ride");

			Moshtan_Utilties_Script.ShowToast (".اشکال در سرور");

			// Open Servers fail popup
			PopUp_VAS_OpenThis (10);
		}
	}


	public void Pressed_ShopPressed_Accept () {
		ShopIAP_SecondCheck (SelectedShopID);
	}

	public void ShopIAP_SecondCheck (string packID) {
		Debug.LogError ("Pressed_ShopPressed_Accept");

		Moshtan_Utilties_Script.ShowToast ("TOASTY! Successfully pressed accept");

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("InAppTitle", packID);
		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", APP_ID);
		postParameters.Add("TransactionId", LastPurchaseTransactionId);
		postParameters.Add("Pin", EnterShopResponseTextHolder.text);
		postParameters.Add("UniqueToken", IapUniqueToken);

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		ConfirmIAPResponse = this.GetComponent<RestUtil>().POST("http://avp.tameemco.ir/api/InAppPurchases/ConfirmInAppPurchase",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("Confirm IAP post completed");
				//				pState = PopUpState.NoPopUp;
				textStatus.text = "";
			});
		StartCoroutine(AmaliatConfirmIAP(() => { Debug.Log("json parse complete"); }));
		textStatus.text = "dar halle daryast ettelat e code taeed az server";
		//		pState = PopUpState.NoPopUp;
	}

	public class ConfirmIAPResponseModel
	{
		public string Message;
		public string Result;
	}

	public IEnumerator AmaliatConfirmIAP(System.Action OnComplete)
	{
		Popup_VasCircleLoading_Start ("در حال بررسی");

		while (!ConfirmIAPResponse.isDone)
			yield return null;

		// Close loading circle after while
		Popup_VasCircleLoading_End ();

		if (ConfirmIAPResponse.text != "") {
			Debug.Log (ConfirmIAPResponse.text);

			var x = JsonUtility.FromJson<ConfirmIAPResponseModel> (ConfirmIAPResponse.text);

			Debug.Log (x.Message);
			Debug.Log (x.Result);

			if (OnComplete != null) {
				OnComplete ();
			}
			if (x.Result == "successful") {
				textStatus.text = "kharid kamel shod sibil ezafe mishe ";
				ShopIAP_GiveMustache ();
			} else {
				textStatus.text = "code taeed dorost nabood kharid laghv mishe";
				PopUp_VAS_OpenThis (8);
				//			pState = PopUpState.NoPopUp;
			}
		} 
		else {
			Moshtan_Utilties_Script.ShowToast (".اشکال در سرور");
			Debug.LogError ("Server ride");

			// Open Servers fail popup
			PopUp_VAS_OpenThis (10);
		}
	}

	public void ShopIAP_GiveMustache () {
		if (OnVAS_PurchaseSuccessful != null) {
			Moshtan_Utilties_Script.ShowToast("OnVAS Successful");
			Debug.LogError ("OnVAS Successful");
			OnVAS_PurchaseSuccessful(SelectedShopID);
		}
	}

	public void Pressed_ShopPressed_Quit () {
		Debug.LogError ("Pressed_ShopPressed_Quit");
		if (OnVAS_PurchaseUnsuccessful!=null)
		{
			Moshtan_Utilties_Script.ShowToast("OnVAS UnSuccessful");
			Debug.LogError("OnVAS UnSuccessful");
			OnVAS_PurchaseUnsuccessful();
		}

	}

	public void Pressed_ShopCodeFail_TryAgain () {
		Debug.LogError ("Pressed_ShopCodeFail_TryAgain");
		PopUp_VAS_OpenThis (7);
	}

	public void Pressed_NotEnoughMoney_Return () {
		Debug.LogError ("Pressed_NotEnoughMoney_Return");
		if (OnVAS_PurchaseUnsuccessful != null)
		{
			Moshtan_Utilties_Script.ShowToast("OnVAS UnSuccessful");
			Debug.LogError("OnVAS UnSuccessful");
			OnVAS_PurchaseUnsuccessful();
		}
	}

	public void Pressed_ServerFail_Quit () {
		Debug.LogError ("Pressed_ServerFail_Quit");

		Application.Quit ();
	}

	public void ContinueToGame_FromLoader () {
		StartCoroutine (ContinueToGame_FromLoaderRoutine ());

		Destroy (objEventSystemHolder);
	}

	public void ContinueToGame_FromMenu () {
		StartCoroutine (ContinueToGame_FromMenuRoutine ());

		Destroy (objEventSystemHolder);
	}

	public IEnumerator ContinueToGame_FromLoaderRoutine () {
		yield return new WaitForSeconds (1);
		FindObjectOfType <Loader_Script> ().LoaderFists_EnablePlay ();
		VAS_Controller_Activator (false);
	}

	public IEnumerator ContinueToGame_FromMenuRoutine () {
		popZeroInputScriptHolder.PopUp_CircleLoading_Close ();

		yield return new WaitForSeconds (0.5F);

		FindObjectOfType <Menu_Main_Script> ().DailyRewardCheck ();
		VAS_Controller_Activator (false);
	}

	public IEnumerator VAS_Controller_DelayActive () {
		yield return new WaitForSeconds (0.1F);
		VAS_Controller_Activator (true);
	}

	public void VAS_Controller_Activator (bool whichBool) {
		canvasHolder.enabled = whichBool;
		graphicRayCasterHolder.enabled = whichBool;
		//		this.gameObject.SetActive (whichBool);

		if (whichBool) {
			// Move in trans
			transform.localPosition = Vector2.zero;
		} else {
			// Move out trans
			transform.localPosition = new Vector2 (80, 0);
		}
	}

	public void Popup_VasCircleLoading_Start (string whatString) {
		StartCoroutine (VAS_Controller_DelayActive ());

		popZeroInputScriptHolder.PopUp_ZeroInput_TextChange (whatString);
		PopUp_VAS_OpenThis (6);
	}

	public void Popup_VasCircleLoading_End () {
		popZeroInputScriptHolder.PopUp_CircleLoading_Close ();
	}

	//	void Update () {
	//		if (Input.GetKey ("m")) {
	//			popZeroInpitScriptHolder.PopUp_CircleLoading_AnimateStart ();
	//		}
	//	}

}
