﻿using UnityEngine;
using System.Collections;

public class Item_Values : MonoBehaviour {

	public static Item_Values Instance;

	public int ItemValue_Pipe_1;
	public int ItemValue_Pipe_2;
	public int ItemValue_Pipe_3;
	public int ItemValue_Pipe_4;

	public float ItemValue_Elixir_1;
	public float ItemValue_Elixir_2;
	public float ItemValue_Elixir_3;
	public float ItemValue_Elixir_4;

	public int ItemValue_Freeze_1;
	public int ItemValue_Freeze_2;
	public int ItemValue_Freeze_3;
	public int ItemValue_Freeze_4;

	public int ItemValue_Health_1;
	public int ItemValue_Health_2;
	public int ItemValue_Health_3;
	public int ItemValue_Health_4;

	public int ItemValue_Hedgehog_1;
	public int ItemValue_Hedgehog_2;
	public int ItemValue_Hedgehog_3;
	public int ItemValue_Hedgehog_4;

	public int ItemValue_Mustache_1;
	public int ItemValue_Mustache_2;
	public int ItemValue_Mustache_3;
	public int ItemValue_Mustache_4;

	public int ItemValue_BigBomb_1;
	public int ItemValue_BigBomb_2;
	public int ItemValue_BigBomb_3;
	public int ItemValue_BigBomb_4;

	[Header("Item To Mustache Values: ")]
	public int ItemToMustacheValue_Common;
	public int ItemToMustacheValue_Rare;
	public int ItemToMustacheValue_Legendary;

	void Awake () {
		Instance = this;
	}

}
