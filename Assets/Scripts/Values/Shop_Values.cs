﻿using UnityEngine;
using System.Collections;

public class Shop_Values : MonoBehaviour {

	public static Shop_Values Instance;

    public MoshtanMarketsData mosthanMarketDataHolder;

    public int[] ShopReward_Arr;
	public int[] ShopPrice_NoOffer_Arr;
	public int[] ShopPrice_AfterOffer_Arr;
	public bool[] ShopHasOffer_Arr;

    void Awake()
    {
        Instance = this;

		StartCoroutine (SetupShopValues ());
    }

	private IEnumerator SetupShopValues () {

		while (PlayerData_Main.Instance == null) {
			yield return null;
		}

		// Shop values for offline (From Balance Constants)
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {

			ShopPrice_NoOffer_Arr [0] = Balance_Constants_Script.Shop_PriceNoOffer_Normal1;
			ShopPrice_NoOffer_Arr [1] = Balance_Constants_Script.Shop_PriceNoOffer_Normal2;
			ShopPrice_NoOffer_Arr [2] = Balance_Constants_Script.Shop_PriceNoOffer_Normal3;
			ShopPrice_NoOffer_Arr [3] = Balance_Constants_Script.Shop_PriceNoOffer_Normal4;
			ShopPrice_NoOffer_Arr [4] = Balance_Constants_Script.Shop_PriceNoOffer_Normal5;
		} else {

			ShopPrice_NoOffer_Arr [0] = Balance_Constants_Script.Shop_PriceNoOffer_NormalVAS1;
			ShopPrice_NoOffer_Arr [1] = Balance_Constants_Script.Shop_PriceNoOffer_NormalVAS2;
			ShopPrice_NoOffer_Arr [2] = Balance_Constants_Script.Shop_PriceNoOffer_NormalVAS3;
			ShopPrice_NoOffer_Arr [3] = Balance_Constants_Script.Shop_PriceNoOffer_NormalVAS4;
			ShopPrice_NoOffer_Arr [4] = Balance_Constants_Script.Shop_PriceNoOffer_NormalVAS5;
		}

		// Shop values for offline (From Balance Constants)
		if (PlayerData_Main.Instance.whatShopType != PlayerData_Main.OnlineShopType.VAS) {
			ShopReward_Arr [0] = Balance_Constants_Script.Shop_Mustache_Normal1;
			ShopReward_Arr [1] = Balance_Constants_Script.Shop_Mustache_Normal2;
			ShopReward_Arr [2] = Balance_Constants_Script.Shop_Mustache_Normal3;
			ShopReward_Arr [3] = Balance_Constants_Script.Shop_Mustache_Normal4;
			ShopReward_Arr [4] = Balance_Constants_Script.Shop_Mustache_Normal5;
		} else {
			ShopReward_Arr [0] = Balance_Constants_Script.Shop_Mustache_NormalVAS1;
			ShopReward_Arr [1] = Balance_Constants_Script.Shop_Mustache_NormalVAS2;
			ShopReward_Arr [2] = Balance_Constants_Script.Shop_Mustache_NormalVAS3;
			ShopReward_Arr [3] = Balance_Constants_Script.Shop_Mustache_NormalVAS4;
			ShopReward_Arr [4] = Balance_Constants_Script.Shop_Mustache_NormalVAS5;
		}

		// Shop values for offline (From Balance Constants)
		for (int i = 0; i < 5; i++) {
			ShopHasOffer_Arr [i] = false;
		}

		// Shop values for offline (From Balance Constants)
		for (int i = 0; i < 5; i++) {
			ShopPrice_AfterOffer_Arr [i] = ShopPrice_NoOffer_Arr [i];
		}
	}

    private void Start()
    {
        GetMarketValues();
    }
    
    public void GetMarketValues()
    {
        StartCoroutine(mosthanMarketDataHolder.Amaliat(FillTheArrays));
    }

    private void FillTheArrays()
	{
		if (PlayerData_Main.Instance != null) {
			switch (PlayerData_Main.Instance.whatShopType) {
			case PlayerData_Main.OnlineShopType.CafeBazaar:
				for (int i = 0; i < mosthanMarketDataHolder.marketResult.Count; i++) {
					ShopReward_Arr [i] = mosthanMarketDataHolder.marketResult [i].cafebazaar_reward;
					ShopPrice_NoOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].cafebazaar_normal_price;
					ShopPrice_AfterOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].cafebazaar_discount_price;
					ShopHasOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].cafebazaar_has_discount;
				}
				Debug.Log("Array filled! CafeBazaar");
				break;
			case PlayerData_Main.OnlineShopType.Myket:
				for (int i = 0; i < mosthanMarketDataHolder.marketResult.Count; i++) {
					ShopReward_Arr [i] = mosthanMarketDataHolder.marketResult [i].myket_reward;
					ShopPrice_NoOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].myket_normal_price;
					ShopPrice_AfterOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].myket_discount_price;
					ShopHasOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].myket_has_discount;
				}
				Debug.Log("Array filled! Myket");
				break;
			case PlayerData_Main.OnlineShopType.VAS:

//				for (int i = 0; i < mosthanMarketDataHolder.marketResult.Count; i++) {
//					ShopReward_Arr [i] = mosthanMarketDataHolder.marketResult [i].vas_reward;
//					ShopPrice_NoOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].vas_normal_price;
//					ShopPrice_AfterOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].vas_discount_price;
//					ShopHasOffer_Arr [i] = mosthanMarketDataHolder.marketResult [i].vashas_discount;
//				}

				// From VAS database but right now, it is offline
				Debug.Log("Array filled! VAS");
				break;
			default:
				break;
			}
		}
	}
}

