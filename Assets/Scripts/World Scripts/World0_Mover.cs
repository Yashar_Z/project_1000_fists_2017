﻿using UnityEngine;
using System.Collections;

public class World0_Mover : MonoBehaviour {

	public HUD_GeneralAnim_Script hudGeneralAnimsScriptHolder;

	public float World0FansLength;
	public float World0DoorLength;
	public float w0PausedAnimedTime;

	public Animation w0AnimHold_Fans;
	public Animation w0AnimHold_Door;

	public GameObject objWorldTop_Left;
	public GameObject objWorldTop_Right;

	public ParticleSystem[] w0FanParticlesArr;

	void OnEnable () {
		EventManager.StartListening ("Pause", PauseWorld);
		EventManager.StartListening ("Freeze", FreezeWorld);
	}

	void OnDisable () {
		EventManager.StopListening ("Pause", PauseWorld);
		EventManager.StopListening ("Freeze", PauseWorld);
	}

	void Start () {
		w0AnimHold_Fans [w0AnimHold_Fans.clip.name].speed = 1F / World0FansLength;
		w0AnimHold_Door [w0AnimHold_Door.clip.name].speed = 1F / World0DoorLength;

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			W0_IntroAnims_Delayer ();
		}
	}

	public void W0_IntroAnims_Delayer () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("W0_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + PlayerData_Main.Instance.introAdditionTime);
			} else {
				Invoke ("W0_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + 1);
			}
		} else {
			Invoke ("W0_IntroAnims_Play", 1F);
		}
	}

	void W0_IntroAnims_Play () {
		// Remove tops with intro
		WorldTops_Disable ();

		w0AnimHold_Fans.Play ();
		w0AnimHold_Door.Play ("World 1 - Garage Door Start Anim 1 (Legacy)");

		// SOUNDD EFFECTS - Garage Door Opening Sound (done)
		if (SoundManager.Instance!= null)
		{
			SoundManager.Instance.KerkerehSound.PlayDelayed(0.7f);
		}
	}

	public void W0_OutroAnims_Play () {
		w0AnimHold_Fans.Stop ();
		w0AnimHold_Door.Play ("World 1 - Garage Door Close Anim 1 (Legacy)");
	}

	void WorldTops_Disable () {
		objWorldTop_Left.SetActive (false);
		objWorldTop_Right.SetActive (false);
	}

	void PauseWorld () {
		if (!EventTrigger.isPaused) {
			if (EventTrigger.isFrozen) {
			} else {
				w0PausedAnimedTime = w0AnimHold_Fans [w0AnimHold_Fans.clip.name].time;
			}
			foreach (ParticleSystem eachParticle in w0FanParticlesArr) {

				// Pause Fans Anim & Particles
				w0AnimHold_Fans.Stop();
				eachParticle.Pause ();
			}
		}

		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			foreach (ParticleSystem eachParticle in w0FanParticlesArr) {

				// Resume Fans Anim & Particles
				w0AnimHold_Fans [w0AnimHold_Fans.clip.name].time = w0PausedAnimedTime;
				w0AnimHold_Fans.Play();
				eachParticle.Play ();
			}
		}
	}

	void FreezeWorld () {
		if (!EventTrigger.isFrozen) {
			if (EventTrigger.isPaused) {
			} else {
				w0PausedAnimedTime = w0AnimHold_Fans [w0AnimHold_Fans.clip.name].time;
			}

			foreach (ParticleSystem eachParticle in w0FanParticlesArr) {

				// Pause Fans Anim & Particles
				w0AnimHold_Fans.Stop();
				eachParticle.Pause ();
			}
		}

		if (EventTrigger.isFrozen) {
			foreach (ParticleSystem eachParticle in w0FanParticlesArr) {

				// Resume Fans Anim & Particles
				w0AnimHold_Fans [w0AnimHold_Fans.clip.name].time = w0PausedAnimedTime;
				w0AnimHold_Fans.Play();
				eachParticle.Play ();
			}
		}
	}
}
