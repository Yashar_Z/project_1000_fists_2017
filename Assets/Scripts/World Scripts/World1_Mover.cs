﻿using UnityEngine;
using System.Collections;

public class World1_Mover : MonoBehaviour {

	public float World2DoorsLength;
	//	public float w1PausedAnimedTime;
	public Animation w1AnimHold_Doors;
	public Animation w1AnimHold_Booth;

	public GameObject objWorldTop_Left;
	public GameObject objWorldTop_Right;

	public ParticleSystem w1SmokeParticleHolder;

	void OnEnable () {
		EventManager.StartListening ("Pause", PauseWorld);
		EventManager.StartListening ("Freeze", FreezeWorld);
	}

	void OnDisable () {
		EventManager.StopListening ("Pause", PauseWorld);
		EventManager.StopListening ("Freeze", PauseWorld);
	}

	void Start () {
		//		w1AnimHold_Fans [w1AnimHold_Fans.clip.name].speed = 1F / World1FansLength;
		w1AnimHold_Doors [w1AnimHold_Doors.clip.name].speed = 1F / World2DoorsLength;

		W1_Booth_Delayer ();

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			W1_IntroAnims_Delayer ();
		}
	}

	public void W1_Booth_Now () {
		w1AnimHold_Booth.Play ();
		W1_Booth_Delayer ();
	}

	public void W1_Booth_Delayer () {
		Invoke ("W1_Booth_Now", Random.Range (13, 25));
	}

	public void W1_IntroAnims_Delayer () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("W1_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + PlayerData_Main.Instance.introAdditionTime);
			} else {
				Invoke ("W1_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + 1);
			}
		} else {
			Invoke ("W1_IntroAnims_Play", 1F);
		}
	}

	void W1_IntroAnims_Play () {
		// Remove tops with intro
		WorldTops_Disable ();

		// Start Particles With Intro (Old place)
//		W1_ParticlesPlay ();

		w1AnimHold_Doors.Play ("World 1 - Cinema Doors Start Anim 1 (Legacy)");

        // SOUNDD EFFECTS - Cinema Door Opening Sound (done)
        if (SoundManager.Instance!=null)
        {
            SoundManager.Instance.CinemaDoor.Play();
        }
	}

	public void W1_OutroAnims_Play () {
		w1SmokeParticleHolder.Stop ();

		if (Random.Range (0, 2) == 1) {
			w1AnimHold_Doors.Play ("World 1 - Cinema Doors Close Anim 1 (Legacy)");
		} else {
			w1AnimHold_Doors.Play ("World 1 - Cinema Doors Close Anim 2 (Legacy)");
		}
	}

	public void W1_ParticlesPlay () {
		w1SmokeParticleHolder.Play ();
	}

	void WorldTops_Disable () {
		objWorldTop_Left.SetActive (false);
		objWorldTop_Right.SetActive (false);
	}

	void PauseWorld () {
		if (!EventTrigger.isPaused) {
			// Pause Smoke Particle
			w1SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Resume Smoke Particle
			w1SmokeParticleHolder.Play ();
		}
	}

	void FreezeWorld () {
		if (!EventTrigger.isFrozen) {
			// Freeze Smoke Particle
			w1SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isFrozen) {
			// Unfreeze Smoke Particle
			w1SmokeParticleHolder.Play ();
		}
	}
}
