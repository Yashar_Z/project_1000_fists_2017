﻿using UnityEngine;
using System.Collections;

public class World2_Mover : MonoBehaviour {

	public float World2DoorLength;
//	public float w2PausedAnimedTime;

	public Animation w2AnimHold_Door;
	public Animation w2AnimHold_InsideCar;

	public GameObject objWorldTop_Left;
	public GameObject objWorldTop_Right;

	public ParticleSystem w2SmokeParticleHolder;

//	void OnEnable () {
//		EventManager.StartListening ("Pause", PauseWorld);
//		EventManager.StartListening ("Freeze", FreezeWorld);
//	}
//
//	void OnDisable () {
//		EventManager.StopListening ("Pause", PauseWorld);
//		EventManager.StopListening ("Freeze", PauseWorld);
//	}

	void OnEnable () {
		EventManager.StartListening ("Pause", PauseWorld);
		EventManager.StartListening ("Freeze", FreezeWorld);
	}

	void OnDisable () {
		EventManager.StopListening ("Pause", PauseWorld);
		EventManager.StopListening ("Freeze", PauseWorld);
	}

	void Start () {
//		w2AnimHold_Fans [w2AnimHold_Fans.clip.name].speed = 1F / World2FansLength;
		w2AnimHold_Door [w2AnimHold_Door.clip.name].speed = 1F / World2DoorLength;

		W2_CarAnimate_Delayer ();

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			W2_IntroAnims_Delayer ();
		}
	}

	public void W2_CarAnimate_Now () {
		w2AnimHold_InsideCar.Play ();
		W2_CarAnimate_Delayer ();
	}

	public void W2_CarAnimate_Delayer () {
		Invoke ("W2_CarAnimate_Now", Random.Range (7, 20));
	}

	public void W2_IntroAnims_Delayer () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("W2_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + PlayerData_Main.Instance.introAdditionTime);
			} else {
				Invoke ("W2_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + 1);
			}
		} else {
			Invoke ("W2_IntroAnims_Play", 1F);
		}
	}

	void W2_IntroAnims_Play () {
		// Remove tops with intro
		WorldTops_Disable ();

		// Start Particles With Intro (Old place)
//		W2_ParticlesPlay ();

		w2AnimHold_Door.Play ("World 2 - Garage Door Start Anim 1 (Legacy)");

        // SOUNDD EFFECTS - Garage Door Opening Sound (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.KerkerehSound.PlayDelayed(0.7f);
        }
	}

	public void W2_OutroAnims_Play () {
		w2SmokeParticleHolder.Stop ();

		if (Random.Range (0, 2) == 1) {
			w2AnimHold_Door.Play ("World 2 - Garage Door Close Anim 1 (Legacy)");
		} else {
			w2AnimHold_Door.Play ("World 2 - Garage Door Close Anim 2 (Legacy)");
		}
	}

	public void W2_ParticlesPlay () {
		w2SmokeParticleHolder.Play ();
	}

	void WorldTops_Disable () {
		objWorldTop_Left.SetActive (false);
		objWorldTop_Right.SetActive (false);
	}

	void PauseWorld () {
		if (!EventTrigger.isPaused) {
			// Pause Smoke Particle
			w2SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Resume Smoke Particle
			w2SmokeParticleHolder.Play ();
		}
	}

	void FreezeWorld () {
		if (!EventTrigger.isFrozen) {
			// Freeze Smoke Particle
			w2SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isFrozen) {
			// Unfreeze Smoke Particle
			w2SmokeParticleHolder.Play ();
		}
	}
}
