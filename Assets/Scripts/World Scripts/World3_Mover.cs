﻿using UnityEngine;
using System.Collections;

public class World3_Mover : MonoBehaviour {

	public float World3DoorsLength;
	//	public float w1PausedAnimedTime;

	public Animation w3AnimHold_Doors;
	public Animation w3AnimHold_Street;
	public Animation w3AnimHold_ParentAll;

	public GameObject objWorldTop_Left;
	public GameObject objWorldTop_Right;

	public ParticleSystem particleFullscreenExplodeHolder;
	public ParticleSystem w3SmokeParticleHolder;

	void OnEnable () {
		EventManager.StartListening ("Pause", PauseWorld);
		EventManager.StartListening ("Freeze", FreezeWorld);
	}

	void OnDisable () {
		EventManager.StopListening ("Pause", PauseWorld);
		EventManager.StopListening ("Freeze", PauseWorld);
	}

	void Start () {
		//		w1AnimHold_Fans [w1AnimHold_Fans.clip.name].speed = 1F / World1FansLength;
		w3AnimHold_Doors [w3AnimHold_Doors.clip.name].speed = 1F / World3DoorsLength;

//		W3_Street_Delayer ();

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			W3_IntroAnims_Delayer ();
		}
	}

	public void W3_Shake_Now () {
		w3AnimHold_ParentAll.Play ();
		particleFullscreenExplodeHolder.Play ();
	}

	public void W3_Street_Now () {
		w3AnimHold_Street.Play ();
		W3_Street_Delayer ();
	}

	public void W3_Street_Delayer () {
		Invoke ("W3_Street_Now", Random.Range (13, 25));
	}

	public void W3_IntroAnims_Delayer () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("W3_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + PlayerData_Main.Instance.introAdditionTime);
			} else {
				Invoke ("W3_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + 1);
			}
		} else {
			Invoke ("W3_IntroAnims_Play", 1F);
		}
	}

	void W3_IntroAnims_Play () {
		// Remove tops with intro
		WorldTops_Disable ();

		// Start Particles With Intro (Old place)
//		W3_ParticlesPlay ();

		w3AnimHold_Doors.Play ("World 3 - Hotel Doors Start Anim 1 (Legacy)");

        // SOUNDD EFFECTS - Garage Door Opening Sound (done)
        if (SoundManager.Instance!= null)
        {
            SoundManager.Instance.GrandHotelDoor.PlayDelayed(2f);
        }

	}

	public void W3_OutroAnims_Play () {
		w3SmokeParticleHolder.Stop ();
		w3AnimHold_Doors.Play ("World 3 - Hotel Doors Close Anim 1 (Legacy)");

//		if (Random.Range (0, 2) == 1) {
//			w3AnimHold_Doors.Play ("World 3 - Hotel Doors Close Anim 1 (Legacy)");
//		} else {
//			w3AnimHold_Doors.Play ("World 3 - Hotel Doors Close Anim 2 (Legacy)");
//		}
	}

	public void W3_ParticlesPlay () {
		w3SmokeParticleHolder.Play ();
	}

	void WorldTops_Disable () {
		objWorldTop_Left.SetActive (false);
		objWorldTop_Right.SetActive (false);
	}

	void PauseWorld () {
		if (!EventTrigger.isPaused) {
			// Pause Smoke Particle
			w3SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Resume Smoke Particle
			w3SmokeParticleHolder.Play ();
		}
	}

	void FreezeWorld () {
		if (!EventTrigger.isFrozen) {
			// Freeze Smoke Particle
			w3SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isFrozen) {
			// Unfreeze Smoke Particle
			w3SmokeParticleHolder.Play ();
		}
	}
}
