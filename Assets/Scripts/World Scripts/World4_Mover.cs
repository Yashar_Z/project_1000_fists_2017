﻿using UnityEngine;
using System.Collections;

public class World4_Mover : MonoBehaviour {

	public float World2DoorsLength;
//	public float w1PausedAnimedTime;
	public Animation w4AnimHold_Doors;

	public GameObject objWorldTop_Left;
	public GameObject objWorldTop_Right;

	public GameObject objNeons_Title;
	public GameObject objNeons_LightbulbsTop;
	public GameObject objNeons_LightbulbsBottom;

	public ParticleSystem w4SmokeParticleHolder;

	private	int intLastFlicker_Curr = 0;
	private	int intLastFlicker_Buffer;

	void OnEnable () {
		EventManager.StartListening ("Pause", PauseWorld);
		EventManager.StartListening ("Freeze", FreezeWorld);
	}

	void OnDisable () {
		EventManager.StopListening ("Pause", PauseWorld);
		EventManager.StopListening ("Freeze", PauseWorld);
	}

	void Start () {
		//		w1AnimHold_Fans [w1AnimHold_Fans.clip.name].speed = 1F / World1FansLength;
		w4AnimHold_Doors [w4AnimHold_Doors.clip.name].speed = 1F / World2DoorsLength;

//		StartCoroutine (W4_Flicker_Delayer ());

		if (PlayerData_Main.Instance != null) {
			// Call from loading!!!
		} else {
			W4_IntroAnims_Delayer ();
		}
	}

	public void W4_Flicker_Now () {
		// To prevent playing to similar flicker anims after each other as long as possible
		intLastFlicker_Buffer = Random.Range (0, 3);

//		Debug.LogError ("BEFORE intLastFlicker_Buffer = " + intLastFlicker_Buffer + "     intLastFlicker_Curr = " + intLastFlicker_Curr);
		if (intLastFlicker_Curr == intLastFlicker_Buffer) {
			intLastFlicker_Buffer = Random.Range (0, 3);
			if (intLastFlicker_Curr == intLastFlicker_Buffer) {
				intLastFlicker_Buffer = Random.Range (0, 3);
			}
		} 

//		Debug.LogError ("AFTER intLastFlicker_Buffer = " + intLastFlicker_Buffer + "     intLastFlicker_Curr = " + intLastFlicker_Curr);
		intLastFlicker_Curr = intLastFlicker_Buffer;

		W4_Flicker_SelectAnim ();
	}

	public void W4_Flicker_SelectAnim () {
		switch (intLastFlicker_Curr) {
		case 0:
//			Debug.LogError ("Flicker Anim 1");
			w4AnimHold_Doors.Play ("World 4 - Cinema Lights Flicker Anim 1 (Legacy)");
			break;
		case 1:
//			Debug.LogError ("Flicker Anim 2");
			w4AnimHold_Doors.Play ("World 4 - Cinema Lights Flicker Anim 2 (Legacy)");
			break;
		case 2:
//			Debug.LogError ("Flicker Anim 3");
			w4AnimHold_Doors.Play ("World 4 - Cinema Lights Flicker Anim 3 (Legacy)");
			break;
		default:
			break;
		}
	}

	public IEnumerator W4_Flicker_FirstTimeOnly () {
		yield return new WaitForSeconds (Random.Range (4, 7));
		W4_Flicker_Now ();
	}

	public IEnumerator W4_Flicker_Delayer () {
		yield return new WaitForSeconds (Random.Range (13, 20));
		W4_Flicker_Now ();
	}

	public void W4_CheckLvl1_LongIntro () {
		if (PlayerData_Main.Instance.introAdditionTime > 1) {
			StartCoroutine (W4_Neons_Title(3));
			StartCoroutine (W4_Neons_LightbulbsTop (0, true));
			StartCoroutine (W4_Neons_LightbulbsTop (5, false));
			StartCoroutine (W4_Neons_LightbulbsBottom (0, true));
		} else {
			StartCoroutine (W4_Neons_Title(1));
			StartCoroutine (W4_Neons_LightbulbsTop (1, true));
			StartCoroutine (W4_Neons_LightbulbsBottom (0, true));
		}
	}

	public IEnumerator W4_Neons_Title (float delay) {
		yield return new WaitForSeconds (delay);
		objNeons_Title.SetActive (true);
	}

	public IEnumerator W4_Neons_LightbulbsTop (float delay, bool isActive) {
		yield return new WaitForSeconds (delay);
		objNeons_LightbulbsTop.SetActive (isActive);
	}

	public IEnumerator W4_Neons_LightbulbsBottom (float delay, bool isActive) {
		yield return new WaitForSeconds (delay);
		objNeons_LightbulbsBottom.SetActive (isActive);
	}

	public void W4_IntroAnims_Delayer () {
		if (PlayerData_InGame.Instance.fullIntro_Allowed) {
			if (PlayerData_Main.Instance != null) {
				Invoke ("W4_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + PlayerData_Main.Instance.introAdditionTime);

				// Check for long intro and lights
				W4_CheckLvl1_LongIntro ();
			} else {
				Invoke ("W4_IntroAnims_Play", PlayerData_InGame.Instance.introAnimLength + 1);

				// Above is checklevel
				StartCoroutine (W4_Neons_Title(1));
				StartCoroutine (W4_Neons_LightbulbsBottom (0, true));
			}
		} else {
			Invoke ("W4_IntroAnims_Play", 1F);
		}
	}

	// Check for campaign (Play open doors / lights) or endless (Only play after pressing play)
	void W4_IntroAnims_Play () {
		if (PlayerData_InGame.Instance.gameSpawnerType == PlayerData_InGame.GameSpawnType.Campaign) {
			W4_IntroAnims_PlayNOW ();
		} 

		// Is Endless
		//		else {
		//		}
	}

	public void W4_IntroAnims_PlayNOW () {
		//		StartCoroutine (W4_IntroAnims_PlayRoutine ());

		// Remove tops with intro
		WorldTops_Disable ();

		// Start Particles With Intro (Old place)
		//		W1_ParticlesPlay ();

		w4AnimHold_Doors.Play ("World 4 - Cinema Lights Start Anim 1 (Legacy)");

		// SOUNDD EFFECTS - Cinema Door Opening Sound (done)
		if (SoundManager.Instance!=null)
		{
			// TODO: Require new sound
			//			SoundManager.Instance.CinemaDoor.Play();
		}
	}

//	IEnumerator W4_IntroAnims_PlayRoutine () {
//		yield return new WaitForSeconds (0.75F);
//
//
//	}

	public void W4_OutroAnims_Play () {
		w4SmokeParticleHolder.Stop ();

		if (Random.Range (0, 2) == 1) {
			w4AnimHold_Doors.Play ("World 4 - Cinema Lights End Anim 1 (Legacy)");
		} else {
			w4AnimHold_Doors.Play ("World 4 - Cinema Lights End Anim 2 (Legacy)");
		}

		// Kill neons in either case of world end
		objNeons_Title.SetActive (false);
//		objNeons_LightbulbsTop.SetActive = false;
		objNeons_LightbulbsBottom.SetActive (false);

	}

	public void W4_ParticlesPlay () {
		w4SmokeParticleHolder.Play ();
	}

	void WorldTops_Disable () {
		objWorldTop_Left.SetActive (false);
		objWorldTop_Right.SetActive (false);
	}

	void PauseWorld () {
		if (!EventTrigger.isPaused) {
			// Pause Smoke Particle
			w4SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isPaused && !EventTrigger.isFrozen) {
			// Resume Smoke Particle
			w4SmokeParticleHolder.Play ();
		}
	}

	void FreezeWorld () {
		if (!EventTrigger.isFrozen) {
			// Freeze Smoke Particle
			w4SmokeParticleHolder.Pause ();
		}

		if (EventTrigger.isFrozen) {
			// Unfreeze Smoke Particle
			w4SmokeParticleHolder.Play ();
		}
	}
}
