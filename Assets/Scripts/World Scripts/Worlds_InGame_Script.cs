﻿using UnityEngine;
using System.Collections;

public class Worlds_InGame_Script : MonoBehaviour {

	public GameObject[] worldsParentObjArr;

	public World1_Mover world1_MoverHolder;
	public World2_Mover world2_MoverHolder;
	public World3_Mover world3_MoverHolder;
	public World4_Mover world4_MoverHolder;
	public World5_Mover world5_MoverHolder;

	public World0_Mover world0_MoverHolder;

	[HideInInspector]
	public int intWorldActive_Endless;

	private int selectedWorldNumber;

	public void World_Activate (int whichWorld) {
		selectedWorldNumber = whichWorld;
		worldsParentObjArr [0].SetActive (false);
		worldsParentObjArr [1].SetActive (false);
		worldsParentObjArr [2].SetActive (false);
		worldsParentObjArr [3].SetActive (false);
		worldsParentObjArr [4].SetActive (false);
		worldsParentObjArr [selectedWorldNumber].SetActive (true);
	}

	public void World_ChangeForEndless () {
		if (intWorldActive_Endless == 1) {
			worldsParentObjArr [0].SetActive (false);
			worldsParentObjArr [3].SetActive (true);
			intWorldActive_Endless = 3;
		} else {
			worldsParentObjArr [3].SetActive (false);
			worldsParentObjArr [0].SetActive (true);
			intWorldActive_Endless = 1;
		}
	}

	public IEnumerator World_EndIt () {
		yield return new WaitForSeconds (1);

		switch (selectedWorldNumber + 1) {
		case 1:
			world1_MoverHolder.W1_OutroAnims_Play ();
//			Debug.LogError ("YO!? END World 1!");
			break;
		case 2:
			world2_MoverHolder.W2_OutroAnims_Play ();
//			Debug.LogError ("YO!? END World 2!");
			break;
		case 3:
			world3_MoverHolder.W3_OutroAnims_Play ();
//			Debug.LogError ("YO!? END World 3!");
			break;
		case 4:
			world4_MoverHolder.W4_OutroAnims_Play ();
//			Debug.LogError ("YO!? END World 4!");
			break;
		case 5:
			world5_MoverHolder.W5_OutroAnims_Play ();
			break;
		default:
			world0_MoverHolder.W0_OutroAnims_Play ();
			break;
		}
	}
}
