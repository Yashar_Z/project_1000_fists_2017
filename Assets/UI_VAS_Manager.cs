﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class UI_VAS_Manager : MonoBehaviour
{

	public enum PopUpState
	{
		NoPopUp, EnterPhone, EnterValidation
		, ResendValidation, SubscriptionMessage, FastSubscribe
		, Recharge
	}
	private PopUpState pState;

	public Transform EnterPhonePanel;
	public Transform EnterValidationPanel;
	public Transform ResendValidationPanel;
	public Transform SubscriptionMessagePanel;
	public Transform FastSubscribePanel;
	public Transform RechargePanel;
	public Text statusText;

	private WWW registerResponse;
	private WWW validationResponse;
	private WWW resendValidationResponse;
	private WWW checkSubscriptionResponse;
	private WWW IAPResponse;
	private WWW ConfirmIAPResponse;

	public string PhoneNumber;
	public string UserId;
	public string AccessToken;
	public string LastPurchaseTransactionId;

	public Text EnterPhoneTextHolder;
	public Text EnterValidationTextHolder;
	void Start()
	{
		//PlayerPrefs.SetString("PhoneNumber", "");
		//PlayerPrefs.SetString("UserId", "");
		//PlayerPrefs.SetString("AccessToken", "");

		PhoneNumber = PlayerPrefs.GetString("PhoneNumber", "");
		UserId = PlayerPrefs.GetString("UserId", "");
		AccessToken = PlayerPrefs.GetString("AccessToken", "");
		Debug.Log("from playerprefs" + PhoneNumber + "--" + UserId + "--" + AccessToken);


		pState = PopUpState.NoPopUp;
		// TODO: local save ro check kon agar phone number ya userid dar on nabood enterphone ro neshon bede
		if ((PhoneNumber == "") || (UserId == "") || (AccessToken == ""))
		{
			pState = PopUpState.EnterPhone;
			statusText.text = "";
		}
		else
		{
			pState = PopUpState.NoPopUp;
			statusText.text = "check subscription";
			CheckSubscription();
		}

	}

//	void Update()
//	{
//		switch (pState)
//		{
//		case PopUpState.NoPopUp:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.EnterPhone:
//			EnterPhonePanel.transform.localPosition = Vector3.zero;
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.EnterValidation:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.localPosition = Vector3.zero;
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.ResendValidation:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.localPosition = Vector3.zero;
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.SubscriptionMessage:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.localPosition = Vector3.zero;
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.FastSubscribe:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.localPosition = Vector3.zero;
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		case PopUpState.Recharge:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.localPosition = Vector3.zero;
//			break;
//		default:
//			EnterPhonePanel.transform.position = new Vector3(-1000, -1000);
//			EnterValidationPanel.transform.position = new Vector3(-1000, -1000);
//			ResendValidationPanel.transform.position = new Vector3(-1000, -1000);
//			SubscriptionMessagePanel.transform.position = new Vector3(-1000, -1000);
//			FastSubscribePanel.transform.position = new Vector3(-1000, -1000);
//			RechargePanel.transform.position = new Vector3(-1000, -1000);
//			break;
//		}
//	}

	#region Register

	public void Pressed_EnterPhoneButton()
	{
		var pn = EnterPhoneTextHolder.text;
		Debug.Log(pn);
		string android_id = "12345";

		#if !UNITY_EDITOR && UNITY_ANDROID
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
		AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
		android_id = secure.CallStatic<string>("getString", contentResolver, "android_id");
		#endif
		Debug.Log(android_id);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("DeviceId", android_id);
		postParameters.Add("Platform", "Android");
		postParameters.Add("PlatformVersion", "1");
		postParameters.Add("SimType", "HA");
		postParameters.Add("PhoneNumber", pn);
		postParameters.Add("AppId", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		registerResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/Login/Register",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("enter Phone post completed");
				PhoneNumber = pn;
				PlayerPrefs.SetString("PhoneNumber", PhoneNumber);
				pState = PopUpState.EnterValidation;
				statusText.text = "";

			});
		StartCoroutine(AmaliatRegister(() =>
			{
				Debug.Log("json parse complete");
			}
		));

		pState = PopUpState.NoPopUp;
		statusText.text = "dar hale ferestandan etelat be server baraye ersal code taeedie";
	}

	private class RegisterResponseModel
	{
		public string Message;
		public string Result;
	}
	public IEnumerator AmaliatRegister(System.Action OnComplete)
	{
		while (!registerResponse.isDone)
			yield return null;

		Debug.Log(registerResponse.text);

		var x = JsonUtility.FromJson<RegisterResponseModel>(registerResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Message == "successful")
		{
			Debug.Log("register completed");
		}
		else
		{
			Debug.Log(x.Result);
			pState = PopUpState.EnterPhone;
		}
	}
	#endregion
	#region Validation
	public void Pressed_EnterValidationCodeButton()
	{
		var vc = EnterValidationTextHolder.text;
		Debug.Log(vc);

		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("ValidationCode", vc);
		postParameters.Add("PhoneNumber", PhoneNumber);
		postParameters.Add("AppId", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		validationResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/Login/Validate",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("validation post completed");
				pState = PopUpState.NoPopUp;
				statusText.text = "";
			});
		StartCoroutine(AmaliatValidation(() => { Debug.Log("json parse complete"); }));
		statusText.text = "dar halle barresi code validation";
		pState = PopUpState.NoPopUp;
	}
	public class ValidationResponseModel
	{
		public string Message;
		public string Result;
		public string AccessToken;
		public string UserId;
		public int ValidDays;
	}
	public IEnumerator AmaliatValidation(System.Action OnComplete)
	{
		while (!validationResponse.isDone)
			yield return null;

		Debug.Log(validationResponse.text);

		var x = JsonUtility.FromJson<ValidationResponseModel>(validationResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);
		Debug.Log(x.AccessToken);
		Debug.Log(x.UserId);
		Debug.Log(x.ValidDays);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Message == "successful")
		{
			statusText.text = "ba tashakor sms welocome alan vasat miad";
			PlayerPrefs.SetString("UserId", x.UserId);
			PlayerPrefs.SetString("AccessToken", x.AccessToken);
		}
		else
		{
			statusText.text = "code validation eshtebah bood";
			pState = PopUpState.ResendValidation;
		}
	}
	#endregion
	#region ResendValidation
	public void Pressed_ResendValidationCodeButton()
	{
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("PhoneNumber", PhoneNumber);
		postParameters.Add("AppId", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();

		resendValidationResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/Login/ResendValidationCode",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("resend validation post completed");
				statusText.text = "";
			});
		StartCoroutine(AmaliatResendValidation(() => { Debug.Log("json parse complete"); }));

		statusText.text = "dar halle ersal mojadad validation code";
		pState = PopUpState.EnterValidation;
	}
	public class ResendValidationResponseModel
	{
		public string Message;
		public string Result;
	}
	public IEnumerator AmaliatResendValidation(System.Action OnComplete)
	{
		while (!resendValidationResponse.isDone)
			yield return null;

		Debug.Log(resendValidationResponse.text);

		var x = JsonUtility.FromJson<ResendValidationResponseModel>(resendValidationResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Message == "successful")
		{
			Debug.Log(x.Message);
		}
		else
		{
			Debug.Log(x.Result);
		}
	}
	#endregion
	#region CheckSubscription
	public void CheckSubscription()
	{
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();

		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		checkSubscriptionResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/Subscription/CheckSubscription",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("check subscription post completed");
				pState = PopUpState.NoPopUp;
				statusText.text = "";
			});
		StartCoroutine(AmaliatCheckSubscription(() => { Debug.Log("json parse complete"); }));

		pState = PopUpState.NoPopUp;
	}
	public class CheckSubscriptionResponseModel
	{
		public string Message;
		public string Result;
	}

	public IEnumerator AmaliatCheckSubscription(System.Action OnComplete)
	{
		while (!checkSubscriptionResponse.isDone)
			yield return null;

		Debug.Log(checkSubscriptionResponse.text);

		var x = JsonUtility.FromJson<ValidationResponseModel>(checkSubscriptionResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Result == "successful")
		{
			statusText.text = "vared bazi mishavad inja user";
		}
		else
		{
			statusText.text = "subscribe nist user";
			pState = PopUpState.FastSubscribe;
		}
	}
	#endregion

	public void Pressed_FastSubscribe()
	{
		Application.OpenURL("sms:307172?body=1");
	}

	#region InAppPurchase
	public void Pressed_InAppPurchasePackButton(string packID)
	{
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("InAppPurchaseId", packID.ToString());
		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		IAPResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/InAppPurchases/InAppPurchase",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("IAP post completed");
				pState = PopUpState.NoPopUp;
				statusText.text = "";
			});
		StartCoroutine(AmaliatIAP(() => { Debug.Log("json parse complete"); }));
		statusText.text = "dar halle ersal ettelat e kharid be server";
		pState = PopUpState.NoPopUp;
	}
	public class IAPResponseModel
	{
		public string Message;
		public string Result;
	}
	public IEnumerator AmaliatIAP(System.Action OnComplete)
	{
		while (!IAPResponse.isDone)
			yield return null;

		Debug.Log(IAPResponse.text);

		var x = JsonUtility.FromJson<IAPResponseModel>(IAPResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Message == "successful")
		{
			statusText.text = "kharid sabt shod ba id " + x.Result;
			LastPurchaseTransactionId = x.Result;
		}
		else
		{
			statusText.text = "kharid ba moshkel movajeh shod";
			pState = PopUpState.NoPopUp;
		}
	}
	#endregion

	#region ConfirmInAppPurchase
	public void Pressed_ConfirmInAppPurchaseButton(int packNumber)
	{
		var postParameters = new System.Collections.Generic.Dictionary<string, string>();
		postParameters.Add("InAppPurchaseId", packNumber.ToString());
		postParameters.Add("UserId", UserId);
		postParameters.Add("AppId", "1");
		postParameters.Add("TransactionId", LastPurchaseTransactionId);
		postParameters.Add("Pin", "1");

		var postHeaders = new System.Collections.Generic.Dictionary<string, string>();
		postHeaders.Add("AccessToken", AccessToken);

		ConfirmIAPResponse = this.GetComponent<RestUtil>().POST("http://avp.appete.mobi/api/InAppPurchases/ConfirmInAppPurchase",
			postHeaders,
			postParameters,
			() =>
			{
				Debug.Log("Confirm IAP post completed");
				pState = PopUpState.NoPopUp;
				statusText.text = "";
			});
		StartCoroutine(AmaliatConfirmIAP(() => { Debug.Log("json parse complete"); }));
		statusText.text = "dar halle daryast ettelat e code taeed az server";
		pState = PopUpState.NoPopUp;
	}
	public class ConfirmIAPResponseModel
	{
		public string Message;
		public string Result;
	}
	public IEnumerator AmaliatConfirmIAP(System.Action OnComplete)
	{
		while (!ConfirmIAPResponse.isDone)
			yield return null;

		Debug.Log(ConfirmIAPResponse.text);

		var x = JsonUtility.FromJson<ConfirmIAPResponseModel>(ConfirmIAPResponse.text);

		Debug.Log(x.Message);
		Debug.Log(x.Result);

		if (OnComplete != null)
		{
			OnComplete();
		}
		if (x.Message == "successful")
		{
			statusText.text = "kharid kamel shod sibil ezafe mishe ";
		}
		else
		{
			statusText.text = "code taeed dorost nabood kharid laghv mishe";
			pState = PopUpState.NoPopUp;
		}
	}
	#endregion

}


